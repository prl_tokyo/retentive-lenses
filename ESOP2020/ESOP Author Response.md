(Better to use a markdown editor for typesetting this response.)

We thank the reviewers for their time spent on reviewing our paper. Here we answer reviewers' questions and address some misunderstandings, where similar concerns from different reviewers are gathered, marked with tags, and answered together. The response is a bit long as it includes the description of reviewers' questions, which take up space.



> [#1] The paper has not well-positioned itself in the literature.
>
> [#1] The notions of *retentiveness* and *reflectiveness* serve very similar purposes, but a discussion of the relationship is missing in the paper.
>
> [#1] Would you please explain the connection between retentive lenses and reflective printer?

We choose the wrong word *reflective* when writing papers in the past; we mean *retentive* all the time. This paper was motived by our previous work on building *reflective* parser and printer pairs, however, the difference between this paper and previous work is still clear.

Firstly, we must point out, as **shown in the introduction**, nearly all the lenses are designed to be *retentive*, and nearly every lens comes with some algorithm for achieving better *Retentiveness*, and it is this paper which formalises *Retentiveness*, achieving it and measuring it using the notion of *links*. 

Secondly, the SLE2016 work focuses on building *reflective* parser and printer pairs for unambiguous grammars and the NGC work extends the tool to handle ambiguous grammars; there, 

- *Retentiveness* (i.e.*reflectiveness*) was only informally presented using examples—like ‘this is the desired updated program text’ and nothing else and like any other lens papers; 
- *links* never appear—the parser in our previous work does not generate consistency links and the input of the printer cannot take any links;
- *edit operations* are not developed and the printer performs position-wise alignment by default.

In general, by no means, they share the similarity on *Retentiveness* and the position of this paper is to provide a general retentive lens framework and demonstrate the feasibility. In conclusion, we think this work should be compared with other general lens frameworks, especially the various alignment strategies in lenses, as discussed in the related work (and Section 6.3).



> [#1] I find the small example in Section 1 counter-productive. The definition of put_2 can be described as cheating as it is defined specifically to defeat the lens laws by invoking a check on "get src".
>
> [#2] I found the motivation for the work as described in the introduction not particularly convincing. In particular, I would have liked to see a more realistic example instead of the chosen simple artificial example... 

- put_2 does not defeat Correctness or Hippocraticness.
- ‘Invoking a check on "get_src"’ can only guarantee Correctness; it does not help guarantee Hippocraticness.

We had tried to use the refactoring example from Section 6.1 from the very beginning. But finally, we decided to deliberately choose this simplest example to make the readers who are unfamiliar with BX understand the **problem in the well-behavedness laws** easily, which **should but cannot precisely describe information retention** and provide guarantees. In other words, if there is a tool only checking well-behavedness, it cannot prevent unreasonable and silly programs—but in practice, well-behaved lenses are already considered a satisfactory solution, which is a contradiction, as **stated in the introduction**. Using this example, our framework means to fix the problem of this kind.



> [#3]
>
> 1. The paper states that “if parts of the view are unchanged, then the corresponding parts of the source remain unchanged”. However, this property is only "useful" if the links created by the get method are "useful". Do you agree? 
>
> 2. The properties of the lenses from section 6 are not stated not proven (P from above).
> 3. I think that just being a retentive lens is not useful. For example, as the authors say any get/put pair which is well-behaved may be extended to become a retentive lens -- and so the strawman from page 3 (get/put_2 from page 3) forms a retentive lens.
> 4. In definition 3, I suspect that you meant to also require that the output links be valid for the source and view, and the input links be valid for source and view – otherwise how could the first conclusion in Proposition 1 hold?

1. Agreed. Yes; it depends on whether the *get* function can decompose a source and view into meaningful fragments. As discussed in the **appendix C.1**, we had tried to generalise region patterns to properties and give *Strong Retentiveness* which forces the get function to decompose a source and view into meaningful fragments so that the fragments consist of all the information and uniquely determine the source—so that even Hippocraticness is subsumed (by *Strong Retentiveness*). However, the framework and links turn out to be complex. Finally, we opt for this simpler but weaker framework.
2. Yes. P from above is not proven (but verified by experiments). However, P is not a property of retentive lenses. *Retentiveness* only considers the ‘`s’=put(v’,ls’)---> get(s’)=(v’,ls’)`’ part in P—when we say ‘preserve information of interest’, the ‘information of interest’ is the data fragments connected to the input links (of the put).  ‘In history’, we had formalised property P with the help of *vertical links* and built a (square) commutative diagram. But eventually, we opted for the current, simpler theory and deleted the notion of vertical links. (But we find that the deletion is not complete, as you can still find ‘strange sentences’ in the related work regarding vertical links: ‘in our setting, we require that two pieces of data linked by vertical correspondence be equal (under a specific pattern)…’. ) The rationale is that the original state-based lens framework (which we extended) does not have the notion of view update. A view given to a put function is not necessarily modified from the view got from the source---it can be constructed in any way, and put does not have to consider how the view is constructed. Coincidentally, the paper about (symmetric) delta-based lenses (Diskin et al., 2011) also introduces square diagrams and later switches to triangular diagrams. So we retain this separation of concern.
3. Yes. Retentive lenses together with proper input links matter since it is the input links that determine ‘information to be retained’. This flexibility is necessary, as it enables the user to specify what to be retained as they wish—not ‘all or none’. The strawman from page 3 can be trivially lifted to a retentive lens using the strategy given in Example 2; however, the lifted retentive lens is indeed less useful for its input links are restricted to a single one or empty—which can only retain ‘all or none’, just like a traditional lens. But note that if a source is not consistent with a given view and there has the single input link, the lifted put should fail, which will not result in loss of information in the updated source. This is different from the unlifted version, which will succeed (and ‘destroy’ the source). Our DSL is useful in the sense that a generated get function decomposes sources and views at a fine level of granularity so that it allows non-trivial input links.
4. We assume that your ‘input links’ mean *ls* and ‘output links’ mean *ls'*. Yes; they are valid. *ls* is valid otherwise put will not produce a new source. *ls'* is produced from get so that it is always valid.



> [#3] Also, I wonder whether you could further simplify the construction from Example 2, by returning and creating an empty set of links.

You are right; Example 2 should be simplified in the way you presented. It is from the last version with *Strong Retentiveness* as described above, and we forgot to relax the conditions to fit in the current (weak) Retentiveness...



> [#2] Can you discuss the limitations of the approach?
>
> [#3] I found the idea of defining consistency relations between the trees appealing, and I wonder whether it could be abstracted and generalised. Something along the lines of given any relation across trees, the links express that relation if.
>
> [#2] It is not clear to me if and how easily the approach can be extended to other (non tree-based) data representation. 

The reviewers may already find the limitations of the approach from our answers above.

1. The current (weak) *Retentiveness* presented in the paper does not force get functions to completely decompose sources and views and it does not subsume Hippocraticness; it is a bit weak. *Strong Retentive* solve the problem but is complex and hard to use. We did not find a more balanced point.
2. There lacks general (or specific) measurement which evaluates ‘how good a get function decomposes the source and view and links the corresponding parts’.
3. We ever considered the generalisation of the framework in several aspects. But the unsuccessful (or at least, uneasy) generalisation perhaps could be seen as another limitation of the framework, either.

- - One is slightly discussed in the **appendix C.1 and C.3**, where we generalise region patterns to arbitrary *properties* and paths to the proofs (witness) of having those properties. This makes it possible to apply *Retentiveness* to non-tree-based data representation. But we have not built a proper DSL for describing retentive lenses yet.
  - Another is to generalise links between a source and a view. The links in our paper are kind of ‘monomorphic’. However, as discussed in the **appendix C.1**, there could be various links connecting properties of different kinds. In general, we may let each link *l* carry a different relation *r* between parts of a source and parts of a view; but we did not dive into it or find good formalisation.