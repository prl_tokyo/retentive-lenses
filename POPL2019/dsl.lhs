\section{A DSL for Retentive Bidirectional Tree Transformation}
\label{sec:dsl}

With theoretical foundations of retentive lenses in hand, we shall propose a domain specific language (DSL) for easily describing them.
Our DSL is designed to be simple but suitable for handling the synchronisation between syntax trees.
We give a detailed description of the DSL by presenting its programming examples (\autoref{subsec:examples}), syntax (\autoref{subsec:syntax}), semantics (\autoref{subsec:dslsem}), and finally the proof of its generated lenses holding Retentiveness (\autoref{mainthm}).


\subsection{Description of the DSL by Examples}
\label{subsec:examples}
In this subsection, we introduce our DSL by describing the consistency relations between the concrete syntax and abstract syntax of the arithmetic expression example in \autoref{fig:running_example_datatype_def}.
From each defined consistency relation, the DSL generates us a pair of |get| and |put| functions forming a retentive lens.
%
Furthermore, we show how to flexibly update the |cst| \autoref{fig:swap_and_put} in different ways using the generated |put| function with different input links.


In our DSL, the user defines data types in Haskell syntax and describes consistency relations between them as if writing |get| functions.
For example, the data type definitions for |Expr| and |Term| written in our DSL remain the same and the consistency relations between them (i.e., the |getE| and |getT| functions in \autoref{fig:running_example_datatype_def}) are expressed as the ones in \autoref{fig:dsl_example}.
Here we describe two consistency relations similar to |getE| and |getT|: one between |Expr| and |Arith|, and the other between |Term| and |Arith|.
%
Each consistency relation is further defined by a set of inductive rules, stating that if the subtrees matched by the same variable appearing on the left-hand side (source side) and right-hand side (view side) are consistent, then the large pair of trees constructed from these subtrees are also consistent.
(For primitive types which do not have constructors such as integers and strings, we consider them consistent if and only if they are equal.)
%
Take |Plus _  x  y  ~  Add  x  y| for example;
it means that if |x_s| is consistent with |x_v|, and |y_s| is consistent with |y_v|, then |Plus a x_s y_s| and |Add x_v y_v| are consistent for any value |a|, where |a| corresponds to a `don't-care' wildcard in |Plus _ x  y|.
So the meaning of |Plus _  x  y  ~  Add  x  y| can be better understood by the `derivation rule' beneath:
\[
\frac{|x_s ~ x_v| \quad |y_s ~ y_v|}
{\forall a.~|Plus a x_s y_s ~ Add x_v y_v|}
\]
Generally, each consistency relation is translated to a pair of |get| and |put| functions, and each of these inductive rules form one case of the two functions.

Now we briefly explain the semantics of |Plus _ x y ~ Add x y|.
In the |get| direction, its behaviour is quite similar to the first case of |getE| in \autoref{fig:running_example_datatype_def}, except that it also updates the links between subtrees marked by |x| and |y| respectively, and establishes a new link between the top nodes |Plus| and |Add| recording the correspondence.
In the |put| direction, it creates a tree whose top node is a |Plus| with empty annotations and recursively builds the subtrees, provided that there is no link connected to the node |Add| (top of the view).
%
If there are some links, then the behaviour of |put| is guided by those links; for example, |put| may additionally preserve the annotation in the old source.
We leave the detailed descriptions to the subsection about semantics.
%

\begin{figure}[t]
\begin{multicols}{2}
\begin{code}
Expr <---> Arith
  Plus   _  x  y  ~  Add  x  y
  Minus  _  x  y  ~  Sub  x  y
  FromT  _  t     ~  t

Term <---> Arith
  Lit     _  i   ~  Num  i
  Neg     _  r   ~  Sub  (Num 0)  r
  Paren   _  e   ~  e
\end{code}
\end{multicols}
\caption{The program in our DSL for synchronising data types defined in \autoref{fig:running_example_datatype_def}.}
\label{fig:dsl_example}
\end{figure}


With the lenses generated from \autoref{fig:dsl_example}, we can synchronise the old |cst| and modified |ast'| in different ways by running |put| with different input links. \autoref{fig:running_example} illustrates this:
\begin{itemize}
  \item If the user assumes that the two non-zero numbers |Num 1| and |Num 2| in the |ast| are exchanged, then four links (between |cst| and |ast'|) should be established: a link connects region |Lit "1 in sub" _| with region |Num _| (of the tree |Num 1|); a link connects |Lit "2 in neg" _| with |Num _| (of the tree |Num 2|); two links connects |1| and |1|, and |2| and |2|.
  The result is |cst_1|, where the literals $1$~and~$2$ are swapped, along with their annotations.
  Note that all other annotations disappeared because the user did not include links for preserving them.

  \item If the user thinks that the two subtrees of |Add| are swapped and passes |put| links that connect not only the roots of the subtrees of |Plus| and |Add| but also all their inner subtrees,
  the desired result should be |cst_2|, which represents `$-2+(0-1)$' and retains all annotations, in effect swapping the two subtrees under |Plus| and adding two constructors |FromT| and |Paren| to satisfy the type constraints.
  % (As mentioned before, the two constructors are added by the injections since they are interconvertible data types.)
  \autoref{fig:running_example} shows the input links for |Neg| and its subtrees.
  \item If the user believes that |ast'| is created from scratch and is not related to |ast|, then |cst_3| may be the best choice, which represents the same expression as |cst_1| except that all annotations are removed.
\end{itemize}

\begin{figure}[t]
\includegraphics[scale=0.6,trim={4cm 7cm 4cm 3cm},clip]{pics/put_with_links.pdf}
\caption[|cst| is updated in many ways.]{|cst| is updated in many ways. \footnotesize{(Red dashed lines are some of the input links for |cst_2|.)}}
\label{fig:running_example}
\end{figure}


Although the DSL is tailored for describing consistency relations between syntax trees, it is also possible to handle general tree transformations.
Now we present three small but typical programming examples other than syntax tree synchronisation.
For the first example, let us consider the binary trees
\vspace{-0.4em}
\begin{center}
\begin{code}
data  BinT a = Tip | Node a (BinT a) (BinT a)
\end{code}
\end{center}
\vspace{-0.4em}
We can concisely define the |mirror| consistency relation between a tree and its mirroring as
\vspace{-0.4em}
\begin{center}
\begin{code}
BinT Int <---> BinT Int
^ ^ Tip         ~  Tip
^ ^ Node i x y  ~  Node i y x
\end{code}
\end{center}
\vspace{-0.4em}
As the second example, we demonstrate the implicit use of some other consistency relation when defining a new one. Suppose that we have defined the following consistency relation between natural numbers and boolean values:
\vspace{-0.4em}
\begin{center}
\begin{code}
Nat <---> Bool
^ ^ ^ Succ _  ~  True
^ ^ ^ Zero    ~  False
\end{code}
\end{center}
\vspace{-0.4em}
Then we can easily describe the consistency relation between a binary tree over natural numbers and a binary tree over boolean values:
\vspace{-0.4em}
\begin{center}
\begin{code}
BinT Nat  <---> BinT Bool
^ ^ ^ Tip           ~  Tip
^ ^ ^ Node x ls rs  ~  Node x ls rs
\end{code}
\end{center}
\vspace{-0.4em}
As the last example, let us consider rose trees, a data structure mutually defined with lists:
\vspace{-0.4em}
\begin{center}
\begin{code}
data  RTree a = RNode a (List (RTree a))
data List a = Nil | Cons a (List a)
\end{code}
\end{center}
\vspace{-0.4em}
We can define the following consistency relation to associate the left spine of a tree with a list:
\vspace{-0.4em}
\begin{center}
\begin{code}
RTree Int <---> List Int
^ ^ RNode i Nil         ~  Cons i Nil
^ ^ RNode i (Cons x _)  ~  Cons i x
\end{code}
\end{center}
\vspace{-0.4em}


\subsection{Syntax}
\label{subsec:syntax}
\begin{figure}[t]
\centerline{
\framebox[10cm]{\vbox{\hsize=14cm
\[
\begin{array}{llllllll}
\colorbox{light-gray}{\textsf{Program}} \\
\bb
  \m{prog} &::=& \m{tDef} \ \m{cDef}_1  \dots \m{cDef}_n \\
\ee \\[0.5em]
%
% \key{Type Synonym} \\
% \bb
% \ee \\[0.5em]
%
\colorbox{light-gray}{\textsf{Type Definition}} \\
\bb
  \m{tDef}  & ::= &  \m{tSyn}_1 \dots \m{tSyn}_m \ \m{tNew}_1 \dots \m{tNew}_n \ \m{conv}_1 \dots \m{conv}_n \\[0.3em]
  \m{tSyn}  & ::= & \q{type}~\m{type}~\q{=}~\m{type}_1 \dots \m{type}_m \\
\ee \\[1em]
\bb
  \m{tNew}  & ::= & \q{data} \ \m{type}  & \! \q{=}  & C_1 \ \m{type}_{11} \dots \m{type}_{1k_{1}}\\
            &     &                      & \q{||} & \dots\\
            &     &                      & \q{||} & C_n \ \m{type}_{n1} \dots \m{type}_{nk_{n}} \\
\ee \\[1.6em]
\bb
  \m{conv} ::= \q{instance}~\q{InterConv}~\m{type}_1~\m{type}_2~\q{where} \\
  \qquad \qquad \ \q{inj}~\m{var}~\q{=}~\m{HaskellExpression} \\
\ee \\[1.2em]
%
\colorbox{light-gray}{\textsf{Consistency Relation Definition}} \\
\bb
  \m{cDef} &::=& \m{type}_s \longleftrightarrow \m{type}_|v|
                           &\reason{relation type}\\
           &   &  \quad r_1 \dots r_n \; \q{;;}  &\reason{inductive rules}\\
\ee \\[1.1em]
%
\colorbox{light-gray}{\textsf{Inductive Rule}}\\
\bb
   r   &::=& \m{pat}_s~\text{`}\!\sim\!\text{'}~\m{pat}_v \\
\ee \\[0.5em]
%
\textsf{Pattern}\\
\bb
   pat &::=& |v|                               &\reason{variable pattern}\\
       &~||& \_                                &\reason{don't-care wildcard}\\
       &~||& C \ \m{pat}_1 \dots \m{pat}_n     &\reason{constructor pattern}\\
\ee
\end{array}
\]
}}}
\caption{Syntax of the DSL.}
\label{fig:dsl_syntax}
\end{figure}


Having seen some programming examples, here we summarise the syntax of our language in \autoref{fig:dsl_syntax}.
A program consists of two main parts: definitions of data types and consistency relations between these data types.
%
We adopt the Haskell syntax for data type definitions, so that a new data type is defined through a set of data constructors |C_1 ... C_n| followed by types, and a type synonym is defined by giving a new name to existing types.
(Definition for \texttt{type} is omitted.)
For the convenience of implementation, the part defining type synonyms should occur before the part defining new data types.
%
At the end of type definitions, the user can describe (override) the conversions between interconvertible data types; otherwise, similar ones will be automatically generated.
We will explain more about this in the syntactic restrictions below.
Now we move to the syntax of consistency relations, where each of them starts with |type_s <---> type_v|, representing the source type and view type for the relation.
The body of each consistency relation is a list of inductively-defined rules, where each rule is defined as a relation between the source and view patterns |pat_s ~ pat_v|, and a pattern |pat| includes variables, constructors, and wildcards.
Finally, two semicolons `;;' are added to finish the definition of a consistency relation. (In this paper, we always omit `;;' when there is no confusion.)
%

\paragraph{Syntactic Restrictions}
\label{subsec:synres}


To guarantee that consistency relations in our DSL indeed correspond to retentive lenses, we impose several syntactic restrictions on the DSL. (Although they are not sufficient and we need further semantic checks.)
Some of the restrictions are quite natural while some of them are a little subtle; the reader may skip the subtle ones at the first reading.

\begin{itemize}
\item On \emph{patterns}, we assume pattern coverage and source pattern disjointness.
Pattern coverage guarantees totality:
for the consistency relation |Ts <---> Tv|, all the patterns on its source side should cover all the possible cases of type |Ts|, and all the patterns on the view side should cover all the cases of type |Tv|.
%
Source pattern disjointness requires that all of the source patterns in a consistency relation do not overlap each other so that at most one pattern is matched when running |get|.
%
Additionally, a bare variable pattern is not allowed on the source side and wildcards are not allowed on the view side; for example, neither |x ~ C x| nor |C x ~ D _ x| is allowed.
%
These requirements imply that |get| is indeed a total function.

\item On \emph{algebraic data types}, we require that types |T_1| and |T_2| be interconvertible when consistency relations |T_1 <---> V| and |T_2 <---> V| are both defined for some view type |V| and there are some types |Ts| and |Tv| such that the definition of |Ts| makes use of |T_1| and |T_2| (directly or indirectly), the definition of |Tv| makes use of |V|, and |Ts <---> Tv| is also defined. \zhixuan{A picture can be very helpful here.}
Consequently, when synchronising |T_s| and |T_v| using |put|, there might be links asserting values of |T_2| should be retained in a context where values of |T_1| are expected, or vice versa.
%
Thus we need ways to convert between |T_1| and |T_2| in such cases.
%
Let us consider a particular case where |T_1| and |T_2| are mutually recursively defined and mapped to the same view type.
For instance, since the two consistency relations |Expr <---> Arith| and |Term <---> Arith| in \autoref{fig:dsl_example} share the same view type |Arith|,
there should be a way to convert (inject) |Expr| into |Term| and vice versa.
Look at |cst'| in \autoref{fig:swap_and_put}, the second subtree of |Plus| is of type |Expr| but created by using a link connected to the old source's subtree |Neg ^ ...| of type |Term|, so we need to wrap |Neg ^ ...| into |FromT "" (Neg ^ ...)| to make the types match.
%
\item On \emph{converting functions}, we require that they not discard information nor add unnecessary information.
Hence they are restricted to the form
$|inj|_{T_1\rightarrow T_2}\,\, x = |C|\, \dots\, x\, \dots$
and the consistency relation $|T_2| \leftrightarrow |V|$ should have a rule: |C _ ^ ... x ... ^ _ ~ x| in which the source pattern only has wildcards except $C$ and $x$.
\end{itemize}


\subsection{Semantics}
\label{subsec:dslsem}


In this subsection, we give a denotational semantics of our DSL by specifying a program's corresponding |get| and |put| as (mathematical) functions. Our language is also implemented as a compiler transforming our DSL to |get| and |put| functions in Haskell.
The implementation is similar to the semantics given below.

\subsubsection{Types and Patterns}
Before defining the semantics of |get| and |put|, we need the semantics of type declarations and patterns in our DSL.
The semantics of type declarations is the same as those in Haskell so that we will not elaborate much here.
For each defined algebraic data type |T|, we also use |T| to denote the set of all values of |T|. In addition, there is a set |Tree|, which is the set of all the values of all the algebraic data types defined in our DSL.
%
|Pattern| is the set of all possible patterns;
for a pattern $p \in |Pattern|$, $|Vars|(p)$ denotes the set of variables in $p$, $|TypeOf|(p,|v|)$ is the set corresponding to the type of $|v| \in |Vars|(p)$, and $|Path|(p,|v|)$ is the path of $|v|$ in $|p|$.

We use the following (partial) functions to manipulate patterns:
\begin{align*}
|isMatch| &: (|p| \in |Pattern|) \times |Tree| \rightarrow |Bool| \\
|decompose| &: (|p| \in |Pattern|) \times |Tree| \pfun \big(|Vars|(|p|) \rightarrow |Tree|\big) \\
|reconstruct| &: (|p| \in |Pattern|) \times \big(|Vars|(|p|) \rightarrow |Tree|\big) \pfun |Tree|
\end{align*}

Given a pattern |p| and a value (i.e., tree) |s|, $|isMatch| |(p, s)|$ tests if |s| matches |p|.
If the match succeeds, $|decompose| |(p, s)|$ returns a function |f| mapping every variable in |p| to its corresponding matched subtree of |s|.
Conversely, $|reconstruct| |(p, f)|$ produces a tree |s| matching pattern |p| by replacing every occurrence of $|v| \in |Vars|(|p|)$ in |p| with $f(|v|)$, provided that |p| does not contain any wildcard.

Since the semantics of patterns in our DSL is rather standard, we omit detailed definitions of these functions. \footnote{We assume non-linear patterns are supported: multiple occurrences of the same variable in a pattern must capture the same value.}


\subsubsection{Get Semantics}\label{subsubsec:getsem}
For a consistency relation $|S| \leftrightarrow |V|$ defined in our DSL with a set of inductive rules $R = \{\, |spat_i| \sim |vpat_i| \mid 1 \leq |i| \leq n\,\}$, its corresponding $\getsv$ function has the following type:
\begin{align*}
\getsv : |S| \rightarrow  |V| \times |Links|
\end{align*}
where $|Links| = \powerset{|Property| \times |Property|} \times |Locations| \times |Locations|$ and $|Locations| = |Name| \pfun |Path|$ as in \autoref{subsec:formal}.
The |get| function defined by $R$ is:
\newcommand{\mywhere}[0]{\quad\text{ where } }
\newcommand{\myspace}[0]{\quad\phantom{\text{ where }} }
\newcommand{\myindent}[0]{\phantom{xxxxxxxxxxx}}
\newcommand{\myindentS}[0]{\phantom{xxxx}}
\newcommand{\myindentSS}[0]{\phantom{xx}}
\begin{align*}
&|get|_|SV| = |genIsTop| \circ |get'|_|SV| \\
&|get'|_|SV|(s) = (|reconstruct|(|vpat|_k, |fst| \circ |vls|),\; l_{\mathit{root}} \sqcup |links|) \numberthis \label{equ:get} \\
&\mywhere |spat|_k \sim |vpat|_k \in R \text{ and } |spat|_k \text{ matches } s \\
&\myspace |vls| = (|get'| \circ |decompose|(|spat|_k,\, s)) \in |Vars|(|spat|_k) \rightarrow |V| \times |Links| \\
&\myspace x,\,y \in |Name| \text{ and fresh} \\
&\myspace |spat'| = |EraseVars|(|fillWildcards|(|spat|_k, s)) \\
&\myspace l_{\mathit{root}} = \Big(\myset{(|HasRegion|\; spat' \; x ,\, |HasRegion| \; |EraseVars|(|vpat|_k) \; y)} \mathop{\cup} |relPos|,\\
&\myspace\myindent \myset{x \mapsto \epsilon},\,  \myset{y \mapsto \epsilon}\Big) \\
&\myspace |relPos| = \big\{ \big(|RelPos|\; |Path|(|spat|_k, t) \; x \; |st|,\, |RelPos| \; |Path|(|vpat|_k, t) \; y \; |vt|\big)  \numberthis \label{equ:relpos}\\
&\myspace\myindent \mid t \in |Vars|(|vpat|_k),\,(\_, (\_, m_|src|, m_|view|)) = |vls|(t), \\
&\myspace\myindent\myindentSS m_|src|(|st|) = \epsilon,\,m_|view|(|vt|) = \epsilon \big\} \\
&\myspace |links| = \bigsqcup \big\{ (|ps|, (|Path|(|spat|_k, t) \dblplus) \circ m_|src|, (|Path|(|vpat|_k, t) \dblplus) \circ m_|view|) \\
&\myspace\myindent \mid t \in |Vars|(|vpat|_k),\,(\_, (|ps|, m_|src|, m_|view|)) = |vls|(t) \big\} \\
&|genIsTop|(|v|, (|ps|, |ms|, |mv|)) = (|v|,\, (|ps|',\,|ms|,\,|mv|)) \\
&\mywhere |rs|,|rv| \in |Name| \text{ s.t. }  |ms|(|rs|) = \epsilon \text{ and } |mv|(|rv|) = \epsilon \\
&\myspace |ps|' = |ps| \mathop{\cup} \myset{\left(|IsTop| \; |rs|,|IsTop| \; |rv|\right)}
\end{align*}
where the operator $\sqcup \in |Links| \times |Links| \rightarrow |Links|$ is the union on |Links| by taking union of corresponding components, and big $\sqcup$ is this operation extended to a set of |Links|.
For a tree $s$ matching |spat_k|, $|fillWildcards|(|spat_k|, s)$ is the resulting pattern of replacing all the wildcards in |spat_k| with the corresponding subtrees of |s| and $|EraseVars|(|spat_k|)$ is the resulting pattern of replacing all variables in pattern |spat_k| with wildcards.

%\zirun{Maybe the following two paragraphs can be moved to the beginning of Get Semantics to help the reader gain the overall idea? I am not sure.}
The idea of $|get|(s)$ is to use the rule $|spat|_k \sim |vpat|_k \in R$ of which $|spat|_k$ matches $s$---our DSL requires such a rule uniquely exists for all $s$---to generate the top portion of the view and recursively generate subtrees for all variables in $|spat|_k$. The recursive call is written as $|get'| \circ |decompose|(|spat|_k,\, s)$ in the definition above, while to be precise, it should be $|get|'_{|TypeOf|(|spat|_k, t),\,|TypeOf|(|vpat|_k, t)}$ for every $t \in |Vars|(|spat|_k)$.

The |get| function also creates links in the recursive procedure. When a rule $|spat|_k \sim |vpat|_k \in R$ is used, it creates a link relating those matched parts as two regions. And it also creates links of |RelPos| recording the relative position of regions so that the produced collection of links satisfies Link Completeness. For the same purpose, a link of |IsTop| is added at the last step to identify the root of the source and the view.

\subsubsection{Put Semantics}
For a consistency relation $|S| \leftrightarrow |V|$ defined in our DSL as $R = \{\, |spat_i| \sim |vpat_i| \mid 1 \leq |i| \leq n\,\}$, its corresponding $|put|_{|s| |v|}$ function has the following type:
\[|put|_{|S| |V|} : |Tree| \times |V| \times |Links| \pfun |S| \]
Given arguments $(|s|,\, |v|,\, (|ps|,\,|ms|,\,|mv|))$, |put| is defined by two cases depending on whether the root of the view is within a region of the input links or not; more precisely, whether there is $y \in |Name|$ such that $|mv|(y) = \epsilon$ and $(|HasRegion| \; |spat| \; x ,\, |HasRegion| \; |vpat| \; y) \in |ps|$.

If such $y$ does not exist, i.e., the root of the view is not within any region of the input links, then |put| simply selects a rule $|spat_k| \sim |vpat_k| \in R$ whose $|vpat|_k$ matches |v|---again, our DSL requires that at least one such rule exist for all |v|---and use $|spat|_k$ as the top portion of the new source.
\begin{align*}
&|put|_{|SV|}(|s|,\, |v|,\, (|ps|,\,|ms|,\,|mv|)) = |reconstruct|(|spat|'_k,\,|ss|) \numberthis \label{equ:put1}\\
&\mywhere |spat|_k \sim |vpat|_k \in R \text{ and } |vpat|_k \text{ matches } |v| \\
&\myspace |vs| = |decompose|(|vpat|_k,\,|v|)\\
&\myspace |ss| = \lambda\,(t \in |Vars|(|spat|_k)) \rightarrow \\
&\myspace\myindentS |put|(|s|, |vs|(t), |trimVPath|(|Path|(|vpat|_k, t),\,(|ps|,\,|ms|,\,|mv|))) \numberthis \label{equ:rec1} \\
&\myspace |spat|'_k = |fillWildcardsWithDefaults|(|spat|_k) \\ \\
&|trimVPath|(|prefix|, (|ps|,\,|ms|,\,|mv|)) = (|ps'|,\,|ms|,\,|mv'|) \\
&\mywhere |mv'| = \myset{ x \mapsto p \mid |mv|(x) = |prefix| \dblplus p} \\
&\myspace |ps'| = \myset{ p \in |ps| \mid \text{Names appeared in } |p| \text{ are defined in (|ms|,\,|mv'|)}}
\end{align*}
The omitted subscription of |put| in (\ref{equ:rec1}) is ${|TypeOf|(|spat|_k, t)\,|TypeOf|(|vpat|_k, t)}$.
Additionally, if there are more than one rules of which $|vpat|$ matches |v|, rules whose |vpat| is \emph{not} a bare variable pattern are preferred.
For example, if both $|spat|_1 \sim |Succ x|$ and $|spat|_2 \sim x$ can be selected, the first one is selected. Such preference helps to avoid infinite recursive calls because if $|vpat|_k = x$, the size of the input of the recursive call in (\ref{equ:rec1}) does not decrease since $|vs|(t) = |v|$ and $|Path|(t, |vpat_k|) = \epsilon$.

For the other case where the root of the view is within some region, |put| `grabs' the related source region as the top portion of the new source.
\begin{align*}
&|put|_{|SV|}(|s|,\, |v|,\, (|ps|,\,|ms|,\,|mv|)) = |inj|_{|TypeOf|(|spat|_k)\rightarrow S}|(reconstruct|(|spat|_k,\,|ss|))  \numberthis \label{equ:put2}\\
&\mywhere l = (|HasRegion| \; |spat| \; x,\,|HasRegion| \; |vpat| \; y) \in |ps| \\
&\myspace\myindentSS \text{ such that } |mv|(y) = \epsilon,\,|ms|(x)\text{ is the shortest } \\
&\myspace |spat_k| \sim |vpat_k| \in R \text{ and } |spat_k| \text{ matches }\footnotemark |spat| \\
&\myspace |vs| = |decompose|(|vpat|_k,\,|v|)\\
&\myspace |ss| = \lambda\,(t \in |Vars|(|spat|_k)) \rightarrow \\
&\myspace\myindentS |put|(|s|, |vs|(t), |trimVPath|(|Path|(|vpat|_k, t),\,(|ps| \setminus \myset{l},\,|ms|,\,|mv|))) \numberthis \label{equ:rec2}
\end{align*}
\footnotetext{Here we extend pattern matching to partial trees such as |spat|. Wildcards in |spat| can be captured by wildcards or variables but nothing else.}
When there is more than one source region linked to the root of the view, |put| chooses the source region whose path is the shortest.
By this, the preserved regions in the new source will have the same relative positions as those in the old source.
Thus |put| implicitly respects all properties asserting relative positions of regions in the old source without explicitly handling |IsTop| links and |RelPos| links in |ps|.
The function $|inj|_{|TypeOf|(|spat|_k),\,S}$ is the transformation from the type of the linked source region to type |S| and is provided by the interconvertible requirement of our DSL (\hyperref[subsec:synres]{Syntax Restrictions}). It is necessary because the linked source region does not necessarily have type |S|.


The last piece for our definition of |put| is its domain, on which |put| satisfies Retentiveness (\ref{law:retain}) and terminates. Some of these conditions are quite subtle and technical. Readers can safely skip them at the first reading.
For a collection of links $|ls| = (|ps|,\,|ms|,\,|mv|)$, $|put|(|s|,\, |v|,\, |ls|)$ is defined when:
\begin{itemize}
\item The collection of links |ls| is valid for |s| and |v| (\autoref{def:validlinks}) and only contains links that can be generated by our |get|.
That is, there exists some $|s'| \in |S|$ such that $|get|(|s|') = (\_,\,(|ps'|,\_,\_))$ and $|ps| \subseteq |ps'|_\sigma$ for some renaming $\sigma$.

\item Regions are well-aligned: For every subtree |t| of |v| whose root is not within the region of any link in |ps|, if |t| matches the view-side pattern of a rule in $R$, the matched portion---i.e., the parts not captured by any variable in the pattern---of |t| does not overlap the view region of any link in |ps|.
This restriction guarantees that when some rule $|spat_k| \sim |vpat_k|$ is used in the first case of |put|, the top portion of |v| matching |vpat_k| does not overlap any view region of some link in |ps|; so that every region link will be handled by the second case of |put| in the recursive procedure, contributing to Retentiveness.
Such well-alignment conditions also appeared in \cite{WangMeng2011}.

\item There is no infinite recursion. In the first case of |put|, the recursive call (\ref{equ:rec1}) does not decrease the size of the argument of |put| when $|vpat_k| = t$ since $|vs|(t) = |v|$ and $|Path|(t, |vpat_k|) = \epsilon$.
Thus we need to restrict the domain of |put| further to rule out the possibility of infinite recursion of such case.
Given type |V| and an infinite sequence of types |S_i| such that for all $i \in \mathbb{N}$, both $|S_i| \leftrightarrow |V|$ and $S_{i+1} \leftrightarrow |V|$ are defined; if there is an inductive rule in $|S_i| \leftrightarrow |V|$:
\[|spat_0| \sim x \quad\text{ for some variable }x\]
where $|TypeOf|(|spat_0|, x) = S_{i+1}$, we require that for any subtree |t| of |v|, there are always an $n \in \mathbb{N}$ and a rule $|spat| \sim |vpat|$ in $|S|_n \leftrightarrow |V|$ such that |vpat| matches |t| and |vpat| is not a bare variable pattern.
\end{itemize}

With the definition of |get| and |put| above, now we have:
\begin{theorem}[Main Theorem]
\label{mainthm}
Let $|put'| = |put|$ with its domain intersected with $|S| \times |V| \times |Links|$, |get| and |put'| form a retentive lens as in \autoref{def:retlens}.
\end{theorem}
The proof of Retentiveness can be found in the appendix (\ref{app:proof}). The basic idea is to prove by induction on the size of the view argument and the link argument of |put|.
Correctness and Link Completeness can be proved similarly.
