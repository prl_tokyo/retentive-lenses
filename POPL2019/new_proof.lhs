\section{Proof of Retentiveness} \label{app:proof}
\begin{theorem}
Let $|put'| = |put|$ with its domain intersected with $|S| \times |V| \times |Links|$, |get| and |put'| form a retentive lens as in \autoref{def:retlens}.
\end{theorem}

\begin{proof}
\newcommand{\mysize}[1]{\lVert #1 \rVert}

We prove they satisfy Retentiveness. Link Completeness and Correctness can be proved similarly.

\begin{lemma}
The recursion of |put| terminates.
\end{lemma}
\begin{proof}
We prove by measuring the size of view argument and link argument.
For the recursive call of the second case of |put| (\ref{equ:rec2}), $\mysize{|vs|(t)} \leq \mysize{|v|}$ and $\mysize{|ps| \setminus \myset{ l }} < \mysize{|ps|}$, thus the size of the input strictly decreased.
For the recursive call of the first case of |put| (\ref{equ:rec1}), if |vpat_k| is not a single variable pattern, then $\mysize{|vs|(t)} < \mysize{|v|}$, thus the size of the input strictly decreased.
However, if $|vpat|_k = x$ for some pattern variable $x$, then $|vs|(t) = |v|$ and the link argument is not decreased either.
But recall that we explicitly ruled out the possibility of an infinite sequence of such non-decreasing recursion in the domain of |put|.
Thus |put| still terminates.
\end{proof}

This lemma enable us to prove Retentiveness by induction on the recursive structure of |put|. For arguments |s|, |v| and |ls| = $(|ps|,\,|ms|,\,|mv|)$, if $(|s|,|v|,|ls|)$ falls into the first case of |put|, we have:
\begin{align*}
&|get|(|put|(|s|,|v|,|ls|)) = |get|(|reconstruct|(|spat|'_k,\, |ss|)) 
\end{align*}
where $|spat|'_k$ and $|ss|$ are defined as in (\ref{equ:put1}). Now we expand the definition of |get| (\ref{equ:get}). 
By the disjointness of source patterns, the same rule $|spat|_k \sim |vpat|_k$ will be selected as by |put|, thus:
\begin{align*}
&|get|(|put|(|s|,|v|,|ls|)) = |genIsTop|((|reconstruct|(|vpat|_k, |fst| \circ |vls|),\; \cdots \sqcup |links|)) \\
&\mywhere |vls| = (|get'| \circ |decompose|(|spat|_k,\, |reconstruct|(|spat|'_k,\, |ss|))) \\
&\myspace \phantom{|vls| }= |get'| \circ |ss| \\
&\myspace \phantom{|vls| }= |get'| \circ \lambda\,(t \in |Vars|(|spat|_k)) \rightarrow \\
&\myspace\myindentS\myindentS |put|(|s|, |vs|(t), |trimVPath|(|Path|(|vpat|_k, t),\,|ls|)) \\
&\myspace \phantom{|vls| }= \lambda\,(t \in |Vars|(|spat|_k)) \rightarrow \\
&\myspace\myindentS\myindentS |get'|(|put|(|s|, |vs|(t), |trimVPath|(|Path|(|vpat|_k, t),\,|ls |))) \numberthis \label{equ:vls} \\
&\myspace |vs| = |decompose|(|vpat|_k, |v|)
\end{align*}
And |links| is:
\begin{align*}
&|links| = \bigsqcup \big\{ (|ps|_t, (|Path|(|spat|_k, t) \dblplus) \circ |ms|_t, (|Path|(|vpat|_k, t) \dblplus) \circ |mv|_t) \numberthis\label{equ:links}\\ 
&\myindent \mid t \in |Vars|(|vpat|_k),\,(\_, (|ps|_t, |ms|_t, |mv|_t)) = |vls|(t) \big\}
\end{align*}
By composing a |genIsTop| after (\ref{equ:vls}), we can apply the inductive hypothesis: Let $|vls|(t) = (\_, (|ps|_t, |ms|_t, |mv|_t))$ and $|trimVPath|(|Path|(|vpat|_k, t),\,|ls|) = (|rs|_t, |ns|_t, |nv|_t)$, then:
\begin{equation}\label{equ:indhyp1}
|rs|_{t}^{*} \subseteq |ps|_{t\,\sigma} \quad\mathop{\wedge}\quad x \in |NV|_t.\, |nv|_t(x) = |mv|_t(\sigma^{-1}(x))
\end{equation}
where $\sigma$ is some renaming function\footnote{Because |get| always uses fresh names, we can assume a uniform $\sigma$ exists for all $t \in |Vars|(|spat_k|)$.}, $|NV|_t$ is the set of all names appeared on the view side of $|rs|_t$, and $A^{*} = A \setminus \myset{(|IsTop| \; x,|IsTop| \; y) \mid x, y \in |Name|}$ in the rest of this section.

From the definition of |trimVPath| and due to the well-aligned restriction we imposed on the domain of |put|, we have:
\[|fst|(|ls|)^* = \bigcup\myset{|rs|_{t}^* \mid t \in |Vars|(|vpat_k|)}\]
Thus:
\[|fst|(|ls|)^* \subseteq \bigcup\myset{|ps|_t \mid t \in |Vars|(|vpat_k|)} = |fst|(|links|)\]
Combined with the |genIsTop| at the last step, we have $|fst|(|ls|) \subseteq |fst|(|links|) \subseteq |fst|(|snd|(|get|(|put|(|s|, |v|, |ls|))))$. This is the first conjunct of Retentiveness that we want to show.

Let the argument $|ls| = (|ps|, |ms|, |mv|)$ and the result $|get|(|put|(|s|, |v|, |ls|)) = (|ps|', |ms|', |mv|')$, then the second conjunct of Retentiveness we want to show is:
\[ \forall x \in |NV|.\, |mv|(x) = |mv'|(\sigma^{-1}(x)) \]
Where |NV| is the names appeared on the view side of |ps|. Again from the well-aligned restriction, we know $\forall x \in |NV|$, there is a $t \in |Vars|(|vpat|_k)$ such that $x \in |NV|_t$.
Intuitively, it means that every region with name $x$ is within some subtree captured by $t$.
From the definition of |trimVPath|, $|mv|(x) = |nv|_t(x) \dblplus |Path|(|vpat|_k, t)$.
Then from the inductive hypothesis (\ref{equ:indhyp1}):
\[|nv|_t(x) \dblplus |Path|(|vpat|_k, t) = |mv|_t(\sigma^{-1}(x)) \dblplus |Path|(|vpat|_k, t)\]
From the definition of |links| (\ref{equ:links}), $|mv'|(\sigma^{-1}(x))$ is exactly $|mv|_t(\sigma^{-1}(x)) \dblplus |Path|(|vpat|_k, t)$. Thus $|mv|(x) = |mv'|(\sigma^{-1}(x))$, which is what we want.

What remains is the second case of |put|: showing Retentiveness when $(|s|, |v|, |ls|)$ falls into (\ref{equ:put2}).
Similar to what we have done above:
\begin{align*}
&|get|(|put|(|s|,|v|,|ls|)) = |get|(|inj|(|reconstruct|(|spat|_k,\, |ss|)))
\end{align*}
where $|spat|_k$ and $|ss|$ are defined as in (\ref{equ:put2}). Because we restricted $|inj|(x)$ functions to be a \emph{wrapping} of $x$, which means that if $|get|(x)=(v, (|ps|, |ms|, |mv|))$ and $|get|(|inj|(x))=(v', (|ps|', |ms|', |mv|'))$ then $v' = v$ and $|ps| \subseteq |ps|' \,\wedge\, |mv| \subseteq |mv|'$. Thus it is sufficient to show Retentiveness for $|get|(|reconstruct|(|spat|_k,\, |ss|))$. As in the first case, we expand the definition of |get|:
\begin{align*}
&|get|(|put|(|s|,|v|,(|ps|,|ms|,|mv|))) = |genIsTop|((|reconstruct|(|vpat|_k, |fst| \circ |vls|),\; l_{\mathit{root}} \sqcup |links|)) \\
&\mywhere |vls| = (|get'| \circ |decompose|(|spat|_k,\, |reconstruct|(|spat|_k,\, |ss|))) \\
&\myspace \phantom{|vls| }= |get'| \circ |ss| \\
&\myspace \phantom{|vls| }= |get'| \circ \lambda\,(t \in |Vars|(|spat|_k)) \rightarrow \\
&\myspace\myindentS\myindentS |put|(|s|, |vs|(t), |trimVPath|(|Path|(|vpat|_k, t),\,(|ps| \setminus \myset{l},\,|ms|,\,|mv|))) \\
&\myspace \phantom{|vls| }= \lambda\,(t \in |Vars|(|spat|_k)) \rightarrow \\
&\myspace\myindentS\myindentS |get'|(|put|(|s|, |vs|(t), |trimVPath|(|Path|(|vpat|_k, t),\,(|ps| \setminus \myset{l},\,|ms|,\,|mv|)))) \numberthis \label{equ:rec3}\\
&\myspace |vs| = |decompose|(|vpat|_k, |v|) \\
&\myspace l = (|HasRegion| \; |spat|_k \; x,\,|HasRegion| \; |vpat|_k \; y) \in |ps| \\
&\myspace\myindentSS\myindentSS \text{ such that } |mv|(y) = \epsilon,\,|ms|(x)\text{ is the shortest }
\end{align*}
where |links| is the same as in (\ref{equ:links}), $l_|root|$ is:
\begin{align*}
&l_{\mathit{root}} = \Big(\myset{(|HasRegion| \; |fillWildcards|(|spat|_k, s) \; x ,\, |HasRegion| \; |vpat|_k \; y)} \\
&\myindent \mathop{\cup} |relPos|,\,\myset{x \mapsto \epsilon},\,  \myset{y \mapsto \epsilon}\Big)
\end{align*}
$|resPos|$, defined as in (\ref{equ:relpos}), is the set of properties asserting relative positions of the regions at the top and regions in the subtrees that are directly adjacent to the top regions.
Links in $(|ps|,|ms|,|mv|)$ comprises three parts: (a) a link of the top source region and view region, (b) links of regions or properties of regions that are in subtrees of the top regions, and (c) links of properties asserting the relative positions of the top region and regions below them.
For (a), the link is retained by the first component of $l_|root|$.
For (b), those links are retained by applying the inductive hypothesis to the recursive call, as what we did in the first case of the proof.
What remains is to show links of (c) are also retained by |put|, though |put| does not handle them explicitly.

If a link $\left(|RelPos| \; p_s \; x_s \; y_s,\, |RelPos| \; p_|v| \; x_|v| \; y_|v|\right) \in |ps|$ with $|ms|(x_s) = |mv|(x_|v|) = \epsilon$, because we restricted links in |ps| can be generated by |get| and our |get| only generates pairs of |RelPos| between adjacent regions, we know $p_s = |Path|(|spat_k|, t_0)$ and $p_|v| = |Path|(|vpat|_k, t_0)$ for some $t_0 \in |Vars|(|spat_k|)$.
Then by the definition of satisfaction this link, $|ms|(y_s) = |Path|(|spat_k|, t_0)$ and $|mv|(y_|v|) = |Path|(|vpat_k|, t_0)$.
And again by our restriction that links in |ps| must be a subset of some |get|, we know that the source region with name $y_s$ must be linked to the view region with name $y_|v|$.
Thus $(|HasRegion| \; |pat|_s \; y_s,\, |HasRegion| \; |pat|_|v| \; y_|v|) \in |ps|$ for some patterns |pat_s| and |pat_v|.
By the definition of |trimVPath|, $|trimVPath|(|Path|(|vpat_k|, t_0), (|ps| \setminus \myset{ l }, |ms|, |mv|))$ still contains the link between $y_s$ and $y_|v|$ with the path of $y_|v|$ being trimmed to be $\epsilon$.
Thus the recursive call
\[|put|(s, |vs|(t_0), |trimVPath|(|Path|(|vpat_k|, t_0), (|ps| \setminus \myset{ l }, |ms|, |mv|)))\]
in (\ref{equ:rec3}) falls into the second case of |put| and the link between region $y_|s|$ and $y_|v|$ will be selected because region $y_|s|$ is directly adjacent to the region above (with name $x_|s|$) so that it must have the shortest path among regions linked to $y_|v|$.
Thus---letting $|vls|(t_0) = (|ps|_{t_0}, |ms|_{t_0}, |mv|_{t_0})$---we have some $|ms|_{t_0}(x_{t_0}) = |mv|_{t_0}(y_{t_0}) = \epsilon$ such that $\sigma(x_{t_0}) = y_s$, $\sigma(y_{t_0}) = y_|v|$ and $(|HasRegion| \; |pat|_s \; x_{t_0} ,\, |HasRegion| \; |pat_v| \; y_{t_0})$. Recall the definition of |relPos|:
\begin{align*}
&|relPos| = \big\{ \big(|RelPos| \; |Path|(|spat|_k, t) \; x \; |st|,\,|RelPos| \; |Path|(|vpat|_k, t) \; y \; |vt|\big)\\
&\myindent \mid t \in |Vars|(|vpat|_k),\,(\_, (\_, m_|src|, m_|view|)) = |vls|(t), \\
&\myindent\myindentSS m_|src|(|st|) = \epsilon,\,m_|view|(|vt|) = \epsilon \big\}
\end{align*}
Thus the link $\left(|RelPos| \; p_s \; x \; x_{t_0},\, |RelPos| \; p_|v| \; y \; y_{t_0}\right)$ is contained in $|relPos|$. Since:
\begin{align*}
|relPos| &\subseteq |fst|(l_|root|) \\
&\subseteq |fst|(|snd|(|put|(|get|(s, |v|, (|ps|, |ms|, |mv|)))))
\end{align*}
We have:
\[\left(|RelPos| \; p_s \; x \; y_s,\, |RelPos| \; p_|v| \; y \; y_|v|\right) \in |fst|(|snd|(|put|(|get|(s, |v|, (|ps|, |ms|, |mv|))))) \]
which is what we want to show.
Finally, if a link $\left(|IsTop| \; x_s, \, |IsTop| \; x_|v|\right) \in |ps|$, it is also retained by the |genIsTop| at the last step. Thus all links of properties of regions are retained as well as links of regions. This completes our proof of Retentiveness.
\end{proof}
