\section{Application}
\label{sec:application}

We demonstrate some practical use of retentiveness by implementing the transformation system between CSTs and ASTs of a small subset of Java 8 in response to the code refactoring example in the introduction.
We show that Retentiveness makes it possible to perform code refactoring on clean ASTs;
and when an AST is modified by a refactoring algorithm, it is possible to retain comments and syntactic sugar on demand.
In the last part of this section, we briefly discuss other potential application scenarios such as resugaring \cite{Pombrio:2014:RLE:2594291.2594319, Pombrio:2015:HRC:2784731.2784755}.



\subsection{Target Language for Refactoring}
To see whether Retentiveness helps to retain comments and syntactic sugar for real-world code refactoring,
we surveyed the standard set of refactoring operations for Java 8 provided by Eclipse Oxygen (with Java Development Tools).
We classify the total 23 refactoring operations as the combinations of four basic operations: substitution, insertion, deletion, and movement.
%
For instance, \emph{Extract Method}, which creates a method containing the selected statements and replace those selected statements (in the old place) with a reference to the created method \cite{eclipse:documentation}, is the combination of insertion (as for the empty method definition), movement (as for the selected statements), and substitution (as for the reference to the created method).
%
For about half of the refactoring operations, position-wise replacement and list alignment (will be discussed in \autoref{subsec:alignment}) is enough, as the code (precisely, subtrees of an AST) is only moved around within a list-like structure (such as an expression list or a statement list).
For the remaining half of the operations, the code is moved far from its original position so that some `global tracking information' such as links are required.
Based on the survey, we believe that a transformation system implemented by retentive lenses is able to support the set of refactoring operations.


Nevertheless, implementation of the whole code refactoring tool for Java 8 using retentive lenses requires much engineering work, and there are further research problems not solved.
%
To make a trade-off, in this paper we focus on the theoretical foundation and language design, and have only implemented a transformation system for a small subset of Java 8 to demonstrate the possibility of having the whole system.
The full-fledged code refactoring tool is left to our future work.




\subsection{Implementation in Our DSL}
Following the grammar of Java 8~\cite{gosling2014java}, we define the data types for the simplified concrete syntax, which consists of only definitions of classes, methods, and variables; arithmetic expressions (including assignment and method invocation); conditional and loop statements.
For convenience, we also restrict the occurrence of statements and expressions to exactly once in most cases (such as variable declarations) except for the class and method body.
%
Then we define the corresponding simplified version of the abstract syntax that follows the one defined by JDT parser~\cite{openjdk}.
We additionally make the AST purer by dropping unnecessary information such as a node's position information in the source file.
%
This subset of Java 8 already has around 80 CST constructs (production rules) and 30 AST constructs; the 70 consistency relations among them generate about 3000 lines of retentive lenses and auxiliary functions (such as the ones for handling interconvertible data types).

Now we write the consistency relations.
Since the structure of the consistency relations for the transformation system is roughly similar to the ones in \autoref{fig:dsl_example}, here we only highlight some different or interesting parts; reviewers can refer to the supplementary material to see the complete program.
%
We see that in the concrete syntax everything is a class declaration while in the abstract syntax everything is a tree.
As a |ClassDecl| should correspond to a |JCClassDecl|, which by definition is yet not a |JCTree|, we use the constructors |FromJCStatement| and |FromJCClassDecl| to make it a |JCTree|, emulating the inheritance in Java.
This is described by the consistency relation\footnote{We include keywords such as \textit{class} in the CST patterns for improving readability, although they should be removed.}
\vspace{-0.4em}
\begin{center}
\begin{code}
ClassDecl <---> JCTree
  NormalClassDeclaration0 _ "class" n "extends" sup body ~
  FromJCStatement (FromJCClassDecl (JCClassDecl N n (J (JCIdent sup)) body))

  NormalClassDeclaration1 _ mdf "class" n "extends" sup body ~
  FromJCStatement (FromJCClassDecl (JCClassDecl (J mdf) n (J (JCIdent sup)) body))
  ... ^ ;;
\end{code}
\end{center}
\vspace{-0.4em}
Depending on whether a class has a modifier (such as \textit{public} and \textit{private}) or not, the concrete syntax is divided into two cases while we use a |Maybe| type in the abstract syntax representing both cases.
%
Similarly, there are further two cases where a class does not extend some superclass and are omitted here.
(To save space, the constructors |Just| and |Nothing| are shortened to |J| and |N| respectively.)


Next, we see how to represent \textit{while loop} and \textit{for-each loop} using the basic \textit{for loop}, as the abstract syntax of a language should be as concise as possible\footnote{Although the JDT parser does not do this.}.
The conversion for \textit{while loop} is quite simple:
\vspace{-0.4em}
\begin{center}
\begin{code}
While "while" "(" exp ")" stmt ~ JCForLoop Nil exp Nil stmt
\end{code}
\end{center}
\vspace{-0.4em}
%
where the four arguments of |JCForLoop| in order denote (list of) initialisation statements, the loop condition, (list of) update expressions, and the loop body.
%
As for a \textit{while loop}, we only need to convert its loop condition |exp| and loop body |stmt| to AST types and put them in the correct places of the \textit{for loop}.
Initialisation statements and update expressions are left empty since there is none.
%

The conversion from \textit{for-each loop} to \textit{for loop} is a little complex, so first consider the general conversion strategy \cite{enhancedFor} through the following example.\\
\begin{minipage}[b]{0.45\textwidth}
\centering
\begin{lstlisting}
        for (type var : exp) {
          // statements using var;
        }
\end{lstlisting}
A \textit{for-each loop}
\end{minipage}%
\begin{minipage}[b]{0.55\textwidth}
\centering
\begin{lstlisting}
      for (int i=0; i < exp.length; i++) {
        type var = exp[i];
        // statements using var;
      }
\end{lstlisting}
The corresponding \textit{for loop}
\end{minipage}\\[0.5em]
%
The |exp| is assumed to be an array-like object that has its length, so that in the converted \textit{for loop} we can use a variable |i|, the length of the object \lstinline{exp.length}, and a post-increment \lstinline{i++} to control the times of the loop.
%
Furthermore, since \lstinline{var} ranges over each object in the array, we need to make this explicitly by using a variable declaration with initialisation in the \textit{for loop}.
These intentions can also be expressed in our DSL straightforwardly (despite being a little verbose)\footnote{We removed the beginning two characters |JC| from all the constructors in the AST except for |JCForLoop|}:
%
\vspace{-0.4em}
\begin{center}
\begin{code}
EnhancedFor "for" "(" ty var ":" exp ")" stmt ~
JCForLoop
  (Cons (FromVarDecl (VarDecl N "i" (PrimTypeTree UInt) (J (FromLit (UInt 0))))) Nil)
  (Binary LESSTHAN (Ident "i") (FieldAccess expr "length") "<")
  (Cons (Unary POSTINC (Ident "i") "++") Nil)
  (FromBlock (Cons (FromVarDecl (VarDecl N var (PrimTypeTree ty)
    (J (ArrayAccess expr (Ident "i"))))) stmt))
\end{code}
\end{center}
\vspace{-0.4em}
%
We can clearly read from the four subtrees of |JCForLoop| that, the first subtree means \lstinline{int i = 0}; the second is \lstinline{i < expr.length}; the third denotes \lstinline{i++}; the last is the concatenation of \lstinline{ty var = expr[i]} and the converted statements from the original \textit{for-each loop} body.
%
Another interesting point is that the variable |expr| appears twice on the view side, once in the filed access (\lstinline{expr.length}) and the other in the array access (\lstinline{expr[i]}).
Other consistency relations can be written alike.


\subsection{System Running}
Now we use the retentive lenses generated from our DSL to run the refactoring example in \autoref{fig:text_and_ast}.
We first test two basic cases: |put cst ast hls| and |put cst ast []|, where |ast| and |hls| are the view and consistency links generated by |get cst|.
%
We get the same |cst| after running |put cst ast hls| and it shows that the generated lenses satisfy Hippocraticness.
We get a |cst'| after running |put cst ast []|, in which the comments disappear and the \textit{for-each loop} becomes a \textit{for loop}.
This demonstrates that |put| can create a new source from scratch only dependent on the given view.
As a special case of Correctness, we also check that |get (put cst ast []) == get cst|.

%
Next we modify |ast| to |ast'|, the tree after code refactoring, and create some diagonal links |hls'| between |cst| and |ast'| by hand.
The diagonal links are designed only to connect the |fuel| method in the |Car| class but not the |fuel| method in the |Bus| class, so that we can observe that comments and \textit{for-each loop} are only preserved for the |Car| class but not the |Bus| class.
This is where Retentiveness helps the user to retain information on demand.
Finally, we also check that |Correctness| holds: |get (put cst ast' hls') == ast'|.


\paragraph{Potential Application}
Retentiveness may apply to similar scenarios such as resugaring~\cite{Pombrio:2014:RLE:2594291.2594319, Pombrio:2015:HRC:2784731.2784755}, which is to print evaluation sequences in a core language using the constructs of its surface syntax.
%
Take the language \textsc{Tiger} \cite{Appel:1998:MCI:522388} (a general-purpose imperative language designed for educational purposes) for example: Its surface syntax offers logical conjunction (| && |) and disjunction (| |||| |), which, in the core language, are desugared to the conditional construct |Cond| after parsing.
Boolean values are also eliminated in the AST, where integer~|0| represents |False| and any non-zero integer represents |True|.
%
For instance, the program text |a && b| is parsed to an AST |Cond a b 0| and |a |||| b| is parsed to |Cond a 1 b|.
If the user want to inspect the result of one-step evaluation of |0 && 10 |||| c|, then the user will face a problem, as the evaluation is in fact performed on the AST |Cond (Cond 0 10 0) 1 c| and will yield new program text in terms of a conditional like $\textit{if}~10~\textit{then}~1~\textit{else}~c$.
%
To solve the problem, \citeauthor{Pombrio:2014:RLE:2594291.2594319} chooses to enrich the AST to incorporate fields for holding tags that mark from which syntactic object an AST construct comes.
By comparison, we can also solve this problem by writing the transformations between the surface syntax and core language using retentive lenses, and pass the lens proper links for retaining syntactic sugar, just like what we have done for Java 8.
%
Both their `tags approach' and our `links approach' need to identify where an AST construct comes from; however, the links approach has an advantage that it leaves ASTs clean so that we do not need to patch up the compiler to deal with tags.
%

% end case studies
