\section{Conclusions and Discussions}
\label{sec:conclusion}

In this paper, we showed that well-behavedness is not sufficient for retaining information by giving a concrete example of code refactoring, in which the syntactic sugar and comments should be preserved in the newly generated program text.
To address the problem, we illustrated how to use links to preserve desired data fragments of the old source after an update, and developed a semantic framework of (asymmetric) retentive lenses for region models.
%
Then we presented a small DSL tailored for describing consistency relations between syntax trees; we showed its syntax, semantics, and proved that the pair of |get| and |put| functions generated from any program in the DSL form a retentive lens.
%
Then we illustrated the practical use of retentive lenses by running the code refactoring example introduced in the introduction.
In the related work, we discussed the relations with alignment, origin tracking, and operation-based BX.
Now, we will briefly discuss our choice of opting for triangular diagrams, composability of retentive lenses, and the feasibility of retaining code styles for refactoring tools in our framework.


% \todo{layouts and second-order properties}
% \todo{discussions (a) (sketch of) properties for programming styles. (a.1) argument alignment. (a.2) second-order properties for arrow alignment. (a.3) braces placement. }
% \begin{code}
% func  ::  a
%       ->  b
%       ->  ...
% \end{code}

\subsection{Opt for Triangular Diagrams}
\label{dis:tridia}
Including vertical correspondence (which represent view updates) in the theory was something we thought about (for quite some time), but eventually, we opted for the current, simpler theory.
The rationale is that the original state-based lens framework (which we extended) does not really have the notion of view-updating built in.
A view given to a |put| function is not necessarily modified from the view got from the source---it can be constructed in any way, and |put| does not have to consider how the view is constructed.
Coincidentally, the paper about (symmetric) delta-based lenses \cite{Diskin-symmetric-delta-lenses} also introduces square diagrams and later switches to triangular diagrams.


We retain this separation of concern in our framework, in particular separating the jobs of retentive lenses and third-party tools that operate in a view-update setting:
third-party tools are responsible for producing vertical correspondence between the consistent view and a modified view (not between sources and views), and when it is time to run |put|, the vertical correspondence are composed (as shown \autoref{fig:swap_and_put}) with the consistency links produced by |get| to compute the input links between the source and the modified view.
%
Although generic `diff' on two pieces of algebraic data is difficult \cite{Miraldo:2017:TDS:3122975.3122976}, `domain-specific diff' is much easier.
For the example of code refactoring, in which the relation between a view and its modified one is known by the refactoring algorithm:
since the algorithm knows how a subtree is moved (i.e. how a subtree's position changes), it can output vertical correspondence recording this movement; other vertical correspondence between unchanged parts can be built position-wise.
%


\subsection{Composability}
% \todo{The discussion might not be mandatory.}
Well-behaved (state-based) lenses are composable; it means that the composition of two well-behaved lenses is still a well-behaved one.
For retentive lenses, we can also define a |comp| function, whose behaviour is similar to that of well-behaved lenses, apart from its producing proper intermediate links for get and put functions.
%
Nevertheless, there are further requirements for composing retentive lenses.
For the moment, we can only say that two retentive lenses |lens_1 :: RetLens A B| and |lens_2 :: RetLens B C| are composable, if |lens_1| and |lens_2| have the same property set on B;
moreover, for any piece of data |b :: B|, |lens_1| and |lens_2| must decompose |b| in the same way.
Otherwise the composition will cause problems, for example:\\
\begin{center}
\includegraphics[scale=0.35,trim={2cm 10cm 2cm 10cm},clip]{pics/linkComp.pdf}
\end{center}
In the above figure, the first lens connects the region |Neg a _| with |Sub (Num 0) _| (the grey parts); while the second lens adopts a different decomposition strategy and decomposes |Sub (Num 0) _| into three small regions and establishes links for them respectively.
It is hard to determine the result of this link composition, and we leave this to the future work.
%
Coincidentally, similar requirements can be found in quotient lenses~\cite{Foster:2008:QL:1411203.1411257}.
A quotient lens operates on sources and views that are divided into many equivalent classes, and the well-behavedness is defined on those equivalent classes rather than a particular pair of source and view.
For sequential composition |l;k|, the authors require that the abstract (view-side) equivalence relation of lens |l| is identical to the concrete (source-side) equivalence of lens |k|.


As for our DSL, we argue that the lack of composition does not cause severe problems because of the philosophy of design.
Take the scenario of writing a parser for example; there are two main approaches for the user to choose: to use parser combinators (such as \textsc{Parsec}) or to use parser generators (such as \textsc{Happy}).
%
While parser combinators offer the user many small composable components,
parser generators usually provide the user with a high-level syntax for describing the grammar of a language using production rules (associated with semantic actions).
Then the generated parser is usually used as a `standalone black box' and will not be composed with some others.
Since our DSL is designed to be a `lens generator', the user will have no difficulty in writing bidirectional transformations without composability.

% \zirun{Removed discussions on Design of the DSL}
% \subsection{Design of the DSL}
% \todo{The discussion might be removed. Just one reviewer asked this question.}
% In the DSL, we make a trade-off and assume that there is only one consistency relation between any two types.
% %
% The advantage is that the name of each consistency relation is automatically generated and maintained so that the syntax of inductive rules can be more concise, because our DSL knows when to implicitly invoke |get| and |put| between the correct source and view types.
% Otherwise, for instance, the rule |Plus x y ~ Add x y| should be written as: |Plus xS yS ~ Add xV yV <== xV ~ f xS , yV <== g yS|.
% %
% The disadvantage is also obvious: in some situation the user does want to define more than one consistency relations between two types.
% For instance, the user may consider two integer lists to be consistent if: (1) their elements are equal position-wise; (2) their elements are equal in the reverse order.
% %
% In this case, our design becomes awkward, since the user needs to define a new data type for the integer list for writing the different consistency relation, which involves wrapping and unwrapping the integer list.

% \subsection{The Word Retentive}
% We are not the first to use the word retentive; its use can be traced back to symmetric lenses \cite{Hofmann:2011:SL:1925844.1926428}, which is the pioneering work of edit lenses~\cite{Hofmann:2012:EL:2103656.2103715}.
% %
% In symmetric lenses, they defined both retentive sum lenses and forgetful sum lenses, where the `retentive one keeps complements for both sub-lenses (branches) while the forgetful one keeps only one complement corresponding to the last put branch.'
% Indeed, retentive is used to describe the one able to memory and preserve more information.

\subsection{Retaining Coding Styles}
A challenge to refactoring tools is to retain the style of program text such as indentation, vertical alignment of identifiers and the place of line breaks.
Some of them can be retained by our DSL, such as the existence of a line break at a certain position, but some other stylings are more difficult to retain.
For example, an argument of a function application may be vertically aligned with a previous argument, when a refactoring tool moves the application to a different place, what should be retained is not the absolute amount of spaces preceding the arguments.
Instead, it should retain the \emph{property} that the amount of spaces makes these two arguments vertically aligned.

Although not implemented in the DSL of this paper, these properties can be added to the set of |Property| in our framework of retentive lenses.
For example, we may have $|VertAligned x y| \in |Property|$ for $|x|, |y| \in |Name|$, and it is satisfied by a CST if the region at the location of |y| in the CST is vertically aligned with a region at the location of |x|.
Then, when |get| computes AST from a vertically aligned argument, instead of including spaces preceding the argument as a part of the source region, |get| produces a link between a property $|VertAligned x y|$ and the AST region.
Later when such links are supplied to |put|, they serve as directives for |put| to adjust the amount of spaces preceding the argument to conform to the styling rule.
Compared with using a code formatter after refactoring, one advantage of handling styles in our framework is flexibility: the styling choices of the user---e.g., an argument opens a new line or stays at the current line---will not be overridden by predefined rules. However, handling coding styles can be very language-specific. Thus it is beyond the scope of this paper and left to the future work of a full-fledged refactoring tool.
