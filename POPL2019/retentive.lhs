% For double-blind review submission, w/o CCS and ACM Reference (max submission space)
% \documentclass[acmsmall,fleqn,review,anonymous]{acmart}\settopmatter{printfolios=true,printccs=false,printacmref=false}

%% For double-blind review submission, w/ CCS and ACM Reference
\documentclass[acmsmall,review,anonymous]{acmart}\settopmatter{printfolios=true}

%% For single-blind review submission, w/o CCS and ACM Reference (max submission space)
%\documentclass[acmsmall,review]{acmart}\settopmatter{printfolios=true,printccs=false,printacmref=false}

%% For single-blind review submission, w/ CCS and ACM Reference
% \documentclass[acmsmall,review]{acmart}\settopmatter{printfolios=true}

% For final camera-ready submission, w/ required CCS and ACM Reference
% \documentclass[acmsmall]{acmart}\settopmatter{}

%include polycode.fmt

%format forall a = "\forall " a
%format exists a = "\exists " a
%format ldot = "."

%format Set_1
%format P_s
%format P_s' = " P_{s''} "
%format P_ss' = " P_{ss''} "
%format subset = " \subset "
%format subseteq = " \subseteq "
%format setconj  = " \cap "
%format sigma   = " \sum "
%format elem    = " \in "
%format getInv = " \textit{get}^{-1} "


%format preyV = "\textit{prey}V"
%format STypes = "S\mkern-3mu\textit{Types}"
%format VTypes = "V\mkern-3.5mu\textit{Types}"
%format S_ExprArithCase0 = "\textit{S\_ExprArithCase0}"
%format V_ExprArithCase0 = "\textit{V\_ExprArithCase0}"
%format prexV = "\textit{prex}V"
%format dollar1 = "\$1"
%format dollar2 = "\$2"
%format dollar3 = "\$3"
%format typeOf = "\textit{typeOf}"

%format pattern_l
%format pattern_r
%format path_l
%format path_r
%format path_t
%format pattern_v
%format path_b
%format pt_1
%format p_1l
%format p_1r
%format pt_2
%format pt_v
%format pt_l
%format pt_r
%format p_2l
%format p_2r
%format vl_1
%format tyX_i
%format tyY_j
%format *** = "*\!\!*\!\!*"

%format e_1
%format e_1' = "e_1''"
%format e_2
%format e_2' = "e_2''"
%format e_3
%format e_b

%format Trait_1
%format Trait_2
%format trait_1
%format trait_2



%format == = "\mathop{\shorteq\kern1pt\shorteq}"
%format ~ = "\mathrel\sim"
%format <== = "\mathrel\Leftarrow"
%format ==> = "\mathrel\Rightarrow"
%format <--> = "\leftrightarrow"
%format --> = "\rightarrow"
%format <---> = "\leftrightarrow"
%format <===> = "\Longleftrightarrow"
%format <==> = "\Leftrightarrow"
%format (VEC(x)) = "\vv{"x"}"
%format (SEMANTIC(x)) = "\llbracket " x "\rrbracket "
%format (VARS(x)) = "\mkern-\thickmuskip(\vv{" x "})"
%format (SUBST(x)(y)) = "\mkern-\thickmuskip[\vv{" x "}/\vv{" y "}]"


%format (ToRegions(x)) = "\mathsf{ToRegions}\llbracket " x "\rrbracket "
%format (InvVarP(x)) = "\mathsf{InvVarP}\llbracket " x "\rrbracket "
%format (GenInsSubtree(x)) = "\mathsf{GenInsSubtree}\llbracket " x "\rrbracket "

%format (GenPutNoLink2 (x) (y)) = "\mathsf{GenPutNoLink} " ( x ) " \llbracket " y "\rrbracket "
%format (GenGet(x)) = "\mathsf{GenGet}\llbracket " x "\rrbracket "
%format (GenGet2 (x) (y)) = "\mathsf{GenGet} " ( x ) " \llbracket " y "\rrbracket "
%format (GenPut(x)) = "\mathsf{GenPut}\llbracket " x "\rrbracket "
%format (GenRepSubtree(x)) = "\mathsf{GenRepSubtree}\llbracket " x "\rrbracket "
%format (MkLink(x))   = "\mathsf{MkLink}\llbracket " x "\rrbracket "
%format (MkTag(x))   = "\mathsf{MkTag}\llbracket " x "\rrbracket "
%format (Pref(x)) = "\mathsf{Pref}\llbracket " x "\rrbracket"
%format typeOfTT = "\mathsf{typeOf}"
%format (CTypeOf(x)) = "\mathsf{TypeOf}\llbracket " x "\rrbracket"
%format (getVar(x)) = "\mathsf{getVar}\llbracket " x "\rrbracket"
%format (MkPat(x)) = "\mathsf{MkPat}\llbracket " x "\rrbracket "
%format (MkRegPat(x)) = "\mathsf{MkRegPat}\llbracket " x "\rrbracket "
%format (local(x)) ="loc\_ " x " "
%format ST ="\mathsf{ST}"
%format VT ="\mathsf{VT}"
%format MkLinkt = " \mathit{MkLink} "
%format (varsTT(x))  = "\mathsf{vars}\llbracket " x "\rrbracket"

%format itOf    = "\mathit{of} "
%format itCase  = "\mathit{case} "
%format itLet   = "\mathit{let} "
%format itIn    = "\mathit{in} "
%format itWhere = "\mathit{where} "
%format whereTT = "\mathsf{where} "
%format itMkPat = "\mathit{MkPat} "

%format GetPut = "\mathsf{GetPut} "
%format PutGet = "\mathsf{PutGet} "

%format Correctness = "\mathsf{Correctness} "
%format Hippocraticness = "\mathsf{Hippocraticness} "
%format Retentiveness = "\mathsf{Retentiveness} "

%format ^   = "\ "
%format ^^  = "\;"
%format ^^^ = "\!\;"
%format ^^^^ = "\!"

%format ::= = "\Coloneqq"

%format T_s1 = "T_{s1}"
%format T_s2 = "T_{s2}"


%format CST_0
%format CST_1
%format CST_2
%format CST_3

%format P_1
%format P_2
%format P_i
%format P_n

%format Q_1
%format Q_2
%format Q_i
%format Q_n


%format t_1
%format t_2
%format s_0
%format s_1
%format s_1' = " s_1'' "
%format s_2
%format s_2' = " s_2'' "
%format s_3
%format s_4
%format s_i
%format v_i
%format s_01
%format s_02
%format v_0
%format v_1
%format v_2
%format v_3
%format l_0
%format l_1

%format ls_j'
%format ls_j

%format ls_i'
%format ls_0' = " \mathit{ls}_0'' "
%format ls_1'
%format ls_n'
%format ls_i
%format ls_0
%format ls_1
%format ls_2
%format ls_3
%format ls_n

%format hl_i'
%format hl_0' = " \mathit{hl}_0'' "
%format hl_1'
%format hl_n'
%format hl_i
%format hl_0
%format hl_1
%format hl_2
%format hl_3
%format hl_n


%format vls_0
%format vls_1
%format vls_1' = " \mathit{vls}_1'' "
%format vls_2
%format vls_3
%format vls_4
%format vls_0' = " \mathit{vls}_0'' "
%format vls_n
%format vls_n'
%format vls_i
%format vls_i'

%format hls_0
%format hls_1
%format hls_1' = " \mathit{hls}_1'' "
%format hls_2
%format hls_3
%format hls_0' = " \mathit{hls}_0'' "
%format hls_n
%format hls_n'
%format hls_i
%format hls_i'

%format imgl_0
%format imgv_0
%format imgh_0

%format ys_0
%format ys_1
%format ys_n
%format ys_0' = " \mathit{ys}_0'' "
%format ys_1' = " \mathit{ys}_1'' "
%format ys_n' = " \mathit{ys}_n'' "

%format y_0'  = " y_0'' "
%format y_1'  = " y_1'' "
%format y_i'  = " y_i'' "
%format y_n'  = " y_n'' "

%format vl_0
%format vl_1
%format vl_2
%format vl_3
%format vl_4
%format vl_i
%format vl_i'
%format hl_0
%format hl_1
%format hl_2
%format hl_3
%format hl_4
%format imag_0
%format x_0
%format x_1
%format x_2
%format x_i
%format x_n
%format y_0
%format y_1
%format y_2
%format y_i
%format y_j
%format y_n
%format z_0
%format z_i
%format z_n
%format y_n
%format y_n
%format prefX_0
%format prefX_i
%format prefX_n
%format prefY_0
%format prefY_i
%format prefY_n
%format env_i
%format env_0
%format env_n
%format get_1
%format put_L

%format get_v
%format get_l
%format put_s
%format put_l
%format put_1
%format put_2
%format put_3
%format inj_s
%format inj_l

%format p_b
%format p_l
%format p_u
%format p_r
%format vl'_0
%format hl'_0
%format pat_l
%format pat_r

%format linkComp = "{\cdot}"

%format exclude = " \backslash\!\backslash "
%format backslash = "\backslash"

%format lens_1
%format lens_2
%format lens_12 = "lens_{12}"
%format (conv (x)) = (x) "^{\circ}"

%format HasRegion = "\mathsf{HasRegion}"
%format IsTop = "\mathsf{IsTop}"
%format RelPos = "\mathsf{RelPos}"
%format VertAligned = "\mathsf{VertAligned}"

\usepackage[UKenglish]{isodate}

\makeatletter
\newcommand{\shorteq}{%
  \settowidth{\@@tempdima}{-}%
  \resizebox{\@@tempdima}{\height}{=}%
}
\makeatother


%% Journal information
%% Supplied to authors by publisher for camera-ready submission;
%% use defaults for review submission.
\acmJournal{PACMPL}
\acmVolume{1}
\acmNumber{CONF} % CONF = POPL or ICFP or OOPSLA
\acmArticle{1}
\acmYear{2018}
\acmMonth{1}
\acmDOI{} % \acmDOI{10.1145/nnnnnnn.nnnnnnn}
\startPage{1}

%% Copyright information
%% Supplied to authors (based on authors' rights management selection;
%% see authors.acm.org) by publisher for camera-ready submission;
%% use 'none' for review submission.
\setcopyright{none}
%\setcopyright{acmcopyright}
%\setcopyright{acmlicensed}
%\setcopyright{rightsretained}
%\copyrightyear{2018}           %% If different from \acmYear

%% Bibliography style
\bibliographystyle{ACM-Reference-Format}
%% Citation style
%% Note: author/year citations are required for papers published as an
%% issue of PACMPL.
\citestyle{acmauthoryear}   %% For author/year citations


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Note: Authors migrating a paper from PACMPL format to traditional
%% SIGPLAN proceedings format must update the '\documentclass' and
%% topmatter commands above; see 'acmart-sigplanproc-template.tex'.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Some recommended packages.
\usepackage{booktabs}   %% For formal tables:
                        %% http://ctan.org/pkg/booktabs
\usepackage{subcaption} %% For complex figures with subfigures/subcaptions
                        %% http://ctan.org/pkg/subcaption

\usepackage{microtype}

\usepackage{multicol}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{stmaryrd}
\usepackage{esvect}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{zi4}
\usepackage[T1]{fontenc}
\usepackage{upquote}
\usepackage{ifthen}
\usepackage{xifthen}
\usepackage{fancybox}
%\usepackage{mathalfa}


% \usepackage[dvipsnames]{xcolor}
\usepackage{tikz}

\usepackage{xcolor}
\newcommand\myworries[1]{\textcolor{red}{\small[#1]}}
\newcommand\todo[1]{\textcolor{blue}{\small[#1]}}
\newcommand\biyacc[0]{\textsc{BiYacc}}
\newcommand\BiYacc[0]{\textsc{BiYacc}}
\newcommand\BiFlux[0]{\textsc{BiFlux}}
\newcommand\biflux[0]{\textsc{BiFlux}}
\newcommand\diff{\,\backslash\!\backslash\,}
\newcommand\dom{{\normalfont\textsc{dom}}}
\newcommand\ldom{{\normalfont\textsc{ldom}}}
\newcommand\rdom{{\normalfont\textsc{rdom}}}

\newcommand{\awa}[2]{\mathrlap{#2}\phantom{#1}} % as wide as

\newcommand\tit[1]{\textit{#1}}
\newcommand\tits[1]{\textit{#1}\ }
\newcommand\ttt[1]{\texttt{#1}}

\newcommand{\varcitet}[3][]{\citeauthor{#2}#3~[\ifthenelse{\isempty{#1}}{\citeyear{#2}}{\citeyear[#1]{#2}}]}

\newcommand\EscapeSans[1]{{-"\text{#1}"-}}

\newtheorem{prereq}{Prerequisites}

\renewcommand{\sectionautorefname}{Section}
\renewcommand{\subsectionautorefname}{Section}
\renewcommand{\subsubsectionautorefname}{Section}
\renewcommand{\figureautorefname}{Figure}
\newcommand{\lemmaautorefname}{Lemma}
\newcommand{\propositionautorefname}{Proposition}
\newcommand{\corollaryautorefname}{Corollary}
\newcommand{\definitionautorefname}{Definition}
\newcommand{\exampleautorefname}{Example}

\newcommand{\getsv}[0]{|get|_{|SV|}}
\newcommand{\yzx}[1]{{\color{blue}(YZX: \textit{#1})} }
\newcommand\numberthis{\addtocounter{equation}{1}\tag{\theequation}}

\definecolor{light-gray}{gray}{0.92}

\allowdisplaybreaks[0]

\newboolean{showcomments}
\setboolean{showcomments}{true} % toggle to show or hide comments
\ifthenelse{\boolean{showcomments}}
{\newcommand{\nb}[2]{
  \fcolorbox{black}{yellow}{\bfseries\sffamily\scriptsize#1}
  {\sf\small$\blacktriangleright$\textit{#2}$\blacktriangleleft$}
 }
 \newcommand{\version}{\emph{\scriptsize$-$working$-$}}
}
{\newcommand{\nb}[2]{}
 \newcommand{\version}{}
}
\newcommand\zhenjiang[1]{\nb{zhenjinag}{#1}}
\newcommand\josh[1]{\nb{josh}{#1}}
\newcommand\zirun[1]{\nb{zirun}{#1}}
\newcommand\zhixuan[1]{\nb{zhixuan}{#1}}

\newcommand{\bb}{\begin{array}{lllll}}
\newcommand{\ee}{\end{array}}
\newcommand{\sem}[1]{[ \!\! |\! [  #1 ]\!\! ] \!| } %  semantics
% \newcommand{\m}[1]{\mbox{\it #1}}
\newcommand{\m}[1]{\textit{#1}}
\newcommand{\q}[1]{\text{`#1'}}
\newcommand{\key}[1]{\textbf{#1}}
\newcommand{\reason}[1]{\mbox{\small~~~~\{ #1 \}}}

\newcommand\dblplus{\mathop{\mathbin{+\mkern-5mu+}}}


\lstset{ 
  basicstyle=\footnotesize\ttfamily,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  language=Java,                 % the language of the code
  % numbers=left,
  % numbersep=5pt,                   % how far the line-numbers are from the code
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,
  tabsize=2,                     % sets default tabsize to 2 spaces
  title=\lstname                 % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\begin{document}

\setlength{\mathindent}{\parindent}

%% Title information
\title[Retentive Lenses]{Retentive Lenses}         %% [Short Title] is optional;
                                        %% when present, will be used in
                                        %% header instead of Full Title.
% \titlenote{with title note}             %% \titlenote is optional;
%                                         %% can be repeated if necessary;
                                        %% contents suppressed with 'anonymous'
\begin{anonsuppress}
\titlenote{Draft manuscript (\today)}   %% \titlenote is optional;
\end{anonsuppress}
% \subtitle{Subtitle}                     %% \subtitle is optional
% \subtitlenote{with subtitle note}       %% \subtitlenote is optional;
%                                         %% can be repeated if necessary;
%                                         %% contents suppressed with 'anonymous'


%% Author information
%% Contents and number of authors suppressed with 'anonymous'.
%% Each author should be introduced by \author, followed by
%% \authornote (optional), \orcid (optional), \affiliation, and
%% \email.
%% An author may have multiple affiliations and/or emails; repeat the
%% appropriate command.
%% Many elements are not rendered, but should be provided for metadata
%% extraction tools.

%% Author with single affiliation.
\author{Zirun Zhu}
% \authornote{}          %% \authornote is optional;
                                        %% can be repeated if necessary
% \orcid{}             %% \orcid is optional
\affiliation{
  \position{Research Assistant}
  \department{Information Systems Architecture Science Research Division}             %% \department is recommended
  \institution{National Institute of Informatics}           %% \institution is required
  \streetaddress{2-1-2 Hitotsubashi, Chiyoda}
  \city{Tokyo}
  % \state{State2b}
  \postcode{101-8430}
  \country{Japan}                   %% \country is recommended
}
\email{zhu@@nii.ac.jp}         %% \email is recommended
\additionalaffiliation{
  \position{Student}
  \institution{SOKENDAI (The Graduate University for Advanced Studies)}            %% \institution is required
  \department{Department of Informatics}              %% \department is recommended
  \streetaddress{Shonan Village}
  \city{Hayama}
  \state{Kanagawa}
  \postcode{240-0193}
  \country{Japan}                    %% \country is recommended
}

\author{Zhixuan Yang}
% \authornote{}          %% \authornote is optional;
                                        %% can be repeated if necessary
% \orcid{}             %% \orcid is optional
\affiliation{
  \position{}
  \department{Information Systems Architecture Science Research Division}             %% \department is recommended
  \institution{National Institute of Informatics}           %% \institution is required
  \streetaddress{2-1-2 Hitotsubashi, Chiyoda}
  \city{Tokyo}
  % \state{State2b}
  \postcode{101-8430}
  \country{Japan}                   %% \country is recommended
}
\email{yzx@@nii.ac.jp}         %% \email is recommended
\additionalaffiliation{
  \position{Student}
  \institution{SOKENDAI (The Graduate University for Advanced Studies)}            %% \institution is required
  \department{Department of Informatics}              %% \department is recommended
  \streetaddress{Shonan Village}
  \city{Hayama}
  \state{Kanagawa}
  \postcode{240-0193}
  \country{Japan}                    %% \country is recommended
}

\author{Hsiang-Shang Ko}
% \authornote{with author2 note}          %% \authornote is optional;
                                        %% can be repeated if necessary
\orcid{0000-0002-2439-1048}             %% \orcid is optional
\affiliation{
  \position{Assistant Professor by Special Appointment}
  \department{Information Systems Architecture Science Research Division}             %% \department is recommended
  \institution{National Institute of Informatics}           %% \institution is required
  \streetaddress{2-1-2 Hitotsubashi, Chiyoda}
  \city{Tokyo}
  % \state{State2a}
  \postcode{101-8430}
  \country{Japan}                   %% \country is recommended
}
\email{hsiang-shang@@nii.ac.jp}         %% \email is recommended


%% Author with two affiliations and emails.
\author{Zhenjiang Hu}
% \authornote{with author2 note}          %% \authornote is optional;
                                        %% can be repeated if necessary
% \orcid{nnnn-nnnn-nnnn-nnnn}             %% \orcid is optional
\affiliation{
  \position{Professor}
  \department{Information Systems Architecture Science Research Division}             %% \department is recommended
  \institution{National Institute of Informatics}           %% \institution is required
  \streetaddress{2-1-2 Hitotsubashi, Chiyoda}
  \city{Tokyo}
  % \state{State2a}
  \postcode{101-8430}
  \country{Japan}                   %% \country is recommended
}
\email{hu@@nii.ac.jp}         %% \email is recommended
\additionalaffiliation{
  \position{Professor}
  \institution{SOKENDAI (The Graduate University for Advanced Studies)}            %% \institution is required
  \department{Department of Informatics}             %% \department is recommended
  \streetaddress{Shonan Village}
  \city{Hayama}
  \state{Kanagawa}
  \postcode{240-0193}
  \country{Japan}                    %% \country is recommended             %% \country is recommended
}


%% Abstract
%% Note: \begin{abstract}...\end{abstract} environment must come
%% before \maketitle command
\begin{abstract}
% THE SUBMISSION ONLY ACCEPT LATEX MATH COMMAND so I use mathit to do the emphasising
Based on Foster et al.'s [2007] lenses, various bidirectional programming languages and systems have been developed for helping the user to write correct data synchronisers.
The two well-behavedness laws of lenses, namely Correctness and Hippocraticness, are usually adopted as the guarantee of these systems.
While lenses are designed to retain information in the source when the view is modified, well-behavedness says very little about the retaining of information,
since Hippocraticness only requires that the source be unchanged if the view is not modified. Thus nothing about retaining is guaranteed when the view is changed.

%To address the problem, we propose a finer-grained law \emph{Retentiveness} and its corresponding $\mathit{retentive lenses}$ on tree structures.
%We identify `part of a tree' (fragment) by proposing a notion $\mathit{region}$, and use $\mathit{links}$ to relate fragments between a source tree and a view tree.
%% Links are passed as additional input to the extended $\mathit{put}$ function, which produces a new source in a way that it preserves all the source fragments attached to the links.
%Retentiveness guarantees that if parts of the view are unchanged, then the corresponding parts of the source are retained as well, and subsumes Hippocraticness as a consequence.
%

To address this problem, we propose an extension of the original lenses, called \emph{retentive lenses}, satisfying a new Retentiveness law which can guarantee that if parts of the view are unchanged, then the corresponding parts of the source are retained as well.
As a concrete example of retentive lenses, we present a domain-specific language for writing tree transformations.
We prove that the pair of $\textit{get}$ and $\textit{put}$ functions generated from a program in our DSL forms a retentive lens.
We study the practical use of retentive lenses by implementing in our DSL a synchroniser between concrete and abstract syntax trees for a small subset of Java, and demonstrate that Retentiveness helps to enforce the retaining of comments and syntactic sugar in code refactoring.

  % Central to our formulation of Retentiveness is the notion of $\textit{links}$, which are used to relate fragments of sources and views.

  % We show the usefulness of retentiveness by presenting examples in two different research areas: resugaring and code refactoring.
  % We prove that any program in our DSL generates a pair of retentive .


% Compared with the well-behaved lenses, the |get| function of a retentive lens is enriched to compute not only the view of the input source but also a set of \emph{links} relating corresponding parts of the source and the view.
% As the view is modified, this set of links is also updated to keep track of this correspondence that still exist between the original source and the modified view.
% The |put| function of the retentive lens is also enriched to take the links between the original source and the modified view as input, and a new property, which we call \emph{Retentiveness}, guarantees that those parts in the original source that still correspond to some parts of the modified view are retained in the right place in the updated source computed by |put|.

% only requiring that the whole source should be unchanged if the whole view is.

  % Researchers in the field of bidirectional transformations have studied data synchronisation for a long time and proposed various properties, of which $\textit{well-behavedness}$ is the most fundamental.

  % However, well-behavedness is not enough to characterise the result of an update (performed by $\mathit{put}$), regarding what information should be retained in the updated source.

\end{abstract}


%% 2012 ACM Computing Classification System (CSS) concepts
%% Generate at 'http://dl.acm.org/ccs/ccs.cfm'.
\begin{CCSXML}
<ccs2012>
<concept>
<concept_id>10011007.10011006.10011050.10011017</concept_id>
<concept_desc>Software and its engineering~Domain specific languages</concept_desc>
<concept_significance>500</concept_significance>
</concept>
</ccs2012>
\end{CCSXML}

\ccsdesc[500]{Software and its engineering~Domain specific languages}
% \ccsdesc[500]{Software and its engineering~General programming languages}

%% End of generated code


%% Keywords
%% comma separated list
\keywords{bidirectional transformations, asymmetric lenses}  %% \keywords are mandatory in final camera-ready submission


%% \maketitle
%% Note: \maketitle command must come after title commands, author
%% commands, abstract environment, Computing Classification System
%% environment and commands, and keywords command.
\maketitle


%include intro.lhs
%include framework.lhs
%include dsl.lhs
%include application.lhs
%include related.lhs
%include discussion.lhs
% %include concluding.lhs


%% Acknowledgments
\begin{acks}                            %% acks environment is optional
                                        %% contents suppressed with 'anonymous'
  %% Commands \grantsponsor{<sponsorID>}{<name>}{<url>} and
  %% \grantnum[<url>]{<sponsorID>}{<number>} should be used to
  %% acknowledge financial support and will be used by metadata
  %% extraction tools.
  % This material is based upon work supported by the
  % \grantsponsor{GS100000001}{National Science
  %   Foundation}{http://dx.doi.org/10.13039/100000001} under Grant
  % No.~\grantnum{GS100000001}{nnnnnnn} and Grant
  % No.~\grantnum{GS100000001}{mmmmmmm}.  Any opinions, findings, and
  % conclusions or recommendations expressed in this material are those
  % of the author and do not necessarily reflect the views of the
  % National Science Foundation.
  We thank Jeremy Gibbons, Meng Wang for useful discussions and comments.
  Yongzhe Zhang heled us to create a nice figure in the introduction of the last submittion, although the figure was later revised.
  This work is partially supported by the \grantsponsor{GS501100001691}{Japan Society for the Promotion of Science}{https://doi.org/10.13039/501100001691} (JSPS) Grant-in-Aid for Scientific Research (S)~No.~\grantnum{GS501100001691}{17H06099}.
\end{acks}


%% Bibliography
\bibliography{retentive}


%% Appendix
\appendix
%include new_proof.lhs

% % include generation.lhs
% % include appendix_proof.lhs

\end{document}
