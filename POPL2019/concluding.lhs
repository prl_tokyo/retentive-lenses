\section{Conclusions and Future Work}
\label{sec:conclusion}

In this paper we have introduced a semantic framework of (asymmetric) retentive lenses, and designed a DSL for writing retentive transformations between simple algebraic data types.
Retentive lenses enjoy a fine-grained local Hippocraticness, which subsumes the traditional global Hippocraticness.
The potential use of retentiveness has been illustrated in case studies on resugaring and code refactoring.
In the last part of the paper, we briefly discuss the potential future work on retentive lenses, regarding composability, generalisation, and parametricity.
% In this sense, every state-based well-behaved lens can be lifted to a (weak) retentive lens.
% composability of retentive lenses, which is also part of our future work.

% \begin{figure}[t]
% % \setlength{\mathindent}{0em}
% \begin{minipage}[t]{0.45\textwidth}
% %
% \begin{code}
% lens12 :: Lens A C
% lens12 = lens1 `comp` lens2

% get lens12 = get lens2 . get lens1

% put lens12 a c =
%   let  b   =  get lens1 a
%        b'  =  put lens2 b c
%   in   put lens1 a b'
% \end{code}
% \end{minipage}%
% %
% \begin{minipage}[t]{0.45\textwidth}
% \begin{code}
% lens_12 :: RetLens AC
% lens_12 = lens1 `comp` lens2

% get lens_12 = get lens2 . get_v lens1

% put lens_12 a c linkAC =
%   let  (b, linkAB)  =  get lens1
%        linkBC       =  (conv linkAB) linkComp linkAC
%        b'           =  put lens2 (b, c, linkBC)
%        linkBC'      =  get_v lens2 b'
%        linkAB'      =  conv (linkBC' linkComp (conv linkAC))
%   in   put lens1 (a, b', linkAB')
% \end{code}
% \end{minipage}
% \caption{Lens Compositions}
% \label{fig:composition}
% \end{figure}


\paragraph{Generalisation}
While our theoretical framework and DSL are designed for asymmetric lenses, we believe they can be extended properly to handle symmetric settings.
Many definitions in our framework are general and not limited to symmetric lenses: for example, the notion of regions, links and uniquely identifying property sets.
%
The signature for the |get| and |put| functions might be
\begin{align*}
& |get : (s elem S, v elem V , P_s ~ Q_v) -> (v' : V, P_s ~ Q_v')| \\
& |put : (s elem S, v elem V , P_s ~ Q_v) -> (s' : S, P_s' ~ Q_v)|
\end{align*}
In addition, the region model in this paper is tailored for algebraic data types (trees), with which many functional programming languages are equipped.
For bidirectional transformations in other languages, such as object-oriented languages, we may come up with other models that are more effective.


\paragraph{Parameterised Data Types}
Unlike combinator-based approaches in which many combinators are generic and parameterised, our tiny DSL does not support parametric data types, with generic bidirectional transformations.
To imitate functional dependent types, our DSL collects all the possible source and view types and assigns each a tag,
so that |get| and |put| functions (indexed by source and view types) can pattern match against these type tags and choose a correct branch.
This approach suddenly fails when facing parameterised types, for instance, |List a|, to which it is hard to assign a tag.
We believe that the DSL might be drastically changed, to handle parameterised types and produce generic lenses.

% \begin{code}
% 	data List a = Nil | Cons a (List a)

% 	List a <---> List b
% 	Nil        ~  Nil
% 	Cons x xs  ~  Cons x xs

% 	a      <---> b

% 	get (a) (b) :: List a -> List b
% \end{code}
