% !TEX root = retentive.lhs

\section{Introduction}
\label{sec:intro}

% ,Dayal:1982:CTU:319732.319740,Gottlob:1988:PUS:49346.50068
% Rendel:2010:ISD:2088456.1863525,Zhu:2016:PRP:2997364.2997369
% lutterkort2008augeas
% ~\cite{foster2007combinators,Bohannon:2008:BRL:1328438.1328487,barbosa2010matching,Hofmann:2012:EL:2103656.2103715,Pacheco:2014:BBF:2643135.2643141,ko2016bigul}
We often need to write pairs of programs to synchronise data.
Typical examples include view querying and updating in relational databases \cite{Bancilhon1981} for keeping a database and its view in sync,
text file format conversion~\cite{macfarlane2013pandoc} (e.g., between Markdown and HTML) for keeping their content and common formatting in sync,
and parsers and printers as front ends of compilers~\cite{Rendel:2010:ISD:2088456.1863525} for keeping program text and its abstract representation in sync.
%All these pairs of programs should satisfy some properties so that they can work correctly.
%
Asymmetric \emph{lenses}~\citep{foster2007combinators} provide a framework for modelling such pairs of programs and discussing what laws they should satisfy; among such laws, two \emph{well-behavedness} laws (explained below) play a fundamental role.
Based on lenses, various bidirectional programming languages and systems (\autoref{sec:related_work}) have been developed for helping the user to write correct synchronisers, and the well-behavedness laws have been adopted as the minimum, and in most cases the only, laws to guarantee.
In this paper, we argue that well-behavedness is not sufficient, and a more refined law, which we call \emph{retentiveness}, should be developed.

Let us first review the definition of well-behaved lenses, borrowing some of \varcitet{Stevens2008}{'s} terminologies.
Lenses are used to synchronise two pieces of data respectively of types $S$~and~$V$, where $S$~contains more information and is called the \emph{source} type, and $V$~contains less information and is called the \emph{view} type.
(For example, the abstract representation of a piece of program text is a view of the latter, since program text contains information like comments and layouts not preserved in the abstract representation.)
Here being synchronised means that when one piece of data is changed, the other piece of data should also be changed such that consistency is \emph{restored} among them, i.e., a \emph{consistency relation}~$R$ defined on $S$~and~$V$ is satisfied.
(For example, a piece of program text is consistent with an abstract representation if the latter indeed represents the abstract structure of the former.)
Since $S$~contains more information than~$V$, we expect that there is a function |get : S -> V| that extracts a consistent view from a source, and this |get| function serves as a consistency restorer in the source-to-view direction: if the source is changed, to restore consistency it suffices to use |get| to recompute a new view.
This |get| function, as reasoned by Stevens, should coincide with~|R| extensionally---that is, |s : S| and |v : V| are related by~|R| if and only if $|get|(s) = |v|$.%
\footnote{Therefore it is only sensible to consider functional consistency relations in the asymmetric setting.}
Consistency restoration in the other direction is performed by another function $|put| : S \times V \to S$, which produces an updated source that is consistent with the input view and can retain some information of the input source (e.g., comments in the original program text).
Well-behavedness consists of two laws regarding the restoration behaviour of |put| with respect to |get| (and thus the consistency relation~|R|):
% the correctness requires
\begin{align}
|get| |(put| |(s,v))| &= |v|
\tag{Correctness}\label{equ:correctness} \\
|put| |(s, get| |(s))| &= |s|
\tag{Hippocraticness}\label{equ:hippocraticness}
\end{align}
Correctness states that necessary changes must be made by |put| such that the updated source is consistent with the view,
and Hippocraticness says that if two pieces of data are already consistent, |put| must not make any change.
A pair of |get| and |put| functions, called a \emph{lens}, is \emph{well-behaved} if it satisfies both laws.

An important synchronisation problem that lenses appear to be able to address is code refactoring~\cite{fowler1999refactoring}.
%
Let us walk through a concrete scenario illustrated in \autoref{fig:text_and_ast}.
The user designed a \texttt{Vehicle} class and thought that it should have a \texttt{fuel} method for all the vehicles.
The \texttt{fuel} method has a JavaDoc-style comment and contains a \emph{for-each loop} (also known as an \emph{enhanced for loop}),
%\footnote{This \emph{for-each loop} is just an example of syntactic sugar, and its body is omitted.}
which can be seen as syntactic sugar and is converted to a standard \texttt{for} loop during parsing.
%
However, when later designing the subclasses, the user realises that bicycles cannot be fuelled, and decides to do the \emph{push-down} code refactoring, removing the \texttt{fuel} method from \texttt{Vehicle} and pushing the method definition down to subclasses \texttt{Bus} and \texttt{Car} but not \texttt{Bicycle}.
%
Instead of directly modifying the program text, most refactoring tools will first parse the program text into its abstract syntax tree (AST), perform code refactoring on the AST, and regenerate new program text.
%
The bottom-left corner of \autoref{fig:text_and_ast} shows the desired program text after refactoring, where we see that the comment associated with |fuel| is also pushed down, and the \emph{for-each} sugar is kept.
%
However, the preservation of the comment and syntactic sugar in fact does not come for free, as the AST, being a concise and compact representation of the program text, include neither comments nor the form of the original \texttt{for} loop.
%
So if the |print| function in \autoref{fig:text_and_ast} takes only the AST as input, it can only give the user an unsatisfactory result in which much information is lost (the comment and \emph{for-each} syntactic sugar), like the one shown in \autoref{fig:refactoring_results}.
This is a well-known problem~\citep{Li2006Comparative,fritzson2008comment,de2012algorithm}, and one way to remedy this is to implement the transformations between program text and ASTs using lenses~\cite{martins2014generating, Zhu:2016:PRP:2997364.2997369}, with |parse| as |get| and |print| as |put|, so that the additional information in program text can be retained by |print|/|put|.

\begin{figure}
\includegraphics[scale=0.6,trim={4cm 4cm 4cm 3cm},clip]{pics/push_down_refactoring.pdf}
\caption[An example of the \emph{push-down} code refactoring.]{An example of the \emph{push-down} code refactoring. \footnotesize{(For simplicity, subclasses \texttt{Bus} and \texttt{Bicycle} are omitted.)}}
\label{fig:text_and_ast}
\end{figure}



\begin{figure}
\includegraphics[scale=0.6,trim={8cm 11.7cm 8cm 3.1cm},clip]{pics/possible_refactoring_results.pdf}
\caption[An unsatisfactory result after refactoring]{An unsatisfactory result after refactoring, losing the comment and \emph{for-each} syntactic sugar.}
\label{fig:refactoring_results}
\end{figure}


This attempt, however, reveals a fundamental weakness about the definition of lenses: while lenses are designed to retain information, well-behavedness actually says very little about the retaining of information.
In our refactoring scenario, knowing that a lens between program text and ASTs is well-behaved does not preclude the unsatisfactory outcome in \autoref{fig:refactoring_results}:
Correctness only requires that the updated program text be parsed to the refactored AST, and this does not require the comment and syntactic sugar to be preserved; Hippocraticness has effect only when the AST is not modified, whereas the AST has been changed in this case.
The root cause of the unintended |put| behaviour is that Hippocraticness only requires the \emph{whole} source to be unchanged if the \emph{whole} view is, but this is too `global' as we have seen, and it is desirable to have a law that gives such a guarantee more `locally'.

To replace Hippocraticness with a finer-grained property, we propose an extension of the original lenses, called \emph{retentive lenses}, which can guarantee that if parts of the view are unchanged, then the corresponding parts of the source are retained as well.
Compared with the original lenses, the |get| function of a retentive lens is enriched to compute not only the view of the input source but also a set of \emph{links} relating corresponding parts of the source and the view.
As the view is modified, this set of links is also updated to keep track of this correspondence that still exists between the original source and the modified view.
The |put| function of the retentive lens is also enriched to take the links between the original source and the modified view as input, and a new law, which we call \emph{Retentiveness}, guarantees that those parts in the original source that still correspond to some parts of the modified view are retained in the right place in the updated source computed by |put|.
(In the refactoring scenario, after the AST is modified, the comment and the \emph{for-each} loop in the original program text are still linked to the moved methods in the AST, and will be guaranteed to be retained in the updated program text by Retentiveness.)
Notably, if the view is not modified and the link structure produced by |get| is kept intact, then Retentiveness will guarantee that the source is not modified by |put|---that is, Hippocraticness is subsumed by Retentiveness and becomes a special case.


In the rest of the paper, we assume that the grammar of a programming language is unambiguous so that the synchronisation between program text and its AST can be reduced to the synchronisation between the program text's concrete syntax trees (CSTs) and ASTs~\cite{Zhu:2016:PRP:2997364.2997369}.
This allows us to focus on a retentiveness framework for algebraic data types (i.e., trees), instead of unstructured data like program text (strings).
% This does not prevent us from tackling refactoring, though, because we can always find a unique CST that is isomorphic to a piece of program text conforming to an unambiguous grammar.
% so that the synchronisation between program text and ASTs can be reduced to the synchronisation between CSTs and ASTs~\cite{Zhu:2016:PRP:2997364.2997369}.
The following are contributions of this paper:
\begin{itemize}
  \item 
  % We give an intuitive description of regions that identify fragments of a tree, links between regions, triangular guarantee
  %We give an intuitive description of Retentiveness and retentive lenses using an example of synchronising arithmetic expressions (\autoref{subsec:intuitive_retentiveness} and \autoref{sec:intuitive_sketch}), through which we also informally introduce the notion of regions, links between regions, and the triangular guarantee satisfied by |put| with links.
  %Then we formalise the above-mentioned notions and extend well-behaved lenses to retentive lenses in \autoref{subsec:formal}, \autoref{subsec:ui}, and \autoref{subsec:laws}.
  We formalise the idea of links, retentive lenses and the law of Retentiveness (\autoref{sec:framework}).

  % propose a semantic framework of retentive lenses for algebraic data types (i.e., trees).
  % We first consider a simpler running example, which is the synchronisation between the concrete representation and abstract representation of arithmetic expressions.
  %
  % Central to our formulation of retentiveness is to decompose sources and views into data fragments, and use \emph{links} to relate those fragments.
  % A retentive |put| function accepts links as its additional input, and produces a new source in a way that it preserves all the data fragments from the old source attached to the links.
  % Retentiveness subsumes hippocraticness, and we show that any (state-based) well-behaved lens can be lifted to a retentive lens (\autoref{sec:framework}).
%
  \item
  %As a concrete example of retentive lenses, we present a domain-specific language (DSL) tailored for writing syntax tree transformations.
  %We give an overview of the DSL by writing the consistency relations for arithmetic expressions, and inspect the behaviour of the generated lenses by running |put| with different input links.
  %After presenting the DSL's syntax (\autoref{subsec:syntax}) and semantics (\autoref{subsec:dslsem}), we prove that the pair of $\textit{get}$ and $\textit{put}$ functions generated from any program in the DSL form a retentive lens (\autoref{mainthm}).
  We present a domain-specific language tailored for writing retentive lenses between trees (\autoref{sec:dsl}).
  %
  \item We study the practical use of retentive lenses by writing the synchronisation between syntax trees of (a small subset of) Java 8 and demonstrate that Retentiveness helps to retain comments and syntactic sugar after code refactoring. (\autoref{sec:application})
  %We also briefly discuss other potential application scenarios such as resugaring (\autoref{sec:application}).
%
\end{itemize}

In \autoref{sec:related_work}, we present related work regarding various alignment strategies for lenses, provenance and origin between two pieces of data, and operational-based bidirectional transformations (BX).
In \autoref{sec:conclusion}, we conclude the paper and discuss our choice of opting for triangular diagrams, composability of retentive lenses, and the feasibility of retaining code styles for refactoring tools in our framework.

