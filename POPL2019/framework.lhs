% !TEX root = retentive.lhs

\section{Retentive Lenses for Trees}
\label{sec:framework}

In this section, we will develop a definition of retentive lenses for algebraic data types.
After formalising some auxiliary concepts in Sections \ref{subsec:formal} and \ref{subsec:ui}, in \autoref{subsec:laws} we will make the following revisions to the definition of a classic lens \citep{foster2007combinators} (given in \autoref{sec:intro}):
\begin{itemize}
\item extending |get| and |put| to incorporate links---specifically, we will make |get| return a collection of consistency links, and make |put| take a collection of input links as an additional argument---and
\item replacing Hippocraticness with a finer-grained law \emph{Retentiveness}, which formalises the statement `if parts of the view are unchanged then the corresponding parts of the source should be retained',
\end{itemize}
and show that Hippocraticness becomes a natural consequence of Retentiveness.
Before giving the formal definitions, we will start by introducing a `region model' for decomposing trees, using the concrete and abstract representations of arithmetic expressions as a concrete example (\autoref{subsec:intuitive_retentiveness}), and also provide a high-level sketch of how retentive lenses work on top of this region model (\autoref{sec:intuitive_sketch}).


\begin{figure}[t]
\setlength{\mathindent}{0em}
\begin{minipage}[t]{0.45\textwidth}
%
\begin{code}
data Expr   =  Plus   Annot  Expr  Term
            |  Minus  Annot  Expr  Term
            |  FromT  Annot  Term

data Term   =  Lit    Annot  Int
            |  Neg    Annot  Term
            |  Paren  Annot  Expr

type Annot  =  String

data Arith  =  Add ^  Arith  Arith
            |  Sub ^  Arith  Arith
            |  Num ^  Int
\end{code}
\end{minipage}%
%
\begin{minipage}[t]{0.45\textwidth}
\begin{code}
getE :: Expr -> Arith
getE (Plus   _ e  t) = Add  (getE e) (getT t)
getE (Minus  _ e  t) = Sub  (getE e) (getT t)
getE (FromT  _    t) = getT t

getT :: Term -> Arith
getT (Lit     _ i  ) = Num i
getT (Neg     _ t  ) = Sub (Num 0) (getT t)
getT (Paren   _ e  ) = getE e
\end{code}
\end{minipage}
\caption{Data types for concrete and abstract syntax of the arithmetic expressions and the consistency relations between them as |getE| and |getT| functions in Haskell.}
\label{fig:running_example_datatype_def}
\end{figure}
%


\subsection{Regions} \label{subsec:intuitive_retentiveness}
Let us first get familiar with the concrete and abstract representations of arithmetic expressions---which our examples are based on---as defined in \autoref{fig:running_example_datatype_def}, where the definitions are in Haskell.
The concrete representation is either an expression of type |Expr|, containing additions and subtractions; or a term of type |Term|, including numbers, negated terms, and expressions in parentheses.
Moreover, all the constructors have an annotation field of type |Annot| for holding comments.
The two concrete types |Expr| and |Term| coalesce into the abstract representation type |Arith|, which does not include explicit parentheses, negations, and annotations.
The consistency relations between CSTs and ASTs are defined in terms of the |get| functions---|e :: Expr| (resp.~|t :: Term|) is consistent with |a :: Arith| exactly when |getE e = a| (resp.~|getT t = a|).


As mentioned in \autoref{sec:intro}, the core idea of Retentiveness is to use links to relate parts of the source and view.
For trees, an intuitive notion of a `part' is a \emph{region}, by which we mean a fragment of a tree at a specific location and whose data are described by a \emph{region pattern} that consists of wildcards, constructors, and constants.
%
For example, in \autoref{fig:swap_and_put}, the grey areas are some of the possible regions:
The topmost region in |cst| is described by the region pattern |Plus "a plus" _ _|~, which says that the region includes the |Plus| node and the annotation |"a plus"|, but not the other two subtrees with roots |Minus| and |Neg| matched by the wildcards.
This is one particular way of decomposing trees, and we call this the \emph{region model}.

\subsection{A High-Level Sketch of Retentive Lenses} \label{sec:intuitive_sketch}
With the region model, the |get| function in a retentive lens not only computes a view but also decomposes both the source and view into regions and produces a collection of links relating source and view regions.
We call this collection of links produced by |get| the \emph{consistency links} because they are a finer-grained representation of how the whole source is consistent with the whole view.
In \autoref{fig:swap_and_put}, for example, the light dashed links between the source |cst| and the view |ast = getE cst| are two of the consistency links.
(For clarity, we do not draw all the consistency links in the figure---the complete collection of consistency links should fully describe how all the source and view regions correspond.)
The topmost region of pattern $|Plus "a plus" _ _|$ in |cst| corresponds to the topmost region of pattern $|Add _ _|$ in |ast|, and the region of pattern $|Neg "a neg" _|$ in the right subtree of |cst| corresponds to the region of pattern $|Sub (Num 0) _|$ in |ast|.

As the view is modified, the links between the source and view are also modified to reflect the latest correspondences between regions.
For example, in \autoref{fig:swap_and_put}, if we change |ast| to |ast'| by swapping the two subtrees under |Add|, then the `diagonal' link relating the $|Neg "a neg" _|$ region and the $|Sub (Num 0) _|$ region can be created to record that the two regions are still `locally consistent' despite that the source and view as a whole are no longer consistent.
%
In general, the collection of diagonal links between |cst| and |ast'| may be created in many ways, such as directly comparing |cst| and |ast'| and producing links between matching areas, or composing the light red dashed consistency links between |cst| and |ast| and the light blue dashed `vertical correspondence' between |ast| and |ast'|.
How these links are obtained is a separable concern, though, and in this paper we will focus on how to restore consistency assuming the existence of diagonal links.
(We will discuss this issue again in \autoref{dis:tridia}.)

When it is time to put the modified view back into the source, the diagonal links between the source and the (modified) view can provide valuable information regarding what should be brought from the old source to the new source.
The |put| function of a retentive lens thus takes a collection of (diagonal) links as additional input and retains regions in a way that respects these links.
More precisely, |put| should provide what we might call the \emph{triangular guarantee}:
Using \autoref{fig:swap_and_put} to illustrate, if the link relating the |Neg "a neg" _| region in |cst| and the |Sub (Num 0) _| region in |ast'| is provided as input to |put|, then after updating |cst| to a new source |cst'|, the consistency links between |cst'| and |ast'| should include a link that relates the |Sub (Num 0) _| region to some region in the new source |cst'|, and that region should have the same pattern as the one specified by the input link, namely |Neg "a neg" _|\,, preserving the negation (as opposed to changing it to a |Minus|, for example) and the associated annotation.
%

\begin{figure}
\includegraphics[scale=0.6,trim={4cm 3.8cm 4cm 4.2cm},clip]{pics/swap_and_put.pdf}
\caption[The triangular guarantee.]{\footnotesize{The triangular guarantee. (Grey areas are some of the possible regions. Two light dashed lines between |cst| and |ast| are two of the consistency links produced by |get cst|.
When updating |cst| with |ast'|, the region |Neg "a neg" _| \,\! connected by the red dashed diagonal link is guaranteed to be preserved in the result |cst'|, and |get cst'| will link the preserved region to the same region |Sub (Num 0) _| of |ast'|. )}}
\label{fig:swap_and_put}

\end{figure}
% One way to obtain the diagonal link is to compose the consistency link and the light blue dashed `vertical correspondence'.
% The link between |cst| and |ast'| can be obtained as the result of composing the top two consistency links and the vertical correspondence.

\subsection{Formalisation of Links}\label{subsec:formal}

%\newcommand{\pfun}[0]{\;\dot{\rightarrow}\;}
\newcommand{\pfun}[0]{\mathrel{\ooalign{\hfil$\mapstochar\mkern5mu$\hfil\cr$\to$\cr}}}
\newcommand{\powerset}[1]{\mathcal P \left({#1}\right)}
\newcommand{\propsat}[3]{#1;#2 \models #3}
\newcommand{\myset}[1]{\left\{\;#1\;\right\}}
%\newcommand{\validlink}[3]{#1 \xleftrightarrow[]{#3} #2}
\newcommand{\validlink}[3]{#1 \leftrightarrow_{#3} #2}
\newcommand{\srcprop}[0]{\normalfont\textsc{SrcProp}}
\newcommand{\viewprop}[0]{\normalfont\textsc{ViewProp}}

Now we formalise our idea of links.
As described in \autoref{sec:intuitive_sketch}, a link relates two regions, and a region consists of a location---which can be a path describing how to reach the top of the region from the root---and a pattern describing the region's content.
One idea is to formalise a region as an element of the set $|Region| = |Pattern| \times |Path|$, where |Pattern| is the set of region patterns and |Path| is the set of paths, and formalise a link as an element of $|Region| \times |Region|$.
The exact representation of paths is not important, but to give examples later, let us assume that a path is a list of natural numbers that indicate which subtree to go into: for example, starting from the root of |cst| in \autoref{fig:swap_and_put}, the empty path |[]| points to the root node |Plus|, the path |[0]| points to |"a plus"| (which is the first subtree under the root), and the path |[2,0]| points to |"a neg"|.
The diagonal link in \autoref{fig:swap_and_put} is then represented as $((|Neg "a neg" _|\,, |[2]|), (|Sub (Num 0) _|\,, |[0]|))$.
%
This simple formalisation does not work too well, however.
%
Recall that if |put| receives an input link between the original source and the view, it will need to guarantee that the content of the source region of the link exists in the updated source, while the location of the region within the updated source can be different from its original location.
%
Therefore, with this definition of links, we cannot say that |put| retains the link between the updated source and the view, because the link describes the source region's location, which may not be retained.


The above discussion prompts us to use a more elaborate formalisation, in which links refer to locations only indirectly.
Assuming a set |Name| of identifiers, we define:
\begin{align*}
|Property| &= \{\, |HasRegion pat x| \mid x \in |Name| \wedge |pat| \in |Pattern| \,\} \cup \cdots \\
|Locations| &= |Name| \pfun |Path|
\end{align*}
where $|A| \pfun |B|$ is the set of partial functions from set~|A| to set~|B|.
To describe some regions in a tree, we first choose some locations in the tree and assign names to them by providing an element of |Locations|, i.e., a partial mapping from identifiers to paths that go from the root to the locations, and then provide a subset of |Property| describing what regions can be found at those locations---an element |HasRegion pat x| of |Property| is interpreted as saying that the location named~$x$ satisfies the property `there is a region of pattern |pat| at that location'.
%It is due to an alternative way of reading $(x, pat)$: region $x$ satisfies the \emph{property} `having region pattern |pat|'.
This interpretation makes our formalisation extensible because we can introduce more varieties of properties, for example, `a location is the root of a tree'; we will do so in \autoref{subsec:ui}.

\begin{example}
In |cst| in \autoref{fig:swap_and_put}, the two shaded regions are described by giving a (partial) location mapping $\{\,x \mapsto [], y \mapsto [2]\,\}$ and two properties |HasRegion (Plus "a plus" _ _) x| and |HasRegion (Neg "a neg" _) y|.
\end{example}

Instead of defining the set of links and then the set of `collections of links', we directly define the latter:
\[ |Links| = \powerset{|Property| \times |Property|} \times |Locations| \times |Locations| \]
where $\powerset{A}$ is the power set of |A|. For $(|ps|, |ms|, |mv|) \in |Links|$, |ps| is a set consisting of pairs of properties. Each pair |(HasRegion pat_x x, HasRegion pat_y y)| represents a link between a region of pattern |pat_x| at location~|x| in the source tree and a region of pattern |pat_y| at location~|y| in the view tree.
To know where the named locations are in the source and view trees, we look them up in the mappings $|ms|$ and $|mv|$ respectively.

\begin{example} \label{ex:link-collection-example}
The diagonal link between |cst| and |ast'| in \autoref{fig:swap_and_put} is represented as the link collection $(\{\,(|HasRegion (Neg "a neg" _) y|, |HasRegion (Sub (Num 0) _) z|)\,\}, \{\,|y| \mapsto [2]\,\}, \{\,|z| \mapsto [0]\,\})$.
\end{example}

A collection of links, i.e., an element of |Links|, may not make sense for given source and view trees---for example, a region mentioned by some link may not exist in the trees at all.
We should therefore characterise when a collection of links is \emph{valid} with respect to a source tree and a view tree: all properties on the source side of the links are satisfied by the source tree and all properties on the view side are satisfied by the view tree.

\begin{definition}[satisfaction]\label{def:sat}
For a tree |t|, $m \in |Locations|$, and a set of properties $\Phi \subseteq {|Property|}$, we say that $\propsat{|t|}{|m|}{\Phi}$ (read `|t| and |m| satisfy $\Phi$') exactly when
\[ \forall (|HasRegion pat x|) \in \Phi. \quad  x \in \dom (m) \;\wedge\; |sel|(|t|, m(x)) \text{ matches } |pat|\text{,}\]
where $|sel|(|t|, |p|)$ is a partial function that returns the subtree of |t| at the end of the path |p| (that starts from the root), or fails if path |p| does not exist in |t|. 
\end{definition}


\begin{definition}[valid links]\label{def:validlinks}
For a collection of links $|ls| = (|ps|, |ms|, |mv|) \in |Links|$, a source $|s| \in |S|$, and a view $|v| \in |V|$, we say that |ls| is \emph{valid} for |s| and |v|, denoted by $\validlink{|s|}{|v|}{|ls|}$, exactly when
\[ \propsat{|s|}{|ms|}{\srcprop(|ps|)} \quad\text{and}\quad \propsat{|v|}{|mv|}{\viewprop(|ps|)}, \]
where $\srcprop(|ps|) = {\myset{\phi_|s| \mid \exists \phi_|v|.\ (\phi_|s|, \phi_|v|) \in |ps|}}$ and $\viewprop(|ps|) = {\myset{\phi_|v| \mid \exists \phi_|s|.\ (\phi_|s|, \phi_|v|) \in |ps|}}$.
\end{definition}

\begin{example}
The link collection given in \autoref{ex:link-collection-example} is valid for |cst| and |ast'| because $|cst|; \{\,|y| \mapsto [2]\,\} \models \{\,|HasRegion (Neg "a neg" _) y|\,\}$ and $|ast'|; \{\,|z| \mapsto [0]\,\} \models \{\,|HasRegion (Sub (Num 0) _) z|\,\}$.
\end{example}



\subsection{Uniquely Identifying Property Sets}\label{subsec:ui}

Before we give the definition of retentive lenses, there is one more ingredient that we need.
One important aim of retentive lenses is to make Hippocraticness an extreme case of the triangular guarantee (\autoref{fig:swap_and_put}).
Recall that the triangular guarantee roughly says that a set of regions in the old source will also exist in the new source. 
Having introduced the set |Property|, an alternative and more abstract way of understanding this is that we are preserving the |HasRegion| properties: if an old source has a region matching a pattern |pat|, i.e., satisfies |HasRegion pat x|, then the new source should also have a region matching |pat|, i.e., satisfy the same property |HasRegion pat x| (where |x| may refer to a new location).
%More generally, there may be other kinds of property that we want to preserve.
The more properties we require the new source to satisfy, the more restrictions we impose on the possible forms of the new source.
In the extreme case, if a set of properties satisfied by the old source can only be satisfied by at most one tree, then preserving all the properties implies that the new source has to be the same as the old source.
We will call such set of properties \emph{uniquely identifying}, and require that the set of properties mentioned by the source side of the links produced by |get| be uniquely identifying.
%\zirun{get produces properties both for the source and view. Do we need to say that the source-side properties are uniquely identifying? (although the view-side properties in fact are.)}

\begin{definition}[uniquely identifying]
$\Phi \subseteq {|Property|}$ is \emph{uniquely identifying} exactly when
\[ \propsat{t}{m}{\Phi} \;\mathrel\wedge\; \propsat{t'}{m'}{\Phi} \quad\implies\quad t = t'\]
\end{definition}

|HasRegion| properties alone cannot be uniquely identifying, though: requiring that a source must contain a set of region patterns does not uniquely determine the source, however complete the set of region patterns is. This can be intuitively understood by analogy with jigsaw puzzles: a picture cannot be determined by only specifying what jigsaw pieces are used to form the picture (if the jigsaw pieces can be freely assembled); to fully solve the puzzle and determine the picture, we also need to specify how the jigsaw pieces are assembled---that is, the relative positions of all the jigsaw pieces. To make it possible for a subset of |Property| to be uniquely identifying, we introduce another two kinds of properties:
\begin{align*}
|Property| &= \{\, |HasRegion pat x| \mid |pat| \in |Pattern| \wedge x \in |Name| \,\} \\
           &\awa{{}={}}{{}\cup{}} \{\, |IsTop x| \mid x \in |Name| \,\} \\
           &\awa{{}={}}{{}\cup{}} \{\, |RelPos p x y| \mid p \in |Path| \wedge x, |y| \in |Name| \,\}
\end{align*}
%where |HasRegion|, |IsTop|, and |RelPos| are different constants serving as tags to distinguish different kinds of property.
The intended meaning of |IsTop x| is that the location named~|x| should be the root of a tree, and |RelPos p x y| means that we can reach the location~|y| from the location~|x| by following the path~|p|.
We formalise these intentions as two additional clauses of \autoref{def:sat}.

\begin{definition}[satisfaction extended]\label{def:sat2}
For a tree |t|, a location mapping $m \in |Locations|$, and a set of properties $\Phi \in \powerset{|Property|}$, we say that $\propsat{t}{m}{\Phi}$ exactly when
\begin{align*}
\forall (|HasRegion x pat|) \in \Phi. \quad  & \awa{x,|y| \in \dom (m)}{x \in \dom (m)}  \;\;\wedge\;\; |sel|(|t|, m(x)) \text{ matches } |pat| \\
\forall (|IsTop x|) \in \Phi. \quad          & \awa{x,|y| \in \dom (m)}{x \in \dom (m)}  \;\;\wedge\;\; m(x) = [] \\
\forall (|RelPos p x y|) \in \Phi. \quad     & x,|y| \in \dom (m)  \;\;\wedge\;\; m(x) \dblplus |p| = m(|y|)
\end{align*}
where `$\dblplus$' is path/list concatenation.
\end{definition}

\begin{example}
The following property set is uniquely identifying:
\begin{align*}
\{\, & |HasRegion (Add _ _) v|, |HasRegion (Sub (Num 0) _) w|, |HasRegion (Sub (Num 0) _) x|, \\
     & |HasRegion (Num 1) y|, |HasRegion (Num 2) z|, \\
     & |IsTop v|, |RelPos [0] v w|, |RelPos [1] v x|, |RelPos [1] w y|, |RelPos [1] x z| \,\}
\end{align*}
and |ast| in \autoref{fig:swap_and_put} is the only tree satisfying the above property set (with location mapping $\{\, |v| \mapsto [], |w| \mapsto [0], |x| \mapsto [1], |y| \mapsto [0, 1], |z| \mapsto [1, 1] \,\}$).
\end{example}

\subsection{Definition of Retentive Lenses}\label{subsec:laws}

Now we have all the ingredients for the formal definition of retentive lenses and can prove that Hippocraticness is implied by the definition.

\begin{definition}[retentive lenses]\label{def:retlens}
For a set |S| of source trees and a set |V| of view trees, a retentive lens between |S| and |V| is a pair of partial functions |get| and |put| of type
\begin{align*}
|get| &: |S| \pfun |V| \times |Links| \\
|put| &: |S| \times |V| \times |Links| \pfun S
\end{align*}
satisfying
\begin{itemize}
\item \emph{Link Completeness}: if $|get| |(s)| = (|v|, |ls|)$ and $|ls| = (|ps|, |ms|, |mv|)$, then $(|s|, |v|, |ls|) \in \dom(|put|)$ and
\begin{gather}\label{law:get}
\validlink{|s|}{|v|}{|ls|} \quad\mathop{\wedge}\quad \srcprop(|ps|) \text{ is uniquely identifying;}
\end{gather}

\item \emph{Correctness}: if $|put| |(s, v, (ps, ms, mv)) = s'|$, then $|s'| \in \dom(|get|)$ and
\begin{align}\label{law:correct}
|get| |(s') = (v, (ps', ms', mv'))| \quad \text{ for some |ps'|, |ms'|, and |mv'|;}
\end{align}

\item \emph{Retentiveness}: (continuing above) there is a permutation on names, i.e., a bijective $\sigma : |Name| \rightarrow |Name|$, such that
\begin{gather}\label{law:retain}
|ps| \subseteq |ps|'_\sigma  \quad\mathop{\wedge}\quad \forall x \in |NV|.\; |mv|(x) = |mv'|(\sigma^{-1}(x))
\end{gather}
where $|ps|'_{\sigma}$ is $|ps'|$ in which every name~|n| is replaced with $\sigma(n)$, and |NV| is the set of all names appearing in $\viewprop(|ps|)$.
\end{itemize}

\end{definition}

Modulo the link arguments, Correctness stays the same as the Correctness law of the original lenses.
Link Completeness requires that the collection of links computed by |get| be valid (\autoref{def:validlinks}), and that the links \textit{completely} record all the information of~|s| so that it is possible to retain the whole~|s| if the links are not modified and directly given to |put| as input.

The most important law is Retentiveness, which formalises the triangular guarantee.
The first conjunct says that every pair of properties of the input must be retained by |put|, modulo a one-to-one correspondence on names.
The possibility of such renaming has to be considered because $|ps|'$ is introduced by a subsequent |get| after |put| with |ps|, which may use different name bindings from |ps|.
The second conjunct further states that when a property (which can be |HasRegion|, |IsTop|, or |RelPos|) is preserved in the updated source, it still corresponds to exactly the same property on the view side, both the property itself and its evidence.
%\zirun{I think that this is stated by the first conjunct but not the second. The second conjunct seems to only care about the evidence.}
In the special case of a link whose two sides are both |HasRegion| properties, the first conjunct says the data contained in the source region must exist somewhere in the updated source, while the second conjunct requires that the piece of data in the updated source must still correspond to the same view region (considering both the data and location).
Consequently, Retentiveness can be specialised to the following triangular guarantee for regions, which was our initial motivation for developing retentive lenses.
\begin{proposition}[Triangular guarantee for regions]
Given |ls = (ps, ms, mv)|, $(|s|, |v|, |ls|) \in \dom(|put|)$, and
\[\phi_s = |HasRegion n spat| \quad \wedge \quad \phi_|v| = |HasRegion m vpat|\]
if $(\phi_s, \phi_|v|) \in |ps|$, letting $(|v|, (|ps', ms', mv'|)) = |get|(|put|(|s|, |v|, |ls|))$, then we also have:
\[(\phi_s, \phi_|v|) \in |ps|'_\sigma \quad \wedge \quad |mv|(m) = |mv'|(\sigma^{-1}(m)) \] 
for some renaming $\sigma$. Intuitively speaking, the region |spat| with name $n$ still exists in the updated source and corresponds to the same view region as in the input.
\end{proposition}

\begin{example}
In \autoref{fig:swap_and_put}, if we give |cst|, |ast'|, the diagonal link $(|HasRegion (Neg "a neg" _) y|,$ $|HasRegion (Sub (Num 0) _) z|)$, $ms = \{\, |y| \mapsto |[2]| \}$, and $|mv| = \{\, z \mapsto |[0]| \,\}$ to |put| and successfully obtain an updated source~|s'|, then $|get|(|s'|)$ will succeed.
Let $|get|(|s'|) = (|v|, (|ps'|, |ms'|, |mv'|))$.
Then we know that the diagonal link can be found in $|ps'|_\sigma$ for some renaming~$\sigma$; that is, we can find a consistency link $c = (|HasRegion (Neg "a neg" _) y'|,$ $|HasRegion (Sub (Num 0) _) z'|) \in |ps'|$ for some names $|y'| = \sigma^{-1}(|y|)$ and $z' = \sigma^{-1}(z)$.
Also we know $|mv'|(z') = |mv'|(\sigma^{-1}(z)) = |mv|(z) = |[0]|$, so the view region referred to by the link~|c| is indeed the same as the one referred to by the input link, and having $c \in |ps'|$ means that the region in~|s'| corresponding to the view region has to be of pattern |Neg "a neg" _| (since the link collection |(ps', ms', mv')| is valid for |s'|~and~|v|).
\end{example}

Now we show that Hippocraticness is a consequence of Retentiveness and Link Completeness with two simple lemmas.

\begin{lemma}[monotonicity]\label{lem:monotone}
If $\Phi \subseteq \Phi'$ and $\propsat{t}{m}{\Phi'}$, then $\propsat{t}{m}{\Phi}$.
\end{lemma}
\begin{proof}
Direct consequence of the definition of $\propsat{t}{m}{\Phi'}$ (\autoref{def:sat2}).
\end{proof}

\begin{lemma}[renaming]\label{lem:renaming}
For any bijective $\sigma : |Name| \rightarrow |Name|$, if $\propsat{t}{m}{\Phi}$, then $\propsat{t}{m\circ\sigma^{-1}}{\Phi_\sigma}$ also holds, where $\Phi_\sigma$ is $\Phi$ with all names in it transformed with $\sigma$.
\end{lemma}
\begin{proof}
Direct consequence of \autoref{def:sat2}. 
\end{proof}

\begin{theorem}[Hippocraticness]\label{thm:hippo}
For a retentive lens,
\[ |get| |(s) = (v, ls)| \quad\Rightarrow\quad |put| |(s, v, ls) = s| \]
\end{theorem}
\begin{proof}
Let $|s' = put| |(s, v, ls)|$ and $|get| |(s') = (v', ls')|$. If |ls = (ps, ms, mv)| and |ls' = (ps', ms', mv')|, then we have $|ps| \subseteq |ps|'_\sigma$ with a renaming function $\sigma$ by Retentiveness (\ref{law:retain}).
Also by the first conjunct of Link Completeness (\ref{law:get}), we have $\validlink{|s|}{|v|}{|ls|}$ and $\validlink{|s'|}{|v'|}{|ls'|}$, which imply $\propsat{|s|}{|ms|}{\srcprop(|ps|)}$ and $\propsat{|s'|}{|ms'|}{\srcprop(|ps'|)}$.
By \autoref{lem:renaming}, we have $\propsat{|s'|}{|ms'|\circ\sigma^{-1}}{\srcprop({|ps|'_\sigma})}$.
Since $|ps| \subseteq |ps|'_\sigma$, with \autoref{lem:monotone}, we also have $\propsat{|s'|}{|ms'|\circ\sigma^{-1}}{\srcprop({|ps|})}$. 
Because Link Completeness requires $\srcprop({|ps|})$ to be uniquely identifying, we have |s' = s|.
\end{proof}

Finally we note that retentive lenses are an extension of the original lenses: every well-behaved lens between trees can be directly turned into a retentive lens (albeit in a trivial way).
\begin{example}
Given a well-behaved lens defined by $g : |S| \rightarrow |V|$ and $p : |S| \times |V| \rightarrow |S|$, we define $|get| : |S| \pfun |V| \times |Links|$ and $|put| : |S| \times |V| \times |Links| \pfun |S|$ as follows: 
\begin{gather*}
|get|(|s|) = (g(|s|), |trivial|(|s|, g(|s|))) \\
|put|(|s|, |v|, |ls|) = p(|s|, |v|)
\end{gather*}
where
\begin{align*}
|trivial|(|s|, |v|) =(\{\;&(|HasRegion| \; |ToPat|(|s|) \; n , |HasRegion| \; |ToPat|(|v|) \; m), \\
													&					(|IsTop| \; n, |IsTop| \; m)\;\}  \\
                       & \hspace{-1em} , \myset{n \mapsto |[]|}, \myset{m \mapsto |[]|})
\end{align*}
|ToPat| turns a tree into a pattern completely describing the tree, and $\dom(|put|)$ is restricted to $\myset{(|s|, |v|, |ls|) \mid \validlink{|s|}{|v|}{|ls|} \text{ and } |ls| = |trivial|(|s|, |v|) \text{ or } (\emptyset, \emptyset, \emptyset)}$. The idea is that |get| generates a link collection relating the whole source tree and view tree as two regions, and |put| accepts either an empty collection of links or the trivial link produced by |get|.
The trivial link generated by |get| is apparently valid and uniquely identifying so that Link Completeness is satisfied.
Correctness holds because the underlying $g$ and $p$ is a well-behaved lens.
When the input link of |put| is empty, Retentiveness is satisfied vacuously; when the input link is the trivial link, Retentiveness is guaranteed by Hippocraticness of $g$ and $p$. Thus |get| and |put| indeed form a retentive lens.
\end{example}
