We thank all the reviewers for their valuable comments and effort spent.

# Main concerns

> The link's approach is complex. It's not clear whether this complexity is inherent or incidental.


Part of the complexity comes from the fact that Hippocraticness is subsumed as a special case of Retentiveness. Otherwise the notion of *uniquely identifying* and two other kinds of properties *IsTop* and *RelPos* will disappear. In this way, the theorem will be simplified a lot with the cost of reintroducing Hippocraticness. But we are not sure whether this complexity is necessary or avoidable (in some other way).

--------

> Some parts of the paper were hard to follow. The presentation of the DSL could be improved.

As suggested by the reviewers, a type system (or other formal presentation) will help to explain and check the restrictions. We will improve this part in the next version.

--------

> The implementation does not appear to have been rigorously evaluated. The case study involving a Java refactoring tool ends rather abruptly.

We did not implement a refactoring tool of a subset of Java 8; rather, we implemented as retentive lenses the transformations between the CSTs and ASTs of the subset, to show that the result of refactoring on an AST can be reflected back to its CST with guarantees such as the retention of comments. Additionally, the view changes on ASTs are not limited to refactoring. For instance, it may be some evaluations (done by an interpreter) as briefly explained in the last paragraph of section 4.3. 

Thus the main concern of our evaluation is to show that it is sufficient to implement those transformations using our DSL and we highlighted several consistency relations among the total 70.
However, as pointed out by the reviewers, our evaluation is restricted to the example in the introduction, and several small ones from Section 3. This is because that we did not implement a tool for generating (vertical) links between a view and its modified version and did not have much test data. We definitely need to do more serious evaluation in the future.


--------

> The rule that turns "for each" loops into for loops seems to be broken.

It is broken. We forgot to take the fresh name constraint into account. Any transformation requiring fresh names cannot be done in our DSL. (Later we find that a fresh name is a new piece of information. So an AST incorporating this new information cannot be treated as a view of its CST.)

--------

> How to appropriately update links when views are changed is asserted to be an orthogonal problem ... Relying on handcrafted links is unsatisfactory and does not scale.

That is true. But we discussed how to ‘update links’ in the last paragraph of section 2.2 and in section 6.1: in practice the updated links can be obtained from the composition of consistency links and vertical links (between views). 
Methods for obtaining vertical links are also slightly discussed in section 6.1. (We should have included the composition operator in the appendix.)



# Detailed response

## For reviewer A

> The question of how to appropriately update links when views are changed is asserted to be an orthogonal problem, but these are absolutely necessary when using this approach. Relying on handcrafted links is unsatisfactory and does not scale.

Answered above.

> The implementation does not appear to have been rigorously evaluated-- the only use case is an example from the introduction, and it depends on a handcrafted set of links between concrete and abstract syntax trees.

Answered above. (There are several other small examples included in the submitted code. They are tree transformations appeared in section 3.)

> Section 2.1: there is no discussion of the FromT constructor (which I believe is used for comments)

The `FromT` constructor is for converting a `Term` into an `Expr`. This comes from the grammar (production rules) of the arithmetic expression. We represent

```
Expr ->  Expr '+' Term  
      |  Expr '-' Term 
      |  Term
```
by

```
data Expr = Plus ...
          | Minus ...
          | FromT ...

```


## For reviewer B

> The formalization of regions, links, and properties adds significant notational clutter. It's not clear whether this complexity is inherent or incidental.

Answered above.

> While the evaluation is impressive, the case study involving a Java refactoring tool ends rather abruptly.

Answered above.

> The formalization of regions, properties, links, and locations is quite complex -- more than what seems necessary for a simple concept. Perhaps part of the complexity comes from the fact that the set of links is an output of the Get function. What would be lost if these notions and associated conditions were instead fixed -- e.g., perhaps by the type of the lens? Would this streamline the development?

We do not fully understand ‘if these notions and associated conditions were instead fixed -- e.g., perhaps by the type of the lens?’
We guess that the reviewer suggests that we should make the type of links only dependent on the types of the source and view, instead of making it dependent on the inhabitants of the types.
We do not know if this is the correct direction, but will try it in the future.


> The presentation of the DSL in Figure 7 could be improved. For example, it would be nice to capture the syntactic restrictions in a formal type system rather than describing them in prose.

Answered above.

> In addition, some aspects of the grammar are not explained -- e.g., what is 'InterConv'? I guess this is interconvertibility, but it's unclear why this is needed in the grammar.

'InterConv' means interconvertibility, at L631 we slightly mentioned that ‘... the user can describe (override) the conversions between interconvertible data types’ and explained it in more detail in the second bullet of the syntactic restrictions.
*Necessary interconvertible functions are automatically generated, but we provide the flexibility to let the user override it.* For example, the user may want to add certain annotation when a term (of type `Term`) is converted to an expression (of type `Expr`): `FromT "my annotation" (Neg "" (Lit 1)) `.

> The case study involving the Java refactoring tool sounds like a non-trivial achievement. Have the side conditions on the DSL been checked mechanically, to ensure that the program satisfies the behavioral law? Overall, this is a strong endorsement for the design, although it's a bit of a shame that the experience to date is limited to a few examples.

Parts of them are checked mechanically, such as restrictions on non-linear patterns; parts of them are not, such as the ‘no infinite recursion’. Currently, some bad DSL programs will go into a loop at runtime. For the DSL program on (a subset of) Java, we tested it on certain points as mentioned in Section 4.3. More mechanical checks will be added after we designing a type system as mentioned above. 


## For reviewer C

> Fairly complicated. Correct?

Answered above.

> Not compositional.

It is a pity that our monolithic DSL (currently) is not compositional. However, our evaluation of writing transformations for a subset of Java 8 does not prove it to be a drawback.
We hope to see good examples proving that combinators' approach beat generators' approach in bx. 

Composability in unidirectional programming is considered essential, while in bidirectional programming it is not that useful as one might think, especially for bx that is not very well-behaved.
The behaviour of the put direction of a complex composed lenses are difficult or impossible to reason about. A year ago we tried to compose several basic list lenses into a big one, whose get direction is the list's maximum segments sum. It is hard to predicate what it does in the put direction, except that it fails in most cases as expected. We hope to see (and make) breakthrough in composability in bx.


> Efficiency?

We did not evaluate the efficiency of our implementation because we think it is not important for demonstrating retentive lenses.

> Some parts of the paper were hard to follow

Answered above. (We guess it is the semantics part.)

> Definition of Links: Does it always make sense to use the same kinds of patterns for the source and the view? It seems to me as if this is rather clumsy in the case of your running example (at least in a typed setting).

We can (and should) use different patterns for the source and view. We used the same pattern for both source and view due to some ‘laziness’.


>Definition 2.9:

> * Perhaps you could note that this definition is satisfied by completely undefined functions.

> * Why is there no requirement that the input to put is valid (s ↔_(ps,ms,mv) v)? I'll assume that you forgot to include this requirement.
> * Would anything break if you changed the definition of Links in such a way that the set of pairs of properties was split up into two sets of properties? The only part of your definition that is affected by this change is the first clause of retentiveness. I assume that the answer is yes, but I'd like to see a good example.

The theorem is formalised on partial functions. It is the DSL's responsibility to make the functions defined on most cases (in practical use).

For the third question: not all valid properties can be linked. For example, `HasRegion (Neg "" _) y` should not be linked with `HasRegion (Add _ _) z`. At L835, we mentioned that the collection of links to `put` should be a subset of possible links generated by `get`. (`get` will not link a negation to an addition.)
Unzipping the property tuples will need additional efforts for (re)linking properties.

> Line 456: Here "mv(z) = [1]" should be "mv(z) = [0]".

Thank you.


> Example 2.15:

> * I assume that "(g(s),trivial(s,v))" should be "(g(s),trivial(s,g(s)))".
> * I don't see how Hippocraticness helps with retentiveness. We have get(put(s,v,trivial(s,v))) = get(p(s,v)) = (g(p(s,v)),trivial(p(s,v),g(p(s,v)))) = (v,trivial(p(s,v),v)). The tricky part is establishing that (HasRegion ToPat(s) n,HasRegion ToPat(v) m) is equal to (HasRegion ToPat(p(s,v)) σ(n),HasRegion ToPat(v) σ(m)) for some σ (which could be the identity function). I don't see why ToPat(s) is necessarily equal to ToPat(p(s,v)).

For the first bullet, yes. It is a typo.

For the second bullet, in fact we have

```
  get(put(s, v, trivial(s,v))) 

= get(put(s, g(s), trivial(s,g(s)))) 

= get(p(s, g(s))) 

= get(s)

= (g(s), trivial(s,g(s)))

= (v, trivial(s,v))
```
Because if the trivial link exists, it means that the view is consistent with the source since the link has patterns completely describing the source and the view. That is where Hippocraticness helps: `p(s,v) = p(s, g(s))` for the well-behaved lenses `g` and `p`.


> Figure 6:

> * There is nothing that indicates that your DSL should insert a Paren constructor when constructing cst₂. Please explain why it does this.
> * If cst₃ is constructed "from scratch" (with no links?), then I wonder why the two Sub constructors are treated differently.

The reviewer can find explanations in the *Syntactic Restrictions* below Figure 7. The Paren constructor is added to make the type correct. According to retentiveness, the `Minus` structure is preserved in the new source. However, the type of `Minus` is `Expr` while at that position data of type `Term` is required. So that `Minus` is wrapped into `Paren` for making the type correct. This is guaranteed by *interconvertible datatypes*. While the converting functions are automatically generated, users can also override it using the `instance InterConv ...` syntax in Figure 7.
See also ‘what is 'InterConv'? I guess this...’ asked by reviewer A.
Requiring some data sharing the same view to be interconvertible and automatically generating the converting functions are indispensable for practical use.

The semantics of `put` describes this behaviour (in section 3.3).
The two `Sub` constructors are treated differently because of the type requirement. The second subtree of `Plus` requires data of type `Expr` while the third subtree of `Plus` requires data of type `Term`. So different consistency relations guided by the type requirements are tried.



> §3.2:

> * Can a data type have zero constructors?
> * Why is there no requirement that source and view patterns contain exactly the same variables? I'm assuming that you forgot to include this requirement, because otherwise your definition of get does not make sense to me.

No. We have not considered data type with zero constructors...

There should be such requirement and of course this condition is checked in the implementation. Sorry that we forgot to include many such (relatively simple) restrictions in the paper. Will add in the next version (with a type system).

> Lines 680-: I'm not sure I've understood exactly how this restriction is defined. Please give a formal definition. (And I think your use of a semicolon is rather confusing.)

Yes, we will design a type system. Roughly speaking, if two data having the common parent (may be indirect) in a tree and can be mapped to the same view. They are required to be interconvertible.


> §3.3.1:

> 1. Should your algebraic types be read inductively? Coinductively? As solutions to some kind of domain equations?
> 2. When you write "all the algebraic data types defined in our DSL", do you mean "all the algebraic data types definable in our DSL"?
> 3. What does "the set corresponding to the type of v ∈ Vars(p)" mean?
> 4. You write that "Path(p, v) is the path of v in p" (my emphasis), but you allow non-linear patterns.
> 5. You claim that decompose is a total function, but you do not explain how it is defined when the pattern does not match.
> 6. You claim that reconstruct is a total function, but you do not explain how it is defined when the pattern contains wildcards.

1. It should be read inductively.
2. Yes. When coming to a user's program in our DSL, it also means that ’all the algebraic data types defined by the user in his program’. Sorry for the confusion.
3. `p` is a pattern containing variables. Assume `x` is in `p`, now we want to ask the type of the subtree marked by `x` in `p`. For example, the type of `x` in `Sub x _` is `Expr`. Here we use the word ‘set’ instead of ‘type’.
4. We forgot to include the details about how to handle non-linear patterns. The ‘non-linear variable usage’ is converted to ‘linear variable usage’ with some tracking information before code generation. For example, given pattern `Mul x 2 ~ Plus x x`, it is converted to a pattern like `Mul xL 2 ~ Plus xR1 xR2` with additional tracking information stating that both `xR1` and `xR2` are related with the same `xL`. 
5. The definition of `decompose` should be partial. The pattern is tested using `isMatch` before passed to `decompose`. It is always defined when `isMatch` returns `True`.
6. The definition of `reconstruct` should be partial. But the actual argument passed to it is either a view pattern that does not have wildcards at all, or a source pattern with wildcards filled by `fillWildcards`.

> §3.3.2:

> 1. Why have you renamed Links to LinkSet?
> 2. With respect to what should x and y be fresh?
> 3. It is not quite clear where you quantify over st and vt.
> 4. How do you handle non-linear patterns?
> 5. Here you use ε, above you used [].
  
1. Sorry. It is a mistake. (We failed to replace all the occurrence of LinkSet with Links.)
2. ‘`x` and `y` fresh’ means that they are variables (of type `Name`) that are not used in any recursive call in the process of get.
3. `st` and `vt` are introduced by set comprehension, and constrained by the conditions after the vertical bar (|). For example, {x | x ∈ R} uses the same syntax.
4. We forgot to include the details about how to handle non-linear patterns. The ‘non-linear variable usage’ is converted to ‘linear variable usage’ with some tracking information before code generation. For example, given pattern `Mul x 2 ~ Plus x x`, it is converted to a pattern like `Mul xL 2 ~ Plus xR1 xR2` with additional tracking information stating that both `xR1` and `xR2` are related with the same `xL`.
5. We use different annotation for empty paths in the hope that above (in section 2) is a concrete example on list while here the structure does not need to be limited to list. But it seems that we had better to use the same annotation.



> §3.3.3:
> Should the type of put_SV start with S rather than Tree?

No. One tricky part of `put` is that the type of the generated source can be different from the input source. The first argument of `put` is the source from which the links fetch data, so it is of type `Tree`.

> Is put_SV non-deterministic?

No, it is definitely deterministic.

> How do you handle non-linear patterns?

Answered above. Additional check is performed for non-linear patterns, to make sure that in the `put` direction, they generate the same source (fragment).


> What does "Names appeared in p are defined in (ms, mv')" mean? I can imagine several interpretations.

Here `p` is a proposition and `ms` and `mv'` are functions mapping names to their paths. We require that the names (variables) appeared in `p` are defined for both `ms` and `mv'`.

> How is fillWildcardsWithDefaults defined? If types are allowed to be empty this could be tricky.

No, types are not allowed to be empty. All the types need to have constructors.

> Type error: TypeOf(spat_k).

Thank you. We forgot to say that this `TypeOf(spat_k)` is an overloaded version.
In some pattern such as `FromT _ t`, there is only one variable, so the name of the variable is not important and can be omitted. Here is one usage of the overloaded version.


> You write that "put chooses the source region whose path is the shortest", but I suppose that there could be several such regions.

In the implementation the definition of ‘shortest path’ also takes the order of the subtree into account. For instance, path [0,1] is considered shorter than path [1,1], although both of them are of length two.



> You write that "Wildcards in spat can be captured by wildcards or variables but nothing else". What about variables in spat?

Here `spat` is a region pattern and there is no variable in `spat`. To be simple (but not fully faithful to the implementation), for instance, given (a case of consistency relation), `Plus _ x y ~ Add x y`, then `spat` is the pattern converted from `Plus _ x y` where all the subtrees denoted by variables are erased: `Plus _ _ _`.



> You write that "the linked source region does not necessarily have type S". Please give an example.

There are several examples, such as `cst'` in Figure 4. the linked  source region is of type `Term` but here the required type is `Expr`. So `FromT` is inserted to make type correct.


> You give the domain of put. Does your implementation check these conditions? Checking them could be expensive, but not checking them would be incorrect.

Some of them are not checked. For instance, the statement ‘there is no infinite recursion’ is not checked at all. But many of them are checked either by our DSL or by GHC. For instance, if the region pattern of a link does not match the given data at the given path, the program fails and GHC will raise a runtime error. In this case, incorrect results will not be produced.


> When you write "vpat_k = t", do you mean that t is an arbitrary variable?

Perhaps. `t` just means a (bare) variable pattern here.


> I don't understand the part about "no infinite recursion":

> * Is the "n" related in any way to the "i"? If not, how do you know that you will encounter this n?

> * You write that "we require that for any subtree t of v, there are always an n ∈ N and a rule spat ∼ vpat in S_n ↔ V such that vpat matches t and vpat is not a bare variable pattern". Because v is a subtree of v this means that this should apply to v itself, right?

* The quantifier over `n` is existential quantifier. Whether this `n` exists or not can be statically checked against the DSL program written by the user.
* Yes. We want `v` and all of its subtrees satisfy this condition.

Take the DSL in Figure 5 for example. Here we may have an infinite sequence of source types: `Expr, Term, Expr, Term, Expr ...`

Take `i = 1` and `v = Mul (Num 1) (Num 2)` for example.

Now S<sub>1</sub> = `Expr`. The consistency relation `Expr ↔ Arith` is tried. But `v` fails to match `Add _ _` and `Minus _ _`. The bare variable pattern `t` is matched (in the case `FromT _ t ~ t`).

`TypeOf(FromT _ t, t)` returns `Term`. So S<sub>2</sub> = S<sub>1+1</sub> = `Term` and put proceeds with `Term ↔ Arith`.
There is a rule `Times _ x y ~ Mul x y` in which the `vpat` is matched by `v` and not a bare variable pattern.
In this case, the size of the view successfully decreases.
So here `n` is just 2 (and equal to `i+1`).

In general, this chain maybe long, especially if there are arithmetic operations of different priorities. (There can be `*`, `+`,`&`,`==`,`<<` ...)



> The for each example looks broken to me. What if (say) "exp" is "i"?

It is broken. This is our fault. We forgot to take the fresh name constraint into account. Any transformation requiring some fresh name cannot be done in our DSL. Later we find that a fresh name is a new piece of information. So that an AST incorporating this new information cannot be treated as a view of its CST.

> You write that "the links approach has an advantage that it leaves ASTs clean so that we do not need to patch up the compiler to deal with tags". However, if the goal is to "resugar" terms that have been modified by the compiler (or something like a partial evaluator/normaliser for open terms), then the links have to be updated along with the terms, right? I suspect that this is quite a bit easier if everything is present in the abstract syntax tree.

It is easier to carry tags in the abstract syntax tree and modifying the compiler for achieving resugaring than using links. However, the design philosophy of abstract syntax tree is to let it be as abstract as possible. In this sense, including tags is a violation of the design philosophy. Additionally, including tags only solve the problem of retaining syntactic sugar but cannot solve the problem of retaining other stuff, such as comments. In order to let an AST suitable for all applications, then the AST ends up carrying everything as the reviewer mentioned.


> I find the discussion of containers on page 22 a little hard to follow. Why would you parametrise Expr and Term in the given way in order to define the types as containers? Containers are typically used to represent functors, and one can represent some inductive types as least fixpoints of containers (using W-types). However, I am not familiar with lenses based on containers, and I don't know what you are trying to accomplish.

At first, we have lenses on lists and trees, etc. Later researchers generalise lenses to operate on containers. ‘In the container framework, a data structure is decomposed into a shape and its content; the shape encodes a set of positions, and the content is a mapping from those positions to the elements in the data structure.’ (From section 5.1.)
For example, given a list [a,b,c,d], its shape is its length 4. The content is a function mapping 0 to a, 1 to b, 2 to c, and 3 to d.

(Rephrased from section 5.1.) There is an assumption that a change on *content* should only result  a change on *content* but not any change on *shape*. We want to argue that this assumption cannot solve (does not fit the setting of) many real problems, by using the running example of this paper.
In particular, the change on a (view's) content: mapping the position containing number 0 to number 1 (i.e. `Sub (Num 0) (Num 100)` becomes `Sub (Num 1) (Num 100)`) should result in a change on (source's) *shape*. Because the negation need to be changed to a subtraction. Subtraction and negation do not have the same shape. (For a tree, its shape is usually its skeleton.)