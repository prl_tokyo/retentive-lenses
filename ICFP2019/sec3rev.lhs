\documentclass[a4paper]{article}

%include polycode.fmt

\title{Revision on Section 3}

\begin{document}
\maketitle

When I was reading Meng and Jeremy's ICFP 2011 paper, investigating how they treated the well-alignment conditions, I noticed they restricted |get| functions to be |fold| on trees.
In our DSL, |get| (and |put|) functions are not exactly |fold|s, but pretty close: |get| finds an $|spat|_i$ matching the top portion of the source, then recursively processes subtrees, then combines the results of the subtrees.
It is not a fold on the structure of tree, but a fold on the structure of regions composed together.

Then I came up with an idea of formalising our totality conditions:
(1) we define two inductive families that characterize total arguments to |get| and |put|, i.e., arguments satisfying well-alignment conditions,
(2) we define total functions |get| and |put| on those inductive families,
(3) we define partial |get| and |put| on all arguments, which try to convert arguments (|s| or |(s, v, ls)|) to values of the newly defined types and invoke their |put|/|get|, or fails if the conversion fails.

\section{Total arguments}
Specifically, for |get|, we define (I am using Agda's syntax (maybe with syntax errors)):
\begin{code}
data GAligned (s : DSLType) : Set where
  region : (p : SPat s)  -> ((v : Vars p) -> GAligned (TypeOf p v)) 
                         -> GAligned s
\end{code}
where |DSLType| is the set of all types defined in the DSL program, |SPat s| is the set of all source patterns of
\[ s \leftrightarrow v = \{|spat|_i \sim |vpat|_i \mid 1 \leq i \leq n\}, \]
|Vars p| is the set of all variables in pattern |p|, and |TypeOf p v| is the type of variable |v| in pattern |p|.

Thus for type |S|, type |GAligned S| is the set of trees that are (constructively) well-aligned with respect to the DSL program.
Then |get| becomes total on |GAligned S| and is a |fold|.

The case for |put| is similar:
\begin{code}
data PAligned (s : DSLType) (st : s) (v : DSLType) : Set where
  link :  -> (r : Rule s v)
          -> ((v : Vars (vpat r)) -> PAligned s st (TypeOf (vpat r) v))
          -> (spath : Path) -> (HasRegion st spath (spat r))
          -> PAligned s st v
  nolink :  -> (r : Rule s v)
            -> ((v : Vars (vpat r)) -> PAligned s st (TypeOf (vpat r) v))
            -> WildcardHoles (spat r)
            -> PAligned s st v
\end{code}
where |Rules s v| is just the set $\{|spat|_i \sim |vpat|_i \mid 1 \leq i \leq n\}$, |HasRegion st spath spat| is a proof that tree |st| has pattern |spat| at path |spath|.

An element |PAligned s st v| is actually a view tree of type |v| built by adding linked regions (linked to regions in tree |st| of type |s|) or not linked regions step by step.
Thus |put| also becomes a total |fold| on |PAligned s v st|.

\section{Total |get| and |put|}

\section{Partial |get| and |put|}

\end{document}
