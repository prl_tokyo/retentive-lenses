\section{Proofs}
\label{sec:appendix}
In this section, we prove \autoref{thm:semsound} and \autoref{thm:semsound2} based on the compiler given in \autoref{sec:generation}.
The core of the proof is showing that the generated |get| and |put| (without second order properties as input) retain all links of regions and satisfy \textit{PutGet}, i.e., the first two properties of \autoref{thm:semsound2}.
This part is showed in detail in the rest of this section.

The key idea of our proofs is to make inductions on the height of the view:
%
Given a source |s| of type |st| and a view |v| of type |vt|, and correspondence links |hls| between source |s| and view |v|,
we assume that Correctness and Retentiveness already hold for all the views |v'| of height less than |h|, then we prove that the two properties also hold for the view |v| of height |h| using induction hypotheses.
The structure of the proofs follow the structure of |put| functions, and are thus divided into two main parts, depending on whether there are input links connected to the top of the view.


% In the following proof, we assume that we will synchronise data between source type |st| and view type |vt|, where |st| is the type of the new source, which in general can be different from the type of original source.




\subsection*{Case 1: When There Is No Top-Level Link}
\label{proof:case_no_link}
We first presents some prerequisite and lemmas, which are used in the proof later.



\begin{prereq}
\label{prereq:getInj}
\textnormal{As mentioned in \hyperref[paragraph:make_inj]{Making Type Correct}, the function |mkInj| is supposed to make changes that are discarded by get (for establishing Correctness) and do not disrupt retentiveness already established for the subtrees.}
\begin{align*}
& |get_v s                 =          get_v (mkInj _ _ s)| \\
& |fst linkComp get_l s    subseteq   fst linkComp get_l (mkInj _ _ s)|
\end{align*}
\end{prereq}

\paragraph{Explanation}
The first equation is exactly `make changes that are discarded by |get|'.
For the second inequation, since |mkInj _ _ s| contains a subtree which is exactly |s|, |get_l| on it should produce more links than on |s|.
In addition, since |mkInj _ _ s| contains a subtree which is exactly |s|, the links of |get_l (mkInj _ _ s)| cover the links of |get_l s|, if we do not consider the proofs (paths) on the source side of the links.


\begin{prereq}
\textnormal{|mkInj| is an identity function if the types lifted from and converted to are the same.}
$$ |mkInj toTy fromTy t || toTy == fromTy = t| $$
\end{prereq}

\vspace{0.5em}



\begin{lemma}
\label{lemma:compBridge}
\textnormal{|fstR linkComp hls| does not care about the proofs (paths) on the source side:}
$$ |fstR linkComp map (addPrefH (pref,[])) hls = fstR linkComp hls| $$
\end{lemma}
\paragraph{Proof sketch} As for `|fstR linkComp hls| does not care about on the source side', it means that:
$$\forall x, \exists y, \exists a, \exists b. x \; |fstR| \; (x,y) \; \land \; (x,y) \; |hls| \; (a,b)$$
So that the equation trivially hold.


%%%%%%%%%%%%%%%%%%
\subsection*{Proofs of Case 1}
Now we start to prove Case 1.
Since there is no link at the top of the view, |put| will invoke |putNoLink|.
Suppose the case of |putNoLink| generated from the consistency declaration |P xs ~ Q ys| is matched, then the goal is to show (we rewrite the two properties in a one line style)
\begin{align*}
& |get_v (put st vt s (Q ys) hls) = v|  \tag{Correctness} \\
& |fstR linkComp hls| \subseteq |fstR linkComp (get_l (put st vt s (Q ys) hls))| \tag{Retentiveness}
\end{align*}


As for Correctness:
\begin{proof}

\begin{align*}
&    |put st vt os (Q ys) hls| \\[0.3em]
=  & \qquad    \{\textnormal{expand the body of |put|} \} \\[-0.2em]
%
& |putNoLink st vt (selSPat st vt (Q ys)) os (Q ys) hls| \\[0.3em]
=  & \qquad   \{\textnormal{expand the body of |putNoLink|} \} \\[-0.2em]
& |P zs|
\end{align*}

\begin{align*}
&   |get_v (P zs)| \\[0.3em]
%
=  & \qquad    \{\textnormal{Substitution of variables |zs| (in |putNoLink|)} \} \\[-0.2em]
&   |get_v (P [... put (CTypeOf x_i) (CTypeOf y_i) os hls_i y_i ...])| \\[0.3em]
%
=  & \qquad    \{\textnormal{According to the source disjointness, the get generated from the} \\[-0.2em]
   & \qquad \phantom{\{}\textnormal{|P xs ~ Q ys| must be chosen} \} \\[-0.2em]
&   |Q [... get_v (put (CTypeOf x_i) (CTypeOf y_i) os hls_i y_i) ... ]| \\[0.3em]
%
=  & \qquad    \{\textnormal{The height of |y_i| is less than |h| and thus Correctness for it holds} \} \\[-0.2em]
&   |Q [... y_i ...]| \\[0.3em]
=  & \qquad    \{\textnormal{All |y_i| contribute to |ys|} \} \\[-0.2em]
&   |Q ys|
\end{align*}
\end{proof}



For Retentiveness:
\begin{proof}

\begin{align*}
&    |put st vt os (Q ys) hls| \\[0.3em]
=  & \qquad    \{\textnormal{expand the body of |put|} \} \\[-0.2em]
%
& |putNoLink st vt (selSPat st vt (Q ys)) os (Q ys) hls| \\[0.3em]
=  & \qquad   \{\textnormal{expand the body of |putNoLink|} \} \\[-0.2em]
& |P zs|
\end{align*}

\begin{align*}
&   |fstR linkComp get_l (P zs)| \\[0.3em]
%
=   & \qquad \{\textnormal{Variable substitution of |zs| (in |putNoLink|). Assume that |zs| = |... z_i ...|} \} \\[-0.2em]
&   |fstR linkComp get_l (P ... put (CTypeOf (x_i ; P)) (CTypeOf (y_i ; Q)) os y_i hls_i ...)| \\[0.3em]
%
=   & \qquad \{\textnormal{Expand the body of |get_l|} \} \\[-0.2em]
&   |fstR linkComp (MkLink (P ; Q) : concat [.. ls_i' ..])| \\[0.3em]
%
=   & \qquad \{\textnormal{Distribute |fstR| into concatenation} \} \\[-0.2em]
&   |fstR linkComp [MkLink (P ; Q)] ++ fstR linkComp concat [.. ls_i' ..]| \\[0.3em]
%
\supseteq   & \qquad \{\textnormal{Just drop |MkLink(P;Q)|} \} \\[-0.2em]
&   |fstR linkComp concat [.. ls_i' ..]| \\[0.3em]
%
=   & \qquad \{\textnormal{Variable substitutions of |ls_i'| (in |get|) } \} \\[-0.2em]
&   |fstR linkComp concat [... map (addPrefH (prefX_i, prefY_i)) ls_i ...])| \\[0.3em]
%
=   & \qquad \{\textnormal{Variable substitutions of |ls_i| (in |get|)} \} \\[-0.2em]
&   |fstR linkComp concat [... map (addPrefH (prefX_i, prefY_i))| \\[-0.2em]
&   |^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ (get_l (CTypeOf (x_i ; P)) (CTypeOf (y_i ; Q)) x_i) ...])| \\[0.3em]
%
=   & \qquad \{\textnormal{Replace |x_i| with the real input |put (CTypeOf (x_i ; P)) (CTypeOf (y_i ; Q)) os y_i hls_i|  } \} \\[-0.2em]
&   |fstR linkComp concat [...  map (addPrefH (prefX_i, prefY_i))| \\[-0.2em]
&   |^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ (get_l (CTypeOf (x_i ; P)) (CTypeOf (y_i ; Q)) | \\[-0.2em]
&   |^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ (put (CTypeOf (x_i ; P)) (CTypeOf (y_i ; Q)) os y_i hls_i))  ...]| \\[0.3em]
%
\supseteq   & \qquad \{\textnormal{The height of |y_i| is less than |h|, we can use induction hypotheses} \} \\[-0.2em]
&   |fstR linkComp concat [... map (addPrefH (prefX_i, prefY_i)) hls_i ...]| \\[0.3em]
%
=   & \qquad \{\textnormal{Variable substitution of |hls_i| (in |putNoLink|)} \} \\[-0.2em]
&   |fstR linkComp concat [... map (addPrefH (prefX_i, prefY_i))| \\[-0.2em]
&   |^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ (map (delPrefH ([], prefY_i)) (filterLinks prefY_i hls)) ...]| \\[0.3em]
%
=   & \qquad \{\textnormal{By |map| fusion, and |addPrefH (PrefX_i, []) =|} \\[-0.2em]
    & \qquad \phantom{\{}\textnormal{ |addPrefH (prefX_i, prefY_i)) . delPrefH ([], prefY_i)|} \} \\[-0.2em]
&   |fstR linkComp concat [... map (addPrefH (prefX_i, [])) (filterLinks prefY_i hls) ...]| \\[0.3em]
%
=   & \qquad \{\textnormal{Dividing a set into disjoint parts and merge them results in the same set} \} \\[-0.2em]
&   |fstR linkComp (map (addPrefH (prefX_i, [])) hls)| \\[0.3em]
%
=   & \qquad \{\textnormal{By \autoref{lemma:compBridge}} \} \\[-0.2em]
&   |fstR linkComp hls|
\end{align*}
\end{proof}

In addition, if we do not drop |MkLink (P;Q)| at the fourth step, we have the following conclusion:
$$ |fstR linkComp ((MkLink (P ; Q)) : hls) subseteq fstR linkComp (get_l (putNoLink st vt (selSPat st vt (Q ys)) os (Q ys) hls)| $$









\subsection*{Case 2: When There Are Links at Top of the View}
Since there are links at the top of the view, |putWithLinks| will be executed.
We divide the whole proofs into small parts by first introducing several lemmas, and then we proceed with the proofs.



\begin{lemma}
\label{lemma:getFetchedLink}
\textnormal{Perform |get| on the source fetched by a link |l| will yield the same link except for their source paths.}
$$ |fstR  linkComp [head (get_l (fetch l os))| = |fstR linkComp [l]|$$
\end{lemma}

\begin{proof}
\begin{align*}
&   |fstR  linkComp [head (get_l (fetch l os))]|  \\[0.3em]
%
=  & \qquad \{\textnormal{Suppose |l = ((P,sPath),(Q,[]))| } \} \\[-0.2em]
&   |fstR  linkComp [head (get_l (P xs))]| \\[0.3em]
%
=  & \qquad \{\textnormal{Expand the definition of |get|} \} \\[-0.2em]
&   |fstR  linkComp [head (l_0 : concat [... ls_i' ...])]| \\[0.3em]
%
=  & \qquad \{\textnormal{Extract the head of a list} \} \\[-0.2em]
&   |fstR  linkComp [l_0]| \\[0.3em]
%
=  & \qquad \{\textnormal{Variable substitution of |l_0| (in |get|)} \} \\[-0.2em]
&   |fstR  linkComp [MkPat (P;Q)]| \\[0.3em]
%
=  & \qquad \{\textnormal{By the definition of |MkPat|} \} \\[-0.2em]
&   |fstR  linkComp [((P,[]),(Q,[]))]| \\[0.3em]
%
=  & \qquad \{\textnormal{|fstR| does not care about the source path} \} \\[-0.2em]
&   |fstR  linkComp [l]| \\[0.3em]
\end{align*}
\end{proof}

\begin{lemma}
\label{lemma:getRepSub}
\textnormal{|get_v| applied to the result of |repSubtree| produces the same result as |get_v| applied to the last argument of |repSubtree|. |get_l| applied to the result of the |repSubtree| is equal to |get_l t_2| with a slight modification, which is to replace the top-level link of it with the first link from |get_l t_1|.}
\begin{align*}
& |get_v (repSubtree sPat t_1 t_2)  =  get_v t_2| \\
& |get_l (repSubtree sPat t_1 t_2)  =  head (get_l t_1) : tail  (get_l t_2) |
\end{align*}
\end{lemma}


\begin{proof}
For the first equation, as \autoref{fig:repSubtree} shows, |repSubtree| takes a pattern and two trees matching the pattern, in which the second tree may only differ from the result of the |repSubtree| at some parts described by the pattern.
In addition, the different parts will all be discarded by |get_s|, so the equation trivially hold.

For the second equation, we can consider an inductive rule |P xs ~ Q ys|.
Suppose |(MkPat P) = A| and |(MkRegPat P) = B|, according to the generated code for a inductive rule , we have
\begin{align*}
& |get_l (repSubtree (MkPat P) (MkRegPat P) (P xs))| \\[0.3em]
%
=  & \qquad \{\textnormal{|(MkPat P) = A| and |(MkRegPat P) = B|} \} \\[-0.2em]
&   |get_l (repSubtree A B (P xs))| \\[0.3em]
%
=  & \qquad \{\textnormal{Expand the body of |repSubtree|} \} \\[-0.2em]
&   |get_l (B xs)| \\[0.3em]
%
=  & \qquad \{\textnormal{Expand the body of |get_l|} \} \\[-0.2em]
&   |l_0 : concat [... ls_i' ...] | \\[0.3em]
%
=  & \qquad \{\textnormal{Variable substitution of |l_0| and |ls_i'| (in |get|)} \} \\[-0.2em]
&   |MkLink(B;Q) : concat [... map (addPrefH (prefX_i, prefY_i)) ls_i ...] |
%
\\[1.2em]
\textnormal{and}\\
%
& |get_l (P xs)| \\[0.3em]
%
=  & \qquad \{\textnormal{Expand the body of |get_l|} \} \\[-0.2em]
&   |l_0 : concat [... ls_i' ...] | \\[0.3em]
%
=  & \qquad \{\textnormal{Variable substitution of |l_0| and |ls_i'| (in |get|)} \} \\[-0.2em]
&   |MkLink(P;Q) : concat [... map (addPrefH (prefX_i, prefY_i)) ls_i ...] | \\[0.3em]
%
\\[1.2em]
\textnormal{and}\\
%
& |get_l ((MkRegPat P))| \\[0.3em]
%
=  & \qquad \{\textnormal{|(MkRegPat P) = B|} \} \\[-0.2em]
&   |get_l B| \\[0.3em]
%
=  & \qquad \{\textnormal{Expand the body of |get_l|} \} \\[-0.2em]
&   |l_0 : _ | \\[0.3em]
%
=  & \qquad \{\textnormal{Variable substitution of |l_0| (in |get|)} \} \\[-0.2em]
&   |MkLink(B;Q) : _ |
\end{align*}

It is obvious that |get_l (repSubtree sPat t_1 t_2) = head (get_l t_1) : tail (get_l t_2)|.

\end{proof}


\begin{lemma}
\label{lemma:insSubtree}
\textnormal{Let |P s_2 = insSubtree sPat s_1 s_0|, we have the following (in)equations, where variables should refer to their bindings in the definition of |insSbutree|.}
\begin{align*}
  |get_v (P s_2)| &= |get_v s_0| \\
  |fstR linkComp (get_l s_0 ++ [MkLink (P; Void)])| &\subseteq |fstR linkComp (get_l (P s_2))|
\end{align*}
\end{lemma}


\begin{proof}
For the first equation, since |sPat| is of pattern |P x ~ x| (there is only one variable), let us assume |insSubtree (P x) s_1 s_0 = P s_2|.
\begin{align*}
&   |get_v (P s_2)| \\[0.3em]
%
=  & \qquad \{\textnormal{There is no constructor in the right-hand side of |P x ~ x|} \} \\[-0.2em]
&   |get_v s_2| \\[0.3em]
%
=  & \qquad \{\textnormal{Variable substitution} \} \\[-0.2em]
&   |get_v (mkInj _ _ s_0)| \\[0.3em]
%
=  & \qquad \{\textnormal{By \hyperref[prereq:getInj]{Prerequisite 1}} \} \\[-0.2em]
&   |get_v s_0|
\end{align*}


For the second inequation,
\begin{align*}
&   |fstR linkComp get_l (P s_2)| \\[0.3em]
%
=  & \qquad \{\textnormal{By the definition of |get_l|, and there is no constructor in the view.} \} \\[-0.2em]
&   |fstR linkComp (MkLink (P ; Void) : concat [... ls_i' ...])| \\[0.3em]
%
%
=  & \qquad \{\textnormal{The pattern |P s_2| has only one subtree |s_2|} \} \\[-0.2em]
&   |fstR linkComp (MkLink (P ; Void) : concat [ls_0'])| \\[0.3em]
%
%
=  & \qquad \{\textnormal{|concat [ls_0'] = ls_0'|} \} \\[-0.2em]
&   |fstR linkComp (MkLink (P ; Void) ++ ls_0')| \\[0.3em]
%
%
=  & \qquad \{\textnormal{Variable substitution of |ls_0'| (in |get|)} \}  \\[-0.2em]
&   |fstR linkComp (MkLink (P ; Void) ++ map (addPrefH (PrefX_0 , PrefY_0)) ls_0)| \\[0.3em]
%
%
=  & \qquad \{\textnormal{Distribute |fstR| into concatenation} \}  \\[-0.2em]
&   |fstR linkComp [MkLink (P ; Void)] ++ fstR linkComp (map (addPrefH (PrefX_0 , PrefY_0)) ls_0)| \\[0.3em]
%
%
=  & \qquad \{\textnormal{Variable substitution of |PrefX_0| , |PrefY_0|, and |ls_0| (in |get|)} \}  \\[-0.2em]
&   |fstR linkComp [MkLink (P ; Void)] ^ ++ | \\[-0.2em]
&   |fstR linkComp (map (addPrefH (Pref (x ; P x) , Pref (y ; Q y))) (get_l s_2)) | \\[0.3em]
%
%
=  & \qquad \{\textnormal{|Pref (y ; Q y)| is empty because the inductive rule is |P x ~ x|} \}  \\[-0.2em]
&   |fstR linkComp [MkLink (P ; Void)] ++ fstR linkComp (map (addPrefH (Pref (x ; P x) , [])) (get_l s_2)) |  \\[0.3em]
%
%
=  & \qquad \{\textnormal{By \autoref{lemma:compBridge}} \}  \\[-0.2em]
&   |fstR linkComp [MkLink (P ; Void)] ++ fstR linkComp (get_l s_2) |  \\[0.3em]
%
%
=  & \qquad \{\textnormal{Variable substitution of |s_2| (in |insSbutree|)} \}  \\[-0.2em]
&   |fstR linkComp [MkLink (P ; Void)] ++ fstR linkComp (get_l (mkInj _ _ s_0))| \\[0.3em]
%
%
\supseteq  & \qquad \{\textnormal{By \hyperref[prereq:getInj]{Prerequisite 1}} \}  \\[-0.2em]
&   |fstR linkComp [MkLink (P ; Void)] ++ fstR linkComp (get_l s_0))|   \\[0.3em]
%
%
\supseteq  & \qquad \{\textnormal{Drop |fstR linkComp [MkLink (P;Void)]|} \}  \\[-0.2em]
&   |fstR linkComp (get_l s_0)|
\end{align*}
\end{proof}



\begin{lemma}
\label{lemma:splice}
\textnormal{Let |s_2 = splice os imag s_0|, we have the following (in)equations, where variables should refer to their bindings in the definition of |splice|.}
\begin{align*}
|get_v s_2|                           & =  |get_v s_0| \\[0.3em]
|fstR linkComp (get_l s_0 ++ [imag])|  & \subseteq |fstR linkComp get_l s_2|
\end{align*}
\end{lemma}

\begin{proof}
For the first equation:
\begin{align*}
&   |get_v s_2| \\[0.3em]
%
=  & \qquad \{\textnormal{Expand the body of |splice| } \} \\[-0.2em]
&   |get_v (insSubtree sPat (fetch imag on) s_0)| \\[0.3em]
%
=  & \qquad \{\textnormal{by \autoref{lemma:insSubtree} } \} \\[-0.2em]
&   |get_v s_0|
%
% =  & \qquad \{\textnormal{Variable substitution of |s_2| (in |insSbutree|)  } \} \\[-0.2em]
% &   |get_v (mkInj _ _ s_0)| \\[0.3em]
% %
% =  & \qquad \{\textnormal{by \hyperref[prereq:getInj]{Prerequisite 1} } \} \\[-0.2em]
% &   |get_v s_0|
\end{align*}


For the second inequation:
\begin{align*}
&   |fstR linkComp get_l s_2| \\[0.3em]
%
=  & \qquad \{\textnormal{Expand the body of |splice|} \} \\[-0.2em]
&   |fstR linkComp get_l (insSubtree sPat (fetch imag os) s_0)| \\[0.3em]
\supseteq  & \qquad \{\textnormal{By \autoref{lemma:insSubtree} } \} \\[-0.2em]
&   |fstR linkComp (get_l s_0 ++ [MkLink (P; Void)])| \\[0.3em]
%
=  & \qquad \{\textnormal{ The variable |imag| in |splice| is |((sPat,sPath),(Void,[]))| } \} \\[-0.2em]
&   |fstR linkComp (get_l s_0 ++ [imag])|
\end{align*}

\end{proof}


\begin{lemma}
\label{lemma:imag_links}
\textnormal{The following (in)equations hold for the code block handling imaginary links.}
\textnormal{Suppose}
$$ |s' = foldr (splice os) s imags| $$
\textnormal{Then}
\begin{align*}
  |get_v s'|                         &= |get_v s| \\
  |fstR linkComp (get_l s ++ imags)|  &\subseteq |fstR linkComp get_l s'|
\end{align*}
\end{lemma}

\begin{proof}
The two (in)equations can be proved by making inductions on the length |imags|.
Suppose the (in)equations hold for |foldr (splice os) s ims|, now we consider the input list which has one more imaginary link:
\begin{align*}
&   |foldr (splice os) s (im:ims)| \\[0.3em]
%
=  & \qquad \{\textnormal{Expand the body of |foldr| once} \} \\[-0.2em]
&   |splice os im (foldr (splice os) s ims)| \\[0.3em]
%
=  & \qquad \{\textnormal{Let |s' = foldr (splice os) s ims|} \} \\[-0.2em]
&   |splice os im s'| \\[0.3em]
%
=  & \qquad \{\textnormal{Let |s'' = splice os im s'| } \} \\[-0.2em]
&   |s''|
\end{align*}
so we need to prove:
\begin{align*}
  |get_v s''|                           &= |get_v s| \\
  |fstR linkComp (get_l s ++ (im:ims))|  &\subseteq |fstR linkComp get_l s''|
\end{align*}
which can be easily derived from the properties of |splice| and induction hypotheses:


For the first equation:
\begin{align*}
&   |get_v s''| \\[0.3em]
=  & \qquad \{\textnormal{By \autoref{lemma:splice}, the property of |splice|} \} \\[-0.2em]
&   |get_v s'| \\[0.3em]
%
=  & \qquad \{\textnormal{By induction hypotheses} \} \\[-0.2em]
&   |get_v s|
\end{align*}

For the second inequation:
\begin{align*}
&   |fstR linkComp get_l s''| \\[0.3em]
%
\supseteq  & \qquad \{\textnormal{By \autoref{lemma:splice}, the property of |splice|} \} \\[-0.2em]
&   |fstR linkComp (get_l s' ++ [im])| \\[0.3em]
%
=  & \qquad \{\textnormal{Distribute |fstR| into concatenation} \} \\[-0.2em]
&   |fstR linkComp (get_l s') ++ fstR linkComp [im])| \\[0.3em]
%
\supseteq  & \qquad \{\textnormal{By induction hypotheses} \} \\[-0.2em]
&   |fstR linkComp (get_l s ++ ims) ++ fstR linkComp [im]| \\[0.3em]
%
=  & \qquad \{\textnormal{Distribute |fstR| into concatenation} \} \\[-0.2em]
&   |fstR linkComp (get_l s) ++ fstR linkComp ims ++ fstR linkComp [im]| \\[0.3em]
%
=  & \qquad \{\textnormal{ |get_l s|, |ims|, and |im| are `disjoint' in |putWithLinks|.} \} \\[-0.2em]
&   |fstR linkComp (get_l s ++ (im:ims))|
\end{align*}
\end{proof}




\begin{lemma}
\label{lemma:real_link}
\textnormal{The code block for handling the real link produces a source |s_2| consistent with the input view |v|, and the source |s_2| is created in a way that takes input link |l| into account, if there is.
The variables in the following (in)equations should refer to their bindings in |putWithLinks|.}
\begin{align*}
&  |get_v s_2| &=& \; |v| \\
&  |fstR linkComp (l:hls')| &\subseteq& \; |fstR linkComp get_l s_2|, \qquad \textnormal{if there is an input real link} \\
&  |fstR linkComp hls'| &\subseteq& \; |fstR linkComp get_l s_2|, \qquad \textnormal{if there is no such input real link}
\end{align*}
\end{lemma}


\begin{proof}
For the first equation |get_v s_2 = v|.

\begin{itemize}
  \item   If there is a real link,
\begin{align*}
&   |get_v s_2| \\[0.3em]
%
=  & \qquad \{\textnormal{Variable substitution of |s_2| (in |putWithLinks|)} \} \\[-0.2em]
&   |get_v (repSubtree sPat s_0 s_1)| \\[0.3em]
%
=  & \qquad \{\textnormal{By \autoref{lemma:getRepSub}}\} \\[-0.2em]
&   |get_v s_1| \\[0.3em]
%
=  & \qquad \{\textnormal{Variable substitution of |s_1| (in |putWithLinks|)} \} \\[-0.2em]
&   |get_v (putNoLink (typeOf s_0) vt sPat os hls' v)| \\[0.3em]
%
=  & \qquad \{\textnormal{No link at the top of the view. Use the result of Case 1} \} \\[-0.2em]
&   |v|
\end{align*}
  \item   If there is no real link,
\begin{align*}
&   |get_v s_2| \\[0.3em]
%
=  & \qquad \{\textnormal{Variable substitution of |s_2| (in |putWithLinks|)} \} \\[-0.2em]
&   |get_v (putNoLink st vt (selSPat st vt v) os hls' v)| \\[0.3em]
%
=  & \qquad \{\textnormal{No link at the top of the view. Use the result of Case 1} \} \\[-0.2em]
&   |v|
\end{align*}
\end{itemize}



For the inequations |fstR linkComp (l:hls') subseteq fstR linkComp get_l s_2| and |fstR linkComp hls' subseteq fstR linkComp get_l s_2|:
\begin{itemize}
  \item   If there is a real link,
\begin{align*}
&   |fstR linkComp get_l s_2| \\[0.3em]
%
=  & \qquad \{\textnormal{Variable substitution of |s_2| (in |putWithLinks|)} \} \\[-0.2em]
&   |fstR linkComp get_l (repSubtree sPat s_0 s_1)| \\[0.3em]
%
=  & \qquad \{\textnormal{By \autoref{lemma:getRepSub} } \} \\[-0.2em]
&   |fstR linkComp (head (get_l s_0) : tail (get_l s_1))| \\[0.3em]
%
=  & \qquad \{\textnormal{Variable substitution of |s_0| and |s_1| (in |putWithLinks|)} \} \\[-0.2em]
&   |fstR linkComp (head (get_l (fetch l os)) : tail (get_l (putNoLink (typeOf s_0) vt sPat os v hls')))| \\[0.3em]
%
=  & \qquad \{\textnormal{Distribute |fstR| over concatenation} \} \\[-0.2em]
&   |fstR  linkComp [head (get_l (fetch l os))] ^ ++| \\[-0.2em]
&   |fstR  linkComp (tail (get_l (putNoLink (typeOf s_0) vt sPat os v hls')))| \\[0.3em]
%
=  & \qquad \{\textnormal{By \autoref{lemma:getFetchedLink}} \} \\[-0.2em]
&   |fstR  linkComp [l]  ++ fstR linkComp (tail (get_l (putNoLink (typeOf s_0) vt sPat os v hls')))| \\[0.3em]
%
=  & \qquad \{\textnormal{|fstR linkComp (tail R) = tail (fstR linkComp R)| since |R| is an ordered list.  } \} \\[-0.2em]
&   |fstR  linkComp [l]  ++ tail (fstR linkComp (get_l (putNoLink (typeOf s_0) vt sPat os v hls')))| \\[0.3em]
%
\supseteq  & \qquad \{\textnormal{No link at the top of the view. Use the result (last inequation) of Case 1} \} \\[-0.2em]
&   |fstR  linkComp [l]  ++ (tail (fstR linkComp (MkLink (_;_) : hls')))| \\[0.3em]
%
=  & \qquad \{\textnormal{|tail| drops the first link in the list} \} \\[-0.2em]
&   |fstR  linkComp [l]  ++ fstR linkComp hls'| \\[0.3em]
%
=  & \qquad \{\textnormal{By the property of list concatenation} \} \\[-0.2em]
&   |fstR linkComp (l : hls')|
\end{align*}


  \item  If there is no real link.
\begin{align*}
&   |fstR linkComp get_l s_2| \\[0.3em]
%
=  & \qquad \{\textnormal{Variable substitution of |s_2| (in |putWithLink|) } \} \\[-0.2em]
&   |fstR linkComp get_l (putNoLink st vt (selSPat st vt v) os v hls'))| \\[0.3em]
%
\supseteq  & \qquad \{\textnormal{No link at the top of the view. Use the result of Case 1} \} \\[-0.2em]
&   |fstR linkComp hls'|
\end{align*}

\end{itemize}

\end{proof}


\subsection*{Proofs of Case 2}
Now we start to prove Case 2. Since there are links at the top of the view, |put| will invoke |putWithLinks|. The goal is to show
\begin{align*}
&  |get_v (putWithLinks st vt os v hls) = v| \tag{Correctness} \\
&  |fstR linkComp hls subseteq  fstR linkComp get_l (putWithLinks st vt os v hls)|  \tag{Retentiveness}
\end{align*}


As for Correctness:
\begin{proof}
\begin{align*}
&    |get_v (putWithLinks st vt os v hls)| \\[0.3em]
%
= & \qquad \{ \textnormal{Expand the body of |putWithLinks|} \} \\[-0.2em]
&    |get_v s_4| \\[0.3em]
%
= & \qquad \{ \textnormal{Variable substitution of |s_4| (in |putWithLinks|)} \} \\[-0.2em]
&    |get_v (mkInj _ _ s_3)| \\[0.3em]
%
= & \qquad \{ \textnormal{By \hyperref[prereq:getInj]{Prerequisite 1} } \} \\[-0.2em]
&    |get_v s_3| \\[0.3em]
%
= & \qquad \{ \textnormal{Variable substitution of |s_3| (in |putWithLinks|)} \} \\[-0.2em]
&    |get_v (foldr (splice os) s_2 imags)| \\[0.3em]
%
= & \qquad \{ \textnormal{By \autoref{lemma:imag_links}} \} \\[-0.2em]
&    |get_v s_2| \\[0.3em]
%
= & \qquad \{ \textnormal{Variable substitution of |s_2| (in |putWithLinks|)} \} \\[-0.2em]
&    |get_v (repSubtree sPat s_0 s_1)| \\[0.3em]
%
= & \qquad \{ \textnormal{By \autoref{lemma:getRepSub}} \} \\[-0.2em]
&    |get_v s_1| \\[0.3em]
%
= & \qquad \{ \textnormal{Variable substitution of |s_1| (in |putWithLinks|)} \} \\[-0.2em]
&    |get_v (putNoLink (typeOf s_0) vt sPat os v hls')| \\[0.3em]
%
= & \qquad \{ \textnormal{No link at the top. Use the result of Case 1} \} \\[-0.2em]
&    |v|
\end{align*}
\end{proof}


As for Retentiveness:
\begin{proof}
\begin{align*}
&    |fstR linkComp get_l (putWithLinks st vt os v hls)| \\[0.3em]
%
= & \qquad \{ \textnormal{Expand the body of |putWithLinks| } \} \\[-0.2em]
&    |fstR linkComp get_l s_4| \\[0.3em]
%
= & \qquad \{ \textnormal{Variable substitutions of |s_4| (in |putWithLinks|)} \} \\[-0.2em]
&    |fstR linkComp get_l (mkInj st (typeOf s_3) s_3| \\[0.3em]
%
\supseteq & \qquad \{ \textnormal{By \hyperref[prereq:getInj]{Prerequisite 1}}  \} \\[-0.2em]
&    |fstR linkComp get_l s_3| \\[0.3em]
%
= & \qquad \{ \textnormal{Variable substitutions of |s_3| (in |putWithLinks|)}  \} \\[-0.2em]
&    |fstR linkComp get_l (foldr (splice os) s_2 imags)| \\[0.3em]
%
\supseteq & \qquad \{ \textnormal{By \autoref{lemma:imag_links}} \} \\[-0.2em]
&    |fstR linkComp (get_l s_2 ++ imags)| \\[0.3em]
%
= & \qquad \{ \textnormal{Distribute |fstR| into concatenation} \} \\[-0.2em]
&    |fstR linkComp (get_l s_2) ++ fstR linkComp imags| \\[0.3em]
%
\supseteq & \qquad \{ \textnormal{By \autoref{lemma:real_link}, suppose the real link |l| exists. }  \} \\[-0.2em]
&    |fstR linkComp (l : hls') ++ fstR linkComp imags|  \\[0.3em]
%
\supseteq & \qquad \{ \textnormal{By the property of list concatenation}  \} \\[-0.2em]
&    |fstR linkComp ([l] ++ hls' ++ imags)|  \\[0.3em]
%
= & \qquad \{ \textnormal{Previously |hls| is divided into |(l, imags, hls')|}  \} \\[-0.2em]
&    |hls|
\end{align*}

If the real link |l| does not exists, the last three steps are:
\begin{align*}
&    |fstR linkComp (get_l s_2) ++ fstR linkComp imags| \\[0.3em]
%
\supseteq & \qquad \{ \textnormal{By \autoref{lemma:real_link}, suppose the real link |l| does not exist. }  \} \\[-0.2em]
&    |fstR linkComp hls' ++ fstR linkComp imags|  \\[0.3em]
%
\supseteq & \qquad \{ \textnormal{By the property of list concatenation}  \} \\[-0.2em]
&    |fstR linkComp (hls' ++ imags)|  \\[0.3em]
%
= & \qquad \{ \textnormal{Previously |hls| is divided into |(imags, hls')|}  \} \\[-0.2em]
&    |hls|
\end{align*}
\end{proof}


\subsection*{As A Retentive Lens}
Theorems proved above will be the core part of proving \autoref{thm:semsound}, but we still need to show two more things:
\begin{itemize}
\item Links produced by $get'$ is uniquely identifying.
\item Our |put| retains all links of second-order properties.
\end{itemize}

For the uniquely identifying property, we should verify that our construction of |get'| generates a set of second-order properties completely specifying how regions are assembled into a tree. Given a concrete implementation of constructing |get'|, in particular, a concrete implementation of generating second-order properties described in \autoref{subsec:proof}, the verification should be straightforward, thus omitted here.

For the retentiveness of links of second-order properties, the proof will be quite similar to the proof above showing retentiveness of regions, thus we also omit it here. 

Now we can conclude \autoref{thm:semsound}, that is, our |get| and |put| functions can be lifted into a retentive lens. Following this, our |get| and |put| functions satisfy \autoref{thm:semsound2}, that is, they satisfy:
\begin{align*}
& |get (put (s, v, l)) = (v', l')| \quad\Rightarrow\quad |v = v'| \tag{Correctness}\\
& |get (put (s, v, l)) = (v', l')| \quad\Rightarrow\quad |fst| \cdot l \subseteq |fst| \cdot l' \tag{Retentiveness} \\
& |get s = (v, l)| \quad\Rightarrow\quad |mputsv (s, v, l) = s| \tag{Hippocraticness}
\end{align*}


