\section{Defining Retentive Bidirectional Semantics via Code Generation}
\label{sec:generation}


In this section we explain in detail how retentive lenses are generated from a set of consistency relations defined in our DSL.
We will give a source-to-source translation from the program in our DSL to Haskell code.
As we go through the translation, we will sketch out why the generated lenses satisfies \autoref{prop:put} by construction, hinting at \autoref{thm:semsound2}.

\subsection{Overview of the Generation}
The (top-level) code generation for an input program |D| is sketched below, in which both the generation for |get| and |put| functions, and the generating process for each inductive rule are independent.
The generation for |put| is further divided into many cases depending on whether we need to handle input links.
It is worth noting that, as we will see later, the |put| function (generated from |putDef|) is irrelevant to the input program |D|, because it will delegate all the work to |putNoLink| and |putWithLinks|, depending on whether there are links connected to the top of the input view.
In addition, the function |putWithLinksDef| also delegates its work to other auxiliary functions when it need to pattern match against input data, and hence independent of |D|.
Important auxiliary functions will be introduced later when they appear in the generated code.
\begin{align*}
& \textsf{GenLenses} \llbracket |dataTypes|\ |dGroups| \rrbracket = |dataTypes| |++| \textsf{GenGets} \llbracket |dGroups| \rrbracket  |++| \textsf{GenPuts} \llbracket |dGroups| \rrbracket \\
%
& \textsf{GenGets} \llbracket |dGroups| \rrbracket = |concatMap| \ \textsf{GenGetG} \llbracket \cdot \rrbracket \ |dGroups|\\
& \textsf{GenPuts} \llbracket |dGroups| \rrbracket = |putDef| |:| |putWithLinksDef| |:| |concatMap| \ \textsf{GenPutNoLinkG} \llbracket \cdot \rrbracket \ |dGroups|\\
& \textsf{GenGetG} \llbracket |st| \ |'<--->'| \ |vt| \ |ds| \ |';'|\rrbracket = |map| \ (\textsf{GenGet} \ (|st|,|vt|)\ \llbracket \cdot \rrbracket)\ |ds| \\
& \textsf{GenPutNoLinkG} \llbracket |st| \ |'<--->'| \ |vt| \ |ds| \ |';'|\rrbracket = |map| \ (\textsf{GenPutNoLink2} \ (|st|,|vt|) \llbracket \cdot \rrbracket) \ |ds|
\end{align*}


The generated |get| and |put| functions have the following signatures,
in which we use some dependent function types that we can emulate to some extent in our actual Haskell code.
%
Compared to \autoref{def:retentive_lenses}, the generated |get| and |put| functions take as additional arguments the types of the input source (|s :: STypes|) and view (|v :: VTypes|), on which the output types of |get| and |put| functions depend.
For |put|, |s_0| is the type of the original source, which in general can be different from the type of the generated source\footnote{Many source types are interconvertible and have the same view, for instance, |0 - 3 :: Expr| and |-3 :: Term|. Given a view, in many situations we may want to generate a new source whose type is different from the old one.}.

%
\begin{code}
get :: (s :: STypes) -> (v :: VTypes) -> s -> (v, [HLink])

put :: (s :: STypes) -> (v :: VTypes) -> s_0 -> v -> [HLink] -> s
\end{code}
For the real Haskell implementation, we emulate dependent types by assigning each source type and view type a \emph{type tag}.
All of the source type tags and view type tags form the union types |STypes| and |VTypes| respectively.
The region patterns of links also need to be encoded, to have their representations as Haskell data.
Briefly, we collect all the source and view patterns appeared in the consistency relations,
and assign different constructors to different patterns (in a way like hashing each pattern to a unique number).
For example, the runtime representation for the region pattern |Plus x _ _| might be |S_ExprArithCase0|.
Note that if the right-hand side of an inductive rule happens to be a variable, we will assign it the |Void| region pattern, meaning that there is in fact no such region (pattern) in the view.
(However, since |get| need to produce a set of links that is uniquely identifying, all of the source data fragments need to connect to some parts in the view -- even they does not seem to exist.)
Let us call a link an \emph{imaginary link} if it has the |Void| region pattern on the view, and otherwise a \emph{real link}.
% It is necessary to make an imaginary link for consistency relations such as |Term _ t ~ t|, otherwise a  source region |Term a _| will get lost after |put|.





\subsection{Generation of |get|}
\label{subsec:gen_get}
Generation of the |get| function is basically converting inductive rules to function definition clauses: if, for example, |Plus| is consistent with |Add|, then |get| can simply map |Plus| to |Add| and continue with the subtrees recursively.
The consistency links between the source and view are thus established by construction.
% However, for |get| to be implementable as a total function in this way, the consistency relations should satisfy the following well-formedness constraints:
% \begin{itemize}
% \item The source patterns in every declaration group should cover all cases so that |get| is fully defined.
% \item All the source patterns should include constructors so that recursion can be well-founded.
% \item For simplicity, we require that, for every inductive rule, each variable in its source pattern should appear exactly once in its view pattern.
% The recursive computation can thus simply process all source subtrees and plug the results into the right places indicated by the view pattern.
% \end{itemize}


The translation is more formally described by the transformation below.
For every inductive rule in a group, |GenGet2 (st,vt)| is invoked to generate a definition clause of |get|.
Every inductive rule has the form |P ~ Q| for some source pattern~|P| and view pattern~|Q|; we often write |P(VARS x) ~ Q(VARS y)| to name variables in |P|~and~|Q| as $\vv{|x|}$~and~$\vv{|y|}$ respectively.
(These variables should in fact be suitably renamed to avoid name clashing.)
Individual variables in $\vv{|x|}$~and~$\vv{|y|}$ are denoted by $x_i$ and $y_i$, and the lines of code containing $x_i$ and $y_i$ should be repeated for every corresponding pair of source and view variables.
Functions that is expanded at compile time all start with a capital character and in \textsf{sans-serif}.
(They can be treated as macros.)
%
\begin{code}
GenGet2 (st,vt) (P (VARS x) ~ Q (VARS y)) =
  get (MkTag st) (MkTag vt) P = (Q, l_0 : concat [ls_0' ... ls_n'] )
    itWhere
      (y_i , ls_i)  = get (CTypeOf (x_i ; P)) (CTypeOf (y_i ; Q)) x_i
      prefX_i       = (Pref (x_i ; P))
      prefY_i       = (Pref (y_i ; Q))
      ls_i'         = map (addPrefH (prefX_i, prefY_i)) ls_i
      l_0           = (MkLink (P ; Q))
\end{code}
%
The generated |get| clause does the following:
\begin{enumerate}
  \item Recursively apply |get| to the subtrees~|x_i| of the input data to get their corresponding views~|y_i| and consistency links~|ls_i|.
  \item Since |x_i|~and~|y_i| are subtrees of the source and view, we need to update the paths in their links~|ls_i| (which previously start from the roots of the subtrees) by adding the corresponding prefixes.
  The prefixes are computed by $\mathsf{Pref}$, and added by |addPrefH|.
  \item Finally we make a new link~|l_0| between the top of the current source and view using $\mathsf{MkLink}$, and put |l_0| into the front of the updated link list.
\end{enumerate}


%  \todo{brief explanation of MkLink}
%\subsubsection{Build Consistency Links} The function $\mathsf{MkLink}$ will create a link between source pattern |P (VARS x)| and view pattern |Q (VARS y)|.
%As \autoref{fig:DSL-syntax} shows, a consistency relation relates two patterns consisting of only constructors (including constants) and variables. So $\mathsf{MkLink}$ works in the following way:
%\begin{code}
%MkLinkt sp@(ConP _)  vp  = ((MkPat sp, []) ,  (MkPat vp, []))
%MkLinkt (VarP _) _       = error "In this case, get is not a function."
%\end{code}
%% MkLinkt :: Pat -> Pat -> HLink
%% itMkPat  :: Pat -> PatCode
%% itMkPat (ConP c ps) = parens c (map itMkPat ps)
%% itMkPat (VarP v )   = "_"
%% itMkPat (LitP l)    = l
%
%%
%\begin{enumerate}
%  \item The first case handles the situation when |P (VARS x)| is a constructor pattern. We build a link consisting of patterns made by $\mathsf{MkPat}$ and empty paths.
%  $\mathsf{MkPat}$ works in a way like hashing each pattern to a unique number, if the input pattern starts with a constructor.
%  For instance, a |sp| of |TF (Factor r)| might be assigned a pattern |SPat_Factor0|, and a |sp| of |Sub (N 0) r| might be assigned a pattern |VPat_Arith1|.
%
%  However, if |sp| is a variable pattern meaning that there is no explicit construct that can be associated with, $\mathsf{MkPat}$ generates for it an \emph{imaginary} constructor represented by the \emph{Void} pattern $()$.
%  \phantomsection
%  \label{def_imaginary_link}
%  Given a horizontal link, let us call it a \emph{real link} if it is made from two constructor patterns;
%  and call it an \emph{imaginary link} if it is view pattern is a variable pattern.
%  (In fact the source pattern cannot be a variable pattern, as the second case shows.)
%
%  \item The second case is when |P (VARS x)| is a variable pattern, meaning that there is no construct \emph{in the source} can be associated with the view.
%  However, this is prohibited in the DSL because it means that a source can be mapped to infinite number of views and |get| is not deterministic. (Think that in the |get| direction, this relation can be used as many times as we want.)
%  So we raise an error for this case.
%\end{enumerate}


\begin{example}
\label{example:get_code}
|GenGet2 (Expr,Arith) (Plus _ x y ~ Add x y)| will produce the following Haskell code:
\begin{code}
get ExprTy ArithTy (Plus r_0 xS yS) = (Add xV yV, l_0 : concat [xls', yls'])
  where
    (xV, xls)  = get ExprTy ArithTy xS
    prexS      = [1]
    prexV      = [0]
    xls'       = map (addPrefH (prexS, prexV)) xls

    (yV, yls)  = get TermTy ArithTy yS
    preyS      = [2]
    preyV      = [1]
    yls'       = map (addPrefH (preyS, preyV)) yls

    l_0        = ((S_ExprArithCase0, []), (V_ExprArithCase0, []))
\end{code}
We see that the first two arguments of |get| come from the encoded types of the group where this clause belongs.
There are two variables (|x| and |y|) in the inductive rule, so they are handled by lines 1--4 and lines 5--8 in the where-block respectively.
Finally the link~|l_0| between encoded region |S_ExprArithCase0| (representing |Plus x _ _|) and |V_ExprArithCase0| (representing |Add _ _|) is added.
\end{example}



\subsection{Generation of |put|}
\label{subsec:gen_put}
Generation of the |put| function is little complex.
Below we describe an algorithm that can be best understood as creating a new source following the structure of the view while satisfying retentiveness by construction.
To satisfy retentiveness, we should inspect whether the top of the view is linked to some part of the original source.
\begin{itemize}
  \item If there is no link at the top of the view, it means that retentiveness does not require that anything be retained at the top.
  In this case, we only need to find a inductive rule |P ~ Q| such that the view matches~|Q|, create a consistent source |P|, and call |put| on the subtrees of the view recursively.

  \item If there are links at the top of the view, for each link we can copy the source fragment it indicates in the original source, to the top of the new source, such that a horizontal link between the updated source and the view can be later created by |get| to satisfy retentiveness.
\end{itemize}



The |put| function thus starts with a case analysis on whether there are top-level links:
\begin{code}
put :: (s :: STypes) -> (v :: VTypes) -> s_0 -> v -> [HLink] -> s
put st vt os v hls  |  not (hasTopLink hls)  = ...
put st vt os v hls  |  hasTopLink hls        = ...
\end{code}
Next we explain the two cases of |put| in detail.

\subsubsection{No Link at the Top}
In this case we need to choose a inductive rule such that the view matches the right-hand side pattern of the clause.
This is done by an auxiliary function |selSPat| that tries to find such a clause and returns (the encoding of) its source pattern.
This source pattern is then passed into another auxiliary function |putNoLink|:
\begin{code}
put st vt os v hls | not (hasTopLink hls) = putNoLink st vt (selSPat st vt v) os v hls

putNoLink :: (s :: STypes) -> (v :: VTypes) -> SPat -> s_0 -> v -> [HLink] -> s
selSPat   :: STypes -> (v :: VTypes) -> v -> SPat
\end{code}

The |putNoLink| function is defined mutually recursively with |put|.
It handles the recursive calls on the subtrees, and eventually creates a new source that has the specified source pattern.
For each inductive rule |P (VARS x) ~ Q (VARS y)| for synchronising data between source type |st| and view type |vt|, the following |putNoLink| is generated:
\begin{code}
GenPutNoLink2 (st,vt) (P (VARS x) ~ Q (VARS y)) =
  putNoLink (MkTag st) (MkTag vt) (MkPat P) os Q hls = P (SUBST z x)
    itWhere
      prefX_i   = Pref (x_i ; P)
      prefY_i   = Pref (y_i ; Q)
      hls_i     = map (delPrefH ([],prefY_i)) (filterLink prefY_i hls)
      z_i       = put (CTypeOf (x_i; P)) (CTypeOf (y_i;Q)) os y_i hls_i
\end{code}
\begin{enumerate}


  \item We recursively invoke |put| on each subtree~|y_i| of the view with a new environment |env_i|, to produce a consistent new source |z_i|.
  |env_i| is obtained by first dividing |env| into disjoint parts distinguished by the prefix path of $y_i$ ($\tit{prefY}_i$) and then deleting the prefix path.

  \item As specified, we create a new source using the pattern~|P|, and use $\vv{|z|}$ as its subtrees.
\end{enumerate}

\begin{example}
Given inductive rule |Plus _ x y ~ Add x y|, the following Haskell code is generated for |putNoLink|:
\begin{code}
putNoLink ExprTag ArithTag S_ExprArithCase0 os (Add xV yV) hls = Plus "X" xSRes ySRes
  where
    prefxS   = [1]
    prefxV   = [0]
    hlsxV    = map (delPrefH ([], prefxV)) (filterLink prefxV hls)
    xSRes    = put ExprTag ArithTag os xV hlsxV

    prefyS   = [2]
    prefyV   = [1]
    hlsyV    = map (delPrefH ([], prefyV)) (filterLink prefyV hls)
    ySRes    = put TermTag ArithTag os yV envyV
\end{code}
Similar to the generated code for |get| in \autoref{example:get_code}, there are two code blocks for handling variables |x| and |y| in the inductive rule respectively.
% Note that |"X"| in the result corresponds to the wildcard pattern in |Plus _ x y|.
Since Haskell does not support dependent types, in fact we need to constantly convert the view and the new source from and to |Dynamic| values.
However, to improve readability, here we intentionally removed the code for handling |Dynamic|s.
\end{example}



\subsubsection{Links at the Top} In addition to creating a consistent source, for each link |hl_0| at top of the view, we need to copy the source fragment in the original source to which |hl_0| connects, so that later |get| can establish a horizontal link |hl| between the newly created source and the view, as required by retentiveness.

What makes the situation complex is that, there can be more than one links connected to the top of the view, and we need to handle all of them to avoid |putWithLinks| invoking itself.
These links consist of at most one real link, and possibly many imaginary links.
We sort the links according to their paths in the source, and handle them one by one in a bottom-up way.
The real link is handled by the case analysis block and the imaginary links are handled by the |foldr|.
Finally |mkInj| is called because the desired output source type might be different from the type of the source fragment connected by a link.

\begin{code}
put st vt os v env | hasTopLink env = putWithLinks st vt os v env

putWithLinks :: (s :: STypes) -> (v :: VTypes) -> s_0 -> v -> Env -> s
putWithLinks st vt os v env = s_4
  itWhere
    (ml, imags, env') = getTopLinks env

    s_2 = itCase ml itOf
      Just l   -> itLet  ((sPat,sPath), (vPat,[]))  = l
                         s_0                = fetch l os
                         s_1                = putNoLink (typeOf s_0) vt sPat os v env'
                  itIn   repSubtree sPat s_0 s_1
      Nothing  -> putNoLink st vt (selSPat st vt v) os v env'

    s_3  = foldr (splice os) s_2 imags
    s_4  = mkInj st (typeOf s_3) s_3
\end{code}

\paragraph{Handling the Possible Real Link |ml|}
\begin{itemize}
  \item If the real link |ml| exists, we need to fetch the source fragment |s_0| indicated by |ml| from the original source and recursively invoke |put| on the subtrees (holes) of |s_0|.
However, invoking |put| on subtrees requires us to do much redundant work very similar to what |putNoLink| does, so that we decide to reuse |putNoLink| to avoid this:
We invoke |putNoLink| again on the current view without top-level links to produce a source |s_1|, which has complete subtrees (there are no holes in |s_1|) but is not retentive at the top-level node.
Thus by replacing holes in |s_0| with complete subtrees in |s_1|, we get the final result that is both retentive at the top-level node and at subtrees. This is illustrated in \autoref{fig:repSubtree} and the code generation for |repSubtree| is:
%
\begin{code}
repSubtree :: SPat -> (s_0 :: STypes) -> (s_0 :: STypes) -> s_0

GenRepSubtree (P (VEC x) ~ Q (VEC y)) =
  repSubtree (MkPat P) (MkRegPat P) (P (VEC x)) = (MkRegPat P) (VEC x)
\end{code}
%
where |MkRegPat| differs |MkPat| in the sense that it convert the variable patterns into wildcard patterns and convert wildcard patterns into (fresh) variable patterns.
For instance, |MkRegPat (Plus _ x y)| gives |Plus fromWild0 _ _| \,.
We have seen this function when we define regions previously.

  \item If there is no real link meaning nothing to be retained (at the top), we just need to invoke |putNoLink| with the new environment $\tit{env}'$.
  % \todo{Ideally, |putNoLink| should be invoked with the source type and source pattern of the last imaginary link in the list |imagfs|, if it exists. Because ... subtle stuff. irrelavent to retentiveness.}
\end{itemize}

\begin{figure}[tbh]
\includegraphics[scale=0.38,trim={2cm 10cm 1cm 10cm},clip]{pics/repsubtree.pdf}
\caption{Illustration of |repSubtree|. (\footnotesize{Suppose that |sPat| is the source pattern of the inductive rule |Plus _ x y ~ Add x y|. Then the tree |s_0| is correct (i.e., retentive) at the annotation field (marked by the wildcard in the source pattern) while has holes at subtrees marked by |x| and |y|. |s_1| is like a `complement' of |s_0|. We can get a completely correct tree by replacing holes in |s_0| with corresponding subtrees from |s_1|.)}}
\label{fig:repSubtree}
\end{figure}




\paragraph{Handling Imaginary Links |imags|}
  Remember that |get| in general is not an injection, and some parts of a source are discarded when producing its view.
  These parts can also be retained, by passing imaginary links to the |put| function.
  Each imaginary link is handled in a way very similar to a real link, as the |splice| function below shows.
  Thus the process of handling all the imaginary links can be expressed by a |foldr|, in which the (imaginary) link with longest path in the original source is dealt with first.
  % , so that the horizontal links established as the witness of retentiveness in the recursive process (of |foldr|) can be easily maintained.
%
\begin{code}
splice os imag s_0 = insSubtree sPat (fetch imag os) s_0

insSubtree :: SPat -> (s_1 :: STypes) -> s_0 :: STypes -> s_1
GenInsSubtree (P x ~ Q y) =
  insSubtree (MkPat (P x)) s_1 s_0 = P s_2
    itWhere   s_2  =  mkInj (CTypeOf (x ; P x)) (typeOf s_0) s_0
\end{code}
%
Every time a source fragment is fetched, we need to insert the accumulated source |s_0| into it as its subtree (by |insSubtree|).
%
Since |insSubtree| is invoked by |splice| and |splice| only handles imaginary links, we just need to generate the code of |insSubtree| for those particular inductive rules, whose left-hand sides are possible to be source patterns of imaginary links.
% Those particular inductive rules, for example |Term _ t ~ t| and |Paren _ e ~ e|, have the following characteristics: their right-hand sides are a single variable, representing a tree |get| from a subtree marked by the same variable in the left-hand sides.
Those particular inductive rules, for example |Term _ t ~ t| and |Paren _ e ~ e|, have the characteristics that there is only one hole in their left-hand sides and we precisely know where the hole is.
Thus inserting a subtree |s_2| into a tree |P| (with only one hole) is represented by |P s_2| in the above code.
Before the insertion, we need to convert the subtree to be inserted to the same type of the hole and meanwhile update the accumulated links.
This is performed by |mkInj|. (explained below)
The whole process is illustrated in \autoref{fig:insSubtree}.



\begin{figure}[tbh]
\includegraphics[scale=0.38,trim={2cm 10cm 2cm 5cm},clip]{pics/insSubtree.pdf}
\caption{Illustration of |insSubtree (Paren _ e) s_1 s_0|. \footnotesize{We insert |s_0| to the hole in |s_1| according to the pattern |Paren _ x| of |s_1|.
The hole for insertion is the subtree indicated by the (only) variable |e| in the pattern.
However, since the type of |s_0| might not match the type of the hole, we need to run |mkInj| first.}}
\label{fig:insSubtree}
\end{figure}


\paragraph{Making Type Correct}
\label{paragraph:make_inj}
Importantly, the source after handling imaginary links might not be of the correct type |st| though.
So functions such as |insSubtree|, |putWithLinks| need to invoke |mkInj| lastly to make the output type correct.
This |mkInj| function is supposed to make changes that are discarded by |get| (for establishing Correctness) and do not disrupt retentiveness already established for the subtrees (for establishing the overall Retentiveness):
\begin{code}
mkInj :: (sup :: STypes) -> (sub :: STypes) -> sub -> sup

get_v s                =  get_v (mkInj_s _ _ s)
fst linkComp get_l s   fsubseteq fst linkComp get_l (mkInj _ _ s)
\end{code}
