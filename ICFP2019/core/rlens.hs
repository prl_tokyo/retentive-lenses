data BTree a = Emp | BTree a (BTree a) (BTree a)

data Pattern a = Wildcard | Var String | Data a (Pattern a) (Pattern a) | EmpPat

type Path = [Int]

toPattern :: BTree a -> Pattern a
toPattern Emp = EmpPat
toPattern (BTree a x y) = Data a (toPattern x) (toPattern y)

sel :: BTree a -> Path -> BTree a
sel t [] = t
sel (BTree d l r) (0 : ps) = sel l ps
sel (BTree d l r) (1 : ps) = sel r ps

isMatch :: Eq a => Pattern a -> BTree a -> Bool
isMatch Wildcard _ = True
isMatch (Var _)  _ = True
isMatch (Data d pl pr) (BTree x xl xr) = x == d && isMatch pl xl && isMatch pr xr
isMatch EmpPat Emp = True
isMatch _ _ = False


decompose :: Pattern a -> BTree a -> String -> BTree a
decompose p t x = sel t (path p x)

reconstruct :: Pattern a -> (String -> BTree a) -> BTree a
reconstruct Wildcard f = undefined
reconstruct (Var s) env = env s
reconstruct (Data a l r) env = BTree a (reconstruct l env) (reconstruct r env)
reconstruct EmpPat env = Emp


vars :: Pattern a -> [String]
vars Wildcard = []
vars (Var x) = [x]
vars (Data _ l r) = vars l ++ vars r
vars EmpPat = []

path :: Pattern a -> String -> Path
path (Var x) s = if s == x then [] else undefined
path Wildcard s = undefined
path EmpPat s = undefined
path (Data _ l r) s = if s `elem` vars l then 0 : path l s
                                         else 1 : path r s

eraseVars :: Pattern a -> Pattern a
eraseVars (Var _) = Wildcard
eraseVars Wildcard = Wildcard
eraseVars (Data a l r) = Data a (eraseVars l) (eraseVars r)
eraseVars EmpPat = EmpPat

fillWildcards :: Pattern a -> BTree a -> Pattern a
fillWildcards Wildcard t = toPattern t
fillWildcards (Var x) _ =  Var x
fillWildcards (Data a l r) (BTree y l' r') = Data a (fillWildcards l l') (fillWildcards r r')
fillWildcards EmpPat _ = EmpPat

fillWildcardsWithDefaults :: Pattern a -> Pattern a
fillWildcardsWithDefaults Wildcard  = EmpPat
fillWildcardsWithDefaults (Var x)  =  Var x
fillWildcardsWithDefaults (Data a l r)  = Data a (fillWildcardsWithDefaults l) (fillWildcardsWithDefaults r)
fillWildcardsWithDefaults EmpPat  = EmpPat

newtype Rel a = Rel [(Pattern a, Pattern a)]

data Link a = Link (Pattern a, Path) (Pattern a, Path)


data Assumptions a = Assumptions { srcCoverage :: BTree a -> (Pattern a, Pattern a), viewCoverage :: BTree a -> (Pattern a, Pattern a) }

get :: (Eq a) => Rel a -> Assumptions a -> BTree a -> (BTree a, [Link a])
get (Rel r) asp s = (reconstruct vpat (fst . vls), lroot : links)
  where (spat, vpat) = srcCoverage asp s
        vls = get (Rel r) asp . decompose spat s
        spat' = eraseVars (fillWildcards spat s)
        lroot = Link (spat', []) (eraseVars vpat, [])
        links = [ Link (a, path spat v ++ b) (c, path vpat v ++ d)
                    | v <- vars vpat, (Link (a,b) (c, d)) <- snd (vls v) ]


put :: (Eq a) => Rel a -> Assumptions a -> BTree a -> BTree a -> [Link a] -> BTree a
put r asp s v ls =
  case topLink ls of
    Nothing -> let (spat, vpat) = viewCoverage asp v
                   vs = decompose vpat v
                   ss t = put r asp s (vs t) (divide (path vpat t) ls)
                   spat' = fillWildcardsWithDefaults spat
               in reconstruct spat' ss
    l@(Just (Link (spat, spath) (vpat, vpath))) -> _

divide :: Path -> [Link a] -> [Link a]
divide prefix ls = filter (\(Link (_, _) (_, vpath)) -> take (length prefix) vpath == prefix) ls


topLink :: [Link a] -> Maybe (Link a)
topLink ls = safeHead (filter (\(Link (_, _) (_, vpath)) -> vpath == []) ls)
  where safeHead :: [a] -> Maybe a
        safeHead [] = Nothing
        safeHead (a:_) = Just a
