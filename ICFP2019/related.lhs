\section{Related Work}
\label{sec:related_work}

\subsection{Alignment}
\label{subsec:alignment}

From the dawn of the bidirectional programming languages~\cite{Foster2007Combinators}, \emph{alignment} has been recognised as an important problem when we need to synchronise two lists---if a view element (in a list) is modified (e.g., inserted, deleted, or reordered), which source element should be matched with the (modified) view element and updated correspondingly? Our work on retentive lenses with links is closely related to this.

\subsubsection{Alignment for Lists}
The earliest lenses \cite{Foster2007Combinators} only allow source and view elements to be matched positionally---the $n$-th source element is simply updated using the $n$-th element in the modified view. Later, lenses with more powerful matching strategies are proposed, such as dictionary lenses \cite{Bohannon2008Boomerang} and their successor matching lenses \cite{Barbosa2010Matching}. In matching lenses, a source is divided into a \emph{resource} consisting of \emph{chunks} of information that can be reordered, and a `rigid complement' storing information outside the chunk structure; the reorderable chunk structure is preserved in the view.
When a |put| is invoked, it will first do source-view elements matching, which finds the correspondence between chunks of the old and new views using some predefined strategies; based on the correspondence, the resource is `pre-aligned' to match the chunks in the new view. Then element-wise updates are performed on the aligned chunks. The design of matching lenses is to be practically easy to use, so they are equipped with a few fixed matching strategies (such as greedy align) from which the user can choose. However, whether the information is retained or not, still depends on the lens applied after matching. As a result, the more complex the applied lens is, the more difficult to reason about the information retained in the new source. (Moreover, they suffer from a disadvantage that the alignment is only between a single source list and a single view list.)

\scName{BiFluX}~\cite{Pacheco2014BiFluX} not only provides the user with align-by-position and align-by-key matching strategies as two primitives but also allows the user to write her/his own alignment strategies. In this way, when we see several lists at once, we are free to search for elements and match them in all the lists. But this alignment still has some limitations: (i) it works only for list-like data (data that can be flattened into a list); (ii) each source element and each view element can only be matched at most once---after that they are classified as either \emph{matched pair}, \emph{unmatched source element}, or \emph{unmatched view element}. Assuming that an element in the view has been copied several times, there is no way to align all the copies with the same source element. (However, it is possible to reuse an element several times for the handling of unmatched elements.)

In contrast, retentive lenses are designed to abstract out matching strategies (alignment) and are more like taking the result of matching as an additional input. This matching is not a one-layer matching but rather, a global one that produces (possibly all the) links between a source's and a view's unchanged parts. The information contained in the linked parts is preserved independently of any further applied lenses.


\subsubsection{Alignment for Containers}
To generalise list alignment, a more general notion of data structures called \emph{containers}~\cite{Abbott-containers} is used \cite{Hofmann2012Edit}.
In the container framework, a data structure is decomposed into a \emph{shape} and its \emph{content}; the shape encodes a set of positions, and the content is a mapping from those positions to the elements in the data structure.
The existing approaches to container alignment take advantage of this decomposition and treat shapes and contents separately.
For example, if the shape of a view container changes, \citeauthor{Hofmann2012Edit}'s approach will update the source shape by a fixed strategy that makes insertions or deletions at the rear positions of the (source) containers.
By contrast, \citeauthor{Pacheco2012Delta}'s method permits more flexible shape changes, and they call it \emph{shape alignment}~\cite{Pacheco2012Delta}.
In our setting, both the consistency on data and the consistency on shapes are specified by the same set of consistency declarations.
In the |put| direction, both the data and shape of a new source is determined by (computed from) the data and shape of a view, so there is no need to have separated data and shape alignments.

Container-based approaches have the same situation (as list alignment) that the retention of information is dependent on the basic lens applied after alignment.
Besides, as a generalisation of list alignment, it is worth noting that separation of data alignment and shape alignment will hinder the handling of some algebraic data types.
First, in practice, it is usually difficult for the user to define container data types and represent their data using containers.
We use two mutually recursive data types \lstinline{Expr} and \lstinline{Term} defined in \autoref{fig:runningExpDataTypeDef} to illustrate. If the user wants to use containers to define them, one way might be to parametrise the types of terminals (leaves in a tree, here \lstinline{Integer} only):
\begin{centerTab}
\begin{lstlisting}
data Expr i  = Plus  (Expr i) (Term i)
             || Minus (Expr i) (Term i)
             || FromT (Term i)
data Term i  = Neg   (Term i)
             || Lit   i
             || Paren (Expr i)
data Arith i = Add   (Arith i) (Arith i)
             || Sub   (Arith i) (Arith i)
             || Num i
\end{lstlisting}
\end{centerTab}
Here the terminals are of the same type \lstinline{Integer}. However, imagine the situation where there are more than ten types of leaves, it is a boring task to parameterise all of them as type variables.

Moreover, the container-based approaches face another serious problem: they always translate a change on data in the view to another change on data in the source, without affecting the shape of a container. This is wrong in some cases, especially when the decomposition into shape and data is inadequate. For example, let the source be \lstinline{Neg (Lit 100)} and the view \lstinline{Sub (Num 0) (Num 100)}. If we modify the view by changing the integer \lstinline{0}~to~\lstinline{1} (so that the view becomes \lstinline{Sub (Num 1) (Num 100)}), the container-based approach would not produce a correct source \lstinline{Minus ...}, as this data change in the view must not result in a shape change in the source. In general, the essence of container-based approaches is the decomposition into shape and data such that they can be processed independently (at least to some extent), but when it comes to scenarios where such decomposition is unnatural (like the example above), container-based approaches can hardly help.



%\subsection{Least Changes and Least Surprise}
%\todo{consider removing the whole subsections. Not so relevant. Save spaces.}
%
%As we have already seen, usually there could be infinite different modifications to an outdated source for bringing it consistent with a given view again.
%It is reasonable to ask if there exist some algorithm which updates the old source into a consistent state with `minimum changes', or bring the user `least surprise' according to some objective function.
%
%\subsubsection{Side-effects in databases}
%In database communities, a variant (but very similar) of the problem is well-studied and researchers have shown some conclusive theorems.
%We call it a variant because the problem permits the input view $v$ to be invalid---not in the range of the query function.
%%
%Later the update function will both update the source and correct the view $v$. % new (valid) view.
%For example, assume |[(a,1) (a,2), (b,1), (b,2)]| in a view are formed by a query joining |[a,b]| and |[1,2]| in a source.
%Then if we delete |[a,1]| only, obviously the new view is not in the range of the query regardless how we modify the source.
%%
%The only solution is to modify both the source and the view simultaneously (or, in their setting, modify the source and regenerate a new view from the modified source), and the additional modifications to the view are regarded as \emph{view side-effect}.
%%
%So the measurement of the view side-effect problem is set to be the number of tuples to be deleted in the view (precisely, excluding the necessary ones) given an update on a source. \cite{Cheney:2009:PDW:1576265.1576266}.
%%
%Similarly, we can define another objective function which cares more about the number of tuples to be deleted in the source and thus is called \emph{source side-effect} problem \cite{Cheney:2009:PDW:1576265.1576266}.
%%
%For this example, given the removal of |(a,1)|, the minimum change on the source can be either to remove |a| or to remove |1|.
%And hence the minimum source side-effect is |1|.
%The removal of |a| or (|1|) from the source will cause another tuple |(a,2)| (or |(b,1)| if |1| is removed from the source) in the view to be deleted as well.
%So the minimum view side-effect is also |1|.
%%
%\todo{Seems handling deletion only?}
%The example is shown by deletions in the view, and in general, side-effect for insertions and replacements are also studied.
%
%For the view side-effect problem and source side-effect problem, they are both NP-hard for queries involving join and either projection or union, and polynomial-time solvable for the remaining queries in the class \cite{Buneman:2002:PDA:543613.543633, Keller:1986:CVU:645913.671458, Cheney:2011:PDA:2139690.2139696}.
%%
%\citeauthor{Dayal:1982:CTU:319732.319740} also introduce the notion of \emph{clean source}: a source is clean if it is not a source of any other tuple (usually a source not involving join operation).
%Clean source enjoys a property that the translation from modifications to a view to the update on it can be side-effect free \cite{Dayal:1982:CTU:319732.319740, Cheney:2011:PDA:2139690.2139696}.
%
%\subsubsection{Side-Effects in BXs}In relational databases, the data is represented as (unordered) tuples, and the operations on the data are usually selection, projection, join, and union.
%However, in the world of BXs, data is trees defined by algebraic data types and operations are any
%well-behaved functions,
%which bring difficulties in defining objective functions for calculating `side-effects' and producing a least-changed new source.
%As the paper \cite{cheney2015towards} pointed out, different applications indeed require different metric (of changes).
%Most pre-defined metric, for example, the graph edit distance, is not a particularly good match (to calculate matched source and view components) for any user's intuitive idea of distance.
%The authors in \cite{cheney2015towards} are `pessimistic about whether usably efficient algorithms that guarantee to find optimal solutions to the least change problem will ever be available'.
%Based on this former research, we decided to invent a framework letting the user (or tools) explicitly describing unchanged parts during the transformation.
%Once some parts are connected by links, they are guaranteed to exist in the newly created source.
%
%\todo{Composing Least-change Lenses \cite{macedo2013composing}}
%% Cheney and Jeremy: Using a similar structure, Macedo et al. [MPCO13] address the tricky question of when it is possible to compose bx that satisfy such a least change condition. As might be expected, they have to abandon determinacy (so that the composition can choose “the right path” through the middle model), and impose stringent additional conditions; fundamentally, there is no reason why we would expect bx that satisfy this kind of least change principle to compose.
%
%% Then they discuss whether there exists precise translation for the modifications in the view to the update in the source.
%% A translation is precise, if it modifies source $s$ to $s'$ and $\textit{query}\ s' = v'$.
%% Intuitively, if view $v'$ is invalid under the $\textit{query}$ function, there is no precise translation---all translations will have side effects on $v'$ and make it become $v''$ after query on the new source.


\subsection{Provenance and Origin}
\label{subsec:provenance}

Our idea of links is inspired by research on provenance \cite{Cheney2009Provenance} in database communities and origin tracking \cite{vanDeursen1993Origin} in the rewriting communities.

\citeauthor{Cheney2009Provenance} classify provenance into three kinds, \emph{why}, \emph{how}, and \emph{where}: \emph{why-provenance} is the information about which data in the view is from which rows in the source; \emph{how-provenance} additionally counts the number of times a row is used (in the source); \emph{where-provenance} in addition records the column where a piece of data is from. In our setting, we require that two pieces of data linked by vertical correspondence be equal (under a specific pattern), and hence the vertical correspondence resembles where-provenance.
Leaping from database communities to programming language communities, we find that the above-mentioned provenance is not powerful enough as they are mostly restricted to relational data, namely rows of tuples. In functional programming, the algebraic data types are more complex (represented as sums of products), and a view is produced by more general functions rather than relational operations such as selection, projection, and join. For this need, \emph{dependency provenance} \cite{Cheney2011Provenance} is proposed; it tells the user on which parts of a source the computation of a part of a view depends. In this sense, our consistency links are closer to dependency provenance.

The idea of inferring consistency links can be found in the work on origin tracking for term rewriting systems~\cite{vanDeursen1993Origin}, in which the origin relations between rewritten terms can be calculated by analysing the rewrite rules statically.
However, it was developed solely for building traces between intermediate terms rather than using trace information to update a tree further. Based on origin tracking, \citeauthor{deJonge2012Algorithm} implemented an algorithm for code refactoring systems, which `preserves formatting for terms that are not changed in the (AST) transformation, although they may have changes in their subterms'~\cite{deJonge2012Algorithm}.
This description shows that the algorithm decomposes large terms into smaller ones that resemble our regions. Therefore, in terms of the formatting aspect, we think that retentiveness can be in effect the same as their theorem if we adopt the `square diagram' (see \autoref{sec:triangularDiagram}). However, they only tailored the theorem for their specific printing algorithm but did not generalise the theorem to other scenarios.


Similarly, \citeauthor{Martins2014Generating} developed a system for attribute grammars which define transformations between tree structures (in particular CSTs and ASTs)~\cite{Martins2014Generating}. Their bidirectional system also uses links to trace the correspondence between source nodes and view nodes, which is later used by |put| to solve the syntactic sugar problem.
The differences between their system and ours are twofold: One is that in their system, links are implicitly used in the |put| direction. The advantage (of implicit link usage) is that, for the user, links become transparent and are automatically maintained when a view is updated; the disadvantage is that, as a result, newly created nodes on an AST can never have `links back' to the old CST, even if they might be the copies of some old nodes.
The second difference is the granularity of links; in their system, a link seems to connect the whole subtrees between a CST and an AST instead of between smaller regions. As a result, if a leaf of an AST is modified, all the nodes belonging to the spine from the leaf to the root will lose their links.

The use of consistency links can also be found in \citeauthor{Wang2011Incremental}'s work, where the authors extend state-based lenses and use links for tracing data in a view to its origin in a source~\cite{Wang2011Incremental}.
When a sub-term in the view is edited locally, they use links to identify a sub-term in the source that `contains' the edited sub-term in the view. When updating the old source, it is sufficient to only perform state-based |put| on the identified sub-term (in the source) so that the update becomes an incremental one. Since lenses generated by our DSL also create consistency links (albeit for a different purpose), they can be naturally incrementalised using the same technique.


\subsection{Operation-based BX}
\label{subsec:operational_bx}
%View-Update Translator}
% \subsubsection{In the Database Community}
% In the database community, there are notions of \emph{updatable views}, and much research has focused on transforming an update on the view side to an update on the source side.
% %
% For example, \cite{Bancilhon1981} introduce the notion of \emph{constant complement} in relational databases:
% Given a database~|s|, a query~|f| and a view |v| queried by |f|, all `the information not visible within |v|' \cite{Bancilhon1981} must reside in the |v|'s complement created by an |f|'s complement function |g|.
% The function |g| should satisfy the property that |(f {-"{\triangle}\;"-} g) s = (f s, g s)| is injective.
% That is, the original database is decomposed into a view and its complement, the latter of which is kept constant; when the view is updated, it is paired with the constant complement and fed into $(f \mathop\triangle g)^{-1}$ to compute an updated database.
% The concept is useful and leads to some applications, such as \emph{bidirectionalisation} (of a query function) in the BX community~\cite{Matsuda:2007:BTB:1291151.1291162}.
% %
% However, there are also inadequacies that make the approach not very practical.
% On the one hand, the time complexity for finding a minimum complement is NP-complete \cite{Cosmadakis:1984:URV:1634.1887};
% on the other hand, finding a complement which might be `arbitrarily large' is not that useful, because the data (in the source) captured by the complement function |g| cannot be changed after an update (hence the name constant complement).
% In extreme cases, if the complement is set to be as large as the whole source, then nothing could be updated.
% %
% % Second, since in (traditional) relational databases the data is only tuples, the method deals with product types only but cannot handle handle sum types.
% % Because for sum types there is even no constant complement in general. (See \autoref{sec:putput}.)
%
%
% \subsubsection{In the BX Community}

%The BX community has also considered translating view updates to source updates to achieve bidirectionality.
Our work is relevant to the operation-based approaches to BX, in particular, the delta-based BX model \cite{Diskin2011Asymmetric,Diskin2011Symmetric} and edit lenses~\cite{Hofmann2012Edit}. The (asymmetric) delta-based BX model regards the differences between a view state |v| and |v'| as \emph{deltas}, which are abstractly represented as arrows (from the old view to the new view).
The main law of the framework can be described as `given a source state |s| and a view delta $|det|_|v|$, $|det|_|v|$ should be translated to a source delta $|det|_|s|$ between |s| and $s'$ satisfying |get s' = v'|'.
%
As the law only guarantees the existence of a source delta $|det|_|s|$ that updates the old source to a correct state, it is yet not sufficient to derive Retentiveness in their model, for there are infinite numbers of translated delta $|det|_|s|$ which can take the old source to a correct state, of which only a few are `retentive'.
To illustrate, \citeauthor{Diskin2011Asymmetric} tend to represent deltas as edit operations such as \emph{create}, \emph{delete}, and \emph{change}; representing deltas in this way will only tell the user what must be changed in the new source, while it requires additional work to reason about what is retained.
However, it is possible to exhibit Retentiveness if we represent deltas in some other proper form. Compared to \citeauthor{Diskin2011Asymmetric}'s work, \citeauthor{Hofmann2012Edit} give concrete definitions and implementations for propagating edit operations (in a symmetric setting).