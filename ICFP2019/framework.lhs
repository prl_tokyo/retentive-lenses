% !TEX root = retentive.lhs

\section{Retentive Lenses for Trees}
\label{sec:framework}

In this section, we will start by introducing a `region model' for decomposing trees (\autoref{subsec:intuitive_retentiveness}), and providing a high-level sketch of what retentive lenses should do (\autoref{sec:intuitive_sketch}).
After that, we develop a formal definition of links (\autoref{sec:linkDefs}) and retentive lenses (\autoref{sec:rlensDef}) for algebraic data types, through revising classic lenses~\cite{foster2007combinators} by
\begin{itemize}
\item extending |get| and |put| to incorporate links---specifically, we will make |get| return a collection of consistency links, and make |put| additionally take a collection of input links---and
\item adding a finer-grained law \emph{Retentiveness}, which formalises the statement `if parts of the view are unchanged then the corresponding parts of the source should be retained'.
\end{itemize}
To be concrete, we will use the concrete and abstract representations of arithmetic expressions as the running example throughout the paper.


\begin{figure}[t]
\setlength{\mathindent}{0em}
\begin{small}
\begin{minipage}[t]{0.45\textwidth}
\begin{center}
\begin{code}
data Expr   =  Plus   Annot  Expr  Term
            |  Minus  Annot  Expr  Term
            |  FromT  Annot  Term

data Term   =  Lit    Annot  Int
            |  Neg    Annot  Term
            |  Paren  Annot  Expr

type Annot  =  String

data Arith  =  Add ^  Arith  Arith
            |  Sub ^  Arith  Arith
            |  Num ^  Int
\end{code}
\end{center}
\end{minipage}%
%
\begin{minipage}[t]{0.45\textwidth}
\begin{center}
\begin{code}
getE :: Expr -> Arith
getE (Plus   _ e  t) = Add  (getE e) (getT t)
getE (Minus  _ e  t) = Sub  (getE e) (getT t)
getE (FromT  _    t) = getT t

getT :: Term -> Arith
getT (Lit     _ i  ) = Num i
getT (Neg     _ t  ) = Sub (Num 0) (getT t)
getT (Paren   _ e  ) = getE e
\end{code}
\end{center}
\end{minipage}
\end{small}
\vspace{-2ex}
\caption{Data types for concrete and abstract syntax of the arithmetic expressions and the consistency relations between them as |getE| and |getT| functions in Haskell.}
\label{fig:running_example_datatype_def}
\end{figure}
%


\subsection{Regions of Trees}
\label{subsec:intuitive_retentiveness}
% Let us first get familiar with the concrete and abstract representations of arithmetic expressions---which our examples are based on---as defined in \autoref{fig:running_example_datatype_def}, where the definitions are in Haskell.
% The concrete representation is either an expression of type |Expr|, containing additions and subtractions; or a term of type |Term|, including numbers, negated terms, and expressions in parentheses.
% Moreover, all the constructors have an annotation field of type |Annot| for holding comments.
% The two concrete types |Expr| and |Term| coalesce into the abstract representation type |Arith|, which does not include explicit parentheses, negations, and annotations.
% The consistency relations between CSTs and ASTs are defined in terms of the |get| functions---|e :: Expr| (resp.~|t :: Term|) is consistent with |a :: Arith| exactly when |getE e = a| (resp.~|getT t = a|).


% Having explained the problem of in its simplest form in the introduction, we shall introduce the running example throughout the paper.
%
Let us first get familiar with the concrete and abstract representations of arithmetic expressions---which our examples are based on---as defined in \autoref{fig:running_example_datatype_def}, where the definitions are in Haskell.
%
The concrete representation is either an expression of type |Expr|, containing additions and subtractions; or a term of type |Term|, including numbers, negated terms, and expressions in parentheses.
%
Moreover, all the constructors have an annotation field of type |Annot| for holding comments.
The two concrete types |Expr| and |Term| coalesce into the abstract representation type |Arith|, which does not include annotations, explicit parentheses, and negations---negations are considered as \emph{syntactic sugar} and represented by some other more fundamental construct, here |Sub|, in the AST.
%
The consistency relations between CSTs and ASTs are defined in terms of the |get| functions---|e :: Expr| (resp.~|t :: Term|) is consistent with |a :: Arith| exactly when |getE e = a| (resp.~|getT t = a|).


As mentioned in the introduction, the core idea of Retentiveness is to use links to relate parts of the source and view.
For trees, an intuitive notion of a `part' is a \emph{region}, by which we mean a fragment of a tree at a specific location and whose data are described by a \emph{region pattern} that consists of wildcards, constructors, and constants.
%
For example, in \autoref{fig:swap_and_put}, the grey areas are some of the possible regions:
The topmost region in |cst| is described by the region pattern |Plus "a plus" _ _|\,, which says that the region includes the |Plus| node and the annotation |"a plus"|, but not the other two subtrees with roots |Minus| and |Neg| matched by the wildcards.
This is one particular way of decomposing trees, and we call this the \emph{region model}.

\subsection{A High-Level Sketch of Retentive Lenses}
\label{sec:intuitive_sketch}
With the region model, the |get| function in a retentive lens not only computes a view but also decomposes both the source and view into regions and produces a collection of links relating source and view regions.
We call this collection of links produced by |get| the \emph{consistency links} because they are a finer-grained representation of how the whole source is consistent with the whole view.
In \autoref{fig:swap_and_put}, for example, the light dashed links between the source |cst| and the view |ast = getE cst| are two of the consistency links.
(For clarity, we do not draw all the consistency links in the figure---the complete collection of consistency links should fully describe how all the source and view regions correspond.)
The topmost region of pattern |Plus "a plus" _ _|\, in |cst| corresponds to the topmost region of pattern |Add _ _|\, in |ast|, and the region of pattern |Neg "a neg" _|\, in the right subtree of |cst| corresponds to the region of pattern |Sub (Num 0) _|\, in |ast|.

If the view is modified, the links between the source and view can be modified to reflect the latest correspondences between regions.
For example, in \autoref{fig:swap_and_put}, if we change |ast| to |ast'| by swapping the two subtrees under |Add|, then the `diagonal' link relating the |Neg "a neg" _|\, region and the |Sub (Num 0) _|\, region can be created to record that the two regions are still `locally consistent' despite that the source and view as a whole are no longer consistent.
%
In general, the collection of diagonal links between |cst| and |ast'| may be created in many ways, such as directly comparing |cst| and |ast'| and producing links between matching areas, or composing the light red dashed consistency links between |cst| and |ast| and the light blue dashed `vertical correspondence' between |ast| and |ast'|.
How these links are obtained is a separable concern, though, and in this paper we will focus on how to restore consistency assuming the existence of diagonal links.
(We will discuss this issue again in \autoref{dis:tridia}.)

When it is time to put the modified view back into the source, the diagonal links between the source and the (modified) view can provide valuable information regarding what should be brought from the old source to the new source.
The |put| function of a retentive lens thus takes a collection of (diagonal) links as additional input and retains regions in a way that it respects these links.
More precisely, |put| should provide the \emph{triangular guarantee}:
Using \autoref{fig:swap_and_put} to illustrate, if the link relating the |Neg "a neg" _|\, region in |cst| and the |Sub (Num 0) _|\, region in |ast'| is provided as input to |put|, then after updating |cst| to a new source |cst'|, the consistency links between |cst'| and |ast'| should include a link that relates some region in the new source |cst'| and the |Sub (Num 0) _|\, region in the view |ast'|, and that region in |cst'| should have the same pattern as the one specified by the input link, namely |Neg "a neg" _|\,, preserving the syntactic sugar negation (as opposed to changing it to a |Minus|, for example) and the associated annotation.
%

\begin{figure}
\includegraphics[scale=0.6,trim={4cm 3.8cm 4cm 4.2cm},clip]{pics/swap_and_put.pdf}
\caption[The triangular guarantee.]{\footnotesize{The triangular guarantee. (Grey areas are some of the possible regions. Two light dashed lines between |cst| and |ast| are two of the consistency links produced by |get cst|.
When updating |cst| with |ast'|, the region |Neg "a neg" _|\, connected by the red dashed diagonal link is guaranteed to be preserved in the result |cst'|, and |get cst'| will link the preserved region to the same region |Sub (Num 0) _| of |ast'|. )}}
\label{fig:swap_and_put}
\end{figure}

\subsection{Formalisation of Links}
\label{sec:linkDefs}

\subsubsection{Basics of Links}
% Now we formalise our idea of links.
As described in \autoref{sec:intuitive_sketch}, a link relates two regions, and a region consists of a pattern describing the region's content and a location, which can be a path describing how to reach the top of the region from the root.
One idea is to formalise a region as an element of the set |Region|, where |Pattern| is the set of region patterns and |Path| is the set of paths; then a link is an element of the set |Link|:
\begin{align*}
|Region| &= |Pattern| \times |Path| \\[-0.8ex]
|Link| &= |Region| \times |Region| \ \text{.}
\end{align*}
The exact representation of paths is not important and may be different for each application, but to be simpler, let us assume that a path is just a list of natural numbers that indicate which subtree to go into: for instance, starting from the root of |cst| in \autoref{fig:swap_and_put}, the empty path |[]| points to the root node |Plus|, the path |[0]| points to |"a plus"| (which is the first subtree under the root), and the path |[2,0]| points to |"a neg"|.
%
% This simple formalisation does not work too well, however.
% Recall that if |put| receives an input link between the original source and the view, it will need to guarantee that the content of the source region of the link exists in the updated source, while the location of the region within the updated source can be different from its original location.
%
% Therefore, with this definition of links, we cannot say that |put| retains the link between the updated source and the view, because the link describes the source region's location, which may not be retained.


% The above discussion prompts us to use a more elaborate formalisation, in which links refer to locations only indirectly.
% Assuming a set |Name| of identifiers, we define:
% \begin{align*}
% |Property| &= \{\, |HasRegion pat x| \mid x \in |Name| \wedge |pat| \in |Pattern| \,\}\\
% |Locations| &= |Name| \pfun |Path|
% \end{align*}
% where $|A| \pfun |B|$ is the set of partial functions from set~|A| to set~|B|.
% The above discussion leads us to the definition
% \begin{align*}
% |Region| &= (|Pattern|, |Path|)
% % |Property| &= \{\, |HasRegion pat x| \mid |pat| \in |Pattern| \,\}
% \end{align*}
% To describe some regions in a tree, we first choose some locations in the tree and assign names to them by providing an element of |Locations|, i.e.~a partial mapping from identifiers to paths that go from the root to the locations, and then provide a subset of |Property|\footnote{\note{The reader may find it unnecessary to abstract regions to properties; however, later we will introduce other kinds of properties for characterising \emph{strong retentive lenses}.}} describing what regions can be found at those locations---an element |HasRegion pat x| of |Property| is interpreted as saying that the location named~$x$ satisfies the property `there is a region of pattern |pat| at that location'.
% This interpretation makes our formalisation extensible because we can introduce more varieties of properties, for example, `a location is the root of a tree'; we will do so in \autoref{subsec:ui}.

\begin{example}
In \autoref{fig:swap_and_put}, the two shaded regions of |cst| are described by $(|Plus "a plus" _ _| \,, [])$ and $(|Neg "a neg" _| \,, [2])$ respectively.
The diagonal link is represented as $((|Neg "a neg" _|\,, |[2]|),$\\
$(|Sub (Num 0) _|\,, |[0]|))$.
\end{example}


Let $\powerset{A}$ denote the power set of |A|, we define the set of collections of links to be
\[ |LinkSet| = \powerset{|Link|}  \ \text{.} \]


\paragraph{Links as Relations}
For the simplicity of notation, we regard a collection of links |ls : LinkSet| as a relation between $\ldom(|ls|)$~and~$\rdom(|ls|)$, where $\ldom(|ls|) = {\myset{\phi_|s| \mid \exists \phi_|v|.\ (\phi_|s|, \phi_|v|) \in |ls|}}$ and $\rdom(|ls|) = {\myset{\phi_|v| \mid \exists \phi_|s|.\ (\phi_|s|, \phi_|v|) \in |ls|}}$.
Here we introduce some standard notations of relations, which will be used later.
%
The converse of a relation $r^\circ : B \sim A$ relates $b \in B$ and $a \in A$ exactly when $r : A \sim B$ relates $a$~and~$b$.
%
The composition $l \cdot r : A \sim C$ of two relations $l : A \sim B$ and $r : B \sim C$ is defined as usual: $(x, z) \in l \cdot r$ exactly when there exists~$y$ such that $(x, y) \in l$ and $(y, z) \in r$.
%
We will allow functions to be implicitly lifted to relations: a function $f : A \to B$ also denotes a relation $f : B \sim A$ such that $(f\,x, x) \in f$ for all $x \in A$.
This flipping of domain and codomain (from $A \to B$ to $B \sim A$) makes functional composition compatible with relational composition: a function composition $|g| \circ |f|$ lifted to a relation is the same as $|g| \cdot |f|$, i.e.~the composition of |g|~and~|f| as relations.


\begin{example}
\label{ex:link-collection-example}
The diagonal link between |cst| and |ast'| in \autoref{fig:swap_and_put} is represented as the link collection $\{\,(|(Neg "a neg" _|\, , [2]), (|Sub (Num 0) _| \, , [0]))\,\}$ (despite of being a singleton), which can also be regarded as a relation relating $|(Neg "a neg" _|\, , [2])$ to $(|Sub (Num 0) _| \, , [0])$ and vice versa.
\end{example}


% \note{Consider deleting the paragraph.}
% Now we can enrich the types of |get| and |put| functions with links.
% Let |S| and |V| be the \note{sets} of the source trees and the view trees respectively; the pair of |get| and |put| functions of a retentive lens between |S| and |V| are \emph{partial} (denoted by $\pfun$) and have the types
% \begin{align*}
% |get| &: |S| \pfun |V| \times |LinkSet| \\
% |put| &: |S| \times |V| \times |LinkSet| \pfun S \ \text{.}
% \end{align*}
% Here we only give the types of (|get| and |put|) of a retentive lens, which are enough to discuss the restrictions on links.
% We will give the formal definitions of retentive lenses and Retentiveness later.


\subsubsection{Validity of Links}

A collection of links may not make sense for a given pair of source and view trees---for example, a region mentioned by some link may not exist in the trees at all.
We should therefore characterise when a collection of links is valid with respect to a source tree and a view tree: all region patterns on the source side of the links are satisfied by the source tree and all region patterns on the view side are satisfied by the view tree.

\begin{definition}[Link Satisfaction]
\label{def:sat}
For a tree |t| and a set of regions $\Phi \subseteq \powerset{|Region|}$, we say that $t \models \Phi$ (read `|t| satisfies $\Phi$') exactly when
\[ \forall (|pat|, |path|) \in \Phi. \quad |sel|(|t|, |path|) \text{ matches } |pat|\text{,}\]
where $|sel|(|t|, |p|)$ is a partial function that returns the subtree of |t| at the end of the path |p| (starting from the root of |t|), or fails if path |p| does not exist in |t|.
\end{definition}


\begin{definition}[Valid Links]
\label{def:validLinks}
For a collection of links $|ls| \in |LinkSet|$, a source $|s| \in |S|$, and a view $|v| \in |V|$, we say that |ls| is \emph{valid} for |s| and |v|, denoted by $\validLink{|s|}{|v|}{|ls|}$, exactly when
\[ |s| \models \ldom(|ls|) \quad\text{and}\quad |v| \models \rdom(|ls|) \ \text{.} \]
\end{definition}



\begin{example}
The link collection given in \autoref{ex:link-collection-example} is valid for |cst| and |ast'| because $|cst| \models \{\,(|Neg "a neg" _|\, , [2])\,\}$, $|ast'| \models \{\,(|Sub (Num 0) _| \, , [0])\,\}$.
\end{example}


% \begin{example}
% The link collection given in \autoref{ex:link-collection-example} is valid (and weakly valid) for |cst| and |ast'|, and a retentive lens whose |get| function is |getE| (in \autoref{fig:running_example_datatype_def}), because $|cst| \models \{\,(|Neg "a neg" _|\, , [2])\,\}$, $|ast'| \models \{\,(|Sub (Num 0) _| \, , [0])\,\}$, and |getE| indeed relates a source region of pattern |Neg "a neg" _|\, to a view region of pattern |Sub (Num 0) _|\,.
% \end{example}



\subsection{Definition of Retentive Lenses}
\label{sec:rlensDef}

Now we have all the ingredients for the formal definition of retentive lenses.

\begin{definition}[Retentive Lenses]
\label{def:retLens}
For a set |S| of source trees and a set |V| of view trees, a retentive lens between |S| and |V| is a pair of partial (denoted by $\pfun$) functions |get| and |put| of type
\begin{align*}
|get| &: |S| \pfun |V| \times |LinkSet| \\
|put| &: |S| \times |V| \times |LinkSet| \pfun S
\end{align*}
satisfying
\begin{itemize}
\item \emph{Hippocraticness}: if $|get| |(s)| = (|v|, |ls|)$, then $(|s|, |v|, |ls|) \in \dom(|put|)$ and
\begin{align}
\label{law:get}
\validLink{|s|}{|v|}{|ls|} \quad\mathop{\wedge}\quad |put(s, v, ls) = s| \text{ ;}
\end{align}

\item \emph{Correctness}: if $|put| |(s, v, ls) = s'|$, then $|s'| \in \dom(|get|)$ and
\begin{align}
\label{law:correct}
|get| |(s') = (v, ls')| \quad \text{ for some |ls'|;}
\end{align}

\item \emph{Retentiveness}: (continuing above)
\begin{align}
\label{law:retain}
|fst| \cdot |ls| \subseteq |fst| \cdot |ls'|
\end{align}
% where $|fst| : A \times B \to A$ is the first projection function (lifted to relation).
\end{itemize}

\end{definition}

Modulo the handling of links, Hippocraticness and Correctness stay the same as their original forms (of the well-behaved lenses) respectively.
%
Retentiveness further states that when a region pattern (data) is preserved in the updated source, it still corresponds to exactly the same region on the view side, both the region pattern (data) itself and its evidence (path).
Retentiveness formalises the triangular guarantee in a compact way, and we can expand it pointwise to see that it indeed specialises to the triangular guarantee.



\begin{proposition}[Triangular guarantee for regions]
Given |ls|, $(|s|, |v|, |ls|) \in \dom(|put|)$, and
\[\phi_s = (|spat| ,|spath|)\quad \wedge \quad \phi_|v| = (|vpat|, |vpath|)\]
if $(\phi_s, \phi_|v|) \in |ls|$ and $(|v|, |ls'|) = |get|(|put|(|s|, |v|, |ls|))$, then we have
\[ (|spat|, (|vpat|,|vpath|)) \in |fst| \cdot |ls'|. \]
Intuitively, this asserts that the region pattern $|spat|$ still exists in the updated source and corresponds to the same view region $(|vpat|,|vpath|)$ as in the input.
\end{proposition}


\begin{example}
In \autoref{fig:swap_and_put}, if a |put| function takes |cst|, |ast'|, and diagonal links $|ls| = \{\,((|Neg "a neg" _| \, ,$ $[2]) , (|Sub (Num 0) _|\, , [0]))\,\}$ as arguments and successfully produces an updated source~|s'|, then $|get|(|s'|)$ will succeed.
Let $(|v|, |ls'|) = |get|(|s'|)$; we know that we can find a consistency link with the path of its source region pattern removed $c = (|Neg "a neg" _|\, , (|Sub (Num 0) _| \, , [0]))  \in |fst| \cdot |ls'|$.
% can be found in $|fst| \cdot |ls'|$; that is,
So the view region referred to by |c| is indeed the same as the one referred to by the input link, and having $c \in |fst| \cdot |ls'|$ means that the region in~|s'| corresponding to the view region will match the pattern |Neg "a neg" _|\,.
% \note{(since the link collection |ls'| is valid for |s'|~and~|v|)}
\end{example}


Finally, we note that retentive lenses are an extension of the original lenses: every well-behaved lens between trees can be directly turned into a retentive lens (albeit in a trivial way).
\begin{example}[Well-behaved Lenses are Retentive Lenses]
Given a well-behaved lens defined by $g : |S| \rightarrow |V|$ and $p : |S| \times |V| \rightarrow |S|$, we define $|get| : |S| \pfun |V| \times |LinkSet|$ and $|put| : |S| \times |V| \times |LinkSet| \pfun |S|$ as follows:
\begin{gather*}
|get|(|s|) = (g(|s|), |trivial|(|s|, g(|s|))) \\
|put|(|s|, |v|, |ls|) = p(|s|, |v|)
\end{gather*}
where
\[ |trivial|(|s|, |v|) = \{\, ((|ToPat|(|s|) , []) \, , (|ToPat|(|v|) , [])) \,\}. \]
In the definition, |ToPat| turns a tree into a pattern completely describing the tree, and $\dom(|put|)$ is restricted to $\big\{\, (|s|, |v|, |ls|) \mid \validLink{|s|}{|v|}{|ls|} \text{ and } |ls| = |trivial|(|s|, |v|) \text{ or } \emptyset \,\big\}$. The idea is that |get| generates a link collection relating the whole source tree and view tree as two regions, and |put| accepts either the trivial link produced by |get| or an empty collection of links.
% The trivial link generated by |get| is apparently valid
Hippocraticness and Correctness hold because the underlying $g$ and $p$ are well-behaved.
When the input link of |put| is empty, Retentiveness is satisfied vacuously; when the input link is the trivial link, Retentiveness is guaranteed by Hippocraticness of $g$ and $p$, which is obvious seen from the proof in appendix (\ref{prf:wblarl}); Thus |get| and |put| indeed form a retentive lens.
\end{example}


\subsection{Lens Composition}
\label{sec:lensComp}
It is standard to provide a composition operator for composing large lenses from small ones;
in this subsection, we discuss this operator for retentive lenses, which basically follows the definition of composition for well-behaved lenses, except that we need to carefully deal with links additionally.

% \hu{\change{But before that, we will introduce \emph{link composition} first, on which the retentive lens composition depends.
% \paragraph{Link Composition}}

We first introduce some notations that will be used.
We use $(\cdot)$ to denote \emph{link composition} and its (overloaded) lifted version that works on two collections of links.
(Although $(\cdot)$ was used for relation composition before, we have shown that a single link can be lifted to a singleton link collection and a collection of links can be viewed as a relation; so it is reasonable to reuse the same symbol.)
% The direction of the composition is from left to right, which is different from the general function composition operator $(\circ)$.
Also, for simplicity, we use |lens_AB|\footnote{We use |lens| to denote a retentive lens (instead of |rlens|) when it is clear from the context.} to denote a (retentive) lens doing synchronisation between trees of sets |A| and |B|, |l_ab| a link between tree |a| (of set |A|) and tree |b| (of set |B|), and |ls_ab| a collection of links between |a| and |b|.

% \begin{definition}[Link Composition]
% \label{def:linkComp}
% Two links $|l_AB = (sp_AB , vp_AB)|$ and $|l_BC = (sp_BC , vp_BC)|$ are composable if $|vp_AB = sp_BC|$, and the result of the composition is $|l_AC = (sp_AB,vp_BC)|$.
% % Note that we explicitly regulate the direction of the composition.
% \end{definition}

% \begin{definition}[Link Collection Composition]
% A collection of links |ls_ab| is \emph{composable} with another collection of links |ls_bc| if and only if
% \[ \rdom(|ls_ab|) \subseteq \ldom(|ls_bc|) \ \text{,} \]
% or more concretely
% \[ \forall |(_^^^,vp_AB)| \in |ls_ab|,\; \exists |(sp_BC^^^,_)| \in |ls_bc|.\ \ |vp_AB| = |sp_BC| \label{linkCompCond} \ \text{.} \]
% %
% The result of the composition is denoted by
% \[ |ls_ab| \cdot |ls_bc| = \{\, |(sp_AB,vp_BC)| \mid |(sp_AB,vp_AB)| \in |p_AB|, |(sp_BC, vp_BC)| \in |p_BC|, |vp_AB| = |sp_BC| \,\} \ \text{.} \]
% \end{definition}
% We impose the requirement on link (collection) composition in the hope that every individual link in |ls_ab| is extended with some link in |ls_bc| to reach the tree |C|; in other words, the composition should succeed for every individual link in |ls_ab|.
% %
% However, we do not require the condition in its most restrictive form, i.e.~$\rdom(|ls_ab|) = \ldom(|ls_bc|)$; this is because in the retentive lens composition, a collection of input links will often be composed with a collection of consistency links, where the latter has much more links than the former.
% %
% The special restriction on link (collection) composition also gives rise to the following property.


% \note{Additionally, in our region model, since each source region has at most one link, the number of composite links is exactly the same as the number of links in |ls_ab|. Do we really need it in the framework part? Make it a requirement?}

% \begin{proposition}%[Link Collection Composition Left Domain Preservation]
% \label{prop:linkCompDomPrsv}
% Given two collections of links |ls_ab| and |ls_bc|, if |ls_ab| is composable with |ls_bc| and $|ls_ac| = |ls_ab| \cdot |ls_bc|$, then $\ldom(|ls_ac|) = \ldom(|ls_ab|)$.
% \end{proposition}
% \begin{proof}
% \note{A direct result of the link collection composition and its condition.}
% \end{proof}


% Finally, we note that in the definition of \hyperref[def:validLinks]{valid links} and \hyperref[def:retLens]{retentive lenses}, |fst| is used for extracting the (source or view) region patterns.
% %
% |fst| has the property that when it is used as the left operand of a relation composition, it does not change the right-domain of the right operand; when it is used as the right operand of a relation composition, it does not change the left-domain of the left operand.
% This property is generalised as follows.

% \begin{proposition}[Soundness of Link Composition]
% Given trees |A|, |B|, and |C|, and two collections of links |ls_ab| and |ls_ac|.
% If $\validLink{|A|}{|B|}{|ls_ab|}$ and $\validLink{|B|}{|C|}{|ls_bc|}$, we also have $\validLink{|A|}{|C|}{|ls_ac|}$ where $|ls_ac| = |ls_ab| \cdot |ls_bc|$.

% Similarly, given trees |A|, |B|, and |C|, and two collections of links |ls_ab| and |ls_ac|, and lenses |lens_AB| and |lens_AC|.
% If $\strongValidLink{|A|}{|B|}{|ls_ab|}{|lens_AB|}$ and $\strongValidLink{|B|}{|C|}{|ls_bc|}{|lens_AB|}$, we also have $\strongValidLink{|A|}{|C|}{|ls_ac|}{|lens_AC|}$ where $|ls_ac| = |ls_ab| \cdot |ls_bc|$ and $|lens_AC| = |lens_AB|;|lens_BC|$.
% \end{proposition}
% \note{correct? proofs?}

\begin{figure}
\includegraphics[scale=0.65,trim={4cm 7.9cm 10cm 3.9cm},clip]{pics/rlensComp.pdf}
\caption[The |put| behaviour of a composite retentive lens.]{The |put| behaviour of a composite retentive lens $|lens_AB|;|lens_BC|$. \footnotesize{(The process of |put| is divided into steps \numcircledmod{1} to \numcircledmod{8}. Step \numcircledmod{8} produces consistency links for showing triangular guarantee.}}
\label{fig:rlensComp}
\end{figure}
% Since |ls_ac'| contains two input links, so do (the composite links) |ls_bc'| and |ls_ab'|.)
% While the consistency links |ls_ab|, |ls_b'c'|, and |ls_a'c'| are more than two.



% \subsubsection{Retentive Lens Composition}
% Having discussed link (collection) composition,
Now, we define the composition of two retentive lenses.
\begin{definition}[Retentive Lens Composition]
\label{def:rlensComp}
Given two retentive lenses |lens_AB| and |lens_BC|, their composite lens |lens_AC| is denoted by |lens_AB ; lens_BC| and the corresponding |get_AC| and |put_AC| functions are defined by
\begin{center}
\begin{code}
{-"\phantom{xxxxxxxxxxxxxxxxxxxxxxxxxxxx}"-}    {-"\quad \quad"-}        put_AC (a , c' , ls_ac') =  a'
                                                                           itWhere  (b, ls_ab)  =  get_AB (a)
 get_AC (a) = (c, ls_ab linkComp ls_bc)                                             ls_bc'      =  conv (conv ls_ac' linkComp ls_ab)
   itWhere  (b, ls_ab)  = get_AB (a)                                                b'          =  put_BC (b, c', ls_bc')
            (c, ls_bc)  = get_BC (b)                                                ls_b'c'     =  fst (get_BC (b'))
                                                                                    ls_ab'      =  ls_ac' linkComp (conv ls_b'c')
                                                                                    a'          =  put_AB (a, b', ls_ab')
\end{code}
\end{center}
where |get_AB| and |put_AB| (resp.~|get_BC| and |put_BC|) are the corresponding |get| and |put| functions of |lens_AB| (resp.~|lens_BC|), and $|ls|^{\circ}$ is the collection of links produced from |ls| by swapping each link's starting point and ending point. (Recall that a collection of links is regarded as a relation, so we use the converse symbol for `reversed links'.)
\end{definition}
While the |get| behaviour of a composite retentive lens is straightforward from the definition, the |put| behaviour is a little complex and can be best understood with the help of \autoref{fig:rlensComp}.
Let us first recap the behaviour of |put| of well-behaved lenses; in \autoref{fig:rlensComp}, if we need to propagate changes from data |c'| back to data |a| without links, we will first construct the \emph{intermediate} data |b| (by running |get_AB a|), propagate changes from |c'| to |b| and produce |b'|, and finally use |b'| to update |a|.
%
Now we consider retentive lenses; it is similar for them to propagate changes from |c'| to |a| with a collection of links |ls_ac'| retaining some information:
Besides the intermediate data |b|, we also need to construct intermediate links |ls_bc'| (\numcircledmod{3} in the figure) for retaining information when updating |b| to |b'|, so that we can further construct intermediate links |ls_ab'| (\numcircledmod{6} in the figure) for retaining information when updating |a| to |a'| using |b'|.
%
% Note that consistency links are always used as the right operand of the link composition operator to ensure that link composition will succeed, if the proposition below is satisfied.


% \begin{proposition}
% \label{prop:validCompSucceed}
% Given two retentive lenses |lens_AB| and |lens_BC|, source |a| and view |c'|, and input link collection |ls_ac'|, the link composition in the |put| direction of their composite lens $|lens_AC| = |lens_AB|;|lens_BC|$ will succeed if the following condition is satisfied:
% \[ \strongValidLink{|a|}{|c'|}{|ls_ac'|}{|get_AC|} \]
% where $|(b,ls_ab)| = |get_AB (a)|$  and $|(c, ls_bc)| = |get_BC (b)|$.
% \end{proposition}


\begin{theorem}[Retentiveness Preservation]
\label{thm:retPrsv}
The composite lens $|lens_AC| = |lens_AB| ; |lens_BC|$ from two retentive lenses |lens_AB| and |lens_BC| is still a retentive lens.
% given that the lenses and the input data |a|, |c'| and |ls_ac| satisfy \autoref{prop:validCompSucceed}.
\end{theorem}
% \note{To save space, here we only show that the composite lens satisfies Retentiveness and omit the proofs for satisfying Hippocraticness and Correctness.}
The proof is available in the appendix (\autoref{prf:retPrsv}).
% \note{Explain the basic idea of the proof?}

