% !TEX root = retentive.lhs

\section{Introduction}
\label{sec:intro}

%\hu{\change{1ss}{22e}}
%\hu{\why{1ss}{22e}}
We often need to write pairs of programs to synchronise data.
Typical examples include view querying and updating in relational databases~\cite{Bancilhon1981} for keeping a database and its view in sync,
text file format conversion~\cite{macfarlane2013pandoc} (e.g.~between Markdown and HTML) for keeping their content and common formatting in sync,
and parsers and printers as front ends of compilers~\cite{Rendel:2010:ISD:2088456.1863525} for keeping program text and its abstract representation in sync.
%All these pairs of programs should satisfy some properties so that they can work correctly.
%
Asymmetric \emph{lenses}~\citep{foster2007combinators} provide a framework for modelling such pairs of programs and discussing what laws they should satisfy; among such laws, two \emph{well-behavedness} laws (explained below) play a fundamental role.
Based on lenses, various bidirectional programming languages and systems (\autoref{sec:related_work}) have been developed for helping the user to write correct synchronisers, and the well-behavedness laws have been adopted as the minimum, and in most cases the only, laws to guarantee.
In this paper, we argue that well-behavedness is not sufficient, and a more refined law, which we call \emph{retentiveness}, should be developed.

To see this, let us first review the definition of well-behaved lenses, borrowing some of \citeauthor{Stevens2008}{'s} terminologies~\cite{Stevens2008}.
Lenses are used to synchronise two pieces of data respectively of types $S$~and~$V$, where $S$~contains more information and is called the \emph{source} type, and $V$~contains less information and is called the \emph{view} type.
(For example, the abstract representation of a piece of program text is a view of the latter, since program text contains information like comments and layouts not preserved in the abstract representation.)
Here being synchronised means that when one piece of data is changed, the other piece of data should also be changed such that consistency is \emph{restored} among them, i.e.~a \emph{consistency relation}~$R$ defined on $S$~and~$V$ is satisfied.
(For example, a piece of program text is consistent with an abstract representation if the latter indeed represents the abstract structure of the former.)
Since $S$~contains more information than~$V$, we expect that there is a function |get : S -> V| that extracts a consistent view from a source, and this |get| function serves as a consistency restorer in the source-to-view direction: if the source is changed, to restore consistency it suffices to use |get| to recompute a new view.
This |get| function, as reasoned by Stevens, should coincide with~|R| extensionally---that is, |s : S| and |v : V| are related by~|R| if and only if $|get|(s) = |v|$.%
\footnote{Therefore it is only sensible to consider functional consistency relations in the asymmetric setting.}
Consistency restoration in the other direction is performed by another function $|put| : S \times V \to S$, which produces an updated source that is consistent with the input view and can retain some information of the input source (e.g.~comments in the original program text).
Well-behavedness consists of two laws regarding the restoration behaviour of |put| with respect to |get| (and thus the consistency relation~|R|):
% the correctness requires
\begin{align}
|get| |(put| |(s,v))| &= |v|
\tag{Correctness}\label{equ:correctness} \\
|put| |(s, get| |(s))| &= |s|
\tag{Hippocraticness}\label{equ:hippocraticness}
\end{align}
Correctness states that necessary changes must be made by |put| such that the updated source is consistent with the view,
and Hippocraticness says that if two pieces of data are already consistent, |put| must not make any change.
A pair of |get| and |put| functions, called a \emph{lens}, is \emph{well-behaved} if it satisfies both laws.


Despite being concise and natural, these two properties are not sufficient for precisely characterising the result of an update performed by |put|, and well-behaved lenses may exhibit unintended behaviour, regarding what information is retained in the updated source.
Let us illustrate this by a very simple example, in which |get| is a projection (function) extracting the first element from a tuple of integers and strings.
(Hence a source and a view are consistent if the first element of the source tuple is equal to the view.)
\begin{small}
\begin{center}
\begin{code}
get :: (Int, String) -> Int
get (i, s) = i
\end{code}
\end{center}
\end{small}
Given this specific |get|, we can define |put_1| and |put_2|, both of which are well-behaved with |get| but the behaviour is rather different:
|put_1| simply replace the integer of the source (tuple) with the view, while |put_2| superfluously sets the string (of the tuple) empty, in silence, when the source (tuple) is not consistent with the view.
\vspace{-2ex}
\begin{small}
\begin{center}
\begin{minipage}[t]{0.45\textwidth}
\begin{code}
put_1 :: (Int, String) -> Int -> (Int, String)
put_1 (i, s) i' = (i', s)
\end{code}
\end{minipage}
\begin{minipage}[t]{0.4\textwidth}
\begin{code}
put_2 :: (Int, String) -> Int -> (Int, String)
put_2 src     i'  | get src == i'  = src
put_2 (i, s)  i'  | otherwise      = (i',  "")
\end{code}
\end{minipage}
\end{center}
\end{small}
From another perspective, |put_1| retains the string value from the old source when performing the update, while |put_2| chooses to discard that information---which is not desired but `perfectly legal', for the string data does not contribute to the consistency relation.
%
In fact, unexpected behaviour of this kind of well-behaved lenses could even lead to disaster in practice.
%
For instance, relational databases are tables consisting of rows of tuples, and well-behaved lenses used for maintaining a database and its view may erase important data after an update, as long as the data does not contribute to the consistency relation. (In most cases, it is simply equal to saying that the data is not in the view.)
%
This fact seems fatal, as asymmetric lenses have been considered a satisfactory solution~\cite{foster2007combinators} to the longstanding view-update problem (stated in the beginning of the paper).
% ~\cite{Bancilhon1981}


% Let us see what problem will happen in this case.
%
% Suppose that we have a consistent pair of |cst| and |ast| shown in \note{figure}, where |cst| represents `$0 - 1 + -2$' and its sub-term `$-2$' is desugared as `$0 - 2$' in |ast|.
%
% If |ast| is modified into |ast′| (also in \note{figure}), where the two non-zero numbers are exchanged.
%
% There is no much restriction on what a well-behaved |put| should do, and we could have a wide range of |put| behaviour, producing, as listed on the right-hand side of \note{figure},
% \begin{itemize}
%   \item |cst_1|, where the literals $1$ and $2$ are swapped, along with their annotations; or
%   \item |cst_2|, which represents the same expression as |cst_1| except that all annotations are removed; or
%   \item |cst_3|, which represents `$-2 + (0 - 1)$' and retains all annotations, in effect swapping the two subtrees under |Plus| and adding two constructors, |Term| and |Paren|, to satisfy the type constraints; or
%   % \item |cst_4|, where the negation syntactic sugar `$-2$' gets lost after the update.
%   \note{one of the four CSTs shall be removed.}
% \end{itemize}

% Among them, |cst_3| might be the most expected result because the user (or the tool) indeed swapped the two subtrees of |Plus| in |ast|.
% However, in practice, a well-behaved |put| generated from existing bidirectional languages (tools) has a very high possibility to return |cst_1| or |cst_2|.
%
% Correctness only requires that the updated program text be parsed to the modified AST, and this does not require the comment and syntactic sugar to be preserved; Hippocraticness has effect only when the AST is not modified, whereas the AST has been changed in this case.
% The root cause of the unintended |put| behaviour is that Hippocraticness only requires the \emph{whole} source to be unchanged if the \emph{whole} view is, but this is too `global' as we have seen, and it is desirable to have a law that gives such a guarantee more `locally'.

The root cause of the information loss (after an update) is that while lenses are designed to retain information, well-behavedness actually says very little about it: the only law guaranteeing information retention is Hippocraticness, which merely requires that the \emph{whole} source should be unchanged if the \emph{whole} view is.
%
In other words, if we have a very small change on the view, we are free to create any source we like.
This is too `global' in most cases, and it is desirable to have a law that makes such a guarantee more `locally'.

To have a finer-grained law, we propose \emph{retentive lenses}, an extension of the original lenses, which can guarantee that if parts of the view are unchanged, then the corresponding parts of the source are retained as well.
Compared with the original lenses, the |get| function of a retentive lens is enriched to compute not only the view of the input source but also a set of \emph{links} relating corresponding parts of the source and the view.
If the view is modified, we may also update the set of links to keep track of the correspondence that still exists between the original source and the modified view.
The |put| function of the retentive lens is also enriched to take the links between the original source and the modified view as input, and it satisfies a new law, \emph{Retentiveness}, which guarantees that those parts in the original source having correspondence links to some parts of the modified view are retained in the right place in the updated source computed by |put|.
% (In the refactoring scenario, after the AST is modified, the comment and the \emph{for-each} loop in the original program text are still linked to the moved methods in the AST, and will be guaranteed to be retained in the updated program text by Retentiveness.)
% Notably, if the view is not modified and the link structure produced by |get| is kept intact, then Retentiveness will guarantee that the source is not modified by |put|---that is, Hippocraticness is subsumed by Retentiveness and becomes a special case.


The main contributions of the paper are as follows:
\begin{itemize}
  %long ver. depending on the space
  \item We formalise the idea of links and develop a formal definition of retentive lenses for algebraic data types\footnote{This paper focuses on the synchronisation between algebraic data types, i.e.~trees, whose structure shall be considered more general than tables in relational databases. (A table is a list of tuples, and both lists and tuples can be encoded as trees.)}. More specifically, we extend |get| and |put| to incorporate links, and add a finer-grained law \emph{Retentiveness}, which precisely captures the intention that `if parts of the view are unchanged, then the corresponding parts of the source should be retained' (\autoref{sec:framework}).
  % short ver.
  % We propose a semantic framework of retentive lenses for algebraic data types\footnote{This paper focuses on the synchronisation between algebraic data types, i.e.~trees, whose structure shall be considered more general than tables in relational databases. (A table is a list of tuples, and both lists and tuples can be encoded as trees.)}. In our framework, any (state-based) well-behaved lens can be lifted to a retentive lens (\autoref{sec:framework}).

  \item To show that retentive lenses are feasible, we present a simple domain-specific language (DSL) tailored for developing synchronisation between syntax trees.
  We present its syntax, semantics, and also prove that any program written in our DSL gives rise to a pair of retentive get and put (\autoref{sec:dsl}).

  \item We demonstrate the usefulness of Retentiveness in practice by presenting two case studies on \emph{resugaring} and \emph{code refactoring}. We show that retentive lenses provide a systematic way for the user to preserve information of interest in the original source after synchronisation on demand (\autoref{sec:application}).
  % In both cases, we need to constantly make modifications to the AST and synchronise the CST accordingly, and retentive lenses provide a systematic way for the user to preserve information of interest in the original CST after synchronisation on demand.
  % We study the practical use of retentive lenses by writing the synchronisation between syntax trees of (a small subset of) Java 8 and demonstrate that Retentiveness helps to retain comments and syntactic sugar after code refactoring.
\end{itemize}

% The organization of the paper is as follows.
After explaining technical contributions, we discuss related work regarding various alignment strategies for lenses, provenance and origin between two pieces of data, and operational-based bidirectional transformations (BX) (\autoref{sec:related_work}).
Finally, we conclude the paper; we briefly discuss why we choose triangular diagrams and a simple version of Retentiveness, our thought on (retentive) lens composition, and the feasibility of retaining code styles for refactoring tools. (\autoref{sec:conclusion}).




% and the feasibility of retaining code styles for refactoring tools in our framework




% which is often used in applications such as code refactoring\footnote{We will explain the synchronisations occurred in code refactoring in detail in XX.}.
% To be concrete, assume that CSTs and ASTs are represented by the algebraic data types defined in \autoref{...}.



% \note{important ... provenance, traceability}
% \note{highlight examples. (forwarding.)}


% \hu{\why{In spite of the importance, in this paper, we will focus on the synchronisation between algebraic data types, i.e.~trees, whose structure shall be considered more general than tables in relational databases. (Because a table is a list of tuples, and both lists and tuples can be encoded as trees.)}{this part is not about the problem we want to show, and may be omited or moved to somewhere else}}

