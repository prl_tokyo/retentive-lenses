\section{A DSL for Retentive Bidirectional Tree Transformation}
\label{sec:dsl}

With theoretical foundations of retentive lenses in hand, we shall propose a domain specific language (DSL) for easily describing them.
Our DSL is designed to be simple but suitable for handling the synchronisation between syntax trees.
We give a detailed description of the DSL by presenting its programming examples (\autoref{subsec:examples}), syntax (\autoref{subsec:syntax}), semantics (\autoref{subsec:dslsem}), and finally the proof of its generated lenses holding Retentiveness (\autoref{mainthm}).


\subsection{Description of the DSL by Examples}
\label{subsec:examples}
In this subsection, we introduce our DSL by describing the consistency relations between the concrete syntax and abstract syntax of the arithmetic expression example in \autoref{fig:running_example_datatype_def}.
From each consistency relation defined in the DSL, we can obtain a pair of |get| and |put| functions forming a retentive lens.
%
Furthermore, we show how to flexibly update the |cst| \autoref{fig:swap_and_put} in different ways using the generated |put| function with different input links.


In our DSL, we define data types in Haskell syntax and describe consistency relations between them as if writing |get| functions.
For example, the data type definitions for |Expr| and |Term| written in our DSL remain the same as those in  \autoref{fig:running_example_datatype_def} and the consistency relations between them (i.e.~the |getE| and |getT| functions in \autoref{fig:running_example_datatype_def}) are expressed as the ones in \autoref{fig:dsl_example}.
Here we describe two consistency relations similar to |getE| and |getT|: one between |Expr| and |Arith|, and the other between |Term| and |Arith|.
%
Each consistency relation is further defined by a set of inductive rules, stating that if the subtrees matched by the same variable appearing on the left-hand side (source side) and right-hand side (view side) are consistent, then the large pair of trees constructed from these subtrees are also consistent.
(For primitive types which do not have constructors such as integers and strings, we consider them consistent if and only if they are equal.)
%
Take |Plus _  x  y  ~  Add  x  y| for example;
it means that if |x_s| is consistent with |x_v|, and |y_s| is consistent with |y_v|, then |Plus a x_s y_s| and |Add x_v y_v| are consistent for any value |a|, where |a| corresponds to the `don't-care' wildcard in |Plus _ x  y|.
So the meaning of |Plus _  x  y  ~  Add  x  y| can be better understood by the `derivation rule' beneath:
\[
\frac{|x_s ~ x_v| \quad |y_s ~ y_v|}
{\|Plus a x_s y_s ~ Add x_v y_v|}
\]
Generally, each consistency relation is translated to a pair of |get| and |put| functions, and each of these inductive rules forms one case of the two functions.

Now we briefly explain the semantics of |Plus _ x y ~ Add x y|.
In the |get| direction, its behaviour is quite similar to the first case of |getE| in \autoref{fig:running_example_datatype_def}, except that it also updates the links between subtrees marked by |x| and |y| respectively, and establishes a new link between the top nodes |Plus| and |Add| recording the correspondence.
In the |put| direction, it creates a tree whose top node is a |Plus| with empty annotations and recursively builds the subtrees, provided that there is no link connected to the node |Add| (top of the view).
%
If there are some links, then the behaviour of |put| is guided by those links; for example, |put| may additionally preserve the annotation in the old source.
We leave the detailed descriptions to the subsection about semantics.
%

\begin{figure}[t]
\begin{small}
\begin{multicols}{2}
\begin{code}
Expr <---> Arith
  Plus   _  x  y  ~  Add  x  y
  Minus  _  x  y  ~  Sub  x  y
  FromT  _  t     ~  t
Term <---> Arith
  Lit     _  i   ~  Num  i
  Neg     _  r   ~  Sub  (Num 0)  r
  Paren   _  e   ~  e
\end{code}
\end{multicols}
\end{small}
% \vspace{-2ex}
\caption{The program in our DSL for synchronising data types defined in \autoref{fig:running_example_datatype_def}.}
\label{fig:dsl_example}
\end{figure}


With the retentive lenses (i.e.~the above |get| and |put|) generated from \autoref{fig:dsl_example}, we can synchronise the old |cst| and modified |ast'| in different ways by running |put| with different input links. \autoref{fig:running_example} illustrates this:
\begin{itemize}
  \item If the user assumes that the two non-zero numbers |Num 1| and |Num 2| in the |ast| are exchanged, then four links (between |cst| and |ast'|) should be established: a link connects region |Lit "1 in sub" _| with region |Num _| (of the tree |Num 1|); a link connects |Lit "2 in neg" _| with |Num _| (of the tree |Num 2|); two links connects |1| and |1|, and |2| and |2|.
  The result is |cst_1|, where the literals $1$~and~$2$ are swapped, along with their annotations.
  Note that all other annotations disappeared because the user did not include links for preserving them.

  \item If the user thinks that the two subtrees of |Add| are swapped and passes |put| links that connect not only the roots of the subtrees of |Plus| and |Add| but also all their inner subtrees,
  the desired result should be |cst_2|, which represents `$-2+(0-1)$' and retains all annotations, in effect swapping the two subtrees under |Plus| and adding two constructors |FromT| and |Paren| to satisfy the type constraints.
  % (As mentioned before, the two constructors are added by the injections since they are interconvertible data types.)
  \autoref{fig:running_example} shows the input links for |Neg| and its subtrees.
  \item If the user believes that |ast'| is created from scratch and not related to |ast|, then |cst_3| may be the best choice, which represents the same expression as |cst_1| except that all the annotations are removed.
\end{itemize}

\begin{figure}[t]
\includegraphics[scale=0.6,trim={4cm 7cm 4cm 3cm},clip]{pics/put_with_links.pdf}
\caption[|cst| is updated in many ways.]{|cst| is updated in many ways. \footnotesize{(Red dashed lines are some of the input links for |cst_2|.)}}
\label{fig:running_example}
\end{figure}


Although the DSL is tailored for describing consistency relations between syntax trees, it is also possible to handle general tree transformations and the following are three small but typical programming examples other than syntax tree synchronisation.
For the first example, let us consider the binary trees
\vspace{-0.8ex}
\begin{center}
\begin{small}
\begin{code}
data  BinT a = Tip | Node a (BinT a) (BinT a)
\end{code}
\end{small}
\end{center}
\vspace{-0.8ex}
We can concisely define the |mirror| consistency relation between a tree and its mirroring as
\vspace{-0.8ex}
\begin{center}
\begin{small}
\begin{code}
BinT Int <---> BinT Int
^ ^ Tip         ~  Tip
^ ^ Node i x y  ~  Node i y x
\end{code}
\end{small}
\end{center}
\vspace{-0.8ex}
As the second example, we demonstrate the implicit use of some other consistency relation when defining a new one. Suppose that we have defined the following consistency relation between natural numbers and boolean values:
\vspace{-0.8ex}
\begin{center}
\begin{small}
\begin{code}
Nat <---> Bool
^ ^ ^ Succ _  ~  True
^ ^ ^ Zero    ~  False
\end{code}
\end{small}
\end{center}
\vspace{-0.8ex}
Then we can easily describe the consistency relation between a binary tree over natural numbers and a binary tree over boolean values:
\vspace{-0.8ex}
\begin{center}
\begin{small}
\begin{code}
BinT Nat  <---> BinT Bool
^ ^ ^ Tip           ~  Tip
^ ^ ^ Node x ls rs  ~  Node x ls rs
\end{code}
\end{small}
\end{center}
\vspace{-0.8ex}
As the last example, let us consider rose trees, a data structure mutually defined with lists:
\vspace{-0.8ex}
\begin{center}
\begin{small}
\begin{code}
data  RTree a = RNode a (List (RTree a))
data List a = Nil | Cons a (List a)
\end{code}
\end{small}
\end{center}
\vspace{-0.8ex}
We can define the following consistency relation to associate the left spine of a tree with a list:
\vspace{-0.8ex}
\begin{center}
\begin{small}
\begin{code}
RTree Int <---> List Int
^ ^ RNode i Nil         ~  Cons i Nil
^ ^ RNode i (Cons x _)  ~  Cons i x
\end{code}
\end{small}
\end{center}
\vspace{-0.8ex}


\subsection{Syntax}
\label{subsec:syntax}
\begin{figure}[t]
\begin{small}
\centerline{
\framebox[10cm]{\vbox{\hsize=14cm
\[
\begin{array}{llllllll}
\colorbox{light-gray}{\textsf{Program}} \\
\bb
  \m{prog} &::=& \m{tDef} \ \m{cDef}_1  \dots \m{cDef}_n \\
\ee \\[0.5em]
%
% \key{Type Synonym} \\
% \bb
% \ee \\[0.5em]
%
\colorbox{light-gray}{\textsf{Type Definition}} \\
\bb
  \m{tDef}  & ::= &  \m{tSyn}_1 \dots \m{tSyn}_m \ \m{tNew}_1 \dots \m{tNew}_n \ \\[0.3em]
  % \m{conv}_1 \dots \m{conv}_n
  \m{tSyn}  & ::= & \q{type}~\m{type}~\q{=}~\m{type} \\
\ee \\[1em]
\bb
  \m{tNew}  & ::= & \q{data} \ \m{type}  & \! \q{=}  & C_1 \ \m{type}_{11} \dots \m{type}_{1k_{1}}\\
            &     &                      & \q{||} & \dots\\
            &     &                      & \q{||} & C_n \ \m{type}_{n1} \dots \m{type}_{nk_{n}} \\
\ee \\[1.6em]
\bb
  \m{type}  & ::= & \m{tycon} \ \m{type}_1 \dots \m{type}_m &\reason{type constructor}\\
            & ||  & \m{tyvar} &\reason{type variable}\\
\ee \\[1.2em]
% \bb
  % \m{conv} ::= \q{instance}~\q{InterConv}~\m{type}_1~\m{type}_2~\q{where} \\
  % \qquad \qquad \ \q{inj}~\m{var}~\q{=}~\m{HaskellExpression} \\
% \ee \\[1.2em]
%
\colorbox{light-gray}{\textsf{Consistency Relation Definition}} \\
\bb
  \m{cDef} &::=& \m{type}_s \longleftrightarrow \m{type}_|v|
                           &\reason{relation type}\\
           &   &  \quad r_1 \dots r_n \; \q{;;}  &\reason{inductive rules}\\
\ee \\[1.1em]
%
\colorbox{light-gray}{\textsf{Inductive Rule}}\\
\bb
   r   &::=& \m{pat}_s~\text{`}\!\sim\!\text{'}~\m{pat}_v \\
\ee \\[0.5em]
%
\colorbox{light-gray}{\textsf{Pattern}}\\
\bb
   pat &::=& |v|                               &\reason{variable pattern}\\
       &~||& \_                                &\reason{don't-care wildcard}\\
       &~||& C \ \m{pat}_1 \dots \m{pat}_n     &\reason{constructor pattern}\\
\ee
\end{array}
\]
}}}
\end{small}
\caption[Syntax of the DSL.]{Syntax of the DSL.}
\label{fig:dsl_syntax}
\end{figure}


Having seen some programming examples, here we summarise the syntax of our language in \autoref{fig:dsl_syntax}.
A program consists of two main parts: definitions of data types and consistency relations between these data types.
%
We adopt the Haskell syntax for definitions of data types, so that a new data type is defined through a set of data constructors |C_1 ... C_n| followed by types, and a type synonym is defined by giving a new name to existing types.
(Definition for \texttt{type} is omitted.)
For the convenience of implementation, the part defining type synonyms should occur before the part defining new data types.
%
% \modified{At the end of type definitions, the user can describe (override) the conversions between interconvertible data types; otherwise, similar ones will be automatically generated.
% We will elaborate on this in the syntactic restrictions below.}\note{Delete this functionality as all of the injection functions can be automatically generated?}
Now we move to the definitions of consistency relations, where each of them starts with |type_s <---> type_v|, representing the source type and view type for the relation.
The body of each consistency relation is a list of inductively-defined rules, where each rule is defined as a relation between the source and view patterns |pat_s ~ pat_v|, and a pattern |pat| includes variables, constructors, and wildcards.
Finally, two semicolons `;;' are added to finish the definition of a consistency relation. (In this paper, we always omit `;;' when there is no confusion.)
%

\paragraph{Syntactic Restrictions}
\label{subsec:synres}

To guarantee that consistency relations in our DSL indeed correspond to retentive lenses, we impose several syntactic restrictions on the DSL.
Our restrictions on patterns are quite natural, while those on algebraic data types are a little subtle, which the reader may skip at the first reading.

\begin{itemize}
\item On \emph{patterns}, we assume (i) pattern coverage and source pattern disjointness.
For any consistency relation $|T_s <---> T_v| = \myset{p_i \sim q_i \mid 1 \leq i \leq n}$ defined in a program, $\myset{p_i}$ should cover all the possible cases of type |T_s|, and  $\myset{q_i}$ should cover all the cases of type |T_v|.
%
Source pattern disjointness requires that any distinct $p_i$ and $p_j$ do not match any tree at the same time so that at most one pattern is matched when running |get|.
%
Additionally, (ii) a bare variable pattern is not allowed on the source side (e.g.~|x ~ D x|) and wildcards are not allowed on the view side (e.g.~|C x ~ D _ x|).
%
%And (3) if some $|q_k| = x$ is a bare variable pattern, we require that the consistency relation between $|TypeOf|(|spat_k|, |x|) |<--->| |Tv| = \myset{ |r_i| \sim |t_i| \mid 1 \leq i \leq m}$ defined in the same program satisfy that $\myset{|t_i| \mid 1 \leq i \leq m}$ covers all cases of |Tv|.


\item On \emph{algebraic data types}, we require that types |T_1| and |T_2| be \emph{interconvertible}\footnote{Two types are interconvertible if there exist injections |inj_1 :: T_1 -> T_2| and |inj_2 :: T_2 -> T_1| for all the inhabitants of |T_1| and |T_2|. For example, \lstinline{Lit 1 :: Term} can be converted to \lstinline{FromT (Lit 1) :: Expr} and \lstinline{FromT (Lit 1) :: Expr} can be converted to \lstinline{Paren (FromT (Lit 1)) :: Term}.} when (i) consistency relations |T_1 <---> V|, |T_2 <---> V|, and |T_s <---> T_v| are all defined for some types |T_s|, |V|, and |T_v|, and (ii) the definition of |T_s| makes use of both |T_1| and |T_2| (directly or indirectly), and the definition of |T_v| makes use of |V|.
%
The following figure depicts some of the cases, in which the vertical dotted links (in black) mean that there may be other nodes in between the connected nodes. The blue horizontal links show that both |T_1| and |T_2| can be consistent with |V|.
\includegraphics[scale=0.6,trim={4.5cm 9.2cm 4cm 7.4cm},clip]{pics/interConv.pdf}\\
%
In this case, in the |put| direction, there might be links asserting that values of type |T_2| should be retained in a context where values of type |T_1| are expected, or vice versa;
thus we need a way to convert between |T_1| and |T_2|.
%
Let us consider a particular case where |T_1| and |T_2| are mutually recursively defined and mapped to the same view type.
For instance, since the two consistency relations |Expr <---> Arith| and |Term <---> Arith| in \autoref{fig:dsl_example} share the same view type |Arith|,
there should be a way to convert (inject) |Expr| into |Term| and vice versa.
Look at |cst'| in \autoref{fig:swap_and_put}, the second subtree of |Plus| is of type |Expr| but created by using a link connected to the old source's subtree |Neg ^ ...| of type |Term|, so we need to wrap |Neg ^ ...| into |FromT "" (Neg ^ ...)| to make the types match.
%


In our DSL, the conversions (injections) for interconvertible data types are automatically generated from consistency relations.
To have an injection $|inj|_{T_1\rightarrow T_2}\,\, x = |C|\, \dots\, x\, \dots$ which directly injects data of type |T_1| to data of type |T_2|, it is equal to saying that the consistency relation $|T_2| \leftrightarrow |V|$ has a rule |C _ ^ ... x ... ^ _ ~ x| in which the source pattern only has wildcards except $C$ and $x$.
For example, |FromT _ t ~ t| gives rise to an injection $|inj|_{|Term| \rightarrow |Expr|}\,\, x = |FromT "" x|$.
Note that in general |T_1| can be indirectly injected to |T_2| through a \emph{chain} of such rules.



% \item On \emph{converting functions}, we require that they neither discard information nor add unnecessary information.
% Hence they are restricted to the form
% $|inj|_{T_1\rightarrow T_2}\,\, x = |C|\, \dots\, x\, \dots$
% and the consistency relation $|T_2| \leftrightarrow |V|$ should have a rule: |C _ ^ ... x ... ^ _ ~ x| in which the source pattern only has wildcards except $C$ and $x$.
\end{itemize}


% \caption[Interconvertible data types.]{Interconvertible data types. \footnotesize{(more explain)}}



\subsection{Semantics}
\label{subsec:dslsem}

We give a denotational semantics of our DSL by specifying the corresponding retentive lens---a pair of |get| and |put|---of a consistency relation defined in the DSL.
In other words, our (bidirectional) semantics of the DSL is defined by two (unidirectional) semantics, a |get| semantics and a |put| semantics.
Our Haskell implementation of the DSL follows the semantics described here.

\subsubsection{Types and Patterns}
To define |get| and |put|, we need to first give the semantics of type declarations and patterns.
The semantics of type declarations is the same as those in Haskell so that we will not elaborate much here.
For each defined algebraic data type |T|, we also use |T| to denote the set of all values of |T|. In addition, we define |Tree| to be the set of all the values of all the algebraic data types defined in our DSL and |Pattern| to be the set of all possible patterns.
For a pattern $p \in |Pattern|$, $|Vars|(p)$ denotes the set of variables in $p$, $|TypeOf|(p,|v|)$ is the set corresponding to the type of $|v| \in |Vars|(p)$, and $|Path|(p,|v|)$ is the path of variable $|v|$ in pattern $|p|$.

We use the following (partial) functions to manipulate patterns:
\begin{align*}
|isMatch| &: (|p| \in |Pattern|) \times |Tree| \rightarrow |Bool| \\
|decompose| &: (|p| \in |Pattern|) \times |Tree| \pfun \big(|Vars|(|p|) \rightarrow |Tree|\big) \\
|reconstruct| &: (|p| \in |Pattern|) \times \big(|Vars|(|p|) \rightarrow |Tree|\big) \pfun |Tree| \ \text{.}
\end{align*}
%
Given a pattern |p| and a value (i.e.~tree) |t|, $|isMatch| |(p, t)|$ tests if |t| matches |p|.
If the match succeeds, $|decompose| |(p, t)|$ returns a function |f| mapping every variable in |p| to its corresponding matched subtree of |t|.
Conversely, $|reconstruct| |(p, f)|$ produces a tree |t| matching pattern |p| by replacing every occurrence of $|v| \in |Vars|(|p|)$ in |p| with $f(|v|)$, provided that |p| does not contain any wildcard.
%
Since the semantics of patterns in our DSL is rather standard, we omit detailed definitions of these functions\footnote{Non-linear patterns are supported: multiple occurrences of the same variable in a pattern must have the same type and they must capture the same value.}.


\subsubsection{Get Semantics}\label{subsubsec:getsem}
For a consistency relation $|S| \leftrightarrow |V|$ defined in our DSL with a set of inductive rules $R = \{\, |spat_k| \sim |vpat_k| \mid 1 \leq |k| \leq n\,\}$, its corresponding $\getsv$ function has the following type:
\begin{align*}
\getsv : |S| \rightarrow  |V| \times |LinkSet|
\end{align*}
The idea of $|get|(s)$ is to use the rule $|spat|_k \sim |vpat|_k \in R$ of which $|spat|_k$ matches $s$---our DSL requires such a rule uniquely exists for all $s$---to generate the top portion of the view and recursively generate subtrees for all variables in $|spat|_k$.
The |get| function also creates links in the recursive procedure: when a rule $|spat|_k \sim |vpat|_k \in R$ is used, it creates a link relating those matched parts as two regions.
%
The |get| function defined by $R$ is:
\newcommand{\mywhere}[0]{\quad\text{ \textbf{where} } }
\newcommand{\myspace}[0]{\quad\phantom{\text{ \textbf{where} }} }
\newcommand{\myindent}[0]{\phantom{xxxxxxxxxxx}}
\newcommand{\myindentS}[0]{\phantom{xxxx}}
\newcommand{\myindentSS}[0]{\phantom{xx}}
\begin{align*}
&|get|_|SV|(s) = (|reconstruct|(|vpat|_k, |fst| \circ |vls|),\; l_{\mathit{root}} \cup |links|) \numberthis \label{equ:get} \\
&\mywhere |spat|_k \sim |vpat|_k \in R \text{ and } |spat|_k \text{ matches } s \\
&\myspace |vls| = (|get| \circ |decompose|(|spat|_k, s)) \in |Vars|(|spat|_k) \rightarrow |V| \times |LinkSet| \\
&\myspace |spat'| = |eraseVars|(|fillWildcards|(|spat|_k, s)) \\
&\myspace l_{\mathit{root}} = \myset{((spat' , [\,]) , (|eraseVars|(|vpat|_k), [\,])) } \\
&\myspace |links| = \big\{\, ((|spat|, |Path|(|spat|_k, t) \dblplus |spath|) , (|vpat|, |Path|(|vpat|_k, t) \dblplus |vpath|)) \\
&\myspace \myindentS\myindentS \mid t \in |Vars|(|vpat|_k),\, ((|spat|,|spath|) , (|vpat|,|vpath|)) \in |snd|(|vls|(t)) \,\big\} \ \text{.}
\end{align*}
For a tree $s$ matching |pat|, $|fillWildcards|(|pat|, s)$ replaces all the wildcards in |pat| with the corresponding subtrees of |s|, and $|eraseVars|(|pat|)$ replaces all the variables in pattern |pat| with wildcards.
%
While the recursive call is written as $|get| \circ |decompose|(|spat|_k,\, s)$ in the definition above, to be precise, |get| should have different subscript $|TypeOf|(|spat|_k, |v|)\,|TypeOf|(|vpat|_k, |v|)$ for different $|v| \in |Vars|(|spat|_|v|)$.
% \note{here we have a comma in between two types while |get_SV| does not have that comma in between |S| and |V|.}




\subsubsection{Put Semantics}\label{sec:putsem}
For a consistency relation $|S| \leftrightarrow |V|$ defined in our DSL as $R = \{\, |spat_k| \sim |vpat_k| \mid 1 \leq |k| \leq n\,\}$, its corresponding $\putsv$ function has the following type:
\[\putsv : |Tree| \times |V| \times |LinkSet| \pfun |S| \ \text{.} \]
Given arguments $(|s|, |v|, |ls|)$, |put| is defined by two cases depending on whether the root of the view is within a region of the input links or not, i.e., whether there is some $(|_|\, , (|_|\, , [])) \in |ls|$.

\begin{itemize}
\item {
  In the first case where the root of the view is not within any region of the input links, |put| selects a rule $|spat_k| \sim |vpat_k| \in R$ whose $|vpat|_k$ matches |v|---again, our DSL requires that at least one such rule exist for all |v|---and uses $|spat|_k$ to build the top portion of the new source:
  wildcards in $|spat|_k$ are filled with default values and variables in $|spat|_k$ are filled with trees recursively constructed from their corresponding parts of the view.
  \begin{align*}
  &|put|_{|SV|}(|s|, |v|, |ls|) = |reconstruct|(|spat|'_k, |ss|) \numberthis \label{equ:put1}\\
  &\mywhere |spat|_k \sim |vpat|_k \in R \text{ and } |vpat|_k \text{ matches } |v| \\
  &\myspace |vs| = |decompose|(|vpat|_k, |v|)\\
  &\myspace |ss| = \lambda\,(t \in |Vars|(|spat|_k)) \rightarrow \\
  &\myspace\myindentS |put|(|s|, |vs|(t), |divide|(|Path|(|vpat|_k, t), |ls|)) \numberthis \label{equ:rec1} \\
  &\myspace |spat|'_k = |fillWildcardsWithDefaults|(|spat|_k) \\
  &|divide|(|prefix|, |ls|) = \myset{(|r|_|s|, (|vpat|, |vpath|)) \mid (|r|_|s|, (|vpat|, |prefix| |++| |vpath|) \in |ls|)}
  % &\mywhere |mv'| = \myset{ x \mapsto p \mid |mv|(x) = |prefix| \dblplus p} \\
  % &\myspace |ps'| = \myset{ p \in |ps| \mid \text{Names appeared in } |p| \text{ are defined in (|ms|,\,|mv'|)}}
  \end{align*}
  The omitted subscript of |put| in (\ref{equ:rec1}) is ${|TypeOf|(|spat|_k, t)\,|TypeOf|(|vpat|_k, t)}$.
  Additionally, if there is more than one rule of which $|vpat|$ matches |v|, the rule whose |vpat| is \emph{not} a bare variable pattern is preferred for avoiding infinite recursive calls: if $|vpat|_k = x$, the size of the input of the recursive call in (\ref{equ:rec1}) does not decrease because $|vs|(t) = |v|$ and $|Path|(t, |vpat_k|) = |[]|$.
  %
  For example, if both $|spat|_1 \sim |Succ x|$ and $|spat|_2 \sim x$ can be selected, the former is preferred.
  % We need such preference to
}
\item {
  In the other case where the root of the view is an endpoint of some link, |put| uses the source region (pattern) of the link as the top portion of the new source.
  \begin{align*}
  &|put|_{|SV|}(|s|, |v|, |ls|) = |inj|_{|TypeOf|(|spat|_k) \rightarrow S}|(reconstruct|(|spat_k'|,|ss|))  \numberthis \label{equ:put2}\\
  &\mywhere l = ((|spat|\footnotemark, |spath|) , (|vpat|, |vpath|)) \in |ls| \\
  &\myspace\myindentSS \text{ such that } |vpath| = |[]|,\,|spath|\text{ is the shortest } \\
  &\myspace |spat_k| \sim |vpat_k| \in R \text{ and } |isMatch|(|spat_k|, |spat|) \\
  &\myspace |spat_k'| =  |fillWildcards|(|spat|_k, spat) \\
  &\myspace |vs| = |decompose|(|vpat|_k, |v|)\\
  &\myspace |ss| = \lambda\,(t \in |Vars|(|spat|_k)) \rightarrow \\
  &\myspace\myindentS |put|(|s|, |vs|(t), |divide|(|Path|(|vpat|_k, t), |ls| \setminus \myset{l})) \numberthis \label{equ:rec2}
  \end{align*}
  \footnotetext{Here we treat region patterns (such as |spat|) as partial trees, i.e.~trees with holes (represented by wildcards) in them. Wildcards in partial trees can be captured by either wildcard patterns or variable patterns in pattern matching.}
  When there is more than one source region linked to the root of the view, |put| chooses the source region whose path is the shortest, which ensures that the preserved region patterns in the new source will have the same relative positions as those in the old source, as the following figure shows.\\
  \includegraphics[scale=0.6,trim={4cm 8.5cm 4.5cm 8cm},clip]{pics/multiLinks.pdf}\\
  Since the linked source region (pattern) does not necessarily have type |S|, we need to use the function $|inj|_{|TypeOf|(|spat|_k) \rightarrow S}$ to convert (inject) it to type |S|; this function is provided by the interconvertible requirement of our DSL (see \hyperref[subsec:synres]{Syntax Restrictions}).
}

\end{itemize}

\subsubsection{Domain of |put|}
The last piece of our definition of |put| is its domain, on which |put| will terminate and satisfy the required properties of the retentive lens formed together with its corresponding |get|.
In the implementation, runtime checks are used to detect invalid arguments when doing |put|; but for clarity, here we define a separate function |check| and the domain of |put| to be $\myset{a : |Tree| \times |V| \times |LinkSet|\mid |check|(a) = \top}$.
\begin{align*}
&|check| : |Tree| \times |V| \times |LinkSet| \rightarrow |Bool| \\
&|check|(|s|, |v|, |ls|) =
  \begin{cases}
  |chkWithLink|(|s|, |v|, |ls|)  \quad & \text{if some} ((\_, \_),(\_, [])) \in |ls|\\
  |chkNoLink|(|s|, |v|, |ls|) & \text{otherwise}
  \end{cases}
\end{align*}
|chkNoLink| corresponds to the first case of |put| (\ref{equ:put1}).
Condition |cond_1| checks that every link in |ls| is processed in one of the recursive calls.
(Specifically, if |Vars(spat_k)| is empty, |ls| in |cond_1| should also be empty meaning that all the links have already been processed.)
|cond_2| summarises the results of |check| for recursive calls.
|cond_3| guarantees the termination of recursion: When |vpat_k| is a bare variable pattern, the recursive call in (\ref{equ:rec1}) does not decrease the size of any of its arguments;
|cond_3| makes sure that such non-decreasing recursion will not happen in the next round\footnote{It is often too restrictive to check two rounds; however, the restriction can be relaxed to checking arbitrary finite rounds.} for avoiding infinite recursive calls.
\begin{align*}
&|chkNoLink|(|s|, |v|, |ls|) = |cond|_1 \wedge |cond|_2 \wedge |cond|_3 \\
&\mywhere |spat|_k \sim |vpat|_k \in R \text{ and } |vpat|_k \text{ matches } |v| \\
&\myspace |vs| = |decompose|(|vpat|_k, |v|)\\
&\myspace |vp|(t) = |Path|(|vpat|_k, t) \\
&\myspace |cond|_1 = |ls| |==| \left(\bigcup_{t \in |Vars|(|spat|_k)} |addVPrefix|(|vp|(t), |divide|(|vp|(t), |ls|))\right) \\
&\myspace |cond|_2 = \bigwedge_{t \in |Vars|(|spat|_k)} |check|(s, |vs|(t), |divide|(|vp|(t), |ls|)) \\
&\myspace |cond|_3 = \text{\textbf{if} } |vpat|_k \text{ is some bare variable pattern `$x$'} \text{ \textbf{then} } \\
&\myspace\myindent|TypeOf|(|spat|_k, x) |<--->| |V|  \text{ has a rule } |spat|_j \sim |vpat|_j \text{ s.t.\ }\\
&\myspace\myindent\myindentSS |isMatch|(|vpat|_j, |v|) \text{ and } |vpat|_j \text{ is not a bare variable pattern} \\
&\myspace |addVPrefix|(|prefix|, |rs|) = \myset{((a, b), (c, |prefix| |++| d)) \mid ((a,b),\,(c,d) \in |rs|}
\end{align*}

For |chkWithLink|, as in the corresponding case of |put| (\ref{equ:put2}), let $|l = ((spat, spath), (vpat, vpath))| \in |ls|$ such that |vpath = []| and |spath| is the shortest if there is more than one such link.
\phantomsection\label{def:chkWithLink}
\begin{align*}
&|chkWithLink|(|s|, |v|, |ls|) = |cond|_1 \wedge |cond|_2 \wedge |cond|_3 \wedge |cond|_4\\
% \bigwedge_{i \in \myset{1, \dots, 4}} |cond|_i\\
&\mywhere |cond|_1 = |isMatch|(|spat|, |sel|(|s|, |spath|)) \wedge |isMatch|(|vpat|, |sel|(|v|, |vpath|)) \\
&\myspace |cond|_2 = \exists ! (|spat|_k, |vpat|_k) \in |R|.\ |vpat| = |eraseVars|(|vpat|_k) \\
&\myspace \myindent \wedge |isMatch|(|eraseVars|(|spat|_k), |spat|) \\
&\myspace \myindent \wedge |fillWildcards|(|spat|_k, |spat|) \text{ is wildcard-free} \\
&\myspace \myindent \wedge \bigwedge_{t \in |Vars|(|spat|_k)} |decompose|(|spat|_k, |spat|)(t) \text{ is wildcard} \\
&\myspace |cond|_3 = |ls| |==| \left(\myset{l} \cup \bigcup_{t \in |Vars|(|spat|_k)} |addVPrefix|(|Path|(|vpat|_k, t), |divide|(|Path|(|vpat|_k, t), |ls| \setminus \myset{l}))\right) \\
&\myspace |cond|_4 = \bigwedge_{t \in |Vars|(|spat|_k)}|check|(s, |vs|(t), |divide|(|Path|(|vpat|_k, t), |ls| \setminus \myset{l}))
\end{align*}
|cond_1| makes sure that the link |l| is valid (\autoref{def:validLinks}) and |cond_2| further checks that it can be generated from some rule of the consistency relations.
|cond_3| and |cond_4| are for recursive calls: the former guarantees that no link will be missed and the latter summarises the results.

% \subsubsection{Domain of |put|}
% \note{Devise dynamic checks for (some of) these restrictions?}
% The last piece for our definition of |put| is its domain, on which |put| will satisfy \hyperref[law:retain]{Retentiveness} and terminate. Some of these conditions are quite subtle and technical.
% The reader can safely skip them at the first reading.
%
% For a collection of links $|ls|$, $|put|(|s|, |v|, |ls|)$ is defined when:
% \begin{itemize}
% \item The collection of links |ls| is valid for |s| and |v| (\autoref{def:validLinks}) and only contains links that can be generated by |get|. That is, there exists some $s' \in S$ such that $|get|(|s'|) = (|_| \, , |ls'|)$ and $|ls'| \subseteq |ls|$.
% This is helpful in preventing input links from relating inconsistent regions, e.g.~a source region of pattern `|Neg "..." a|' and a view region of pattern `|Add a b|'.
% In this case, there is no hope to deliver triangular guarantee since such invalid links cannot appear among any consistency links produced by |get|.
%
% \item Regions are well-aligned: For every subtree |t| of |v| whose root is not within the region of any link in |ls|, if |t| matches the view-side pattern of a rule in $R$, the matched portion---i.e.~the parts not captured by any variable in the pattern---of |t| does not overlap the view region of any link in |ls|.
% This restriction guarantees that when some rule $|spat_k| \sim |vpat_k|$ is used in the first case of |put|, the top portion of |v| matching |vpat_k| does not overlap any view region of some link in |ls|, so that every link will be handled by the second case of |put| in the recursive procedure, contributing to Retentiveness.
% Such well-alignment conditions also appeared in \citeauthor{WangMeng2011}'s paper~\cite{WangMeng2011}.
%
% \item There is no infinite recursion. In the first case of |put|, the recursive call (\ref{equ:rec1}) does not decrease the size of the argument \note{which argument?} of |put| when $|vpat_k| = t$ (i.e.~ a bare variable pattern) since $|vs|(t) = |v|$ and $|Path|(t, |vpat_k|) = \epsilon$.
% Thus we need to restrict the domain of |put| further to rule out the possible infinite recursion caused by such case:
% Given type |V| and an infinite sequence of types |S_i| such that for all $i \in \mathbb{N}$, both $|S_i| \leftrightarrow |V|$ and $S_{i+1} \leftrightarrow |V|$ are defined, if there is an inductive rule in $|S_i| \leftrightarrow |V|$
% \[|spat_0| \sim x \quad\text{ for some variable }x\]
% where $|TypeOf|(|spat_0|, x) = S_{i+1}$, we require that for any subtree |t| of |v|, there are always an $n \in \mathbb{N}$ and a rule $|spat| \sim |vpat|$ in $|S|_n \leftrightarrow |V|$ such that |vpat| matches |t| and |vpat| is not a bare variable pattern (i.e.~the size of the argument of |put| decreases.)
% \end{itemize}


% There are still chances for valid links (\autoref{def:validLinks}) to be improper with regard to the consistency relation (i.e.~the |get| function) of a retentive lens.
% %
% For our syntax tree example, an arbitrary input link might relate inconsistent regions, e.g.~a source region of pattern `|Neg "..." a|' and a view region of pattern `|Add a b|';
% in this case, there is no hope to deliver the triangular guarantee since such invalid links cannot appear among the consistency links produced by |get|.
% This leads to the following definition of \emph{valid links}.

% \note{Consider deleting the definition of valid links if the proof for retentiveness preservation does not use it. Then rename (recover) \emph{weakly valid links} to \emph{valid links}.}
% \begin{definition}[Strong Valid links]
% \label{def:strongValidLinks}
% For a collection of links $|ls| \in |LinkSet|$, a source $|s| \in |S|$, a view $|v| \in |V|$, and a retentive lens consisting of |get| and |put| between |S| and |V|, we say that |ls| is \emph{valid} for |s|, |v|, and |get|, denoted by $\strongValidLink{|s|}{|v|}{|ls|}{|get|}$, exactly when
% %
% \[ \validLink{|s|}{|v|}{|ls|} \quad\land\quad |get s| = (\_,\,|ls_0|) \text{ such that } |ls| \cdot |fst| \subseteq |ls_0| \cdot |fst|. \]
% %
% where $|fst| : A \times B \to A$ is the projection function extracting the first element from a tuple (lifted to relation).
% \hu{\why{That is, the input links |ls| are considered to be modified from consistency links |ls_0|}{difficult to understand.}}; for instance, in \autoref{fig:swap_and_put}, the view region $(|Sub (Num 0) _|\, , [1])$ of |ast| is moved to $(|Sub (Num 0) _| \, , [0])$ of |ast'|.
% % a subset of modified consistency links whose view regions  are moved to some other place to form view regions
% % \note{Explain the definition.}
% \end{definition}

\subsubsection{Retentiveness of the DSL}
With the definitions of |get| and |put| above, now we have:

\begin{theorem}[Main Theorem]
\label{mainthm}
Let $|put'| = |put|$ with its domain intersected with $|S| \times |V| \times |LinkSet|$, |get| and |put'| form a retentive lens as in \autoref{def:retLens}.
\end{theorem}
The proof can be found in the appendix (\ref{app:proof}).
The proof goes by induction on the size of the arguments to |put| or |get|.
