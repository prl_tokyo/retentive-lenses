We thank all the reviewers for their valuable comments and effort spent; we answer the most important questions in the *main concerns* part, followed by answers to each question in detail.

# Main concerns

## 1
Regarding Retentiveness, many reviewers concern *details about the management of link sets, i.e. where input links ls' come from when calling put* and think that *discussion around alignment is unfair*

Retentiveness aims to give the user control over the information retention in the *put* direction; thus, in the theory of Retentiveness, this argument is assumed to be provided by the user by default; the user can (should) compute input links in different ways for different applications.

Thus, it is better to view the problem from the perspective stated in Section 5.1 (related work): At the dawn of lenses, alignment between a source and a modified view is *position-wise*; then, several better alignment algorithms (such as *align by key* and *align by edit distance*) are devised; later, alignment over list (-like) structures is *parameterised* as an argument to *put* (in the work of  Pacheco et al.) to give the user more control and flexibility, at the cost of letting the user specify how to align elements in detail.
However, the parameterised alignment still only *locally* considers the source and view lists being aligned at a time, which is often not desired. (We have a concrete example below.) On the contrary, as can be seen clearly from Figure 2, Figure 5, and Figure 7 in our paper, the link approach allows two elements from any lists to be ‘aligned’ *globally*, which, however, in turn adds to more burden on the user (as expected).
Moreover, for well-behaved lenses, an alignment function always uses some pre-defined lenses or user-supplied lenses to update the aligned source element; that is why the retention of information in fact depends on the invoked lenses and sometimes information gets lost even a source and a view are ‘well-aligned’. By contrast, for retentive lenses, the ‘aligned’ (portion of) source element is already preserved, regardless of any other lenses.
All in all, Retentiveness can be considered more general than parameterised alignment, regarding the control over what is retained. 

Nonetheless, we agree with the reviewers that the design of Retentiveness gives much burden to the user so that for implementation of retentive lenses, useful tools should provide a way to ease the work of obtaining input links for the user. Currently, our DSL handles part of the input-link-generation work by automatically generating consistency links between a source and its consistent view from consistency relations; the details of how the links are generated are hidden behind. As presented in Section 6.1, with consistency links, the user can provide vertical correspondence between a view `v` and a modified view `v'`; then the vertical correspondence is composed with consistency links to become input links *ls'* to *put*. (We had given the definition of vertical correspondence and the composition there.) We also slightly mentioned how to build vertical correspondence for the edit operation *movement*. But for our evaluation, we need (plan) to demonstrate in detail how to write a tool for common refactoring scenarios and output vertical correspondence.
This is indeed a missing part in our evaluation, because we had written a function to ad hoc modify the consistency links connected to the `fuel` function to become input links.

Finally. it is worth noting that there is in general no hope to provide a universal method for establishing vertical correspondence: if we pre-define some ‘diff’ (align) functions over the old and new view, the vertical correspondence computed by pre-defined ‘diff’ functions might not be the user's desired one, and retentive lenses act as if they degenerate to well-behaved lenses; if we pre-define some operations for modifying the view, the pre-defined operations may not be sufficient for the user's application. That is why by default we ask the user to supply input links.


------

An example other than synchronisation between syntax trees for showing why *local* alignment that only considers the source and view lists being aligned is not desired: Assume that the source is the information about employees at company *L* and employees at company*W*, respectively stored in two lists `[sL_1 .. sL_n]` and  `[sW_1 .. sW_n]`. The view has two corresponding lists `[vL_1 .. vL_n]` and `[vW_1 .. vW_n]` with some fields of each employee's information dropped. In an extreme case, we can assume that the view only has employees' names (as shown in the Boomerang program in the part exceeding 1000 words). Suppose that an employee in company *L* transferred to company *W*, so that we need to modify the view to view' `[vL2 .. vL_n]` and `[vL_1, vW_1 .. vW_n]`. When we align the source's *W* list `[sW_1 .. sW_n]` with the view's *W* list  `[vL_1, vW_1 .. vW_n]`, there is less hope to reuse the information stored in `sL_1` when some element is updated by `vL_1`, since `sL_1` is not in the current source list *W* that is being updated but from another source list *L* that have been already updated. As a result, all the information about the employee *sL_1* in company *W* in the new source gets lost, except for his/her name. (Operation-based lenses also have their own problems when the shape of list (-like) structures or the data changes, as discussed in Section 5.1.) 

## 2

> Can you give an example of a primitive that seems reasonable but breaks retentiveness in a way that will cause regret down the line?

It depends on how we use Retentiveness, for we have proved that all the (well-behaved) primitives can satisfy Retentiveness in a trivial way.
But as the above example regarding employees (and the example in Figure 7) show, our link approach *allows two elements from any lists to be ‘aligned’*, which is usually not the case for alignment such as the ones adopted in Boomerang and parameterised by BiFluX. For instance, please consider the program in Boomerang shown later in the part exceeding 1000 words.

## 3 
> Presenting case studies from a different application area.

This suggestion indeed helps. We can immediately add a case study on XML synchronisation without effort, since XML files conform to their DTDs which can be represented by algebraic data types.

## The Boomerang program for the employee's problem

```
let getName : lens =
  grammar
    name ::= "name: " ln:(key NAME) ", " "salary: " SALARY ", " "country: " lc:COUNTRY
    <->
    "name: " ln
  end

let getNames : lens =
  grammar
    getNames ::= n:<dictionary "" : getName> <-> n
               | n:<dictionary "" : getName> newline ns:getNames <-> n newline ns
  end

let finalLens : lens = 
  getNames "some information for separation" getNames
```



## For reviewer A

> On the other hand, I wonder if the restriction to trees is really necessary. 

The formulation of Retentiveness itself does not require tree structures. Last year, we abstracted *region patterns* as *properties* and *paths* as *proofs* so that a link relates two `(property, proof)` pairs. However, the reviewers said that the formulation was too abstract and complex, so we had made our formulation simpler and made it work for the region model for ADTs only. Details can be found in [our old paper](http://php.yozora.moe/papers/icfp2018(rejected).pdf).

>  Couldn't you generalize the notion of links to arbitrary expressions

At the very beginning of the research, we considered letting a link relate a portion of source and a portion of view with some relation *R*.  Later we quickly turns to simpler framework for ADTs where all the links are of the same kind (i.e. relating region pairs). We may rethink it in the future.


> Moreover, although the idea is clear, Def. 2.6 is a bit confusing to me. The novel part (retentiveness) is a bit sloppy. What do you exactly mean with "(continuing above)"? Also, although obvious, function fst is not defined. Please formalize better this definition, since it is the key novelty of the paper.

Thanks. ‘Continuing above’ means that the ‘bindings’ for link set *ls* and *ls'* should be found in the above bullet regarding Correctness. And we forgot to explain the (Haskell) function `fst (x,y) = x` that is lifted to a relation.

>Example 2.9 made me think that retentive lenses might be as imprecise as ordinary lenses, since it depends on the considered links. Is this true? If so, please clarify in the paper.

We will clarity it. The retention of information really depends on the input links --- because the user needs to specify what they want to retain; for instance, a subtraction 0 - 2 or negation -2. Example 2.9 also shows that the flexibility of retaining information also depends on how a lens decomposes the source and the view: classic well-behaved lenses that are a special case of retentive lenses do not decompose sources and views (i.e. treat a source/view as a whole.), as a result, we do not have precise control of the information retention.


## For reviwer B

> Q1: all lens reconstructions look suspiciously like pushout constructions; can the difference between retentive lenses and the original formulation (or other variations) be captured more abstractly, so that it is clearer what "the least information-losing reconstruction" is?

We would like to consider the problem in this direction; it looks inspired. But, could you please elaborate more on *lens reconstructions*? We could not understand its meaning.

> Q2: complementing horizontal composition (composition of multiple lenses, as treated in the paper), are there also notions of vertical composition? These would concern a single lens, ie relate a sequence of minor view updates to a one-shot application of a large update, perhaps with commutativity or associativity laws for successive updates.

Yes, use the definition of vertical links we discussed in Section 6.1, it is easy to do vertical composition. The composition is very similar to the composition of horizontal links and vertical links.

```
[ (vpath1, vpat1, vpath2') |  (vpath1, vpat1, vpath1') <- vls1,    
                              (vpath2, vpat2, vpath2') <- vls2 ,
                              vpat1 == vpat2, vpath1' == vpath2]
```


> A full justification of the new axiom should include an evaluation or application in a different application area compared to syntax tree manipulations in SW engineering, eg from the data base / XML / config management worlds.

Answered in the main part.

> As the authors discuss in the final sections of the paper, several design alternatives (non-triangular diagrams, strong retentiveness, different notions of lens composition) are conceivable. While the rationale the paper gives for the choices the author(s) made appears sensible, the paper would be strengthened if some formal criterion were given that justifies these choices. At present, lens composition appears to be the only formal result (and is only a base-line condition), so while intuitively appealing, the proposed solution feels a little ad-hoc.

We can add the formal definition for the notion of Strong Retentiveness in the appendix. (See [our old paper](http://php.yozora.moe/papers/icfp2018(rejected).pdf)). The discussions about triangular diagrams and square diagrams can often be found when the researchers compare state-based lenses with (delta- or) operation-based lenses; we can cite them. In short, triangular diagrams are more flexible and square-diagrams are often tightly coupled with applications. For lens compositions, we need to think more.

> My grade should be understood to be at the lower end of B; there is indeed technical innovation but I judge it as a little too incremental, and the evaluation would benefit from being broadened.

As *reviewer D* suggests, the technical innovation seems subtle and incremental but in fact it makes a big difference in the lens framework. For the evaluation part, we have answered in the main part.



## For reviewer C

> 1) Please discuss how the management of the linksets affects the users of lenses.

Answered in the main part.

> -- The technical development seems solid but in places hand wavy. For example what is the meaning of inter-convertible?

If two types are inter-convertible, their inhabitants can be converted to each other while keeping the same view. E.g. `Lit 1 :: Term` can be converted to `FromT (Lit 1) :: Expr`, while `get (Lit 1) = Num 1` and `get (FromT (Lit 1)) = Num 1`. This notion is not related to the definition of Retentiveness, but related to the implementation of our retentive lenses for *algebraic data types*. We should add a formula to explicitly define it.


> -- My main concern about the paper is that it doesn't discuss whether the management of the linksets becomes a painful and error prone process for complex and large structures. As this is a design paper that aims to help the correct development of bidirectional programs it doesn't seem like a good idea to swift the obligation of producing lenses that preserve the required information from the lens author to the lens user.

Answered in the main part.

> -- For the same reason, I find unfair the discussion around alignment. The approaches discussed there attempt to encapsulate the management of the information that needs to be preserved in the implementation of the lens providing to the user a curated interface.

We think we answered in the main part.

> -- It almost feels like there is a missing abstraction layer on top retentive lenses that could hide all the linkSet management in wrappers for get and put that are parameterized by a linkset related specification. This approach would also allow abstracting the definition of retentiveness by eliminating the need for linkset arguments and using a relation between the source and target arguments and results of get and put to express the preservation of information.

As discussed above, for the implementation of retentive lenses (our DSL), we definitely can provide this kind of abstraction, which automatically handles links in some pre-defined ways just like automatically align a source and a view using some pre-defined functions. But we do not know whether we can add this layer to the definition of Retentiveness.

Our current DSL does something similar for the user's convenience and abstraction. The user describes consistency relations (correspondence) between data types and the retentive lenses are generated from the consistency relations. The *get* function of the lens will automatically generate link sets; although the relations are implicitly consistency relations (between data types) and cannot be manually augmented. At the very beginning of the research, we considered letting a link relate a portion of source and a portion of view with some relation *R*.  Later we quickly turns to simpler framework for ADTs where all the links are of the same kind (i.e. relating region pairs). We may rethink it in the future.

For other details, we answered in the main part.

> -- The case studies section was a let down. Both applications are discussed superficially and in particular there is no mention at all of the above issues about the management of the linksets.

Management of linksets are answered in the main part.

> -- I find it surprising that there is no discussion of symmetric lenses.

We did not explicitly discuss Retentiveness for symmetric lenses; this is mainly because we are extending asymmetric lenses but not the symmetric ones. Nevertheless, edit lenses are symmetric lenses and the discussions about them can be found in the related work section, both in the alignment strategies subsection and in the operation-based BX section.

Symmetric lenses can be extended with Retentiveness similarly. In this case, there are two functions putL and putR which both consume input links and produce consistency links for the data in common. For instance: putL (a, b, ls) = (b', ls'). Still we have `fst · ls ⊆ fst · ls'`. This is indeed an interesting work we could do, but not in this paper.

> -- The part about maintaining code styles under refactoring seems to be missing.

Finally, due to the page limit, we deleted the discussion about maintaining code styles under refactoring. However, forgot to delete relevant words in the introduction.



## For reviewer D

All are already answered in the main part.


