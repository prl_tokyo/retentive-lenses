\section{Case Studies}
\label{sec:application}

% In the rest of the paper, we assume that the grammar of a programming language is unambiguous so that the synchronisation between program text and its AST can be reduced to the synchronisation between the program text's concrete syntax trees (CSTs) and ASTs~\cite{Zhu:2016:PRP:2997364.2997369}.
% This allows us to focus on a retentiveness framework for algebraic data types (i.e., trees), instead of unstructured data like program text (strings).
% This does not prevent us from tackling refactoring, though, because we can always find a unique CST that is isomorphic to a piece of program text conforming to an unambiguous grammar.
% so that the synchronisation between program text and ASTs can be reduced to the synchronisation between CSTs and ASTs~\cite{Zhu:2016:PRP:2997364.2997369}.


% In this section we demonstrate some practical use of Retentiveness through potential applications such as resugaring~\cite{Pombrio:2014:RLE:2594291.2594319, Pombrio:2015:HRC:2784731.2784755}, and code refactoring~\cite{fowler1999refactoring}.


In this section, we demonstrate the usefulness of Retentiveness in practice by presenting two case studies on \emph{resugaring}~\cite{Pombrio:2014:RLE:2594291.2594319, Pombrio:2015:HRC:2784731.2784755} and \emph{code refactoring}~\cite{fowler1999refactoring}.
In both cases, we need to constantly make modifications to the AST\footnote{Many code refactoring tools make modifications to the CST instead. However, the design of the tools can be simplified if they migrate to modify the AST, as the paper will show.} and synchronise the CST accordingly.
Retentive lenses provide a systematic way for the user to preserve information of interest in the original CST after synchronisation.
% Retentiveness helps the user to know which information in the original CST is guaranteed to be retained after the synchronisation.


\subsection{Resugaring}
For a programming language, usually the constructs of its surface syntax are richer than those of its abstract syntax (core language).
The idea of resugaring is to print evaluation sequences in a core language using the constructs of its surface syntax.
% We will show that our DSL is capable of reflecting AST changes resulting from evaluation back to CSTs, by writing consistency relations between the surface syntax and the abstract syntax and passing the generated |put| proper links.
%
Take the language \textsc{Tiger}~\cite{Appel:1998:MCI:522388} (a general-purpose imperative language designed for educational purposes) for example: Its surface syntax offers logical conjunction (| && |) and disjunction (| |||| |), which, in the core language, are desugared to the conditional construct |Cond| after parsing.
Boolean values are also eliminated in the AST, where integer~|0| represents |False| and non-zero integers represent |True|.
%
For instance, the program text |a && b| is parsed to an AST |Cond a b 0| and |a |||| b| is parsed to |Cond a 1 b|.
If the user wants to inspect the result of one-step evaluation of |0 && 10 |||| c|, then the user will face a problem, as the evaluation is in fact performed on the AST |Cond (Cond 0 10 0) 1 c| and will yield new program text in terms of a conditional like $\textit{if}~10~\textit{then}~1~\textit{else}~c$.
%

To solve the problem,~\citeauthor{Pombrio:2014:RLE:2594291.2594319} enrich the AST to incorporate fields for holding tags that mark from which syntactic object an AST construct comes.
Conversely, we can also solve the problem while leaving the AST clean by using retentive lenses; we can write consistency relations between the surface syntax and the abstract syntax and passing the generated |put| function proper links for retaining syntactic sugar.
Below are the code snippet in our DSL and the illustration of resugaring by Retentiveness.
Note that syntactic sugar |Or| is retained since Retentiveness requires $|fst| \cdot |ls| \subseteq |fst| \cdot |ls'|$.
\begin{small}
\begin{center}
\begin{multicols}{2}
\begin{code}
OrOp   <---> Arith
  Or l r     ~  Cond l (Num 1)  r
  FromAnd t  ~  t

AndOp  <---> Arith
  And l r    ~  Cond l r (Num 0)
  FromCmp t  ~  t
\end{code}
\end{multicols}
\end{center}
\end{small}
% , just like what we have done for Java~8.
%
% \begin{figure}
\begin{center}
\includegraphics[scale=0.65,trim={6cm 7.5cm 6cm 5.15cm},clip]{pics/resugaring.pdf}\\
\end{center}
% \caption[Resugaring by Retentiveness.]{Resugaring by Retentiveness. \footnotesize{(Syntactic sugar |Or| is retained since Retentiveness requires $|fst| \cdot |ls| \subseteq |fst| \cdot |ls'|$.)}}
% \label{fig:resugaring}
% \end{figure}
Both \citeauthor{Pombrio2014Resugaring}'s `tag approach' and our `link approach', in actuality, identifies where an AST construct comes from; however, the link approach has an advantage that it leaves ASTs clean and unmodified so that we do not need to patch up the existing compiler to deal with tags.

%



\subsection{Refactoring}
\label{sec:refactoring}
In this subsection, we first discuss the feasibility of Retentiveness for code refactoring, and in particular we use code refactoring for Java~8 as an example (\autoref{sec:refactorFeasibility}).
Then we introduce a specific refactoring scenario of Java~8 (\autoref{sec:refactProblem}) and use it as the running example throughout the whole subsection:
We show how to use our DSL to implement the transformation system between CSTs and ASTs of a small subset of Java~8 (\autoref{sec:java8SyncImpl}), and demonstrate how to use the generated (retentive) lenses to perform code refactoring on \emph{clean} ASTs for the specific refactoring scenario while retaining comments and syntactic sugar in the updated program text on demand (\autoref{sec:refactorSysRun}).
% when an AST is modified by some refactoring algorithm, it is possible to retain comments and syntactic sugar in the updated program text on demand.

% the specific refactoring scenario
% in detail and show that Retentiveness makes it possible


\subsubsection{Feasibility of Retentiveness for Code Refactoring}
\label{sec:refactorFeasibility}
To see whether Retentiveness helps to retain comments and syntactic sugar for real-world code refactoring,
we surveyed the standard set of refactoring operations for Java~8 provided by Eclipse Oxygen (with Java Development Tools).
We classify the total 23 refactoring operations as the combinations of four basic operations: substitution, insertion, deletion, and movement.
%
For instance, \emph{Extract Method} that creates a method containing the selected statements and replace those selected statements (in the old place) with a reference to the created method~\cite{eclipse:documentation}, is the combination of insertion (as for the empty method definition), movement (as for the selected statements), and substitution (as for the reference to the created method).
%
For about half of the refactoring operations, position-wise replacement and list alignment (will be discussed in \autoref{subsec:alignment}) is sufficient, as the code (precisely, subtrees of an AST) is only moved around within a list-like structure (such as an expression list or a statement list).
For the remaining half of the operations, the code is moved far from its original position so that some `global tracking information' such as links are required.
Based on the survey, we believe that a transformation system implemented by retentive lenses is able to support the set of refactoring operations.



\begin{figure}
\includegraphics[scale=0.6,trim={4cm 4cm 4cm 3cm},clip]{pics/push_down_refactoring.pdf}
\caption[An example of the \emph{push-down} code refactoring.]{An example of the \emph{push-down} code refactoring. \footnotesize{(For simplicity, subclasses \texttt{Bus} and \texttt{Bicycle} are omitted.)}}
\label{fig:text_and_ast}
\end{figure}


\begin{figure}
\includegraphics[scale=0.6,trim={8cm 11.7cm 8cm 3.1cm},clip]{pics/possible_refactoring_results.pdf}
\caption[An unsatisfactory result after refactoring]{An unsatisfactory result after refactoring, losing the comment and \emph{while} syntactic sugar.}
\label{fig:refactoring_results}
\end{figure}


\subsubsection{A Specific Refactoring Scenario}
\label{sec:refactProblem}
% An important synchronisation problem that lenses appear to be able to address is code refactoring~\cite{fowler1999refactoring}.
%
% Let us walk through a concrete scenario illustrated in \autoref{fig:text_and_ast}.
The specific refactoring scenario is illustrated in \autoref{fig:text_and_ast}.
At first, the user designed a \texttt{Vehicle} class and thought that it should have a \texttt{fuel} method for all the vehicles.
The \texttt{fuel} method has a JavaDoc-style comment and contains a \texttt{while} loop, which can be seen as syntactic sugar and is converted to a standard \texttt{for} loop during parsing.
%
However, when later designing the subclasses, the user realises that bicycles cannot be fuelled, and decides to do the \emph{push-down} code refactoring, removing the \texttt{fuel} method from \texttt{Vehicle} and pushing the method definition down to subclasses \texttt{Bus} and \texttt{Car} but not \texttt{Bicycle}.
%
Instead of directly modifying the program text, most refactoring tools will first parse the program text into its AST, perform code refactoring on the AST, and regenerate new program text.
%
The bottom-left corner of \autoref{fig:text_and_ast} shows the desired program text after refactoring, where we see that the comment associated with |fuel| is also pushed down, and the \texttt{while} sugar is kept.
%
However, the preservation of the comment and syntactic sugar in fact does not come for free, as the AST, being a concise and compact representation of the program text, includes neither comments nor the form of the original \texttt{while} loop.
%
So if the user implements the |parse| and |print| functions as a well-behaved lens, it might only give the user an unsatisfactory result in which much information is lost (the comment and \texttt{while} syntactic sugar), like the one shown in \autoref{fig:refactoring_results}.
% This is a well-known problem~\citep{Li2006Comparative,fritzson2008comment,de2012algorithm} and we have seen that well-behaved lenses do not preclude the unsatisfactory outcome.

% if the |print| function in \autoref{fig:text_and_ast} takes only the AST as input, 


% and one way to remedy this is to implement the transformations between program text and ASTs using lenses~\cite{martins2014generating, Zhu:2016:PRP:2997364.2997369}, with |parse| as |get| and |print| as |put|, so that the additional information in program text can be retained by |print|/|put|.

% This attempt, however, reveals a fundamental weakness about the definition of lenses: while lenses are designed to retain information, well-behavedness actually says very little about the retaining of information.
% In our refactoring scenario, knowing that a lens between program text and ASTs is well-behaved does not preclude the unsatisfactory outcome in \autoref{fig:refactoring_results}:




\subsubsection{Implementation in Our DSL}
\label{sec:java8SyncImpl}
Implementation of the whole code refactoring tool for Java~8 using retentive lenses requires much engineering work, and there are further research problems not solved.
%
To make a trade-off, in this paper we focus on the theoretical foundation and language design, and have only implemented the transformation system for a small subset of Java~8 to demonstrate the possibility of having the whole system.
The full-fledged code refactoring tool is left for future work.



Following the grammar of Java~8~\cite{gosling2014java}, we define the data types for the simplified concrete syntax, which consists of only definitions of classes, methods, and variables; arithmetic expressions (including assignment and method invocation); conditional and loop statements.
For convenience, we also restrict the occurrence of statements and expressions to exactly once in most cases (such as variable declarations) except for the class and method body.
%
Then we define the corresponding simplified version of the abstract syntax that follows the one defined by JDT parser~\cite{openjdk}.
We additionally make the AST purer by dropping unnecessary information such as a node's position in the source file.
%
This subset of Java~8 has around 80 CST constructs (which represent production rules) and 30 AST constructs; the 70 consistency relations among them generate about 3000 lines of retentive lenses and auxiliary functions (such as the ones for handling interconvertible data types).

Now we define the consistency relations.
Since the structure of the consistency relations for the transformation system is roughly similar to the ones in \autoref{fig:dsl_example}, here we only highlight two of them as examples; reviewers can refer to the supplementary material to see the complete program.
%
We see that for the concrete syntax everything is a class declaration (|ClassDecl|), while for the abstract syntax everything is a tree (|JCTree|).
As a |ClassDecl| should correspond to a |JCClassDecl|, which by definition is yet not a |JCTree|, we use the constructors |FromJCStatement| and |FromJCClassDecl| to make it a |JCTree|, emulating the inheritance in Java.
This is described by the consistency relation\footnote{We include keywords such as \textit{class} in the CST patterns for improving readability, although they should be removed.}
\vspace{-0.8ex}
\begin{small}
\begin{center}
\begin{code}
ClassDecl <---> JCTree
  NormalClassDeclaration0 _ "class" n "extends" sup body ~
  FromJCStatement (FromJCClassDecl (JCClassDecl N n (J (JCIdent sup)) body))

  NormalClassDeclaration1 _ mdf "class" n "extends" sup body ~
  FromJCStatement (FromJCClassDecl (JCClassDecl (J mdf) n (J (JCIdent sup)) body))
  ...
\end{code}
\end{center}
\end{small}
\vspace{-0.8ex}
Depending on whether a class has a modifier (such as \textit{public} and \textit{private}) or not, the concrete syntax is divided into two cases while we use a |Maybe| type in the abstract syntax representing both cases.
(To save space, the constructors |Just| and |Nothing| are shortened to |J| and |N| respectively.)
%
Similarly, there are further two cases where a class does not extend some superclass and are omitted here.


Next, we see how to represent \texttt{while} loop using the basic \texttt{for} loop, as the abstract syntax of a language should be as concise as possible\footnote{Although the JDT parser does not do this.}:
% The conversion for \textit{while loop} is straightforward:
\vspace{-0.8ex}
\begin{small}
\begin{center}
\begin{code}
Statement <---> JCStatement
  While "while" "(" exp ")" stmt ~ JCForLoop Nil exp Nil stmt
\end{code}
\end{center}
\end{small}
\vspace{-0.8ex}
%
where the four arguments of |JCForLoop| in order denote (list of) initialisation statements, the loop condition, (list of) update expressions, and the loop body.
%
As for a \texttt{while} loop, we only need to convert its loop condition |exp| and loop body |stmt| to AST types and put them in the correct places of the \texttt{for} loop.
Initialisation statements and update expressions are left empty since there is none.
%

% The conversion from \textit{for-each loop} to \textit{for loop} is a little complex, so first consider the general conversion strategy~\cite{enhancedFor} through the following example.\\
% \begin{minipage}[b]{0.45\textwidth}
% \centering
% \begin{lstlisting}
%         for (type var : exp) {
%           // statements using var;
%         }
% \end{lstlisting}
% A \textit{for-each loop}
% \end{minipage}%
% \begin{minipage}[b]{0.55\textwidth}
% \centering
% \begin{lstlisting}
%       for (int i=0; i < exp.length; i++) {
%         type var = exp[i];
%         // statements using var;
%       }
% \end{lstlisting}
% The corresponding \textit{for loop}
% \end{minipage}\\[0.5em]
% %
% The |exp| is assumed to be an array-like object that has its length, so that in the converted \textit{for loop} we can use a variable |i|, the length of the object \lstinline{exp.length}, and a post-increment \lstinline{i++} to control the times of the loop.
% %
% Furthermore, since \lstinline{var} ranges over each object in the array, we need to make this explicitly by using a variable declaration with initialisation in the \textit{for loop}.
% These intentions can also be expressed in our DSL straightforwardly (despite being a little verbose)\footnote{We removed the beginning two characters |JC| from all the constructors in the AST except for |JCForLoop|}:
% %
% \vspace{-0.8ex}
% \begin{center}
% \begin{code}
% EnhancedFor "for" "(" ty var ":" exp ")" stmt ~
% JCForLoop
%   (Cons (FromVarDecl (VarDecl N "i" (PrimTypeTree UInt) (J (FromLit (UInt 0))))) Nil)
%   (Binary LESSTHAN (Ident "i") (FieldAccess expr "length") "<")
%   (Cons (Unary POSTINC (Ident "i") "++") Nil)
%   (FromBlock (Cons (FromVarDecl (VarDecl N var (PrimTypeTree ty)
%     (J (ArrayAccess expr (Ident "i"))))) stmt))
% \end{code}
% \end{center}
% \vspace{-0.8ex}
% %
% We can clearly read from the four subtrees of |JCForLoop| that, the first subtree means \lstinline{int i = 0}; the second is \lstinline{i < expr.length}; the third denotes \lstinline{i++}; the last is the concatenation of \lstinline{ty var = expr[i]} and the converted statements from the original \textit{for-each loop} body.
% %
% Another interesting point is that the variable |expr| appears twice on the view side, once in the filed access (\lstinline{expr.length}) and the other in the array access (\lstinline{expr[i]}).
% Other consistency relations can be written alike.


\subsubsection{System Running}
\label{sec:refactorSysRun}
Now we use the retentive lenses generated from our DSL to run the refactoring example in \autoref{fig:text_and_ast}.
We first test two basic cases: |put cst ast ls| and |put cst ast []|, where |ast| and |ls| are respectively the view and consistency links generated by |get cst|.
%
We obtain the same |cst| after running |put cst ast ls| and it shows that the generated lenses satisfy Hippocraticness.
We obtain |cst'| after running |put cst ast []|, in which the comments disappear and the \texttt{while} loop becomes a \texttt{for} loop.
This demonstrates that |put| can create a new source from scratch only depending on the given view.
As a special case of Correctness, we also check that |get (put cst ast []) == get cst|.

%
Then we modify |ast| to |ast'|, the tree after code refactoring, and create some diagonal links |ls'| between |cst| and |ast'| manually.
The diagonal links are designed only to connect the |fuel| method in the |Car| class but not the |fuel| method in the |Bus| class, so that we can observe that comments and \texttt{while} loop are only preserved for the |Car| class but not the |Bus| class.
This is where Retentiveness helps the user to retain information on demand.
Finally, we also check that |Correctness| holds: |get (put cst ast' ls') == ast'|.


