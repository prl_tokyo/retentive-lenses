\documentclass{llncs}

%include polycode.fmt

%format forall a = "\forall " a
%format exists a = "\exists " a
%format ldot = "."

%format Set_1
%format P_s
%format P_s' = " P_{s''} "
%format P_ss' = " P_{ss''} "
%format subset = " \subset "
%format subseteq = " \subseteq "
%format setconj  = " \cap "
%format sigma   = " \sum "
%format elem    = " \in "
%format getInv = " \textit{get}^{-1} "


%format preyV = "\textit{prey}V"
%format SType = "S\mkern-3mu\textit{Type}"
%format VType = "V\mkern-3.5mu\textit{Type}"
%format S_ExprArithCase0 = "\textit{S\_ExprArithCase0}"
%format V_ExprArithCase0 = "\textit{V\_ExprArithCase0}"
%format prexV = "\textit{prex}V"
%format dollar1 = "\$1"
%format dollar2 = "\$2"
%format dollar3 = "\$3"
%format typeOf = "\textit{typeOf}"

%format pattern_l
%format pattern_r
%format path_l
%format path_r
%format path_t
%format pattern_v
%format path_b
%format pt_1
%format p_1l
%format p_1r
%format pt_2
%format pt_v
%format pt_l
%format pt_r
%format p_2l
%format p_2r
%format vl_1
%format tyX_i
%format tyY_j
%format *** = "*\!\!*\!\!*"

%format e_1
%format e_1' = "e_1''"
%format e_2
%format e_2' = "e_2''"
%format e_3
%format e_b

%format Trait_1
%format Trait_2
%format trait_1
%format trait_2


%format insSubtree_s
%format insSubtree_l

%format == = "\mathop{\shorteq\kern1pt\shorteq}"
%format ~ = "\mathrel\sim"
%format <== = "\mathrel\Leftarrow"
%format ==> = "\mathrel\Rightarrow"
%format <---> = "\longleftrightarrow"
%format <===> = "\Longleftrightarrow"
%format <==> = "\Leftrightarrow"
%format (VEC(x)) = "\vv{"x"}"
%format (SEMANTIC(x)) = "\llbracket " x "\rrbracket "
%format (VARS(x)) = "\mkern-\thickmuskip(\vv{" x "})"
%format (SUBST(x)(y)) = "\mkern-\thickmuskip[\vv{" x "}/\vv{" y "}]"

%format GenProgram    = "\mathsf{GenProgram}"
%format (GenProgram1(x)) = "\mathsf{GenProgram}\llbracket " x "\rrbracket "
%format (GenPutRec(x)) = "\mathsf{GenPutRec}\llbracket " x "\rrbracket "
%format (GenGet(x)) = "\mathsf{GenGet}\llbracket " x "\rrbracket "
%format (mkLink(x))   = "\mathsf{mkLink}\llbracket " x "\rrbracket "
%format (getPrefix(x)) = "\mathsf{getPrefix}\llbracket " x "\rrbracket"
%format typeOfTT = "\mathsf{typeOf}"
%format (ctypeOf(x)) = "\mathsf{typeOf}\llbracket " x "\rrbracket"
%format (getVar(x)) = "\mathsf{getVar}\llbracket " x "\rrbracket"
%format (mkPat(x)) = "\mathsf{mkPat}\llbracket " x "\rrbracket "
%format (local(x)) ="loc\_ " x " "
%format ST ="\mathsf{ST}"
%format VT ="\mathsf{VT}"
%format mkLinkt = " \mathit{mkLink} "
%format (varsTT(x))  = "\mathsf{vars}\llbracket " x "\rrbracket"

%format itOf    = "\mathit{of} "
%format itCase  = "\mathit{case} "
%format itLet   = "\mathit{let} "
%format itIn    = "\mathit{in} "
%format itWhere = "\mathit{where} "
%format whereTT = "\mathsf{where} "
%format itMkPat = "\mathit{mkPat} "

%format GetPut = "\mathsf{GetPut} "
%format PutGet = "\mathsf{PutGet} "

%format Correctness = "\mathsf{Correctness} "
%format Hippocraticness = "\mathsf{Hippocraticness} "
%format Retentiveness = "\mathsf{Retentiveness} "

%format ^ = "\ "
%format ^^ = "\;"

%format ::= = "\Coloneqq"

%format CST_0
%format CST_1
%format CST_2
%format CST_3

%format P_1
%format P_2
%format P_i
%format P_n

%format Q_1
%format Q_2
%format Q_i
%format Q_n


%format t_1
%format t_2
%format s_0
%format s_1
%format s_1' = " s_1'' "
%format s_2
%format s_2' = " s_2'' "
%format s_3
%format s_4
%format s_i
%format v_i
%format s_01
%format s_02
%format v_0
%format v_1
%format v_2
%format v_3
%format l_0
%format l_1

%format ls_j'
%format ls_j

%format ls_i'
%format ls_0' = " \mathit{ls}_0'' "
%format ls_1'
%format ls_n'
%format ls_i
%format ls_0
%format ls_1
%format ls_2
%format ls_3
%format ls_n

%format hl_i'
%format hl_0' = " \mathit{hl}_0'' "
%format hl_1'
%format hl_n'
%format hl_i
%format hl_0
%format hl_1
%format hl_2
%format hl_3
%format hl_n


%format vls_0
%format vls_1
%format vls_1' = " \mathit{vls}_1'' "
%format vls_2
%format vls_3
%format vls_4
%format vls_0' = " \mathit{vls}_0'' "
%format vls_n
%format vls_n'
%format vls_i
%format vls_i'

%format hls_0
%format hls_1
%format hls_1' = " \mathit{hls}_1'' "
%format hls_2
%format hls_3
%format hls_0' = " \mathit{hls}_0'' "
%format hls_n
%format hls_n'
%format hls_i
%format hls_i'

%format imgl_0
%format imgv_0
%format imgh_0

%format ys_0
%format ys_1
%format ys_n
%format ys_0' = " \mathit{ys}_0'' "
%format ys_1' = " \mathit{ys}_1'' "
%format ys_n' = " \mathit{ys}_n'' "

%format y_0'  = " y_0'' "
%format y_1'  = " y_1'' "
%format y_i'  = " y_i'' "
%format y_n'  = " y_n'' "

%format vl_0
%format vl_1
%format vl_2
%format vl_3
%format vl_4
%format vl_i
%format vl_i'
%format hl_0
%format hl_1
%format hl_2
%format hl_3
%format hl_4
%format imag_0
%format x_0
%format x_1
%format x_2
%format x_i
%format x_n
%format y_0
%format y_1
%format y_2
%format y_i
%format y_j
%format y_n
%format z_0
%format z_i
%format z_n
%format y_n
%format y_n
%format prefX_0
%format prefX_i
%format prefX_n
%format prefY_0
%format prefY_i
%format prefY_n
%format env_i
%format env_0
%format env_n
%format get_1
%format put_L

%format get_v
%format get_l
%format put_s
%format put_l
%format put_1
%format put_2
%format put_3
%format putRec_s
%format putRec_l
%format inj_s
%format inj_l
%format mkInj_s
%format mkInj_l

%format p_b
%format p_l
%format p_u
%format p_r
%format vl'_0
%format hl'_0
%format pat_l
%format pat_r

%format linkComp = "{\cdot}"

%format exclude = " \backslash\!\backslash "
%format backslash = "\backslash"



\makeatletter
\newcommand{\shorteq}{%
  \settowidth{\@@tempdima}{-}%
  \resizebox{\@@tempdima}{\height}{=}%
}
\makeatother

\usepackage{microtype}
\usepackage[backend=biber]{biblatex}
\addbibresource{retentiveness.bib}

\usepackage{amssymb}
\usepackage{stmaryrd}
\usepackage{inconsolata}
\usepackage{mathtools}
\usepackage{esvect}
\usepackage{hyperref}
\usepackage{listings}
\usepackage[T1]{fontenc}
\usepackage{upquote}

\usepackage{xcolor}
\newcommand\myworries[1]{\textcolor{red}{\small[#1]}}
\newcommand\todo[1]{\textcolor{blue}{\small[#1]}}
\newcommand\biyacc[0]{\textsc{BiYacc}}
\newcommand\BiYacc[0]{\textsc{BiYacc}}
\newcommand\BiFlux[0]{\textsc{BiFlux}}
\newcommand\biflux[0]{\textsc{BiFlux}}
\newcommand\diff{\,\backslash\!\backslash\,}

\newcommand\tit[1]{\textit{#1}}
\newcommand\tits[1]{\textit{#1}\ }
\newcommand\ttt[1]{\texttt{#1}}

\newcommand\EscapeSans[1]{{-"\text{#1}"-}}

\newtheorem{prereq}{Prerequisites}

\def\figureautorefname{Fig.}
\def\sectionautorefname{Sect.}
\def\subsectionautorefname{Sect.}
\def\subsubsectionautorefname{Sect.}

\allowdisplaybreaks

\begin{document}

\title{Retentive Lenses}

\author{Zirun Zhu\inst{1, 2} \and Hsiang-Shang Ko\inst{1} \and Zhenjiang Hu\inst{1, 2}}

\institute{National Institute of Informatics, Japan \and SOKENDAI (The Graduate University for Advanced Studies), Japan
\email{\{zhu,hsiang-shang,hu\}@@nii.ac.jp}}

\maketitle

\begin{abstract}
% using at least 70 and at most 150 words. It will be set in 9-point
% font size and be inset 1.0 cm from the right and left margins.
% There will be two blank lines before and after the Abstract.

% We often need to write pairs of programs that synchronise data.
Researchers in the field of \emph{bidirectional transformation}s have studied problems on data synchronisation for a long time and proposed various properties, of which \emph{well-behavedness} is the most fundamental.
However, well-behavedness is not enough for characterising the results of an update (performed by $\mathit{put}$),
% and well-behaved \emph{lenses} can still exhibit unintended behaviour,
in particular regarding what information is retained in the updated source.
The root cause of the wild range of |put| behaviour is that the only property guaranteeing the retention of source information only requires that the whole source should be unchanged if the whole view is.

In this paper we propose a new property \emph{retentiveness}, which enables us to directly reason about the retention of source information locally.
Central to our formulation of retentiveness is the notion of \emph{links}, which are used to relate fragments of sources and views.
These links are passed as additional input to the extended $\mathit{put}$ function, which produces a new source in a way that preserves all the source fragments attached to the links.
% We extend the $\mathit{put}$ function to accept links as additional input, and produce a new source in a way that preserves all the source fragments attached to the links.
%
We validate the feasibility of retentiveness by designing a domain-specific language (DSL) supporting mutually recursive algebraic datatypes, in which the local consistencies between the source and the view can be described easily.
We prove that the pair of $\mathit{get}$ and $\mathit{put}$ functions generated from any program in the DSL satisfy retentiveness.
We evaluate the usefulness of retentiveness by presenting examples in two different research areas: resugaring and code refactoring, and discuss the potential usage in other settings.

% demonstrating that different source fragments are retained by giving different links.



 % to let the user describe the two programs in a declarative and \todo{unified way?}.


\keywords{bidirectional transformations, domain-specific languages, functional programming}
\end{abstract}




\section{Introduction}
\label{sec:intro}

Every now and then, we need to write pairs of programs that synchronise data.
Typical examples include:
\begin{itemize}
  \item view querying and updating in relational databases \cite{Bancilhon1981,Dayal:1982:CTU:319732.319740,Gottlob:1988:PUS:49346.50068} (keeping a database and its view in sync),
  \item parsers and printers as front ends of compilers \cite{Matsuda:2007:BTB:1291151.1291162,Rendel:2010:ISD:2088456.1863525,Zhu:2016:PRP:2997364.2997369} (keeping program text and its internal representation in sync), and
  \item designing tools that convert documents from one format to another \cite{macfarlane2013pandoc} (keeping the document contents in sync).
\end{itemize}
All these pairs of programs should satisfy some properties in order to work in harmony.
Such properties have been studied by researchers in the field of \emph{bidirectional transformations} \cite{czarnecki2009bidirectional} (BXs for short); of these properties, the notion of \emph{well-behavedness} \cite{foster2005combinators, Stevens2008} is the most fundamental, and various bidirectional programming languages and systems have been developed in different settings, in the hope of helping the user to write pairs of well-behaved programs easily~\cite{foster2005combinators,Bohannon:2008:BRL:1328438.1328487,barbosa2010matching,Hofmann:2012:EL:2103656.2103715,Pacheco:2014:BBF:2643135.2643141,ko2016bigul}. \todo{add papers for graph and model transformations}

In this paper we focus on a particular BX model, \emph{asymmetric lenses}, and introduce their well-behavedness in Stevens's terminologies~\cite{Stevens2008}.
Synchronisation is about transforming multiple pieces of data such that consistency is \emph{restored} among them, i.e., a \emph{consistency relation} is satisfied after the transformation.
In the asymmetric setting, we consider a binary consistency relation~$R$ between types $S$~and~$V$,
where $S$~contains more information and is called the \emph{source} type, and $V$~contains less information and is called the \emph{view} type.
Since $S$~contains more information than~$V$, we expect that there is a function |get :: S -> V|\,\footnote{We use (sometimes extended) Haskell notation throughout the paper.} that extracts a consistent view from a source; this |get| function, as reasoned by Stevens, should coincide with~|R| extensionally --- that is, |s :: S| and |v :: V| are related by~|R| if and only if |get s = v|.%
\footnote{Therefore it is only sensible to consider functional consistency relations in the asymmetric setting.}
Consistency restoration is nontrivial in only one direction, and is performed by another function |put :: S -> V -> S|, which produces an updated source that is consistent with the input view and can retain some information of the input source.
Well-behavedness consists of two properties regarding the restoration behaviour of |put| with respect to |get| (and thus the consistency relation~|R|):
% the correctness requires
\begin{align}
|get (put s v)| &= |v|
\tag{Correctness}\label{equ:correctness} \\
|put s (get s)| &= |s|
\tag{Hippocraticness}\label{equ:hippocraticness}
\end{align}
Correctness states that necessary changes must be made so that inconsistent data become consistent again,
while hippocraticness says that if two pieces of data are already consistent, the restorer (|put|) must not make any change.
A pair of |get| and |put| functions, called a \emph{lens}, is \emph{well-behaved} if it satisfies both properties.

%
Despite being concise and natural, the two well-behavedness properties are not enough for characterising the results of the update (performed by |put|), and well-behaved lenses can still exhibit unintended behaviour, in particular regarding what information is retained in the updated source.
%
To illustrate, let us consider the synchronisation between concrete and abstract syntax trees (CSTs and ASTs) of arithmetic expressions, represented as algebraic datatypes defined in \autoref{fig:running_example_datatype_def}.
The CSTs can be either expressions (|Expr|, containing additions and subtractions) or terms (|Term|, including numbers, negated terms and expressions in parentheses), and all constructors have an annotation field (|Annot|);
in comparison, the ASTs do not include explicit parentheses, negations, and annotations.
Consistency between CSTs and ASTs is given directly in terms of the |get| functions in \autoref{fig:running_example_datatype_def}.
We start from a consistent pair of |CST| and |AST| shown in \autoref{fig:running_example}, where |CST| represents ``$0-1+-2$'', whose sub-term ``$-2$'' appears in |AST| desugared as ``$0-2$''.
If |AST| is modified into |AST'| (also in \autoref{fig:running_example}) where the two non-zero numbers are exchanged, there is very little restriction on what a well-behaved |put| can do; for example, as listed on the right-hand side of \autoref{fig:running_example}, it can produce:
\begin{itemize}
  \item |CST_1|, where the literals $1$~and~$2$ are swapped, along with their annotations;
  \item |CST_2|, which represents the same expression as |CST_1| except that all annotations are removed;
  \item |CST_3|, which represents ``$-2+(0-1)$'' and retains all annotations, in effect swapping the two subtrees under |Plus| and adding two constructors, |Term| and |Paren|, to satisfy the type constraints.
%  \footnote{The left subtree of |EAdd| is required to be of type |Expr|. Since |Neg| is of type |Term|, the constructor |ET| is added to inject it to the type of |Expr|. |Paren| is added for the same reason.}
\end{itemize}


\begin{figure}[t]
\renewcommand{\hscodestyle}{\small}
\setlength{\mathindent}{0em}
\begin{minipage}[t]{.5\textwidth}
%
\begin{code}
data Expr   =  Plus   Annot  Expr  Term
            |  Minus  Annot  Expr  Term
            |  Term   Annot  Term

data Term   =  Lit    Annot  Int
            |  Neg    Annot  Term
            |  Paren  Annot  Expr

type Annot  =  String

data Arith  =  Add  Arith  Arith
            |  Sub  Arith  Arith
            |  Num  Int
\end{code}
\end{minipage}%
%
\begin{minipage}[t]{0.5\textwidth}
\begin{code}
getE :: Expr -> Arith
getE (Plus   _ e  t) = Add  (getE e) (getT t)
getE (Minus  _ e  t) = Sub  (getE e) (getT t)
getE (Term   _    t) = getT t

getT :: Term -> Arith
getT (Lit     _ i  ) = Num i
getT (Neg     _ t  ) = Sub (Num 0) (getT t)
getT (Paren   _ e  ) = getE e
\end{code}
\end{minipage}
\caption{Datatypes for concrete and abstract syntax trees and the consistency relation between them as two |get| functions.}
\label{fig:running_example_datatype_def}
\end{figure}
%

\begin{figure}[t]
\renewcommand{\hscodestyle}{\small}
\setlength{\mathindent}{0em}
\begin{minipage}[t]{.5\textwidth}
\begin{code}
CST =
  Plus "an add"
    (Minus "a sub"
      (Term "a Term" (Lit "0 in sub" 0))
      (Lit "1 in sub" 1))
    (Neg "a neg" (Lit "2 in neg" 2))

AST =
  Add  (Sub (Num 0) (Num 1))
       (Sub (Num 0) (Num 2))


AST' =
  Add  (Sub (Num 0) (Num 2))
       (Sub (Num 0) (Num 1))
\end{code}
\end{minipage}%
%
\begin{minipage}[t]{0.5\textwidth}
\begin{code}
CST_1 =
  Plus "an add"
    (Minus "a sub"
       (Term "a Term" (Lit "0 in sub" 0))
       (Lit "2 in neg" 2))
    (Neg "a neg" (Lit "1 in sub" 1))

CST_2 =
  Plus ""
    (Minus ""  (Term "" (Lit "" 0))
              (Lit "" 2))
    (Neg  ""  (Lit "" 1))

CST_3 =
  Plus "an add"
    (Term ""
      (Neg "a neg" (Lit "2 in neg" 2 )))
    (Paren ""
      (Minus "a sub"
         (Term "a Term" (Lit "0 in sub" 0 ))
         (Lit "1 in sub" 1 ))
\end{code}
\end{minipage}
\caption{|CST| is updated in three ways in response to the change from |AST| to |AST'|.}
\label{fig:running_example}
\end{figure}
% recover font size in code
\renewcommand{\hscodestyle}{}
The root cause of the above wild range of |put| behaviour is that while lenses are designed to enable retention of source information (annotations and the negation syntactic sugar in the above example), the only property guaranteeing that is hippocraticness, which only requires that the \emph{whole} source should be unchanged if the \emph{whole} view is.
This is too ``global'' in most cases, where we want a more ``local'' hippocraticness property saying that if \emph{parts} of the view are unchanged then the \emph{corresponding parts} of the source should be retained.
%Rather than let the user randomly guess, we approach the problem by proposing a new property, \emph{retentiveness}, which can directly tell the user what is retained after the update (|put|).
In this paper we propose a new \emph{retentiveness} property as a form of local hippocraticness.
\todo{Central to our formulation of retentiveness is the notion of \emph{links}, which are used to relate fragments of sources and views.}
A retentive |put| function accepts links as additional input, and can produce a new source in a way that preserves all the source fragments attached to the links.
For the example in \autoref{fig:running_example}, we can produce either |CST_1|, |CST_2|, or |CST_3| by just passing a retentive |put| function different links marking which parts of the source should be retained.

% \begin{itemize}
%   \item | ls_1 = [] |
%   \item | ls_2 = [  ((EAdd,[])  , (Add,[])),  ((ESub,[1]) , (Sub,[0])) ,  ((Neg,[2])  , (Sub,[1])),|
%                  |  ((ET,[1,1]) , ((),[0,0])) ,  ((EN,[1,1,1]) , (N,[0,0]))] |
%   \item | ls_3 = [...] |
% \end{itemize}
%

% CONSIDER REMOVE THE PARAGRAPH?
% The reader familiar with bidirectional transformations may immediately recall the notion of \emph{alignment} \todo{\cite{barbosa2010matching}}, and may wonder why we need retentiveness after all.
% However, the reader will come to the conclusion that existing BXs framework and their alignment strategies are not capable of handling our \todo{(more complicated examples in \autoref{sec:case_studies})} as reading through the paper.
% \todo{Briefly explained in advance, we will handle synchronisations between algebraic datatypes rather than lists, trees, or some other containers types \cite{Abbott-containers}.}

\todo{to be rewritten in the end}
The rest of the paper is organised as follows:
(Contribution 1:) In \autoref{sec:links_and_retentiveness} we propose the notion of links, the composition operation between links, and retentiveness.
%
(Contribution 2:) To validate the feasibility of retentiveness, in \autoref{sec:dsl} we design a lightweight domain-specific language, and show the program for solving the problem in \autoref{fig:running_example}.
Then we present the syntax and semantics of the DSL, in order to prove that any program written in our DSL give rise to a pair of |get| and |put| satisfying retentiveness.
The proof is shown in \autoref{sec:proof}.
%
(Contribution 3:) To show the usefulness of the retentiveness, in \autoref{sec:case_studies} we present examples in two different areas: code refactoring~\cite{fowler1999refactoring} and resugaring~\cite{pombrio2014resugaring,Pombrio:2015:HRC:2784731.2784755},
and discuss the potential usage in other settings.
%
\todo{Related work and discussions comes in sequence.}


\todo{getput and putget are trivial to satisfy. On the other hand, (in state-based frameworks) some laws such as $\tit{PutPut}$ is too strong in many scenarios and prohibits many useful update.
The reader may note that existing bidirectional languages can also be extended to be aware of retentiveness.
}


\section{Correspondence Links and Retentiveness}
\label{sec:links_and_retentiveness}

In this section we will develop a definition of retentiveness, formalising the statement ``if parts of the view are unchanged then the corresponding parts of the source should be retained'' in a step-by-step manner.
As mentioned in \autoref{sec:intro}, the core idea is to use links to relate parts of the source and view; this is inspired by the notion of \emph{provenance} from the database community.
(See \autoref{subsec:provenances} for detailed explanation.)
For example, \autoref{fig:get_and_swap} shows the set of \emph{consistency links} between |CST| and |AST| in \autoref{fig:running_example}, indicating, for each part of the view, from which part of the source it is computed by the |getE| and |getT| functions in \autoref{fig:running_example_datatype_def}.
This set of links can be updated along with the view, maintaining the relationship between the updated view and the source --- for example, if we swap the subtrees |Num 1| and |Num 2| in |AST| to obtain |AST'|, then the links should be updated as well, where the two subtrees in |AST'| are still linked to where they originally come from in |CST|.
These two links serve as instructions to a retentive |put| that, in the updated |CST_1| it produces, the parts corresponding to the subtrees |Num 2| and |Num 1| should be exactly those fragments of |CST| linked with the subtrees.
That is, if we find the consistency links between |CST_1| and |AST'|, we will be able to locate the corresponding parts of |CST_1| and assert that they should be the same as those fragments of |CST|; this is visualised in \autoref{fig:swap_and_put}, and in particular we put in some vertical links between |CST| and |CST_1| to say that the linked parts should be the same.

From the above sketch, we see that we will need two kinds of link: \emph{horizontal links} for linking corresponding parts of the source and view, and \emph{vertical links} for asserting which parts should be retained after updating.
We will formalise the two kinds of link in \autoref{subsec:two_links}, and define their composition in \autoref{subsec:composition}.
This composition will enable us to give, in \autoref{subsec:retentiveness}, a concise definition of retentiveness after we enrich |get| and |put| to process and produce links.


\subsection{Horizontal and Vertical Links}
\label{subsec:two_links}

% the consistency links between the right subtree of |Plus| and the right subtree of |Add| and  are depicted
\begin{figure}[tbh]
\includegraphics[scale=0.35,trim={1cm 5.2cm 1cm 2.3cm},clip]{pics/get_and_swap.pdf}
\caption{Consistency links between |CST| and |AST|, and between |AST| and |AST'|}
\footnotesize{To avoid mess and saving spaces, only parts of the consistency links are depicted and annotations in the |CST| are omitted as ellipses. The data in grey background are considered as a whole. (For example, |Neg "..."| is connected to |Sub (Num 0)|.) The links are updated along with the swap, so subtrees in a view (for example |Num 2|) can still track the link to where they originally come from.}
\label{fig:get_and_swap}
\end{figure}

\begin{figure}[tbh]
\includegraphics[scale=0.35,trim={1cm 3cm 1cm 2.3cm},clip]{pics/swap_and_put.pdf}
\caption{Commutative diagram showing links among |CST|, |AST'|, and |CST_1|}
\footnotesize{There are four commutative diagrams drawn with different styles of lines. The links between |CST| and |AST'| are sent to |put_1| to produce |CST_1|. The links between |CST_1| and |AST'| are consistency links built from |get CST_1|. The parts connected by links between |CST| and |CST_1| are equal.}
\label{fig:swap_and_put}
\end{figure}



\subsubsection{Horizontal Links as Dependency}
We borrow the notion of \emph{dependency-provenance} introduced by Cheney {et~al.}~\cite{Cheney:2011:PDA:2139690.2139696} to express horizontal links.
Dependency-provenance reveals the dependency of a computation process and tells the user ``computation of a certain part in the result depends on which parts in the input data''.
In this paper we adopt a simpler notion of ``parts of data''.
Since the source and view data are inhabitants of algebraic datatypes \todo{J: need to say somewhere earlier that we assume both the source and view types are simple, mutually recursive algebraic datatypes} and thus trees, we can use \emph{paths (from the root)} to identify a subtree of the data, and \emph{patterns} to identify a region starting from the root of the subtree.
We define a horizontal link as a pair of patterns (of type |Pat|) and paths (of type |Path|):
\begin{code}
type HLink = ((Path, Pat), (Path, Pat))
\end{code}
A pair of type |(Path, Pat)| identifies a fragment of a tree: we go down the path from the root of the tree and reach a subtree, and the identified fragment is the part of the subtree matching the pattern.
An |HLink| between two pieces of data then says that there is a correspondence between their identified fragments.
\todo{See below for a concrete representation of |Path| and |Pat| and some examples.}
\todo{The concrete representation for paths and patterns are shown later.}

\subsubsection{Vertical Links as Equality}
Vertical links are similar to \emph{where-provenance}~\cite{Buneman2001}, which shows from which place a piece of data is \emph{copied}.
In other words, they are a special case of horizontal links where the two patterns are the same, since the two linked fragments should be equal.
We express this restriction more explicitly by defining a new type that has only one pattern:
\begin{code}
type VLink = (Path, Pat, Path)
\end{code}



\subsection{Composition of Links}
\label{subsec:composition}

In the beginning of this section we mentioned that horizontal links can be updated along with the view, but in practice, view-updating is usually performed by a link-agnostic tool (like an existing refactoring tool), and we need to somehow find a set of correspondence links between the original source and the updated view ourselves.
One way is to perform a ``diff'' on the original and updated views~\cite{Miraldo:2017:TDS:3122975.3122976} and then modify the consistency links between the original source and view accordingly.
If the ``diffing'' produces a set of vertical links describing what is copied into the updated view from the original view, we can then compute the correspondence links by extending the consistency links (between the original source and view) with the vertical links to reach the updated view.
That is, the correspondence links can be computed by some kind of \emph{composition} of horizontal and vertical links.

Composition of a horizontal link and a vertical link is denoted by the $(|linkComp|)$~operator, which is distinguished from function composition~$(\circ)$. (Syntactically, we assume that link composition has the same precedence as function composition: lower than function application but higher than all other operations.)
%
Given a horizontal link |hl = ((path_l,pattern_l) , (path_r,pattern_r))| and a vertical link |vl = (path_t , pattern_v , path_b)|, their composition is defined (partially) by\footnote{The direction of link composition is from left to right, which is different from the direction of function composition in most programming languages.}:
\begin{code}
hl linkComp vl =  ((path_l,pattern_l) , (path_b,pattern_r))
                  {-"\qquad\text{if }"-} path_r = path_t {-"\text{and }"-} pattern_r = pattern_v
\end{code}
We can then lift this composition to work for a list of horizontal links |hls :: [HLink]| and a list of vertical links |vls :: [VLink]|:
\begin{code}
hls linkComp vls = [ hl linkComp vl  |  hl <- hls,  vl <- vls,  (hl linkComp vl) {-"\text{ is defined}"-} ]
\end{code}
where the composition operator is overloaded.
(Note that this lifted composition becomes total.)
We further overload the composition operator for a symmetrical definition for composing a vertical link with a horizontal link:
\begin{code}
vl linkComp hl =  ((path_t,pattern_v) , (path_r,pattern_r))
                  {-"\qquad\text{if }"-} path_l = path_b {-"\text{and }"-} pattern_l = pattern_v
\end{code}
and also the lifted version:
\begin{code}
vls linkComp hls = [vl linkComp hl  |  vl <- vls,  hl <- hls,  (vl linkComp hl) {-"\text{ is defined}"-} ]
\end{code}
This alternative composition will be used in the definition of retentiveness below.


\subsection{Retentiveness}
\label{subsec:retentiveness}
We have introduced horizontal links, vertical links, and their compositions, which are the ingredients we need for extending the definition of well-behaved lenses to retentive lenses.
We first extend |get| and |put| to produce and process links:
% \begin{equation}
\begin{code}
get  ::  S -> (V, [HLink])

put  ::  S -> V -> [HLink] -> (S, [VLink])
\end{code}
% \end{equation}
where
\begin{itemize}
  \item |get| produces a list of horizontal links (of type |[HLink]|) between the input source and the computed view, and
  \item |put| takes a list of horizontal links into account when producing a new source, along with which a list of vertical links (of type |[VLink]|) between the original source and the new source is produced.
\end{itemize}
We call the horizontal links produced by |get| the \emph{consistency links} \myworries{we have used this word somewhere before the definition}.
For brevity, we define some shorthands for compositions of projection functions with |get| and |put|:

\begin{minipage}[t]{.4\textwidth}
\vspace{-2ex}
%\setlength{\mathindent}{0em}
\begin{code}
get_v  s  =  fst  (get s)
get_l  s  =  snd  (get s)
\end{code}
\end{minipage}
\begin{minipage}[t]{.6\textwidth}
\vspace{-2ex}
\setlength{\mathindent}{0em}
\begin{code}
put_s  s v hls  =  fst  (put s v hls)
put_l  s v hls  =  snd  (put s v hls)
\end{code}
\end{minipage}

The well-behavedness properties should be reformulated for |get| and |put| functions of the new types.
Correctness is essentially the same:
\begin{equation}
|get_v (put_s s v hls) = v|
\tag{Correctness}\label{def:retentive-correctness}
\end{equation}
while hippocraticness now has effect only when the horizontal links accepted by |put| are the consistency links, which indicate that the view is still fully consistent with the source:
\begin{equation}
|get s = (v, hls)| \quad\Rightarrow\quad |put_s s v hls = s|
\tag{Hippocraticness}\label{def:retentive-hippocraticness}
\end{equation}
In addition, we formulate the new retentiveness property as:
\begin{equation}
  |put s v hls = (s', vls)| \quad\Rightarrow\quad
  |vls linkComp get_l s' = hls|
\tag{Retentiveness}\label{def:retentiveness}
\end{equation}
% get_v (put_s s v' ls)                       = v'
% put_l s v'  linkComp get_l (put_s s v' ls)  = ls
%  Given a source state~|s|, a view state~|v|, and a set of horizontal links |hls|, we require that (|get| produces both a view and a set of links. The subscript |l| in |get_l| means that we extract the links from the result)
To see that retentiveness indeed captures our intuition described at the beginning of this section:
Consider a horizontal link |hl `elem` hls|.
Its existence means that a part of the view corresponds to a part of the original source, and the latter should be retained as the part of the new source that corresponds to the part of the view.
This retention is assured by a suitable vertical link, which necessarily exists in |vls| if |vls linkComp get_l s' = hls| holds.

\myworries{no concrete implementation for path and patterns yet, for this example (concrete implementation has been moved to somewhere later)}


\subsection{Abstract framework for Retentiveness}

\todo{Do we really need all of the three laws? According to Retentiveness, intuitively, the new source should be identical to the old one if all the information in the old source are going to be preserved.
In this sense, will Retentiveness, as local correctness, subsumes Correctness, the global correctness, such that Correctness becomes a special case of the Retentiveness? The answer is yes, but not already.
}

There are basically two problems preventing us from deriving Correctness from Retentiveness. We summarised the two problems here (in an informal way):
\begin{itemize}
  \item Retentiveness only cares about what is retained. However, it does not prevent new stuff being added. In other words, |put| may insert ``useless information'' into the newly generated source as long as the information is dropped by |get|. (Remember that |get| in general is not injective.)
  % example

  \item Retentiveness does not specify the exact positions for the retained information in the new source. In other words, any ``information regarding position'' is not taken into account, in the current Retentiveness law.
  For example, suppose |get| is a sorting process and |get_v [3,2,1] = [1,2,3]|. Then |put [3,2,1] [1,2,3] hls| (where |hls| is the consistency links generated by |get_l [3,2,1]|) can still produce a source |[2,1,3]| obeying Retentiveness but violating Correctness.
\end{itemize}


A quick fix for the two problems is to add a new kind of link, \emph{edge links}, which carry the information regarding relative positions of two nodes in a source and corresponding two nodes in a view. Note that only if two adjacent nodes (i.e. a parent and a child) in a source both have consistency links to (two adjacent nodes) a view, we can draw an edge-link relating the adjacent nodes in a source and in a view. (but not necessary to draw an edge-link).
%
The effectiveness of the approach can be checked:
\begin{itemize}
  \item If there is an edge-link, nothing should be inserted in between the two adjacent nodes. Otherwise the edge-link will be broken and Retentiveness does not hold.

  \item If there is some modifications to the data, for example, swapping two subtrees of a parent node |X| in the view, the edge links between |X| and its two subtrees are broken, and new information can be added in between.
  \myworries{Permission for adding new information is sometimes necessary. For instance, the types of two subtrees might be different, and we need to add some data (constructors) to make the types match if the two subtrees are swapped}
\end{itemize}

\myworries{However, adding this kind of link is still not sufficient to let Retentiveness subsume GetPut. The reason is (a little) obvious: we cannot draw an edge link for the top of a tree to prevent inserting information to the top. In other words, it is not sufficient to guarantee the top node in an old source remains the top in a new source.
In general, these links cannot prevent data of algebraic data types from ``growing'' up.
The growing points for this example happens to be one, which is not necessarily.
For a list, it may grow at either the head or the rear.}

It is far from satisfactory to include all these odds and ends, just for subsuming Correctness in terms of links.
Thus we wish to build a high-level and abstract framework, in which Correctness can be easily derived from Retentiveness, in a reformulated form.
Links will be just one of the possible instantiation of the abstract framework.


The key idea for building the abstract framework, is to decompose sources and views into a set of regions, and properties on or between the regions.
Then various (horizontal) links are just relations that relate regions and properties in the source and those in the view:
\begin{itemize}
  \item Patterns are a kind of (concrete representation of) regions. As \autoref{fig:get_and_swap} shows, the CST can be decomposed into many patterns (in grey background) such as |Plus _ _ _|, |Neg _ _|, etc.

  \item A link stating that the linked nodes |T| and |T'| are top of trees: there are unary relations (predicates) on node |T| and |T'| respectively. The link relates these two (unary) relations.

  \item An edge link stating that no information can be inserted in between the linked nodes |P| and node |C| in the source, and linked nodes |P'| and |C'| in the view: there are binary relations between |P| and |C|, and |P'| and |C'| respectively. The link relates these two (binary) relations.

  \item For other links relating regions (patterns in our concrete setting) in a source and regions in a view, they simply means that connected regions (patterns) are consistent. \myworries{definition of consistent for patterns?}
\end{itemize}
%
It is evident that the idea is extensible, in the sense that whenever we need a new kind of link carrying whatever complex information, we can always represent this information using relations, perhaps in an n-nary form.
But according to our experience, up to binary relation is enough for most cases.

\subsubsection{Signature for Data Structures}

Formally, our abstract framework requires that sources and views can be represented by a set of regions |rs|, unary relations |trait_1|, and binary relations |trait_2|.
Given a source (or view), the types of its regions, unary relations, and binary relations are defined by
\begin{code}
record ConfigSig (S : Set) : Set_1 where
  Region  : Set
  Trait_1 : Region -> Set
  Trait_2 : Region -> Region -> Set
\end{code}
We call this record a \emph{signature} for the given source (or view).

Known a signature, (perhaps) the simplest way to represent it is to use a dependent triple consisting of lists of regions and relations, where types of relations depend on the regions:
\begin{code}
Config : {S : Set} -> ConfigSig S -> Set
Config sig = sigma  [ rs elem List Region ]
                    List ( sigma [ i elem Fin (length rs) ] Trait₁ (rs !! i)) ×
                    List (  sigma [ i elem Fin (length rs) ]
                            sigma [ j elem Fin (length rs) ]
                            Trait₂ (rs !! i) (rs !! j))
  where open ConfigSig sig
\end{code}
The instance of type |Config| is called a \emph{configuration}.



\subsubsection{Retentive Lenses}
In addition to |get| and |put| functions, a retentive lens consists of source and view signatures (|sigS| and |sigV|), and decomposation functions (|decompS| and |decompV|) which produce configurations of sources and views.
\begin{code}
  sigS       : ConfigSig S
  sigV       : ConfigSig V

  get        : S -> V
  getP       : Config sigS -> Config sigV

  decompS    : (s : S) -> Config sigS
  decompV    : (v : S) -> Config sigV

  put        : (s : S) (v : V) (config : Config sigS) -> Compat config v ->
               Σ[ s' ∈ S ] get s' ≡ v × Satisfy sig config s'
\end{code}

\myworries{since |->| is used for constructing function type, we use |=>| for logic implication.}
\paragraph{Complete Property Set} A set of regions, unary relations and binary relations are \emph{complete} (denoted by |crels|) if it can uniquely determine a source (or view).
In other words, any two sources (or views) satisfying the same complete |crels| are equal.
\begin{code}
  crels complete <===>
  (forall s, s' ldot (satisfy s crels && satisfy s' crels => s == s'))
\end{code}
% Note that if a given |rels| is not complete, we cannot deduce that |s == s'| even if they both satisfy |rels|.

The decomposation functions |decompS| \myworries{and decompV} should produce a set of regions and relations that are complete:
\begin{code}
  forall s, s' ldot satisfy s' (decomp s) == True => s == s'
\end{code}

\paragraph{View-Compatible} A set of regions and relations on a source |s| are \emph{compatible} with a view |v| if the result of the following check is |True|:
\begin{code}
  compat : Config sigS -> V -> Bool
  compat configS v = (getP configS) subseteq (decomp v)
\end{code}
  % or [cprels subseteq decomp s | s <- getInv v]
In particular, |decompS| always produce a set of compatible regions and relations:
\begin{code}
  getP (decomp S) subseteq (decomp (get s))
  compat (decomp S) (get s) == True
\end{code}


Now the input for a |put| function is original source |s|, (modified) view |v'|, plus a set of properties (including regions) |P_ss'| to be preserved.
The properties |P_ss'| should be a subset of any complete property set of |s| (in particular, |decomp s|), and be compatible with the new view |v'|.
\myworries{The framework is for specifying properties to be \emph{preserved}, but not for specifying any other property not satisfied by the original source.}
\begin{code}
  P_ss' subseteq (decomp s)
  getP P_ss' subseteq (decomp v')

  put s v' P_ss' = s' => P_ss' subseteq (P_s setconj P_s')
    where   P_s  = decomp s
            P_s' = decomp s'
\end{code}

\begin{figure}[tbh]
\includegraphics[scale=0.38,trim={5cm 2cm 6cm 8cm},clip]{pics/abstractF.pdf}
\caption{Illustration of ...}
\label{fig:abstractF}
\end{figure}

% This is because that a tree can satisfy many sets of relations and we only require that the `decompose` function gives a unique decomposition (I guess it should the "maximum" decomposition.)

% We say that a set of (unary and binary) relations *r* are **compatible** with a view if ``or [r `subset` decompose s | s <- getInv v]`` returns `True`, where `getInv` returns all the sources consistent with the view (`s ~ v` if `get s == v`).




\paragraph{Example} Let us see a concrete example in \autoref{fig:swap_and_put},
in which one of the input link for |put_1| to produce |CST_1| is |hl = ((Lit "sub" _,[1,2]) , (Num _ , [1,1]))|.
Now we track the execution of |put_1| and suppose the current (focus on the) view is |Num 1|,
and we need to produce a source fragment consistent with |Num|.
The link |hl| will be used to fetch the source fragment |Lit "sub"| from the top of |CST| through the path |[1,2]|, and copied to the newly created source.
Thus we have a vertical link |vl = ([1,2],Lit "sub" _,[2,1])|.
If we perform |get| on the newly created source, there will be a horizontal link |hl' = ((Lit "sub" _, [2,1]), (Num 1, [1,1]))| as the figure shows.
By the definition of composition, we have |vl liniComp hl' = ((Lit "sub" _, [1,2]) , (Num 1, [1,1])) = hl|,
which satisfies |Retentiveness|.


% \todo{replace or supplement the above with an example}

\todo{explicit about link validity?}

%According to the definition of the vertical links, the existence of |vls| directly tells us that each pair of trees connected by a link $\tit{vl} \in \tit{vls}$ are \todo{equivalent up to the pattern?}.
%Thus any information in $s$ captured by $\tit{hl} \in \tit{hls}$ remains intact in $s'$ after |put|.

% \todo{user write |EAdd x _ y ~ |. In vertical links, the pattern becomes |EAdd _ a _|}

% For any source state $s$ and (probably inconsistent) view state $v'$ and a set of links $l_{sv'}$ between $s$ and $v'$,
% we require that there always exist a new source $s'$, a set of vertical link $l_{ss'}$ between $s$ and $s'$, and a set of horizontal link $l_{s'v'}$ between $s'$ and $v'$ such that the diagram commute: $l_{ss'} \cdot l_{s'v'} = l_{sv'}$.
% Now we rephrase the equation in the asymmetric BX frameworks.



\subsubsection{Representation of Patterns and Paths}
\label{subsubsec:rep_of_links}
\paragraph{Patterns}
We borrow the notation of Haskell patterns, to avoid redefining patterns and pattern matching.
The patterns we use is a subset of Haskell patterns, containing only constructors, constants, variables, and wildcards (|_|), as shown in \autoref{fig:DSL-syntax}.
\todo{The patterns in horizontal links and vertical links are of the same syntax, but are interpreted in a slightly different way.}


% \todo{consider removing. Please note that even variable patterns in Haskell are just ``named wildcards'', in vertical links they are interpreted in a different way to express equality more conveniently:
% if two pieces of data match a variable pattern, they need to be the same (equal) although we do not care the exact content.}
% As an illustration, given a vertical link |(path_0, Cons _ _, path_1)|
% $(\textit{path}_0 \, , \, \textit{Cons}\ x\ \_ \, , \, \textit{path}_1)$,
% both of the subtrees at $\textit{path}_0$ and $\textit{path}_1$ must be constructed by $\tit{Cons}$.
% While there is no restriction on the right subtrees, their left subtrees must be equal.

\paragraph{Paths}
%We already see that inhabitants of algebraic datatypes are trees.
%For each of inductive datatypes $t$, we may always generate its corresponding path datatype, whose inhabitants are guaranteed to be the set of valid paths on $t$.
% \todo{By using strongly-typed paths with rich information, we may also drop the patterns in the definition of horizontal and vertical links as they are already encoded into the types of the paths.
% For instance ...}
For simplicity, in this paper we represent paths as a sequence of natural numbers.
Each number~$i$ means that we need to walk into the $i$th field (subtree) of the current piece of data.
For instance, a path denoted by |[2,1,...]| means that from the top of a tree, we select its third subtree $t_3$, and from $t_3$ we select its second subtree, and so on. (We count from zero.)

\paragraph{Examples} Let us see some concrete examples regarding the representation of links in \autoref{fig:get_and_swap}.
The horizontal link connects |Neg| and |Sub| is represented as |( (Neg x _,[2]) , (Sub (Num 0) _,[1]) )|,
where the constructors and constants (of the data) are preserved in the pattern as it is;
the annotation field (which is projected through the process of |get|) in |Neg| is marked by a fresh variable |x|;
the right subtrees of |Neg| and |Sub| are marked by underscores in the pattern,
since we do not care about them (they are connected by another link).
%
After |Num 1| and |Num 2| are swapped, let us see two typical vertical links in \autoref{fig:get_and_swap}: | ([1], Sub (Num 0) _ ,[1]) | and |([2,2], Num _, [1,2])|.
The subtree |Sub (Num 0) _| is not affected by the swap, so its paths in |CST| and |CST'| are the same.
While the |Num _| is moved to the left part of the tree, and hence the path is updated in |CST'| as well.



%
\section{A DSL Guaranteeing Retentiveness}
\label{sec:dsl}

To show that the property of |Retentiveness| is realisable, in this section we design a lightweight DSL supporting simple mutually recursive algebraic datatypes, in which the local consistencies between the source and the view can be described easily.
From the user-defined consistency relations, a pair of well-behaved |get| and |put| functions satisfying retentiveness will be generated.
In \autoref{subsec:arith_program_dsl}, we first give the \emph{consistency relations} for solving the program appeared in \autoref{fig:running_example},
showing that we can produce either |CST_1|, |CST_2|, or |CST_3| by just passing a retentive |put| function different links stating which parts in the source should be retained.
\todo{Then we introduce the the syntax of the in \autoref{subsec:syntax}. The detailed generation of the pair of |get| and |put| is presented in the next section.}
%
The approach we presented here is general, and traditional state-based bidirectional languages can also be extended to incorporate correspondence links.
% There are two main reasons why we decide not to extend the existing bidirectional languages but create a new one:
% \begin{itemize}
%   \item our DSL is designed for describing the consistency relations between two trees easily, which is suitable for our application scenario.
%   \item it is easier to prove the retentiveness of a lightweight DSL rather than full-fledged ones.
% \end{itemize}



\subsection{Solving the Synchronisation Problem in our DSL}
\label{subsec:arith_program_dsl}

% \todo{From the above program, we statically know how to build consistency links.}

The program for synchronising CSTs and ASTs defined in \autoref{fig:running_example_datatype_def} can be written in our DSL as:
\renewcommand{\hscodestyle}{\small}
\begin{multicols}{2}
\begin{code}
Expr <---> Arith
  Plus   _  x  y  ~  Add  x  y
  Minus  _  x  y  ~  Sub  x  y
  Term   _  t     ~  t       ;

Term <---> Arith
  Lit     _  i   ~  Num  i
  Neg     _  r   ~  Sub  (Num 0)  r
  Paren   _  e   ~  e             ;
\end{code}
\end{multicols}%
%recover font size in code environment
\renewcommand{\hscodestyle}{}%
The program states that we need to synchronise data of type |Expr| and |Arith|, and the synchronisation itself need to call another synchronisation for data of type |Term| and |Arith|.
So we have two groups of consistency \todo{declarations}.
Let us look at a particular consistency relation |EAdd _ x y ~ Add x y|.
It says that a piece of data |EAdd _ x y| is consistent with another piece of data |Add x y|, under the condition that both the two subtrees marked by |x| and the two subtrees marked |y| are consistent.
The field marked by underscore (|_|) is not considered as the consistency requirements.




% From the declarative program, a |get| and a |put| function will be generated.
There is a top level |GenProgram| function, which will generate a pair of |get| and |put| functions and some auxiliary functions for (each group of) well-formed input consistency \myworries{declarations}.
We will use some dependent function types, which we can emulate to some extent in our actual Haskell code.
% \renewcommand{\hscodestyle}{\small}
\begin{code}
get :: (s :: SType) -> (v :: VType) -> s -> (v, [HLink])

put :: (s :: SType) -> (v :: VType) -> s_0 -> v -> Env -> (s, [VLink])

type Env    =  [HLink]
\end{code}
% \renewcommand{\hscodestyle}{}
% GenProgram1 (grps) = (GenGet grps) ++ (GenPutRec grps)
% putRec : (s : SType) -> (v : VType) -> SPat -> s_0 -> v -> Env -> (s, [VLink])
Compared to the definitions in \autoref{subsec:retentiveness}, the generated |get| and |put| functions take as additional arguments the types of the input source (|s :: SType|) and view (|v :: VType|), on which the output types of |get| and |put| functions depend.
For |put|, |s_0| is the type of the original source, from where the links of type |Env| fetch data.
(Note that |s_0| can be different from the type of the generated source. For example, even if we want to produce a new source of type |Term|, we may still need to fetch data from an original source of type |Expr|.)

% \todo{explain type tags}
For the real Haskell implementation, we emulate dependent types by assigning each source type and view type a \emph{type tag}.
All of the source type tags and view type tags form the union type |SType| and |VType| respectively.
% \todo{explain encoding of patterns (used in links)}
The patterns of links also need to be encoded, to have their representations as Haskell data.
Briefly, we collect all the source patterns and view patterns appeared in the consistency declarations,
and assign different constructors to different patterns (in a way like hashing each pattern to a unique number).
Note that if the view pattern is a variable, we assign it the \emph{unit} pattern $()$.
For example, the runtime representation for the source pattern |Add _ x y| might be |S_ExprArithCase0| and the representation for the view pattern |t| is |()|.
\todo{Given a horizontal link, let us call it an \emph{imaginary link} if it is view pattern is the unit pattern $()$, and otherwise a \emph{real link}.
It is necessary to make an imaginary link for consistency declarations such as |Term _ t ~ t|, otherwise its source information cannot be retained by links.}
% (In fact the source pattern cannot be a variable pattern, as the second case shows.)


% The first case handles the situation when |P (VARS x)| is a constructor pattern. We build a link consisting of patterns made by $\mathsf{mkPat}$ and empty paths.
% $\mathsf{mkPat}$ works in a way like hashing each pattern to a unique number, if the input pattern starts with a constructor.
% For instance, a |sp| of |TF (Factor r)| might be assigned a pattern |SPat_Factor0|, and a |sp| of |Sub (N 0) r| might be assigned a pattern |VPat_Arith1|.

% However, if |sp| is a variable pattern meaning that there is no explicit construct that can be associated with, $\mathsf{mkPat}$ generates for it an \emph{imaginary} constructor represented by the \emph{unit} pattern $()$.
% \phantomsection
% \label{def_imaginary_link}
% Given a horizontal link, let us call it a \emph{real link} if it is made from two constructor patterns;
% and call it an \emph{imaginary link} if it is view pattern is a variable pattern.
% (In fact the source pattern cannot be a variable pattern, as the second case shows.)

% The second case is when |P (VARS x)| is a variable pattern, meaning that there is no construct \emph{in the source} can be associated with the view.
% However, this is prohibited in the DSL because it means that a source can be mapped to infinite number of views and |get| is not deterministic. (Think that in the |get| direction, this relation can be used as many times as we want.)
% So we raise an error for this case.


% \todo{mention (the top-level) `Gen' and type signatures; to make the type constraints clearer, we will use some dependent function types, which we can emulate to some extent in our actual Haskell code.}

% Now we describe the generated |get| function in detail. |get| has the type
% \renewcommand{\hscodestyle}{\small}
% \begin{code}
% get :: (s :: SType) -> (v :: VType) -> s -> (v, [HLink])
% type [HLink]  = [HLink]
% type HLink   = ((Pat, Path), (Pat, Path))
% type Path    = [Step]
% type Step    = [Integer]
% \end{code}
% %recover font size in code environment
% \renewcommand{\hscodestyle}{}
% It takes as input a source type of type |SType|, a view type of type |VType|, and a source of type |s|.
% |get| outputs a view of type |v|, and horizontal links of type |[HLink]|.
% Since the type of the input source (|SType|) can be inferred from the input source, we make it an implicit argument wrapped in the curly brackets.
% (The notation is borrowed from \textsc{Agda}\todo{cite?}.)
%

The representation for paths is also slightly different from the definition given in \autoref{subsubsec:rep_of_links}\\
\begin{minipage}{.5\textwidth}
\begin{code}
type Path   =  [Step]
\end{code}
\end{minipage}%
\begin{minipage}{.5\textwidth}
\begin{code}
type Step   =  [Integer]
\end{code}
\end{minipage}
as we define a path to be a list of steps, of which itself is a list of integers.
For instance, the step from |EAdd _ (EAdd _ _ x) _| to its subtree marked by |x| is |[1,2]|, and the path from the root of a tree to |x| might be |[... [1,2] ...]|.
Defining paths in this way will help us preclude some ill-formed ones:
for example, the path |[1,2,3]| is in fact unreachable in the given list of steps |[[1,2],[3], ...]|,
while the concatenation of the steps will permit it.

%
Now we are able to generate |CST_1|, |CST_2|, and |CST_3| by passing the generated |put| function different links. For example, the |CST_1| is obtained by the link (the swapped subtrees are marked in red) \footnote{we show the (simplified) patterns before encoding for saving spaces}:
\renewcommand{\hscodestyle}{\small}
\begin{code}
hls_1 =  [  ((Plus,[]),(Add,[]))                               , ^^ ((Minus,[1]) , (Sub,[0]))
         ,  ((Neg,[2]),(Sub,[1]))                              , ^^ ((Term,[1,1]) , ((),[0,0]))
         ,  {-" {\color{red} ((Lit,[1,2]),(Num,[1,1])) } "-}  , ^^ {-" {\color{red} ((Lit,[2,1]) , (Num,[0,1]))  } "-}
         ,  ((Lit, [1,1,1]),(Num,[0,0]))                       , ^^ {-" {\color{red} ((1, [1,2,1]), (1,[1,1,0])) } "-}
         ,  {-" {\color{red} ((2,[2,1,1]),(2,[0,1,0])) } "-}  , ^^ ((0,[1,1,1,1]), (0,[0,0,0]))]
\end{code}
\renewcommand{\hscodestyle}{}
% \begin{itemize}
%   \item | hls_1 = [  ((Plus,[])  , (Add,[])),  ((ESub,[1]) , (Sub,[0])) ,  ((Neg,[2])  , (Sub,[1])),|\\
%         \hspace*{3em} | ((ET,[1,1]) , ((),[0,0])) ,  ((EN,[1,1,1]) , (N,[0,0]))] |
%   \item | hls_2 = [  ((EAdd,[])  , (Add,[])),  ((ESub,[1]) , (Sub,[0])) ,  ((Neg,[2])  , (Sub,[1])),|\\
%         \hspace*{3em} | ((ET,[1,1]) , ((),[0,0])) ,  ((EN,[1,1,1]) , (N,[0,0]))] |
%   \item | hls_3 = [... all seven links ...] |
% \end{itemize}
% |hls_1| is empty, so |CST_1| is produced from the view only and nothing in the original source is retained there.
% |hls_2| contains five links, so there are five annotations in |CST_2| remain. The two annotations |"1 in sub"| and |"2 in neg"| that do not connected by |hls_2| are not preserved.
% |hls_3| contains all seven links so in |CST_3| all the annotations remain after the update.

% To give an example, suppose that we are writing a parser for an arithmetic expression language and now we need to map its parse tree representation to the abstract syntax representation,
% by discarding parentheses structure ($\tit{Factor}$ is of highest priority and $\tit{Expr}$ is of lowest. A constructor $\tit{Paren}$ is added as parentheses from high priority to low priority.) and desugaring negative expressions into subtractions with the left operand being zero.
% For example, $\tit{ET}\ (\tit{TF}\ (\tit{Paren}\ (\tit{ET} \ (\tit{TF} \ (\tit{Neg} \ (\tit{ENum} \ 1))))))$
% will be mapped to $\tit{Sub}\ (\tit{Num}\ 0)\ (\tit{Num}\ 1)$.
% The program can be written in our DSL as \autoref{fig:arithmetic} shows.
%
% Note that the consistency relation in \autoref{fig:arithmetic} uses a simplified syntax
% $\textit{Pat} \, `\sim \text{'} \, \textit{Pat}$, which is just syntactic sugar, and will be converted to the full syntax, as the following example shows:
% %
% \begin{minipage}{.45\textwidth}
% \begin{align*}
% & \qquad \qquad \textit{Expr} \longleftrightarrow \textit{Arith} \\[-1ex]
% & \qquad \qquad \textit{EAdd}\ x \ y \sim \textit{Add}\ x\ y \\[-1ex]
% \end{align*}
% \end{minipage}%
% \begin{minipage}{.1\textwidth}
% $\Longrightarrow$
% \end{minipage}%
% \begin{minipage}{.3\textwidth}
% \begin{align*}
% & \textit{Expr} \longleftrightarrow \textit{Arith} \\[-1ex]
% & \textit{ESub}\ \textit{xl} \ \textit{yl} \sim \textit{Sub}\ \textit{xr} \ \textit{yr} \\[-1ex]
%   \Leftarrow\ & \textit{xl} \sim \textit{xr}, \ \textit{yl} \sim \textit{yr}
% \end{align*}
% \end{minipage}

\subsection{Syntax}
\label{subsec:syntax}
The concrete syntax of our DSL is shown in \autoref{fig:DSL-syntax}.
A program in our DSL consists of two parts: datatype declarations ($\tit{HsTypeDec}$) and groups of consistency relations separated by semicolons ($\tit{CRels}$).

\begin{figure}
\begin{minipage}{.48\textwidth}
%
\begin{code}
  Program  ::=  {HsTypeDec}{-"\!\!"-}* CRels

  CRels    ::=  {CRel ';'}{-"\!"-}+

  CRel     ::=  Type <---> Type
                {CRelBody}{-"\!"-}+
\end{code}
\end{minipage}%
%
\begin{minipage}{0.02\textwidth}
\quad
\end{minipage}%
%
\begin{minipage}{0.45\textwidth}
\begin{code}
CRelBody  ::=       Pat '~' Pat

Pat       ::=  Constructor {Pat}{-"\!\!"-}*
          |    Constant
          |    Variable
          |    '(' Pat ')'
\end{code}
\end{minipage}

% Type     ::=  Constructor
%  |     Pat '~' Pat
% '<=='  {Variable '~' Variable}+


\caption{Concrete Syntax of the DSL}
\footnotesize{Nonterminals are defined by (|::=|) and terminals are wrapped in single quotes (`\ ') . ($*$) and ($+$) are from Kleene start syntax meaning zero-or-more occurrence and one-or-more occurrence respectively. $\tit{HsTypeDec}$ is datatype declarations in Haskell.}
\label{fig:DSL-syntax}
\end{figure}
%

\section{Generation of retentive lenses and its correctness}
% \todo{
% and show its semantics in \autoref{subsec:generate_get} and \autoref{subsec:generate_put}, by compiling it to Haskell code.
% The proof of the retentiveness in the next section replies heavily on how the |get| and |put| functions are generated.}


In this section we explain in detail how a retentive lens can be generated from a set of consistency declarations.
We will give a source-to-source translation from the consistency declaration language to Haskell.
As we go through the translation, we will sketch why the generated lens satisfies \todo{\ref{def:retentiveness}} by construction, and also discover ``well-formedness'' requirements about the consistency declarations, hinting at the following theorem.
\begin{theorem}
\textnormal{Given a consistency declaration~|D|, if it is well-formed (see \autoref{sec:well-formedness}), then for every group in~|D| between source type~|S| and view type~|V|, |get S V| and |put S V| in \todo{Gen(D)} satisfy \ref{def:retentive-correctness}, \ref{def:retentive-hippocraticness}, and \ref{def:retentiveness}.}
\end{theorem}
The detailed proof of the theorem is provided in the appendix.

\subsection{Generation of |get|}

Generation of the |get| function is basically converting consistency declaration clauses to function definition clauses: if, for example, |Plus| is consistent with |Add|, then |get| can simply map |Plus| to |Add| and continue with the subtrees recursively.
The consistency links between the source and view are thus established by construction.
However, for |get| to be implementable as a total function in this way, the consistency declaration should satisfy the following well-formedness constraints:
\begin{itemize}
\item The source patterns in every declaration group should cover all cases so that |get| is fully defined.
\item All the source patterns should include constructors so that recursion can be well-founded.
\item For simplicity, we require that, for every declaration clause, the variables in the source pattern should appear exactly once in the view pattern.
The recursive computation can thus simply process all source subtrees and plug the results into the right places indicated by the view pattern.
\end{itemize}

%\begin{enumerate}
%  \item Given input data |P xs|, generate a consistent |Q|.
%        Recursively apply |get| to the subtrees |xs| to produce subtrees |ys| and horizontal links |ls_i|. The view is |Q ys|.
%  \item Since |xs| and |ys| are subtrees, we need to update the links |ls_i| by adding prefix paths.
%        Finally we make a new link |l_0| between the top of the current source |P| and view |Q|, and put the link |l_0| into the front of the updated links.
%        The produced links are |l_0 : concat [... ls_i ...]|.
%\end{enumerate}


% The |get| function is matched against the input data, and all the possible cases of the the input data are described by the consistency relationss.

The translation is more formally described by the |GenGet| transformation below, which is invoked on every consistency declaration clause to generate a definition clause of |get|.
Every clause has the form |P ~ Q| for some source pattern~|P| and view pattern~|Q|; we often write |P(VARS x) ~ Q(VARS y)| to name variables in |P|~and~|Q| as $\vv{|x|}$~and~$\vv{|y|}$ respectively.
(These variables should in fact be suitably renamed to avoid name clashing.)
The individual variables in $\vv{|x|}$~and~$\vv{|y|}$ are denoted by $x_i$ and $y_i$, and the lines of code containing $x_i$ and $y_i$ should be repeated for every corresponding pair of source and view variables.
%
\renewcommand{\hscodestyle}{\small}
\begin{code}
GenGet (P (VARS x) ~ Q (VARS y)) =
  get st vt P = (Q, l_0 : concat [ls_0' ... ls_n'] )
    itWhere
      (y_i , ls_i)  = get (ctypeOf (x_i ; P)) (ctypeOf (y_i ; Q)) x_i
      prefX_i       = (getPrefix (x_i ; P))
      prefY_i       = (getPrefix (y_i ; Q))
      ls_i'         = map (addStepH (prefX_i, prefY_i)) ls_i
      l_0           = (mkLink (P ; Q))
\end{code}
% addStepH : (Step,Step) -> HLink s v -> HLink s v
%recover font size in code environment
\renewcommand{\hscodestyle}{}
%The code in \textsf{sans serif} and a semantic bracket ($\llbracket \cdot \rrbracket$) can be seen as a macro and is interpreted to other code at compile time.
%\myworries{[The |P| (|Q|) in the generated code is short for |P (VARS x)| (|Q (VARS y)|). Wildcards in |Q| should be replaced by default values.}
%
The generated |get| clause does the following:
\begin{enumerate}
  \item Recursively apply |get| to the subtrees~|x_i| of the input data to get their corresponding views~|y_i| and links~|ls_i|.
  \item Since |x_i|~and~|y_i| are subtrees of the source and view, we need to update the paths in the links~|ls_i|, which start from the roots of the subtrees, by adding the right prefixes.
  The prefixes are computed by $\mathsf{getPrefix}$, and added by |addStepH|.
  \item Finally we make a new link~|l_0| between the top of the current source and view using |mkLink|, and put |l_0| into the front of the updated link list. 
%  \todo{brief explanation of mkLink}
\end{enumerate}
%


%\subsubsection{Build Consistency Links} The function $\mathsf{mkLink}$ will create a link between source pattern |P (VARS x)| and view pattern |Q (VARS y)|.
%As \autoref{fig:DSL-syntax} shows, a consistency relations relates two patterns consisting of only constructors (including constants) and variables. So $\mathsf{mkLink}$ works in the following way:
%\renewcommand{\hscodestyle}{\small}
%\begin{code}
%mkLinkt sp@(ConP _)  vp  = ((mkPat sp, []) ,  (mkPat vp, []))
%mkLinkt (VarP _) _       = error "In this case, get is not a function."
%\end{code}
%%recover font size in code environment
%\renewcommand{\hscodestyle}{}
%% mkLinkt :: Pat -> Pat -> HLink
%% itMkPat  :: Pat -> PatCode
%% itMkPat (ConP c ps) = parens c (map itMkPat ps)
%% itMkPat (VarP v )   = "_"
%% itMkPat (LitP l)    = l
%
%%
%\begin{enumerate}
%  \item The first case handles the situation when |P (VARS x)| is a constructor pattern. We build a link consisting of patterns made by $\mathsf{mkPat}$ and empty paths.
%  $\mathsf{mkPat}$ works in a way like hashing each pattern to a unique number, if the input pattern starts with a constructor.
%  For instance, a |sp| of |TF (Factor r)| might be assigned a pattern |SPat_Factor0|, and a |sp| of |Sub (N 0) r| might be assigned a pattern |VPat_Arith1|.
%
%  However, if |sp| is a variable pattern meaning that there is no explicit construct that can be associated with, $\mathsf{mkPat}$ generates for it an \emph{imaginary} constructor represented by the \emph{unit} pattern $()$.
%  \phantomsection
%  \label{def_imaginary_link}
%  Given a horizontal link, let us call it a \emph{real link} if it is made from two constructor patterns;
%  and call it an \emph{imaginary link} if it is view pattern is a variable pattern.
%  (In fact the source pattern cannot be a variable pattern, as the second case shows.)
%
%  \item The second case is when |P (VARS x)| is a variable pattern, meaning that there is no construct \emph{in the source} can be associated with the view.
%  However, this is prohibited in the DSL because it means that a source can be mapped to infinite number of views and |get| is not deterministic. (Think that in the |get| direction, this relation can be used as many times as we want.)
%  So we raise an error for this case.
%\end{enumerate}


\subsubsection{An Example of Generated |get|} |GenGet (Plus _ x y ~ Add x y)| will produce the following code:
\renewcommand{\hscodestyle}{\small}
\begin{code}
get ExprTy ArithTy (Plus _ xS yS) = (Add xV yV, l_0 : concat [xls', yls'])
  where
    (xV, xls)  = get ExprTy ArithTy xS
    prexS      = [1]
    prexV      = [0]
    xls'       = map (addPrefixH (prexS, prexV)) xls

    (yV, yls)  = get TermTy ArithTy yS
    preyS      = [2]
    preyV      = [1]
    yls'       = map (addPrefixH (preyS, preyV)) yls

    l_0        = ((S_ExprArithCase0, []), (V_ExprArithCase0, []))
\end{code}
%recover font size in code environment
\renewcommand{\hscodestyle}{}
We see that the first two arguments of |get| come from the types of the group where this clause belongs.
There are two variables (|x| and |y|) in the consistency declaration clause, so they are handled by lines 1--4 and lines 5--8 in the |where| block respectively.
Finally the link~|l_0| between |Plus| and |Add| is added.
%, with patterns calculated by |itMkPat| at compile time and empty paths.

\subsection{Generation of |put|}

Generation of the |put| function is more complex.
Below we describe an algorithm that can be best understood as creating a new source following the structure of the view while satisfying retentiveness by construction.
To satisfy retentiveness, we should inspect whether the top of the view is linked to some part of the original source.
\begin{itemize}
  \item If there are no links at the top of the view, meaning that retentiveness does not require that anything be retained at the top, we only need to find a declaration clause |P ~ Q| such that the view matches~|Q|, create a consistent source |P|, and call |put| on the subtrees of the view recursively.

  \item If there are links at the top of the view, for each link we can copy a source fragment at the other end of the link to the top of the new source, such that a vertical link can be created to satisfy retentiveness.

\end{itemize}

%\todo{include properties. i.e. input of putRec does not have top level links.}

%Now we explain the generated |put| function in detail.
%We have seen that we need to handle two cases depending on whether there are links at top of the view. The condition is checked by |hasTopLink|.
% This is checked by |hasTopLink|, which takes a list of links and check if there is any link whose path of the current view is an empty list.
\renewcommand{\hscodestyle}{\small}
% NO END
The generated |put| function thus starts with a case analysis on whether there are top-level links:
\begin{code}
put :: (s :: SType) -> (v :: VType) -> s_0 -> v -> Env -> (s, [VLink])
put st vt os v env  |  not (hasTopLink env)  = ...
put st vt os v env  |  hasTopLink env        = ...

type Env = [HLink]

hasTopLink :: Env -> Bool
\end{code}
Next we explain the two cases of |put| in detail.

\subsubsection{When There Are No Links}
% Given input data |Q ys|, if there is no link at top of the view, we only need to create consistent source |P| and call |put| on subtrees to produce |xs| and (links at subtrees) |vls|. Finally we get result |(P xs, vls)|.
In this case we need to choose a declaration clause such that the view matches the right-hand side pattern.
This is done by an auxiliary function |selSPat| that tries to find such a clause and returns (the encoding of) its source pattern.
This source pattern is then passed into another auxiliary function |putRec|: 
%However, since the process will also be called by the case handling links, we rename it to a new function |putRec| mutually recursive with |put|.
\begin{code}
put st vt os v env | not (hasTopLink env) = putRec st vt (selSPat st vt v) os v env

putRec :: (s :: SType) -> (v :: VType) -> SPat -> s_0 -> v -> Env -> (s, [VLink])
\end{code}
%|putRec| has an additional argument of type |SPat| representing one of the source patterns defined in the consistency relations.
%In general, more than one sources can be mapped to the same view, so we need to specify a particular source pattern that the updated source should match.
%(For example, remember in \autoref{fig:running_example} both a negation and a subtraction in the source are mapped to subtractions in the view.)
% To illustrate, both |Neg (ENum 1)| and |ESub (ET (ENum 0) (ENum 1))| are mapped to |Sub (Num 0) (Num 1)| (we omit the |Num| constructors here)
% and we need to tell |putRec| which to generate.
% (This is extremely important for the case handling links, later we will see that we must generate exact the same pattern specified in the link.)

The |putRec| function is defined mutually recursively with |put|.
It handles the recursive calls on the subtrees, and eventually creates a new source that has the specified source pattern.
The function is generated as follows:
\begin{code}
GenPutRec (P (VARS x) ~ Q (VARS y)) =
  putRec st vt (mkPat P) os Q env =
    mkInj st (ctypeOf (P;P)) (P (SUBST z x), concat [vls_0' ... vls_n'])
    itWhere
      prefX_i            = getPrefix (x_i ; P)
      prefY_i            = getPrefix (y_i ; Q)
      env_i              = map (delPrefixH ([],prefY_i)) (queryEnvBy prefY_i env)
      (z_i,vls_i)        = put (ctypeOf (x_i; P)) (ctypeOf (y_i;Q)) os y_i env_i
      vls_i'             = map (addPrefixV ([],prefX_i)) vls_i

queryEnvBy  :: Step -> Env -> Env
delStepH    :: (Step,Step) -> HLink  -> HLink
addStepV    :: (Step,Step) -> VLink -> VLink
mkInj       :: sup -> sub -> (sub, [VLink]) -> (sup, [VLink])
\end{code}
\begin{enumerate}

  \item We recursively invoke |put| on each subtrees~|y_i| of the view with a new environment |env_i|.
  |env_i| is obtained by first dividing $\tit{env}$ into disjoint parts distinguished by the prefix path of $y_i$ ($\tit{prefY}_i$) and then deleting the prefix path.
%  (Deleting the prefix path will maintain the loop invariant that the path to current view is always an empty list.)
We get a consistent new source |z_i| as the result, along with some vertical links~|vls_i| that are evidence of retentiveness for the subtrees.

%  \item Since $z_i$ becomes subtree in the newly generated source, the links connected to it should be updated with a prefix, similar to the case in |get|.

  \item As specified, we create a new source using the pattern~|P|, and use $\vv{|z|}$ as the subtrees.
  The vertical links we produce are simply the recursively computed vertical links, which, after being suitably extended with prefixes, together witness that retentiveness is established for the whole tree, since there are no top-level input links.

  \item This new source might not be of the right type |st| though, in which case we need an auxiliary function |mkInj| to convert it to the type |st| and update the links accordingly.
  This |mkInj| function is supposed to make changes that are discarded by |get| (for establishing \ref{def:retentive-correctness}) and do not disrupt retentiveness already established for the subtrees (for establishing the overall \ref{def:retentiveness}):
  \begin{code}
get_v s               =  get_v (mkInj_s (s, vls))

vls linkComp get_l s  =  mkInj_l (s, vls) linkComp get_l (mkInj_s (s,vls))
\end{code}
  
%  and links |concat [vls_0' ... vls_n']|, however, the type of $P \vv{z}$ might be different from the desired source type |st|, so we need an auxiliary function |mkInj| to convert $P \vv{z}$ into a term of type |st|, and update the links accordingly.

  % (For instance, consider the case when the generated source need to be of type |Expr| but the pattern specified by the link is a negation of type |Term|.)
  
%    \item If the view matches~|Q| and the specified source pattern matches |(mkPat (P))|, we generate the consistent data $P$ (using the consistency relations $P\ \vv{x} \sim Q \ \vv{y}$).
\end{enumerate}

%\paragraph{Example}

%Given consistency declaration |Plus _ x y ~ Add x y|, we have the following code for |putRec|
%\begin{code}
%putRec ExprTag ArithTag S_ExprArithCase0 os (Add xV yV) env =
%  (toDyn *** id) (mkInj ExprTag ExprTag (EAdd "X" xSRes ySRes, vls'))
%  where
%    prefxS  = [1]
%    prefxV  = [0]
%    envxV   = map (delStepH ([], prefxV)) (queryEnvBy prefxV env)
%    (xSRes, xSvl) = (flip fromDyn ExprNull *** id)
%                      (put ExprTag ArithTag os xV envxV)
%    xSvl'   = map (addStepSS ([], prefxS)) xSvl
%
%    prefyS  = [2]
%    prefyV  = [1]
%    envyV   = map (delStepH ([], prefyV)) (queryEnvBy prefyV env)
%    (ySRes, ySvl) = (flip fromDyn TermNull *** id)
%                      (put TermTag ArithTag os yV envyV)
%    ySvl'   = map (addStepSS ([], prefyS)) ySvl
%
%    vls' = concat [xSvl', ySvl']
%\end{code}
%The Haskell does not support dependent types, so we need to constantly wrap the new source into a |Dynamic| value and extract it out.
%\todo{flip, (***) means...}



\subsubsection{When There are Links} In addition to creating a consistent source, for each link |hl| at top of the view, we need to establish a vertical link |vl| between the input source and newly created source.
\begin{code}
put st vt os v env | hasTopLink env = (s_4, vls_4)
  itWhere
    (l_, imags, env') = getTopLinks env

    (s_2, vls_2) = itCase l_ itOf
      Just l   ->
        itLet  ((sPat,sPath), _)  = l
               s_0                = fetch l os
               (s_1, vls)         = putRec (typeOf s_0) vt sPat os v env'
               vl_0               = (sPath, sPat, [])
        itIn   (repSubtree sPat s_0 s_1, vl_0 : vls)
      Nothing  ->
        putRec st vt (selSPat st vt v) os env' v

    (s_3, vls_3)  = foldr (splice os) (s_2, vls_2) imags
    (s_4, vls_4)  = mkInj st (typeOf s_3) (s_3, vls_3)
\end{code}
\begin{code}
splice os imag (s_0, vls_0) = (s_2, vl_0 : vls_2)
  itWhere
    ((sPat, sPath), ((), []))   = imag
    s_1                         = fetch l os
    (s_2,vls_2)                 = insSubtree sPat s_1 (s_0, vls_0)
    vl_0                        = (sPath, sPat, [])

insSubtree (P x) s_1 (s_0, vls_0) = (P s_2,vls_3)
  itWhere
    (s_2,vls_2)  =  mkInj (ctypeOf (x ; P x)) (typeOf s_0) (s_0, vls_0)
    vls_3        =  map (addPrefixV ([],getPrefix (x ; P x))) vls_2
\end{code}
% (s_2,vls_2)          = mkInj (typeOf (getVar sPat)) (typeOf s_0) (s_0,vls_0)
% (s_3,vls_3)          = fillHole sPat s_1 s_2
\begin{enumerate}
  \item First we get all of the links that the top of the view is associated with and divide the links into a \emph{real} link |l_| and a list of \emph{imaginary} links |imags|.
  The remaining links (not at top of the view) form the new environment |env'|.
  Then we use separate code to deal with the real link and imaginary links.

  \item (a) If there is a real link (|l_ = Just l|), we first fetch a source fragment $s_0$ at path |sPath| matching pattern |sPat| from the original source |os|.
  |s_0| matches |sPat|, but its subtrees marked by variables in |sPat| need to be further updated.
  For instance, suppose |sPat| is from the (left-hand side of the) consistency relation |ESub _ x y ~ Sub x y|, then the subtrees of $s_0$ marked by the underscore will be retained after the update, while the subtrees marked by $x$ and $y$ need to be recursively updated.
  %
  Instead of invoking |put| on the subtrees of $s_0$ (e.g.: $x$ and $y$) directly, we call |putRec| with the same view $v$ and the new environment |env'| (note that there is no link at top of |v'|) to produce $s_1$ and vertical links $vls$.\footnote{Reusing |putRec| saves lots of generated code.}
  %
  Now the subtrees of $s_1$ are correct (because they are generated by the recursive call) and we move them into $s_0$ to get the final source, by invoking the function |repSubtree| as shown in \autoref{fig:repSubtree}.
  %
  Finally, the vertical link |vl_0| between the original source |os| and the newly created source $s_0$ is added to |vls|.

  (b) If there is no real link, we do not need to create any vertical link in this code snippet.
  So we just call |putRec| with the new environment $\tit{env}'$.


  \item For imaginary links, we run a |foldr| on them.
  (|imags| returned by the function |getTopLinks| have already been sorted according to their paths in the original source, to make sure they also appear in order in the newly created source.)
  Each step of the $\tit{foldr}$ is very similar to the way we handle real links:
  We first fetch a source fragment $s_1$ connected by the |imag| link, and then make the input $s_0$ to become a particular subtree of $s_1$, which is achieved by |insSubtree| as shown in \autoref{fig:insSubtree}.
  Meanwhile, the path of the old links |vls_0| is also updated.
  The particular subtree is marked by the only variable in |sPat|.
  (For instance, the subtree marked by |t| in the |sPat| |ET _ t|.)
  Finally, the vertical link $vl_0$ between |os| and |s_2| is added.
  %

  \item After handling imaginary links, the type of $s_3$ may still differ from the desired type of output source $st$, so |mkInj| is invoked again.

\end{enumerate}

\begin{figure}[tbh]
\includegraphics[scale=0.38,trim={2cm 10cm 1cm 10cm},clip]{pics/repsubtree.pdf}
\caption{Illustration of |repSubtree|}
\footnotesize{Suppose |sPat| is the source pattern from the consistency declaration |Plus _ x y ~ Add x y|. Then the tree |s_0| is correct with regard to the annotation field while incorrect regarding the subtrees marked by |x| and |y|. |s_1| is like a ``complement'' of |s_0|. We can get a correct result By concatenating all the correct subtrees.}
\label{fig:repSubtree}
\end{figure}


\begin{figure}[tbh]
\includegraphics[scale=0.38,trim={2cm 10cm 2cm 5cm},clip]{pics/insSubtree.pdf}
\caption{Illustration of |insSubtree (Paren _ e) s_1 s_0|}
\footnotesize{We insert |s_0| to be one of the subtree of |s_1| according to the input pattern |Paren _ x|, which is also the pattern of |s_1|. The place for the insertion is the subtree indicated by a variable in the pattern (|e| here). Since the type of the tree to be inserted might not match the type of the hole, we need to run |mkInj| first.}
\label{fig:insSubtree}
\end{figure}
 % which is also unique to the pattern because the pattern must contain only one variable.




\subsection{Well-formedness}
\label{sec:well-formedness}

\todo{well-formed programs satisfy retentiveness. write the theorem of retentiveness}
Before generating a pair of |get| and |put| functions, we need to first check if the input program is well-formed and passes the following checks.
From now on, we use the abstract syntax |P (VARS x) ~ Q (VARS y)| to represent a consistency relations, where $P$ and $Q$ consist of constructors and constants appeared in the consistency relations, and $\vv{x}$ and $\vv{y}$ are the variables appeared.


\subsubsection{Define Before Use}
For any consistency relations in a group,
\renewcommand{\hscodestyle}{\small}
\begin{code}
TyS <---> TyV
    P_1 (VARS x_1) ~ Q_1 (VARS y_1)
    ...
    P_n (VARS x_n) ~ Q_n (VARS y_n)
\end{code}
%recover font size in code environment
\renewcommand{\hscodestyle}{}
the datatypes $\tit{TyS}$ and $\tit{TyV}$ must be defined.
|P_1 (VARS x_1), ... P_n (VARS x_n)| must be of type |TyS|,
and |Q_1 (VARS y_2), ... Q_n (VARS y_n) | must be of type |TyV|.
Moreover, for each $x_i \in \vv{x}$ and $y_j \in \vv{y}$ where $x_i = y_j$,
we require that there must be consistency relations defined for |tyX_i| and |tyY_j| somewhere in the program,
where |tyX_i| and |tyY_j| are types of $x_i$ and $y_j$ respectively.
We use the following function to perform these checks, whose result must be $\tit{True}$:
($\tit{vars}$ extracts all the variables in a pattern)
\renewcommand{\hscodestyle}{\small}
\begin{code}
isDefined TyS && isDefined TyV &&

typeOf (P_1(VARS x)) == TyS  &&  typeOf (P_2(VARS x)) == TyS && ...

typeOf (Q_1(VARS y)) == TyV  &&  typeOf (Q_2(VARS y)) == TyV && ...

all . map (\ tyXY -> isRelDefined tyXY) $ $
  map (\ x -> (typeOf x (P_1(VARS(x))), typeOf x (Q (VARS(y))))) (vars (P_1(VARS x))) && ...
\end{code}
%recover font size in code environment
\renewcommand{\hscodestyle}{}
%

\subsubsection{Linear Variable Usage}
The variable usage must be linear:
the number of variables in |P (VARS x)| must be equal to those in |Q (VARS y)|,
and each variable must appear exactly once in both side.
The following check must yield |True|: (|nub| eliminates duplicate elements in a list)
%
\begin{code}
sort (vars (P (VARS x))) == sort (vars (Q (VARS y))) && linear (P (VARS x)) && linear (Q (VARS y))
  where linear xs = nub xs == xs
\end{code}


\subsubsection{Source and View Coverage}
We require that the programs in our DSL to be total, and here we show some minimum checks for guaranteeing totality.
For every user defined datatypes |TyS| and |TyV| that appear in consistency declarations:
The disjunction of patterns |P_1 (VARS x) ... P_n (VARS x)| should cover all the cases of type |TyS|.
Similarly, The disjunction of patterns |Q_1 (VARS y) ... Q_n (VARS y)| should cover all the cases of type |TyV|.
Otherwise the |get| and |put| will be undefined on some input data.

\subsubsection{Source Disjointness}
The source patterns |P_1 (VARS x) ... P_n (VARS x)| in a group of consistency declarations should be disjoint, which is a \todo{sufficient} condition for guaranteeing |Correctness|.
Intuitively, suppose in the |put| direction, the code generated from the consistency declaration |P_i (VARS x) ~ Q_i (VARS y)| is executed, we know the (input) view is of pattern |Q_i (VARS y)| and the new source is of pattern |P_i (VARS x)|.
Then if |P_1 ... P_n| are disjoint, it is sufficient to reason that the |get| generated from |P_i (VARS x) ~ Q_i (VARS y)| will be called on the input of pattern |P_i (VARS x)|.
Hence we get |Q_i (VARS y)| as the same view and |Correctness| holds.





\subsubsection{Interconvertible Datatypes}
The most interesting check is that, if two terms $x$ and $y$ of different types in the source are mapped to the same term $z$ in the view (by different |get| functions), we require that there are injections (the |mkInj| function below) for converting between |typeOf x| and |typeOf y|.
Moreover, the additional data (constructors, etc.) added by the injection
should be projected out in the process of |get|.
\renewcommand{\hscodestyle}{\small}
\begin{code}
mkInj : (t : ToTy) -> (s : FromTy) -> s -> t

get x == get y =>
  get  (mkInj (typeOf x) (typeOf y) y)  == get  y  &&
  get  (mkInj (typeOf y) (typeOf x) x)  == get  x
\end{code}
  % typeOf (mkInj (typeOf x) (typeOf y)  y)  == typeOf x  &&
  % typeOf (mkInj (typeOf y) (typeOf x)  x)  == typeOf y  &&
%recover font size in code environment
\renewcommand{\hscodestyle}{}

These conditions can actually be checked syntactically, without running |get|.
For instance, by reading the left-hand side of the consistency relationss |ET _ t ~ t|,
we know that any data of type |Term| can be converted to a new piece of data of type |Expr| using the |ET| constructor.
And from |Paren _ e ~ e| we know that any data of type |Expr| can be converted to a new piece of data of type |Term| using the |Paren| constructor.

% The third line looks tricky and the reader may wonder why the equivalence is not on two source terms (e.g. $\tit{inj}_{(x,y)} \ x == y$) but rather on their images under the \tit{get} function.
% But think twice, this is actually obvious as the injection can only ``add more information'' to the data so that $\tit{inj}_{(x,y)} \ x == y$ and $\tit{inj}_{(y,x)} \ y == x$ will never hold simultaneously.
% In addition, the third line also indicates that the added information must be dropped by the \tit{get} function, \todo{otherwise |PutGet| will not hold.}

% \todo{revise later.}
The importance of requiring these \emph{interconvertible datatypes} can be found in \autoref{fig:running_example}.
In the process of producing |CST_3|, the second field of |EAdd| is of type |Expr|.
However, the tree (in the original source) connected by the input link is |Neg ...| of type |Term|.
We need additionally add an |ET| constructor to preserve the |Neg ...| in the newly created source.


%end generation

\section{Auxiliaries}

\subsection{Notations}
Since many functions such as |get|, |put|, and |mkInj| not only produce a source (or view) but also a bunch of links, for simplicity, we define some functions to avoid doing projections explicitly, where the subscript $s$ means source, $v$ means view, and $l$ means links:
\begin{multicols}{2}
\begin{code}
  get_v     = fst  . get
  get_l     = snd  . get
  put_s     = fst  . put
  put_l     = snd  . put
  putRec_s  = fst  . putRec
  putRec_l  = snd  . putRec
  mkInj_s   = fst  . mkInj
  mkInj_l   = snd  . mkInj
\end{code}
\end{multicols}

\subsection{Prerequisites}
\begin{prereq}
\textnormal{|mkInj| does not affect the result of |get| and the composation results of vertical and horizontal links. Suppose |(s',vls')  = mkInj (s,vls)|, we require that}
\begin{code}
get_v s               =  get_v (mkInj_s (s, vls))

vls linkComp get_l s  =  mkInj_l (s, vls) linkComp get_l (mkInj_s (s,vls))
\end{code}
\label{prereq:getInj}
\end{prereq}
\vspace{-2em}

\paragraph{Explanation}
The first equation is rather intuitive and simple: we require that all the additional constructors added by the injection function should be later projected through the process of |get|, and hence the view does not change. \\
% For the second equation, we \\
% let | A = inj_l (s_0, vl_0) = map (addStepV ([],prefix)) vl_0 |, and \\
% let | B = snd (get (inj_s (s_0,vl_0))) = map (addStepH (prefix, [])) hl_0 ++ imagLinks |
For the second equation, we have
\begin{code}
mkInj_l (s, vls)          = map (addStepV ([],pref))  vls
get_l (mkInj_s (s,vls))   = map (addStepH (pref, [])) (get_l s) ++ newImagLinks

{-"\\[-4ex]"-}

    mkInj_l (s, vls) linkComp get_l (mkInj_s (s,vls))
= {  after injection, all the links {-"\tit{in}\ "-} vls have a prefix path pref,
     but the the paths {-"\tit{of}\ "-} all the newImagLinks are less than pref,
     so they are {-"\tit{not}\ "-} composable}
    mkInj_l (s, vls) linkComp (get_l (mkInj_s (s,vls)) exclude newImagLinks)

=   map (addStepV ([],pref)) vls linkComp map (addStepH (pref, [])) (get_l s)

= {use theorem 1}
    vls linkComp get_l s
\end{code}



\begin{lemma}
\textnormal{Adding prefix path to the connecting points of all the composable vertical and horizontal links does not affect the result of the composation.}
\begin{code}
map (addStepV ([],pref)) vls  linkComp  map (addStepH (pref,[])) hls = vls linkComp hls
\end{code}
\label{lemma:compBridge}
\end{lemma}
\vspace{-2em}
By definition, for each composable pair $vl = (p_u, pat, p_b) \in vls$ and $hl = ((pat_l,p_l),(pat_r,p_r)) \in hls$, they must satisfy $p_b = p_l \land pat = pat_l$.
Later, |map (addStepV ([],pref)) vl| changes each |vl| to |vl'_0 = (p_u, pat, pref ++ p_b)| and |map (addStepH (pref,[])) hl| changes each |hl| to |hl'_0 = ((pat_l, pref ++ p_u), (pat_r, p_b))|. We still have |pref ++ p_b = pref ++ p_l && pat = pat_l|. So the result of the composation is not changed.
% \begin{code}
%   vl   = (p_u, pat, p_b)
%   hl   = ((pat_l,p_l),(pat_r,p_r))
%   vl'  = (p_u, pat, pref ++ p_b)
%   hl'  = ((pat_l, pref ++ p_u), (pat_r, p_b))

%     vl' linkComp hl'
% =   (p_u, pat, pref ++ p_b) linkComp ((pat_l, pref ++ p_u), (pat_r, p_b))
% =   (p_u, pat, p_b) linkComp ((pat_l, p_u), (pat_r, p_b))
% =   vl linkComp hl
% \end{code}

\begin{lemma}
\textnormal{Modify the paths not used as the testing condition for the composation is equal to first perform the composition and modify the paths later.}
\begin{code}
   map (addStepV (prefX,[])) vls linkComp  map (addStepH ([],prefY)) hls
=  map (addStepH (prefX,prefY)) (vl linkComp  hls)
\end{code}
\label{lemma:compModify}
\end{lemma}
\vspace{-2em}

\begin{corollary}
By \autoref{lemma:compBridge} and \autoref{lemma:compModify}, we have
\begin{code}
   map (addStepV (prefX,pref)) vls linkComp  map (addStepH (pref,prefY)) hls
=  map (addStepH (prefX,prefY)) (vls linkComp  hls)
\end{code}
\label{corollary:compBridgeModify}
\end{corollary}
\vspace{-2em}


\begin{lemma}
\textnormal{|get| after |repSubtree| produces the same result as |get| applied to the last argument of |repSubtree|.}
\begin{code}
get (repSubtree sPat t_1 t_2)  =  get t_2
\end{code}
\label{lemma:getRepSub}
\end{lemma}
\vspace{-2em}
% get_l (repSubtree sPat t_1 t_2)  =  get_l t_2
\paragraph{Proof sketch} |repSubtree| takes a pattern and two trees matching the pattern, in which the second subtree may only differ from the result of the |repSubtree| at some parts described by the pattern.
In addition, the different parts will all be projected through |get|, so the equation trivially hold.




\begin{lemma}
\textnormal{Let |(P s_2 , vls_3) = insSubtree sPat s_1 (s_0, vls_0)|, we have}
\begin{code}
  get_v (P s_2) = get_v s_0

  vls_3 linkComp (get_l (P s_2)) = vls_0 linkComp s_0
\end{code}
\label{lemma:insSubtree}
\end{lemma}
\vspace{-2em}
\paragraph{Proof sketch} Intuitively, |insSubtree| only make input |s_0| a particular subtree of |P| and update links |vls_0| accordingly, where |P| is the constructor of |sPat| and |sPat| is from an imaginary link.
By the definition of imaginary link, |get_v (P xs) = get_v xs|.
|insSubtree| does not add any link, so the links composition should not change either.
Detailed proof can be found in \autoref{sec:proof_insSubtree}.



\begin{lemma}
\textnormal{Let |(s_2, vl_0 : vls_2) = splice os imag (s_0, vls_0)|, we have:}
\begin{code}
get_v s_2                          =  get_v s_0

(vl_0 : vls_2) linkComp get_l s_2  =  imag : (vls_0 linkComp get_l s_0)
\end{code}
\label{lemma:splice}
\end{lemma}
\vspace{-2em}
\paragraph{Proof sketch} |splice| fetch a source fragment |s_1| using the imaginary link |imag| and make |s_0| a subtree of |s_1|. Thus |get_v s_2 = get_v s_0| according to the definition of imaginary links.
For the second equation, since |splice| will build a vertical link |vl_0| between |os| and |s_2| for input |imag|, |vl_0 linkComp get_l s_2| should produce the |imag| link.
Detailed proof can be found in \autoref{sec:proof_splice}



\begin{lemma}
\textnormal{The code handling real links does not affect the result of |get_v|, and necessary produce a vertical link if there is. (|v| is the input view)}
\begin{code}
  get_v s_2 = v

  vls_2 linkComp (get_l s_2) = l : env'
\end{code}
\label{lemma:real_link}
\vspace{-2em}
\end{lemma}
\paragraph{Proof sketch} Given a view |v|, the data |s_0| fetched from the real link |l| at top of |v| is consistent with |v| regarding |sPat| of the link |l|.
The subtrees of |s_0| are from the result of |putRec| without links at the top, and we can use induction hypotheses. So we have |get_v s_2 = v|
The links |vls| contains two parts: the link |vl_0| created for the input link |l|; and the links |vls| built from the recursive call, which should produce |env'| when composed with |get_l s_2| by induction hypotheses.
Detailed proof can be found in \autoref{sec:proof_real_link}.


\begin{lemma}
\textnormal{|mkInj| is an identity function if the types lifted from and to are the same.}
\begin{code}
toTy == fromTy => mkInj toTy fromTy a = a
\end{code}
\end{lemma}
\vspace{-2em}



%end auxiliaries

\section{Proof of Retentiveness}
\label{sec:proof}
In this section, we first (re)present the properties of programs written in our DSL, and then show the proofs in order.
\begin{theorem}
  \textnormal{The pair of |get| and |put| functions generated from a well-formed program in our DSL satisfies:}
  \begin{equation}
    |get s = (v,hls)| \ \Rightarrow \ |put_s (tyOf s) (tyOf v) s v hls = s|
    \tag{Hippocraticness}
  \end{equation}
  \begin{equation}
    |put (tyOf s) (tyOf v) s v hls = (s',_)| \ \Rightarrow \ |get_v s' = v|
    \tag{Correctness}
  \end{equation}
  \begin{equation}
    |put (tyOf s) (tyOf v) s v hls = (s',vls)| \ \Rightarrow \ |vls linkComp get_l s' = hls|
    \tag{Retentiveness}
  \end{equation}
\end{theorem}
The |put| of traditional asymmetric state-based BXs accepts a source state $s$ and a view state $v$,
while our |put| accepts links |hls| as well.
In the proof, we usually write the properties in one line for simplicity, as
\begin{code}
  put_s st vt s (get_v s) (get_l s) = s
  get_v (put_s st vt s v hls) = s
  put_l st vt s v hls linkComp get_l (put_s st vt s v hls) = hls
\end{code}
where |st| and |vt| are |typeOf s| and |typeOf v| respectively.

\subsection{Proof of Hippocraticness and Retentiveness}
The proofs of |Hippocraticness| and |Retentiveness| can be merged, which are overall |get| after |put|.
% We will prove that the |get| and |put| functions generated from our DSL satisfy retentiveness.
% That is, for input view $v$ and links $env$, the following equations hold (here we ignoring some arguments of the |put|):
% \begin{code}
%   get_v (put_s v env) = v
%   put_l v env linkComp get_l (put_s v env) = env
% \end{code}
% Before starting the proof, we need to check that the input links $l_{sv'}$ are all valid:
% for any link |l = ((sPat, sPath), (vPat,vPath))|, it must satisfy
% \begin{code}
% satPat spat (lookup spath s) == True && satPat vpat (lookup vpath v) == True
% \end{code}
% For easy denoting, let us call the code generated from $\texttt{genPut1}$ $\tit{put1}$ and the code generated from $\texttt{genPut2}$ $\tit{put2}$.
The basic idea is to make inductions on the height of the view:
assume Hippocraticness and Retentiveness hold for all the views of height less than |h|,
then we prove that the two properties also hold for the view of height |h|.
Given a source state |s|, a view state |v| of height |h|, and links |env|, there are two cases for |put (typeOf s) (typeOf v) s v env|.
% $$(\forall n. \ (\forall m. \ m < n \rightarrow P \ m) \rightarrow P \ n) \rightarrow P \ n$$


\subsection*{Case 1: When There is no Link at Top of the View.}
Since there is no link at top of the view, |put| will call |putRec|.
Suppose the case of |putRec| generated from the consistency relation |P (VARS x) ~ Q (VARS y)| is matched, then we have:
\begin{code}
    put st vt os (Q ys) env
      {{-"\textnormal{expand the definition of |put|} "-}}
=   putRec st vt (selSPat st vt (Q ys)) os (Q ys) env
      {{-"\textnormal{expand the definition of |putRec|} "-}}
=   mkInj st (ctypeOf (P zs)) (P zs, concat [... vls_i' ...])
\end{code}


\begin{itemize}
  \item For |Hippocraticness|, we need to prove \\
  |get_v (mkInj_s st (ctypeOf (P zs)) (P zs, _)) = Q ys|. \\
  We show the sketch of the proof:
\begin{enumerate}
    \item By \hyperref[prereq:getInj]{Prerequisite 1}, |get_v (mkInj_s ...)| is equal to |get_v (P zs)|.
    \item According to the source disjointness, the |get| generated from the same consistency relations |P xs ~ Q ys| must be chosen for |get_v (P zs)|.
    Thus |get_v (P zs) = Q ys'|, where |ys'| are subtrees |y_0' ... y_n'|,
    \item For each |y_i'|, since its height is less then $h$, we know |Correctness| holds and |y_i' == y_i|. Thus |Q ys' = Q ys| and |Correctness| holds for the view of height $h$.
\end{enumerate}
  See the detailed proof in \autoref{proof_correctness_no_link}.

%
  \item For |Retentiveness|, we need to prove
\begin{align*}
& |put_l st vt os (Q ys) env linkComp get_l (put_s st vt os (Q ys) env)| \\[0.2em]
%
=   & \qquad \{\textnormal{ expand the definition of |put| and |putRec| } \} \\[-0.2em]
& |mkInj_l st (ctypeOf (P zs)) (P zs, concat [... vls_i' ...]) linkComp| \\[-0.2em]
& |get_l (mkInj_s st (ctypeOf (P zs)) (P zs, concat [... vls_i' ...]))| \\[0.2em]
%
=   & \qquad \{\textnormal{ by \hyperref[prereq:getInj]{Prerequisite 1}} \} \\[-0.2em]
& |concat [... vls_i' ...] linkComp get_l (P zs)| \\[0.2em]
%
=   & \qquad \{\textnormal{ expand the definition of |get_l (P zs)|} \} \\[-0.2em]
& |concat [... vls_i' ...] linkComp (mkLink (P xs ; Q ys) : concat [.. ls_i' ..])| \\[0.2em]
%
=   & \qquad \{\textnormal{ ... some proof ...} \} \\[-0.2em]
&  |env|
\end{align*}
  For the |some proof| part, intuitively, we know that
  \begin{enumerate}
    \item |mkLink (P xs ; Q ys)| cannot be composed with any |vls_i'| because it does not have a prefix path.
    \item |concat [... vls_i' ... ]  linkComp  concat [... ls_i' ... ]| is equal to
          |concat [...  vls_i' linkComp ls_i' ... ]| because for any |vls_i'| and |ls_j'| with |i /= j|, their prefix are disjoint and are not composable.
    \item |vls_i'| is obtained from the results of |put| on the subtrees of height less than $h$.
          |ls_i'| is obtained from the results of |put| on the subtrees of height less than $h$.
          So by induction hypotheses, the result of |vls_i' linkComp ls_i'| should be |env_i|.
    \item |concat [ ... env_i ...]| is equal to the input |env| and thus |Retentiveness| holds.
  \end{enumerate}
\end{itemize}

See the detailed proof in \autoref{proof:retentive_no_link}

\subsection*{Case 2: When There are Links at Top of the View}
Since there are links at top of the view, the |put| dealing with links will be executed.
According to \hyperref[prereq:getInj]{\tit{Prerequisite}\ 1}, the last line |mkInj| can be omitted because it has no effect on the equations to be proved.
Then in \hyperref[subsubsec:deal_with_imaginary_links]{Case 2-1} and \hyperref[subsubsec:deal_with_real_link]{Case 2-2} we will prove some more properties to make the following equation hold for |Correctness| and |Retentiveness| respectively:


\begin{code}
    get_v (put_s st vt os v env)
= { expand the definition {-"\tit{of}\ "-} put handling links}
    get_v s_4

= { expand the definition {-"\tit{of}\ "-} put handling links}
    get_v (mkInj_s st (typeOf s_3) (s_3, vls_3))

= { by {-"\ \hyperref[prereq:getInj]{\tit{Prerequisite}\ 1}"-} }
    get_v s_3

= { by the property shown {-"\tit{in}\ "-} Case 2-1}
    get_v s_2

= { by the property shown {-"\tit{in}\ "-} Case 2-2}
    v

{-"\\[-5ex]"-}

    put_l st vt os v env linkComp get_l (put_s st vt os v env)

= { expand put }
    vls_4 linkComp get_l s_4

= { expand put }
    mkInj_l st (typeOf s_3) (s_3, vls_3) linkComp get_l (mkInj_s st (typeOf s_3) (s_3, vls_3))

= { by {-"\ \hyperref[prereq:getInj]{\tit{Prerequisite}\ 1}"-} }
    vls_3 linkComp get_l s_3

= { by the property shown {-"\tit{in}\ "-} Case 2-1}
    (vls_2 linkComp get_l s_2) ++ imags

= { by the property shown {-"\tit{in}\ "-} Case 2-2}

=   env
\end{code}



\subsubsection{Case 2-1: Deal With Imaginary Links}
\label{subsubsec:deal_with_imaginary_links}
We will show that the process of handling imaginary links will not affect the result of |get_v|.
Meanwhile, for each input imaginary link, it will necessarily create a corresponding vertical link between the original source and the newly created source.
Suppose |(s',vls') = foldr (splice os) (s,vls) imags|, then:
\begin{code}
  get_v s'                = get_v s

  vls' linkComp get_l s'  = (vls linkComp get_l s) ++ imags
\end{code}
% To do so, however, we need to first prove the properties of |splice|, which helps us easily prove the property (of |foldr|) above.

% \paragraph{Properties of |foldr|} Now we can prove the properties of |foldr| by making inductions on the length of the input list.
We can prove the properties of |foldr| by making inductions on the length of the input list.
If the input has one more imaginary link,
let
\begin{code}
    foldr (splice os) (s,vls) (imag:imags)
=   splice os imag (foldr (splice os) (s,vls) imags)
=   splice os imag (s',vls')
=   (s'', vls'')
\end{code}
we need to prove:
\begin{code}
  get_v s''                 = get_v s

  vls'' linkComp get_l s''  = (imag:imags) ++ (vls linkComp get_l s)
\end{code}
which can be easily derived from the properties of |splice| and induction hypotheses:


\begin{code}
    get_v s''
= {By {-"\ \autoref{lemma:splice}"-}, the property {-"\tit{of}\ "-} splice}
    get_v s'

= {induction hypotheses}
    get_v s

{-"\\[-4ex]"-}

    vls'' linkComp get_l s''
= {By {-"\ \autoref{lemma:splice}"-}, the property {-"\tit{of}\ "-} splice}
    imag : (vls' linkComp get_l s')

= {induction hypotheses}
    imag : (imags ++ (vls linkComp get_l s))

=   (imag:imags) ++ (vls linkComp get_l s)
\end{code}




\subsubsection{Case 2-2: Deal With the Real Link}
\label{subsubsec:deal_with_real_link}

We will show that the process of handling the real link does not affect the result of |get_v|.
Meanwhile, if there is a real link, we will necessarily create a corresponding vertical link between the original source and the newly created source.
That is, refer to the code of |put st vt os v env|, we have
\begin{code}
    get_v s_2
=   v


    (vls_2 linkComp get_l s_2) ++ imags
=   (l : env') ++ imags
=   env
\end{code}


Proof of the first equation |get_v s_2 = v|.
\begin{code}
    get_v s_2
= {By {-"\ \autoref{lemma:real_link}"-}}
    v
\end{code}

Proof of the second equation |(vls_2 linkComp get_l s_2) ++ imags = env|
\begin{code}
    (vls_2 linkComp get_l s_2) ++ imags

= {By {-"\ \autoref{lemma:real_link}"-}}
    (l:env') ++ imags

=   env
\end{code}


\subsection{Proof of |GetPut|}
Induction on the height of the source $h$.

\begin{code}
  put_s st vt s (get_l s) (get_v s) = s
\end{code}

  % put_l st vt s (get_l s) (get_v s) linkComp
  % (get_l (put_s st vt s (get_l s) (get_v s))) = get_l s

  % put_l st vt s (get_l s) (get_v s) linkComp (get_l s) = get_l s


% \begin{code}
%     get_v (P (VEC x))
% =   Q (VEC y)
% =   Q (... get x_i ...)

%     get_l (P (VEC x))
% =   l_0 : concat [... ls_i' ...]
% =   (mkLink (P ; Q)) : concat [... map (addPathH (prefX_i,prefY_i)) ls_i]
% =   (mkLink (P ; Q)) : concat [... map (addPathH (prefX_i,prefY_i)) (get_l x_i)]

% \end{code}

% Obviously, for |put_s st vt os (get_l s) (get_v s)|, there are links at top of the view.
% Suppose the |get| code generated from |P (VEC x) ~ Q (VEC y)| is selected for source |s|.

% The links are produced by |get|, so by all means the top of the view cannot only have imaginary links.
% \begin{code}
%     put_s   st vt s
%             ((mkLink (P ; Q)) : concat [... map (addPathH (prefX_i,prefY_i)) (get_l x_i)])
%             (Q (... get x_i ...))
% =   s_4

% \end{code}

% end proof.


\section{Case Studies}
\label{sec:case_studies}
% If we leap out of DB, and dive into other research area, for example, the code refactoring in software engineering, we will see similar scenarios.
In this section we illustrate how retentiveness is useful in practice by presenting case studies about \emph{resugaring} \cite{pombrio2014resugaring,Pombrio:2015:HRC:2784731.2784755} and \emph{code refactoring} \cite{fowler1999refactoring}.
In both scenarios, we need to constantly make modifications to the ASTs\footnote{Many code refactoring tools make modifications to the CSTs instead. However, the design of the tools can be simplified if they migrate to modify the ASTs, as the paper will show.} and synchronise the CSTs accordingly.
Retentiveness helps the user to know which information in the original CSTs is guaranteed to be retained after the synchronisation.

\subsection{Resugaring}
\label{subsec:resugaring}
For a programming language, usually the constructs of its surface syntax are richer than those of its abstract syntax (core language).
The idea of \emph{resugaring}~\cite{pombrio2014resugaring,Pombrio:2015:HRC:2784731.2784755} is to print evaluation sequences in a core language using the constructs of its surface syntax.
We will show that our DSL is capable of reflecting AST changes resulting from evaluation back to CSTs, by writing a straightforward consistency declaration between the surface syntax and the abstract syntax and passing the generated |put| proper links.
% Now we will show that, by specifying the consistency relations between the program text and the abstract syntax, retentiveness also helps us to achieve resugaring.


Take the language \textsc{Tiger} \cite{Appel:1998:MCI:522388} (a general-purpose imperative language designed for educational purposes) for example: The surface syntax offers logical conjunction (| && |) and disjunction (| |||| |), which, in the core language, are desugared in terms of the conditional construct $\tit{if}\text{-}\tit{then}\text{-}\tit{else}$ during parsing.
Boolean values are also eliminated: |False| is represented by integer~|0| and |True| is represented by a non-zero integer.
%As a result, \textsc{Tiger}'s abstract syntax has neither these constructs, nor the boolean value:
%
% The conversion from |&&| and | |||| | to $\tit{if}\text{-}\tit{then}\text{-}\tit{else}$ is usually performed along with the process of parsing.
For instance, the program text |a && b| is parsed to an AST |If a b 0|, and |a |||| b| is parsed to |If a 1 b|.
%
Suppose that evaluation is defined on the core language only.
If we want to observe the evaluation sequence of |0 && 10 |||| c|, then we will face a problem, as the evaluation is actually performed on |If (If 0 10 0) 1 c| and the printer will yield an evaluation sequence in terms of $\tit{if}\text{-}\tit{then}\text{-}\tit{else}$.

% For a term like |If a 1 b|, If the programmer want to observe every evaluation step on a term done by the interpreter, this actually causes a problem:
% to observe the evaluation process of |a && b |||| c|, use a traditional printer to print every step of the evaluated result does not help, as the printer will yield an evaluation sequence in |if-then-else| construct.
% To address the problem, Pombrio et al. propose the notion of \emph{Resugaring}\cite{pombrio2014resugaring,Pombrio:2015:HRC:2784731.2784755}, which will recover the original construct when ejecting the result of an evaluation upon an AST.

To solve the problem in our DSL, let us first consider a familiar situation where we write a parser in \textsc{Yacc} to desugar logical expressions into conditional expressions:

\begin{code}
OrOp  -> Or     OrOp   AndOp      {If ^^ ^^  dollar2 ^^ ^^ ^^  1 ^^ ^^ ^^        dollar3  }
       | AndOp                    {$1                                                     };

AndOp -> And    AndOp  CompareOp  {If ^^ ^^  dollar1 ^^ ^^ ^^  dollar2 ^^ ^^ ^^  0        }
       | CompareOp                {$1                                                     };
...
\end{code}
in which the concrete syntax is defined by production rules.
Parsing can conceptually be thought of as first recovering a CST (of which the datatypes are automatically generated by Yacc) representing the structure of the program being parsed,
% The datatypes of concrete syntax are automatically generated,
and then converting the CST to an AST by the semantic actions in curly brackets.
Similarly, these intentions can be expressed in our DSL as shown in \autoref{fig:resugaring_dsl},
in which the left-hand side of the consistency declaration is the concrete syntax, and the right-hand side is the abstract syntax.
For instance, in the code snippet, we see that |Or l r| is consistent with |If l 1 r|, if the subtrees marked by the same variable are consistent.
(Currently, our DSL does not handle the part of parsing where strings are converted to trees, and instead assumes that the CST has already been found.
In other words, it handles the synchronisation between CSTs and ASTs, which is the more interesting part of the task of parsing.
See our previous work on BiYacc~\cite{Zhu:2016:PRP:2997364.2997369} for a discussion about how string parser generators and CST--AST synchronisation can be integrated.)
% |Or l r| is desugared in the parsing process, and |If l 1 r| is resugared in the printing process.

From the consistency declaration, a pair of |get| and |put| functions are generated.
As \autoref{fig:resugaring} shows, by providing |put| proper links, we are able to achieve the same effect described by \citeauthor{pombrio2014resugaring}~\cite{pombrio2014resugaring}:
When, internally, |If (If 0 10 0) 1 c| evaluates to |If 0 1 c|, we can observe that (the program text represented by) the CST goes from |Or (And 0 10) c| to |Or 0 c| without losing the syntactic sugar.
% retentiveness helps us keep the logical disjunction in the surface syntax, if .
Compared to \citeauthor{pombrio2014resugaring}'s method, we do not need to enrich the datatypes of the ASTs to incorporate fields for holding tags that mark which syntactic object an AST construct comes from.
There is no need to insert tags into the ASTs during parsing or patch the compiler to be aware of the tags.


\begin{figure}[htb]
% \begin{minipage}{.45\textwidth}
% \centering
% \begin{code}
% OrOp <---> Arith
%   Or l r      ~  If l 1 r
%   ToAndOp t   ~  t

% {-"\\[-4ex]"-}

% CompareOp <---> Arith
%   ...
%   ToAddOp t   ~  t

% MulOp <---> Arith
%   ...
% \end{code}
% \end{minipage}%
% \begin{minipage}{.01\textwidth}
% \phantom{place}
% \end{minipage}%
% \begin{minipage}{.45\textwidth}
% \centering
% \begin{code}
% AndOp <---> Arith
%   And l r         ~  If l r 0
%   ToCmpOp t   ~  t

% {-"\\[-4ex]"-}

% AddOp <---> Arith
%   Add ...
%   Sub ...
%   ToMulOp t ~ t

% ...
% \end{code}
% \end{minipage}
\begin{multicols}{2}
\begin{code}
OrOp <---> Arith
  Or l r     ~  If l 1 r
  ToAndOp t  ~  t

{-"\\[-4ex]"-}

AndOp <---> Arith
  And l r    ~  If l r 0
  ToCmpOp t  ~  t


CompareOp <---> Arith
  ...
  ToAddOp t  ~  t

{-"\\[-4ex]"-}

AddOp <---> Arith
  Sub ...
  ToMulOp t ~ t

\end{code}
\end{multicols}

% {-"\\[-4ex]"-}

% MulOp <---> Arith
%   ...


\caption{Consistency declaration between logical and conditional expressions}
\label{fig:resugaring_dsl}
\end{figure}


\begin{figure}[tbh]
\includegraphics[scale=0.38,trim={4cm 6cm 1.5cm 2.3cm},clip]{pics/resugaring.pdf}
\caption{Resugaring by retentiveness}
\footnotesize{The horizontal links and vertical links are represented by dashed lines and dotted lines respectively. |hl_1| and |hl_2| are produced by |get_l CST|. Let |CST' = put CST AST' hls|, where |hls| can be obtained from |[hl_1, hl_2] linkComp [vl_1] = [hl_1 linkComp vl_1]|. Thus the constructor |Or| connected by |hl_1 linkComp vl_1| is retained after the update. (Many unimportant links are ignored to avoid mess.)}
\label{fig:resugaring}
\end{figure}



\subsection{Code refactoring}
Code refactoring is the restructuring of programs without changing its semantics~\cite{fowler1999refactoring}.
Typical examples of refactoring include renaming all the occurrences of a certain variable and moving a method to a super class.
%
% for example, to find the scope of a variable.
Since it is hard to perform analyses directly on unstructured program text, refactoring usually involves (i)~parsing the program text to a tree representation, (ii)~modifying the tree representation (in a semantics-preserving way), and (iii)~producing a new piece of program text incorporating all the modifications.
Ideally, we want to choose the ASTs of a programming language to be the tree representation, as its compiler already comes with a parser which parses the program text to the AST, thereby saving the cost of defining datatypes and implementing parsers.
%
Despite the benefit, there are inevitable difficulties with performing code refactoring on ASTs.
As ASTs usually do not include information regarding comments, layouts, and syntactic sugar (as we have seen in \autoref{subsec:resugaring}) of their corresponding programs,
how to elegantly retain this information when producing a new piece of program text after code refactoring has become a research problem.
\todo{cite}
%
% Some tools choose to evade the responsibilities for maintaining such information (cite),
% while some other tools choose to avoid doing refactoring on an AST, instead, they do refactoring on a CST or even a token stream,
% which usually contain all the information and are isomorphic to the original program text. (cite)


\begin{figure}[tbh]
\begin{minipage}{.5\textwidth}

The original program:
\vskip -2ex
\begin{code}
swap x y =
  let  v_1 = a && b -- conjunction
       v_2 = c || d -- disjunction
  in   e_b
\end{code}
\end{minipage}%
%
\begin{minipage}{.5\textwidth}
The desired program after refactoring:
\vskip -2ex
\begin{code}
swap x y = e_b
  where
    v_1 = a && b -- conjunction
    v_2 = c || d -- disjunction
\end{code}
\end{minipage}
\caption{The program for code refactoring and the desired result}
\label{fig:refactor_program}
\end{figure}


\begin{figure}[tbh]
\includegraphics[scale=0.38,trim={3.2cm 2.5cm 3.8cm 2.5cm},clip]{pics/refactoring.pdf}
\caption{Retain comments and layouts for code refactoring by retentiveness}
\label{fig:refactoring}
\footnotesize{%
% The horizontal links and vertical links are represented by dashed lines and dotted lines respectively.
For simplicity, we use |e_1| and |e_2| to represent the entire definitions |v_1 = ...| and |v_2 = ...| respectively. In the ASTs, we use |e_1'| instead of |e_1| to mean that |e_1'| does not include comments and syntactic sugars.
Also, although we only draw one link between |e_1| and |e_1'|, in fact all the nodes in |e_1| and |e_1'| are connected. Many unimportant links are omitted.
}
\end{figure}


Here we show that retentiveness makes it possible to perform code refactoring on ASTs and flexibly retain desired comments, layouts, and syntactic sugar on demand.
%
Suppose that there is a function |swap| shown in \autoref{fig:refactor_program}, of which the function body is a $\tit{let}$ expression binding two variables |v_1|~and~|v_2| and returning~|e_b|.
The user wants to make |e_b| directly the body of the function definition and move the variable definitions |v_1| and |v_2| to a $\tit{where}$ block, with the comments for |v_1| and |v_2| retained.
%
In our DSL, this can be achieved as follows:
\begin{enumerate}
  \item Write a consistency declaration between the language's CSTs and ASTs

  \item Perform code refactoring on the AST by moving the variable definitions from the $\tit{let}$ expression to the $\tit{where}$ block and making |e_b| the direct child of |ABody|. We get |AST'|.

  \item Pass the |put| function generated from step~1 the arguments |AST|, modified |AST'|, and links |[hl_1 linkComp vl_1, hl_2 linkComp vl_2]| as illustrated in \autoref{fig:refactoring}. We get |CST'|.
\end{enumerate}
According to retentiveness, the comments, layouts, and syntactic sugar in the variable definitions for |v_1| and |v_2| are all retained in the |CST'|.
The consistency declaration is very similar to \autoref{fig:resugaring_dsl}, and is omitted here.
% \todo{in fact all the links.}
% A refactoring tool can achieve this by first parsing the program text (represented by its CST) to an AST;
% where we see the function name and parameters are merged into a single list in the process.
% on the AST, the refactoring tool moves |e1| and |e2| from |ALet| to |AWhere|, and make |eb| become the direct child of the function body.


%
% Then by giving proper links $l_{vv'}$ showing the provenance of expressions |e1|, |e3|, and |eb| between AST and AST' and compose $l_{vv'}$ with $l_{sv}$, we get diagonal links $l_{sv'}$.
% According to the definition of retentiveness, after $\tit{put}$ we will get a new piece of program text whose |e1|, |e2|, |e3|, and |eb| are directly copied from the original program text,
% and thus all the layouts, comments, syntactic sugar within them are preserved as well.
% In this example, we ignore some unimportant links and we do not expand expressions |e1|, |e2|, |e3| and |eb| into details to show how the comments are stored for avoiding mess in the figure.
% The user may think that comments can be associated with expressions, or terminal symbols, depending on how the parser builds the CST.


\section{Discussion and Related Work}
\subsection{Alignment}
\label{subsec:alignment}
% In BX communities: various approaches entitled alignment have been proposed to enhance retentiveness
\subsubsection{Dawn of BX Languages and Alignment Strategies for Lists}
From the dawn of the bidirectional programming languages~\cite{foster2005combinators}, \emph{alignment} has been recognised as an important problem when dealing with synchronisation of lists --- when the view elements are somehow modified (e.g., inserted, deleted, and reordered), which source elements should be matched with the new view elements and updated correspondingly?
%various \emph{alignment} strategies have been proposed to make the results produced by \emph{put} more satisfactory when there are some structural changes in views.

The earliest lenses only allow source and view elements to be matched positionally --- the $n$-th source element is simply updated using the $n$-th element in the modified view.
Later, lenses with more powerful matching strategies are proposed, such as dictionary lenses \cite{Bohannon:2008:BRL:1328438.1328487} and their successor matching lenses \cite{barbosa2010matching}.
We briefly discuss matching lenses here.
%As the name implies, dictionary lenses match source and view elements based on their (unique) \emph{key} values specified by programmers.
The $\mathit{put}$ transformations offered by matching lenses separate the processes of source--view element matching and element-wise updating.
At the beginning, the source is divided into a ``resource'' and a ``rigid complement'': a resource consists of ``chunks'' of information that can be reordered, and a rigid complement stores information outside the chunk structure.
This reorderable chunk structure is preserved in the view.
After the view is updated, $\mathit{put}$ first uses one of several alignment strategies to compute a correspondence between the locations of chunks in the old and new views.
Based on the correspondence, the resources are ``pre-aligned'' to match the new view chunks positionally, and then element-wise updates are performed.
%For the alignment strategies, matching lenses provides several strategies including greedy align, best align, and \emph{minimal edit distance} with optional non-crossing option, and a threshold.
There are laws (\texttt{PutChunk} and \texttt{PutNoChunk}) dictating that correct pairs of resource and view chunks should be used during the element-wise updates, but these laws look more like an operational semantics for $\mathit{put}$ (as remarked by the authors), and do not declaratively state what source information is retained.
% Similarly, \BiFlux\ \cite{Pacheco:2014:BBF:2643135.2643141} provide the user with align-by-position and align-by-key strategies as two primitive lenses.
%\biflux\

% It supports swap/reorder in the get direction by extending its lenses and redefining the PutChunk, PutNoChunk laws.
% The retentiveness is not (explicitly) discussed. The laws are rather operational and can be read as "the i'th element is produced by ... if it is aligned or not". It does not say what the result looks like (or just say the result looks like put use the basic lens k).

%\paragraph{User-supplied alignment functions}
%\todo{need to read the paper...}
%Some other lenses, for example, \todo{???} adopts a programmable principle which let the user provides a matching function.
%The matching result is then separated into three kingdoms and handled with different user supplied functions: what to do for the pair of matched source and view elements (say, using function $f$), what to do for the source elements not matched, and what to do for the view elements not matched.
%But this align still has the limitations that, (1) suitable only for ``listifiable'' datatypes --- datatypes that can be flattened into a list.
%(2) any source and view elements can only be matched once.
%Assume that an element in the view has been copied several times, there is no way to align all copies with the same source element.
% This align is ``fixed'' as a one-layer-only alignment, in the sense that if the input source and view (of the align function) are tree structures, the align treats two trees as a whole and the user have no way to align their subtrees.

\subsubsection{Alignment for \emph{Containers}}
\label{subsubsec:alignment_containers}
To generalise list alignment, in recent years the BX community turns to a more general notion of data structures called \emph{containers}~\cite{Abbott-containers}.
In the container framework, a data structure is decomposed into a shape and its content; the shape encodes a set of positions, and the content is a mapping from those positions to the elements in the data structure.
For example, the shape of a list is its length, say~$l$, the encoded set of positions is $P = \{0,1,\ldots,l-1\}$, and the content of the list is a mapping which specifies for each $i \in P$ the $i$-th element of the list.
All the approaches to container alignment take advantage of this decomposition and treat shapes and contents separately.
For example, if the shape of a view container changes, \textcite{Hofmann:2012:EL:2103656.2103715}'s approach will update the source shape by a fixed strategy that make insertions or deletions at the rear positions of the (source) containers.
By contrast, \textcite{pacheco2012delta}'s method permits more flexible shape changes and they call it \emph{shape alignment}.
In our setting, both the consistency on data and the consistency on shapes are specified by the same set of consistency declarations.
In the |put| direction, both the data and shape of a new source is determined by (computed from) the data and shape of a view, so there is no need to have separated data and shape alignments.

In addition, we find that the separation of data alignment and shape alignment hinders the handling of some algebraic datatypes.
First, in practice it is usually difficult for the user to define container datatypes and represent their data in terms of containers.
We use the datatypes defined in \autoref{fig:running_example_datatype_def} to illustrate, where two mutually recursive datatypes |Expr| and |Term| are defined.
If the user wants to define |Expr| and |Term| using containers,
one way might be to parametrise the types of terminals (leaves in a tree, here |Integer| only):
\renewcommand{\hscodestyle}{\small}
\begin{minipage}[t]{.5\textwidth}
\begin{code}
data Expr i   =  Plus   (Expr i)  (Term i)
              |  Minus  (Expr i)  (Term i)
              |  Term   (Term i)
data Term i   =  Neg    (Term i)
              |  Lit    i
              |  Paren  (Expr i)
\end{code}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
\begin{code}
data Arith i  =  Add  (Arith i)  (Arith i)
              |  Sub  (Arith i)  (Arith i)
              |  Num i
\end{code}
\end{minipage}
Here the terminals are of the same type |Integer|.
However, imagine the situation where there are more than ten types of leaves, it is rather a misery to parameterise all of them as type variables.

Moreover, the container-based approaches face another serious problem: they always translate a change on data in the view to another change on data in the source, without affecting the shape of a container.
This is actually wrong in some cases, especially when the decomposition into shape and data is inadequate.
%
For example, let the source be |Neg (Lit 100)| and the view be |Sub (Num 0) (Num 100)|.
If we modify the view by changing the integer~$0$ to~$1$ (so the view becomes |Sub (Num 1) (Num 100)|), a container-based approach is not likely to produce a correct source |Minus ...|, as this data change in the view must not result in a shape change in the source.
In general, the essence of container-based approaches is the decomposition into shape and data such that they can be processed independently (at least to some extent), but when it comes to scenarios where such decomposition is unnatural (like the example above), container-based approaches can hardly help.
%Thus we see that the idea of isolating the shape and data does not work for some cases, and hence the container-based approaches.

%
From this aspect, retentiveness and ``put with links as global alignment'' advances the above approaches in the sense that retentiveness enables the user to know what is retained after |put|, while other approaches merely tell the user which basic lens will be applied after the alignment.
As a results, the more complex the lenses is, the more difficult to reason the information retained in the new source for those approaches.

\todo{do we need to cite Jorge's delta align?}
% \todo{quotient lenses: equivalent classes. OK, no time to write}

%\subsection{Least Changes and Least Surprise}
%\todo{consider removing the whole subsections. Not so relevant. Save spaces.}
%
%As we have already seen, usually there could be infinite different modifications to an outdated source for bringing it consistent with a given view again.
%It is reasonable to ask if there exist some algorithm which updates the old source into a consistent state with ``minimum changes'', or bring the user ``least surprise'' according to some objective function.
%
%\subsubsection{Side-effects in databases}
%In database communities, a variant (but very similar) of the problem is well-studied and researchers have shown some conclusive theorems.
%We call it a variant because the problem permits the input view $v$ to be invalid --- not in the range of the query function.
%%
%Later the update function will both update the source and correct the view $v$. % new (valid) view.
%For example, assume |[(a,1) (a,2), (b,1), (b,2)]| in a view are formed by a query joining |[a,b]| and |[1,2]| in a source.
%Then if we delete |[a,1]| only, obviously the new view is not in the range of the query regardless how we modify the source.
%%
%The only solution is to modify both the source and the view simultaneously (or, in their setting, modify the source and regenerate a new view from the modified source), and the additional modifications to the view are regarded as \emph{view side-effect}.
%%
%So the measurement of the view side-effect problem is set to be the number of tuples to be deleted in the view (precisely, excluding the necessary ones) given an update on a source. \cite{Cheney:2009:PDW:1576265.1576266}.
%%
%Similarly, we can define another objective function which cares more about the number of tuples to be deleted in the source and thus is called \emph{source side-effect} problem \cite{Cheney:2009:PDW:1576265.1576266}.
%%
%For this example, given the removal of |(a,1)|, the minimum change on the source can be either to remove |a| or to remove |1|.
%And hence the minimum source side-effect is |1|.
%The removal of |a| or (|1|) from the source will cause another tuple |(a,2)| (or |(b,1)| if |1| is removed from the source) in the view to be deleted as well.
%So the minimum view side-effect is also |1|.
%%
%\todo{Seems handling deletion only?}
%The example is shown by deletions in the view, and in general, side-effect for insertions and replacements are also studied.
%
%For the view side-effect problem and source side-effect problem, they are both NP-hard for queries involving join and either projection or union, and polynomial-time solvable for the remaining queries in the class \cite{Buneman:2002:PDA:543613.543633, Keller:1986:CVU:645913.671458, Cheney:2011:PDA:2139690.2139696}.
%%
%\citeauthor{Dayal:1982:CTU:319732.319740} also introduce the notion of \emph{clean source}: a source is clean if it is not a source of any other tuple (usually a source not involving join operation).
%Clean source enjoys a property that the translation from modifications to a view to the update on it can be side-effect free \cite{Dayal:1982:CTU:319732.319740, Cheney:2011:PDA:2139690.2139696}.
%
%\subsubsection{Side-Effects in BXs}In relational databases, the data is represented as (unordered) tuples, and the operations on the data are usually selection, projection, join, and union.
%However, in the world of BXs, data is trees defined by algebraic datatypes and operations are any
%well-behaved functions,
%which bring difficulties in defining objective functions for calculating ``side-effects'' and producing a least-changed new source.
%As the paper \cite{cheney2015towards} pointed out, different applications indeed require different metric (of changes).
%Most pre-defined metric, for example, the graph edit distance, is not a particularly good match (to calculate matched source and view components) for any user's intuitive idea of distance.
%The authors in \cite{cheney2015towards} are ``pessimistic about whether usably efficient algorithms that guarantee to find optimal solutions to the least change problem will ever be available''.
%Based on this former research, we decided to invent a framework letting the user (or tools) explicitly describing unchanged parts during the transformation.
%Once some parts are connected by links, they are guaranteed to exist in the newly created source.
%
%\todo{Composing Least-change Lenses \cite{macedo2013composing}}
%% Cheney and Jeremy: Using a similar structure, Macedo et al. [MPCO13] address the tricky question of when it is possible to compose bx that satisfy such a least change condition. As might be expected, they have to abandon determinacy (so that the composition can choose “the right path” through the middle model), and impose stringent additional conditions; fundamentally, there is no reason why we would expect bx that satisfy this kind of least change principle to compose.
%
%% Then they discuss whether there exists precise translation for the modifications in the view to the update in the source.
%% A translation is precise, if it modifies source $s$ to $s'$ and $\textit{query}\ s' = v'$.
%% Intuitively, if view $v'$ is invalid under the $\textit{query}$ function, there is no precise translation --- all translations will have side effects on $v'$ and make it become $v''$ after query on the new source.


\subsection{Connections Between the Source and the View}
\label{subsec:provenances}
In \autoref{subsec:two_links}, we have mentioned that our idea of horizontal links and vertical links are inspired by the notion of (various kinds of) \emph{provenance} in the database community.
%
In the paper by \textcite{Cheney:2009:PDW:1576265.1576266}, the authors classify provenance into three kinds: \emph{why}, \emph{how}, and \emph{where}.
\emph{Why-provenance} is the first formalised provenance and was called \emph{lineage} at the time \cite{Cui:2000:TLV:357775.357777};
it is the information about which data in the view is from which rows in the source.
%
However, knowing only the rows where a piece of data in the view comes from is not informative enough for many applications.
Consequently, researchers propose two refinements of why-provenance:
\emph{how-provenance}, which additionally records the times of the usage of a row (in the source),
and \emph{where-provenance}, which tells the user not only the row but also the column (that is, a cell) where a piece of data is \emph{copied}.
% (in relational databases, a precise location can be represented by its row and column numbers.)
In our setting, we require two pieces of data linked by vertical links to be equal (under a certain pattern).
Hence the vertical links resemble the where-provenance.


If we leap from relational databases to programming languages, we will find that these kinds of provenance are not powerful enough, as they are restricted to a particular representation of data, namely rows of tuples.
%
In functional programming, data can be defined by algebraic datatypes, which can be sums, products, and even functions etc, and views may be produced by a wide range of mechanisms.
%
For this need, \emph{dependency provenance} \cite{Cheney:2011:PDA:2139690.2139696} and \emph{expression provenance} \cite{Acar2012} are proposed:
the former tells the user on which parts of a source the computation of a part of a view depends, and the latter can even record a tree tracking how a part of a view is computed by some primitive operations \cite{Acar2012}.
%(Of course they design a language for the annotation-propagation framework.)
%
In this sense, the definition of our horizontal links is like the dependency provenance.
However, our DSL is more simplified, and the links cannot express data dependencies for general functions.

The idea of inferring consistency links (horizontal links generated by |get|) can also be found in work on origin tracking for term rewriting systems~\cite{vanDeursen:1993:OT:162204.162214}, where the origin relations between the rewritten terms are obtained by a static analysis of the rewrite rules.
%\todo{quickly added the reference regarding origin tracking, to avoid the blame from reviewers.}


\subsection{View-Update Translator}
\subsubsection{In the Database Community}
In the database community, there are notions of \emph{updatable views}, and much research has focused on transforming an update on the view to an update on the source.
%
For example, \textcite{Bancilhon1981} introduce the notion of \emph{constant complement} in relational databases:
Given a database~$s$, a query~$f$ and a view $v$ queried by $f$, all ``the information not visible within $v$'' \cite{Bancilhon1981} must reside in the $v$'s complement created by an $f$'s complement function $g$, which should make the function |(f {-"{\triangle}\;"-} g) s = (f s, g s)| an injection.
That is, the original database is decomposed into a view and a complement, the latter of which is kept constant; when the view is updated, it is paired with the constant complement and fed into $(f \mathop\triangle g)^{-1}$ to compute an updated database.
The concept is useful and leads to some applications, such as \emph{bidirectionalisation} (of a query function) in the BX community~\cite{Matsuda:2007:BTB:1291151.1291162}.
%
However, there are also inadequacies that make the approach not very practical.
For instance, the time complexity for finding a minimum complement is NP-complete \cite{Cosmadakis:1984:URV:1634.1887};
also, finding a complement that might be ``arbitrarily large'' is not that useful, because the data (in the source) captured by the complement function cannot be changed after an update (hence the name constant complement).
In extreme cases, if the complement is set to be as large as the whole source, then nothing could be updated.
%
% Second, since in (traditional) relational databases the data is only tuples, the method deals with product types only but cannot handle handle sum types.
% Because for sum types there is even no constant complement in general. (See \autoref{sec:putput}.)


\subsubsection{In the BX Community}
The BX community has also considered translating view updates to source updates to achieve bidirectionality.
Among them, \textcite{Diskin2011Asym}'s delta-based BX model and \textcite{Hofmann:2012:EL:2103656.2103715}'s Edit Lenses are more relevant to our work.
%
The delta-based BX model regards the differences between the view state $v$ and $v'$ as \emph{deltas} and the differences are abstractly represented by arrows (from the old view to the new view).
The main equational law in the framework is:
Given a source state $s$ and a view delta $\tit{det}_v$ between $v$ and $v'$, $\tit{det}_v$ should be translated to a source delta $\tit{det}_s$ between $s$ and $s'$ satisfying |get s' = v'|.
%
As the law only guarantees the existence of a source delta $\tit{det}_s$ that updates the old source to a correct state,
it is not sufficient for deriving retentiveness in their model
%
because, given a translated delta $\tit{det}_s$, there are still an infinite number of interpretations for generating a correct source $s'$, with only few of them being ``retentive''.
%
To illustrate, \citeauthor{Diskin2011Asym} tend to represent deltas as edit operations such as \emph{create}, \emph{delete}, and \emph{change}, aiming to transform edits on the view to edits on the source.
Representing deltas in this way can only tell the user in the new source what must be changed, while there is additional work to do for reasoning what must be retained.
However, as there are some similarities between the delta-based and retentive frameworks, by carefully designing and imposing proper properties on the representations of deltas, it might be possible for delta-based BXs to exhibit retentiveness.
%
Compared to \citeauthor{Diskin2011Asym}'s work, \citeauthor{Hofmann:2012:EL:2103656.2103715} give concrete definitions and implementations for Edit Lenses propagating edit sequences.
Other discussions for Edit Lenses can be found in \autoref{subsubsec:alignment_containers} (regarding container alignment).

% The emphases of their paper is the theoretical framework.
% As a result, no concrete examples are given to show the feasibility, no notion of equivalence, and no combinators for constructing their \emph{sd-lens}, as discussed in \cite{Hofmann:2012:EL:2103656.2103715}, either.


% Clearly, \cite{Diskin2011Asym} defines laws similar to correctness and hippocraticness in the delta framework which is also able to theoretically absorb retentiveness by representing deltas as correspondence links.



% Since there is no concrete examples, we do not see (and the authors do not tell us) the benefit to view deltas as arrows and data as objects in categories.
% note that in practical situations the source in  put s v  can be out of the domain of get s and the put must also produce a correct new source.




\section{Discussions}
in the discussion part, we can mention\\
- the grammar is tailored for some existing languages, such as BiYacc\\


More to check:\\
- third honomorphism on trees, and context bx\\
- get producing links => get with context (holes) => view diffing\\
\\
future work: \\
- ordering for imaginary links.

% end discussions

\section{Acknowledgements}
Meng Wang for PutPut laws, incremental update (Meng, bx), scenarios on refacotring and more 
Picture drawing.


\newpage

% changing fontsize of the biblio might be illegal
\renewcommand*{\bibfont}{\small}
\printbibliography


\appendix


\section{Proof supplements}
\subsection{Proof of the case not handling links}
\label{proof_correctness_no_link}
Proof of |Hippocraticness|
\begin{code}
      get_v (put_s st vt os env (Q ys)) = Q ys
\end{code}
\begin{code}
    get_v (put_s st vt os env (Q ys))

=       { expand the definition {-"\tit{of}\ "-} put }
    get_v (mkInj_s st (ctypeOf (P zs)) (P zs, _))

=       { by {-"\hyperref[prereq:getInj]{Prerequisite 1} "-}}
    get_v (P zs)

=       { zs is the results {-"\tit{of}\ "-} the put on subtrees }
    get_v (P [... put_s (ctypeOf x_i) (ctypeOf y_i) os env_i y_i ...])

=       {  According to the source disjointness, the get generated from the
         same consistency relations (P xs ~ Q ys) must be chosen}
    Q [... get_v (put_s (ctypeOf x_i) (ctypeOf y_i) os env_i y_i) ... ]

=       {the height {-"\tit{of}\ "-} y_i is less than h and we can use inductions hypotheses}
    Q [... y_i ...]

=   Q ys
\end{code}


\subsection{Proof of |vls' linkComp get_l (P zs) = env|}
\label{proof:retentive_no_link}
\begin{code}
Assume

vls'                 = concat [... vls_i' ...]
vls_i'               = map (addStepV ([],prefX_i)) vls_i
vls_i                = put_l (ctypeOf x_i) (ctypeOf y_i) os env_i y_i
env_i                = map (delPathH ([],prefY_i)) (queryEnvBy prefY_i env)

get_l (P zs)         = mkLinkt (P xs) (Q ys) : concat [... ls_i' ...]
ls_i'                = map (addStepH (prefX_i, prefY_i)) ls_i
ls_i                 = get_l z_i

vls_i linkComp ls_i  = env_i




    vls' linkComp get_l (P zs)
=   concat [... vls_i' ...] linkComp (mkLinkt (P xs) (Q ys) : concat [... ls_i' ...])


= {distribute composition over concatenation}
    concat [... vls_i' ...]  linkComp [mkLinkt (P xs) (Q ys)] ++
    concat [... vls_i' ...]  linkComp concat [... ls_i' ...]

= {  mkLinkt (P xs) (Q ys) cannot be composed with any {-"\tit{of}\ "-} [... vls_i' ...]
     because it does {-"\tit{not}\ "-} have a prefix path}
    concat [... vls_i' ...]  linkComp concat [... ls_i' ...]

=   concat [... map (addStepV ([],prefX_i)) vls_i ...] linkComp
    concat [... map (addStepH (prefX_i, prefY_i)) ls_i ...]

= {  we see that the prefix path {-"\tit{of}\ "-} any two vls_i' are disjoint and
     the prefix path {-"\tit{of}\ "-} any two ls_i' are disjoint{-"."-}
     only vls_i' and ls_i' are composable}
    concat [...  vls_i' linkComp ls_i' ...]

= { according to {-" \ \autoref{lemma:compBridge} "-} }
    concat [ ...  map (addStepH ([], prefY_i)) (vls_i .  ls_i)]

= {  vls_i and ls_i are generated from trees {-"\tit{of}\ "-} height less than h,
     by induction hypotheses}
    concat [ ... map (addStepH ([], prefY_i)) env_i ...]

=   concat [ ... map  (addStepH ([], prefY_i))
                      (map (delPathH ([],prefY_i)) (queryEnvBy prefY_i env)) ...]

= {map fusion}
    concat [ ... map  (addStepH ([], prefY_i) . delPathH ([],prefY_i))
                      (queryEnvBy prefY_n env) ...]

=   concat [... queryEnvBy prefY_i env ...]

=   env
\end{code}


% \subsection{Proof of |foldr| over |imags| creating necessary links}
% \label{proof_imaginary_links}
% \begin{itemize}
%   \item \textbf{Base case.}
%   In the base case, $\tit{imags}$ is an empty list.
%   We have $s' = s$ and $\tit{vls}' = \tit{vls}$.
%   Hence |hls' = get_l s' = get_l s = hls|.
%   \begin{code}
%      (vls' linkComp hls') exclude (vls linkComp hls)
%   =  (vls linkComp hls) exclude (vls linkComp hls) 
%   =  []
%   =  imags
%   \end{code}

%   \item \textbf{Inductive case.}
%   Suppose the property holds for the input list $\tit{imags}$. Let
%   \begin{code}
%   (s_1, vls_1)  = foldr (splice  os) (s, vls) imags
%   imags         = (vls_1 linkComp hls_1) exclude (vls linkComp hls)
%   hls           = get_l s
%   hls_1         = get_l s_1
%   \end{code}
%   %
%   Consider the property for input list $\tit{imag} : \tit{imags}$.
%   \begin{code}
%       foldr (splice os) (s, vls) (imag : imags)
%   =   splice  os  imag (foldr (splice os) (s, vls) imags)
%   =   splice  os  imag (s_1, vls_1)
%   =   (s_2, imgv_0 : map (addStepV ([],prefixS)) vls_1)

%   get_l s_1  = hls_1
%   get_l s_2  = imgh_0 : map (addStepH (prefixS,[])) hls_1
%   \end{code}
%   %
%   It is easy to derive:
%   \begin{code}
%     (  (imgv_0 : map (addStepV ([],prefixS))  vls_1) linkComp
%        (imgh_0 : map (addStepH (prefixS,[]))  hls_1 )) exclude (vls linkComp hls)
% = { {-"let \ "-}  vls_1' = map (addStepV ([],prefixS)) vls_1
%                   hls_1' = map (addStepH (prefixS,[])) hls_1}
%     ((imgv_0 : vls_1' ) linkComp (imgh_0 : hls_1')) exclude (vls linkComp hls)
% = { distribute the links' composition over list concatenation }
%     (  ([imgv_0] linkComp [imgh_0]) ++ ([imgv_0] linkComp [hls_1']) ++
%        (vls_1' linkComp [imgh_0]) ++ (vls_1' linkComp hls_1')) exclude (vls linkComp hls)
% = { by I: imgv_0 linkComp imgh_0 = imag, imgv_0 linkComp hls_1' = [], vls_1' linkComp imgh_0= [] }
%     ([imag] ++ [] ++ [] ++ (vls_1' linkComp hls_1')) exclude (vls linkComp hls)
% =   ([imag] ++ (vls_1' linkComp hls_1')) exclude (vls linkComp hls)
% = { distribute difference over concatenation}
%     ([imag] exclude (vls linkComp hls)) ++ ((vls_1' linkComp hls_1') exclude (vls linkComp hls))
% = { by II: [imag] exclude (vls linkComp hls) = [imag] }
%     [imag] ++ ((vls_1' linkComp hls_1') exclude (vls linkComp hls))
% = { by {-" \ \autoref{lemma:compBridge}"-} : vls_1' linkComp hls_1' = vls_1 linkComp hls_1 }
%     [imag] ++ ((vls_1 linkComp hls_1) exclude (vls linkComp hls))
% = { by induction hypotheses}
%     [imag] ++ imags
% =   imag : imags
%   \end{code}

%   I: Go through the definition of \tit{splice}, we can directly get $\tit{prjv}_0 \circ \tit{prjh}_0 = ((\tit{sPath},\tit{sPat}) , ((),[])) = \tit{prjl}$.
%   For $\tit{prjv}_0$ and $\tit{hls}'_1$, their paths used for the composition are disjoint:
%   the path of $\tit{hls}'_1$ has at least $\tit{prefixS}$, while the path of $\tit{prjv}_0$ is $[]$.
%   So the result of the composition is empty.
%   (We can draw an analogy with the situation in relational database, where the keys and foreign keys used for join operation are disjoint.)
%   The reason is the same for the composition between $\tit{prjh}_0$ and $\tit{vls}'_1$.

%   II: We can show $\{\tit{prjl}\} \diff (\tit{vls} \circ \tit{hls}) = \{\tit{prjl}\}$ by a simple induction.
%   \begin{itemize}
%     \item For the base case, which is in the first step of the $\tit{foldr}$, $\tit{hls}$ can actually be divided into two parts:
%     the horizontal links come from the recursive call (on subtrees), which are below the current focus of the view and whose paths in the view are always not empty;
%     and the horizontal link $\tit{hl}_0$ newly created.
%     So given whatever vertical link $\tit{vl}_i$, either the view path of the links $\{\tit{vl}_i\} \circ \tit{hls}$ are non-empty (the first case) --- which is not the same as $\tit{prjl}$;
%     nor the pattern is not a unit pattern (the second case) --- which is not the same as $\tit{prjl}$.
%     (Note that the view path and pattern of $\tit{prjl}$ are from $\tit{prjh}_0$.)

%     \item For the inductive case, $\tit{vls}$ and $\tit{hls}$ in addition contains some ``imaginary links'' built from previous recursive steps.
%     By the definition of $\tit{splice}$, the data used to form ``imaginary links'' ($\tit{vls}$ and $\tit{hls}$) come from the input $\tit{prjls}$.
%     However, the input $\tit{prjls}$ are already sorted according to their $\tit{sPath}$ value.
%     Hence all of the source paths (in the left part of the tuple) of $\tit{vls}$ built from $\tit{prjls} \diff {\tit{prjl}}$ are not equal to the source path of $\tit{prjl}$;
%     all of the source paths (in the left part of the tuple) of $\tit{vls} \circ \tit{hls}$ are not equal to the source path of $\tit{prjl}$;
%     then we have $\tit{prjl} \diff \tit{vls} \circ \tit{hls} = \tit{prjl}$.
%   \end{itemize}

% \end{itemize}


% \section{\texttt{PutPut} and constant complement}
% \label{sec:putput}
% It might be surprising that there is a very similar law, $\texttt{PutPut}$ in BX communities with a completely different appearance. PutPut says that $\textit{put} (\textit{put} \ s \ v_1) v_2 = \textit{put} \ s \ v_2$.
% The communities like to interpret the law as ``The put function is history-irrelevant''.
% A naive examination is that given $v$ consistent with $s$ and a modified $v'$, we set $v1 = v'$  and $v2 = v$, then PutPut states that $\textit{put}\ (\textit{put}\ s\ v_1)\ v = \textit{put}\ s\ v$ = $s$.
% Obviously, if put touch some other parts not in the view, the result is hardly $s$.

% (Also, we can say that only very well-behaved bx is g-translatable.)

\section{Proof of the property of insSubtree}
\label{sec:proof_insSubtree}

\begin{code}
    insSubtree (P x) s_1 (s_0, vls_0) = (P s_2, vls_3)

    get_v (P s_2)

= {source disjointness }
    Q (get_v s_2)

= {P x is the source pattern {-"\tit{of}\ "-} an imaginary link, so Q {-"\tit{in}\ "-} fact does not exist}
    get_v s_2

= {expand s_2}
    get_v (mkInj_s (ctypeOf (x ;P x)) (typeOf s_0) (s_0,vls_0))

= {by {-"\ \hyperref[prereq:getInj]{\tit{Prerequisite}\ 1}"-}}
    get_v s_0
\end{code}



\begin{code}
    vls_3 linkComp (get_l (P s_2))

= {expand vls_3}
    map (addStepV ([], (getPrefix (x ; P x))) ) vls_2 linkComp (get_l (P s_2))

= {by the definition of get, suppose get P = Q}
    map (addStepV ([], (getPrefix (x ; P x))) ) vls_2 linkComp (mkLink (P ; Q) : concat [ls_0'])

= {distribute composition over concatenation, concat [ls_0'] = ls_0'}
    (map (addStepV ([], (getPrefix (x ; P x))) ) vls_2 linkComp [mkLink (P ; Q)]) ++
    (map (addStepV ([], (getPrefix (x ; P x))) ) vls_2 linkComp ls_0')

= {mkLink (P ; Q) is a link at top and has no prefix path}
    map (addStepV ([], (getPrefix (x ; P x))) ) vls_2 linkComp ls_0'

= {expand vls_2}
    map  (addStepV ([], (getPrefix (x ; P x))))
         (mkInj_l (ctypeOf (x ; P x)) (typeOf s_0) (s_0,vls_0)) linkComp ls_0'

= {by the definition of get}
    map  (addStepV ([], (getPrefix (x ; P x))))
         (mkInj_l (ctypeOf (x ; P x)) (typeOf s_0) (s_0,vls_0)) linkComp
    map (addStepH (getPrefix (x ; P x) , getPrefix (y ; Q y))) (get_l s_2)

= {P x is the source pattern of an imaginary link, so getPrefix (y ; Q y) is empty}
    map  (addStepV ([], (getPrefix (x ; P x))))
         (mkInj_l (ctypeOf (x ; P x)) (typeOf s_0) (s_0,vls_0)) linkComp
    map (addStepH (getPrefix (x ; P x) , [])) (get_l s_2)

= {by {-" \ \autoref{lemma:compBridge} "-}}
    mkInj_l (ctypeOf (x ; P x)) (typeOf s_0) (s_0,vls_0) linkComp (get_l s_2)

= {expand s_2}
    mkInj_l (ctypeOf (x ; P x)) (typeOf s_0) (s_0,vls_0) linkComp
    get_l (mkInj_s (ctypeOf (x ; P x)) (typeOf s_0) (s_0,vls_0))

= {by {-" \ \hyperref[prereq:getInj]{\tit{Prerequisite}\ 1} "-}}
    vls_0 linkComp s_0

\end{code}


%%%%%%%%%%%%%%%%
\section{}
\label{sec:proof_splice}
Let |(s_2, vl_0 : vls_2) = splice os imag (s_0, vls_0)|, we have:
\begin{code}
get_v s_2                          =  get_v s_0

(vl_0 : vls_2) linkComp get_l s_2  =  imag : (vls_0 linkComp get_l s_0)
\end{code}


Proof of the first equation:

\begin{code}
    get_v s_2

=   get_v (insSubtree_s sPat s_1 s_2)

= {by {-"\ \autoref{lemma:getRepSub}"-} }
    get_v s_2

=   get_v (mkInj_s (typeOf s_1) (typeOf s_0)  (s_0, vls_0))

= {by {-"\ \hyperref[prereq:getInj]{\tit{Prerequisite}\ 1}"-} }
    get_v s_0
\end{code}


Proof of the second equation:
\begin{code}
    (vl_0 : vls_2) linkComp get_l s_2

=   (vl_0 : vls_2) linkComp get_l (insSubtree_s sPat s_1 (s_0,vls_0))

= {distribute composition over concatenation}
    ([vl_0]  linkComp get_l (insSubtree_s sPat s_1 (s_0,vls_0))) ++
    (vls_2   linkComp get_l (insSubtree_s sPat s_1 (s_0,vls_0)))

= {expand vls_2}
    ([vl_0]  linkComp get_l (insSubtree_s sPat s_1 (s_0,vls_0))) ++
    (  (insSubtree_l sPat s_1 (s_0,vls_0))  linkComp
       get_l (insSubtree_s sPat s_1 (s_0,vls_0)))

= {by {-"\ \autoref{lemma:insSubtree}"-}}
    ([vl_0]  linkComp get_l (insSubtree_s sPat s_1 (s_0,vls_0))) ++
    (vls_0 linkComp get_l s_0)

= {by the equation below}
    [((sPat,sPath),((),[]))] ++ vls_0 linkComp get_l s_0

=   imag : (vls_0 linkComp get_l s_0)
\end{code}

\begin{code}
    [vl_0]  linkComp get_l (insSubtree_s sPat s_1 (s_0,vls_0))
= {expand vl_0}
    [(sPath,sPat,[])] linkComp get_l (insSubtree_s sPat s_1 (s_0,vls_0))

= {by the definition of insSubtree, suppose sPat is (P x)}
    [(sPath,sPat,[])] linkComp get_l (P s_2)

= {by the definition of get}
    [(sPath,P x,[])] linkComp
    (mkLink (P x; Q y) : concat [map (addStepH (prefX_i, prefY_i) (get_l s_2))])

= {concat [x] = x}
    [(sPath,P x,[])] linkComp
    (mkLink (P x; Q y) : (map (addStepH (prefX_i, prefY_i) (get_l s_2))))

= {by the definition of mkLink}
    [(sPath,P x,[])] linkComp
    (((mkPat (P x), []) (mkPat (Q y), [])) : (map (addStepH (prefX_i, prefY_i) (get_l s_2))))

= {P x is the source pattern of an imaginary link, so mkPat (Q y) is ()}
    [(sPath,P x,[])] linkComp
    (((P, []) ((),[])) : (map (addStepH (prefX_i, prefY_i) (get_l s_2))))

= {distribute composition over concatenation}
    ([(sPath,P x,[])] linkComp [((P x, []) ((),[]))]) ++
    ([(sPath,P x,[])] linkComp (map (addStepH (prefX_i, prefY_i) (get_l s_2))))

= {prefX_i = getPrefix[x; P x] is not empty and thus not composable with [(sPath,P x,[])]}
    [((P x,sPath) , ((),[]))] ++ []

= {P x = sPat}
    [((sPat, sPath) , ((),[]))]

\end{code}
%


\section{Proof of the property of the code handling real link}
\label{sec:proof_real_link}
\begin{code}
  get_v s_2 = v

  vls_2 linkComp (get_l s_2) = l : env'
\end{code}

Proof of the first equation |get_v s_2 = v|.

\begin{itemize}
  \item   If there is a real link,
\begin{code}
    get_v s_2

=   get_v (repSubtree sPat s_0 s_1)

= { by {-"\ \autoref{lemma:getRepSub}"-} }
    get_v s_1

=   get_v (putRec_s (typeOf s_0) vt sPat os env' v)

= { no link at top {-"\mathit{of}\ "-} the view, use the result {-"\mathit{of}\ "-} Case 1}
    v
\end{code}
  \item   If there is no real link,
\begin{code}
    get_v s_2

=   get_v (putRec_s st vt (selSPat st vt v) os env' v)

= { no link at top {-"\mathit{of}\ "-} the view, use the result {-"\mathit{of}\ "-} Case 1}
    v
\end{code}
\end{itemize}



Proof of the second equation |(vls_2 linkComp get_l s_2) = l : env'|

\begin{itemize}
  \item   If there is a real link,
\begin{code}
    vls_2 linkComp get_l s_2
=   (vl_0 : vls) linkComp get_l (repSubtree sPat s_0 s_1)

= { by {-"\ \autoref{lemma:getRepSub}"-} }
    (vl_0 : vls) linkComp get_l s_1

=   (vl_0 : vls) linkComp (get_l (putRec_s (typeOf s_0) vt sPat os env' v))

= { distribute composition over concatenation}
    [vl_0]  linkComp (get_l (putRec_s (typeOf s_0) vt sPat os env' v)) ++
    vls     linkComp (get_l (putRec_s (typeOf s_0) vt sPat os env' v))

= { no link at top {-"\mathit{of}\ "-} the view, use the result {-"\mathit{of}\ "-} Case 1}
    [vl_0]  linkComp (get_l (putRec_s (typeOf s_0) vt sPat os env' v)) ++ env'

=   ([(sPath,sPat,[])] linkComp (get_l (putRec_s (typeOf s_0) vt sPat os env' v))) ++ env'

= { use the equation below}
    [(sPat,sPath), (mkPat (Q ys),[])] ++ env'

= { v matches pattern (mkPat (Q ys)) because v = Q (VEC ys)}
    l : env'
\end{code}

  Assume |putRec (typeOf s_0) vt sPat os v env'| match the code generated from |P (VEC x) ~ Q (VEC y)|.
\begin{code}
    [(sPath,sPat,[])] linkComp get_l (putRec_s (typeOf s_0) vt sPat os env' (Q ys))

= {by the definition of get}
    [(sPath,sPat,[])] linkComp (l_0 : concat [ ... ls_i' ...])

= {expand l_0 and ls_i'}
    [(sPath,sPat,[])] linkComp
    (mkLink (P (VEC x) ; Q (VEC y)) : concat [... map (addStepH (prefX_i, prefY_i)) (get_l x_i) ...])

= {expand mkLink ,}
    [(sPath,sPat,[])] linkComp
    (   ((mkPat (P (VEC x)),[]) , (mkPat (Q (VEC y)), [])) :
        concat [... map (addStepH (prefX_i, prefY_i)) (get_l x_i) ...])

= {P (VEC x) is {-"\mathit{of}\ "-} pattern sPat}
    [(sPath,sPat,[])] linkComp
    (  ((sPat,[]),(mkPat (Q ys), [])) :
       concat [... map (addStepH (prefX_0, prefY_0)) (get_l x_i) ...])

= {distribute composition over concatenation}
    ([(sPath,sPat,[])] linkComp [((sPat,[]),(mkPat (Q ys), []))]) ++
    ([(sPath,sPat,[])] linkComp concat [... map (addStepH (prefX_0, prefY_0)) (get_l x_i) ...])

= { links with different prefix are {-"\mathit{not}\ "-} composable}
    [(sPath,sPat,[])] linkComp [((sPat,[]),(mkPat (Q ys), []))]

= {definition of composition}
    [(sPat,sPath), (mkPat (Q ys),[])]

\end{code}

  \item  If there is no real link.
\begin{code}
    vls_2 linkComp get_l s_2
=   (putRec_l st vt (selSPat st vt v) os env' v linkComp
     get_l (putRec_s st vt (selSPat st vt v) os env' v))

= { no link at top {-"\mathit{of}\ "-} the view, use the result {-"\mathit{of}\ "-} Case 1}
    env'
\end{code}

\end{itemize}


\end{document}
