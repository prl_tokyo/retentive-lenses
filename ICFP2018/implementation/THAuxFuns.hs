-- auxiliary functions for template haskell

{-# LANGUAGE ViewPatterns, LambdaCase #-}

module THAuxFuns where

import qualified Def as D
import Language.Haskell.TH
import Language.Haskell.TH.Syntax
import Language.Haskell.TH.Quote

import Language.Haskell.Meta

import Data.Char (toUpper)
import Data.Generics
import Data.Data
import Data.Typeable
import Control.Monad.State

import Debug.Trace

typeOfStr :: Typeable a => a -> String
typeOfStr = show . typeOf

-- write our own parseType' function later
parseType' :: String -> Type
parseType' = (\case
  Left  err -> error err
  Right ty -> ty) . parseType

-- write our own parseExp' function later
parseExp' :: String -> Exp
parseExp' = (\case
  Left  err -> error err
  Right ty -> ty) . parseExp


var2E :: String -> Exp
var2E = VarE . mkName

var2P :: String -> Pat
var2P = VarP . mkName

var2T :: String -> Type
var2T = VarT . mkName

con2E :: String -> Exp
con2E = ConE . mkName

con2P :: String -> Pat
con2P s = ConP (mkName s) []

con2T :: String -> Type
con2T = ConT . mkName


simpleValD :: Pat -> Exp -> Dec
simpleValD p e = ValD p (NormalB e) []

simpleInfixE :: Exp -> Exp -> Exp -> Exp
simpleInfixE l op r = InfixE (Just l) op (Just r)

simpleClause :: [Pat] -> Exp -> Clause
simpleClause ps e = Clause ps (NormalB e) []

simpleDataD :: String -> [Con] -> Cxt -> Dec
simpleDataD s cons derives = DataD [] (mkName s) [] Nothing cons derives

infixr 5 `mkArrTy`
mkArrTy :: Type -> Type -> Type
mkArrTy f x = (ArrowT `AppT` f) `AppT` x

infixr 5 `mkArrTyS`
mkArrTyS :: String -> Type -> Type
mkArrTyS f x = (ArrowT `AppT` (var2T f)) `AppT` x



-- add a postfix to variable names. this can be used to generate fresh name.
addPosToVar :: String -> D.Pattern -> D.Pattern
addPosToVar pfix (D.ConP c ty ps) = D.ConP c ty (map (addPosToVar pfix) ps)
addPosToVar pfix (D.VarP v ty)  = D.VarP (v ++ pfix) ty
addPosToVar pfix l@(D.LitP  _)  = l
addPosToVar pfix w@(D.WildP _)  = w

-- add a prefix to variable names. this can be used to generate fresh name.
addPreToVar :: String -> D.Pattern -> D.Pattern
addPreToVar pfix (D.ConP c ty ps) = D.ConP c ty (map (addPreToVar pfix) ps)
addPreToVar pfix (D.VarP v ty)  = D.VarP (pfix ++ v) ty
addPreToVar pfix l@(D.LitP  _)    = l
addPreToVar pfix w@(D.WildP _)    = w

-- extract variables in a pattern
extractVars :: D.Pattern -> [String]
extractVars (D.ConP c ty ps) = concatMap extractVars ps
extractVars (D.VarP v ty)  = [v]
extractVars (D.LitP  _)  = []
extractVars (D.WildP _)  = []


renameWildP :: D.Pattern -> State Int D.Pattern
renameWildP (D.ConP c ty ps) = do
  ps' <- mapM renameWildP ps
  return $ D.ConP c ty ps'
renameWildP v@(D.VarP  _ _) = return v
renameWildP l@(D.LitP  _)   = return l
renameWildP (D.WildP ty)    = do
  i <- get
  put (i+1)
  return $ D.VarP ("fromWild" ++ show i) ty

---------------

----------------------

-- translate patterns in our DSL into Haskell's Pat
dslPat2HsPat :: D.Pattern -> Pat
dslPat2HsPat (D.ConP c _ ps) = ConP (mkName c) (map dslPat2HsPat ps)
dslPat2HsPat (D.VarP  v _)   = VarP (mkName v)
dslPat2HsPat (D.LitP  l)     = LitP (toHsLit l)
dslPat2HsPat (D.WildP _)     = WildP

-- translate patterns in our DSL into Haskell's Exp
dslPat2HsExp :: D.Pattern -> Exp
dslPat2HsExp (D.ConP c _ ps) = foldl1 AppE ((ConE . mkName $ c):(map dslPat2HsExp ps))
dslPat2HsExp (D.VarP v _)    = VarE (mkName v)
dslPat2HsExp (D.LitP l)      = LitE (toHsLit l)
dslPat2HsExp (D.WildP ty)    = mkDefVal ty


-- mkDefVal :: D.DataTypeRep -> Exp
-- mkDefVal a = -- trace (show $ mkDefVal' (D.printTypeRep a)) $
--   mkDefVal' (D.printTypeRep a)

mkDefVal :: D.DataTypeRep -> Exp
mkDefVal (D.TyCon "Integer"[]) = LitE (IntegerL 0)
mkDefVal (D.TyCon "Int"    []) = LitE (IntegerL 0)
mkDefVal (D.TyCon "String" []) = LitE (StringL "X")
mkDefVal (D.TyCon "Char"   []) = LitE (CharL 'X')
mkDefVal (D.TyCon "Bool"   []) = ConE (mkName "False")
mkDefVal (D.TyCon "Double" []) = LitE (RationalL 0)
mkDefVal (D.TyCon c pars) = ConE (mkName (c ++ "Null"))
mkDefVal (D.TyVar v)      = error "Impossible. In mkDefVal."



-- mkDefVal' :: String -> Exp
-- mkDefVal' "Integer" = LitE (IntegerL 0)
-- mkDefVal' "Int"     = LitE (IntegerL 0)
-- mkDefVal' "String"  = LitE (StringL "X")
-- mkDefVal' "Char"    = LitE (CharL 'X')
-- mkDefVal' "Bool"    = ConE (mkName "False")
-- mkDefVal' "Double"  = LitE (RationalL 0)
-- mkDefVal' other     = ConE (mkName (other ++ "Null"))


mkDefVal2 :: String -> String
mkDefVal2 "Integer" = "0"
mkDefVal2 "Int"     = "0"
mkDefVal2 "String"  = "\"X\""
mkDefVal2 "Char"    = "'X'"
mkDefVal2 "Bool"    = "\"False\""
mkDefVal2 "Double"  = "0"
mkDefVal2 other     = error "error in mkDefVal2"



toHsLit :: D.Literal -> Lit
toHsLit (D.LitInt  i) = IntegerL i
toHsLit (D.LitChar   c) = CharL c
toHsLit (D.LitString s) = StringL s
-- toHsLit (D.LitFloat d) = RationalL d
-- toHsLit (D.LitBool   True) = ConE (mkName "True")
-- toHsLit (D.LitBool   False) = ConE (mkName "False")


-- convert some haskell pattern to a constructor name (string) without space
-- this function is PARTIAL
hsPat2ConName :: Pat -> String
hsPat2ConName (ConP n ps) = concat (capitaliseFst (nameBase n) : (map hsPat2ConName ps))
hsPat2ConName (ParensP pat) = hsPat2ConName pat
hsPat2ConName (VarP v) = capitaliseFst (nameBase v)
hsPat2ConName (LitP l) = "Lit" ++ show l
hsPat2ConName (TupP ps) = "Tup" ++ concatMap hsPat2ConName ps
hsPat2ConName (InfixP p1 op p2) = infixName op ++ hsPat2ConName p1 ++ hsPat2ConName p2
hsPat2ConName (ListP []) = "EmptyL"
hsPat2ConName (ListP ps) = "List" ++ concatMap hsPat2ConName ps
-- hsPat2ConName WildP = error "cannot be wildcard pattern. In hsPat2ConName."
hsPat2ConName WildP = "WP"

-- give operator a valid name
infixName :: Name -> String
infixName (nameBase -> ":") = "Cons"
infixName (nameBase -> "+") = "Plus"
infixName (nameBase -> "-") = "Minus"
infixName (nameBase -> "*") = "Mul"
infixName (nameBase -> "/") = "Div"

-- convert a DataTypeRep in our DSL to Haskell Exp
tyRep2HsExp :: D.DataTypeRep -> Exp
tyRep2HsExp (D.TyCon s fields) = foldl1 AppE (var2E s : map tyRep2HsExp fields)


capitaliseFst :: String -> String
capitaliseFst [] = error "empty string fed into function capitaliseFst."
capitaliseFst (c:cs) = toUpper c : cs

listInt2Exp :: [Int] -> Exp
listInt2Exp = foldr (\ i (ListE es) -> ListE (LitE (IntegerL (fromIntegral i)) : es)) (ListE [])

listInt2Pat :: [Int] -> Pat
listInt2Pat = foldr (\ i (ListP es) -> ListP (LitP (IntegerL (fromIntegral i)) : es)) (ListP [])
