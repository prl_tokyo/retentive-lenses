{-# LANGUAGE OverloadedStrings #-}
module PrintDataTypes where

import Def
import Text.PrettyPrint as TPP
-- import BiYacc.Helper.Utils
-- import BiYacc.Translation.BX.GenBrackets


import qualified Data.Map as Map (lookup)
import Data.Maybe (fromJust)
import Data.Generics hiding (empty)
import Text.PrettyPrint as TPP hiding (empty)
import qualified Text.PrettyPrint as TPP

import Text.Show.Pretty
import Debug.Trace

prtAbsDTs :: DataTypeDecs -> String
prtAbsDTs decs = render . vcat2 . map prtAbsDT $ decs

prtAbsDT :: DataTypeDec -> Doc
prtAbsDT (DataTypeDec ty defs) =
  "data" <+> text (printTypeRep ty) <+> "=" $+$
  nest2 (foldl1c (\e -> "  " <> prtDef e) (\es e -> es $$ "|" <+> prtDef e) defs) $+$
  nest2 ("|" <+> text (addNullCase ty)) $+$
  nest2 "deriving (Show, Read, Eq, Data, Typeable)"
  where prtDef :: DataTypeDef -> Doc
        prtDef (DataTypeDef con fields) =
          hsep $ [text con] ++ map (text . wrapParen . printTypeRep) fields
        -- if there is any space, it means that the type has parameter.
        wrapParen :: String -> String
        wrapParen s@('(':_) = s -- do not add parens for tuple type, no need
        wrapParen s | elem ' ' s = "(" ++ s ++ ")"
        wrapParen s = s


vcat2 = vcat3 . punctuate (text "\n")
  where vcat3 [] = TPP.empty
        vcat3 a@(x:xs) = foldr1 ($+$) a

nest2 :: Doc -> Doc
nest2 = nest 2

nest3 = nest 3

nestn6 = nest (-6)

nestn2 = nest (-2)

nestn1 = nest (-1)
