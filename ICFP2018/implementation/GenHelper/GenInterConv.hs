{-
generate interconvertible data types injections for converting between them.

For example, if we have data types

data Expr = ... | ToTerm Term
data Term = ... | ToFactor Factor
data Factor = ... | ToExpr Expr

then we can use ToTerm to inject a value of type Term into a value of type Expr.
The value (Term) became the first subtree of an Expr, so we need to add a path [0].

class IntConv sub sup where
  inj :: sub -> sup

these declarations are written by users:


instance IntConv Expr Term where
  inj a = FromT "TT" a
instance IntConv Term Factor where
  inj a = FromF "FF" a
instance IntConv Factor Expr where
  inj a = Paren "EE" a
...

and we need to make these declarations transitive and generate:

instance IntConv Expr Factor  where
  inj a = FromT "TT" (FromF "TT" a)
...



-- also generate mkInj functions, because we need to handle data wrapped in Dynamic,
mkInj :: SubTyTag -> SupTyTag -> Dynamic -> Dynamic

mkInj ExprTag TermTag (s0, vls) | isJust (fromDynamic s0 :: Maybe Term) = toDyn s'
  where s' = inj $ fromJust (fromDynamic s0 :: Maybe Term) :: Expr
...

mkInj a b c | a == b = c

-}
module GenHelper.GenInterConv where

import Def as D
import Language.Haskell.TH
import THAuxFuns
import TypeInf (findVarPath)
import Data.List (nub, nubBy)
import Control.Arrow

import Text.Show.Pretty
import Debug.Trace


genAllInjsDef :: InterConv -> String
genAllInjsDef (InterConv decls) =
  let decls' = rmInjReflexive (mkInjTransitive decls)
  in  pprint $ genIntConvClassDec : map genIntConvInsDec decls' ++
      [mkInjSig] ++ map genMkInjDecs decls' ++ [idInjDec]

genIntConvClassDec :: Dec
genIntConvClassDec = ClassD [] (mkName "IntConv") tyVars [] [SigD (mkName "inj") sigTy]
  where sigTy = var2T "sub" `mkArrTy` var2T "sup"
        tyVars = [PlainTV (mkName "sub"), PlainTV (mkName "sup")]


genIntConvInsDec :: InjDecl -> Dec
genIntConvInsDec (InjDecl (sup,sub) var rhs) = InstanceD Nothing [] ty [dec]
  where
    ty  = var2T "IntConv" `AppT` var2T (printTypeRep sup) `AppT` var2T (printTypeRep sub)
    dec = FunD (mkName "inj") [simpleClause [var2P var] e]
    e   =  dslPat2HsExp rhs


mkInjSig :: Dec
mkInjSig =  SigD (mkName "mkInj") funTy
 where funTy = foldr1 mkArrTy [var2T "SubTyTag", var2T "SupTyTag"
                              ,var2T "Dynamic" ,var2T "Dynamic"]


-- we need to handle data wrapped in Dynamics. So we wrap the function inj
-- into mkInj to handle Dynamics.
genMkInjDecs :: InjDecl -> Dec
genMkInjDecs (InjDecl tyRepPair var rhs) =
  FunD (mkName "mkInj") [genMkInjDec ((printTypeRep *** printTypeRep) tyRepPair)]


genMkInjDec :: (String,String) -> Clause
genMkInjDec (sub,sup) = Clause pat (GuardedB [(guard,e)]) [w]
  where
    pat = [con2P (sub ++ "Tag"), con2P (sup ++ "Tag"), (var2P "s0")]
    guard = NormalG $ var2E "isJust" `AppE`
              (SigE (var2E "fromDynamic" `AppE` var2E "s0")
                    (con2T "Maybe" `AppT` con2T sub))
    e = var2E "toDyn" `AppE` var2E"s'"
    w = simpleValD (var2P "s'") (SigE
          (var2E "inj" `AppE` (var2E "fromJust" `AppE`
                                (SigE (var2E "fromDynamic" `AppE` var2E "s0")
                                      (con2T "Maybe" `AppT` con2T sub))))
          (con2T sup))


idInjDec :: Dec
idInjDec = FunD (mkName "mkInj") [Clause [var2P "a" , var2P "b", var2P "c"]
             (GuardedB [(NormalG $ simpleInfixE (var2E "a") (var2E "==") (var2E "b")
                       ,var2E "c")]) [] ]


-- also make pair of the last element and the first element
mkCirClose :: [a] -> [(a,a)]
mkCirClose ls | length ls < 2 =
  error "length of the input list cannot be less than 2. in mkCirClose"
mkCirClose ls = (last ls, head ls) : mkCirOpen ls

-- do not make pair of the last element and the first element
mkCirOpen :: [a] -> [(a,a)]
mkCirOpen [a,b] = [(a,b)]
mkCirOpen (a:b:ls) = (a,b) : mkCirOpen (b:ls)
mkCirOpen _ = error "length of the input list cannot be less than 2. in mkCirOpen"



-- This function sets InterConv (a,a') (cons,path) | a == a' to InterConv (a,a) ([],[])
-- because in this case, we should not go from type a to type a by a chain of injections,
-- rather, an id function is enough.
-- resetCyclicToId = foldr (\ e@(InterConv (a, b) (tys, step)) es ->
--   if a == b then InterConv (a,a) (DataTypeRep "" [] , []) : es else e:es) []

-- calculate transitive closure. if (a,b) and (b,c) are in the list, then also
-- add (a,c) to the list.
-- mkTransitive :: Eq a => [(a, a)] -> [(a, a)]
-- mkTransitive l = toFixed oneStep l
--   where toFixed :: (Eq a) => (a -> a) -> a -> a
--         toFixed f s = let s' = f s
--                       in  if s' == s then s else toFixed f s'
--         oneStep l = nub $ l ++ [(a,c) | (a,b) <- l, (b',c) <- l, b == b' ]

-- remove reflexive pairs
-- rmReflexive :: Eq a => [(a, a)] -> [(a, a)]
-- rmReflexive = filter (\(a,b) -> a /= b)


-- Specilised version for the Interconvertible datatypes.
-- input [String] and [Step] are singletons (not always, depends on the user's program)
mkInjTransitive :: [InjDecl] -> [InjDecl]
mkInjTransitive l = toFixed oneStep l
  where toFixed :: (Eq a) => (a -> a) -> a -> a
        toFixed f s = let s' = f s
                      in  if s' == s then s else toFixed f s'
        oneStep l = nubBy test $ l ++
          [let path  = findVarPath var pat
               path' = findVarPath var' pat'
           in  InjDecl (a,c) var (insHole (pat, path')  pat') |
                InjDecl (a, b) var pat   <- l,
                InjDecl (b',c) var' pat' <- l,
                printTypeRep b == printTypeRep b' ]
          -- from a to b BY con, from b to c BY con', so from a to c BY [con',con]
          -- because when use AppE later, we get con' (con ...), first apply con.
        test :: InjDecl -> InjDecl -> Bool
        test (InjDecl (a,b) _ _) (InjDecl (c,d) _ _) = a == c && b == d


-- insert a datatype rep (d) into a focus of another datatype rep indicated by path (p)
insHole :: (Pattern, Path) -> Pattern -> Pattern
insHole (d, []) _  = error "impossible"
insHole (d, p) (D.ConP con ty fields) =
  D.ConP con ty (insHole2 (d, p) fields)

-- insert to place
insHole2 :: (Pattern, Path) -> [Pattern] -> [Pattern]
insHole2 (d, [p]) ds =
  let (before, this, after) = (take p ds , ds !! p , drop (p + 1) ds)
  in  before ++ [d] ++ after
insHole2 (d, p:ps) ds =
  let (before, this, after) = (take p ds , ds !! p , drop (p + 1) ds)
  in  before ++ [insHole (d,ps) this] ++ after


-- specialised version work for InjDecl datatypes
-- we do not need to do injection when the Sup-Type and Sub-Type are the same
rmInjReflexive :: [InjDecl] -> [InjDecl]
rmInjReflexive = filter (\ (InjDecl (a,b) _ _)  -> a /= b)
