module GenHelper.GenPatTag where

mkPrimSPatTag :: String -> String
mkPrimSPatTag = fst . mkPrimPatTag

mkPrimVPatTag :: String -> String
mkPrimVPatTag = snd . mkPrimPatTag

mkPrimPatTag :: String -> (String,String)
mkPrimPatTag ty = ("S_" ++ ty, "V_" ++ ty)
