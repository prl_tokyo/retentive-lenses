{-# LANGUAGE RecordWildCards #-}
module GenHelper.GenInsSubtree where

{-
insert a subtree into the ONLY hole in a given tree
generate code like:

insSubtree :: SPatTag -> Dynamic -> Dynamic -> Dynamic
insSubtree (S_ExprArithCase2)
          ((fromDynamic :: Dynamic -> Maybe Expr) -> Just (ET fromWild0 theHole))
          s0 = ET fromWild0 s1
 where s1 = mkInj (tyTag theHole) (askDynTyTag s0) s0
-}

import qualified Def as D
import THAuxFuns
import GenBX.GenPutNoLink
import GenHelper.GenRegPat
import GenHelper.GenInterConv
import Language.Haskell.TH

import Data.Map as Map hiding (map, foldl, foldr, filter, null, drop, partition, take)
import TypeInf (findVarPath)
import Debug.Trace
import Text.Show.Pretty

genInsSubtreeDef :: Map D.Pattern D.RegPatRep -> [D.Group] -> D.InterConv -> String
genInsSubtreeDef sRegPatEnv grps (D.InterConv injDecs) =
  let defs = genInsSubtreeDec sRegPatEnv grps injDecs
  in  if null defs
        then "insSubtree = undefined -- no need to define it since it will not be invoked."
        else pprint genInsSubtreeSig ++ "\n" ++ pprint defs


genInsSubtreeSig :: Dec
genInsSubtreeSig = SigD (mkName "insSubtree")
  (var2T "RegPat" `mkArrTy` dynT  `mkArrTy` dynT `mkArrTy` dynT)
  where dynT = var2T "Dynamic"

genInsSubtreeDec :: Map D.Pattern D.RegPatRep -> [D.Group] -> [D.InjDecl] -> [Dec]
genInsSubtreeDec sRegPatEnv grps injDecs =
  let grps' = keepInterConvs injDecs grps
  in  map (genInsSubtreeGrpDec sRegPatEnv) grps'

genInsSubtreeGrpDec :: Map D.Pattern D.RegPatRep -> (D.Group, Int, D.Path) -> Dec
genInsSubtreeGrpDec sRegPatEnv (D.Group (sTy,vTy) [D.Rule{..}], rNum, path) =
  FunD (mkName "insSubtree") [clause]
  where
    clause = Clause [var2P regPat, viewPS, var2P "s0"]
               (NormalB (var2E "toDyn" `AppE` dslPat2HsExp (repHoleName "s1" (invlhs, path))))
               [mkInj]
    regPat = D.fromJ "cannot find pattern. in genRepSubtree" $
               Map.lookup (normPattern lhs) sRegPatEnv

    invlhs = D.pat2RegPat' lhs
    viewPS = ViewP (ParensE (var2E ("fromDynamic :: Dynamic -> Maybe (" ++
                                     D.printTypeRep sTy ++ ")")))
                   (ConP (mkName "Just") [dslPat2HsPat $ repHoleName "theHole" (invlhs, path)])
    mkInj = simpleValD (var2P "s1")
              (var2E "fromDyn" `AppE` mkInj2 `AppE` var2E nullCon)
    mkInj2 = foldl1 AppE [var2E "mkInj"
             ,var2E "tyTag" `AppE` var2E "theHole"
             ,var2E "askDynTyTag" `AppE` var2E "s0"
             ,var2E "s0"]
    holeType = askType (invlhs, path)
    nullCon = pprint (mkDefVal holeType)



keepInterConvs :: [D.InjDecl] -> [D.Group] -> [(D.Group, Int, D.Path)]
keepInterConvs _ [] = []
keepInterConvs injDecs (D.Group{..} : ds) =
  case concatMap keep1 (zip gRules [0..]) of
    [] -> keepInterConvs injDecs ds
    [(r, rNum, varPath)] -> (D.Group gTy [r], rNum, varPath) : keepInterConvs injDecs ds

  where
    keep1 :: (D.Rule, Int) -> [(D.Rule, Int, D.Path)]
    keep1 (r@(D.Rule (D.ConP cons _ _) rhs _) , rNum) =
      [(r, rNum, returnStepForInj cons injDecs) | isInj cons injDecs]
    keep1 _ = []

    returnStepForInj :: String -> [D.InjDecl] -> D.Path
    returnStepForInj cons0 injDecs =
      case filter (\ (D.InjDecl _ _ (D.ConP cons1 _ _)) -> cons0 == cons1) injDecs of
        [] -> error $ "panic. cannot do injection using constructor (" ++
                      cons0 ++ "). in returnStepForInj."
        [D.InjDecl _ var pat] -> findVarPath var pat

    isInj :: String -> [D.InjDecl] -> Bool
    isInj cons0 injDecs =
      any (\(D.InjDecl _ _ (D.ConP cons1 _ _)) -> cons0 == cons1) injDecs

-- data InterConv = InterConv InjDecls
-- type InjDecls = [InjDecl]
-- data InjDecl = InjDecl (DataTypeRep,DataTypeRep) Var String


-- replace the ONLY hole in a LHS pattern with a new variable name
-- the search for the hole is guided by Step
repHoleName :: String -> (D.Pattern, D.Path) -> D.Pattern
repHoleName newName (D.ConP con rep fields , []) = D.ConP con rep fields
repHoleName newName (D.ConP con rep fields , p)  =
  D.ConP con rep (repHoleName2 newName (fields,p))
repHoleName newName (D.VarP v rep , []) = D.VarP v rep
repHoleName _ _ = error "impossible case in repHoleName."

repHoleName2 :: String -> ([D.Pattern], D.Path) -> [D.Pattern]
repHoleName2 newName (ds, [p]) =
  let (before, this, after) = (take p ds , ds !! p , drop (p + 1) ds)
  in  case this of
        D.WildP rep  -> before ++ [D.VarP newName rep] ++ after
repHoleName2 newName (ds, p:ps) =
  let (before, this , after) = (take p ds , ds !! p , drop (p + 1) ds)
  in  before ++ [repHoleName newName (this,ps)] ++ after


-- replace the ONLY hole in a LHS pattern with a new variable name
-- a special version works for the generation of mkInj and interconvertible datatypes
-- "theHole".  Term String Expr --> Term String theHole
repHoleName' :: String -> (D.DataTypeRep, D.Path) -> D.DataTypeRep
repHoleName' newName (d, [])  = d
repHoleName' newName (D.TyCon con fields, p) =
  D.TyCon con (repHoleName'2 newName (fields,p))
  where
    repHoleName'2 :: String -> ([D.DataTypeRep], D.Path) -> [D.DataTypeRep]
    repHoleName'2 newName (ds, [p]) =
      let (before, this, after) = (take p ds , ds !! p , drop (p + 1) ds)
      in  before ++ [D.TyCon newName []] ++ after
    repHoleName'2 newName (ds, p:ps) =
      let (before, this , after) = (take p ds , ds !! p , drop (p + 1) ds)
      in  before ++ [repHoleName' newName (this,ps)] ++ after

-- ask the type of a particular place in a pattern guided by a path to that place
askType :: (D.Pattern, D.Path) -> D.DataTypeRep
askType (pat, [])  = D.pat2TypeRep pat
askType (D.ConP con rep fields , p:ps) =
  let this = fields !! p
  in  askType (this,ps)
askType _ = error "impossible case in askType."
