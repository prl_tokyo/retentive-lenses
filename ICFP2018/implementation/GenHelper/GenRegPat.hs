{-
we need to generate datatypes for representing all the left-hand paterns of
consistency declarations.

Expr <---> Arith
Plus _ x y ~ Add x y

data SRegPat =
    PlusRP0
  | MinusRP1
  | FromTermRP2
  | ...

We also need to do so for the right-hand sides because we need the run-time
representation of the pattern for view-side of the HLinks.

data VRegPat =
    AddRP0
  | SubRP1
  | ...

For simplicity, the two data types are mereged into one in this implementation
-}


module GenHelper.GenRegPat where

import qualified Def as D
import THAuxFuns

import Language.Haskell.TH as TH

import Data.Map hiding (foldr, foldl, map)
import qualified Data.Map as Map
import Text.Show.Pretty
import Control.Monad.State
import Data.List (nub)
import Data.Maybe (catMaybes)

import Debug.Trace

-- generate datatype declaration of Regions from [D.DataTypeRep]
genRegPatDef :: [D.Group] -> String
genRegPatDef grps =
  let regPatEnv = genRegPatEnv grps
      sRegs     = elems regPatEnv
      sRegsPlus = sRegs ++ map mkPrimPatReg D.primTypes

      sDec = DataD [] (mkName "RegPat") [] Nothing
              (map (\tg -> NormalC (mkName tg) []) sRegsPlus ++
  -- add additional unit pattern for imaginary nodes.
                    [NormalC (mkName "Void") []])
              [var2T "Eq", var2T "Show", var2T "Ord", var2T "Read"]
  in  pprint sDec



-- tags for prim types. only string representations
mkPrimPatReg :: String -> String
mkPrimPatReg ty = ty ++ "R"


-------------

-- generate a map from pattern to its hash, which is the runtime representation
-- for both the pattern and its region pattern (pat: Plus _ x y. regpat: Plus a _ _)
genRegPatEnv :: [D.Group] -> Map D.Pattern D.RegPatRep
genRegPatEnv grps = unions $ map (\g -> evalState (genRegPatGroup g) 0) grps
  where
    genRegPatGroup :: D.Group -> State Int (Map D.Pattern D.RegPatRep)
    genRegPatGroup (D.Group (sTy,vTy) rs) = do
      ms <- mapM (genRegPat2 (sTy,vTy)) rs
      return $ unions ms

    genRegPat2 :: (D.DataTypeRep, D.DataTypeRep) -> D.Rule -> State Int (Map D.Pattern D.RegPatRep)
    genRegPat2 (sTy,vTy) (D.Rule lhs rhs _) = do
      let lhs' = normPattern lhs
          rhs' = normPattern rhs
      n <- get
      put (n + 1)
      return $ if checkIfVarPat lhs' then singleton lhs' "Void"
                  else singleton lhs' (D.printTyAsName sTy ++ D.printTyAsName vTy ++ "S" ++ show n)
               `union`
               if checkIfVarPat rhs' then singleton lhs' "Void"
                  else singleton rhs' (D.printTyAsName sTy ++ D.printTyAsName vTy ++ "V" ++ show n)

      where
        checkIfVarPat :: D.Pattern -> Bool
        checkIfVarPat p@(D.VarP _ _) = True
        checkIfVarPat p = False



normPattern :: D.Pattern -> D.Pattern
normPattern pat = evalState (normPattern' pat) 0

-- pattern "a" and pattern "z" are the same.
-- normalise a pattern by renaming its variables starting from "v0".
-- and erase type field
normPattern' :: D.Pattern -> State Int D.Pattern
normPattern' (D.ConP con _ pats) = do
  pats' <- mapM normPattern' pats
  return $ D.ConP con (D.mkTyRep "" []) pats'
normPattern' (D.VarP v _) = do
  i <- get
  put (i + 1)
  return $ D.VarP ("v" ++ show i) (D.mkTyRep "" [])
normPattern' (D.LitP  lit) = return $ D.LitP lit
normPattern' (D.WildP _) = return $ D.WildP (D.mkTyRep "" [])

-- Plus _ a b ~ Add x y <= a ~ x, b ~ y ; ... ; FromTerm _ a ~ t <= a ~ t ;
-- will give [(PlusR String Unit Unit, AddR Unit Unit),
-- (FromTermR String Unit, UnitR)]
-- where the "String" should be replaced by the type of the hole.





-- erase constructor and literals. leave only variables pattern. for rhs
-- top level cons should be remained
-- eraseConsLit :: D.Pattern -> D.Pattern
-- eraseConsLit (D.ConP con ty ps) = D.ConP con ty (concatMap ers ps)
--   where ers :: D.Pattern -> [D.Pattern]
--         ers (D.ConP con ty ps) = concatMap ers ps
--         ers (D.LitP _) = []
--         ers p         = [p]
-- eraseConsLit (D.LitP _) = D.ConP "NoUse" (D.mkTyRep "NoUse" []) []
-- eraseConsLit p          = p



--
-- addRegions :: D.Pattern -> State Int D.Pattern
-- addRegions (D.ConP con ty ps) = do
--   ps' <- mapM addRegions ps
--   return $ D.ConP con ty ps'
-- addRegions vp@(D.VarP _ _)    = return vp
-- addRegions lp@(D.LitP  l)     = return lp
-- addRegions (D.WildP ty) = do
--   i <- get
--   put (i + 1)
--   return $ D.VarP ("r" ++ show i) ty

-- change the top constructor "Con" to "ConR", and invoke toRegPatRec for its children
-- if there is no constructor on the top, return Nothing
-- for children, we do not need to attach "R" to the name of the constructor.
-- dPat2RegPat :: D.Pattern -> Maybe D.Pattern
-- dPat2RegPat cp@(D.ConP con ty ps) = Just $ D.ConP (con ++ "R") ty ps'
--   where
--     (D.ConP _ _ ps') = evalState (toRegPatRec cp) 0
--
--     toRegPatRec :: D.Pattern -> State Int D.Pattern
--     toRegPatRec (D.ConP con ty ps) = do
--       ps' <- mapM toRegPatRec ps
--       return $ D.ConP con ty ps'
--     toRegPatRec (D.VarP  _ ty)     = return $ D.WildP ty
--     toRegPatRec (D.LitP  l)        = return $ D.LitP l
--     toRegPatRec (D.WildP ty) = do
--       i <- get
--       put (i + 1)
--       return $ D.VarP ("r" ++ show i) ty
-- dPat2RegPat cp@(D.VarP _ ty) = Nothing
-- -- dPat2RegPat cp@(D.VarP _ ty) = D.WildP ty
-- dPat2RegPat _ = error "panic. invalid input pattern for dPat2RegPat."

----------------
