{-
we need to generate datatypes for representing all the left-hand paterns of
consistency declarations.

data SRegions =
    PlusR String Unit Unit
  | MinusR String Unit Unit
  | FromTerm String Unit
  | ...
  | TermArith0Tag
  | ...

We also need to do so for the right-hand sides because we need the run-time
representation of the pattern for view-side of the HLinks.

data VRegions =
    AddR Unit Unit
  | SubR Unit Unit
  | ...
-}


module GenHelper.GenRegions where

import qualified Def as D
import THAuxFuns

import Language.Haskell.TH as TH

import Data.Map hiding (foldr, foldl, map)
import qualified Data.Map as Map
import Text.Show.Pretty
import Control.Monad.State
import Data.List (nub)
import Data.Maybe (catMaybes)

-- generate datatype declaration of Regions from [D.DataTypeRep]
genRegDef :: D.ASTDataTypeDecs -> String
genRegDef decs =
  let sRegs0 = concatMap genEachTyDef decs
      sRegs = sRegs0 ++ map mkPrimReg D.primTypes

      sDec = DataD [] (mkName "Region") [] Nothing
              (map (\tg -> NormalC (mkName tg) []) sRegs ++
  -- add additional unit pattern for imaginary nodes.
                    [NormalC (mkName "Unit_S") []
                    ,NormalC (mkName "Unit_V") []])
              [var2T "Eq", var2T "Show"]
  in pprint sDec


genEachTyDef :: D.ASTDataTypeDec -> [String]
genEachTyDef (D.ASTDataTypeDec ty defs) =
   map (genEachTyEachCon ty) defs

-- Plus String Expr Term --> PlusR String Expr Term
genEachTyEachCon :: D.DataTypeRep -> D.ASTDataTypeDef -> String
genEachTyEachCon ty (D.ASTDataTypeDef con fields) =
  showASTDataTypeDef $ D.ASTDataTypeDef (con ++"R") fields

showASTDataTypeDef :: D.ASTDataTypeDef -> String
showASTDataTypeDef (D.ASTDataTypeDef con fields) =
  foldr (\x xs -> D.printTypeRep x D.+^+ xs) "" (D.mkSimpleTyRep con : fields)

-- Plus _ a b ~ Add x y <= a ~ x, b ~ y ; ... ; FromTerm _ a ~ t <= a ~ t ;
-- will give [(PlusR String Unit Unit, AddR Unit Unit),
-- (FromTermR String Unit, UnitR)]
-- where the "String" should be replaced by the type of the hole.


-- for generating TYPE DEFINITIONS for Regions in String.
tyDef2RegTypeInStr :: D.Pattern -> Maybe String
tyDef2RegTypeInStr p = do
  s <- dPat2RegPat p
  return (toStr s)
  where
    toStr :: D.Pattern -> String
    toStr (D.ConP  con ty ps) =
      foldr1 (D.+^+) (con : map toStr ps)
    toStr (D.VarP  _ ty)    = D.printTypeRep ty
    toStr (D.LitP  l)       = D.showLit l
    toStr (D.WildP ty)      = D.printTypeRep ty


-- tyDef2RegTypeInStr :: D.Pattern -> Maybe String
-- tyDef2RegTypeInStr p = do
--   s <- dPat2RegPat p
--   return (toStr s)
--   where
--     toStr :: D.Pattern -> String
--     toStr (D.ConP  con ty ps) =
--       foldr1 (D.+^+) (con : map toStr ps)
--     toStr (D.VarP  _ ty)    = D.printTypeRep ty
--     toStr (D.LitP  l)       = D.showLit l
--     toStr (D.WildP ty)      = D.printTypeRep ty


-- D.Pattern to Region's data in String
-- generate a piece of DATA for Regions in the generated haskell code
dPat2RegDataInStr :: D.Pattern -> Maybe String
dPat2RegDataInStr p = do
  s <- dPat2RegPat p
  return (toStr s)
  where
    toStr :: D.Pattern -> String
    toStr (D.ConP  con ty ps) =
      foldr1 (D.+^+) ("(" : con : map toStr ps ++ [")"])
    toStr (D.VarP  v ty)    = pprint $ mkDefVal ty
    toStr (D.LitP  l)       = D.showLit l
    toStr (D.WildP ty)      = pprint $ mkDefVal ty


-- D.Pattern to Region's pattern in String
-- generate a PATTERN for Regions in the generated haskell code
dPat2RegPatInStr :: D.Pattern -> Maybe String
dPat2RegPatInStr p = do
  s <- dPat2RegPat p
  return (toStr s)
  where
    toStr :: D.Pattern -> String
    toStr (D.ConP  con ty ps) =
      foldr1 (D.+^+) ("(" : con : map toStr ps ++ [")"])
    toStr (D.VarP  v ty)    = v
    toStr (D.LitP  l)       = D.showLit l
    toStr (D.WildP ty)      = pprint $ mkDefVal ty


-- erase constructor and literals. leave only variables pattern. for rhs
-- top level cons should be remained
eraseConsLit :: D.Pattern -> D.Pattern
eraseConsLit (D.ConP con ty ps) = D.ConP con ty (concatMap ers ps)
  where ers :: D.Pattern -> [D.Pattern]
        ers (D.ConP con ty ps) = concatMap ers ps
        ers (D.LitP _) = []
        ers p         = [p]
eraseConsLit (D.LitP _) = D.ConP "NoUse" (D.mkSimpleTyRep "NoUse") []
eraseConsLit p          = p



--
addRegions :: D.Pattern -> State Int D.Pattern
addRegions (D.ConP con ty ps) = do
  ps' <- mapM addRegions ps
  return $ D.ConP con ty ps'
addRegions vp@(D.VarP _ _)    = return vp
addRegions lp@(D.LitP  l)     = return lp
addRegions (D.WildP ty) = do
  i <- get
  put (i + 1)
  return $ D.VarP ("r" ++ show i) ty

-- change the top constructor "Con" to "ConR", and invoke toRegPatRec for its children
-- if there is no constructor on the top, return Nothing
-- for children, we do not need to attach "R" to the name of the constructor.
dPat2RegPat :: D.Pattern -> Maybe D.Pattern
dPat2RegPat cp@(D.ConP con ty ps) = Just $ D.ConP (con ++ "R") ty ps'
  where
    (D.ConP _ _ ps') = evalState (toRegPatRec cp) 0

    toRegPatRec :: D.Pattern -> State Int D.Pattern
    toRegPatRec (D.ConP con ty ps) = do
      ps' <- mapM toRegPatRec ps
      return $ D.ConP con ty ps'
    toRegPatRec (D.VarP  _ ty)     = return $ D.WildP ty
    toRegPatRec (D.LitP  l)        = return $ D.LitP l
    toRegPatRec (D.WildP ty) = do
      i <- get
      put (i + 1)
      return $ D.VarP ("r" ++ show i) ty
dPat2RegPat cp@(D.VarP _ ty) = Nothing
-- dPat2RegPat cp@(D.VarP _ ty) = D.WildP ty
dPat2RegPat _ = error "panic. invalid input pattern for dPat2RegPat."

------------
-- tags for prim types. only string representations

mkPrimReg :: String -> String
mkPrimReg ty = ty ++ "R"
----------------


genAskDynTagDef :: [D.Group] -> String
genAskDynTagDef = pprint . genAskDynTag

genAskDynTag :: [D.Group] -> [Dec]
genAskDynTag grps =
  [sig, FunD (mkName "askDynTag")
             (map (\(D.Group (sTy, _) _) -> genAskDynTag2 sTy) grps ++
              map (genAskDynTag2 . D.mkSimpleTyRep) D.primTypes)]
  where sig = SigD (mkName "askDynTag") (var2T "Dynamic" `mkArrTy` var2T "TyTag")

genAskDynTag2 :: D.DataTypeRep -> Clause
genAskDynTag2 sTy = Clause [viewPV] body []
  where
    viewPV = ViewP (ParensE $ var2E ("fromDynamic :: Dynamic -> Maybe " ++ D.printTypeRep sTy))
               (ConP (mkName "Just") [WildP])
    body = NormalB (var2E (D.tyAsFunName sTy ++ "Tag"))
