{-# LANGUAGE RecordWildCards #-}
{-

For each user-defined datatype, generate a tag representing the type.
Types in Haskell are not first class citizen, so we need some representation
(tags) to denote types.

data TyTag =
    ExprTag
  | TermTag
  | FactorTag
  | ArithTag


Also generate a type class TagTypeable and instances for each data type
containing functions "tyTag" for mapping data to its typeTag.

class TagTypeable a where
 tyTag :: a -> TyTag

instance TagTypeable Arith where
  tyTag (Add _ _) = ArithTag
  tyTag (Sub _ _) = ...

-}

module GenHelper.GenTypeable where

import qualified Def as D
import THAuxFuns
import GenHelper.GenRegPat
import TypeInf

import Language.Haskell.TH

import Data.Map hiding (foldr, map, foldl)
import qualified Data.Map as Map

import Control.Monad.State

-------------------------
-- generate datatype declaration of TypeTags from D.DataTypeDecs
genTypeTagDef' :: [D.Group] -> Dec
genTypeTagDef' = genTypeTagDef . D.getTypes

-- generate datatype declaration of TypeTags from [DataTypeRep]
-- each user-defined datatype and primitive type will be assigned one tag.
genTypeTagDef :: [D.DataTypeRep] -> Dec
genTypeTagDef tys =
  let env = genTypeTagEnv tys
      tags = elems env ++ primTags
      primTags = map mkPrimTyTag D.primTypes
  in  simpleDataD "TyTag"
        (map (\tg -> NormalC (mkName tg) []) tags)
        [var2T "Eq", var2T "Show"]


genTypeTagEnv :: [D.DataTypeRep] -> Map String String
genTypeTagEnv = foldr
  (\ d env -> insert (D.printTyAsName d) (D.printTyAsName d ++ "Tag") env)
  Map.empty
---------------------------


-- from here, generate type class and instances
genTagTypeableClass = pprint genTagTypeableClassDec
genTagTypeableInstances tyDecs grps = pprint $ genTagTypeableInstanceDecs tyDecs grps


-- a class equipped with a function from data to its type
genTagTypeableClassDec :: Dec
genTagTypeableClassDec = ClassD [] (mkName "TagTypeable") [PlainTV (mkName "a")] [] decs
  where  decs = [SigD (mkName "tyTag") (var2T "a" `mkArrTy` var2T "TyTag")]

-- only generate instances for concrete data types appeared in concsistency relations
genTagTypeableInstanceDecs :: [D.DataTypeDec] -> [D.Group] -> [Dec]
genTagTypeableInstanceDecs tyDecs grps =
  concat $ evalState (mapM (genFromGroup tyDecs) grps) []

genFromGroup :: [D.DataTypeDec] -> D.Group -> State [D.DataTypeRep] [Dec]
genFromGroup tyDecs (D.Group (sTy@(D.TyCon sTyName _),vTy@(D.TyCon vTyName _)) _) = do
  let sTySubst = produceSubst sTy tyDecs
      (D.DataTypeDec sTyRep sTyDefs) = applyTyVarSubstsAndSelect sTyName sTySubst tyDecs
      srcTyTag = var2T "TagTypeable" `AppT`  var2T (D.printTypeRepWithParen sTyRep)
      srcDecs = map (genDef sTyRep) sTyDefs

      vTySubst = produceSubst vTy tyDecs
      (D.DataTypeDec vTyRep vTyDefs) = applyTyVarSubstsAndSelect vTyName vTySubst tyDecs
      viewTyTag = var2T "TagTypeable" `AppT`  var2T (D.printTypeRepWithParen vTyRep)
      viewDecs = map (genDef vTyRep) vTyDefs

  done <- get
  put (addIfNotExist vTyRep (addIfNotExist sTyRep done))

  return $ (if sTyRep `elem` done then [] else [InstanceD Nothing [] srcTyTag srcDecs]) ++
           (if vTyRep `elem` done then [] else [InstanceD Nothing [] viewTyTag viewDecs])
  where
    genDef tyRep (D.DataTypeDef con tys) = FunD (mkName "tyTag")
             [simpleClause [genTyTagPat con tys]
               (ConE (mkName (D.printTyAsName tyRep ++ "Tag")))]

    genTyTagPat :: String -> [a] -> Pat
    genTyTagPat con tys = ConP (mkName con) (map (const WildP) tys)

addIfNotExist :: Eq a => a -> [a] -> [a]
addIfNotExist a as = if a `elem` as then as else a : as


-------------
-- tags for primitive types
mkPrimTyTag :: String -> String
mkPrimTyTag s = s ++ "Tag"



genAskDynTyTagDef :: [D.Group] -> String
genAskDynTyTagDef = pprint . genAskDynTyTag

genAskDynTyTag :: [D.Group] -> [Dec]
genAskDynTyTag grps =
  [sig, FunD (mkName "askDynTyTag")
             (map (\(D.Group (sTy, _) _) -> genAskDynTyTag2 sTy) grps ++
              map (\t -> genAskDynTyTag2 $ D.mkTyRep t []) D.primTypes)]
  where sig = SigD (mkName "askDynTyTag") (var2T "Dynamic" `mkArrTy` var2T "TyTag")

genAskDynTyTag2 :: D.DataTypeRep -> Clause
genAskDynTyTag2 sTy = Clause [viewPV] body []
  where
    viewPV = ViewP (ParensE $ var2E ("fromDynamic :: Dynamic -> Maybe " ++
                              D.printTypeRepWithParen sTy))
               (ConP (mkName "Just") [WildP])
    body = NormalB (var2E (D.printTyAsName sTy ++ "Tag"))


-- generate TagTypeable instances for SPatTag.

-- genSPatTagTagTypeableInstances = pprint . genSPatTagTagTypeableInstanceDecs
--
-- genSPatTagTagTypeableInstanceDecs :: [D.Group] -> [Dec]
-- genSPatTagTagTypeableInstanceDecs grps =
--   [InstanceD Nothing [] ty
--     (forPrim)]
--     -- (concatMap genSPatTagTagTypeableInstanceDec grps ++ forPrim)]
--   where
--     ty = var2T "TagTypeable" `AppT`  var2T "SPatTag"
--     forPrim = map handlePrim D.primTypes
--     handlePrim ty = FunD (mkName "tyTag")
--                       [simpleClause [var2P (mkPrimSPatTag ty)]
--                       (ConE . mkName $ ty ++ "Tag")]


-- genSPatTagTagTypeableInstanceDec :: D.Group -> [Dec]
-- genSPatTagTagTypeableInstanceDec D.Group{..} = genDef gTy gRules
--   where
--     genDef :: (D.DataTypeRep,D.DataTypeRep) -> [D.Rule] -> [Dec]
--     genDef (sTy,vTy) = fst . foldl
--       (\ (acc, c) (D.Rule{..}) ->
--         (FunD (mkName "tyTag")
--                   [simpleClause [var2P (pat2CaseNumberS (sTy,vTy) c)]
--                     (ConE . mkName $ D.printTypeRep sTy ++ "Tag")]
--            : acc , c + 1))
--       ([] , 0)

-- for primitive types
-- genSPatTagTagTypeableInstancePrim = D.primTypes
