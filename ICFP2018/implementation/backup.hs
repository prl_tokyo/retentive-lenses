escapePrim :: D.DataTypeRep -> Exp
escapePrim ty =
  let tyStr = D.typeRep2Str ty
  in  case tyStr of
        "Integer" -> LitE (IntegerL 0)
        "String"  -> LitE (StringL "")
        "Char"    -> LitE (CharL 'X')
        -- "Bool" ->
        -- "Double" ->
        -- "Float" ->
        -- "Int" ->
        _ -> ConE . mkName $ tyStr ++ "Null"


--------------------
-- merge groups by their view-type
mergeGrpByVT :: [D.Group] -> [D.Group]
mergeGrpByVT = merge . sortByVT
  where
    merge :: [D.Group] -> [D.Group]
    merge [g] = [g]
    merge (g1@(D.Group (st1,vt1) rs1) : g2@(D.Group (_,vt2) rs2) : gs) =
      if vt1 == vt2
      then
        let g1' = D.Group (st1,vt1) (rs1 ++ rs2)
        in  merge (g1':gs)
      else g1 : merge (g2:gs)



-- ,"isPrefix :: Step -> Path -> Bool"
-- ,"isPrefix [] _ = True"
-- ,"isPrefix (s:ss) (p:ps) | s == p = isPrefix ss ps"
-- ,"isPrefix _ _ = False"


-- example code for fetch
{-
 "locToPathInteger :: Path -> Integer -> Dynamic"
,"locToPathInteger [] i = toDyn i"
,"locToPathInteger _ _  = error \"no more path\""
,""
,"locToPath :: Path -> Expr -> Dynamic"
,"locToPath = locToPathExpr"
,""
,"locToPathExpr :: Path -> Expr -> Dynamic"
,"locToPathExpr []     src = toDyn src"
,"locToPathExpr (p:ps) src ="
,"  case (src, p) of"
,"    (EAdd x _, 0)    -> locToPathExpr ps x"
,"    (EAdd _ y, 1)    -> locToPathTerm ps y"
,"    (ESub x _, 0)    -> locToPathExpr ps x"
,"    (ESub _ y, 1)    -> locToPathTerm ps y"
,"    (Term t, 0)      -> locToPathTerm ps t"
,""
,""
,"locToPathTerm :: Path -> Term -> Dynamic"
,"locToPathTerm []     src = toDyn src"
,"locToPathTerm (p:ps) src ="
,"  case (src, p) of"
,"    (EMul x _, 0)    -> locToPathTerm   ps x"
,"    (EMul _ y, 1)    -> locToPathFactor ps y"
,"    (EDiv x _, 0)    -> locToPathTerm   ps x"
,"    (EDiv _ y, 1)    -> locToPathFactor ps y"
,"    (Factor f, 0)    -> locToPathFactor ps f"
,""
,""
,"locToPathFactor :: Path -> Factor -> Dynamic"
,"locToPathFactor []     src = toDyn src"
,"locToPathFactor (p:ps) src ="
,"  case (src, p) of"
,"    (ENum n ,  0) -> locToPathInteger ps n"
,"    (ENeg f ,  0) -> locToPathFactor  ps f"
,"    (Paren x , 0) -> locToPathExpr    ps x"
,""
,"getIntegerInteger s = (s,)"
,"putIntegerInteger _ _ _ _ v = v"
,""

-}


------------------------ infer type, forgot to handle borderline cases.
-- let Add x y = Add undefined undefined
-- in  typeOfStr x

-- pattern to Exp filling undefined for all variable occurrence
pat2UDExp :: P.Pattern -> Exp
pat2UDExp (P.ConsP c ps) = ParensE $ foldl1 (AppE) ((ConE . mkName $ c):(map pat2UDExp ps))
pat2UDExp (P.VarP  _)    = VarE (mkName "undefined")
pat2UDExp (P.LitP  l)    = LitE (interpLit l)
pat2UDExp (P.WildP)      = error "do not support wildcards in expression."


getVarsInP :: P.Pattern -> [P.Var]
getVarsInP (P.ConsP c ps) = concatMap getVarsInP ps
getVarsInP (P.VarP  v)    = [v]
getVarsInP (P.LitP  l)    = []
getVarsInP (P.WildP)      = error "do not support wildcards in expression."


allPats :: Data a => a -> [P.Pattern]
allPats = everything (++) (mkQ [] go)
  where
    go :: P.Rule -> [P.Pattern]
    go (P.Rule l r _) = [l,r]

--
inferType :: P.Var -> P.Pattern -> Exp
inferType v pat =
  let hsPat  = dslPat2HsPat pat
      udData = NormalB (pat2UDExp pat)
      dec    = ValD hsPat udData []
      exp    = parseExp' $ "typeOfStr " ++ v
  in  LetE [dec] exp

-- let Add x y = Add undefined undefined
-- in  [typeOfStr x , typeOfStr y] ++
-- let Sub x y = Sub undefined undefined
-- in  [typeOfStr x , typeOfStr y] ++ ...
inferTypes :: [P.Pattern] -> Exp
inferTypes [pat] =
  let vars   = getVarsInP pat
      hsPat  = dslPat2HsPat pat
      udData = NormalB (pat2UDExp pat)
      dec    = ValD hsPat udData []
      exp    = ListE $ map (\v -> parseExp' ("typeOfStr " ++ v)) vars
  in  LetE [dec] exp

inferTypes (p:ps) =
  let vars   = getVarsInP p
      hsPat  = dslPat2HsPat p
      udData = NormalB (pat2UDExp p)
      dec    = ValD hsPat udData []
      exp0   = ListE $ map (\v -> parseExp' ("typeOfStr " ++ v)) vars
      -- exp1   = InfixE (Just exp0) (VarE GHC.Base.++) (Just (inferTypes ps))
      -- exp1   = InfixE (Just exp0) (VarE (mkName "++")) (Just (inferTypes ps))
      exp1   = InfixE (Just exp0) (parseExp' "(++)") (Just (inferTypes ps))
  in  LetE [dec] exp1

---------------------------


-- invoke ghci is very slow
runInferType :: Var -> Pattern -> String
runInferType var pat =
  let res = unsafePerformIO $ runInterpreter (do
        -- WARNINGS: never load mouldes which are also used for compiling the program.
        -- see the Language.Haskell.Interpreter package for detailed explaination!
        loadModules ["Test.hs"]

        -- Sets the modules whose context is used during evaluation. All bindings of these
        -- modules are in scope, not only those exported.
        setTopLevelModules ["Test"]
        -- (Name, Qualified) . (Data.Map, Just M)]
        setImportsQ [("Prelude", Nothing)]

        let expr1 = TH.pprint $ inferType var pat
        -- interpret expr1 (as :: String)
        interpret expr1 (as :: String))
  in  case res of
        Left  err -> error (ppShow err)
        Right a   -> a


-- invoke ghci is very slow
runInferTypes :: [Pattern] -> [String]
runInferTypes pats =
  let res = unsafePerformIO $ runInterpreter (do
        -- WARNINGS: never load mouldes which are also used for compiling the program.
        -- see the Language.Haskell.Interpreter package for detailed explaination!
        loadModules ["Test.hs"]

        -- Sets the modules whose context is used during evaluation. All bindings of these
        -- modules are in scope, not only those exported.
        setTopLevelModules ["Test"]
        -- (Name, Qualified) . (Data.Map, Just M)]
        setImportsQ [("Prelude", Nothing)]
        let expr1 = TH.pprint $ inferTypes pats
        -- interpret expr1 (as :: String)
        interpret expr1 (as :: [String]))
  in  case res of
        Left  err -> error (ppShow err)
        Right a   -> a


gggg :: [Group] -> String
gggg groups =
  let pats = allPats groups
  in  TH.pprint (inferTypes pats)

-- ggg :: IO ()
-- ggg = do
--   let r = (runInferType "x" pat1)
--   putStrLn (show r)
--   let l = (runInferType "y" pat1)
--   putStrLn (show l)


-- haha = do
--   i <- eval "1 + 6 :: Int" ["System.Eval.Haskell"] :: IO (Maybe Int)
--   when (isJust i) $ putStrLn (show (fromJust i))


-- error. eval can only load packages installed in the system
-- hehe var pat = do
--   ma <- eval_ (genCode var pat) ["Data.Data", "Data.Typeable", "Test"] [] [] ["."]
--   -- ma <- eval (genCode var pat) ["Data.Data", "Data.Typeable", "Def"]
--   case ma of
--     Left err -> error (concat err)
--     Right a  -> do
--       when (isJust a) $ putStrLn (fromJust a)



-----------------------
