module Main where

import Def
import Parser
import TypeInf
import GenProgram
import PrintDataTypes
import GenHelper.GenFetchable
import GenHelper.GenTypeable
import GenHelper.GenRegPat

import Data.Maybe
import Data.Map hiding (map, take, null)
import Text.Parsec
import Text.Show.Pretty
import qualified Language.Haskell.TH as TH

import System.Directory
import Text.PrettyPrint

import System.Environment
import Control.Monad
import System.IO.Unsafe
import Debug.Trace



main :: IO ()
main = putStrLn "Hello World"

-- main :: IO ()
-- main = do
--   args <- getArgs
--   if length args < 2
--     then error $ "not enough input arguments." +^+
--                  "please input data-type-def-file and program-file"
--     else do
--   let [typeDefFile, progFile] = args
--   typeDefStr <- readFile typeDefFile
--   progStr <- readFile progFile
--   case parse pAllDatatypesAndInterConvs "TypeDefinitionFile" typeDefStr of
--     Left  err -> pPrint err
--     Right (allTypes, intercvs) -> do
--       putStrLn (ppShow intercvs)
--       case parse pProgram "dataFile" progStr of
--         Left  err -> pPrint err
--         Right groups -> do
--           putStrLn (ppShow groups)
--           let env  = buildCon2FieldsEnv allTypes
--               groups' = map (\g@(Group (sTy, vTy) _) -> applyTyAnno sTy vTy env g) groups
--               prog    = foldr1 newlineSS $ map TH.pprint (genProgram groups')
--               tagTyDefs = TH.pprint $ genTypeTagDef (getTypes allTypes)
--           -- putStrLn prog
--           writeFile "Test/BXDef.hs" $ foldr1 newlineSS
--             [bxDefModHead
--             ,prtAbsDTs allTypes, tagTyDefs
--             ,genTagTypeableClass
--             ,genTagTypeableInstances allTypes
--
--             -- ,genSPatTagTagTypeableInstances groups'
--
--             ,genFetchableClass
--             ,genFetchableInstance allTypes
--
--             -- ,genSPatTagDef groups'
--             -- ,genVPatTagDef groups'
--             ]
--
--           writeFile "Test/Dekita.hs" $ foldr1 newlineSS
--             [bxModHead
--             ,prog
--             ]

-- head for BXDef Module
bxDefModHead :: String
bxDefModHead = foldr1 newlineS
  ["{-# LANGUAGE DeriveDataTypeable, TypeSynonymInstances, FlexibleInstances, MultiParamTypeClasses, ViewPatterns #-}"
  ,""
  ,"module BXDef where"
  ,""
  ,"import Prelude hiding (putChar)"
  ,"import Data.Data"
  ,"import Data.Generics hiding (GT)"
  ,"import Data.Dynamic"
  ,"import Data.List (partition, sortBy)"
  ,"import Data.Map hiding (map, foldl, foldr, filter, null, drop, partition)"
  ,"import Data.Maybe (catMaybes, isJust, fromJust)"
  ,""
  ,"type Region = (RegPat, Path)"
  ,"type RLink = (Region, Region)"
  ,""
  ,"type NamedPat = (RegPat, Int)"
  ,"type NamedRegion = (NamedPat, Path)"
  ,"type NamedRLink = (NamedRegion, NamedRegion)"
  ,""
  ,"data SProp = Top NamedPat"
  ,"           | Child NamedPat NamedPat Integer"
  ,"  deriving (Show, Eq, Ord)"
  ,""
  ,"type SPropLink = (SProp, SProp)"
  ,"data HLink = FirstOrderLink  NamedRLink"
  ,"           | SecondOrderLink SPropLink"
  ,"  deriving (Show, Eq, Ord)"
  ,""
  ,"type SSLink = (Path,RegPat,Path)"
  ,"type VVLink = (Path,Region,Path)"
  ,""
  ,"type OSDyn = Dynamic"
  ,"type SDyn  = Dynamic"
  ,"type VDyn  = Dynamic"
  ,""
  ,"type OSTyTag = TyTag"
  ,"type STyTag  = TyTag"
  ,"type VTyTag  = TyTag"
  ,"type SupTyTag = TyTag"
  ,"type SubTyTag = TyTag"
  ,""
  ,"type Path = [Integer]"
  ,"type Env = [HLink]"
  ,"type HLinks = [HLink]"
  ,"type SSLinks = [SSLink]"
  ,"type VVLinks = [VVLink]"
  ,""
  ,pathFuns
  ,""
  ,genSecondOrderPropFuns
  ,""
  ,eraseSecondOrderPropFuns
  ,""
  ,"apFst f (x,y) = (f x , y)"
  ,""
  ,""
  ]
--------

--
spliceDef :: String
spliceDef = foldr1 newlineS
  ["splice :: OSDyn -> RLink -> SDyn -> SDyn"
  ,"splice osDyn imag s0 = insSubtree sReg (fetch' (askDynTyTag osDyn) osDyn imag) s0"
  ,"  where"
  ,"    ((sReg,_),(Void,[])) = imag"]


-- functions handling paths and steps.
pathFuns = foldr1 newlineS
  ["-- functions handling paths and steps."
  ,"-- compose a SSLink and a RLink"
  ,"compSSSV :: SSLink -> RLink -> Maybe RLink"
  ,"compSSSV (sPathU,sReg1,sPathB) ((sReg2,sPathL),(vReg,vPathR))"
  ,"  | sPathB == sPathL && sReg1 == sReg2 = Just ((sReg2,sPathU),(vReg,vPathR))"
  ,"compSSSV _ _ = Nothing"
  ,""
  ,"-- compose SSLinks and [RLink]"
  ,"compSSSVs :: SSLinks -> [RLink] -> [RLink]"
  ,"compSSSVs ss sv = catMaybes [compSSSV s v | s <- ss, v <- sv]"
  ,""
  ,"filterEnv :: Path -> [RLink] -> [RLink]"
  ,"filterEnv s = filter (\\ (_ , (_,p)) -> isPrefix s p)"
  ,""
  ,"isPrefix :: Path -> Path -> Bool"
  ,"isPrefix [] _ = True"
  ,"isPrefix (s1:ss1) (s2:ss2) | s1 /= s2 = False"
  ,"isPrefix (s1:ss1) (s2:ss2) | s1 == s2 = isPrefix ss1 ss2"
  ,""
  ,"-- delete the given prefix path from all links"
  ,"delPathH :: (Path,Path) -> RLink -> RLink"
  ,"delPathH (sPre,vPre) ((sReg,sp),(vReg,vp)) ="
  ,"  ((sReg,delPrefix sPre sp) , (vReg,delPrefix vPre vp))"
  ,""
  ,"delPrefix :: Path -> Path -> Path"
  ,"delPrefix [] p = p"
  ,"delPrefix s1 s2 = if isPrefix s1 s2 then drop (length s1) s2"
  ,"  else error $ \"the first input path is not a prefix of the second \\n\" ++"
  ,"               \"input path1: \" ++ show s1 ++ \"\\n\" ++ \"input path2: \" ++ show s2"
  ,""
  ,"addPathSS :: (Path,Path) -> SSLink -> SSLink"
  ,"addPathSS (sPre1,sPre2) (sp1,sPat,sp2) = (sPre1 ++ sp1,sPat,sPre2 ++ sp2)"
  ,""
  ,"addPathH :: (Path,Path) -> RLink -> RLink"
  ,"addPathH (sPre,vPre) ((sReg,sp),(vReg,vp)) = ((sReg,sPre ++ sp) , (vReg,vPre ++ vp))"
  ,""
  ,"hasTopLink :: [RLink] -> Bool"
  ,"hasTopLink = ([] `elem`) . map (snd . snd)"
  ,""
  ,"-- get links with empty view paths (link at the top)"
  ,"-- return a Maybe real-link, a list of imaginary links (may be empty list), and remainings"
  ,"-- to do so, we first sort the HLinks in the following order:"
  ,"-- (3) : the links whose view-paths are empty come first"
  ,"-- (2) : among (3), whose view-patterns is Void come first."
  ,"-- (1) : among (2), whose source-paths are smaller come first."
  ,"getTopLinks :: [RLink] -> (Maybe RLink, [RLink], [RLink])"
  ,"getTopLinks hls ="
  ,"  let (candidates3,rem3) = partition (\\ (_,(_,vpath)) -> null vpath) hls"
  ,"      -- rem2 should be either empty or singleton"
  ,"      (candidates2,rem2) = partition (\\ (_,(vRegion,_)) -> vRegion == Void) candidates3 "
  ,"      candidates1 = sortBy cmpSPathRL candidates2"
  ,"  in  case rem2 of"
  ,"        []  -> (Nothing, candidates1, rem3)"
  ,"        [r] -> (Just r, candidates1, rem3)"
  ,""
  ,"cmpSPathRL :: RLink -> RLink -> Ordering"
  ,"cmpSPathRL ((_,sp1),_) ((_,sp2),_) = if sp1 < sp2 then LT"
  ,"  else if sp1 == sp2 then EQ else GT"
  ,""
  ]

  -- ,"cmpSPathHL :: HLink -> HLink -> Ordering"
  -- ,"cmpSPathHL ((sp1,_),_) ((sp2,_),_) = if sp1 < sp2 then LT"
  -- ,"  else if sp1 == sp2 then EQ else GT"
  -- ,""
  -- ,"data Unit = Unit deriving (Show, Eq, Ord, Read)"


genSecondOrderPropFuns :: String
genSecondOrderPropFuns = foldr1 newlineS
  ["toHLink :: [RLink] -> [HLink]"
  ,"toHLink rls ="
  ,"  let nrls = nameRLink rls"
  ,"  in  map FirstOrderLink nrls ++ [topProp nrls] ++ relativePos nrls"
  ,""
  ,"nameRLink :: [RLink] -> [NamedRLink]"
  ,"nameRLink rls ="
  ,"  let rls' = sortBy cmpSPathRL rls"
  ,"  in  zipWith (\\((sPat,sPath), (vPat,vPath)) i ->"
  ,"                  (((sPat,i),sPath) , ((vPat,i),vPath))) rls' [0..]"
  ,""
  ,"-- assume input lists are sorted"
  ,"topProp :: [NamedRLink] -> HLink"
  ,"topProp nrls ="
  ,"  case filter (\\ (((sPat,si),sPath) , ((vPat,vi),vPath)) -> null sPath && null vPath) nrls of"
  ,"    [((nspat,[]) , (nvPat,[]))] -> SecondOrderLink (Top nspat , Top nvPat)"
  ,""
  ,"-- assume input lists are sorted"
  ,"relativePos :: [NamedRLink] -> [HLink]"
  ,"relativePos [] = []"
  ,"relativePos (nrl@((_,sPath0), _):nrls) ="
  ,"  let (children, others) = span (\\ ((_,sPath1), _)  -> 1 + length sPath0 == length sPath1) nrls"
  ,"  in  establishPosLink nrl children ++ relativePos nrls"
  ,""
  ,"establishPosLink :: NamedRLink -> [NamedRLink] -> [HLink]"
  ,"establishPosLink _ [] = []"
  ,"establishPosLink father@((fnsPat,fsPath),(fnvPat,fvPath)) (((cnsPat,csPath),(cnvPat,cvPath)) : cs) ="
  ,"  let sPos = if null (drop (length fsPath) csPath)"
  ,"              then error \"impossible. in generating second-order links (relative positions)\""
  ,"              else head $ drop (length fsPath) csPath"
  ,"      vPos = if null (drop (length fvPath) cvPath)"
  ,"              then -1"
  ,"              else head $ drop (length fvPath) cvPath"
  ,"  in  SecondOrderLink ((Child fnsPat cnsPat sPos) , (Child cnvPat cnvPat vPos)) :"
  ,"      establishPosLink father cs"
  ,""
  ]

eraseSecondOrderPropFuns :: String
eraseSecondOrderPropFuns = foldr1 newlineS
  ["eraseSecondOrderProp :: [HLink] -> [RLink]"
  ,"eraseSecondOrderProp [] = []"
  ,"eraseSecondOrderProp (FirstOrderLink nrLink : l) = eraseName nrLink : eraseSecondOrderProp l"
  ,"eraseSecondOrderProp (SecondOrderLink _ : l) = eraseSecondOrderProp l"
  ,""
  ,"eraseName :: NamedRLink -> RLink"
  ,"eraseName (((regPatS,_), pathS), ((regPatV,_), pathV)) = ((regPatS, pathS) , (regPatV, pathV))"
  ]



-- head for BX Module
bxModHead :: String
bxModHead = foldr1 newlineS
  ["{-# LANGUAGE ViewPatterns #-}"
  ,"module BX where"
  ,""
  ,"import BXDef"
  ,"import Prelude hiding (putChar)"
  ,"import Data.Maybe (isJust, isNothing, fromJust)"
  ,"import Data.List (partition, sortBy, sort)"
  ,"import Data.Map hiding (map, foldl, foldr, filter, null, drop, partition)"
  ,"import Data.Dynamic"
  ,"import Text.Show.Pretty (pPrint, ppShow)"
  ]
