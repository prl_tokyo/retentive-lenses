module Parser where

import Def
import Text.Parsec

import Data.Map (singleton, unions, Map)
import Control.Monad
import Control.Monad.State

import Parsers.TyDefParser
import Parsers.CRParser

import qualified Text.Parsec.Token as TPT

import Debug.Trace


--------------------------------------------
-- type-def fileName -> consistency-relations fileName -> return...
parseAll :: String -> String -> IO (DataTypeDecs, InterConv, [Group])
parseAll dFile crFile = do

  defStr <- readFile dFile
  crStr  <- readFile crFile
  let TyDefAndInterConv decs intercv =  tyDefParser' defStr
      grps = crParser' crStr
  return (decs, intercv, grps)
