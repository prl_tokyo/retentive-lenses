{
{-# LANGUAGE DeriveDataTypeable, ViewPatterns #-}
module Parsers.TyDefParser (tyDefParser, tyDefParser') where


import Data.Char
import Data.Data
import Data.Typeable
import Def

}

%name tyDefParser
%tokentype { Token }
%error { tyParseError }



%token
  int             { TKInt    $$ }
  bool            { TKBool   $$ }
  string          { TKStr    $$ }
  char            { TKChar   $$ }
  var             { TKVar $$ }
  cons            { TKCons $$ }
  '='             { TKEq }
  '|'             { TKOr }
  '('             { TKLParen }
  ')'             { TKRParen }
  'data'          { TKData }
  '#InterConvertible' { TKInterConv }
  'instance'      { TKInstance }
  'InterConv'     { TKCInterConv }
  'where'         { TKWhere }
  'inj'           { TKInj }

%%

Defs     : TypeDefs InterConv      { TyDefAndInterConv $1 $2 }

TypeDefs : TypeDef                  { [$1] }
         | TypeDef TypeDefs         { $1 : $2 }


TypeDef : 'data' cons TyVars TyLines  {DataTypeDec (TyCon $2 $3) $4}

TyVars : {- empty -}  { [] }
       | var TyVars   { (TyVar $1) : $2}

TyLines : TyLine         {[$1]}
        | TyLine TyLines {$1 : $2}

TyLine : '=' cons TyFields {DataTypeDef $2 $3}
       | '|' cons TyFields {DataTypeDef $2 $3}

TyFields : {- empty -}    { [] }
         | TyField TyFields { $1 : $2}


TyField : cons    { TyCon $1 [] }
        | var     { TyVar $1 }
        | '(' cons TyFields ')' { TyCon $2 $3 }


InterConv : {- -}                                { InterConv [] }
          | '#InterConvertible' InstanceDecls    { InterConv $2 }

InstanceDecls : InstanceDecl                { [$1] }
              | InstanceDecl InstanceDecls  { $1 : $2 }


--   (sup, sub) / variable in lhs / the function to do the injection
InstanceDecl : 'instance' 'InterConv' Sup Sub 'where' 'inj' var '=' ConsPat { InjDecl ($3,$4) $7 $9 }

Sup :  cons                 { TyCon $1 [] }
    | '(' cons TyFields ')' { TyCon $2 $3 }

Sub :  cons                 { TyCon $1 [] }
    | '(' cons TyFields ')' { TyCon $2 $3 }



ConsPat : cons          {ConP $1 (mkSimpleTyRep "UNDEFINED") []}
        | cons ConsArgs {ConP $1 (mkSimpleTyRep "UNDEFINED") $2}

ConsArgs : ConsArg          { [$1] }
         | ConsArg ConsArgs { $1 : $2}

ConsArg  : Constants         { $1 }
         | var               { VarP $1 (mkSimpleTyRep "UNDEFINED") }
         | cons              { (ConP $1 (mkSimpleTyRep "UNDEFINED") [])}
         | '(' cons ConsArgs ')'    { ConP $2 (mkSimpleTyRep "UNDEFINED") $3 }

Constants : int      {LitP (LitInt $1) }
          | bool     {LitP (LitBool $1) }
          | string   {LitP (LitString $1) }
          | char     {LitP (LitChar $1) }


{
tyParseError :: [Token] -> a
tyParseError a = error $ "Parse error. Unconsumed tokens: " ++ show a


data Token = TKInt  Integer
           | TKBool Bool
           | TKStr  String
           | TKChar Char
           | TKVar  String
           | TKCons String
           | TKData
           | TKEq
           | TKOr
           | TKLParen
           | TKRParen
           | TKInterConv  -- keyword #InterConvertible
           | TKCInterConv -- class InterConv
           | TKInstance
           | TKIntConv
           | TKWhere
           | TKInj
  deriving (Show, Eq)


-- simple non-total lexer. does not support the case when variable names contain "instance"
lexer :: String -> [Token]
lexer [] = []
lexer ('=':cs) = TKEq : lexer cs
lexer ('|':cs) = TKOr : lexer cs
lexer ('(':cs) = TKLParen : lexer cs
lexer (')':cs) = TKRParen : lexer cs
lexer ('d':'a':'t':'a':cs) | (isSpace . head $ cs) = TKData : lexer cs
lexer ('#':'I':'n':'t':'e':'r':'C':'o':'n':'v':'e':'r':'t':'i':'b':'l':'e':cs) =
  TKInterConv : lexInterConv cs
lexer (c:cs)
      | isSpace c = lexer cs
      | isLower c = lexVar  (c:cs) lexer
      | isUpper c = lexCons (c:cs) lexer
lexer cs = error $ "non exhaustive patterns in lexer. First untokenised 100 chars: " ++ take 100 cs


findInstance :: String -> (String, String)
findInstance ('i':'n':'s':'t':'a':'n':'c':'e' : cs) | (isSpace . head $ cs) =
  ([], "instance" ++ cs)
findInstance (c:cs) =
  let (c1, cs1) = findInstance cs
  in  (c:c1, cs1)
findInstance [] = ([],[])

lexInterConv :: String -> [Token]
lexInterConv [] = []
lexInterConv ('(':cs) = TKLParen : lexInterConv cs
lexInterConv (')':cs) = TKRParen : lexInterConv cs
lexInterConv ('=':cs) =
  let (before,after) = findInstance cs
  in  TKEq : (lexInterConv before ++ lexInterConv after)
lexInterConv ('i':'n':'s':'t':'a':'n':'c':'e':cs) | (isSpace . head $ cs) = TKInstance : lexInterConv cs
lexInterConv ('I':'n':'t':'e':'r':'C':'o':'n':'v':cs) | (isSpace . head $ cs) = TKCInterConv : lexInterConv cs
lexInterConv ('w':'h':'e':'r':'e':cs) | (isSpace . head $ cs) = TKWhere : lexInterConv cs
lexInterConv ('i':'n':'j':cs) | (isSpace . head $ cs) = TKInj : lexInterConv cs
lexInterConv (c:cs)
      | isSpace c = lexInterConv cs
      | otherwise = lexData (c:cs)


lexData (c:cs) | isUpper c  = lexCons (c:cs) lexInterConv
lexData (c:cs) | isLower c  = lexVar  (c:cs) lexInterConv
lexData (c:cs) | isNumber c = lexNum  (c:cs) lexInterConv
lexData ('"':cs)  = lexStr  cs lexInterConv
lexData ('\'':cs) = lexChar cs lexInterConv
lexData ('T':'r':'u':'e':cs)     = TKBool True : lexInterConv cs
lexData ('F':'a':'l':'s':'e':cs) = TKBool False : lexInterConv cs
lexData otherwise = error $ "lex error in the right-hand side of interconvertible declarations. unconsumed tokens: \n" ++ otherwise


lexStr :: String -> (String -> [Token]) -> [Token]
lexStr cs nextLexer =
  let (str, rest) = span (\c -> c /= '"') cs
  in  TKStr str : nextLexer (tail rest)


lexChar :: String -> (String -> [Token]) -> [Token]
lexChar (c:'\'':cs) nextLexer = TKChar c : nextLexer cs
lexChar _ _ = error "unsupported characters. (e.g.: escape)"


lexNum :: String -> (String -> [Token]) -> [Token]
lexNum cs nextLexer =
  let (i,rest) = span isNumber cs
  in  TKInt (read i :: Integer) : nextLexer rest

lexVar :: String -> (String -> [Token]) -> [Token]
lexVar cs nextLexer =
  let (var,rest) = span isAlphaNum cs
  in  TKVar var : nextLexer rest

lexCons :: String -> (String -> [Token]) -> [Token]
lexCons cs nextLexer =
  let (cons,rest) = span isAlphaNum cs
  in  TKCons cons : nextLexer rest

----------
mkSimpleTyRep :: String -> DataTypeRep
mkSimpleTyRep str = TyCon str []


tydeftest = do
  stream <- readFile "spinetype.txt"
  let tokens = lexInterConv stream
      ast = tyDefParser tokens
  print ast

tyDefParser' = tyDefParser . lexer

}
