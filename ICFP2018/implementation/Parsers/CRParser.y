{
{-# LANGUAGE DeriveDataTypeable, ViewPatterns #-}
module Parsers.CRParser (crParser, crParser') where

import Data.Char
import Data.Data
import Data.Typeable
import Def

}

%name crParser
%tokentype { Token }
%error { crParseError }



%token
  int             { TKInt    $$ }
  bool            { TKBool   $$ }
  string          { TKStr    $$ }
  char            { TKChar   $$ }
  var             { TKVar  $$ }
  cons            { TKCons $$ }
  '\n'            { TKLF }
  '<--->'         { TKCRArrowTy }
  '~'             { TKCRArrowR  }
  ';;'            { TKDSemi }
  '_'             { TKWild }
  '('             { TKLParen }
  ')'             { TKRParen }


%%

Program  : CRs { $1 }

CRs : CR      { [$1] }
    | CR CRs  { $1 : $2 }

CR     : Type '<--->' Type LFs InductRules ';;' LFs { Group ($1,$3) $5}


Type   : cons TyFields { TyCon $1 $2 }

TyFields : {- empty -}    { [] }
         | TyField TyFields { $1 : $2}


TyField : cons    { TyCon $1 [] }
        | var     { TyVar $1 }
        | '(' cons TyFields ')' { TyCon $2 $3 }


InductRules : InducRule              { [$1] }
            | InducRule InductRules  { $1 : $2 }


InducRule : LHSPat '~' RHSPat LFs      {Rule $1 $3 []}


LHSPat : ConstructorPat { $1 }

ConstructorPat : cons          {ConP $1 (mkSimpleTyRep "UNDEFINED") []}
               | cons ConsArgs {ConP $1 (mkSimpleTyRep "UNDEFINED") $2}

ConsArgs : ConsArg          { [$1] }
         | ConsArg ConsArgs { $1 : $2}

ConsArg  : Constants         { $1 }
         | var               { VarP $1 (mkSimpleTyRep "UNDEFINED") }
         | '_'               { WildP (mkSimpleTyRep "UNDEFINED") }
         | cons              { (ConP $1 (mkSimpleTyRep "UNDEFINED") [])}
         | '(' cons ConsArgs ')'    { ConP $2 (mkSimpleTyRep "UNDEFINED") $3 }


RHSPat : Constants           { $1 }
       | ConstructorPat      { $1 }
       | var                 { VarP $1 (mkSimpleTyRep "UNDEFINED") }


Constants : int      {LitP (LitInt $1) }
          | bool     {LitP (LitBool $1) }
          | string   {LitP (LitString $1) }
          | char     {LitP (LitChar $1) }

LFs : '\n'        { }
    | '\n'  LFs   { }
{

crParseError :: [Token] -> a
crParseError a = error $ "Parse error. Unconsumed tokens: " ++ show a


data Token =
             TKInt  Integer
           | TKBool Bool
           | TKStr  String
           | TKChar Char
           | TKVar  String
           | TKCons String

           | TKLF
           | TKCRArrowTy
           | TKCRArrowR
           | TKDSemi
           | TKSemi
           | TKWild
           | TKLParen
           | TKRParen
  deriving (Show, Eq)


lexer :: String -> [Token]
lexer [] = []
lexer ('\n':cs) = TKLF   : lexer cs
lexer ('_':cs)  = TKWild : lexer cs
lexer ('(':cs)  = TKLParen : lexer cs
lexer (')':cs)  = TKRParen : lexer cs
lexer ('~':cs)  = TKCRArrowR : lexer cs
lexer (';':';':cs)  = TKDSemi : lexer cs
lexer (';':cs)  = TKSemi : lexer cs
lexer ('<':'-':'-':'-':'>':cs) = TKCRArrowTy : lexer cs
lexer (c:cs)
      | isSpace c = lexer cs
      | otherwise = lexData (c:cs)
lexer cs = error $ "non exhaustive patterns in lexer. First untokenised 100 chars: " ++ take 100 cs


lexData (c:cs) | isUpper c = lexCons (c:cs)
lexData (c:cs) | isLower c = lexVar  (c:cs)
lexData (c:cs) | isNumber c = lexNum  (c:cs)
lexData ('"':cs)  = lexStr  cs
lexData ('\'':cs) = lexChar cs
lexData ('T':'r':'u':'e':cs)     = TKBool True : lexer cs
lexData ('F':'a':'l':'s':'e':cs) = TKBool False : lexer cs


lexStr cs  =
  let (str,rest) = span (\c -> c /= '"') cs
  in  TKStr str : lexer (tail rest)


lexChar (c:'\'':cs) = TKChar c : lexer cs
lexChar _ = error "unsupported characters. (e.g.: escape)"

lexNum cs =
  let (i,rest) = span isNumber cs
  in  TKInt (read i :: Integer) : lexer rest


lexVar cs =
  let (var,rest) = span isAlphaNum cs
  in  TKVar var : lexer rest


lexCons cs =
  let (cons,rest) = span isAlphaNum cs
  in  TKCons cons : lexer rest


--------------
mkSimpleTyRep :: String -> DataTypeRep
mkSimpleTyRep str = TyCon str []


crtest = do
  stream <- readFile "spineprogram.txt"
  let tokens = lexer stream
      ast = crParser tokens
  print ast

crParser' = crParser . lexer
}
