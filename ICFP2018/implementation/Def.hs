{-# LANGUAGE DeriveDataTypeable, ViewPatterns #-}

module Def where

import Data.Data
import Data.Typeable
import Data.Map hiding (map, foldr, drop, filter)

import Text.Parsec hiding (State)
import qualified Text.Parsec.Token as TPT
import Text.Parsec.Language
import Data.List as List (nub)


import Control.Monad.State

-- type will be extended to support parametricity later
data Group = Group
  {gTy   :: (DataTypeRep, DataTypeRep)
  ,gRules :: [Rule]
  }
  deriving (Data, Typeable, Show, Eq)

data Rule = Rule
  {lhs :: Pattern
  ,rhs :: Pattern
  ,bases :: [((Var,DataTypeRep),(Var,DataTypeRep))]
  }
  deriving (Data, Typeable, Show, Eq)

data Pattern =
    -- constructor pattern: constructor name, type, sub patterns
    ConP  String DataTypeRep [Pattern]
  | VarP  Var DataTypeRep  -- var name, var type
  | LitP  Literal
  | WildP DataTypeRep
  deriving (Data, Typeable, Show, Eq, Ord)

data Literal
  = LitInt    Integer
  | LitFloat  Double
  | LitBool   Bool
  | LitChar   Char
  | LitString String
  deriving (Data, Typeable, Show, Eq, Ord)


type Var = String
type Path = [Int]

-- compile time representation for pattern and its region pattern
type RegPatRep = String


primTypes :: [String]
primTypes = ["Integer","String","Char","Bool"]

---------- data types for abstrac syntax


data TyDefAndInterConv = TyDefAndInterConv DataTypeDecs InterConv
  deriving (Typeable, Data, Show, Eq)


type DataTypeDecs   = [DataTypeDec]

-- DataType type definitions.  DataType "Arith" [...]
data DataTypeDec    = DataTypeDec  DataTypeRep [DataTypeDef] deriving (Typeable, Data, Show, Eq, Read)

-- TypeDef constructor [types]
data DataTypeDef = DataTypeDef String [DataTypeRep] deriving (Typeable, Data, Show, Eq, Read)

data DataTypeRep =
    TyCon String [DataTypeRep]
  | TyVar String
  deriving (Typeable, Data, Show, Eq, Read, Ord)


-- InterConv (from-type, to-type) variable rhs
type InjDecls = [InjDecl]
data InjDecl = InjDecl (DataTypeRep,DataTypeRep) Var Pattern
  deriving (Data, Typeable, Show, Eq)

data InterConv = InterConv InjDecls
  deriving (Data, Typeable, Show, Eq)


--  Plus ---> ([Annot, Expr, Term], Expr)
-- constructor ([subsequent fields' types], constructor's type)
type Cons2FieldsEnv = Map String ([DataTypeRep], DataTypeRep)

-- build env from constructors to their type fields.    Add ---> ([Arith, Arith], Arith)
buildCon2FieldsEnv :: DataTypeDecs -> Cons2FieldsEnv
buildCon2FieldsEnv datatypes = unions $ map refine1 datatypes
  where
    refine1 :: DataTypeDec -> Cons2FieldsEnv
    refine1 (DataTypeDec ty typedefs) = unions $ map (refine2 ty) typedefs
    refine2 :: DataTypeRep -> DataTypeDef -> Cons2FieldsEnv
    refine2 ty (DataTypeDef cons tyDefs) = singleton cons (tyDefs,ty)


-------------
isPrimTy :: String -> Bool
isPrimTy t = t `elem` primTypes

showLit :: Literal -> String
showLit (LitInt    i) = show i
-- showLit (LitFloat d) = show d
showLit (LitBool   b) = show b
showLit (LitChar   c) = show c
showLit (LitString s) = show s


mkTyRep :: String -> [DataTypeRep] -> DataTypeRep
mkTyRep str fields = TyCon str fields

-- print a type representation to its string representation.
-- there is no parenthesis in the outmost layer
printTypeRep :: DataTypeRep -> String
printTypeRep = printTypeRepOuter

printTypeRepOuter t =
  case t of
    TyCon conName []    -> conName
    TyCon conName ts    -> conName ++ " " ++ concatMap printTypeRepInner ts
    TyVar x -> x

-- print a type representation to its string representation.
printTypeRepInner :: DataTypeRep -> String
printTypeRepInner t =
  case t of
    TyCon conName []    -> conName
    TyCon conName ts    -> "(" +^+ conName +^+ concatMap printTypeRepInner ts +^+ ")"
    TyVar x -> x

printTypeRepWithParen = printTypeRepInner


-- how to handle parameterised datatypes ?
printTyAsName :: DataTypeRep -> String
printTyAsName (TyCon n ts) = n ++ foldr (++) "" (map printTyAsName ts)
printTyAsName (TyVar x) = x

printTyTag :: DataTypeRep -> String
printTyTag (printTyAsName -> s)     = s ++ "Tag"


addNullCase :: DataTypeRep -> String
addNullCase (TyCon n _) = n ++ "Null"

addComma :: [String] -> String
addComma [t] = t
addComma (t:ts) = t +^+ "," +^+ addComma ts

wrapParen :: String -> String
wrapParen s = "(" +^+ s +^+ ")"


pat2RegPat' pat = evalState (pat2RegPat pat) 0

-- change a pattern to its region pattern by reversing
-- WildP and VarP in it. change EAdd _ x y to EAdd a _ _
pat2RegPat :: Pattern -> State Int Pattern
pat2RegPat (ConP con rep fields) = do
  fields' <- mapM pat2RegPat fields
  return $ ConP con rep fields'
pat2RegPat (VarP  _ rep) = return $ WildP rep
pat2RegPat (LitP  l) = return $ LitP l
pat2RegPat (WildP rep) = do
  i <- get
  put (i+1)
  return $ VarP ("fromWild" ++ show i) rep

pat2TypeRep :: Pattern -> DataTypeRep
pat2TypeRep  (VarP  _ rep)   = rep
pat2TypeRep  (ConP  _ rep _) = rep
pat2TypeRep  (WildP rep)     = rep
pat2TypeRep  (LitP  lit) = case lit of
  LitString _ -> mkTyRep "String" []
  LitInt    _ -> mkTyRep "Integer" []
  -- LitFloat _ -> mkTyRep "Double" []
  LitBool   _ -> mkTyRep "Bool" []
  LitChar   _ -> mkTyRep "Char" []


isVarPat :: Pattern -> Bool
isVarPat (VarP _ _) = True
isVarPat _ = False


-- get all the types USED in the consistency relations
-- users define some parameterised types in the TypeDec part and use them
-- with concrete parameters in the consistency relations part
getTypes :: [Group] -> [DataTypeRep]
getTypes = List.nub . concatMap (\(Group (sTy,vTy) _) -> [sTy, vTy])


concatSpace :: [String] -> String
concatSpace [] = []
concatSpace (x:xs) = x ++ " " ++ concatSpace xs

infixr 5 +^+
(+^+) :: String -> String -> String
x +^+ y = x ++ " " ++ y

-- unsafe
fromJ :: String -> Maybe a -> a
fromJ errMsg ma = case ma of
  Nothing -> error errMsg
  Just a  -> a


infixr 5 `newlineSS`
newlineSS :: String -> String -> String
newlineSS s1 s2 = s1 ++ "\n\n" ++ s2

infixr 5 `newlineS`
newlineS :: String -> String -> String
newlineS s1 s2 = s1 ++ "\n" ++ s2

foldr1c :: (a -> b) -> (a -> b -> b) -> [a] -> b
foldr1c c _ [e] = c e
foldr1c c h (e:es) = h e (foldr1c c h es)
foldr1c _ _ [] = error "empty list fed to foldr1c"

foldl1c :: (a -> b) -> (b -> a -> b) -> [a] -> b
foldl1c c _ [e] = c e
foldl1c c h (e:es) = gg h (c e) es
  where gg h c1 [] = c1
        gg h c1 (e:es) = gg h (h c1 e) es
foldl1c _ _ [] = error "empty list fed to foldl1c"

isRight :: Either a b -> Bool
isRight (Left _)  = False
isRight (Right _) = True
