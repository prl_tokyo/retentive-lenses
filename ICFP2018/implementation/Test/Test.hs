module Test where

import Main
import Def
import Parser
import TypeInf
import GenProgram
import GenBX.GenPutWithLinks
import PrintDataTypes
import GenHelper.GenFetchable
import GenHelper.GenTypeable
import GenHelper.GenSelSPat
import GenHelper.GenRegPat
import GenHelper.GenRepSubtree
import GenHelper.GenInsSubtree
import GenHelper.GenInterConv

import Data.Maybe
import Data.Map hiding (map, take, null)
import Text.Parsec
import Text.Show.Pretty
import qualified Language.Haskell.TH as TH

import System.Directory
import Text.PrettyPrint

import System.Environment
import Control.Monad
import System.IO.Unsafe
import Debug.Trace

-- test "Test/spinetype.txt" "Test/spineprogram.txt"
-- test "Test/esopType.txt" "Test/esopProgram.txt"
-- test "Test/expType.txt" "Test/expProgram.txt"
test :: String -> String -> IO ()
test dFile crFile = do
  -- typeDefStr <- readFile typeDefFile
  -- progStr <- readFile progFile
  -- case parse pAllDatatypesAndInterConvs "TypeDefinitionFile" typeDefStr of
  --   Left  err -> pPrint err
  --   Right (tyDecs, intcvs) -> do
  (tyDecs, intcvs, crs__) <- parseAll dFile crFile
      -- putStrLn (ppShow intcvs)
      -- case parse pProgram "ConsistencyRelationFile" progStr of
      --   Left  err -> pPrint err
      --   Right groups__ -> do
          -- putStrLn (ppShow tyDecs)
  -- putStrLn (ppShow crs__)
  -- let env  = buildCon2FieldsEnv tyDecs
  let crs_ = addBases crs__
      crs = map (\g@(Group (sTy, vTy) _) -> applyTyAnno sTy vTy tyDecs g) crs_
      prog    = foldr1 newlineSS $ map TH.pprint (genProgram crs)
      regPatEnv = genRegPatEnv crs
  -- putStrLn prog
  writeFile "Test/BXDef.hs" $ foldr1 newlineSS
    [bxDefModHead
    ,prtAbsDTs tyDecs
    ,TH.pprint $ genTypeTagDef' crs
    ,genTagTypeableClass
    ,genTagTypeableInstances tyDecs crs

    -- ,genSPatTagTagTypeableInstances crs

    ,genFetchableClass
    ,genFetchableInstance tyDecs
    ,genFetchDispatches crs
    ,forTuple2Def
    ,forTuple3Def

    ,genAllInjsDef intcvs
    ,genSelSPatDef crs
    ,genAskDynTyTagDef crs

    ,spliceDef
    ,genRepSubtreeDef regPatEnv crs
    ,genInsSubtreeDef regPatEnv crs intcvs
    ,genRegPatDef crs
    ]

  writeFile "Test/BX.hs" $ foldr1 newlineSS
    [bxModHead
    ,prog
    ,topPut
    ]
