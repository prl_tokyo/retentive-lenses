data Arith = Add Arith Arith
           | Sub Arith Arith
           | Mul Arith Arith
           | Div Arith Arith
           | Num Integer
  deriving (Show, Eq, Data, Typeable)

data Expr =
    Plus   String Expr Term
  | Minus  String Expr Term
  | FromT  String Term
  deriving (Data, Typeable, Show, Eq)

data Term =
    Times      String Term Factor
  | Division   String Term Factor
  | FromF      String Factor
  deriving (Data, Typeable, Show, Eq)

data Factor =
    Neg   String Factor
  | Lit   String Integer
  | Paren String Expr
  deriving (Data, Typeable, Show, Eq)


#InterConvertible

[Factor, Term, Expr]
Factor -> Term    |through|  (FromF String Factor, [1])
Term   -> Expr    |through|  (FromT String Term, [1])
Expr   -> Factor  |through|  (Paren String Expr, [1])
