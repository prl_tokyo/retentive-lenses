-- test cases
putTest os links ast = put ExprTag ArithTag os (toDyn ast) links

eaddT1 = TF "" (Lit "comm2" 2)
eaddE1 = ET "" (TF "" (Lit "comm1" 1))
os1 = Plus "comm+" eaddE1 eaddT1
-- os1 = 1 "comm1" + "comm+" 2 "comm2"

links1 =
  [ ( ( S_ExprArithCase0 , [] ) , ( V_ExprArithCase0 , [] ) )
  , ( ( S_ExprArithCase2 , [ 1 ] ) , ( V_Unit , [ 0 ] ) )
  , ( ( S_TermArithCase2 , [ 1 , 1 ] ), ( V_Unit , [ 0 ] ))
  , ( ( S_FactorArithCase1 , [ 1 , 1 , 1 ] ), ( V_FactorArithCase1 , [ 0 ] ))
  , ( ( S_Integer , [ 1 , 1 , 1 , 1 ] ), ( V_Integer , [ 0 , 0 ] ))
  , ( ( S_TermArithCase2 , [ 2 ] ) , ( V_Unit , [ 1 ] ) )
  , ( ( S_FactorArithCase1 , [ 2 , 1 ] ), ( V_FactorArithCase1 , [ 1 ] ))
  , ( ( S_Integer , [ 2 , 1 , 1 ] ), ( V_Integer , [ 1 , 0 ] ))
  ]

ast1 = Add (Num 1) (Num 2)
ast1' = (Sub (Num 99) (Num 999))


tp1  = apFst (flip fromDyn ExprNull) (putTest os1 links1 ast1)
tp1' = apFst (flip fromDyn ExprNull) (putTest os1 links1 ast1') -- fail. links not valid



--------------------

os2 = Plus "comm+" eaddE2 eaddT2
eaddE2 = Minus "comm-" (ET "" (TF "" (Lit "comm0" 0))) (TF "" (Lit "comm1" 1))
eaddT2 = TF "" (Neg "neg" (Lit "comm2" 2))
-- os2 = 0 "comm0" - "comm-" 1 "comm1"    + "comm+"  -2 "comm2"

ast2  = Add (Sub (Num 0) (Num 1)) (Sub (Num 0) (Num 2))

-- getExprArith os2
getOS2ProducedLinks =
  [ ( ( S_ExprArithCase0 , [] ) , ( V_ExprArithCase0 , [] ) )
  , ( ( S_ExprArithCase1 , [ 1 ] ) , ( V_ExprArithCase1 , [ 0 ] ) )
  , ( ( S_ExprArithCase2 , [ 1 , 1 ] ) , ( V_Unit , [ 0 , 0 ] ))
  , ( ( S_TermArithCase2 , [ 1 , 1 , 1 ] ) , ( V_Unit , [ 0 , 0 ] ))
  , ( ( S_FactorArithCase1 , [ 1 , 1 , 1 , 1 ] ), ( V_FactorArithCase1 , [ 0 , 0 ] ))
  , ( ( S_Integer , [ 1 , 1 , 1 , 1 , 1 ] ), ( V_Integer , [ 0 , 0 , 0 ] ))
  , ( ( S_TermArithCase2 , [ 1 , 2 ] ), ( V_Unit , [ 0 , 1 ] ))
  , ( ( S_FactorArithCase1 , [ 1 , 2 , 1 ] ), ( V_FactorArithCase1 , [ 0 , 1 ] ))
  , ( ( S_Integer , [ 1 , 2 , 1 , 1 ] ), ( V_Integer , [ 0 , 1 , 0 ] ))

  , ( ( S_TermArithCase2 , [ 2 ] ) , ( V_Unit , [ 1 ] ) )
  , ( ( S_FactorArithCase0 , [ 2 , 1 ] ), ( V_FactorArithCase0 , [ 1 ] ))
  , ( ( S_FactorArithCase1 , [ 2 , 1 , 1 ] ), ( V_FactorArithCase1 , [ 1 , 1 ] ))
  , ( ( S_Integer , [ 2 , 1 , 1 , 1 ] ), ( V_Integer , [ 1 , 1 , 0 ] ))
  ]

-- swap left and right subtree (of the root node only) of ast2
ast2' = Add (Sub (Num 0) (Num 2)) (Sub (Num 0) (Num 1))

os2LinksSwapLR =
  [ ( ( S_ExprArithCase0 , [] ) , ( V_ExprArithCase0 , [] ) )
  , ( ( S_ExprArithCase1 , [ 1 ] ) , ( V_ExprArithCase1 , [ 1 ] ) )
  , ( ( S_ExprArithCase2 , [ 1 , 1 ] ) , ( V_Unit , [ 1 , 0 ] ))
  , ( ( S_TermArithCase2 , [ 1 , 1 , 1 ] ) , ( V_Unit , [ 1 , 0 ] ))
  , ( ( S_FactorArithCase1 , [ 1 , 1 , 1 , 1 ] ), ( V_FactorArithCase1 , [ 1 , 0 ] ))
  , ( ( S_Integer , [ 1 , 1 , 1 , 1 , 1 ] ), ( V_Integer , [ 1 , 0 , 0 ] ))
  , ( ( S_TermArithCase2 , [ 1 , 2 ] ), ( V_Unit , [ 1 , 1 ] ))
  , ( ( S_FactorArithCase1 , [ 1 , 2 , 1 ] ), ( V_FactorArithCase1 , [ 1 , 1 ] ))
  , ( ( S_Integer , [ 1 , 2 , 1 , 1 ] ), ( V_Integer , [ 1 , 1 , 0 ] ))

  , ( ( S_TermArithCase2 , [ 2 ] ) , ( V_Unit , [ 0 ] ) )
  , ( ( S_FactorArithCase0 , [ 2 , 1 ] ), ( V_FactorArithCase0 , [ 0 ] ))
  , ( ( S_FactorArithCase1 , [ 2 , 1 , 1 ] ), ( V_FactorArithCase1 , [ 0 , 1 ] ))
  , ( ( S_Integer , [ 2 , 1 , 1 , 1 ] ), ( V_Integer , [ 0 , 1 , 0 ] ))
  ]



-- put with identity links
(tp2Src, tp2VLinks) = apFst (flip fromDyn ExprNull) (putTest os2 getOS2ProducedLinks ast2)
testProperty2 =
  tp2Src == os2 &&
  sortBy cmpSPath (tp2VLinks `compSSSVs` getOS2ProducedLinks) == sortBy cmpSPath getOS2ProducedLinks


-- put with links meaning that the left and right subtrees of Sub (in the ast, root) changes
(tp3Src, tp3VLinks) = apFst (flip fromDyn ExprNull) (putTest os2 os2LinksSwapLR ast2)

-- the horizontal links produced from PutGet.
tp3PutGetProducedLinks = snd $ getExprArith tp3Src

os3 = Plus "comm+" (ET "" eaddT2) (TF "" (Paren "" eaddE2))
testProperty3 = -- triangle commute graph.
  sortBy cmpSPath (tp3VLinks `compSSSVs` tp3PutGetProducedLinks) == sortBy cmpSPath os2LinksSwapLR

-- also we have:
testSrcEquiv = fst (getExprArith tp3Src) == fst (getExprArith os3)



-- put without links
tp4 = apFst (flip fromDyn ExprNull) (putTest os2 [] ast2)

-- Plus "comm+" (ET "" (TF "" (Neg "" (Lit "comm2" 2))))
--      (TF "" (Paren "" (Minus "comm-" (ET "" (TF "" (Lit "comm0" 0))) (TF "" (Lit "comm1" 1)))))
