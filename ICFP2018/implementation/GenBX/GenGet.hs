{-# LANGUAGE RecordWildCards #-}

module GenBX.GenGet where

import qualified Def as D
import TypeInf
import THAuxFuns
import GenHelper.GenRegPat
import Data.Map as Map hiding (map, foldr)
import Language.Haskell.TH
import Control.Monad.State
import Control.Arrow ((***))
import Data.Maybe (maybe)

import Debug.Trace
import Text.Show.Pretty

genGroupGet :: Map D.Pattern D.RegPatRep -> D.Group -> [Dec]
genGroupGet sRegPatEnv D.Group{..}  =
  let funName = "get" ++ D.printTyAsName (fst gTy) ++ D.printTyAsName (snd gTy)
      hsSTy = var2T (D.printTypeRep (fst gTy)) -- source type in haskell type rep
      hsVTy = var2T (D.printTypeRep (snd gTy))
      sig = SigD (mkName funName)
              (hsSTy `mkArrTy` (TupleT 2 `AppT` hsVTy `AppT` var2T "[RLink]"))
  in  [sig, FunD (mkName funName) (map (genClauseGet sRegPatEnv gTy) gRules)]


genClauseGet :: Map D.Pattern D.RegPatRep -> (D.DataTypeRep,D.DataTypeRep) -> D.Rule -> Clause
genClauseGet sRegPatEnv (sTy,vTy) D.Rule{..} = Clause [pat] body decs
  where
    -- pat  = dslPat2HsPat (evalState (addRegions lhs) 0)
    pat  = dslPat2HsPat lhs
    body = NormalB (TupE [dslPat2HsExp rhs, lsE])
    decs = concatMap (genGetEachSubTree (lhs,rhs)) bases ++ [mkL0, hlUnion]

    lsE = simpleInfixE (var2E "l0") (var2E ":") (var2E "ls'")
    hlUnion = simpleValD (var2P "ls'")
              (var2E "concat" `AppE` subHLs bases)

    subHLs :: [((D.Var,D.DataTypeRep),(D.Var,D.DataTypeRep))] -> Exp
    subHLs = foldr (\((x,_),(_,_)) (ListE vls) -> ListE (var2E (x ++ "ls'"):vls)) (ListE [])

    mkL0 = simpleValD (var2P "l0") (mkHLink sRegPatEnv (lhs,rhs))
    -- mkL0 = simpleValD (var2P "l0") (mkHLink (sTy,vTy) lhs rhs tagEnv)


genGetEachSubTree :: (D.Pattern,D.Pattern) -> ((D.Var,D.DataTypeRep),(D.Var,D.DataTypeRep)) -> [Dec]
genGetEachSubTree (sPat,vPat) ((x,tyX) , (y,tyY)) =
  let getResDec = simpleValD (TupP [VarP (mkName y), VarP (mkName (x ++ "ls")) ])
                    (VarE (mkName $ "get" ++ D.printTyAsName tyX ++ D.printTyAsName tyY)
                                  `AppE` (VarE (mkName x)))

      preXDec = simpleValD (var2P ("pre" ++ x)) (listInt2Exp $ findVarPath x sPat)
      preYDec = simpleValD (var2P ("pre" ++ y)) (listInt2Exp $ findVarPath y vPat)

      xHL'Dec = simpleValD (var2P (x ++ "ls'"))
                (foldl1 AppE [var2E "map"
                             ,var2E "addPathH" `AppE` TupE [var2E ("pre" ++ x),var2E ("pre" ++ y)]
                             ,var2E (x ++ "ls")])
  in [getResDec,preXDec,preYDec,xHL'Dec]




-- source-pat -> view-pat -> HLink
mkHLink :: Map D.Pattern D.RegPatRep -> (D.Pattern, D.Pattern) -> Exp

-- imaginary link
mkHLink env (lhs, D.VarP _ _) =
  let sRegPat = D.fromJ "error in MkLink. 2" $ Map.lookup (normPattern lhs) env
      t1 = TupE [con2E sRegPat, con2E "[]"]
      t2 = TupE [con2E "Void", con2E "[]"]
  in  TupE [t1, t2]


-- normal link
mkHLink env (lhs, rhs) =
  let sRegPat = D.fromJ "error in MkLink. 1" $ Map.lookup (normPattern lhs) env
      vRegPat = D.fromJ "error in MkLink. 2" $ Map.lookup (normPattern rhs) env
      t1 = TupE [con2E sRegPat, con2E "[]"]
      t2 = TupE [con2E vRegPat, con2E "[]"]
  in  TupE [t1, t2]


-- no link
-- mkHLink env (sTy, vTy) = var2E "[]" -- or should we report an error ?


----------- get for primitive types
primGets :: [Dec]
primGets = map mkPrimGet D.primTypes

mkPrimGet :: String -> Dec
mkPrimGet ty = FunD (mkName' ty) [clause1]
  where mkName' n = mkName ("get" ++ n ++ n)
        clause1 = simpleClause [var2P "s"] (TupE [var2E "s", ListE [mkPrimLinkH ty]])

mkPrimLinkH :: String -> Exp
mkPrimLinkH ty = TupE [TupE [con2E (mkPrimPatReg ty), con2E "[]"]
                      ,TupE [con2E (mkPrimPatReg ty), con2E "[]"]]
------

-- top-level interface for "get". post-generate second-order properties after invoking the real get
topGet :: D.Group -> [Dec]
topGet D.Group{..}  =
  let funName = "topGet" ++ sTy ++ vTy
      hsSTy = var2T (D.printTypeRep (fst gTy)) -- source type in haskell type rep
      hsVTy = var2T (D.printTypeRep (snd gTy))
      (sTy, vTy) = (D.printTyAsName *** D.printTyAsName) gTy
      sig = SigD (mkName funName)
              (hsSTy `mkArrTy` (TupleT 2 `AppT` hsVTy `AppT` var2T "[HLink]"))
      body = NormalB (LetE [dec] (var2E "(view, toHLink rlinks)"))
      dec  = ValD (TupP [var2P "view", var2P "rlinks"]) decBody []
      decBody = NormalB $ var2E $ "get" ++ sTy  ++ vTy ++ " s"
  in  [sig, FunD (mkName funName) [Clause [var2P "s"] body []]]
