{-# LANGUAGE RecordWildCards #-}

module GenBX.GenPutNoLink where

import qualified Def as D
import Parser
import TypeInf
import Data.Map as Map (Map, lookup)
import THAuxFuns
import GenHelper.GenRegPat

import Language.Haskell.TH

-- generate functions for walking down the view and recursively calling the put on subtrees.
-- since both the case put-handling-links and put-not-handling-links will call this case,
-- we give it a name as a new function to make the execution process clearer.
putNoLinkSig :: Dec
putNoLinkSig =
  let sig = SigD (mkName "putNoLink")
              (var2T "OSTyTag" `mkArrTy` var2T "STyTag"  `mkArrTy` var2T "VTyTag" `mkArrTy`
                con2T "RegPat" `mkArrTy` var2T "OSDyn" `mkArrTy` var2T "VDyn"
                  `mkArrTy` var2T "[RLink]" `mkArrTy` var2T "SDyn")
  in  sig

genPutNoLinkDef :: [D.Group] -> [Dec]
genPutNoLinkDef grps =
  let sRegPatEnv = genRegPatEnv grps
  in  map (\ D.Group{..} -> FunD (mkName "putNoLink")
                                 (map (genPutNoLinkCase sRegPatEnv gTy) gRules)) grps



-- generate a function for walking down the view and recursively calling the put on subtrees.
genPutNoLinkCase :: Map D.Pattern D.RegPatRep -> (D.DataTypeRep,D.DataTypeRep) -> D.Rule -> Clause
genPutNoLinkCase sRegPatEnv (sTy,vTy) D.Rule{..} =
  Clause [var2P "osTy", con2P (D.printTyTag sTy), con2P (D.printTyTag vTy), var2P sRegPat
         ,var2P "osDyn", viewPV, var2P "env"] body decs
  where
    sRegPat = D.fromJ "error in genPutNoLinkCase. 1" $ Map.lookup (normPattern lhs) sRegPatEnv
    viewPV = ViewP (ParensE $ var2E ("fromDynamic :: Dynamic -> Maybe (" ++ D.printTypeRep vTy ++ ")"))
                   patV
    patV = ConP (mkName "Just") [dslPat2HsPat rhs]
    body = NormalB newS

    newS = var2E "toDyn" `AppE`
           dslPat2HsExp (addPreToVar "(fromJust " $ addPosToVar "Res)" lhs)
    decs = concatMap (genPutEachSubTree (lhs,rhs)) bases

    -- given a list of variables, generate a list containing variable names to vertical links
    -- e.g.:  [x ~ a, y ~ b, z ~ c] gives [xVl, yVl, zVl]
    subVLs :: [((D.Var,D.DataTypeRep),(D.Var,D.DataTypeRep))] -> Exp
    subVLs = foldr (\((x,_),(_,_)) (ListE vls) -> ListE (var2E (x ++ "vl'"):vls)) (ListE [])


-- group-view-type -> (lhs,rhs) of a declaration -> (a-var-in-lhs,a-var-in-rhs) -> [Dec]
genPutEachSubTree :: (D.Pattern,D.Pattern) ->
  ((D.Var,D.DataTypeRep),(D.Var,D.DataTypeRep)) -> [Dec]
genPutEachSubTree (lhs,rhs) ((x,tyXRep) , (y,tyYRep)) =
  let preXDec = simpleValD (var2P ("pref" ++ x)) (listInt2Exp $ findVarPath x lhs)
      preYDec = simpleValD (var2P ("pref" ++ y)) (listInt2Exp $ findVarPath y rhs)
      envXDec = simpleValD (var2P ("env" ++ x ++ y))  (genSubEnv y)
      -- (nullConX,nullConY) = (pprint (mkDefVal tyXRep) , pprint (mkDefVal tyYRep) )

      putResTyDec = SigD (mkName (x ++ "Res")) (var2T $ "Maybe " ++ D.wrapParen (D.printTypeRep tyXRep))
      putResDec = simpleValD (var2P (x ++ "Res")) (foldl1 AppE [var2E "fromDynamic", resE1])

      resE1 = foldl1 AppE [var2E "put", var2E "osTy"
                          ,con2E . D.printTyTag . D.fromJ "cannot find the type of source variable. 0xa0." $
                            findVarType x lhs
                          ,con2E . D.printTyTag . D.fromJ "cannot find the type of view variable. 0xa1." $
                            findVarType y rhs
                          ,var2E "osDyn", var2E "toDyn" `AppE` var2E y
                          ,var2E ("env" ++ x ++ y)]

  in [preXDec,preYDec,envXDec,putResTyDec,putResDec]


--
genSubEnv :: D.Var -> Exp
genSubEnv y = foldl1 AppE
  [var2E "map"
  ,var2E "delPathH" `AppE` TupE [var2E "[]", var2E ("pref" ++ y)]
  ,var2E "filterEnv" `AppE` var2E ("pref" ++ y) `AppE` var2E "env"]


--
genClausePutNoLink :: (D.DataTypeRep, D.DataTypeRep) -> D.Rule -> Clause
genClausePutNoLink  (sTy,vTy) D.Rule{..} =
  Clause [con2P sTyTag , var2P "sPat", originS , env, var2P "originalV"] body []
  where
    (originS , env) = (var2P "os", var2P "env")
    (sTyTag,vTyTag) = (D.printTyAsName sTy ++ "Tag",D.printTyAsName vTy ++ "Tag")
    patV = dslPat2HsPat rhs
    body = GuardedB [(guard, e)]

    -- possible cases of the guard
    guard = NormalG (con2E "True")
    e = foldl1 AppE $ map var2E ["put" ++ D.printTyAsName vTy ++ "Rec"
                                ,sTyTag,"sPat","os","env", "originalV"]


---------- put for primitive types
primPutNoLink :: [Dec]
primPutNoLink = concatMap genP D.primTypes
  where
    genP ty =
      let resTy = TupleT 2 `AppT` var2T "Dynamic" `AppT` var2T "SSLinks"
      in  [FunD (mkName "putNoLink") [mkPrimPutNoLinkRec ty] ]


mkPrimPutNoLinkRec :: String -> Clause
mkPrimPutNoLinkRec ty = simpleClause
  [WildP, con2P (ty ++ "Tag"), con2P (ty ++ "Tag"), WildP, WildP, var2P "dynV", WildP]
  (var2E "dynV")


mkPrimPutNoLink :: String -> Clause
mkPrimPutNoLink ty = simpleClause
  [con2P (ty ++ "Tag"), var2P "sPat", var2P "os", var2P "env", var2P "v"]
  (foldl1 AppE [var2E ("put" ++ ty ++ "Rec"), var2E (ty ++ "Tag")
               ,var2E "sPat", var2E "os", var2E "env", var2E "v"])


mkPrimLinkSS :: String -> Exp
mkPrimLinkSS s = TupE [con2E "[]" , con2E (mkPrimPatReg s) , con2E "[]"]
