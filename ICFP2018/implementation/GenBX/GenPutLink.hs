{-# LANGUAGE RecordWildCards #-}

module GenBX.GenPutLink where

import qualified Def as D
import Parser
import TypeInf
import THAuxFuns
import GenBX.GenPutNoLink
-- import GenHelper.GenPatTag (genPatTagEnv, pat2Str)
import Language.Haskell.TH

import Data.Map as Map hiding (map, foldl, foldr, filter, null, drop, partition)
import Data.List (nub)
import Control.Monad.State
import Control.Arrow

-- putWithLinks :: TyTag -> TyTag -> OS -> Dynamic -> Env -> (Dynamic, SSLinks)
genPutWithLinksSig :: Dec
genPutWithLinksSig =
  let resTy = TupleT 2 `AppT` var2T "Dynamic" `AppT` var2T "SSLinks"
      sig = SigD (mkName "putWithLinks")
              (var2T "TyTag" `mkArrTy` var2T "TyTag" `mkArrTy` var2T "OS"
                `mkArrTy` var2T "Dynamic" `mkArrTy` var2T "Env" `mkArrTy` resTy)
  in  sig

genPutWithLinksDec :: [Dec]
genPutWithLinksDec =
  [FunD (mkName "putWithLinks") [genClausePutLink]]

-- genPutWithLinksDec :: [D.Group] -> [Dec]
-- genPutWithLinksDec [] = []
-- genPutWithLinksDec (D.Group{..}:ds) =
--   FunD (mkName "putWithLinks") (map (genClausePutLink gTy) gRules) :
--   genPutWithLinksDec ds


genClausePutLink :: Clause
genClausePutLink =
  -- [source-type-tag, source-pattern-tag, origin-source, view-pattern, env]
  Clause [var2P "st", var2P "vt", var2P "os", var2P "dynV", var2P "env"] body decs
  where
    body = NormalB (TupE [var2E "s4", var2E "vls4"])
    decs = [getLinksD, anaRealD, spliceD, mkInjD]
    getLinksD   = simpleValD (TupP [var2P "maybeL", var2P "imags", var2P "env'"])
                    (var2E "getTopLinks" `AppE` var2E "env")

    -- case analysis on real link
    anaRealD = ValD (TupP [var2P "s2", var2P "vls2"])
                    (NormalB $ CaseE (var2E "maybeL") [hasReal, noReal]) []
    hasReal = Match (ConP (mkName "Just") [var2P "l"]) (NormalB hasRealBody) []
    hasRealBody = LetE [expandLD, fetchD, putRecD, vl0D] insSubE
    expandLD = simpleValD (TupP [TupP [var2P "sPath", var2P "sReg"]
                               ,TupP [var2P "[]", WildP]])  (var2E "l")
    fetchD = simpleValD (var2P "s0")
              (var2E "fetch" `AppE` var2E "sPath" `AppE` var2E "os")


    putRecD = simpleValD (TupP [var2P "s1", var2P "vls"]) putRecD2
    putRecD2 = var2E "putRec" `AppE` (ParensE $ var2E "askDynTag" `AppE` var2E "s0")
               `AppE` var2E "vt" `AppE` var2E "sReg" `AppE` var2E "os" `AppE`
               var2E "dynV" `AppE` var2E "env'"

    vl0D = simpleValD (var2P "vl0") (TupE [var2E "sPath", var2E "sReg", var2E "[]"])

    insSubE = TupE [foldl1 AppE $ map var2E ["repSubtree","sReg","s0","s1"]
                   , simpleInfixE (var2E "vl0") (var2E ":") (var2E "vls")]

    noReal  = Match (ConP (mkName "Nothing") []) (NormalB noRealBody2) []
    noRealBody2 = var2E "putRec" `AppE` var2E "st" `AppE` var2E "vt" `AppE`
                  selPat `AppE` var2E "os" `AppE`
                  var2E "dynV" `AppE` var2E "env'"

    selPat = ParensE $ var2E "selSPat" `AppE` var2E "st" `AppE` var2E "vt"
            `AppE` var2E "dynV"

    spliceD = simpleValD (TupP [var2P "s3", var2P "vls3"])
                (foldl1 AppE (map var2E ["foldr", "(splice os)", "(s2, vls2)", "imags"]))

    mkInjD = simpleValD (TupP [var2P "s4" , var2P "vls4"])
             (foldl1 AppE [var2E "mkInj", var2E "st"
                          ,var2E "(askDynTag s3)"
                          ,TupE [var2E "s3" , var2E "vls3"]])
