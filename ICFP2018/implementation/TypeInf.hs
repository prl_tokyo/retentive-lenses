{-# LANGUAGE RecordWildCards #-}

module TypeInf where

import Def
import Data.Map as Map hiding (map)
import Data.Maybe (catMaybes)
import Control.Monad.Reader
import Data.Generics

import Text.Show.Pretty
import Debug.Trace

-----------------------------------------------

applyTyAnno :: Data a => DataTypeRep -> DataTypeRep -> DataTypeDecs -> a -> a
applyTyAnno grpSTy grpVTy tyDecs =
  everywhere' (mkT (addTypeAnno' grpSTy grpVTy tyDecs))


addTypeAnno' :: DataTypeRep ->     -- (group) source type
                DataTypeRep ->     -- (group) view type
                DataTypeDecs ->    -- Type declarations
                Rule ->            -- data to be processed
                Rule
addTypeAnno' sTy@(TyCon sTyName _) vTy@(TyCon vTyName _) tyDecs Rule{..} =
  let substL = produceSubst sTy tyDecs
      tyDecL = applyTyVarSubsts' sTyName substL tyDecs
      envL   = buildCon2FieldsEnv tyDecL

      lhs'   = if isVarPat lhs
                  then handleVarP lhs sTy
                  else addTypeAnno tyDecs (lhs, sTy)

      substR = produceSubst vTy tyDecs
      tyDecR = applyTyVarSubsts' vTyName substR tyDecs
      envR   = buildCon2FieldsEnv tyDecR

      rhs'   = if isVarPat rhs
                  then handleVarP rhs vTy
                  else addTypeAnno tyDecs (rhs, vTy)
  -- in  trace (ppShow lhsA ++ "\n" ++ ppShow rhsA)
  in  trace (ppShow lhs')
      $ Rule lhs' rhs' (updateBasis [lhs',rhs'] bases)


-- substitutions of type variables to concrete data types
-- the first argument is the concrete data type appeared in consistency relations (e.g. source type)
-- the second argument is the data type declarations user defined before
produceSubst :: DataTypeRep -> DataTypeDecs -> [(String,DataTypeRep)]
produceSubst (TyCon tyName []) _ = []
produceSubst (TyCon tyName ts) decs =
  let vars = findTyVars tyName decs
  in  zip vars ts

findTyVars :: String -> DataTypeDecs -> [String]
findTyVars tyName [] = []
findTyVars tyName (DataTypeDec (TyCon tyName' vars) _ : decs) =
  if tyName == tyName'
    then map (\(TyVar v) -> v) vars
    else findTyVars tyName decs

-- instantiate parameterised type variables to concrete types and update the DataTypeDec
-- the first argument is a list of pair of (Parameterised-TyVar, Concrete-Types)
applyTyVarSubsts :: [(String, DataTypeRep)] -> DataTypeDec -> DataTypeDec
applyTyVarSubsts [] dec = dec
applyTyVarSubsts (subst:substs) dec@(DataTypeDec (TyCon tyName _) _) =
  applyTyVarSubsts substs $ everywhere' (mkT (applyOneSubst tyName subst)) dec
  where
    applyOneSubst :: String -> (String, DataTypeRep) -> DataTypeRep -> DataTypeRep
    -- applyOneSubst tyName (v, new) (TyCon tyName' pars) = TyCon tyName'
    --   (if tyName == tyName'
    --     then map (\(TyVar v') -> if v == v' then new else TyVar v') pars
    --     else pars)
    applyOneSubst _ (v, new) tv@(TyVar v') | v == v' = new
    applyOneSubst _ _ tyRep = tyRep


-- the substitution for one data type, replace type variables with concrete data types
-- e.g. user may define : data List a = ... ; data Tree a = ...
-- we only want to replace "a" with Int in the data type List but not Tree
applyTyVarSubsts' :: String -> [(String, DataTypeRep)] -> [DataTypeDec] -> [DataTypeDec]
applyTyVarSubsts' _ _ [] = []
applyTyVarSubsts' tyName substs (dec@(DataTypeDec (TyCon tyName' vars) _) : decs) =
  if tyName == tyName'
    then applyTyVarSubsts substs dec : decs
    else dec : applyTyVarSubsts' tyName substs decs


applyTyVarSubstsAndSelect :: String -> [(String, DataTypeRep)] -> [DataTypeDec] -> DataTypeDec
applyTyVarSubstsAndSelect _ _ [] = error $ "impossible. type variable substitutions failed. check if there are typos"
applyTyVarSubstsAndSelect tyName substs (dec@(DataTypeDec (TyCon tyName' vars) _) : decs) =
  if tyName == tyName'
    then applyTyVarSubsts substs dec
    else applyTyVarSubstsAndSelect tyName substs decs


updateBasis :: [Pattern] ->
  [((Var,DataTypeRep),(Var,DataTypeRep))] -> [((Var,DataTypeRep),(Var,DataTypeRep))]
updateBasis pats bases = map go bases
  where
    go ((vL,_),(vR,_)) =
      let [tyL] = catMaybes $ map (findVarType vL) pats
          [tyR] = catMaybes $ map (findVarType vR) pats
      in  ((vL,tyL),(vR,tyR))


-- non borderline cases
addTypeAnno :: DataTypeDecs ->
               (Pattern, DataTypeRep) ->         -- data to be processed
               -- Cons2FieldsEnv ->  -- env from constructor to ([subsequent field], type)
               -- Maybe String ->    -- constructor of a data type. E.g.: the “Add” in “Add lhs rhs”.
               --                    -- to be used for variable pattern.
                                  -- set to Nothing if it is boaderline cases.
               Pattern
addTypeAnno tyDecs (ConP consName _ pats , ty@(TyCon tyName _)) =
  -- produce and apply type varaible substitutions and build new env
  let subst   = produceSubst ty tyDecs
      tyDecs' = applyTyVarSubsts' tyName subst tyDecs
      env'    = buildCon2FieldsEnv tyDecs'
  -- lookup pats' types in the new env and recursively invoking addTypeAnno
      subTys' = fst . fromJ "impossible. 0xC14." $ Map.lookup consName env'
      pats'   = map (addTypeAnno tyDecs') (zip pats subTys')
  in  ConP consName ty pats'

addTypeAnno tyDecs (VarP var _ , ty) = VarP var ty

addTypeAnno tyDecs (WildP _ , ty) = WildP ty

addTypeAnno _ (litP, _) = litP



-- the case when the view pattern is merely a variable and whose type should be
-- referred to the group view type
handleVarP :: Pattern     ->   -- data to be processed
              DataTypeRep ->   -- the type of the view (type of the group of actions)
              Pattern

handleVarP (VarP uv' _) vt = VarP uv' vt
handleVarP pat _  = pat



------------------------ lookup
findVarType :: Var -> Pattern -> Maybe DataTypeRep
findVarType var (ConP var' ty subpats) =
  if var == var'
    then Just ty
    else case catMaybes (map (findVarType var) subpats) of
          [ty] -> Just ty
          _:_  -> error $ "variable " ++ var ++ " occurred more than once in the view pattern"
          []   -> Nothing

findVarType var (VarP var' ty) = if var /= var' then Nothing else Just ty
findVarType _ _ = Nothing

-----------
findVarPath :: Var -> Pattern -> [Int]
findVarPath v p = case findVarPath' v p [] of
  Nothing   -> error $ "variable " ++ v ++ " does not exist in pattern" ++ ppShow p
  Just path -> path

findVarPath' :: Var -> Pattern -> [Int] -> Maybe [Int]
findVarPath' var (ConP _ _ subPats) p =
  let subPaths = zipWith (\a b -> a ++ [b]) (repeat p) [0,1..]
      subs     = zip subPats subPaths
  in  case catMaybes (map (\(pat,path) -> findVarPath' var pat path) subs) of
        [p'] -> Just p'
        _:_  -> error $ "variable " ++ var ++ " occurred more than once in a pattern."
                         ++ " In function findVarPath'"
        []   -> Nothing

findVarPath' var (VarP var' _) p = if var /= var' then Nothing else Just p
findVarPath' _ _ _ = Nothing
