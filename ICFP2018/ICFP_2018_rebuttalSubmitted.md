We thank all the reviewers for their effort spent in giving us plenty of advice.

# Main concerns

We address three main concerns about this research:

1. why the theory doesn't include vertical links between the old and new views, and how links between the source and view are tracked as the view is edited;
2. why it is beneficial to build an abstract theory of "properties";
3. the expressiveness of the DSL and the scale of the case studies.

## 1

Including vertical links (which represent (view) updates) in the theory was something we thought about (for quite some time), but eventually we opted for the current, simpler theory. The rationale is that the original state-based lens framework (which we extended) doesn't really have the notion of view-updating built in. A view given to `put` is not necessarily modified from the view got from the source — it can be constructed in any way, and put doesn't have to consider how the view is constructed. We retain this separation of concern in our framework, in particular separating the jobs of retentive lenses and "third-party tools" that operate in a view-update setting: third-party tools are responsible for producing vertical links between the consistent view and a modified view (not between sources and views), and when it is time to run `put`, the vertical links are composed (as shown in Figures 8 and 10) with the consistency links produced by `get` to compute the input links between the source and the modified view. And it's feasible to produce vertical links in *specific* scenarios: in refactoring, for example, a refactoring tool knows it's moving code fragments around and can produce vertical links accordingly. We will expand on these in the revision.

## 2

The abstract theory was developed to bring out the essence of how hippocraticness arises as a special case of retentiveness.
Requiring a region to be kept in the new source is abstracted as requiring a property to be retained, and when enough properties are retained, the new source will be sufficiently characterised to be the same as the old one. It is this general structure that gives rise to hippocraticness, not anything specific about the region model.

## 3

It is as Reviewer B says: "for the applications the paper targets, such simple transformations between trees suffice in many cases". One important application we target is refactoring, and we have thought about the 23 refactoring operations provided by Eclipse on Java 8. These operations can be thought of as combinations of four basic operations: substitution, movement, deletion, and insertion, all of which can be handled in the region model.

We intend this paper to provide mainly a theoretic foundation. Based on our experience with an earlier implementation [Zhu et al. 2016], we have identified some practical problems (e.g., dealing with ambiguous grammars) that are independent from this work. We're thus leaving the development of a practical tool to future work, and it is premature to do large-scale experiments before having such a tool.


# Detailed response (exceeding the 500 word limit)

## For Review #98A

> Paper summary
>
> Proposes a refinement of the laws for bidirectional transformations (lenses) requiring that "small changes to the view get mapped to small changes to the source."

More precisely, it's about "unchangedness" rather than change. We described retentiveness informally as "if parts of the view are unchanged then the corresponding parts of the source should be retained as well" (89–91).

> 306: This seems a little underspecified, since you haven't said what are the sets from which the p and q properties are drawn (or even said what I think is true, that these sets are parameters to the definition). Moreover (maybe you're going to get to this later) there is a big question about how this relation is represented concretely, and how the get function actually computes it!

$P$ and $Q$ are the sets of properties on the source type $S$ and view type $V$ respectively. We assumed that this definition follows the paragraph above, and omitted some explanations, which we will add. At this point the formulation is still entirely mathematical; a concrete representation of the relation (links) is defined only later in Section 3.3.2 and 3.4.1.

> 584: "it is a bit tricky for keeping retentiveness compared with traditional approaches" -- ???. (In particular, which traditional approaches?)

We want to say that, since our put needs to create a new source satisfying retentiveness, its behaviour is a bit tricky and requires careful thought, in contrast to traditional approaches (in the sense of implementing the traditional definition of lenses), which do not need to satisfy retentiveness formally.

> Overall hesitation: There is something a bit funny about using a program to define what it means for that very program to be retentive! It feels like a shame that the definition of retentiveness isn't independent of the program. Would this not make sense? Some discussion of this issue seems important.

Sorry for the confusion, but the definition of retentiveness is independent of the program.

> 643: The restrictions on patterns feel a bit like the typing restrictions on the sum lens in Foster et al. Any comments?

If you are asking about the conditional tree lenses, the answer is probably yes. For example, we want to enforce that get will select the same "branch" (pattern) after put succeeds.

> 1091: The discussion of matching lenses is much too shallow. The description of matching lenses is reasonable, but there is no meaningful comparison to the present proposal. This seems a shame, as matching lenses are a concrete and practical instance of some very related concepts. (The comparison to container-based approaches is more substantial, and I think the criticism of those approaches is fair.)

You're right; we didn't include a proper comparison, which will probably require some notion of retentive lens combinators and how matching lenses can be recast as such combinators.

> The discussion of composability restrictions reminds me of a similar restriction on quotient lenses. It would be worth citing that discussion.

Thanks. We did not notice it before. It seems that similar problem indeed occurs there: "to check that an instance of sequential composition l;k is wellformed, we need to verify that l's abstract equivalence relation and the k's concrete one are identical."

> QUESTIONS:
>
> I think the answer to this is yes, but I want to double-check (because too many papers on lenses silently choose No!). Your get and put functions are total (on the specified types), right?

Yes. For the theory of retentiveness, triple spaces are used to guarantee that put is total.

> Can you prove that some "standard" lens language, e.g. Boomerang, is retentive? (I know you proved that all classic well-behaved lenses are retentive [407-425], but only in a trivial sense. My intuition is that it should be true in a more interesting sense also.)

This looks like an interesting project. The existing state-based lenses still strive to retain original source information. Given a source and a view, we can represent what a state-based put retains explicitly as a set of links between the source and view. And then we can lift the lens to a retentive one by generating such links from any input source and view before processing the source and view by the original put, and prove that the result of the lifting is indeed retentive. (And get should be enriched to produce appropriate consistency links too.)

> 220-224: How is this accomplished, concretely? (This is discussed later, but it would be good to foreshadow it a bit here.)

(Addressed as the first main concern.)

> Does the term "retentive" have anything to do with the "retentive sum lens" of Hofmann, Pierce, and Wagner? If not, perhaps it would avoid confusion to choose an adjective that doesn't already have an established meaning in this literature?

Both the use of "retentive" here and there are the same: retentive lenses try to keep more information compared with "forgetful" ones.
Here, the information is from the original source;
there, the information is from the complements of the sum.
We should have cited that paper, thanks.


## For Review #98B

> It is mentioned in the abstract and introduction that existing BX laws only govern the case where there is no change to the view. While understanding the point it is making, I have to say that the statement is very inaccurate. The Correctness law does require certain source information to be retained.

There appears to be some misunderstanding, as Correctness only requires that changes need to be made so that the new source is consistent with the view. These changes can be exclusively new information propagated from the view, but not retained from the old source.

For a concrete example, suppose `get` is the first-projection on a tuple: `get (1, "a") = 1`, and `get (2, "b") = 2`.
Then both `put1 (1, "a") 2 = (2, "a")`, and `put2 (1, "a") 2 = (2, "")` satisfy Correctness, while `put2` retains nothing from the old source.

> I also do not see the point of triple space. Isn't it just a triple? Or there is something deep that justifies such an elaborate treatment?

Triple space is defined in Definition 2.3. It is a subset of the triples of source, view, and links on which `put` is defined. And explanation is given above the definition (313–321).

> In Definition 2.6, it says that get is a subset of triple space. What does it mean?

As explained in Section 2.2.1, get is implicitly promoted to a relation, and the inclusion is the ordinary relational inclusion.

## For Review #98C
 
> Most of the paper is about the retentiveness criterion, but fails to explain adequately how that addresses problems past approaches haven't. I can see how the "globalness" of Hippocractiness is a problem, and how retentiveness is "less global", but how that is relevant for constructing lenses is not apparent from the paper.

We're not sure what you mean by relevance to lens construction. We wish that lenses could in addition satisfy retentiveness, rather than the basic hippocraticness.

> The formalism seems worthwhile, but its connection to the retentiveness criterion is tenuous. The "examples" section immediately introduces an additional concept ("vertical links"), which should have founds its way into the retentiveness criterion. The examples themselves are too small to successfully make the case of the formalism, and aren't compared with solutions to the same problems with other formalisms.

(See the response to the main concerns.)

> 208: The numbering seems odd - if why cst_3, not cst' corresponding to ast'

This $\mathit{cst}_3$ here is the visualisation of the $\mathit{cst}_3$ in Figure 2. All of $\mathit{cst}_1$, $\mathit{cst}_2$, $\mathit{cst}_3$, and $\mathit{cst}_4$ correspond to $\mathit{ast}'$.

> 154: Why is "Num 0" missing in cst? (Same in the text.)

`Num` is one of the constructs of the abstract syntax, while the corresponding construct in the concrete syntax is `Lit`.

> 214: The criterion seems insufficient for addressing the primary use case of bidirectional programming - when the view changes. With this, the entire rationale for rentiveness falls down, and you later posit an additional criterion ("vertical links") in the examples section that does just that

At line 214, it is a figure. But we believe we've answered this question in the main concerns section.

> 267: The typography is really confusing - some of the subsubsections are numbered, whereas others aren't.

Some are subsubsections (which are numbered), while others are paragraphs (unnumbered).

> 267: At this point, I have only a hazy notion of what a region is - is it a pattern + path? The paths seem to disappear at some point and get re-introduced later.

Yes, a region is a pattern plus a path.

> 270: What if a fragment got deleted? What if two get swapped?

In the first case, the corresponding link is also deleted. The second case is our example in the introduction.

> 280: "family of sets": The definition only seems to have a set?

Sorry, we should not have included "given a tree t" at line 279. (It should have been "a family of sets indexed by t".)

> 445: The use of Arith here is confusing, as you note yourself. The child construct appears out of thin air - what's the property language exactly?

We use the type `Arith` here for illustration, because it is simpler than the mutual recursive data type `Expr` and `Term` and the tree can be smaller.

The property language here consists of patterns, Top(x) for all variable names x, and similarly Child(pat,x,y,z).

> 569: So it seems you can only handle structural recursion - true?

Yes. Addressed in the main concerns section (regarding expressiveness).

> 572: I'm confused at this point - what's the DSL construct? The inference rule or the pattern correspondence? It's the latter, so what's the point of the former?

Sorry for not making it clear. The DSL constructs are "induction rules", which you call pattern correspondences.
Expanding them into inference rules is just an attempt to make the semantics of induction rules clearer.

> 634: Why is the restriction necessary? It seems to make many programming tasks awkward.

It simplifies the language and its presentation.

> 639: "respect relative positions": What does that mean?

We suppose you mean line 689. It means that, for example, if region A in the original source is above region B, then in the new source region A is still above region B.

> 690: The "S" in SPat seems to refer to the source type - shouldn't it be Pat S or something?

We suppose you mean line 699. We don't assume that Pat can be defined as a family of types.

> 821: So if the second-order properties are important, why do they get added as an afterthought?

Because second-order properties are built upon first-order properties (precisely, and their proofs), so they are indeed added afterwards.
In addition, second-order properties are so important to the theorem while not so important to this particular implementation, because this particular implementation respects all the possible second-order properties, as explained in the paper.

> 1068: How do comments and layout get preserved?

As Fig.1 illustrates, CST nodes contain annotations (comments and possibly layouts). By retentiveness, if the node in the new source comes from the old source, the annotation information is also preserved, although it is not in the AST.
Note that concrete syntax trees (parse trees) indeed correspond to program text.

> 1089: Why is "operational semantics" a problem

Because operational semantics describes how to do a job, rather than the result after doing a job. Here, we want to say that it does not exhibit the retention of information clearly.

> 1104: "would hinder": Well, does it hinder or not?

We believe it hinders, as the immediate example shows.

> 1118: "imagine": I don't get the point - you have a DSL that gets elaborated into Haskell. You could easily elaborate into those parameters.

In the DSL the data types are written by hand.

> 1148 "tend to represent": do they or don't they?

We assume you mean 1184.
The paper by Diskin et al. gave only an abstract framework without saying exactly how it's supposed to be instantiated. In one subsection, they discuss potential representation of deltas, which are edits. So we believe they tend to do so.

> 1247: It's unclear what that final sentence means - it would require drastic changes to cater to parameterised types? Or these changes are straightforward but drastic?

The first one. Although the user can define (simple) parameterised data types, their usage in the program part must be instantiated with concrete type parameters. The current syntax of the DSL seems impossible to fully support parameterised data types (so as to generate higher-order lenses).


## For Review #98D
> Paper summary

> Rating rationale and comments for author
> 
> By and large the design of retentive lenses seems quite elegant and well thought out. I encourage you to make a serious effort to address the issues with composability.

Yes. It's not trivial though; like what Reviewer A pointed out, this is a problem also encountered in another formalism (quotient lenses).

> Do you have a prototype implementation? I would have been more convinced if you had included evidence and experience of having built some large examples using retentive lenses.

Yes, we have a prototype implementation in Haskell and we describe the code generation in the appendix. However, we currently do not have large examples using that DSL, with reasons explained in the main concerns section.

> 159: What do you mean by "in order"?

Just one after the other... We first present the syntax, and then present the semantics.

> 1188: What does "proper" mean?

This is probably not a right choice of word. What we wanted to say was that it might be possible to enhance Diskin et al.'s formalism with retentiveness. But to do so, we should instantiate deltas as some "links" recording unchanged parts, rather than as edits recording changed parts (as what they tend to do in their paper).