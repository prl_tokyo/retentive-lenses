% !TEX root = retentive.lhs

\section{A Semantic Framework of Retentive Lenses}
\label{sec:framework}

In this section we will develop a definition of retentive lenses, where we enrich the pair of functions in lenses and formalise the statement ``if parts of the view are unchanged then the corresponding parts of the source should be retained'' in a more abstract framework, and show that Hippocraticness becomes a consequence~(\autoref{subsec:abstract_framework}).
Before that, we will start from an intuitive description of a ``region model'' for tree transformations, and how a retentive lens operates on this model.

\subsection{The Region Model}
\label{subsec:intuitive_retentiveness}

\begin{figure}
\includegraphics[scale=0.35]{pics/get_and_swap.pdf}
\caption{Regions and consistency links.}
\label{fig:get_and_swap}
\end{figure}

\begin{figure}
\includegraphics[scale=0.35]{pics/swap_and_put.pdf}
\caption{The triangular guarantee.}
\label{fig:swap_and_put}
\end{figure}

As mentioned in \autoref{sec:intro}, the core idea of retentiveness is to use links to relate parts of the source and view.
For trees, an intuitive notion of a ``part'' is a \emph{region}, by which we mean a top portion of a subtree that can be described by a \emph{region pattern} that consists of variables, constructors, and constants.
For example, in \autoref{fig:get_and_swap}, the grey areas are some of the possible regions.
The topmost region in |cst|, for example, is described by the region pattern `|Plus "..." a b|', which says that the region includes the |Plus| node and the annotation |"..."|, i.e., the first subtree under |Plus|; the other two subtrees (with roots |Minus| and |Neg|) under |Plus| are not part of the region, but we give them names |a|~and~|b| (which will be used in \autoref{subsec:second-order}).
This is one particular way of decomposing trees, and we call this the \emph{region model}.

Within the region model, the |get| function in a retentive lens not only computes a view but also decomposes both the source and the view into regions and produces a set of links relating source and view regions.
We call this set of links produced by |get| the \emph{consistency links}, because they are a finer-grained representation of how the whole source is consistent with the whole view.
In \autoref{fig:get_and_swap}, for example, the red dashed lines are two of the consistency links between the source |cst| and the view |ast = getE cst|.
With this particular |getE| function, the topmost region of pattern `|Plus "..." a b|' in |cst| corresponds to the topmost region of pattern `|Add a b|' in |ast|, and the region of pattern `|Neg "..." a|' in the right subtree of |cst| corresponds to the region of pattern `|Sub (Num 0) a|' in |ast|.
Note that, for clarity, \autoref{fig:get_and_swap} does not show all consistency links --- the complete set of consistency links should fully describe how all the source and view regions correspond.

As the view is modified, the links between the source and view can also be modified to reflect the latest correspondences between regions.
For example, if we change |ast| in \autoref{fig:get_and_swap} to |ast'| in \autoref{fig:swap_and_put} by swapping the two subtrees under |Add|, then the link relating the `|Neg "..." a|' region and the `|Sub (Num 0) a|' region is also maintained to record that the two regions are still ``locally consistent'' despite that the source and view as a whole are no longer consistent.

When it is time to put the modified view back into the source, the links that remain between the source and the view can provide valuable information regarding what should be brought from the old source to the new source.
The |put| function of a retentive lens thus takes a set of links as additional input, and retains regions in a way that respects the links.
More precisely, |put| should provide what we might call the \emph{triangular guarantee}:
Using \autoref{fig:swap_and_put} to illustrate, if a link is provided as input to |put|, say the one relating the `|Neg "..." a|' region in |cst| and the `|Sub (Num 0) a|' region in |ast'| , then after updating |cst| to a new source |cst_3| (which is guaranteed to be consistent with |ast'|), the consistency links between |cst_3| and |ast'| should include a link that relates the `|Sub (Num 0) a|' region to some region in the new source |cst_3|, and that region should have the same pattern as the one specified by the input link, namely `|Neg "..." a|', preserving the negation (as opposed to changing it to a |Minus|, for example) and the associated annotation/comment.


\subsection{Formalisation of Retentive Lenses}
\label{subsec:abstract_framework}

%\todo{Summary/roadmap. Rough types of |get| and |put|. We talk mainly about regions, but build some abstraction to facilitate understanding of retentiveness and the proof of GetPut. Two objectives: formalising the ``triangular guarantee'' and proving GetPut. (Developing the definition of retentive lenses informally in the text, so that we can walk through the auxiliary definitions and eventually reach the main definition.)}

Recall that a classic lens~\citep{foster2007combinators} is a pair of functions |get : S -> V| and |put : S -> V -> S| satisfying two well-behavedness laws, Correctness |get (put s v) = v| and Hippocraticness |put s (get s) = s|.
The aim of this section is to make the following revisions to the definition:
\begin{itemize}
\item extending |get| and |put| to incorporate links --- specifically, we will make |get| return a set of consistency links and |put| take a set of input links as an additional argument --- and
\item replacing Hippocraticness with a finer-grained law that captures the ``triangular guarantee'' described in \autoref{subsec:intuitive_retentiveness} and subsumes Hippocraticness.
\end{itemize}
We will start by introducing some mathematical notations we use in this paper.

%We have seen that sources and views should be decomposed into small parts so that links can relate them.
%Let us call these small parts \emph{regions}.
%Starting from these decompositions, in this section we will develop a theoretical framework for retentive lenses and show that how hippocraticness is derived from retentiveness.
% and later show that links are one of the concrete instantiations.
% To be concise, let us call a set of regions and relations among regions a set of \emph{properties}.

\subsubsection{Preliminaries}

In this section and the next we use sets, total functions, and relations as our mathematical foundation.
We will write $r : A \sim B$ to denote that r is a relation between sets $A$~and~$B$, i.e., $r \subseteq A \times B$.
Given a relation $r : A \sim B$, its ``left domain'' $\ldom\;r$ is the set $\{\, x \in A \mid \exists y \in B.\; (x, y) \in r \,\}$, and its converse $r^\circ : B \sim A$ relates $b \in B$ and $a \in A$ exactly when $r$~relates $a$~and~$b$.
The composition $l \cdot r : A \sim C$ of two relations $l : A \sim B$ and $r : B \sim C$ is defined as usual: $(x, z) \in l \cdot r$ exactly when there exists~$y$ such that $(x, y) \in l$ and $(y, z) \in r$.
We will allow functions to be implicitly lifted to relations: a function $f : A \to B$ also denotes a relation $f : B \sim A$ such that $(f\,x, x) \in f$ for all $x \in A$.
This flipping of domain and codomain (from $A \to B$ to $B \sim A$) makes functional composition compatible with relational composition: a function composition $f \circ |g|$ lifted to a relation is the same as $f \cdot |g|$, i.e., the composition of |f|~and~|g| as relations.

To allow more precision, we will use some notations from dependent type theory.
Let $P$~be a family of sets indexed by~$A$, and $P(x)$ the set at index $x \in A$ in the family.
We write $(x \in A) \to P(x)$ to denote the set of dependent functions~$f$ that maps every $x \in A$ to $f\,x \in P(x)$.
Also we write $(x \in A) \times P(x)$ to denote the set of dependent pairs $(x, y)$ where $x \in A$ and $y \in P(x)$.

\subsubsection{Abstracting Links.}
\label{subsubsec:abstraction}

We will develop a series of definitions abstracting region patterns, regions, links, and sets of links, so as to arrive at a framework that does not specifically talk about the region model but still captures the essence of retentiveness.
We will then be able to enrich |get| and |put| to make them handle links.

\paragraph{From region patterns to properties.}

A rough reading of the triangular guarantee described in \autoref{subsec:intuitive_retentiveness} is that a fragment in the old source linked to the view should also appear in the new source.
An alternative and more abstract way of understanding this is that we are preserving a kind of property that says ``a tree includes a specific region pattern'': if an old source has a fragment matching a region pattern, then the new source should also have a fragment matching the same region pattern.
More generally, there may be other kinds of property that we want to preserve.
(We will see another kind of property in \autoref{subsec:second-order}.)

Intuitively, a property is a description that can be satisfied.
Below is a more proof-relevant formulation that we will need:
\begin{definition}[properties]
\label{def:properties}
A \emph{property} on a set~$X$ is a value ~$p$ equipped with a family of sets $x \models p$ where $x$~ranges over~$X$.
\end{definition}

Inhabitants of $x \models p$ are considered proofs that $x$ satisfies~$p$.
For example, given a tree~$t$, a region pattern |pat| (\autoref{subsec:intuitive_retentiveness}) can be regarded as a property by equipping it with a family of sets:
\[ t \models |pat| \quad=\quad (|path| \in |Path|) \times |LeadToRegion|(t, |path|, |pat|) \]
where $|LeadToRegion|(t, |path|, |pat|)$ is the set of proofs that |path| is a valid path from the root of~$t$ to a sub-tree which matches |pat|.

%\begin{example}
%\todo{revise}
%For our running example as shown in \autoref{fig:running_example}, there are two types |Expr| and |Term|.
%Let us take |Expr| for the set~|X| and |CST| for~|x|.
%Let |p| be a predicate (property) that |CST| consists of a region |Plus a _ _|\,. Then a proof of type $x \models p$ can be represented as a path in |CST| pointed to a subtree matches the pattern |Plus a _ _|\,. In this case, the proof is simply an empty path pointing to the root of |CST|.
%Let $P$ be a singleton |{p}| and we have $x \models P$
%\end{example}

\paragraph{Regions as properties with proofs.}

We have seen that region patterns can be cast as properties: given a region pattern, a proof of the corresponding property is a path pointing to a region described by the region pattern.
Now consider regions, which are fragments matching a region pattern at a specific location.
This suggests that a region corresponds to a pair of a property and a proof that the property is satisfied.
For brevity, we give a definition of the sets of such pairs of property and proof:
\begin{definition}
Let $P$~be a set of properties on~|X| and $x \in X$. Define:
\[ P_x \quad=\quad (p \in P) \times (x \models p) \]
\end{definition}
That is, an inhabitant of~$P_x$ is a property and a proof that $x$~satisfies the property.
We will sometimes refer to~$P_x$ as the set of properties provably satisfied by~$x$.

\paragraph{Sets of links as relations between properties with proofs.}

In \autoref{subsec:intuitive_retentiveness}, a link connects a region in the source and another in the view; that is, it is a pair of source and view regions.
We have seen how regions are abstractly represented: given a source~|s|, the set of regions in~|s| is abstractly represented by~|P_s| where |P|~is the set of all possible region patterns (regarded as properties) for sources; similarly, the set of regions in a view~|v| is abstractly represented by~|Q_v| where |Q|~is the set of all possible region patterns for views.
It follows that, abstractly, a link should be a pair in the set $P_|s| \times Q_|v|$, and a set of links is a relation between $P_|s|$~and~$Q_|v|$.

\paragraph{Enriching get and put with links.}

Now we can enrich the type of |get| to:
\[ |get| ~:~ (|s| \in S) \,\to\, (|v| \in V) \times (P_|s| \sim Q_|v|) \]
That is, upon receiving a source $|s| \in S$, |get| will produce a view $|v| \in V$ and a relation between the regions in |s|~and~|v|.

It is tempting to do exactly the same for |put|, assigning it the type:
\[ (|s| \in S) \times (|v| \in V) \times (P_|s| \sim Q_|v|) \to S \]
This is not enough, however, because not all triples in the above domain are valid inputs.
In particular, for our syntax tree example (\autoref{subsec:intuitive_retentiveness}), an arbitrary input link might relate inconsistent regions, e.g., a source region of pattern `|Neg "..." a|' and a view region of pattern `|Add a b|'; in this case there is no hope to deliver the triangular guarantee since such invalid links cannot appear among the consistency links produced by |get|.
In general, we usually do not want to consider all triples in the above domain, and only want to operate on a subset consisting of those sources and views that are ``not too inconsistent'' and those links that are valid for the sources and views, such that consistency restoration is possible.
Inspired by \citet{Diskin-symmetric-delta-lenses}, we call this subset of triples a \emph{triple space}.

\begin{definition}[triple spaces]
A \emph{triple space}~|T| between a source set~|S| with properties~$P$ and a view set~$V$ with properties~$Q$ is a set such that:
\[ T \quad\subseteq\quad (s \in S) \times (|v| \in V) \times (P_s \sim Q_|v|) \]
\end{definition}

The definition of a retentive lens will start from specifying a triple space~|T|, which determines the range of triples of source, view, and links that the lens operates on; the domain of |put| will be set to~|T|, and |get| will be required to produce triples (the input sources included) only in~|T|.

\subsubsection{Uniquely Identifying Property Sets}

One important aim of retentive lenses is to make Hippocraticness an extreme case of the triangular guarantee (\autoref{subsec:intuitive_retentiveness}).
Having developed an abstract framework in \autoref{subsubsec:abstraction}, we can now give a quick sketch of how we can derive Hippocraticness.
Recall that the triangular guarantee roughly says that a set of input properties satisfied by the old source will also be satisfied by the new source.
The more input properties there are, the more restrictions we put on the new source.
In the extreme case, if the set of input properties can only be satisfied by at most one source, then the new source has to be the same as the old source since it is required to satisfy the same properties.
We will call such set of properties \emph{uniquely identifying}, and require that the set of source properties mentioned by the links produced by |get| should be uniquely identifying.

\begin{definition}[satisfaction of property sets]
Let $P$~be a set of properties on~$X$.
\[ x \models^* P \quad=\quad (p \in P) \to (x \models p) \]
\end{definition}

That is, $x \models^* P$ is the set of proofs that $x$~satisfies all properties in~$P$, since an inhabitant of $x \models^* P$ is a function that maps every $p \in P$ to an inhabitant of $x \models p$.

\begin{definition}[uniquely identifying property sets]
\label{def:uniquely-identifying}
A set~$P$ of properties on~$X$ is \emph{uniquely identifying} exactly when
\[ x \models^* P\ \text{is inhabited}\ \mathrel\wedge\ x' \models^* P\ \text{is inhabited} \quad\Rightarrow\quad x = x' \]
\end{definition}

That is, there is at most one element of~$X$ satisfying all properties in~$P$.

%\begin{example}
%  Let us consider an example regarding natural numbers (|Nat|) where the set of properties is $P = \{ x \geq 2, \ x \leq 2 \}$. $P$ on |Nat| is uniquely identifying since there is only one element $2$ of |Nat| satisfying $P$.
%  \todo{change to partially applied equality}
%\end{example}
%
%\begin{example}
%\label{example:not_unique_set}
%  For a counter example, in \autoref{fig:running_example}, a (singleton) set of properties $P = \{\!$ has a region |Plus x _ _| $\! \}$ on type |Expr| is not uniquely identifying |CST|,
%  since it is easy to verify that |CST_1 /= CST| also satisfies the singleton set of properties.
%  In fact, for mutual recursive types such as |Expr| and |Term|, there is no such set of properties consisting of regions only that is uniquely identifying. We will show a solution to this problem in next section.
%\end{example}

\subsubsection{Retentive Lenses}

We now have all the ingredients for the formal definition of retentive lenses, as well as the proof that retentive lenses satisfy Hippocraticness.

\begin{definition}[retentive lenses]
\label{def:retentive_lenses}
Let |T|~be a triple space between a source set~$S$ with properties~$P$ and a view set~$V$ with properties~$Q$.
A \emph{retentive lens} on~$T$ is a pair of functions |get| and |put| of type:
\begin{align*}
|get| &: (|s| \in S) \to (|v| \in V) \times (P_|s| \sim Q_|v|) \\
|put| &: T \to S
\end{align*}
such that
\begin{align}
& |get| \subseteq T \label{prop:getT} \\
& |get s = (v, l)| \quad\Rightarrow\quad \ldom(|fst| \cdot l)\ \text{is uniquely identifying} \label{prop:getU} \\
& |get (put (s, v, l)) = (v', l')| \quad\Rightarrow\quad |v = v'|\ \mathrel\wedge\ |fst| \cdot l \subseteq |fst| \cdot l'
\label{prop:put}
\end{align}
where $|fst| : (a \in A) \times B(a) \to A$ (for any set~|A| and family of sets~|B| indexed by~|A|) is the first projection function.
\end{definition}

What is new here is~(\ref{prop:put}): on the right of the implication, the left conjunct is Correctness (PutGet), and the right conjunct, which we may call \emph{Retentiveness}, formalises the triangular guarantee in a compact way.
We can expand Retentiveness pointwise to see that it indeed specialises to the triangular guarantee.

\begin{proposition}[triangular guarantee]
\label{prop:triangular_guarantee}
Given a retentive lens, let $(|s|, |v|, |l|) \in T$, $s' = |put (s, v, l)|$, and $((p, m), (q, n)) \in l$.
Then |get s' = (v, l')| for some $l' : P_|s'| \sim Q_|v|$, and there exists $m' \in (s' \models p)$ such that $((p, m'), (q, n)) \in l'$.
\end{proposition}

Interpreting this in the region model:
Suppose that there is an input link relating two regions |(p, m)| in the old source and |(q, n)| in the view, where |p|~and~|q| are region patterns and |m|~and~|n| contain the positions of the regions.
Then |get| must produce a link relating a region |(p, m')| in the new source and the view region |(q, n)|, meaning that, in the new source, the region pattern~|p| appears at some location (i.e.,~|m'|) that is consistent with |(q, n)|.

We then prove Hippocraticness with the help of two simple lemmas.

\begin{lemma}
\label{lem:get}
For a retentive lens,
\[ |get s = (v, l)| \quad\Rightarrow\quad s \models^* \ldom(|fst| \cdot l)\ \text{is inhabited} \]
\end{lemma}
\begin{proof}
Suppose that |get s = (v,l)|, where $l : P_s \sim Q_|v|$.
For every $p \in \ldom(|fst| \cdot l)$, by the definitions of left domain and relational composition, there exists $(p', m) \in P_|s|$ and $(q, n) \in Q_|v|$ such that $p' = p$ and $((p', m), (q, n)) \in l$ --- note that this implies $m \in (s \models p)$.
This maps every $p \in \ldom(|fst| \cdot l)$ to an inhabitant of $s \models p$, and therefore constitutes an inhabitant of $s \models^* \ldom(|fst| \cdot l)$.
\end{proof}

\begin{lemma}
\label{lem:restriction}
Let $P$~and~$P'$ be sets of properties on~$X$ and $x \in X$.
If $x \models^* P$ is inhabited and $P' \subseteq P$, then $x \models^* P'$ is also inhabited.
\end{lemma}
\begin{proof}
An inhabitant of $x \models^* P'$ is a function of type $(p \in P') \to (x \models p)$, and can be obtained by restricting the domain of an inhabitant of $x \models^* P$, which is a function of type $(p \in P) \to (x \models p)$.
\end{proof}

\begin{theorem}[Hippocraticness]
\label{thm:hippocraticness}
For a retentive lens,
\[ |get s = (v, l)| \quad\Rightarrow\quad |put (s, v, l) = s| \]
\end{theorem}
\begin{proof}
Supposing that |get s = (v, l)| and |put (s, v, l) = s'| for some~|s'|, our goal is to prove $s = s'$.
By~(\ref{prop:put}), |get s' = (v, l')| for some~|l'| such that $|fst| \cdot l \subseteq |fst| \cdot l'$. Applying $\ldom$ to both sides of the inclusion yields
\begin{equation}
\ldom(|fst| \cdot l) \subseteq \ldom(|fst| \cdot l')
\label{eq:hippocraticness}
\end{equation}
since $\ldom$ is monotonic.
By \autoref{lem:get}, both $s \models^* \ldom(|fst| \cdot l)$ and $s' \models^* \ldom(|fst| \cdot l')$ are inhabited.
Moreover, by \autoref{lem:restriction}, $s' \models^* \ldom(|fst| \cdot l)$ is also inhabited.
Finally, since $\ldom(|fst| \cdot l)$ is uniquely identifying by~(\ref{prop:getU}), we obtain $s = s'$ as required.
\end{proof}

\paragraph{Classic lenses as retentive lenses.}

As a simple example, we show that every well-behaved lens can be turned into a retentive lens.
The idea is that a classic lens corresponds to a retentive lens that guarantees to retain the whole source or nothing at all.
The retentive lens will be set up such that the only way to form links between a source and a view is to link the whole source with the whole view if they are consistent.
If the view is unchanged, the link can remain intact, and the whole source is retained;
otherwise, if the view is changed in any way, the link is broken, and the retentive |put| does not have to guarantee that anything in the old source is retained.

Formally:
Let $|g| : S \to V$ and $|p| : S \to V \to S$ be a well-behaved lens.
The property sets associated with |S|~and~|V| are |S|~and~|V| themselves respectively; given $|s| \in S$ (resp.~$|v| \in V$), the set $x \models |s|$ (resp.~$x \models |v|$) is inhabited by a single element~`$*$' if $x = s$ (resp.~$x = |v|$) or uninhabited otherwise.
The triple space~$T$ consists of triples |(s, v, l)| such that $|fst| \cdot l \cdot |fst|^\circ \subseteq |g|^\circ$ --- that is, a link can only exist between |s|~and |g s|.
Now we define:
\[ \begin{array}{lcl}
|get s| &=& (|g s|, \{((|s|, *), (|g s|, *))\}) \\
|put (s, v, l)| &=& |p s v|
\end{array} \]
It is easy to verify (\ref{prop:getT}), (\ref{prop:getU}), and the first part of~(\ref{prop:put}) (which is just PutGet).
For the second part of~(\ref{prop:put}):
If $l$~is empty, the conclusion holds trivially.
Otherwise, $l$~can only be $\{((|s|, *), (|g s|, *))\}$, implying that |v = g s|.
We then have |put (s, v, l) = p s (g s) = s| by GetPut, and |(v', l') = get (put (s, v, l)) = get s = (g s, {((s, *), (g s, *))})|.
Therefore |l' = {((s, *), (g s, *))} = l|, which implies $|fst| \cdot l \subseteq |fst| \cdot l'$.

%\paragraph{Customising the definition.}
%
%We should note that \autoref{def:retentive_lenses} is not a definitive version.
%For example, $|fst| \cdot l \subseteq |fst| \cdot l'$ in~(\ref{prop:put}) is equivalent to $l \subseteq |fst|^\circ \cdot |fst| \cdot l'$, in which the relation $|fst|^\circ \cdot |fst| : P_s \sim P_{s'}$ (where |s' = put (s, v, l)|) is really a hard-wired choice, requiring only that a property in the old source also holds in the new source.
%In the region model, for example, one possible restriction we might want to impose is that |put| should not move regions too far away from their original location, so we want not only that a region pattern appears in the new source but also that the path leading to the new region should be similar to the path to the old region in some way --- that is, we may also want to relate proofs in the triangular guarantee.

\subsection{Retentive Lenses for the Region Model}
\label{subsec:second-order}

Driven by the region model, we have arrived at a definition of retentive lenses (\autoref{def:retentive_lenses}) and seen that it entails Hippocraticness (\autoref{thm:hippocraticness}).
This definition does not immediately work for the region model, however.
This is because region patterns by themselves cannot form uniquely identifying property sets~(\autoref{def:uniquely-identifying}) --- requiring that a source must contain a set of region patterns does not uniquely determine it, however complete the set of region patterns is.
This can be intuitively understood by analogy with jigsaw puzzles: a picture cannot be determined by only specifying what jigsaw pieces are used to form the picture (if the jigsaw pieces can be freely assembled); to fully solve the puzzle and determine the picture, we also need to specify how the jigsaw pieces are assembled --- that is, the relative positions of all the jigsaw pieces.

Below we will introduce \emph{second-order property sets} into the theory so that it is possible to form uniquely identifying property sets in the region model.
We will also show that \autoref{def:retentive_lenses} and \autoref{thm:hippocraticness} only loosely depend on the specific structure of property sets, and can be adapted for second-order property sets with almost no structural change.
The general idea is that while second-order property sets have more internal constraints, they are still sets of properties, so the ideas behind the formal definitions for retentive lenses remain more or less the same.

\subsubsection{Uniquely Identifying Property Sets for the Region Model}

For brevity, we will use the |Arith| data type in the examples below (even though |Arith| is used elsewhere as a view type, on which we do not need property sets to be uniquely identifying).
We have been using property sets as conjunctive predicates.
For example, the property set $\{|Add a b|, |Sub a b|\}$ is used as a predicate $(t \models |Add a b|) \mathrel\wedge (t \models |Sub a b|)$ on~$t$, saying that $t$~should contain two regions, one of pattern |Add a b| and the other of pattern |Sub a b|.
If we want to additionally specify the relative position of these two regions --- or more abstractly, how the proofs of the two properties relate --- we need to switch to a dependent conjunction like $(r \in (t \models |Add a b|)) \mathrel\wedge (r' \in (t \models |Sub a b|)) \mathrel\wedge |Child|(|Add a b|, a, r, r')$, where $|Child|(|Add a b|, a, r, r')$ says that the path in~|r'| is the one in~|r| extended with one step that navigates from the root of the region of pattern |Add a b| to the subtree named~$a$ --- that is, the region of pattern |Sub a b| is directly below the region of pattern |Add a b| at the position named~|a|.
(This form of |Child| is in fact slightly simplified; in our formalisation below, |Child| will need to take more arguments.)
If a tree~|t| satisfies this predicate, it means that |t|~contains the two specified regions and, furthermore, the |Sub| region is the left child of the |Add| region.
We call properties like `|Child|' \emph{second-order properties}, which can refer to proofs of other properties.


With second-order properties capable of expressing relative position, it is now possible for a set of properties to be uniquely identifying.
For example, consider the following set of properties (expressed as a conjunctive predicate):
\begin{align}
& (r_0 \in (t \models |Num 0|)) \mathrel\wedge (r_1 \in (t \models |Num 1|)) \mathrel\wedge (r_2 \in (t \models |Num 2|)) \mathrel\wedge{} \nonumber \\
& (r_3 \in (t \models |Add a b|)) \mathrel\wedge (r_4 \in (t \models |Add a b|)) \mathrel\wedge{} \nonumber \\
& |Top|(r_3) \mathrel\wedge |Child|(|Add a b|, a, r_3, r_4) \mathrel\wedge |Child|(|Add a b|, a, r_4, r_0) \mathrel\wedge{} \nonumber \\
& |Child|(|Add a b|, b, r_4, r_1) \mathrel\wedge |Child|(|Add a b|, b, r_3, r_2) \label{ex:second-order}
\end{align}
where |Top| says that a region is the topmost one in a tree (that is, there is no other region above the named region).
This property set is satisfied by exactly one tree, namely:
\begin{code}
Add (Add (Num 0) (Num 1)) (Num 2)
\end{code}
It is also possible to specify a subset of these properties to be retained when the above tree is updated (as long as we are careful not to refer to regions that are left out of the subset), like:
\begin{align*}
& (r_0 \in (t \models |Num 0|)) \mathrel\wedge (r_3 \in (t \models |Add a b|)) \mathrel\wedge (r_4 \in (t \models |Add a b|)) \mathrel\wedge{} \nonumber \\
& |Child|(|Add a b|, a, r_3, r_4) \mathrel\wedge |Child|(|Add a b|, b, r_3, r_2)
\end{align*}
An updated tree satisfying this property will still have a region of pattern |Add (Add (Num 0) a) b|\,, but new regions can be added above it (because we no longer require $|Top|(r_3)$) or as its subtrees (because we no longer specify the right child of $r_3$~and~$r_4$).
Note that although it may be tempting to express the property set~(\ref{ex:second-order}) equivalently and more simply as the predicate $t \models |Add (Add (Num 0) (Num 1)) (Num 2)|$, this simpler predicate is monolithic and cannot be partially retained.

\subsubsection{Formalisation of Second-Order Properties}

The theory becomes more complicated when second-order properties are involved.
We have to deal with references to proofs of other properties, and be careful about validity of references.
Satisfaction of a second-order property set is no longer just satisfaction of individual properties, because whether a second-order property is satisfied depends on which proofs are supplied to show that other properties are satisfied.
To avoid unnecessary complication, we will formulate our definitions just for the structure we need: a set of (named) region patterns, whose proofs (locations of matching regions) are referred to by either a unary predicate (|Top|) or a binary one (|Child|).
The theory below is still somewhat obscure though, and can be safely skipped on first reading.

\begin{definition}[second-order property sets]
A \emph{second-order property set} on a set~$X$ is the union of three disjoint sets:
\begin{itemize}
\item a set~$P_0$ of properties on~$X$,
\item a set~$P_1$ equipped with a function $(-)^* : P_1 \to P_0$ such that every $p_1 : P_1$ is a property on $(x : X) \times (x \models p_1^*)$, and
\item a set~$P_2$ equipped with two functions $(-)^* : P_2 \to P_0$ and $(-)^{**} : P_2 \to P_0$ such that every $p_2 : P_2$ is a property on $(x : X) \times (x \models p_2^*) \times (x \models p_2^{**})$.
\end{itemize}
\end{definition}

To express the property set~(\ref{ex:second-order}) in our example, we can take the set~$P_0$ to be the cartesian product of a name set (consisting of names like $r_0$, $r_1$, etc) and the set of region patterns, and the set~$P_1$ to be properties of the form $|Top|(r, |pat|)$ where $(r, |pat|) \in P_0$.
(We omit discussion of~$P_2$, which consists of binary predicates (like |Child| in our example) and is analogous to~$P_1$.)
The function $(-)^*$ maps a $P_1$-property to the $P_0$-property it refers to --- for example, $(|Top|(r, |pat|))^* = (r, |pat|)$.
With this function, we can then say that any $p_1 \in P_1$ is a property on the proof of the property it refers to, namely~$p_1^*$.
We need to name the properties in~$P_0$ because the same region pattern may appear multiple times, and we want to refer to the different regions matching the same pattern --- in the property set~(\ref{ex:second-order}), we give two different names $r_3$~and~$r_4$ to two regions of the same pattern |Add a b|\,, so that the two regions can be properly referred to by the second-order properties.
%The need for naming will become clearer when we define when a second-order property set is satisfied.

The usual way to proceed from this point is to define when a subset of a second-order property set (representing a dependent conjunction like~(\ref{ex:second-order})) is well-formed, making sure that a $P_1$- or $P_2$-property does not refer to a $P_0$-property not included in the subset, and then define its proofs.
We will take a slightly quicker route, directly defining a well-formed proof that a subset of a second-order property set on~$X$ is satisfied by some $x \in X$.
Such a proof is a subset of the properties provably satisfied by~$x$ such that references are made correctly within the subset.

\begin{definition}
\label{def:second-order-provably-satisfied-properties}
Let $x : X$ and $P = P_0 \uplus P_1 \uplus P_2$ be a second-order property set on~$X$.
The set~$P_x$ (properties provably satisfied by~$x$) is defined by
\begin{align*}
P_x
={} & (p_0 : P_0) \times (x \models p_0) \\
\mathrel\uplus{} & (p_1 : P_1) \times (m : x \models p_1^*) \times ((x, m) \models p_1) \\
\mathrel\uplus{} & (p_2 : P_2) \times (m : x \models p_2^*) \times (m' : x \models p_2^{**}) \times ((x, m, m') \models p_2)
\end{align*}
\end{definition}

\begin{definition}
\label{def:second-order-well-formedness}
Let $x : X$ and $P = P_0 \uplus P_1 \uplus P_2$ be a second-order property set on~$X$.
A subset~$A$ of~$P_x$ is \emph{well-formed} exactly when:
\begin{itemize}
\item for every $p_0 : P_0$, if $(p_0, m) \in A$ and $(p_0, m') \in A$, then $m = m'$;
\item for every $p_1 : P_1$, if $(p_1, m, n) \in A$, then $(p_1^*, m) \in A$;
\item for every $p_2 : P_2$, if $(p_2, m, m', n) \in A$, then $(p_2^*, m) \in A$ and $(p_2^{**}, m') \in A$.
\end{itemize}
\end{definition}

\begin{definition}
\label{def:second-order-satisfaction}
Let $P'$~be a subset of a second-order property set~$P$ on~$X$ and $x \in X$.
The inhabitants of the set $x \models^* P'$ are exactly the well-formed subsets~$A$ of~$P_x$ such that $|fst|[A] = P'$, i.e., the image of~$A$ under |fst| is~$P'$.
\end{definition}

\paragraph{Adapting Retentive Lenses for Second-Order Properties.}

We have redefined the set of provably satisfied properties (\autoref{def:second-order-provably-satisfied-properties}), and for a subset $P'$ of a second-order property set, redefined the set $x \models^* P'$ of proofs that $x$~satisfies all properties in~$P'$ (\autoref{def:second-order-satisfaction}).
To make retentive lenses work with second-order properties, and also make the proof of \autoref{thm:hippocraticness} to go through, we need one final revision to the definition of triple spaces.

\begin{definition}[second-order triple spaces]
A \emph{second-order triple space}~|T| between a source set~|S| with second-order properties~$P$ and a view set~$V$ with second-order properties~$Q$ is a set such that:
\[ T \quad\subseteq\quad (s \in S) \times (|v| \in V) \times (P_s \sim Q_|v|) \]
and for every $|(s, v, l)| \in T$, both $\ldom\;l$ and $\ldom(l^\circ)$ are well-formed (\autoref{def:second-order-well-formedness}).
\end{definition}

We can now substitute these new definitions about property sets for the old ones used in the definition of retentive lenses (\autoref{def:retentive_lenses}).
As for the proof of \autoref{thm:hippocraticness}, which relies on Lemmas \ref{lem:get}~and~\ref{lem:restriction}: the statement of \autoref{lem:get} is valid for the new definitions about second-order property sets, and \autoref{lem:restriction} needs an additional assumption that $y \models P'$~should be inhabited for some~$y$ (to ensure that $P'$ is ``syntactically well-formed'').
\autoref{thm:hippocraticness} still holds despite the addition of the assumption, because the assumption is met ($|s| \models \ldom(|fst| \cdot l)$ is inhabited) when using \autoref{lem:restriction} in the proof.

