\section{A DSL for Retentive Bidirectional Tree Transformation}
\label{sec:dsl}

With theoretical foundations of retentive lenses in hand, we shall propose a domain specific language for easily describing them. Our DSL is designed to be simple, and yet able to handle many bidirectional tree transformations, which may be used in resugaring and bidirectional refactoring\footnote{We will explain this in more detail in \autoref{sec:case_studies} as case studies.}.
In our DSL, users basically write a |get| function in the form of inductively-defined consistency relations between sources and views,
and a |put| function will be generated to pair with the |get| forming a retentive lens.
\todo{in sec XXX}


\subsection{A Flavour of the DSL}
\label{subsec:flavor}
To give a flavour of the language, let us consider the synchronisation between concrete and abstract syntax trees (CSTs and ASTs), which are represented by the algebraic data types defined in \autoref{fig:running_example_datatype_def}.
The CSTs can be either expressions (|Expr|, containing additions and subtractions) or terms (|Term|, including numbers, negated terms and expressions in parentheses), and all constructors have an annotation field (|Annot|);
in comparison, the ASTs do not include explicit parentheses, negations, and annotations.
Consistency between CSTs and ASTs is given directly in terms of the |get| functions in \autoref{fig:running_example_datatype_def}.


This (``degenerated'') consiscenty relation (i.e. the |get| function) can be expressed using our DSL as \autoref{fig:dsl_example} shows. (The data type definitions written in our DSL remains the same and are thus omitted.)
%
Similar to the |get| functions in \autoref{fig:running_example_datatype_def}, here we describe two consistency relations: one between |Expr| and |Arith|, and the other between |Term| and |Arith|.
%
Each consistency relation is further defined by a set of inductive rules, stating that if the subtrees matched by the same variable name appearing the left-hand side and right-hand side are consistent, then the pair of trees constructed from these subtrees are also consistent.
%
Take |Plus _  x  y  ~  Add  x  y| for example.
It means that if |x_s| is consistent with |x_v|, and |y_s| is consistent with |y_v|, then |Plus s x_s y_s| and |Add x_v y_v| are consistent for any value |s|, where |s| corresponds to a ``don't-care'' wildcard in |Plus _ x  y|.
So the meaning of |Plus _  x  y  ~  Add  x  y| can be defined more formally by the following ``derivation'' rule:
\[
\frac{|x_s ~ x_v| \quad |y_s ~ y_v|}
{\forall s.~|Plus s x_s y_s ~ Add x_v y_v|}
\]

Now we briefly explain its semantics.
In the forward direction, its behaviour is quite similar to the first case of |getE| in \autoref{fig:running_example_datatype_def}, except that it also updates the links between subtrees of the source and the view and establishes a new one between the top nodes recording the correspondence.
In the backward direction, it creates a tree whose top node is |Plus _ _ _| and recursively builds the subtrees of |Plus _ _ _|, provided that the view matches |Add _ _| and there is no link connected to the top of the view.
%
The behaviour will be more subtle if there is some links connected to the top of the view, since |put| needs to take retentiveness into account.
We leave the detailed description to the later part.
In this way, each consistency relation is assigned a |get| and a |put| function, and each inductive rule of the consistency relation contributes one case to the two functions\footnote{Precisely speaking, it contributes more than one cases to |put|. We will it later.}.

% In the backward direction, while |put| needs to propagate changes in the view back to the source, it is a bit tricky for keeping retentiveness, and we will show how to carefully use links to gain retentiveness later.




% In this way, the generated |get| functions for the two consistency relations look exactly the same as the ones in \autoref{fig:running_example_datatype_def}.

% In the rest of this section, after explaining the syntax together with some examples, we give a bidirectional semantics of the language, and prove its retentiveness.


Suppose that we have a consistent pair of |cst| and |ast| shown in \autoref{fig:running_example}, where |cst| represents ``$0-1+-2$''
and its sub-term ``$-2$'' is desugared as ``$0-2$'' in |ast|.
If |ast| is modified into |ast'| (also in \autoref{fig:running_example}), where the two non-zero numbers are exchanged,
there is not much restriction on what a well-behaved |put| should do,
and we could have a wide range of |put| behaviour, producing,
as listed on the right-hand side of \autoref{fig:running_example},
\begin{itemize}
  \item |cst_1|, where the literals $1$~and~$2$ are swapped, along with their annotations; or
  \item |cst_2|, which represents the same expression as |cst_1| except that all annotations are removed; or
  \item |cst_3|, which represents ``$-2+(0-1)$'' and retains all annotations, in effect swapping the two subtrees under |Plus| and adding two constructors, |Term| and |Paren|, to satisfy the type constraints; or
  \item |cst_4|, where the negation syntactic sugar ``$-2$'' gets lost after the update.
\end{itemize}

\begin{figure}[t]
\setlength{\mathindent}{0em}
\begin{minipage}[t]{0.45\textwidth}
%
\begin{code}
data Expr   =  Plus   Annot  Expr  Term
            |  Minus  Annot  Expr  Term
            |  Term   Annot  Term

data Term   =  Lit    Annot  Int
            |  Neg    Annot  Term
            |  Paren  Annot  Expr

type Annot  =  String

data Arith  =  Add  Arith  Arith
            |  Sub  Arith  Arith
            |  Num  Int
\end{code}
\end{minipage}%
%
\begin{minipage}[t]{0.45\textwidth}
\begin{code}
getE :: Expr -> Arith
getE (Plus   _ e  t) = Add  (getE e) (getT t)
getE (Minus  _ e  t) = Sub  (getE e) (getT t)
getE (Term   _    t) = getT t

getT :: Term -> Arith
getT (Lit     _ i  ) = Num i
getT (Neg     _ t  ) = Sub (Num 0) (getT t)
getT (Paren   _ e  ) = getE e
\end{code}
\end{minipage}
\caption{Data types for concrete and abstract syntax trees and the consistency relation between them as two |get| functions in Haskell.}
\label{fig:running_example_datatype_def}
\end{figure}
%

\begin{figure}[t]
\begin{multicols}{2}
\begin{code}
Expr <---> Arith
  Plus   _  x  y  ~  Add  x  y
  Minus  _  x  y  ~  Sub  x  y
  Term   _  t     ~  t       ;

Term <---> Arith
  Lit     _  i   ~  Num  i
  Neg     _  r   ~  Sub  (Num 0)  r
  Paren   _  e   ~  e             ;
\end{code}
\end{multicols}
\caption{A program in our DSL for synchronising ADTs in \autoref{fig:running_example_datatype_def}.}
\label{fig:dsl_example}
\end{figure}



\begin{figure}[t]
\includegraphics[scale=1,trim={0cm 0cm 0cm 0cm},clip]{pics/no_retentiveness.pdf}
\caption{|CST| is updated in many ways in response to a change on AST.}
\label{fig:running_example}
\end{figure}

Among them, |cst_3| might be the most expected result because the user (or the tool) indeed swapped the two subtrees of |Plus| in |ast|. However, in practice, a well-behaved |put| generated from existing bidirectional languages (tools) has a very high possibility to return |cst_1| or |cst_2|.




\subsection{Syntax}

\begin{figure}[t]
\centerline{
\framebox[11cm]{\vbox{\hsize=13cm
\[
\begin{array}{llllllll}
\key{Program} \\
\bb
\m{prog} &::=& \m{tDef}_1~\ldots\ ~\m{tDef}_m ~\m{cDef}_1 \; \ldots \; \m{cDef}_n \\
\ee \\
\\
\key{Type Definition} \\
\bb
  \m{tDef} & ::= & \key{data} \ \m{type}  & =  & C_1 \ \m{type}_{11} \ \cdots\ \m{type}_{1m_{1}}\\
      &     &                      & || & \cdots\\
      &     &                      & || & C_k \ \m{type}_{11} \ \cdots \ \m{type}_{1m_{k}}\\
\ee\\
% \bb  \m{type} & ::= & \m{tName} \ \m{type}_1~\cdots~\m{type}_n & \reason{type name}\\
% \ee
\\
\key{Consistency Relation Definition} \\
\bb
  \m{cDef} &::=& \m{type} \longleftrightarrow \m{type}
                           &\reason{relation type}\\
           &   &  \quad r_1; \; \ldots \; r_n ;  &\reason{inductive rules}\\
\ee\\
\key{\small Inductive Rule}\\
\bb
   r   &::=& \m{pat}_1 \sim \m{pat}_2\\
\ee\\
\key{\small Pattern}\\
\bb
   pat &::=& |v|                               &\reason{variable pattern}\\
       &~||& \_                                &\reason{don't-care wildcard}\\
       &~||& C \ \m{pat}_1 \ \cdots \m{pat}_n  &\reason{constructor pattern}\\
\ee
\end{array}
\]
}}}
\caption{Syntax of the DSL.}
\label{fig:dsl_syntax}
\end{figure}


\autoref{fig:dsl_syntax} gives the syntax of the language.
% for describing retentive bidirectional transformation between algebraic data structures.
A program consists of two main parts. The first part is definitions of simple \emph{algebraic data types}, where an algebraic data type is defined through a set of data constructors |C_1 ... C_k|,
similar to those in functional languages such as Haskell.
The second part is definitions of \emph{consistency relations} between these data types, in which each consistency relation consists of a relation type declaration and a set of inductively-defined rules.
%
The relation type declaration is in the form of |srcType <---> viewType|, representing the source and view types for the synchronisation.
Each inductive rule is defined as a relation between source and view patterns |pat_s ~ pat_v|, where a pattern consists of variables, constructors, and wildcards, as we have already seen in the examples of \autoref{subsec:flavor}.
% Recall that we have seen a rule |Plus _  x  y  ~  Add  x  y| in Section \autoref{subsec:flavor}, where some variables appear on both sides. Its intuitive meaning is

% reading that if corresponding components are consistent with each other in the source and the view, then the source and the view are consistent.
% Our using of the same variables in both sides gives a concise notation for describing this meaning.

One might be wondering why we do not give a name to a consistency relation.
This is because we assume, without loss of generality, that there is only one consistency relation between any two types.
In fact, if we want to define multiple consistency relations between two types, we can indirectly achieve this by giving different names to the view types (like type synonyms), one name for writing one consistency relation.
% Note that the type for each variable in a pattern can be easily derived from the type of the data constructors used in the pattern.


\subsubsection{Syntactic Restrictions}
\label{subsec:synres}

We shall impose several syntactic restrictions to the DSL for guaranteeing that a consistency relation |T_s <---> T_v| indeed implies a view generation function |T_s --> T_v|.


\begin{itemize}
\item On \emph{variables}, we assume \emph{linear variable usage}.
The variable usage must be linear in the sense that a variable must appear exactly once in each side of an inductive rule.

\item On \emph{patterns}, we assume two restrictions: \emph{pattern coverage} and \emph{source pattern disjointness}.
Pattern coverage guarantees totality:
for a consistency relation, all the patterns on the left-hand sides should cover all the possible cases of the source type |T_s|, and all the patterns on the right-hand side should cover all the cases of the view type |T_v|.
% We require that the programs in our DSL to be total, in the sense that
%
Source pattern disjointness requires that all of the source patterns in a consistency relation should be disjoint, so that at most one pattern can be matched when running |get|.
This implies that the program converting a source to its view is indeed a function (regardless of the order of the inductive rules).


%format T_s1 = "T_{s1}"
%format T_s2 = "T_{s2}"

\item On \emph{algebraic data types}, we assume \emph{interconvertible data types}, for allowing |put| to switch between sources of different types while retaining the core information.
%
If we want to define two consistency relations sharing the same view type, say |T_s1 <---> T_v| and |T_s2 <---> T_v|, we require that the two source types, |T_s1| and |T_s2|, should be interconvertible.
%
For instance, since the two consistency relations |Expr <---> Arith| and |Term <---> Arith| in \autoref{fig:dsl_example} share the same view type |Arith|,
we should be able to convert |Expr| to |Term| and vice versa.
%
Syntactically speaking, a type |T_1| can be converted into |T_2|, if the type definition of |T_2| contains a case
|C_i type_1 ^ ... ^ T_1 ^ ... ^ type_n|,
and the consistency relation of |T_2 <---> T_v| contains a projection rule |C_i _ ^ ... ^ x ^ ... ^ _ ~ x|, for wrapping a piece of data of type |T_1| into that of type |T_2|.

\end{itemize}

\subsubsection{Programming Examples}
Although being tiny, the DSL is able to describe many interesting bidirectional tree transformations.
Here are some examples (including data type definitions):
\begin{itemize}
  \item The |mirror| transformation, which swaps all the subtrees of a given tree in both |get| and |put| directions: \\[-0.8em]
\begin{minipage}[t]{0.4\textwidth}
\begin{code}
^ ^ ^ ^ ^ ^ ^ ^ ^ ^  BinT Int <---> BinT Int
^ ^ ^ ^ ^ ^ ^ ^ ^ ^  ^ ^ Tip         ~  Tip
^ ^ ^ ^ ^ ^ ^ ^ ^ ^  ^ ^ Node i x y  ~  Node i y x
\end{code}
\end{minipage}%
%
\begin{minipage}[t]{0.5\textwidth}
\begin{code}
^ ^ ^ ^ ^ ^ ^ ^ ^ data  BinT a =
^ ^ ^ ^ ^ ^ ^ ^ ^       ^ ^     Tip
^ ^ ^ ^ ^ ^ ^ ^ ^       ^ ^  |  Node a (BinT a) (BinT a)
\end{code}
\end{minipage}


  \item The |spine| transformation, which collects the information carried by the left-most child of a rose tree in a list: \\[-0.8em]
\begin{minipage}[t]{0.4\textwidth}
\begin{code}
^ ^ ^ ^ ^ ^ ^ ^ ^  RTree  <---> [Int]
^ ^ ^ ^ ^ ^ ^ ^ ^  ^ ^ ^ RNode i []       ~  [i]
^ ^ ^ ^ ^ ^ ^ ^ ^  ^ ^ ^ RNode i (x : _)  ~  i:x
\end{code}
\end{minipage}%
%
\begin{minipage}[t]{0.5\textwidth}
\begin{code}
^ ^ ^ ^ ^ ^ ^ ^ ^ data  RTree a =
^ ^ ^ ^ ^ ^ ^ ^ ^       ^ ^ RNode a (List (RTree a))

^ ^ ^ ^ ^ ^ ^ ^ ^ data List a = Nil | Cons a (List a)

\end{code}
\end{minipage}

  \item The |map| transformation, which applies a bidirectional transformation to all the nodes in a binary tree: \\[-0.8em]
\begin{minipage}[t]{0.4\textwidth}
\begin{code}
^ ^ ^ ^ ^ ^ ^ ^ ^  BinT Int  <---> BinT Bool
^ ^ ^ ^ ^ ^ ^ ^ ^  ^ ^ ^ Tip           ~  Tip
^ ^ ^ ^ ^ ^ ^ ^ ^  ^ ^ ^ Node x ls rs  ~  Node x ls rs
\end{code}
\end{minipage}%
%
\begin{minipage}[t]{0.5\textwidth}
\begin{code}
^ ^ ^ ^ ^ ^ ^ ^ ^  Int <---> Bool
^ ^ ^ ^ ^ ^ ^ ^ ^  ^ ^ ^ (\i   -> if i > 0 then True else False)
^ ^ ^ ^ ^ ^ ^ ^ ^  ^ ^ ^ (\i b ->  case b of
^ ^ ^ ^ ^ ^ ^ ^ ^  ^ ^ ^           ^ ^ ^  True  -> if i > 0 then i else 1
^ ^ ^ ^ ^ ^ ^ ^ ^  ^ ^ ^           ^ ^ ^  False -> if i <= 0 then i else 0)
\end{code}
\end{minipage}

  % \item \zhenjiang{Will add an example about composition.}

\end{itemize}

% Our tiny language, however, cannot divide a tree into two parts according to a given predicate, which also causes difficulty in doing filtering.

%\todo{Complex version can be enriched later in this way...}
% |Cons a as || a `elem` ... ~ Cons a as|
% |Cons _ as  ~ as|

We leave more interesting and practical case studies on bidirectional refactoring and resugaring  to \autoref{sec:case_studies}.

\subsection{Semantics}
\label{subsec:dslsem}
In this subsection, we give a denotational semantics of our DSL by specifying the corresponding |get| and |put| as mathematical functions. Our language is also implemented as a compiler transforming programs in our DSL to |get| and |put| functions in Haskell. Our Haskell implementation directly follows the semantics given here.

Because our |put| function and its implementation will respect relative positions of regions by default---no matter whether they are required by the user or not, a design choice we made to simplify |get| and |put| to only produce and accept links between regions---so that the user will not be bothered with specifying links for second-order properties.
In \autoref{subsec:proof}, we will prove that the |get| and |put| functions given below, when lifted to a version also producing and accepting second-order properties, actually form a retentive lens.

\subsubsection{Types and Patterns}
To define the semantics of |get| and |put|, we first need a semantics of type declarations and patterns in our DSL. For type declarations, their semantics is the same as in Haskell and thus we will not elaborate here.
For each defined algebraic data type |T|, we will also use |T| to refer to the set of values of type |T|. For patterns in our DSL, we will use |SPat| to refer to the set of all patterns on |S| for every type |S|. For a pattern $|p| \in |SPat|$, |Vars p| will be the set of variables in |p| and |TypeOf p v| will be the set corresponding to the type of |v| in pattern |p|. We will also use |Pat| to denote the tagged union of patterns for all types, that is, $|Pat| = \{\, (|S|,\; |sp|) \mid |S| \text{ is a type, }|sp| \in |SPat| \,\}$.

To manipulate patterns, we will use the following functions:
\begin{align*}
|isMatch_S| &: (|p| \in |SPat|) \rightarrow |S| \rightarrow |Bool| \\
|decompose_S| &: (|p| \in |SPat|) \rightarrow |S| \rightarrow \big((|v| \in |Vars p|) \rightarrow |TypeOf p v|\big) \\
|reconstruct_S| &: (|p| \in |SPat|) \rightarrow \big((|v| \in |Vars p|) \rightarrow |TypeOf p v|\big) \rightarrow |S|
\end{align*}
|isMatch p s| tests if the value |s| matches the pattern |p|. If they match, |decompose p s| will result in a function |f| from every variable |v| in |p| to the corresponding subtree of |s|.
Conversely, |reconstruct p f| produces a value |s| matching the pattern |p| by replacing every $|v| \in |Vars p|$ in |p| with |f v|, provided that |p| does not contain any wildcards.
Since the semantics of patterns in our DSL is rather standard, we will also omit formal definitions of these functions.


%format mgetsv = "\getsv"
%format mgetspvp = "get_{S^\prime V^\prime}"
%format msetin = "\!\!\in\!\!"
%format munion = "\mathop{\cup}"
%format (mset (x)) = "\big\{\;" x  " \;\big\} "
%format (mbigparen (x)) = "\big(\;" x  " \;\big) "
%format (mBigparen (x)) = "\Big(\;" x  " \;\Big) "
%format mbigunion = "\bigcup"
\subsubsection{Get Semantics}\label{subsubsec:getsem}
For a consistency relation $|S| \longleftrightarrow |V|$ defined in our DSL with a set of inductive rules $R = \{\, |spat_i| \sim |vpat_i| \mid |i| \in \mathcal{I}\,\}$, its corresponding $\getsv$ function has the following signature:
\begin{align*}
\getsv : |S| \rightarrow |V| \times (|Region| \sim |Region|)
\end{align*}
where |Region| is $|Pat| \times |Path|$, and |Path| is the set of lists of integers, which encode paths in trees. The body of $|get|_{SV}$ is:
\begin{code}
mgetsv s =   let  spat_k = selectPat s R
                  f = decompose_S spat_k s
                  vs = get . f
                  links = mbigunion (mset (updatePaths_t (snd (vs t)) | t msetin Vars spat_k))
                  newLink = (mset (mBigparen (^(mbigparen ((S, ^fillWildcards s spat_k), [])), mbigparen ((V,^ vpat_k), [])^)))
             in   (mbigparen (reconstruct_S vpat_k (fst . vs), ^ newLink munion links))
\end{code}
where |selectPat s R| returns the unique source pattern in |R| matching |s|. Such a pattern exists because our DSL syntactically requires all source patterns of |R| to be disjoint and total.

By matching the source with |spat_k|, we bind every variable in |spat_k| to a subtree of |s| as |f : (v msetin Vars spat_k) -> TypeOf spat_k v|.
Then we recursively call |get| for all subtrees, expressed as |get . f| in the definition above, while to be precise, we need to invoke $|get|_{|TypeOf spat_k v|,\; |TypeOf vpat_k v|}$ for every |t msetin Vars spat_k|.

With the subtrees and links produced by recursive calls, we can construct the result of |mgetsv|:
(1)~the returned view is created simply by reconstructing the view pattern |vpat_k| with subtrees generated recursively;
and (2)~the returned links should be the union of:
(2.1) a new link between two regions at the root of |s| and |v| respectively (|newLink|).
(Note that subtrees of |s| matching wildcards of |spat_k| are also treated as parts of the region, and this is achieved by invoking |fillWildcards s spat_k| to replace all the wildcards of |spat_k| with the corresponding subtrees of |s|.)
(2.2) and also links produced by recursive calls, for which we need to be careful to update all paths in them.
For a link $((\,\cdots, \mathit{spath}),\;(\,\cdots, \mathit{vpath}))$ generated by the recursive call for pattern variable |v|, we need to prepend |spath| to the path of |v| in |spat_k|, and |vpath| to the path of |v| in |vpat_k|, which is expressed as |updatePaths| in the above definition.

\subsubsection{Put Semantics}\label{subsubsec:putsem}
To define a corresponding |put| function, we need to determine its domain |T| to make it total and retentive.
Here we will first define the body of |put|, and later the definition of |T| will come out naturally.

\newcommand{\putsv}[0]{|put|_{SV}}
%format mputsv = "\putsv"
%format memptyset = "\emptyset"
%format mneq = "\neq"
%format msetminus = "\setminus"
%format mand = "\wedge"
%format mwildcardfree = "\text{wildcard free}"
For a consistency relation $|S| \longleftrightarrow |V|$ in our DSL defined with $R = \{\, |spat_i| \sim |vpat_i| \mid |i| \in \mathcal{I}\,\}$, its corresponding |mputsv| function has the signature:
\[\putsv : T \rightarrow |S|\]
where |T| is a subset of $|S| \times |V| \times (|Region| \sim |Region|)$. The body of |mputsv| is defined as:
\begin{code}
mputsv (s, v, ls) =
  let linksToVRoot = (mset (mbigparen (((S', spat), spath), ((V, vpat), [])) msetin ls))
  in if  linksToVRoot mneq memptyset then
         let  l@(((S', spat), spath), ((V, vpat), [])) = shortestSPathLink linksToVRoot
              vs = decompose_V vpat v
              ss = \(t  msetin Vars spat) ->
                        put s (vs t) (divideLinks_t (ls msetminus {l}))
         in {-"\mathit{inj}_{S^\prime S}\;"-} (reconstruct spat ss)
     else  let  l = mset ((spat_k ~ vpat_k) msetin R | isMatch vpat_k v)
                (spat_k ~ vpat_k) = anyElement l
                vs = decompose_V vpat_k v
                ss = \(t  msetin Vars spat_k) ->
                          put s (vs t) (divideLinks_t ls)
           in reconstruct (fillWildcardsWithDefaults spat_k) ss
\end{code}
The process of |mputsv| is divided into two cases:
\begin{enumerate}
\setlength{\parskip}{0.5em}%
\setlength{\itemsep}{0.5em}%
\item \textit{The root of the view is connected to input links.} In this case, we pick one link among them (|shortestSPathLink|), and use the source region of the link as the root of the new source, whose subtrees are created by recursive calls to subtrees of |v|.
The function |divideLinks_t lset| divides the set of links into appropriate parts for each recursive call, and maintains the paths of all view regions:
for each link in |lset|, if the path of its view region has a prefix equal to the path of |t| in |vpat|, |divideLinks_t| will remove that prefix; if it does not have such a prefix, |divideLinks_t| will remove it from the set |lset|.
Note that since the type of the newly created source |S'| is possibly different from the return type |S|, we need to wrap it into |S| using $\mathit{inj}_{S'S}$.
Such a wrapper always exists because it is syntactically guaranteed by our DSL (\autoref{subsec:synres}).

The choice of |shortestSPathLink| at the beginning is also important.
By always choosing the link whose source region is at the shallowest position, we can guarantee that those regions in the new source will have the same relative positions as those in the original source.
Thus our |put| functions will respect all of the possible second-order properties asserting relative positions of regions in the old source.

\item \textit{The root of the view is not attached to any input link.} In this case, we only need to perform a ``traditional'' |put|.
We choose an arbitrary inductive rule with a matched view pattern in set $R = \{\, |spat_i| \sim |vpat_i| \mid |i| \in \mathcal{I}\,\}$, and the corresponding source pattern is used to create the new source.
If the source pattern contains wildcards, we also need to replace those wildcards with some default values.
\end{enumerate}

From the definition of |mputsv|, we can define |T| to be:
% and our |put| function is total and retentive:
\begin{itemize}
\item All input links are valid, in the sense that: (1) the path for proving a region pattern indeed leads to a subtree matching that pattern, and (2) the source region and the view region of a link match with some inductive rule. % defining the consistency relation.

\item For every subtree of the view whose root is not attached to any link, it always matches a view pattern in the set of rules |R| defining the consistency relation.
This requirement guarantees that in the second case of |put|, we can always create a consistent source.

\item All regions of input links are non-overlapping. Additionally, for every subtree of the view whose root is not attached to any link and matches a view pattern in |R|, the matched fragment is non-overlapping with any region of the input links either. This requirement guarantees that our |put| functions will eventually process all input links in the recursive procedure. (|put| will miss processing some input links if there is overlap.)
\end{itemize}




\subsection{To Retentive Lenses}
\label{subsec:proof}
In this subsection, we will show that |mgetsv| and |mputsv| functions specified above satisfy the following properties:
\begin{align*}
& |mgetsv (mputsv (s, v, l)) = (v', l')| \quad\Rightarrow\quad |v = v'| \tag{Correctness}\\
& |mgetsv (mputsv (s, v, l)) = (v', l')| \quad\Rightarrow\quad |fst| \cdot l \subseteq |fst| \cdot l' \tag{Retentiveness} \\
& |mgetsv s = (v, l)| \quad\Rightarrow\quad |mputsv (s, v, l) = s| \tag{Hippocraticness}
\end{align*}
However, they do not form a retentive lens immediately since they do not generate and process second-order properties and thus do not satisfy the \hyperref[prop:getU]{uniquely identifying} requirement.
In fact, our |mgetsv| and |mputsv| functions respect \textit{all possible} second-order properties by default no matter whether they are required by the user or not, which is exactly the reason why we leave out second-order properties from the interface of |mgetsv| and |mputsv|.

In the rest of this subsection, we will show that |mgetsv| and |mputsv| in our semantics can be lifted to a version producing and accepting second-order properties, and that version of |mgetsv| and |mputsv| will form a retentive lens.
Finally, we have a corollary showing that |mgetsv| and |mputsv| satisfy the properties listed above.

%format mprop2 = "\mathit{Prop^2}"
\subsubsection{Second-Order Properties}
Before defining the lifted version of |get| and |put|, we need to formalise second-order properties in our DSL, which are properties on the proofs of first-order properties (region patterns) as explained in \autoref{subsec:second-order}.
In particular, we will use them to specify: (1) some region is the root of a tree, and (2) some region is the $n$-th child of another region. Thus we define second-order properties as a set |mprop2|:
\begin{align*}
|mprop2| &=   |NamedPat| \,\uplus\, (|NamedPat| \times |NamedPat| \times \mathbb{N}) \\
|NamedPat| &= |Pat| \times \mathbb{N}
\end{align*}
where $\uplus$ is disjoint union, and |NamedPat| is the set of patterns paired with a number so that second-order properties can refer to a region pattern by its number.
Similarly, we also define $|NamedRegion| = |NamedPat| \times |Path|$.

\subsubsection{Lifted Get and Put}
Given a pair of |mgetsv| and |mputsv| with signatures:
\begin{align*}
\getsv : |S| \rightarrow |V| \times (|Region| \sim |Region|) \quad \quad \quad \quad \putsv : |T| \rightarrow |S|
\end{align*}
where |T| is a subset of $|S| \times |V| \times (|Region| \sim |Region|)$, we can construct a pair of |get'| and |put'| with the following signatures:
\begin{align*}
|get'| : |S| \rightarrow |V| \times (|SatProp| \sim |SatProp|) \quad \quad \quad \quad |put'| &: |T'| \rightarrow |S|
\end{align*}
where $|SatProp| = |NamedRegion| \uplus |mprop2|$ and |T'| is a subset of $|S| \times |V| \times (|SatProp| \sim |SatProp|)$. 

The construction of |put'| is trivial.
Given source |s|, view |v|, and second-order links |ls|, we can first obtain first-order links |l| by simply filtering out all links between second-order properties (|mprop2|) in |ls|, and turns links between |NamedRegion| into links between |Region| by removing all names (the $\mathbb{N}$ part).
Then we can invoke |mputsv s v l| and use its result as the result of |put' s v ls|.
(Remember that |put| in fact respects all of the possible second-order properties.)

Now we construct |get'|, whose returned view is the same as that produced by |mgetsv| and returned links should characterise all the relative positions (i.e.: uniquely identifying).
% besides all first-order links between regions produced by |mgetsv|, |get'| will also generate a set of second-order properties completely characterising the relative positions of all regions produced by |mgetsv|.
More precisely, |get'| generates links in the following way:
(1) |get'| transforms all links between |Region| (produced by |mgetsv|) into links between |NamedRegion| by assigning unique names to region patterns of every link.
(2) For the link connecting roots of the source and view, |get'| creates two |mprop2| asserting that those two regions are roots and a link between them.
(3) For every pair of source regions $|sr|_1$ and $|sr|_2$ where $|sr|_1$ is the $x$-th child of $|sr|_2$, |get'| create a $|p_s| \in |mprop2|$ asserting that the named region pattern in $|sr|_1$ should be the $x$-th child of the named region pattern in $|sr|_2$.
Similarly, for the view regions $|vr|_1$ and $|vr|_2$ corresponding to $|sr|_1$ and $|sr|_2$ respectively, |get'| also creates $|p_v| \in |mprop2|$ recording their relative position in the view.
Then, a link $(|p_s|, |p_v|)$ is added.

Finally, let us define |T'|, the domain of |put'|.
In \autoref{subsec:dslsem}, |T| is defined to restrict the input source, view, and links between regions so that |mputsv| becomes total.
We shall let |T'| to be a subset of |T| with further restrictions on links between second-order properties.
For every link between (second-order properties) $|p_s|$ and $|p_v|$, we require that:
\begin{itemize}
\item The assertion of |p_s| and |p_v| should be satisfied by regions in the set of input links.
For example, if $|p_s| = \mathit{inj_1}\; (|pat|,\, |i|)$ (|inj_1| injects |NamedPat| to |mprop2|) asserts that the named region pattern $(|pat|, \, |i|)$ must be the root of the source, then its source region (referred by integer |i|) should indeed locate at the root.
% then we require that the set of input links contains a link between regions whose source region pattern is $(|pat|,\, |i|)$, and its source region indeed locates at the root.

\item |p_s| and |p_v| should be correctly paired:
If |p_s| is an assertion that source region |sr_1| is the root, then |p_v| must be the assertion that its corresponding view region |vr_1| is the root of the view.
Similarly, if |p_s| is an assertion that source region |sr_1| is the |n|-th child of |sr_2|, then |p_v| must be its corresponding assertion on |vr_1| and |vr_2|.
\end{itemize}

\subsubsection{Retentiveness}
\begin{theorem}\label{thm:semsound}
|get'| and |put'| defined above form a retentive lens.
\end{theorem}
\begin{proof}
For the sake of brevity, we only describe the overall idea here and the detailed proof can be found in the appendix.
The proof is based on our Haskell implementation, which is basically a more precise version of the semantics given above.
%
The \hyperref[prop:getU]{uniquely identifying} requirement is relatively easy to prove because our |get'| uses second-order properties that precisely specify how regions form a tree.
%
\hyperref[prop:put]{Correctness} and \hyperref[prop:put]{Retentiveness} can be proved by performing inductions on the height of the view:
we assume that the two properties already hold for all the views of height less than |h|,
and then we prove that the two properties also hold for any view of height |h| using induction hypotheses.
% Under the induction hypothesis, we do equational reasoning on the definition of |get' (put' s v l)|, expanding it to a form with sub-terms of |get' (put' s v' l')| for smaller |s'| and |l'|.
% With the induction hypothesis, we should be able to verify the retentiveness requirement is satisfied.
\end{proof}

Now we have the following corolarry characterising |mgetsv| and |mputsv| denoted by our semantics.
\begin{corollary}\label{thm:semsound2}
A pair of |mgetsv| and |mputsv| satisfy:
\begin{align*}
& |mgetsv (mputsv (s, v, l)) = (v', l')| \quad\Rightarrow\quad |v = v'| \\
& |mgetsv (mputsv (s, v, l)) = (v', l')| \quad\Rightarrow\quad |fst| \cdot l \subseteq |fst| \cdot l' \\
& |mgetsv s = (v, l)| \quad\Rightarrow\quad |mputsv (s, v, l) = s|
\end{align*}
\begin{proof}
By \autoref{thm:semsound} and \autoref{thm:hippocraticness}, |get'| and |put'| satisfy these properties. From the process of constructing |get'| and |put'| using |mgetsv| and |mputsv|, we know these properties are also satisfied by |mgetsv| and |mputsv|.
\end{proof}
\end{corollary}


