% !TEX TS-program = lhs + pdflatex
% !TEX encoding = UTF-8 Unicode

% For double-blind review submission, w/o CCS and ACM Reference (max submission space)
%\documentclass[acmsmall,fleqn,review,anonymous,draft]{acmart}\settopmatter{printfolios=true,printccs=false,printacmref=false}

%% For double-blind review submission, w/ CCS and ACM Reference
\documentclass[acmsmall,review,anonymous]{acmart}\settopmatter{printfolios=true}

%% For single-blind review submission, w/o CCS and ACM Reference (max submission space)
%\documentclass[acmsmall,review]{acmart}\settopmatter{printfolios=true,printccs=false,printacmref=false}

%% For single-blind review submission, w/ CCS and ACM Reference
%\documentclass[acmsmall,review]{acmart}\settopmatter{printfolios=true}
% For final camera-ready submission, w/ required CCS and ACM Reference
% \documentclass[acmsmall]{acmart}\settopmatter{}

%include polycode.fmt

%format forall a = "\forall " a
%format exists a = "\exists " a
%format ldot = "."

%format Set_1
%format P_s
%format P_s' = " P_{s''} "
%format P_ss' = " P_{ss''} "
%format subset = " \subset "
%format subseteq = " \subseteq "
%format setconj  = " \cap "
%format sigma   = " \sum "
%format elem    = " \in "
%format getInv = " \textit{get}^{-1} "


%format preyV = "\textit{prey}V"
%format STypes = "S\mkern-3mu\textit{Type}"
%format VTypes = "V\mkern-3.5mu\textit{Type}"
%format S_ExprArithCase0 = "\textit{S\_ExprArithCase0}"
%format V_ExprArithCase0 = "\textit{V\_ExprArithCase0}"
%format prexV = "\textit{prex}V"
%format dollar1 = "\$1"
%format dollar2 = "\$2"
%format dollar3 = "\$3"
%format typeOf = "\textit{typeOf}"

%format pattern_l
%format pattern_r
%format path_l
%format path_r
%format path_t
%format pattern_v
%format path_b
%format pt_1
%format p_1l
%format p_1r
%format pt_2
%format pt_v
%format pt_l
%format pt_r
%format p_2l
%format p_2r
%format vl_1
%format tyX_i
%format tyY_j
%format *** = "*\!\!*\!\!*"

%format e_1
%format e_1' = "e_1''"
%format e_2
%format e_2' = "e_2''"
%format e_3
%format e_b

%format Trait_1
%format Trait_2
%format trait_1
%format trait_2


%format insSubtree_s
%format insSubtree_l

%format == = "\mathop{\shorteq\kern1pt\shorteq}"
%format ~ = "\mathrel\sim"
%format <== = "\mathrel\Leftarrow"
%format ==> = "\mathrel\Rightarrow"
%format <---> = "\longleftrightarrow"
%format <===> = "\Longleftrightarrow"
%format <==> = "\Leftrightarrow"
%format (VEC(x)) = "\vv{"x"}"
%format (SEMANTIC(x)) = "\llbracket " x "\rrbracket "
%format (VARS(x)) = "\mkern-\thickmuskip(\vv{" x "})"
%format (SUBST(x)(y)) = "\mkern-\thickmuskip[\vv{" x "}/\vv{" y "}]"


%format (ToRegions(x)) = "\mathsf{ToRegions}\llbracket " x "\rrbracket "
%format (InvVarP(x)) = "\mathsf{InvVarP}\llbracket " x "\rrbracket "
%format (GenInsSubtree(x)) = "\mathsf{GenInsSubtree}\llbracket " x "\rrbracket "

%format GenProgram    = "\mathsf{GenProgram}"
%format (GenProgram1(x)) = "\mathsf{GenProgram}\llbracket " x "\rrbracket "
%format (GenPutRec(x)) = "\mathsf{GenPutRec}\llbracket " x "\rrbracket "
%format (GenGet(x)) = "\mathsf{GenGet}\llbracket " x "\rrbracket "
%format (GenRepSubtree(x)) = "\mathsf{GenRepSubtree}\llbracket " x "\rrbracket "
%format (MkLink(x))   = "\mathsf{MkLink}\llbracket " x "\rrbracket "
%format (getPref(x)) = "\mathsf{getPref}\llbracket " x "\rrbracket"
%format typeOfTT = "\mathsf{typeOf}"
%format (ctypeOf(x)) = "\mathsf{typeOf}\llbracket " x "\rrbracket"
%format (getVar(x)) = "\mathsf{getVar}\llbracket " x "\rrbracket"
%format (MkPat(x)) = "\mathsf{MkPat}\llbracket " x "\rrbracket "
%format (local(x)) ="loc\_ " x " "
%format ST ="\mathsf{ST}"
%format VT ="\mathsf{VT}"
%format MkLinkt = " \mathit{MkLink} "
%format (varsTT(x))  = "\mathsf{vars}\llbracket " x "\rrbracket"

%format itOf    = "\mathit{of} "
%format itCase  = "\mathit{case} "
%format itLet   = "\mathit{let} "
%format itIn    = "\mathit{in} "
%format itWhere = "\mathit{where} "
%format whereTT = "\mathsf{where} "
%format itMkPat = "\mathit{MkPat} "

%format GetPut = "\mathsf{GetPut} "
%format PutGet = "\mathsf{PutGet} "

%format Correctness = "\mathsf{Correctness} "
%format Hippocraticness = "\mathsf{Hippocraticness} "
%format Retentiveness = "\mathsf{Retentiveness} "

%format ^ = "\ "
%format ^^ = "\;"

%format ::= = "\Coloneqq"

%format CST_0
%format CST_1
%format CST_2
%format CST_3

%format P_1
%format P_2
%format P_i
%format P_n

%format Q_1
%format Q_2
%format Q_i
%format Q_n


%format t_1
%format t_2
%format s_0
%format s_1
%format s_1' = " s_1'' "
%format s_2
%format s_2' = " s_2'' "
%format s_3
%format s_4
%format s_i
%format v_i
%format s_01
%format s_02
%format v_0
%format v_1
%format v_2
%format v_3
%format l_0
%format l_1

%format ls_j'
%format ls_j

%format ls_i'
%format ls_0' = " \mathit{ls}_0'' "
%format ls_1'
%format ls_n'
%format ls_i
%format ls_0
%format ls_1
%format ls_2
%format ls_3
%format ls_n

%format hl_i'
%format hl_0' = " \mathit{hl}_0'' "
%format hl_1'
%format hl_n'
%format hl_i
%format hl_0
%format hl_1
%format hl_2
%format hl_3
%format hl_n


%format vls_0
%format vls_1
%format vls_1' = " \mathit{vls}_1'' "
%format vls_2
%format vls_3
%format vls_4
%format vls_0' = " \mathit{vls}_0'' "
%format vls_n
%format vls_n'
%format vls_i
%format vls_i'

%format hls_0
%format hls_1
%format hls_1' = " \mathit{hls}_1'' "
%format hls_2
%format hls_3
%format hls_0' = " \mathit{hls}_0'' "
%format hls_n
%format hls_n'
%format hls_i
%format hls_i'

%format imgl_0
%format imgv_0
%format imgh_0

%format ys_0
%format ys_1
%format ys_n
%format ys_0' = " \mathit{ys}_0'' "
%format ys_1' = " \mathit{ys}_1'' "
%format ys_n' = " \mathit{ys}_n'' "

%format y_0'  = " y_0'' "
%format y_1'  = " y_1'' "
%format y_i'  = " y_i'' "
%format y_n'  = " y_n'' "

%format vl_0
%format vl_1
%format vl_2
%format vl_3
%format vl_4
%format vl_i
%format vl_i'
%format hl_0
%format hl_1
%format hl_2
%format hl_3
%format hl_4
%format imag_0
%format x_0
%format x_1
%format x_2
%format x_i
%format x_n
%format y_0
%format y_1
%format y_2
%format y_i
%format y_j
%format y_n
%format z_0
%format z_i
%format z_n
%format y_n
%format y_n
%format prefX_0
%format prefX_i
%format prefX_n
%format prefY_0
%format prefY_i
%format prefY_n
%format env_i
%format env_0
%format env_n
%format get_1
%format put_L

%format get_v
%format get_l
%format put_s
%format put_l
%format put_1
%format put_2
%format put_3
%format putRec_s
%format putRec_l
%format inj_s
%format inj_l
%format mkInj_s
%format mkInj_l

%format p_b
%format p_l
%format p_u
%format p_r
%format vl'_0
%format hl'_0
%format pat_l
%format pat_r

%format linkComp = "{\cdot}"

%format exclude = " \backslash\!\backslash "
%format backslash = "\backslash"

\makeatletter
\newcommand{\shorteq}{%
  \settowidth{\@@tempdima}{-}%
  \resizebox{\@@tempdima}{\height}{=}%
}
\makeatother

%% Journal information
%% Supplied to authors by publisher for camera-ready submission;
%% use defaults for review submission.
\acmJournal{PACMPL}
\acmVolume{1}
\acmNumber{CONF} % CONF = POPL or ICFP or OOPSLA
\acmArticle{1}
\acmYear{2018}
\acmMonth{1}
\acmDOI{} % \acmDOI{10.1145/nnnnnnn.nnnnnnn}
\startPage{1}

%% Copyright information
%% Supplied to authors (based on authors' rights management selection;
%% see authors.acm.org) by publisher for camera-ready submission;
%% use 'none' for review submission.
\setcopyright{none}
%\setcopyright{acmcopyright}
%\setcopyright{acmlicensed}
%\setcopyright{rightsretained}
%\copyrightyear{2018}           %% If different from \acmYear

%% Bibliography style
\bibliographystyle{ACM-Reference-Format}
%% Citation style
%% Note: author/year citations are required for papers published as an
%% issue of PACMPL.
\citestyle{acmauthoryear}   %% For author/year citations

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Note: Authors migrating a paper from PACMPL format to traditional
%% SIGPLAN proceedings format must update the '\documentclass' and
%% topmatter commands above; see 'acmart-sigplanproc-template.tex'.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Some recommended packages.
\usepackage{booktabs}   %% For formal tables:
                        %% http://ctan.org/pkg/booktabs
\usepackage{subcaption} %% For complex figures with subfigures/subcaptions
                        %% http://ctan.org/pkg/subcaption

\usepackage{microtype}

\usepackage{multicol}
\usepackage{amssymb}
\usepackage{stmaryrd}
% \usepackage{inconsolata}  this causes an error
\usepackage{mathtools}
\usepackage{esvect}
\usepackage{hyperref}
\usepackage{listings}
\usepackage[T1]{fontenc}
\usepackage{upquote}
%\usepackage{mathalfa}

\usepackage{enumitem}

\usepackage{xcolor}
\newcommand\myworries[1]{\textcolor{red}{\small[#1]}}
\newcommand\todo[1]{\textcolor{blue}{\small[#1]}}
\newcommand\biyacc[0]{\textsc{BiYacc}}
\newcommand\BiYacc[0]{\textsc{BiYacc}}
\newcommand\BiFlux[0]{\textsc{BiFlux}}
\newcommand\biflux[0]{\textsc{BiFlux}}
\newcommand\diff{\,\backslash\!\backslash\,}
\newcommand\dom{{\normalfont\textsc{dom}}}
\newcommand\ldom{{\normalfont\textsc{ldom}}}
\newcommand\rdom{{\normalfont\textsc{rdom}}}

\newcommand\tit[1]{\textit{#1}}
\newcommand\tits[1]{\textit{#1}\ }
\newcommand\ttt[1]{\texttt{#1}}

\newcommand\varcitet[2]{\citeauthor{#1}#2~[\citeyear{#1}]}

\newcommand\EscapeSans[1]{{-"\text{#1}"-}}

\newtheorem{prereq}{Prerequisites}

\renewcommand{\sectionautorefname}{Section}
\renewcommand{\subsectionautorefname}{Section}
\renewcommand{\subsubsectionautorefname}{Section}
\renewcommand{\figureautorefname}{Figure}
\newcommand{\lemmaautorefname}{Lemma}
\newcommand{\propositionautorefname}{Proposition}
\newcommand{\corollaryautorefname}{Corollary}
\newcommand{\definitionautorefname}{Definition}
\newcommand{\exampleautorefname}{Example}

\allowdisplaybreaks


\begin{document}

\setlength{\mathindent}{\parindent}

%% Title information
\title[Retentive Lenses]{Retentive Lenses}         %% [Short Title] is optional;
                                        %% when present, will be used in
                                        %% header instead of Full Title.
% \titlenote{with title note}             %% \titlenote is optional;
%                                         %% can be repeated if necessary;
                                        %% contents suppressed with 'anonymous'
% \subtitle{Subtitle}                     %% \subtitle is optional
% \subtitlenote{with subtitle note}       %% \subtitlenote is optional;
%                                         %% can be repeated if necessary;
%                                         %% contents suppressed with 'anonymous'


%% Author information
%% Contents and number of authors suppressed with 'anonymous'.
%% Each author should be introduced by \author, followed by
%% \authornote (optional), \orcid (optional), \affiliation, and
%% \email.
%% An author may have multiple affiliations and/or emails; repeat the
%% appropriate command.
%% Many elements are not rendered, but should be provided for metadata
%% extraction tools.

%% Author with single affiliation.
\author{Zirun Zhu}
\authornote{}          %% \authornote is optional;
                                        %% can be repeated if necessary
\orcid{}             %% \orcid is optional
\affiliation{
  \position{Research Assistant}
  \department{Information Systems Architecture Science Research Division}             %% \department is recommended
  \institution{National Institute of Informatics}           %% \institution is required
  \streetaddress{2-1-2 Hitotsubashi, Chiyoda}
  \city{Tokyo}
  % \state{State2b}
  \postcode{101-8430}
  \country{Japan}                   %% \country is recommended
}
\email{zhu@@nii.ac.jp}         %% \email is recommended
\affiliation{
  \position{Student}
  \department{Department of Informatics}              %% \department is recommended
  \institution{SOKENDAI}            %% \institution is required
  \streetaddress{Shonan Village}
  \city{Hayama}
  \state{Kanagawa}
  \postcode{240-0193}
  \country{Japan}                    %% \country is recommended
}
\email{zhu@@anet.soken.ac.jp}          %% \email is recommended


\author{Hsiang-Shang Ko}
% \authornote{with author2 note}          %% \authornote is optional;
                                        %% can be repeated if necessary
% \orcid{nnnn-nnnn-nnnn-nnnn}             %% \orcid is optional
\affiliation{
  \position{Position2a}
  \department{Information Systems Architecture Science Research Division}             %% \department is recommended
  \institution{National Institute of Informatics}           %% \institution is required
  \streetaddress{2-1-2 Hitotsubashi, Chiyoda}
  \city{Tokyo}
  % \state{State2a}
  \postcode{101-8430}
  \country{Japan}                   %% \country is recommended
}
\email{hsiang-shang@@nii.ac.jp}         %% \email is recommended

%% Author with two affiliations and emails.
\author{Zhenjiang Hu}
% \authornote{with author2 note}          %% \authornote is optional;
                                        %% can be repeated if necessary
% \orcid{nnnn-nnnn-nnnn-nnnn}             %% \orcid is optional
\affiliation{
  \position{Professor}
  \department{Information Systems Architecture Science Research Division}             %% \department is recommended
  \institution{National Institute of Informatics}           %% \institution is required
  \streetaddress{2-1-2 Hitotsubashi, Chiyoda}
  \city{Tokyo}
  % \state{State2a}
  \postcode{101-8430}
  \country{Japan}                   %% \country is recommended
}
\email{hu@@nii.ac.jp}         %% \email is recommended
\affiliation{
  \position{Department Chair}
  \department{Department of Informatics}             %% \department is recommended
  \institution{SOKENDAI (The Graduate University for Advanced Studies)}            %% \institution is required
  \streetaddress{Shonan Village}
  \city{Hayama}
  \state{Kanagawa}
  \postcode{240-0193}
  \country{Japan}                    %% \country is recommended             %% \country is recommended
}
\email{}         %% \email is recommended

%% Abstract
%% Note: \begin{abstract}...\end{abstract} environment must come
%% before \maketitle command
\begin{abstract}
% Researchers in the field of \emph{bidirectional transformation}s have studied problems on data synchronisation for a long time and proposed various properties, of which \emph{well-behavedness} is the most fundamental.
% However, well-behavedness is not enough for characterising the results of an update (performed by $\mathit{put}$),
% % and well-behaved \emph{lenses} can still exhibit unintended behaviour,
% in particular regarding what information is retained in the updated source.
% The root cause of the wide range of |put| behaviour is that the only property guaranteeing the retention of source information only requires that the whole source should be unchanged if the whole view is.

% In this paper we propose a new property \emph{retentiveness}, which enables us to directly reason about the retention of source information locally.
% Central to our formulation of ret
% entiveness is the notion of \emph{links}, which are used to relate fragments of sources and views.
% These links are passed as additional input to the extended $\mathit{put}$ function, which produces a new source in a way that preserves all the source fragments attached to the links.
% % We extend the $\mathit{put}$ function to accept links as additional input, and produce a new source in a way that preserves all the source fragments attached to the links.
% %
% We validate the feasibility of retentiveness by designing a domain-specific language (DSL) supporting mutually recursive algebraic datatypes, in which the local consistencies between the source and the view can be described easily.
% We prove that the pair of $\mathit{get}$ and $\mathit{put}$ functions generated from any program in the DSL satisfy retentiveness.
% We evaluate the usefulness of retentiveness by presenting examples in two different research areas: resugaring and code refactoring, and discuss the potential usage in other settings.
\end{abstract}


%% 2012 ACM Computing Classification System (CSS) concepts
%% Generate at 'http://dl.acm.org/ccs/ccs.cfm'.
\begin{CCSXML}
<ccs2012>
<concept>
<concept_id>10011007.10011006.10011008</concept_id>
<concept_desc>Software and its engineering~General programming languages</concept_desc>
<concept_significance>500</concept_significance>
</concept>
<concept>
<concept_id>10003456.10003457.10003521.10003525</concept_id>
<concept_desc>Social and professional topics~History of programming languages</concept_desc>
<concept_significance>300</concept_significance>
</concept>
</ccs2012>
\end{CCSXML}

\ccsdesc[500]{Software and its engineering~General programming languages}
\ccsdesc[300]{Social and professional topics~History of programming languages}
%% End of generated code


%% Keywords
%% comma separated list
\keywords{bidirectional transformations, domain-specific languages, functional programming}  %% \keywords are mandatory in final camera-ready submission


%% \maketitle
%% Note: \maketitle command must come after title commands, author
%% commands, abstract environment, Computing Classification System
%% environment and commands, and keywords command.
\maketitle

\section{Introduction}
\label{sec:intro}
Every now and then, we need to write pairs of programs that synchronise data.
Typical examples include:
\begin{itemize}
  \item view querying and updating in relational databases \cite{Bancilhon1981,Dayal:1982:CTU:319732.319740,Gottlob:1988:PUS:49346.50068} (keeping a database and its view in sync),
  \item parsers and printers as front ends of compilers \cite{Matsuda:2007:BTB:1291151.1291162,Rendel:2010:ISD:2088456.1863525,Zhu:2016:PRP:2997364.2997369} (keeping program text and its internal representation in sync), and
  \item designing tools that convert documents from one format to another \cite{macfarlane2013pandoc} (keeping the document contents in sync).
\end{itemize}
All these pairs of programs should satisfy some properties in order to work in harmony.
Such properties have been studied by researchers in the field of \emph{bidirectional transformations} \cite{czarnecki2009bidirectional} (BXs for short); of these properties, the notion of \emph{well-behavedness} \cite{foster2005combinators, Stevens2008} is the most fundamental, and various bidirectional programming languages and systems have been developed in different settings, in the hope of helping the user to write pairs of well-behaved programs easily~\cite{foster2005combinators,Bohannon:2008:BRL:1328438.1328487,barbosa2010matching,Hofmann:2012:EL:2103656.2103715,Pacheco:2014:BBF:2643135.2643141,ko2016bigul}. \todo{add papers for graph and model transformations}

In this paper we focus on a particular BX model, \emph{asymmetric lenses}, and introduce their well-behavedness in Stevens's terminologies~\cite{Stevens2008}.
Synchronisation is about transforming multiple pieces of data such that consistency is \emph{restored} among them, i.e., a \emph{consistency relation} is satisfied after the transformation.
In the asymmetric setting, we consider a binary consistency relation~$R$ between types $S$~and~$V$,
where $S$~contains more information and is called the \emph{source} type, and $V$~contains less information and is called the \emph{view} type.
Since $S$~contains more information than~$V$, we expect that there is a function |get :: S -> V|\,\footnote{We use (sometimes extended) Haskell notation throughout the paper.} that extracts a consistent view from a source; this |get| function, as reasoned by Stevens, should coincide with~|R| extensionally --- that is, |s :: S| and |v :: V| are related by~|R| if and only if |get s = v|.%
\footnote{Therefore it is only sensible to consider functional consistency relations in the asymmetric setting.}
Consistency restoration is nontrivial in only one direction, and is performed by another function |put :: S -> V -> S|, which produces an updated source that is consistent with the input view and can retain some information of the input source.
Well-behavedness consists of two properties regarding the restoration behaviour of |put| with respect to |get| (and thus the consistency relation~|R|):
% the correctness requires
\begin{align}
|get (put s v)| &= |v|
\tag{Correctness}\label{equ:correctness} \\
|put s (get s)| &= |s|
\tag{Hippocraticness}\label{equ:hippocraticness}
\end{align}
Correctness states that necessary changes must be made so that inconsistent data become consistent again,
while hippocraticness says that if two pieces of data are already consistent, the restorer (|put|) must not make any change.
A pair of |get| and |put| functions, called a \emph{lens}, is \emph{well-behaved} if it satisfies both properties.

%
Despite being concise and natural, the two well-behavedness properties are not enough for characterising the results of the update (performed by |put|), and well-behaved lenses can still exhibit unintended behaviour, in particular regarding what information is retained in the updated source.
%
To illustrate, let us consider the synchronisation between concrete and abstract syntax trees (CSTs and ASTs) of arithmetic expressions, represented as algebraic datatypes defined in \autoref{fig:running_example_datatype_def}.
The CSTs can be either expressions (|Expr|, containing additions and subtractions) or terms (|Term|, including numbers, negated terms and expressions in parentheses), and all constructors have an annotation field (|Annot|);
in comparison, the ASTs do not include explicit parentheses, negations, and annotations.
Consistency between CSTs and ASTs is given directly in terms of the |get| functions in \autoref{fig:running_example_datatype_def}.
We start from a consistent pair of |CST| and |AST| shown in \autoref{fig:running_example}, where |CST| represents ``$0-1+-2$'', whose sub-term ``$-2$'' appears in |AST| desugared as ``$0-2$''.
If |AST| is modified into |AST'| (also in \autoref{fig:running_example}) where the two non-zero numbers are exchanged, there is very little restriction on what a well-behaved |put| can do; for example, as listed on the right-hand side of \autoref{fig:running_example}, it can produce:
\begin{itemize}
  \item |CST_1|, where the literals $1$~and~$2$ are swapped, along with their annotations;
  \item |CST_2|, which represents the same expression as |CST_1| except that all annotations are removed;
  \item |CST_3|, which represents ``$-2+(0-1)$'' and retains all annotations, in effect swapping the two subtrees under |Plus| and adding two constructors, |Term| and |Paren|, to satisfy the type constraints.
%  \footnote{The left subtree of |EAdd| is required to be of type |Expr|. Since |Neg| is of type |Term|, the constructor |ET| is added to inject it to the type of |Expr|. |Paren| is added for the same reason.}
\end{itemize}


\begin{figure}[t]
\setlength{\mathindent}{0em}
\begin{minipage}[t]{0.45\textwidth}
%
\begin{code}
data Expr   =  Plus   Annot  Expr  Term
            |  Minus  Annot  Expr  Term
            |  Term   Annot  Term

data Term   =  Lit    Annot  Int
            |  Neg    Annot  Term
            |  Paren  Annot  Expr

type Annot  =  String

data Arith  =  Add  Arith  Arith
            |  Sub  Arith  Arith
            |  Num  Int
\end{code}
\end{minipage}%
%
\begin{minipage}[t]{0.45\textwidth}
\begin{code}
getE :: Expr -> Arith
getE (Plus   _ e  t) = Add  (getE e) (getT t)
getE (Minus  _ e  t) = Sub  (getE e) (getT t)
getE (Term   _    t) = getT t

getT :: Term -> Arith
getT (Lit     _ i  ) = Num i
getT (Neg     _ t  ) = Sub (Num 0) (getT t)
getT (Paren   _ e  ) = getE e
\end{code}
\end{minipage}
\caption{Datatypes for concrete and abstract syntax trees and the consistency relation between them as two |get| functions.}
\label{fig:running_example_datatype_def}
\end{figure}
%

\begin{figure}[t]
% \renewcommand{\hscodestyle}{\small}
\setlength{\mathindent}{0em}
\begin{minipage}[t]{0.5\textwidth}
\begin{code}
CST =
  Plus "an add"
    (Minus "a sub"
      (Term "a Term" (Lit "0 in sub" 0))
      (Lit "1 in sub" 1))
    (Neg "a neg" (Lit "2 in neg" 2))

AST =
  Add  (Sub (Num 0) (Num 1))
       (Sub (Num 0) (Num 2))


AST' =
  Add  (Sub (Num 0) (Num 2))
       (Sub (Num 0) (Num 1))
\end{code}
\end{minipage}%
%
\begin{minipage}[t]{0.5\textwidth}
\begin{code}
CST_1 =
  Plus "an add"
    (Minus "a sub"
       (Term "a Term" (Lit "0 in sub" 0))
       (Lit "2 in neg" 2))
    (Neg "a neg" (Lit "1 in sub" 1))

CST_2 =
  Plus ""
    (Minus ""  (Term "" (Lit "" 0))
              (Lit "" 2))
    (Neg  ""  (Lit "" 1))

CST_3 =
  Plus "an add"
    (Term ""
      (Neg "a neg" (Lit "2 in neg" 2 )))
    (Paren ""
      (Minus "a sub"
         (Term "a Term" (Lit "0 in sub" 0 ))
         (Lit "1 in sub" 1 ))
\end{code}
\end{minipage}
\caption{|CST| is updated in three ways in response to the change from |AST| to |AST'|.}
\label{fig:running_example}
\end{figure}
% recover font size in code
% \renewcommand{\hscodestyle}{}
The root cause of the above wide range of |put| behaviour is that while lenses are designed to enable retention of source information (annotations and the negation syntactic sugar in the above example), the only property guaranteeing that is hippocraticness, which only requires that the \emph{whole} source should be unchanged if the \emph{whole} view is.
This is too ``global'' in most cases, where we want a more ``local'' hippocraticness property saying that if \emph{parts} of the view are unchanged then the \emph{corresponding parts} of the source should be retained.
%Rather than let the user randomly guess, we approach the problem by proposing a new property, \emph{retentiveness}, which can directly tell the user what is retained after the update (|put|).
In this paper we propose a new \emph{retentiveness} property as a form of local hippocraticness.
\myworries{Central to our formulation of retentiveness is the notion of \emph{links}, which are used to relate fragments of sources and views.}
A retentive |put| function accepts links as additional input, and can produce a new source in a way that preserves all the source fragments attached to the links.
For the example in \autoref{fig:running_example}, we can produce either |CST_1|, |CST_2|, or |CST_3| by just passing a retentive |put| function different links marking which parts of the source should be retained.

% \begin{itemize}
%   \item | ls_1 = [] |
%   \item | ls_2 = [  ((EAdd,[])  , (Add,[])),  ((ESub,[1]) , (Sub,[0])) ,  ((Neg,[2])  , (Sub,[1])),|
%                  |  ((ET,[1,1]) , ((),[0,0])) ,  ((EN,[1,1,1]) , (N,[0,0]))] |
%   \item | ls_3 = [...] |
% \end{itemize}
%

% CONSIDER REMOVE THE PARAGRAPH?
% The reader familiar with bidirectional transformations may immediately recall the notion of \emph{alignment} \todo{\cite{barbosa2010matching}}, and may wonder why we need retentiveness after all.
% However, the reader will come to the conclusion that existing BXs framework and their alignment strategies are not capable of handling our \todo{(more complicated examples in \autoref{sec:case_studies})} as reading through the paper.
% \todo{Briefly explained in advance, we will handle synchronisations between algebraic datatypes rather than lists, trees, or some other containers types \cite{Abbott-containers}.}

\todo{to be rewritten in the end}
Contributions of this work is summarised as follows:
\begin{itemize}
  \item We propose the retentiveness framework, which is built upon the notion of links. (\autoref{sec:links_and_retentiveness})
%
  \item To validate the feasibility of retentiveness, we design a lightweight domain-specific language, and show the program for solving the problem in \autoref{fig:running_example}.
  (\autoref{sec:dsl})
        Then we present the syntax and semantics of the DSL, in order to prove that any program written in our DSL give rise to a pair of |get| and |put| satisfying retentiveness.
        The proof is shown in \autoref{sec:proof}.
%
  \item To show the usefulness of the retentiveness, we present examples in two different areas: code refactoring~\cite{fowler1999refactoring} and resugaring~\cite{pombrio2014resugaring,Pombrio:2015:HRC:2784731.2784755}, and discuss the potential usage in other settings. (\autoref{sec:case_studies})
%
\end{itemize}


\todo{getput and putget are trivial to satisfy. On the other hand, (in state-based frameworks) some laws such as $\tit{PutPut}$ is too strong in many scenarios and prohibits many useful update.
The reader may note that existing bidirectional languages can also be extended to be aware of retentiveness.
}





\section{Correspondence Links and Retentiveness}
\label{sec:links_and_retentiveness}

In this section we will develop a definition of retentiveness, formalising the statement ``if parts of the view are unchanged then the corresponding parts of the source should be retained'' in a step-by-step manner.
%
In \autoref{subsec:intuitive_retentiveness}, we demonstrate how parts of a source and a view are related by links, and how these related source fragments are kept in the new source in an informal but intuitive way.
% This seems to lead to a corollary which is yet another rephrased version of GetPut.
% However, this is not true and reasons are given at the end of this subsection.
% in the beginning of \autoref{subsec:problem_of_retentiveness}.
Then in \autoref{subsec:abstract_framework}, we formalise the framework of retentive lenses and retentiveness, showing that hippocraticness can be derived from retentiveness.


\subsection{Intuition}
\label{subsec:intuitive_retentiveness}

As mentioned in \autoref{sec:intro}, the core idea of retentiveness is to decompose the source and view into small parts, or \textit{regions}, and use links to relate regions of the source and view; this is inspired by the notion of \emph{provenance} from the database community.
(See \autoref{subsec:provenances} for detailed explanation.)
For example, \autoref{fig:get_and_swap} shows the set of \emph{consistency links} between |CST| and |AST| in \autoref{fig:running_example}, indicating, for each part of the view, from which part of the source it is \myworries{computed} by the |getE| and |getT| functions in \autoref{fig:running_example_datatype_def}.
This set of links can be updated along with the view by the user, maintaining the relationship between the updated view and the source --- for example, if we swap the subtrees |Num 1| and |Num 2| in |AST| to obtain |AST'|, then the links should be updated as well (\myworries{as vertical links in blue representing equality show}), where the two subtrees in |AST'| are still linked to where they originally come from in |CST|. (\myworries{use: can still track where they originally come from?})
These two links serve as instructions to a retentive |put| that, in the updated |CST_1| it produces, the parts corresponding to the subtrees |Num 2| and |Num 1| should be exactly those fragments of |CST| linked with the subtrees.
That is, if we find the consistency links between |CST_1| and |AST'|, we will be able to locate the corresponding parts of |CST_1| and assert that they should be the same as those fragments of |CST|; this is visualised in \autoref{fig:swap_and_put}, and in particular we draw lines between |CST| and |CST_1| to say that the linked parts are the same.



% the consistency links between the right subtree of |Plus| and the right subtree of |Add| and  are depicted
\begin{figure}[tbh]
\includegraphics[scale=0.35,trim={1cm 5.2cm 1cm 2.3cm},clip]{pics/get_and_swap.pdf}
\caption{Links between |CST| and |AST|, and between |AST| and |AST'|. Annotations in the |CST| are omitted as ellipses and only parts of the regions and consistency links are depicted. Data in grey background are regions and are considered as whole parts. Consistency links are updated along with the swap operation on |AST| so that in |AST'| subtrees can still be tracked to where they originally come from in |CST|.}
\label{fig:get_and_swap}
\end{figure}
% For example, |Neg "..."| is connected to |Sub (Num 0)|.

\begin{figure}[tbh]
\includegraphics[scale=0.35,trim={1cm 3cm 1cm 2.3cm},clip]{pics/swap_and_put.pdf}
\caption{The result of a retentive |put|. The links between |CST| and |AST'| are sent to |put_1| to produce |CST_1|. The links between |CST_1| and |AST'| are consistency links produced by |get CST_1|. We also draw vertical lines to connect equal parts of |CST| and |CST_1|.}
\label{fig:swap_and_put}
\end{figure}



\subsection{Links}
\label{subsec:links}
\todo{Y: Revise the rest of this section to emphasise links are local consistency.}
Before formalising retentive lenses motivated in our concrete examples, we will first formalise the idea of links. In our concrete examples of algebraic data types, a link connects regions in the source and the view. In this subsection, we generalise \textit{having a region} to be arbitrary \textit{properties} satisfied by the source or the view, and links become pairs of properties satisfied by the source and the view respectively. 

\begin{definition}[properties]
\label{def:properties}
A \emph{property}~|p : P| on a set~$X$ is a value equipped with a family of types $x \models p$ where $x$~ranges over~$X$, which are considered as proofs that $x$ satisfies $p$. 

For a list of properties |l = [ p_1, p_2, ..., p_n ]|, we define $x \models^* l$ to be $(x \models p_1) \times (x \models p_2) \times ... \times (x \models p_n)$, i.e., a type proves $x$ satisfies every element of $l$.
\end{definition}

For our running example shown in \autoref{fig:running_example}, there are two types |Expr| and |Term|.
Let us take |Expr| for the set~|X| and |cst|\myworries{Y: rename values to have lower-letter names in the figure.} for~|x|.
Let |p| be a predicate (property) that |cst| has a region |Plus a _ _|\,. Then a proof of $x \models p$ can be represented as a path in |cst| pointed to a subtree matches |Plus a _ _|\,. In this case, the proof is simply an empty path pointing to the root of |cst|.

\begin{definition}[uniquely identifying]
We say a list~$l$ of properties on~$X$ \emph{uniquely identifies} $X$ exactly when: if both $x \models^* l$ and  $x' \models^* l$ are inhabited, then $x = x'$. That is, there is at most one element of~$X$ satisfying~$l$.
\end{definition}

%We abbreviate $(p : P) \times (x \models p)$ as $P_x$.


For example, let $l = [\: x \geq 2, \ x \leq 2 \:] $ be a list of properties on |Nat|. $l$ is uniquely identifying since there is only one element $2$ of |Nat| satisfying $l$.  For a counter example, in \autoref{fig:running_example}, a (singleton) list of properties $l = [$ has a region |Plus a _ _| $]$ on type |Expr| does not uniquely identify |cst|, since it is easy to verify that |cst_1 /= cst| also satisfies |l|.
Actually, we can never uniquely identify an $x : \mathit{Expr}$ with only properties asserting $x$ has some regions. In the next section, we will introduce more properties to uniquely identify algebraic data types.


\begin{definition}[links]
\label{def:links}
For a source type |S|, a view type |V|, a type |P| of properties on |S|, and a type |Q| of properties on |V|, we define the type of \textit{links} between |S| and |V| to be 
\[|Links s v| = \big(\;l : |List (P, Q)|\,, \, s \models^* (|map fst l|)\,, \,|v| \models^* (|map snd l|)\;\big)\]
\end{definition}

Essentially, links between $s$ and $v$ are pairs of properties $(p, q)$ with a proof that $s$ satisfies $p$ and a proof of $v$ satisfies $q$. This can be seen more clearly from the following function:
\begin{code}
zipLinks : Links s v -> List (p : P,  q : Q,  s{-"\!\models"-}p,  v{-"\!\models"-}q)
zipLinks ([], Unit, Unit) = []
zipLinks ((p, q) : xs, (sp, sps), (vq, vqs)) = (p, q, sp, vq) :: zipLinks xs sps vqs 
\end{code}

\myworries{Y: Give another example of links here? Still the ADT example or something else?}

\subsection{Retentive Lenses}
\label{subsec:abstract_framework}

With the notion of links, we can define our retentive lenses now:
\begin{definition}[retentive lenses]
For a source type |S| with property type |P|, a view type |V| with property type |Q|, and let |T| to be a subset of $(s : S) \times (|v| : V) \times (|Links s v|)$. A retentive lens on |T| is a pair of fucntions |get| and |put| of type:
\begin{align*}
|get| &: (s : S) \to (|v| : V) \times (|Links|\; s\; v)  \\
|put| &: T \to S
\end{align*}
such that |get s = (v, l)| implies $|(s, v, l)| \in T$. 
\end{definition}

In this definition, |get| takes a source as input and produces a view and links between them. The subset |T| is introduced to be the domain of |put| so that |put| can be partial on $(s : S) \times (|v| : V) \times (\mathit{Links} \; s \; v)$. Effectively, |put| takes a source, a view and links between them as input, and produces an updated source.

In addition, a retentive lens should satisfy the following set of laws:
\begin{enumerate}
\setlength{\itemsep}{0.5em}
\setlength{\parskip}{0.5em}
\setlength{\parsep}{0pt}     
\item \label{law:cor} \textit{Correctness}: if |get (put (s, v, l)) = (v', l')| then |v = v'|.

  This law says that |put| should correctly ``put'' a view into a source in the sense that |get| the updated source will result that view. This law is also a part of the well-behavedness of asymmetric lenses.

\item \label{law:ret} \textit{Retentivness}: if |get (put (s, v, l)) = (v', l')|, then |eraseSProofs (zipLinks l)| forms a subset of |eraseSProofs (zipLinks l')|, where |eraseSProofs = map (\(p, q, sp, vq) -> (p, q, vq))| is a function discards all proofs about $s$ in links $l$. 

  This law says that |put| should retain its link argument, in the sense that all links in $l$ between the old source $s$ and the view $v$ should be preserved for the updated source and the view ($v' = v$ by \textit{Correctness}), while the updated source is allowed to satisfy properties in $l$ in possibly different ways.
 
\item \label{law:comp} \textit{Completeness}: if |get s = (v, (l, ps, pv))|, then |map fst l| is a list of properties uniquely identifying |S|, and |map snd l| uniquely identifies |V|. 

  This law requires every part of the source should correspond to some part of its view, and vice versa. This may sound unnatural in an asymmetric setting where the view usually contains less information than the source.  In fact, what this law says is those parts present only in the source should still logically correspond to some parts of the view, which is not an unrealistic assumption. For example, comments in a concrete syntax tree should correspond to the node being commented. The motivation for this law is that we want links generated by |get| to be \textit{complete} in the sense that the user can use these links to retain every part of the source.  \myworries{Y: I don't know how to explain it clearly because this law is still plausible for me.}
\end{enumerate}

With these three laws, we can show that a retentive lens also satisfy the \textit{Hippocraticness} law for asymmetric lenses.

\begin{theorem}[Hippocraticness]
For a retentive lens, 
\[ |get s = (v, l)| \quad \Rightarrow \quad |put (s, v, l) = s|. \]
\end{theorem} 
\begin{proof}
Let |s' = put (s, v, l)|, |get s' = (v', l')|, |l' = (pq, sp, vq)|, and |srcProperties (pq, _, _) = map fst pq| is a function extracts all properties about the source from |Links s v|. 

From \textit{Retentiveness} (\ref{law:ret}), we know |srcProperties l| is a subset of |srcProperties l'|. 
Since $|sp| \in (s' \models^* |srcProperties l'|)$. We know |s'| should also satisfy |srcProperties l|, which is already satisfied by $s$. Now by the \textit{Completeness} of |l| (\ref{law:comp}), we conclude |s' = s| because |srcProperties l| uniquely identifies |S|.
\end{proof}

\begin{example}
Every well-behaved asymmetric lens |(get : S -> V, put : S -> V -> S)| can be lifted into a retentive lens by defining a trivial link type. Let the type of properties on |S| be |P = S|, and the type of properties on |V| be |Q = V|.  An |s : S| satisfies a property |p : P| iff.\, s = p, and |v : V| satisfies |q : Q| iff.\, |v = q|. 

The retentive |get_r| can be defined as |get_r s = (get s, ([(s, get s)], refl, refl))|. Clearly the returned links satisfy \textit{Completeness} (\ref{law:comp}). To define retentive |put_r|, we first define its domain |T| to be $T = \big\{\; (\_, \_, (pq, \_, \_)) \in (s : S,\; v : V, \; |Links s v|) \mid \text{for all |(p, q)| in pq, |get p = q|} \big\}$. Now we can simply define:
\begin{align*}
& |put_r : T -> S| \\
& |put_r (s, v, l) = put s v|
\end{align*}
\textit{Correctness} (\ref{law:cor}) of the retentive lens is guaranteed by the \textit{\ref{equ:correctness}} of |(get, put)|. \textit{Retentiveness}~(\ref{law:ret}) is guaranteed by the specific choice of |T| and \textit{\ref{equ:hippocraticness}} of |(get, put)|.
\end{example}


\begin{example} Let |Vec n Int| be the source and the view type. We can define a retentive lens whose \textit{get} direction sorts the source, and |put (s, v, l)| produces a vector |s'| whose elements form the same set as those of |v|, and |l| is used to specify some elements in $v$ come from the original source $s$ and their positions in $s$ should be kept in $s'$. 

Properties on the source and the view will both have type |(Fin n, Int)|, a pair of an index and an integer. A vector |x| satisfies a property |(p, n)| if and only if the |p|-th element of |x| is |n|, i.e., |x {-" \models "-} p| equals |( (x!(fst p)) {-"\! \equiv "-} (snd p))|.

|get s| returns a sorted vector |v| of |s| and a |Links s v| whose property list is:
\[ [\;\big(i, x),\; (\mathit{pos}(i), x)\big) \;||\; x\text{ is the i-th element of } s \;] \]
where $\mathit{pos}(i)$ is the position |s ! i| goes to in the sorted result.

|put (s, v, l)| returns a vector $s'$ such that |sort s' = v| and for any $|(i, x)| \in |srcProperties l|$, |s' ! i = x|. This |put| can be implemented by fixing values required by links in their required positions first and then placing all other values of |v| arbitrarily. 
\end{example}


\section{A DSL for Retentive Lenses of ADTs}
\label{sec:dsladt}
In this section we introduce a lightweight DSL for writing retentive lenses between algebraic data types. In this DSL, users basically only write |get| functions as inductively-defined consistency relations. Then our compiler can derive compatible retentive lenses in Haskell, consisting of |get| and |put| functions with suitable types of properties and links.   

\begin{figure}[htb]
\begin{multicols}{2}
\begin{code}
Expr <---> Arith
  Plus   _  x  y  ~  Add  x  y
  Minus  _  x  y  ~  Sub  x  y
  Term   _  t     ~  t       ;

Term <---> Arith
  Lit     _  i   ~  Num  i
  Neg     _  r   ~  Sub  (Num 0)  r
  Paren   _  e   ~  e             ;
\end{code}
\end{multicols}
\caption{A program in our DSL for ADTs in \autoref{fig:running_example_datatype_def}.}
\label{fig:dsl_example}
\end{figure}

\autoref{fig:dsl_example} is a program in our DSL for synchronising algebraic data types in \autoref{fig:running_example_datatype_def}. This program describes two consistency relations: one between |Expr| and |Arith|, and one between |Term| and |Arith|. Each consistency relation is defined as a set of inductive rules. Each rule |pat_S ~ pat_V| says if two trees |s : S| and |v : V| are consistent provided that they match with |pat_S| and |pat_V| respectively and their subtrees marked by the same variable name in both patterns are consistent. For example, by |Plus _  x  y  ~  Add  x  y|, we define if |x_s| and |x_v| are consistent, |y_s| and |y_v| are consistent, then |Plus s x_s y_s| and |Add x_v y_v| are consistent for any value |s| since it corresponds to a wildcard in |Plus _  x  y|. Additionally, two values of base types such as |Int| are considered as consistent iff.\, they are equal. 

The formal grammar of our DSL is showed in \autoref{fig:dsl_syntax}. A program consists of two parts: definitions of algebraic data types in Haskell's syntax, and consistency relations between these data types. To guarantee a defined consistency relation are actually a function, thus can be interpreted as |get|, our DSL requires all patterns on the source side are non-overlapping and patterns on the view side do not use wildcards.

\subsection{Generated links and lenses}
To derive a retentive lens based on a program in our DSL, our compiler needs to derive a suitable type of |Links|. In our formalisation, the type of links is determined by types of properties on the source and the view. As motivated by examples in \autoref{subsec:intuitive_retentiveness}, \textit{regions} of ADTs will be our properties on the source and the view. More formally, for any source type or view type |T| in a program, if $|pats|(T) = \{\;|pat_1|, \dots, |pat_n|\;\}$ are all patterns associated with |T| in this program, we derive types |TRegion| to be:
\begin{align*}
|TRegion|  = |Complement|(|pat_1|) + \dots + |Complement|(|pat_n|)
\end{align*} 
where $(+)$ is the sum type and $|Complement|(pat_i)$ is the product of types of wildcards in |pat_i|. For example, if a constructor |C : Int -> String -> Int -> T| and |pat_i = C _ _ x| then $|Complement|(pat_i) = |Int| \times |String|$. Basically, a region is data discarded by the corresponding pattern\footnote{We can tell to which pattern a region corresponds because |TRegion| is a tagged union.} of that region.

For a |t : T| and |r : TRegion|, $t \models r$ should be the type of proofs that |t| has a subtree matches with the pattern and data of |r|. Although we could precisely encode this proposition as a type in Haskell with \myworries{Y: those hacks I don't know what exactly is the name}, our compiler simply defines $t \models r$ to be the type of paths in |T|, which are valid proofs only when they lead to a subtree matches with the region |r|. Our compiler guarantees generated |get| functions always produce valid paths as proofs and |put| functions are considered defined only when the input is valid. This simplification is made majorly for making generated lenses easier to use for programmers.

As mentioned in \autoref{subsec:links}, properties in the form of \textit{having a region} cannot fully identify a value of an ADT. 
For example, if |t_1| is a subtree of |t_2|, then whenever |t_1| has a region, |t_2| also has it. And also, |t_3| could have the same set of regions as |t_4| while these regions are assembled in a different way to form a tree. 
Hence, to make our set of properties complete enough to uniquely identify a value, besides properties saying \textit{having a region}, we also want properties saying \textit{having nothing else}, or equivalently \textit{region |r_1| is the root}, and also \textit{region |r_1| is the $i$-th child of region |r_2|}. 

To formalise these new properties, note that they are \textit{second order} properties because they are asserting about regions, which are also properties by themself. To allow such dependency, we need to extend our formalisation in \autoref{subsec:links} a little bit. For a list |l : List P| of properties, its element |p| could dependent on its predecessors in |l|. Let |p_k| be |l|'s |k|-th property and let $0 \leq d_{k,1}, \cdots, d_{k, n} < k$ be the indices of |p_k|'s dependencies. The definition of |x| satisfies the list |l| becomes a dependent product type, where proofs of dependencies are supplies as arguments, as in:
\[x \models^* l  =  \bigg(|pf|_1 : x \models p_1,\;\;\; \cdots,\;\;\; |pf|_k : \big((x \models p_k) \; |pf|_{d_{k,1}}\; \dots \; |pf|_{d_{k,n}}\big), \;\;\;\cdots\bigg)\]
For each individual |p_k|, the type of $x \models p_k$ is $T_{d_{k, 1}} \rightarrow \cdots \rightarrow T_{d_{k, 1}} \rightarrow |Type|$, where |T_i| is the type of the |i|-th component of dependent pair above.  

Now let us go back to define properties for ADTs. For a type |T|, we define the type of second-order properties to be:
\begin{align*}
|data TProperty2 = IsRoot Int || IsNthChild Int Int Int|
\end{align*}
If a list |l| of properties contains |IsRoot i| as an element, then |i|-th element of |l| should be a |p_i : TRegion|, and the element |IsRoot i| asserts that the region proving |p_i| should be the root of the tree. On the other hand, |IsNthChild i j n| means that the region proving |i|-th property should be the |n|-th child of the region proving |j|-th property in the list. 

Similar with what do for |TRegion|, our implementation does not exactly encode the meaning of a second order property as a Haskell type to be the type of its proofs. In fact, our Haskell implementation simply treats the unit type as the type of proofs for second order properties. And a ``proof'', namely the unit, is valid only when the referred regions indeed satisfy that second order property. Again, our |get| functions only produce valid ``proof''s and our |put| functions are only defined with valid ``proof''s as input, and they could check whether their input are valid at runtime.

With second order ones, our toolbox of properties can uniquely identifies an ADT. For a type |T|, we define:
\begin{align*}
|TProperty| \quad &= \quad|TRegion| + |TProperty2| \\ 
|TProof| \quad &= \quad |TPath| + |Unit|
\end{align*}
where |TPath| is the type of paths in |T|. The signatures of |get| and |put| are\footnote{|get| and |put| directly use links in the |zipLinks| form to avoid the dependent pair between properties and proofs in \autoref{def:links}.}:
\begin{align*}
|get| &: S |->| (V, |List (SProperty, VProperty, SProof, VProof|)) \\
|put| &: (S, V, |List (SProperty, VProperty, SProof, VProof|)) |->| S
\end{align*}

For each consistency relation between |S| and |V| defined in a program of our DSL, our compiler will generate a retentive lens with the above signatures.

\begin{figure}
\begin{minipage}{.4\textwidth}
%
\begin{code}
Program   ::=  {HsTypeDec}{-"\!\!"-}* CRels

CRels     ::=  {CRel ';'}{-"\!"-}+

CRel      ::=  Type <---> Type
               {CRelBody}{-"\!"-}+
CRelBody  ::=  Pat '~' Pat
\end{code}
\end{minipage}%
%
\begin{minipage}{0.02\textwidth}
\quad
\end{minipage}%
%
\begin{minipage}{0.4\textwidth}
\begin{code}
Pat  ::=  Constructor {Pat}{-"\!\!"-}*
     |    Constant
     |    Variable
     |    '_'
     |    '(' Pat ')'
\end{code}
\end{minipage}

% Type     ::=  Constructor
%  |     Pat '~' Pat
% '<=='  {Variable '~' Variable}+

\caption{Grammar of our DSL. ``*'' and ``+'' are Kleene star and plus representing zero-or-more occurrences and one-or-more occurrences. Symbols wrapped in single quotes, e.g., |'_'|, are terminal symbols.}
\label{fig:dsl_syntax}
\end{figure}
%

\end{document}
