\section{Related Work and Discussions}
\label{sec:related_work}

\todo{\cite{martins2014generating} also uses links to trace source information... the overview case analysis on put looks quite similar.}

\subsection{Alignment}
\label{subsec:alignment}

Our work on retentive lenses with links is closely related to the research on alignment in bidirectional programming.

% In BX communities: various approaches entitled alignment have been proposed to enhance retentiveness
%\subsubsection{Dawn of BX Languages and Alignment Strategies for Lists}
%From the dawn of the bidirectional programming languages~\cite{foster2007combinators}, \emph{alignment} has been recognised as an important problem when dealing with synchronisation of lists --- when the view elements are somehow modified (e.g., inserted, deleted, and reordered), which source elements should be matched with the new view elements and updated correspondingly?
%various \emph{alignment} strategies have been proposed to make the results produced by \emph{put} more satisfactory when there are some structural changes in views.

The earliest lenses \cite{foster2007combinators} only allow source and view elements to be matched positionally --- the $n$-th source element is simply updated using the $n$-th element in the modified view.
Later, lenses with more powerful matching strategies are proposed, such as dictionary lenses \cite{Bohannon:2008:BRL:1328438.1328487} and their successor matching lenses \cite{barbosa2010matching}.
%We briefly discuss matching lenses here.
%As the name implies, dictionary lenses match source and view elements based on their (unique) \emph{key} values specified by programmers.
In matching lenses, the $\mathit{put}$ transformations separate the processes of source--view element matching and element-wise updating.
At the beginning, the source is divided into a ``resource'' and a ``rigid complement'': a resource consists of ``chunks'' of information that can be reordered, and a rigid complement stores information outside the chunk structure.
This reorderable chunk structure is preserved in the view.
If the view is updated, $\mathit{put}$ first finds a correspondence between chunks of the old and new views based on some predefined strategies.
Based on the correspondence, the resources are ``pre-aligned'' to match the new view chunks positionally, and then element-wise updates are performed.
%For the alignment strategies, matching lenses provides several strategies including greedy align, best align, and \emph{minimal edit distance} with optional non-crossing option, and a threshold.
There are laws (\texttt{PutChunk} and \texttt{PutNoChunk}) dictating that correct pairs of resource and view chunks should be used during the element-wise updates, but these laws look more like an operational semantics for $\mathit{put}$ (as remarked by the authors), and do not declaratively state what source information is retained.
% Similarly, \BiFlux\ \cite{Pacheco:2014:BBF:2643135.2643141} provide the user with align-by-position and align-by-key strategies as two primitive lenses.
%\biflux\

% It supports swap/reorder in the get direction by extending its lenses and redefining the PutChunk, PutNoChunk laws.
% The retentiveness is not (explicitly) discussed. The laws are rather operational and can be read as "the i'th element is produced by ... if it is aligned or not". It does not say what the result looks like (or just say the result looks like put use the basic lens k).

%\paragraph{User-supplied alignment functions}
%\todo{need to read the paper...}
%Some other lenses, for example, \todo{???} adopts a programmable principle which let the user provides a matching function.
%The matching result is then separated into three kingdoms and handled with different user supplied functions: what to do for the pair of matched source and view elements (say, using function $f$), what to do for the source elements not matched, and what to do for the view elements not matched.
%But this align still has the limitations that, (1) suitable only for ``listifiable'' data types --- data types that can be flattened into a list.
%(2) any source and view elements can only be matched once.
%Assume that an element in the view has been copied several times, there is no way to align all copies with the same source element.
% This align is ``fixed'' as a one-layer-only alignment, in the sense that if the input source and view (of the align function) are tree structures, the align treats two trees as a whole and the user have no way to align their subtrees.

%\subsubsection{Alignment for \emph{Containers}}
%\label{subsubsec:alignment_containers}
To generalise list alignment, a more general notion of data structures called \emph{containers}~\cite{Abbott-containers} is used \cite{Hofmann:2012:EL:2103656.2103715}.
In the container framework, a data structure is decomposed into a shape and its content; the shape encodes a set of positions, and the content is a mapping from those positions to the elements in the data structure.
%For example, the shape of a list is its length, say~$l$, the encoded set of positions is $P = \{0,1,\ldots,l-1\}$, and the content of the list is a mapping which specifies for each $i \in P$ the $i$-th element of the list.
The existing approaches to container alignment take advantage of this decomposition and treat shapes and contents separately.
For example, if the shape of a view container changes, \citeauthor{Hofmann:2012:EL:2103656.2103715}'s approach will update the source shape by a fixed strategy that make insertions or deletions at the rear positions of the (source) containers.
By contrast, \citeauthor{pacheco2012delta}'s method permits more flexible shape changes and they call it \emph{shape alignment}.
In our setting, both the consistency on data and the consistency on shapes are specified by the same set of consistency declarations.
In the |put| direction, both the data and shape of a new source is determined by (computed from) the data and shape of a view, so there is no need to have separated data and shape alignments.

It is worth noting that separation of data alignment and shape alignment will hinder the handling of some algebraic data types.
First, in practice it is usually difficult for the user to define container data types and represent their data in terms of containers.
We use the data types defined in \autoref{fig:running_example_datatype_def} to illustrate, where two mutually recursive data types |Expr| and |Term| are defined.
If the user wants to define |Expr| and |Term| using containers,
one way might be to parametrise the types of terminals (leaves in a tree, here |Integer| only):\\
\begin{minipage}[t]{.5\textwidth}
\begin{code}
data Expr i   =  Plus   (Expr i)  (Term i)
              |  Minus  (Expr i)  (Term i)
              |  Term   (Term i)
data Term i   =  Neg    (Term i)
              |  Lit    i
              |  Paren  (Expr i)
\end{code}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
\begin{code}
data Arith i  =  Add  (Arith i)  (Arith i)
              |  Sub  (Arith i)  (Arith i)
              |  Num i
\end{code}
\end{minipage}\\
Here the terminals are of the same type |Integer|.
However, imagine the situation where there are more than ten types of leaves, it is rather a misery to parameterise all of them as type variables.

Moreover, the container-based approaches face another serious problem: they always translate a change on data in the view to another change on data in the source, without affecting the shape of a container.
This would be wrong in some cases, especially when the decomposition into shape and data is inadequate.
%
For example, let the source be |Neg (Lit 100)| and the view be |Sub (Num 0) (Num 100)|.
If we modify the view by changing the integer~$0$ to~$1$ (so the view becomes |Sub (Num 1) (Num 100)|), the container-based approach would not produce a correct source |Minus ...|, as this data change in the view must not result in a shape change in the source.
In general, the essence of container-based approaches is the decomposition into shape and data such that they can be processed independently (at least to some extent), but when it comes to scenarios where such decomposition is unnatural (like the example above), container-based approaches can hardly help.
%Thus we see that the idea of isolating the shape and data does not work for some cases, and hence the container-based approaches.

%
In contrast, retentiveness and ``put with links as global alignment'' advances the above approaches in the sense that retentiveness enables the user to know what is retained after |put|, while other approaches merely tell the user which basic lens will be applied after the alignment.
As a result, the more complex the lens is, the more difficult to reason about the information retained in the new source for those approaches.

%\todo{do we need to cite Jorge's delta align?}
% \todo{quotient lenses: equivalent classes. OK, no time to write}

%\subsection{Least Changes and Least Surprise}
%\todo{consider removing the whole subsections. Not so relevant. Save spaces.}
%
%As we have already seen, usually there could be infinite different modifications to an outdated source for bringing it consistent with a given view again.
%It is reasonable to ask if there exist some algorithm which updates the old source into a consistent state with ``minimum changes'', or bring the user ``least surprise'' according to some objective function.
%
%\subsubsection{Side-effects in databases}
%In database communities, a variant (but very similar) of the problem is well-studied and researchers have shown some conclusive theorems.
%We call it a variant because the problem permits the input view $v$ to be invalid --- not in the range of the query function.
%%
%Later the update function will both update the source and correct the view $v$. % new (valid) view.
%For example, assume |[(a,1) (a,2), (b,1), (b,2)]| in a view are formed by a query joining |[a,b]| and |[1,2]| in a source.
%Then if we delete |[a,1]| only, obviously the new view is not in the range of the query regardless how we modify the source.
%%
%The only solution is to modify both the source and the view simultaneously (or, in their setting, modify the source and regenerate a new view from the modified source), and the additional modifications to the view are regarded as \emph{view side-effect}.
%%
%So the measurement of the view side-effect problem is set to be the number of tuples to be deleted in the view (precisely, excluding the necessary ones) given an update on a source. \cite{Cheney:2009:PDW:1576265.1576266}.
%%
%Similarly, we can define another objective function which cares more about the number of tuples to be deleted in the source and thus is called \emph{source side-effect} problem \cite{Cheney:2009:PDW:1576265.1576266}.
%%
%For this example, given the removal of |(a,1)|, the minimum change on the source can be either to remove |a| or to remove |1|.
%And hence the minimum source side-effect is |1|.
%The removal of |a| or (|1|) from the source will cause another tuple |(a,2)| (or |(b,1)| if |1| is removed from the source) in the view to be deleted as well.
%So the minimum view side-effect is also |1|.
%%
%\todo{Seems handling deletion only?}
%The example is shown by deletions in the view, and in general, side-effect for insertions and replacements are also studied.
%
%For the view side-effect problem and source side-effect problem, they are both NP-hard for queries involving join and either projection or union, and polynomial-time solvable for the remaining queries in the class \cite{Buneman:2002:PDA:543613.543633, Keller:1986:CVU:645913.671458, Cheney:2011:PDA:2139690.2139696}.
%%
%\citeauthor{Dayal:1982:CTU:319732.319740} also introduce the notion of \emph{clean source}: a source is clean if it is not a source of any other tuple (usually a source not involving join operation).
%Clean source enjoys a property that the translation from modifications to a view to the update on it can be side-effect free \cite{Dayal:1982:CTU:319732.319740, Cheney:2011:PDA:2139690.2139696}.
%
%\subsubsection{Side-Effects in BXs}In relational databases, the data is represented as (unordered) tuples, and the operations on the data are usually selection, projection, join, and union.
%However, in the world of BXs, data is trees defined by algebraic data types and operations are any
%well-behaved functions,
%which bring difficulties in defining objective functions for calculating ``side-effects'' and producing a least-changed new source.
%As the paper \cite{cheney2015towards} pointed out, different applications indeed require different metric (of changes).
%Most pre-defined metric, for example, the graph edit distance, is not a particularly good match (to calculate matched source and view components) for any user's intuitive idea of distance.
%The authors in \cite{cheney2015towards} are ``pessimistic about whether usably efficient algorithms that guarantee to find optimal solutions to the least change problem will ever be available''.
%Based on this former research, we decided to invent a framework letting the user (or tools) explicitly describing unchanged parts during the transformation.
%Once some parts are connected by links, they are guaranteed to exist in the newly created source.
%
%\todo{Composing Least-change Lenses \cite{macedo2013composing}}
%% Cheney and Jeremy: Using a similar structure, Macedo et al. [MPCO13] address the tricky question of when it is possible to compose bx that satisfy such a least change condition. As might be expected, they have to abandon determinacy (so that the composition can choose “the right path” through the middle model), and impose stringent additional conditions; fundamentally, there is no reason why we would expect bx that satisfy this kind of least change principle to compose.
%
%% Then they discuss whether there exists precise translation for the modifications in the view to the update in the source.
%% A translation is precise, if it modifies source $s$ to $s'$ and $\textit{query}\ s' = v'$.
%% Intuitively, if view $v'$ is invalid under the $\textit{query}$ function, there is no precise translation --- all translations will have side effects on $v'$ and make it become $v''$ after query on the new source.


\subsection{Provenance and Origin}
\label{subsec:provenances}

Our work was inpired by research on provenance \cite{Cheney:2009:PDW:1576265.1576266} in the DB community and origin \cite{vanDeursen:1993:OT:162204.162214} in the rewriting community.

%In \autoref{subsec:two_links}, we have mentioned that our idea of horizontal links and vertical links are inspired by the notion of (various kinds of) \emph{provenance} in the database community.
%
As discussed in \cite{Cheney:2009:PDW:1576265.1576266}, provenance can be classified into three kinds: \emph{why}, \emph{how}, and \emph{where}.
\emph{Why-provenance} is the first formalised provenance and was called \emph{lineage} at the time \cite{Cui:2000:TLV:357775.357777};
it is the information about which data in the view is from which rows in the source.
%
However, knowing only the rows a piece of data in the view is from is not informative enough for many applications.
Consequently, researchers propose two refinements of why-provenance:
\emph{how-provenance}, which additionally counts the number of times a row is used (in the source),
and \emph{where-provenance}, which additionally records the column where a piece of data is from.
By combining the row and column information of a piece of data, we know exactly where it is \emph{copied} from.
In our setting, we require two pieces of data linked by vertical links to be equal (under a certain pattern).
Hence the vertical links resemble where-provenance.


If we leap from relational database communities to programming language communities, we will find that these kinds of provenance are not powerful enough, as they are mostly restricted to relational data, namely rows of tuples.
%
In functional programming, we often use algebraic data types, which in general are sums of products including function types,
and produce views by more general (recursive) functions rather than selection, projection, join, etc.
%
For this need, \emph{dependency provenance} \cite{Cheney:2011:PDA:2139690.2139696} and \emph{expression provenance} \cite{Acar2012} are proposed:
the former tells the user on which parts of a source the computation of a part of a view depends, and the latter can even record a tree tracking how a part of a view is computed from some (predefined) primitive operations \cite{Acar2012}.
%
In this sense, our horizontal links are closer to dependency provenance.

The idea of inferring consistency links (horizontal links generated by |get|) can be found in the work on origin tracking for term rewriting systems~\cite{vanDeursen:1993:OT:162204.162214}, in which the origin relations between the rewritten terms can be calculated by analysing the rewrite rules statically. However, it is designed for tracing back, and nothing is mentioned about updating backwards, as we do in this paper.
%
Using these consistency links, in the area of BX, \citeauthor{WangMeng2011} propose a method to incrementalise state-based lenses by tracking links between data in the view and their origin in the source.
When the view is edited locally in a sub-term, they use links to identify a sub-term in the source that contains the edited sub-term in the view.
Then to update the old source, it is sufficient to only perform state-based |put| on those sub-terms. Since lenses generated by our DSL also create links (although for a different purpose), our lenses can be naturally incrementalised by their method.
% with links we created.
%Write more later.}
%\todo{tracking in slicers}



\subsection{Operational-based BX}
%View-Update Translator}
% \subsubsection{In the Database Community}
% In the database community, there are notions of \emph{updatable views}, and much research has focused on transforming an update on the view side to an update on the source side.
% %
% For example, \cite{Bancilhon1981} introduce the notion of \emph{constant complement} in relational databases:
% Given a database~|s|, a query~|f| and a view |v| queried by |f|, all ``the information not visible within |v|'' \cite{Bancilhon1981} must reside in the |v|'s complement created by an |f|'s complement function |g|.
% The function |g| should satisfy the property that |(f {-"{\triangle}\;"-} g) s = (f s, g s)| is injective.
% That is, the original database is decomposed into a view and its complement, the latter of which is kept constant; when the view is updated, it is paired with the constant complement and fed into $(f \mathop\triangle g)^{-1}$ to compute an updated database.
% The concept is useful and leads to some applications, such as \emph{bidirectionalisation} (of a query function) in the BX community~\cite{Matsuda:2007:BTB:1291151.1291162}.
% %
% However, there are also inadequacies that make the approach not very practical.
% On the one hand, the time complexity for finding a minimum complement is NP-complete \cite{Cosmadakis:1984:URV:1634.1887};
% on the other hand, finding a complement which might be ``arbitrarily large'' is not that useful, because the data (in the source) captured by the complement function |g| cannot be changed after an update (hence the name constant complement).
% In extreme cases, if the complement is set to be as large as the whole source, then nothing could be updated.
% %
% % Second, since in (traditional) relational databases the data is only tuples, the method deals with product types only but cannot handle handle sum types.
% % Because for sum types there is even no constant complement in general. (See \autoref{sec:putput}.)
%
%
% \subsubsection{In the BX Community}

%The BX community has also considered translating view updates to source updates to achieve bidirectionality.
Our work is relevant to the operation-based approaches to BX, in particular, the delta-based BX model \cite{Diskin2011Asym} and \emph{edit lenses} \cite{Hofmann:2012:EL:2103656.2103715}.
% are more relevant to our work.
%
The delta-based BX model regards the differences between the view state |v| and |v'| as \emph{deltas} and the differences are abstractly represented by arrows (from the old view to the new view).
The main law (property) in the framework is:
Given a source state |s| and a view delta $|det|_|v|$ between |v| and |v'|, $|det|_|v|$ should be translated to a source delta $|det|_|s|$ between |s| and $s'$ satisfying |get s' = v'|.
%
As the law only guarantees the existence of a source delta $|det|_|s|$ that updates the old source to a correct state,
it is not sufficient for deriving retentiveness in their model
%
because, given a translated delta $|det|_|s|$, there are still an infinite number of interpretations for generating a correct source $s'$, with only few of them being retentive.
%
To illustrate, \citeauthor{Diskin2011Asym} tend to represent deltas as edit operations such as \emph{create}, \emph{delete}, and \emph{change}, aiming to transform edits on the view to edits on the source.
However, representing deltas in this way can only tell the user in the new source what must be changed, while there is additional work to do for reasoning what must be retained.
As discussed in this paper, by carefully designing and imposing proper properties on the representations of deltas, it is possible for delta-based BXs to exhibit retentiveness.
%
Note that compared to \citeauthor{Diskin2011Asym}'s work, \citeauthor{Hofmann:2012:EL:2103656.2103715} give concrete definitions and implementations for propagating edit sequences.
%Other discussions for Edit Lenses can be found in \autoref{subsubsec:alignment_containers} (regarding container alignment).
