\subsection{Semantics}
\label{subsec:dslsem}
In this sub-section, we give a denotational semantics of our DSL by specifying the corresponding |get| and |put| as mathematical functions. Our language is also implemented as a compiler tansforming programs in our DSL to |get| and |put| functions in Haskell. Our Haskell implementation directly follows the semantics given here.

Because our |put| function and its implementation will respect relative positions of regions by default---no matter they are required by the user or not---an designing choice we made is to simplify |get| and |put| to only produce and accept links of regions, so that the user do not need to bother to specify links of second order properties. In \autoref{subsec:proof}, we will prove |get| and |put| functions given below, lifted to a version also producing and accepting second order properties, actually form a retentive lens.

\subsubsection{Types and Patterns}
To define the semantics of |get| and |put|, we firstly need a semantics of type declarations and patterns in our DSL. For type declarations, their semantics is the same as in Haskell, thus we will not elaborate here. For each defined algebraic data type |T|, we will also use |T| to refer to the set of values of type |T|. For patterns in our DSL, we will use |SPat| to refer to the set of all pattern on |S| for every type |S|. For a pattern $|p| \in |SPat|$, |Vars p| will be the set of variables in |p| and |TypeOf p v| will be the set corresponding to the type of |v| in pattern |p|. We will also use |Pat| to denote the tagged union of patterns for all types, that is, $|Pat| = \{\; (|S|,\; |sp|) \mid |S| \text{ is a type, }|sp| \in |SPat| \;\}$.

To manipulate patterns, we will use the following functions:
\begin{align*}
|isMatch_S| &: (|p| \in |SPat|) \rightarrow |S| \rightarrow |Bool| \\
|decompose_S| &: (|p| \in |SPat|) \rightarrow |S| \rightarrow \big((|v| \in |Vars p|) \rightarrow |TypeOf p v|\big) \\
|reconstruct_S| &: (|p| \in |SPat|) \rightarrow \big((|v| \in |Vars p|) \rightarrow |TypeOf p v|\big) \rightarrow |S|
\end{align*}
|isMatch p s| tests if the value |s| matches the pattern |p|. If they match, |decompose p s| will result in a function from every variable |v| in |p| to the corresponding sub-term of |s|. Conversely, |reconstruct p f| produces a value |s| matching the pattern |p| by replacing every $|v| \in |Vars p|$ in |p| with |f v|, provided that |p| does not contain any wildcards. Since the semantics of patterns in our DSL is rather standard, we will also omit formal definitions of these functions.


%format mgetsv = "\getsv"
%format mgetspvp = "get_{S^\prime V^\prime}"
%format msetin = "\!\!\in\!\!"
%format munion = "\mathop{\cup}"
%format (mset (x)) = "\big\{\;" x  " \;\big\} "
%format (mbigparen (x)) = "\big(\;" x  " \;\big) "
%format (mBigparen (x)) = "\Big(\;" x  " \;\Big) "
%format mbigunion = "\bigcup"
\subsubsection{Get Semantics}\label{subsubsec:getsem}
For a consistency relation $|S| \leftrightarrow |V|$ defined in our DSL with a set of inductive rules $R = \{\, |spat_i| \sim |vpat_i| \mid |i| \in \mathcal{I}\,\}$, its corresponding $\getsv$ function has the following signature:
\begin{align*}
\getsv : |S| \rightarrow |V| \times (|Region| \sim |Region|)
\end{align*}
where |Region| is $|Pat| \times |Path|$, and |Path| is the set of lists of integers, which encode paths in trees. The body of $|get|_{SV}$ is:
\begin{code}
mgetsv s =   let  spat_k = selectPat s R
                  f = decompose_S spat_k s
                  vs = get . f
                  links = mbigunion (mset (updatePaths_t (snd (vs t)) | t msetin Vars spat_k))
                  newLink = (mset (mBigparen (^(mbigparen ((S, ^fillWildcards s spat_k), [])), mbigparen ((V,^ vpat_k), [])^)))
             in   (mbigparen (reconstruct_S vpat_k (fst . vs), ^ newLink munion links))
\end{code}
where |selectPat s R| returns the unique source pattern in |R| matching |s|. Such pattern exits because our DSL syntactically requires all source patterns of |R| are disjoint and total. 

By matching the source with |spat_k|, we bind every variable in |spat_k| to a sub-term of |s| as |f : (v msetin Vars spat_k) -> TypeOf spat_k v|.
Then we recursively call |get| for all sub-terms, expressed as |get . f| in the definition above, while to be precise, we need to call $|get|_{|TypeOf spat_k v|,\; |TypeOf vpat_k v|}$ for every |t msetin Vars spat_k|.

With the sub-terms and links produced by recursive calls, we can construct the result of |mgetsv|. 
(1)~The returned view simply is created by reconstruct the view pattern |vpat_k| with sub-terms generated recursively. 
(2)~The returned links should be the union of (2.1) a new link between two regions at the root of |s| and |v| respectively (|newLink|). Note that sub-terms of |s| matching wildcards of |spat_k| are also treated as parts of the region, so we use |fillWildcards s spat_k| to replace all wildcards of |spat_k| with the corresponding sub-terms of |s|;
(2.2) and also links produced by recursive calls, for which we need to be careful to update all paths in them.
For a link $((\,\cdots, \mathit{spath}),\;(\,\cdots, \mathit{vpath}))$ generated by the recursive call for pattern variable |v|, we need to prepend the path of |v| in |spat_k| to |spath| and the path of |v| in |vpat_k| to |vpath|, this is expressed as |updatePaths| in the above definition.

\subsubsection{Put Semantics}\label{subsubsec:putsem}
To define a corresponding |put| function, we need to define its domain |T|. Since we will use |T| to restrict the input of |put| to a domain that our |put| functions can handle and guarantee retentiveness, we will first show the body of |put| and then the definition of |T| will come out naturally.

\newcommand{\putsv}[0]{|put|_{SV}}
%format mputsv = "\putsv"
%format memptyset = "\emptyset"
%format mneq = "\neq"
%format msetminus = "\setminus"
%format mand = "\wedge"
%format mwildcardfree = "\text{wildcard free}"
For a consistency relation $|S| \leftrightarrow |V|$ in our DSL defined with $R = \{\, |spat_i| \sim |vpat_i| \mid |i| \in \mathcal{I}\,\}$, the corresponding |mputsv| function has signature:
\[\putsv : T \rightarrow |S|\]
where |T| is a subset of $|S| \times |V| \times (|Region| \sim |Region|)$. The body of |mputsv| is defined as:
\begin{code}
mputsv (s, v, ls) = 
  let linksToVRoot = (mset (mbigparen (((S', spat), spath), ((V, vpat), [])) msetin ls))
  in if  linksToVRoot mneq memptyset then 
         let  l@(((S', spat), spath), ((V, vpat), [])) = shortestSPathLink linksToVRoot
              vs = decompose_V vpat v
              ss = \(t  msetin Vars spat) ->
                        put s (vs t) (divideLinks_t (ls msetminus {l})) 
         in {-"\mathit{inj}_{S^\prime S}\;"-} (reconstruct spat ss)
     else  let  l = mset ((spat_k ~ vpat_k) msetin R | isMatch vpat_k v)
                (spat_k ~ vpat_k) = anyElement l
                vs = decompose_V vpat_k v
                ss = \(t  msetin Vars spat) ->
                          put s (vs t) (divideLinks_t ls) 
           in reconstruct (fillWildcardsWithDefaults spat_k) ss
\end{code} 
The process of |mputsv| is divided into two cases:
\begin{enumerate}
\setlength{\parskip}{0.5em}%
\setlength{\itemsep}{0.5em}%
\item \textit{The root of the view is an endpoint of some input links.} In this case, we pick one link among them (|shortestSPathLink|), and use the source region of the link as the root of the new source, whose sub-terms are created by recursive calls to sub-terms of |v|. The function |divideLinks_t lset| divides the set of links to appropriate recursive calls and maintains paths of all view regions. For each link of |lset|, if the path of view region has a prefix  equal to the path of |t| in |vpat_k|, |divideLinks_t| will remove that prefix; if it does not have such a prefix, |divideLinks_t| will remove that link from the set |lset|. Note that our created new source might have type |S'| that is possibly different from the return type |S|, we need to wrap it into |S| with $\mathit{inj}_{S'S}$. Such wrapper always exists because it is syntactically guaranteed by our DSL (\autoref{subsec:synres}).

The choice of |shortestSPathLink| at the beginning is also important. By choosing the source region at the shallowest position at first, those regions in our new source will have the same relative positioning as in the original source. Thus our |put| functions will respect all possible second-order properties asserting their relative positioning in the old source. 

\item \textit{The root of the view is not attached to any input links.} In this case, we only need to perform a ``regular'' put. We choose an arbitrary rule with a matched view pattern in set $R = \{\, |spat_i| \sim |vpat_i| \mid |i| \in \mathcal{I}\,\}$ defining the consistency relation. The corresponding source pattern is used to create the new source. If the source pattern contains wildcards, we also need to replace those wildcards with some default values.
\end{enumerate}

From the definition of |mputsv|, we can define |T| to be the following domain in which our |put| function can be total and guarantee retentiveness:
\begin{itemize}
\item All input links are valid, in the sense that the source region and the view region match with a rule defining the consistency relation.
\item For every sub-term of the view, if it is not in the view region of any links, it always matches a view pattern in the set of rules |R| defining the consistency relation. This requirement guarantee in the second case of |put|, we can always create a consistent source.
\item All regions of links are non-overlapping. Additionally, for every sub-term of the view not in any regions, if it matches a view pattern in |R|, the matched fragment is non-overlapping with any regions of the input links either. This requirement guarantee our |put| functions will eventually process all input links in the recursive procedural.
\end{itemize}