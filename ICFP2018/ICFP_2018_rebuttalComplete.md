We thank all the reviewers for their time and effort spent in giving us criticism and advice.

This rebuttal consists of two parts. The first part answers the most important and common questions asked by all the reviewers, and the second part contains detailed answers to each question.

# Part I
Based on the reviews, the main complaints and weaknesses of this research can be summarised as:

1.  Why the theorem does not include vertical links between the old and new view?
2.  How can we build third-party tools that track the "links" between the source and view and keep them up to date as the view is edited?

We will show that these are not weaknesses, and in some sense strengths, as we have carefully considered them when designing the retentiveness theory.

## Answer to 1

At the beginning, in our framework we indeed includes vertical links  and the composition between horizontal and vertical links, and retentiveness is in the form of link composition. However, later we realise that these concepts are not necessary.
To make the theorem simpler and closer to state-based lenses -- which accepts (in addition to a source and a modified view) links between the source and the modified view, rather than the "deltas" (i.e., vertical links) between the old view and the modified view -- we have wiped out vertical links and link compositions from the basic framework.

This also separates the job of retentive lenses from third-party tools: **retentive lenses produce and maintain links between a source and its consistent view** (as our DSL does), **and third-party tools produce vertical links between two views** (not between sources and views as asked in question 1). The design enjoys some modularity: different (retentive) lens languages and different third-party tools can be flexibly integrated in different situations. As Fig. 8 and Fig.9 (in the paper) show, different third-party tools are used to produce vertical links, while the same lens language (our DSL) is used for tracking links between sources and views. These two kinds of links are composed as input to retentive $\mathit{put}$ functions.

We will add a paragraph (back) to the Case Study section explaining the theory for vertical links and the composition between horizontal links and vertical links more clearly.

## Answer to 2

After answering question 1, now the problem becomes how to track links between two views, rather than between sources and views.

General strategies for tracking links between two arbitrary trees can be hard (ref. Type-Directed Diffing of Structured Data). But we can describe specific strategies to track links for our two case studies, as the view and its one-step modified one are quite similar:

  - For resugaring (one-step evaluation on an AST), a simple but sufficient strategy is to build all the vertical links between regions in the old and modified view position-wise, except for the changed (evaluated) parts.
  - For code refactoring (moving regions around on an AST), the strategy is also natural: since refactoring algorithm knows how a subtree is moved (how a subtree's position changes), it can also output a vertical link recording this movement. Other vertical links can be built position-wise similarly as resugaring.

That is, we use different approaches to establishing vertical links in different applications.


# Part II

We hope the reviewers to be happy with our answers, and find the approach presented in the paper (more) reasonable.

## For Review #98A

> Paper summary

> Proposes a refinement of the laws for bidirectional transformations (lenses) requiring that "small changes to the view get mapped to small changes to the source." After developing the basic theory, a small DSL is introduced, where programs are guaranteed to be retentive in the proposed sense. Two small case studies round out the paper.

This is correct. And in addition, retentiveness subsumes Hippocraticness.


> Rating rationale and comments for author
> 
> The basic issue addressed by the paper -- the desire for stronger lens laws -- has already been addressed in multiple papers, but this one is particularly nice in several respects:
> the property of retentiveness is natural and intuitive; it is presented both in a concrete form (trees and patterns) and more abstractly ("properties")
> beyond the abstract theory theory, a concrete DSL is proposed, based on an attractive notion of bidirectional pattern matching, together with a proof that it satisfies the proposed property
> a real (prototype) implementation is described
> the technical development is admirably clear (not always a given, in this area!)
> Overall, this work significantly advances the state of the art in the area of bidirectional functional programming, and I am strongly in favor of acceptance. However, I do have one significant complaint: the development assumes the existence of "third-party tools" that are able to track the "links" between source and view and keep them up to date as the view is edited. This seems (a) nontrivial, to the extent that I'm somewhat skeptical it can be done for real and therefore (b) worthy of both practical implementation and (c) theoretical consideration. The lack of either implementation or theory addressing this aspect of the problem strikes me as a significant weakness, which I would urge the authors to address in a final version of the paper.

Answered in Part I.

> DETAILS:

> 33: The 2009 citation is OK, but for scholarly completeness you should cite at least a couple of the seminal database view update papers, and perhaps also Foster 07.

TODO


> 37: You should also cite Augeas -- it is one of the few lens languages that is really used in industry. And the Haskell lens library, for the same reason.

Thanks. Read and added.


> 60-91: This argument is pretty convincing, but it would be even more convincing if you could cite actual examples of bidirectional languages that "get it wrong."

In our paper, we write "However, in practice, a well-behaved put generated from existing bidirectional languages (tools) has a very high possibility to return cst1 or cst2."

As for classical lens combinators: 
it seems not easy to find the implementation of sum lenses (by Foster et al), while Matching Lenses and Boomerang work on string data only...
As for special tools designed for converting between program text (CSTs) and ASTs, such as Invertible Syntax Descriptions and FliPpr, they are not BXs after all...
So we hesitated to give citations here.


> 220-224: You don't say much, here, about how the links are updated to reflect updates to the view. Indeed, this is likely to be challenging, as past work on delta lenses, edit lenses, and the like shows.

Answered in Part I.


> 267: "A rough reading of the triangular guarantee described in Section 2.1 is that a fragment in the old source linked to the view should also appear in the new source." I know you said "rough", but I was confused by the omission of "unless this fragment got changed in the view."

The triangular guarantee considers only a (final) source and a (final) view states, and links between them.
Edits on the view are not considered, or, should be performed and links should be re-established before reaching the final states.
If "this fragment got changed in the view.", then the link connecting this fragment might be broken and thus the fragment is not required to be retained.
Other details can be found in Part I.


> 299: I got stuck on this for a while: "We have seen how regions are abstractly represented: given a source s, the set of regions in s is abstractly represented by Ps where P is the set of all possible region patterns (regarded as properties) for sources." After reading it several times and thinking about it, I think I've got it now, but perhaps you could expand it a bit (with an example, perhaps?) to make sure people don't get lost here.

TODO:


> 306: This seems a little underspecified, since you haven't said what are the sets from which the p and q properties are drawn (or even said what I think is true, that these sets are parameters to the definition). Moreover (maybe you're going to get to this later) there is a big question about how this relation is represented concretely, and how the get function actually computes it!

CHECK: $P$ and $Q$ are the sets of properties on the source type $S$ and view type $V$ respectively. We assume that this definition follows the paragraph above, so some explanations are omitted. We will add the explanations.
The representation of the relation (links) is defined later in Section 3.3.2 and 3.4.1


> Throughout this section, I sometimes feel confused whether the variable P is being used for the set of all possible properties, or just a particular set of properties relevant to some set of concrete inputs. E.g. in defn 2.5.

CHECK: Leave to josh...
$P$ is the set of all properties (of a set $X$). If x is an inhabitant of $X$, then $P_x$ is the set of possible properties provably satisfied by $x$ (the inhabitant).
A set $X$ can have many property sets that are uniquely identifying, each for an $x$ (an inhabitant).


> 492: "The theory below is still somewhat obscure though, and can be safely skipped on first reading". This advice is welcome -- I wish more papers did similar things. :-) But it would help if you could comment whether you believe the theory extends to the fully general case.

"The theory below" is an example of the generalisation to the basic theory. It shows how to extend the basic theory to deal with second-order properties. Using the same technology, we can deal with more complex properties (n-order). In this sense, it shows some generality.


> At this point in a first careful reading of the paper, my main concern is not the complexity of the theory (though, indeed, the second-order properties and triple spaces do get a bit daunting) but rather its flexibility and how all this expressive power is going to get instantiated, concretely, by an implementation. (And specific questions like How is the implementation going to guarantee that the edits made by the user land withing the specified triple space so that the put function can deal with them?) Presumably we're going to find out about all this in the next sections.

The triangular property starts from the source, modified view, and links between them. So the edits from the old view to the new view are not the concerns of our theorem.
In the implementation, our put function checks if the input links are valid (with regard to the triple space T).
Restrictions on the DSL program and the defining of triple space T can be found at Section 3.2.1 and the last part of Section 3.3.
When building an interactive editor, we usually provide the user with only basic edits, so that every step of edit is valid and land within the specified triple space.
(Refer to the composition of horizontal links and vertical links in Part I.)


> 584: "it is a bit tricky for keeping retentiveness compared with traditional approaches" -- ???. (In particular, which traditional approaches?)

We want to say that "since our put need to create a new source satisfying retentiveness, its behaviour is a bit tricky and hard to explain briefly here. While traditional approaches do not need to satisfy retentiveness (so their put functions are comparatively easy to describe)."


> Overall hesitation: There is something a bit funny about using a program to define what it means for that very program to be retentive! It feels like a shame that the definition of retentiveness isn't independent of the program. Would this not make sense? Some discussion of this issue seems important.
the definition of retentiveness

Sorry for delivering wrong ideas. The definition of retentiveness is independent of the program. While the triple space T depends on the input program.


> 905: I wish these vertical links were part of the theoretical story! They seem critical, and nontrivial.

Answered in Part I.


> Section 4, overall: I'm afraid I'm rather disappointed that there is no discussion at all about how these third-party tools are supposed to do what they do. This seems like a significantly hard part of the job!

Answered in Part I.


> I skimmed 3.3 and 3.4 -- I was happy to trust that the language you'd presented could be given a reasonable semantics and that it would obey the theorems you'd proposed, and I wanted to get on to the case studies. Other readers may feel the same, so, although writing down the proof somewhere does seem useful, if you're tight for space you might move it to the appendix.

Thanks for the suggestion.


> 973: "Compared to Pombrio and Krishnamurthi’s method, we do not need to enrich the data types of the ASTs to incorporate fields for holding tags that mark from which syntactic object an AST construct comes." This does not seem like a fair comparison, since you are assuming that the same information is given to you by a (magical) "third-party tool."

Sorry for not making it clear. The traces (consistency links) between the CST and AST is established by (the get of) our DSL, not a third-party tool, so there is no need to enrich the AST's data types.
A third party tool only outputs vertical links (recording unchanged part) between the old view and the new view (after one-step evaluation). We think it is not "the same information".
Other details can be found in Part I.


> 643: The restrictions on patterns feel a bit like the typing restrictions on the sum lens in Foster et al. Any comments?

If you are asking the conditionals of tree lenses, the answer is yes. We want to enforce that get will select the same "branch" (pattern) after put succeeds.


> 1091: The discussion of matching lenses is much too shallow. The description of matching lenses is reasonable, but there is no meaningful comparison to the present proposal. This seems a shame, as matching lenses are a concrete and practical instance of some very related concepts. (The comparison to container-based approaches is more substantial, and I think the criticism of those approaches is fair.)

We will add statements like: The design of matching lenses is to be practically easy to use, so they are equipped with a few fixed matching strategies that can be chosen by the user. However, whether the information is retained or not, still depends on the lenses performed after matching.
While retentive lenses are designed to abstract out (parameterise) the matching (alignment) strategy, and are more like to take the results of "final matching" as input. This "final matching" produces links between unchanged parts globally for information retention, rather than apply other lenses to the linked parts.



> 1200: Some readers may miss what you mean by "composable" (i.e., "horizontal" or categorical composition, as opposed to, for example, pairing).

TODO not clear


> The discussion of composability restrictions reminds me of a similar restriction on quotient lenses. It would be worth citing that discussion.

Thanks. We did not know it before. It seems that similar problem indeed occurs there:
"to check that an instance of sequential composition l;k is wellformed, we need to verify that l's abstract equivalence relation and the k's concrete one are identical."
We need to investigate and to see if they have better solutions later.


> There are some references missing to bidirectional parser / printer formalisms (they can be found in some of the earlier lens papers, like Foster et al). I recall there were one or two that were pretty similar to your DSL.

We have cited papers "Invertible Syntax Descriptions", "FliPpr: A Prettier Invertible Printing System", and "Parsing and Reflective Printing, Bidirectionally".
We missed a paper "Generating attribute grammar-based bidirectional transformations from rewrite rules", and will add it.



> QUESTIONS:

> I think the answer to this is yes, but I want to double-check (because too many papers on lenses silently choose No!). Your get and put functions are total (on the specified types), right?

Yes. For the theory of retentiveness, triple spaces are used to guarantee that get and put functions are total and operating on a subset of specified types.
For the DSL, there are certain syntactic checks on the program so that the triple space T (in particular the set of valid links) can be correctly fixed, as shown in the last part of Section 3.3



> Can you prove that some "standard" lens language, e.g. Boomerang, is retentive? (I know you proved that all classic well-behaved lenses are retentive [407-425], but only in a trivial sense. My intuition is that it should be true in a more interesting sense also.)

We guess that, "in a more interesting sense" means that a lens (whose put direction is) equipped with some alignment strategies, such as those described and developed in matching lenses (and Boomerang), can be retentive.

As we mentioned above, retentive lenses are designed to abstract out (parameterise) the matching (alignment) strategy, and are more like to take the results of "final matching" as input. This "final matching" should produce links between unchanged parts globally for information retention, rather than apply other lenses to the linked parts.
TODO:
to get the result of "final matching", we may need "retentive lens" combinators, which ... restricts domains...

So we believe the answer is "Yes", but it seems not an easy task and need to be thought out in the future.




> 220-224: How is this accomplished, concretely? (This is discussed later, but it would be good to foreshadow it a bit here.)

Answered in Part I.


> Does the term "retentive" have anything to do with the "retentive sum lens" of Hofmann, Pierce, and Wagner? If not, perhaps it would avoid confusion to choose an adjective that doesn't already have an established meaning in this literature?

Both the use of "retentive" here and there are the same: retentive lenses try to keep more information compared with "forgetful" ones.
Here, the information is from the original source;
there, the information is from the complements of the sum.
At least we should have cited that paper, thanks.




minors:
> 73: period should be comma
> 276: "A property p on a set X is a value equipped" -- I had trouble understanding this at first; "A property on a set X is a value p equipped" would have been easier to parse, and a gloss like "A property on a set X is a value p (drawn from some other set P) equipped" would have helped even more.
> 355: Nit: The colon before the math display is not needed (and not grammatical) because the sentence continues after the display. here are other instances of this.
> 562: Please put Figure 5 on this page.
> 659: "Although being" => "Though it is"
> 685: tansforming
> 708: Find a way to start the sentence with a capital letter.
> 905: shortly => briefly
> 1013: its => their
> 1080: mathing
> 3.2.2: I initially missed the type declarations in these examples. Maybe add a "where" between the two columns?



## For Review #98B
> Paper summary

> This paper presents a new model of lenses coined retentive lenses, which decorates traditional bidirectional transformations with traceability links. The purpose of this addition is to remedy a limitation of exiting BX laws, which may result in parts of the source to be updated even through the update on the view does not necessarily affect them. The paper presented a semantic framework for the new type of lenses and a language for programming them.

> Rating rationale and comments for author

> I agree with the motivation of the work and appreciate that a lot of work has been put into the design of the framework. However, I think the current state of the paper still leaves one wanting in many aspects. Detailed comments are as follows.

> It is mentioned in the abstract and introduction that existing BX laws only govern the case where there is no change to the view. While understanding the point it is making, I have to say that the statement is very inaccurate. The Correctness law does require certain source information to be retained.

There appears to be some misunderstanding about the basics of BX, as Correctness only requires that changes need to be made so that the new source is consistent with the view. These changes are basically new information propagated from the view, but not retained from the old source.

For a concrete example, suppose get is the first-projection on a tuple: `get (1, "a") = 1`, and `get (2, "b") = 2`.
Then both `put1 (1, "a") 2 = (2, "a")`, and `put2 (1, "a") 2 = (2, "")` satisfy Correctness, while `put2` retains less information from the old source.


> Section 2.1 is meant to be an intuitive summary. However, it is quite difficult to read. I struggled to get a clear intuition of what a region is, and the descriptions are a bit long-winded. I suggest that the regions in the example to be named to declutter the text.

A region is a pattern together with a path. It is a good idea to name the regions, and we will do it in the next version.


> I do not understand the description of triangular guarantee. What happens if the region linked is updated?

Answered in Part I.


> Section 2.2 is over engineered in my opinion. I do not see why a relative simple concept of region has to be defined in such an indirect and complicated way. I also do not see the point of triple space. Isn’t it just a triple? Or there is something deep that justifies such an elaborate treatment? In Definition 2.6, it says that get is a subset of triple space. What does it mean? As far as I can see, much of the preliminaries introduced in Section 2.2.1 is only used in a small part towards the end of Section 2.2. Maybe this means that it can be introduced on demand?

Triple space is defined in Definition 2.3. It is a subset of a triple so that the lens operating on it becomes total.
Note that not all of the links between a source region and a view region are valid. For example, a link between negation and an addition is invalid.

TODO, how to answer "As far as I can see, much of the preliminaries introduced in Section 2.2.1 is only used in a small part towards the end of Section 2.2. Maybe this means that it can be introduced on demand?"


> Section 2.3 is incomprehensible. The paper prize itself for proposing a new lens model. But if it takes 11-pages of dense reading to have a chance to understand the basic model, maybe it is not the right model after all.

The basic model only includes Section 2.1 and 2.2. Understanding the basic model does not require the reader to understand Section 2.3.
In Section 2.3.2 we have written that "The theory below is still somewhat obscure though can be safely skipped on first reading.". Maybe we need move this sentence to somewhere earlier.



> The language in Section 3 is clean, though lacks expressiveness. It is clear that only the most straightforward transformations can be encoded. For example, will you be able to handle if-branching? It might be that for the applications the paper targets, such simple transformations between trees are suffice in many cases. But there is no discussion on this.

You are right, we can only handle if-branching based on pattern matching of the source and the view, and are not able to handle general conditions.
We agree that for simple examples shown in the paper pattern-matching-based conditionals are suffice and will add discussions on it.


> I am a bit disappointed by Section 4. Two case studies are presented, but neither is discussed in sufficient details to offer insights. The examples are too simple to make a case. I would really like to see more extensive programming with the new language to get a sense on whether the expressiveness offered is reasonably sufficient. For example, it shouldn’t difficult to try out the standard refactoring operations and see how they can be encoded.

In fact, before establishing the theoretical framework and designing the tiny DSL, we have thought about and (somehow tried) the standard set of refactoring operations provided by Eclipse on Java 8.
The total 23 refactoring operations can be thought as combinations of the four basic operations: substitution, movement, deletion, and insertion. (They are usually only the combination of the substitution and one other.)

Eclipse tries to keep the comments associated with the code when the code is moving around, but it will not keep the comments regarding code insertion, deletion, and substitution.

For about half of the refactoring operations, list alignment seems to be enough, as the code (to be precise, subtrees of an AST) is moved around within a list (e.g.: a list of expressions).
While for the remaining half of the operations, code is moved far from their original position, so a "global tracking strategy", such as links, are required.
Based on our survey and observation, we believe that our get and put functions with links are able to support all the set of refactoring operations, with the retention of comments.

On the other hand, it does have some difficulty in really trying out a set of refactoring operations as it requires that a parser and a printer should be generated from a program in the DSL, to handle the complete transformation between program text and ASTs.
One real difficulty falls on the the handling of ambiguous grammars with disambiguation directives (as is known that the grammar of Java 8 is ambiguous).
The tools we cited in the paper, such as Invertible Syntax Descriptions, FliPpr, and BiYacc, cannot handle ambiguous grammars.
This is part of our future work (of another paper).

If the reviewer thinks that these discussions are necessary, we will add them to the paper.


> I was alarmed by the sudden introduction of another type of links at the beginning of Section 4, and only then realised that it is basically some information on what updates are performed. I think this is another example of over engineering in this paper, where relatively simple things are made to sound more complicated than they are.

In fact vertical links can be viewed as recording the parts where updates are not performed.
Answered in Part I.


> I think self-adjusting computation shall be discussed in the related work as well.

Thanks for reminding us & we did not know this work before.
At first glance, we find that self-adjusting computation is more about incremental computation, that is, how the computation result gets updated with little cost if the input to the computation changes.
However, in our situation, both the input (source) and output (view) of a "computation" can change.
In the related work, we cited the paper "Incremental Updates for Efficient Bidirectional Transformations", which we think is more relevant.




## For Review #98C
> Paper summary
> 
> The paper describes "retentive lenses", and presents a new consistency criterion for lenses that goesd beyon the traditional global "Hippocractiness". It also presents a new formalism for defining lenses that meets retentiveness.

> Rating rationale and comments for author
> 
> Most of the paper is about the retentiveness criterion, but fails to explain adequately how that addresses problems past approaches haven't. I can see how the "globalness" of Hippocractiness is a problem, and how retentiveness is "less global", but how that is relevant for constructing lenses is not apparent from the paper.

TODO: ??? 
"How is that relevant for constructing lenses":
We wish that lenses could in addition satisfy retentiveness, rather than the basic Hippocraticness. ???


> The formalism seems worthwhile, but its connection to the retentiveness criterion is tenuous. The "examples" section immediately introduces an additional concept ("vertical links"), which should have founds its way into the retentiveness criterion. The examples themselves are too small to successfully make the case of the formalism, and aren't compared with solutions to the same problems with other formalisms.

TODO: ??? The DSL we introduced, will generate lenses satisfying retentiveness. ???
For the explanations on vertical links and why they are not parts of the theory, please refer to Part I.
For the case study on resugaring, we compared our solution with the original solution provided by Pombrio and Krishnamurthi.
For the case study on code refactoring, please refer to the answer to the above question "I am a bit disappointed by Section 4...".
In addition, since we (think that we) are the first to introduce retentiveness, we believe that other solutions cannot guarantee the retention of information from the theoretical part.


> Details:

> 34: A one-sense intuition of what "well-behavedness" is would make this easier to follow

In the next paragraph, we say that "Correctness states that... while hippocraticness says that...".
But we should introduce it a bit earlier: well-behavedness wants to prevent get and put doing wrong.
TODO: add it


> 50: mention that the first S is the original source, V the new view, the second S the updated source

TODO: Thanks. add it.


> 106: Visually, it's not clear that Arith is the view datatype.

TODO: Thanks. think a way ...


> 208: The numbering seems odd - if why cst_3, not cst' corresponding to ast'

This $\mathit{cst}_3$ here is the visualisation of the $\mathit{cst}_3$ in Figure 2. All of $\mathit{cst}_1$, $\mathit{cst}_2$, $\mathit{cst}_3$, and $\mathit{cst}_4$ correspond to $\mathit{ast}'$.

> 154: Why is "Num 0" missing in cst? (Same in the text.)

Nothing is missing there. `Num` is one of the constructs of the abstract syntax, while the corresponding construct in the concrete syntax is `Lit`.


> 186: Also discuss the second region in Fig. 3
TODO:


> 214: The criterion seems insufficient for addressing the primary use case of bidirectional programming - when the view changes. With this, the entire rationale for rentiveness falls down, and you later posit an additional criterion ("vertical links") in the examples section that does just that

At line 214, it is a figure. But we believe Part I has answered this question already.


> 267: The typography is really confusing - some of the subsubsections are numbered, whereas others aren't.

Some numbered are subsubsections, while other unnumbered are paragraphs.


> 267: At this point, I have only a hazy notion of what a region is - is it a pattern + path? The paths seem to disappear at some point and get re-introduced later.

Yes, a region is a (region) pattern plus a path.


> 270: What if a fragment got deleted? What if two get swapped?

Answered in Part I.


> 280: "family of sets": The definition only seems to have a set?
TODO:


> 286: Why is beneficial to talk of region patterns as "properties"? It seems to make the presentation hard to understand.

It is a kind of abstraction. Because in other scenarios, we definitely can have properties other than region patterns.


> 347: At this point, I'm very unclear as to what problem you're solving.

As the beginning of 2.2.3 states, we want to "... make Hippocraticness an extreme case of the triangular guarantee" states (line 330).
This requires us to define *uniquely identifying property sets* at line 347.


> 445: The use of Arith here is confusing, as you note yourself. The child construct appears out of thin air - what's the property language exactly?

We use the type `Arith` here for illustration, because it is simpler than the mutual recursive data type `Expr` and `Term` and the tree can be smaller.
The sentence in the parentheses at line 445 just want to notify the author that, there is no restriction on any view type's property sets to be uniquely identifying. (Since we are talking about asymmetric lenses, it is unavoidable that views are simpler.)

The child construct just resembles jigsaw pieces, as we explained 20 lines above.

> 493: "somewhat obscure": definitely true ...

We mentioned that it can be safely skipped on first reading.


> 569: So it seems you can only handle structural recursion - true?

True. We should have had discussions on it. Refer to the question above " > The language in Section 3 is clean, though lacks expressiveness... ".


> 572: I'm confused at this point - what's the DSL construct? The inference rule or the pattern correspondence? It's the latter, so what's the point of the former?

Sorry for not making it clear, the DSL constructs are consistency relations (defined by inductive rules, which you call it inference rules).
The comparison of inference rules is just an attempt to make the semantics of inductive rules clearer to the user.


> 634: Why is the restriction necessary? It seems to make many programming tasks awkward.

Please tell us what kind of bidirectional programming tasks will be awkward with the restriction; otherwise it is hard to answer the question.



> 639: "respect relative positions": What does that mean?

We suppose you mean line 689. It means that, for example, if region A in the original source is above region B, then in the new source region A is still above region B.


> 690: The "S" in SPat seems to refer to the source type - shouldn't it be Pat S or something?

We suppose you mean line 699. We do not know why this confusion arises.


> 705: You describe some auxiliary functions, but leave out some others, without a clear rule as to which is which. Generally, the description of the algorithms is hard to follow, as parts are in pseudocode and some are in prose.

We apologise that there are many typos in this subsection, making the work even hard to understand. We believe that we have fixed (most of) them.
There is detailed code generation process in the appendix.


> 771: "endpoint of some input links": An example for >1 input links would help illustrate this.

It means "the root of the view is connected to some input links." We will modify this in the new version.


> 821: So if the second-order properties are important, why do they get added as an afterthought?

Because second-order properties are built upon first-order properties (precisely, and their proofs), so they are indeed added afterwards.
In addition, second-order properties are so important to the theorem while not so important to this particular implementation, because this particular implementation respects all the possible second-order properties, as explained in the paper.


> 843: At this point, I can't follow the prose algorithm description anymore.

TODO: Maybe you can follow it in our next version.


> 910: The introduction of vertical links is reasonable, but pretty much throws out the argument for retentiveness.

Answered in Part I


> 1068: How do comments and layout get preserved?

As Fig.1 illustrates, CST nodes contain annotations (comments and possibly layouts). By retentiveness, if the node in the new source comes from the old source, the annotation information is also preserved, although it is not in the AST.
Note that concrete syntax trees (parse trees) indeed correspond to program text.


> 1089: Why is "operational semantics" a problem

Because operational semantics describes how to do a job, rather than the result after doing a job. Here, we want to say that it does not exhibit the retention of information clearly.


> 1104: "would hinder": Well, does it hinder or not?

We believe it hinders, as the immediate example shows.


> 1118: "imagine": I don't get the point - you have a DSL that gets elaborated into Haskell. You could easily elaborate into those parameters.

TODO:


> 1148 "tend to represent": do they or don't they?

We assume you mean 1184.
The paper by Diskin et al. does not provide the implementation of delta lenses. In one subsection, they discuss potential representation of deltas, which are edits. So we believe they tend to do so.


> 1247: It's unclear what that final sentence means - it would require drastic changes to cater to parameterised types? Or these changes are straightforward but drastic?

TODO:




> 73: "no much" -> "Not much"
> 34: "wild range" -> "wide range"
maybe both are correct
> 97: Can't parse that sentence: "occurred" -> "occurring"?
> 192: "consistent links" -> "consistency links"
> 273: "kinds of property" -> "kinds of properties"
> 555: "yet sufficiently to handle" -> "yet sufficient for handling"
> 1049: "sugars" -> "sugar"
> 1080: "mathing" -> "Matching"
> 1197: "slightly" -> "briefly"





## For Review #98D
> Paper summary

> Asymmetric lenses are well-established as a means for maintaining a bidirectional correspondence between a data source and a view on that data. However, they offer no way of specifying how auxiliary data is allowed to be updated in the source when the view is updated. This means, for instance, that meta data may end up being garbled by an update. The paper addresses this problem by introducing retentive lenses, which augment asymmetric lenses with a notion of links which must be maintained by updates.

> The paper motivates the problem with simple examples, describes the theory of retentive lenses, and outlines the design of a DSL for programming with retentive lenses. It is reasonably well written (modulo many minor grammatical errors) and presents a novel solution to a practical problem. The authors try to underplay the lack of composability of their approach, but I find this rather disappointing and do not really buy their excuse that parser generators are not composable either.

> Rating rationale and comments for author
> 
> By and large the design of retentive lenses seems quite elegant and well thought out. I encourage you to make a serious effort to address the issues with composability.

Thanks. We will address the issue of composability more in the next version (maybe in the appendix).

On the one hand, we do believe that it can do many things without the power of composability, compared to parser generators.

On the other hand, we slightly considered the issue of composition, as discussed in the paper.
(What we did not know is that, quotient lenses encountered very similar problem as reviewer A mentioned.)

Here are some sample informal discussions (we thought before):

Given two well-behaved lenses `lens_1 :: Lens A B` and `lens_2 :: Lens B C`,  we can define a function `comp` which accepts `lens_1` and `lens_2` as input and returns a new well-behaved lens `lens_12　:: A C` as output, and interpret it as:

```
lens12 :: Lens A C
lens12 = Comp lens1 lens2


get lens12
  = get (Comp lens lens2)
  = get lens2 . get lens1

put lens12 a c =
  let  b   =  get lens1 a
       b'  =  put lens2 b c
  in   put lens1 a b'
```

Similarly, given two retentive lenses, `rtLens_1 :: RTLens A B` and `rtLens_2 :: RTLens B C`, the composition function and its interpretation might be:

```
rtLens12 :: RTLens A C
rtLens12 = RComp lens1 lens2

get rtLens12 =
  let (B, linkAB) = get rtLens1
      (C, linkBC) = get rtLens2
  in  (C, linkAB linkComp linkBC)

put rtLens12 a c linkAC =
  let  (b, linkAB)  =  get rtLens1
       linkBC       =  (conv linkAB) linkComp linkAC
       b'           =  put rtLens2 (b, c, linkBC)
       linkBC'      =  fst (get rtLens2 b')
       linkAB'      =  conv (linkBC' linkComp (conv linkAC))
       a'           =  put rtLens1 (a, b', linkAB')
  in   a'
```
where some of the intermediate links are produced by the link-inverse operation: `conv ((pat_l,path_l),(pat_r,path_r)) = ((pat_r,path_r),(pat_l,path_l))`, which swaps the left region and right region of a link.

The definition of `RComp` is quite simple in its get direction while a bit complex in the put direction.
To understand the behaviour, thinking that in both directions, we need to first find a piece of data of type `B` as a bridge for synchronising data between type `A` and `C`.
A key point is that, some intermediate links are used, to first establish connections between `a :: A` and `b :: B`, for the parts to be finally retained in `a' :: A`.
Then we know what need to be retained when using `c :: C` updates `b :: B`, and got `b' :: B`
Finally, the retained information in `b' :: B` is used to update `a :: A`.
(The code can be simplified if we ignore the direction of link compositions.)

However, we have not proved that the composed one is really retentive. And this approach only tends to succeed in the ideal case, where `rtLens1` decomposes its view "in the same way" as `rtLens2` decomposes its source.
We leave the complex cases, where `rtLens1` and `rtLens2` have different decomposation strategies, to the future work.

(There is similar problem in quotient lenses, refer to the question "> The discussion of composability restrictions reminds me of a similar restriction ...")



> Do you have a prototype implementation? I would have been more convinced if you had included evidence and experience of having built some large examples using retentive lenses.

Yes, we have a prototype implementation in Haskell and we describe the code generation in the appendix. However, we currently do not have large examples using that DSL.


> There must be relevant related work on tree logics that you should be citing. For instance, I would expect that the work of Philippa Gardner and collaborators on context logic is relevant.

TODO: tree logics...


> I liked the related work section. It would be nice to see some more concrete examples of how retentive lenses compare and combine with other approaches, though I appreciate that you have limited space, so that might have to wait for a longer journal paper or appear in a separate appendix.

TODO: 



> 159: What do you mean by "in order"?

Just one after the other... We first present the syntax, and then present the semantics.


> 361: Presumably in get \subseteq T you are interpreting get as a relation and then reordering its components. Please spell out what you're doing here.

TODO:



> 451/452: You might consider reordering the arguments of Child so that it matches the order in which they appear in the English description of its meaning.

TODO:


> Figure 10: The CST and AST are rather similar here. I expect you can come up with a better example.

Usually the CST and AST are similar, as introduced in many compiler books (such as Modern Compiler implementation in Java by Appel).
We believe that examples presented upon more complicated and dissimilar CSTs and ASTs help to prove the expressiveness of the DSL, but they will not help much to show the usefulness of retentiveness (compared to using simple ones). 
But we will try to see if we can come up dissimilar CSTs and ASTs.


> 1188: What does "proper" mean?

The "proper" means that, to have retentiveness, the authors (Diskin et al.) should not represent deltas as edits recording changed parts (as what they tend to do in their paper). Rather, they should represent deltas as some "links" recording unchanged parts.


> I find your argument that lack of composability is OK because parser generators are not composable
rather dubious. I appreciate that lack of composability doesn't completely invalidate your approach, but it is disappointing.

Thanks. We answered it in another question above. It is our future work to work out the composition of horizontal links.


> 289: "a property" and "a proof"
> 667/668: "the path to the left-most child"
> 708: don't being a sentence with a symbol or function
TODO: 


> 15/16: "get and put functions"
> 39/40: "refinement" should be "refined"
> 61: "would" should be "may"
> 62: "should be" should be "is"
> 73: "no" should be "not"
> 86/87: "guaranteeing" should be "guaranteed"
> 555/556: "sufficient"
> 629--630: "as we have already seen in the examples of Section 3.1"
> 673/674: perhaps "bidirectional transformation" rather than "function"
> 685: "transforming"
> 727: "Such pattern" should be "Such a pattern"
> 777/778: "Such wrapper" should be "Such a wrapper"
> 1064/1065: "arguments" should be "arguments:" (for disambiguation)
> 1080: "mathing" should be "matching"
> 1134: "a results" should be "a result"
> 1135: "lenses" should be "lens"
> 1135: "reason the" should be "reason about the"
> 1139: "researches" should be "research"
> 1140: "the origin" should be "origin"
> 1146: "number of times a row is used"
> 1148: "copied" should be "copied from"
> 1150: "resemble the" should be "resemble"
> 1170: ",)" should be "),"
> 1175: "the edit lenses" should be "edit lenses"
> 1184: Why is "retentive" in quotes?
> 1196: "usage" should be "use"
> 1197: "slightly" should be "briefly"
> 1217: delete "our"
> 1281: "the composition" should be "composition"
> 1222: "." should be ","
> 1231/1232: "identifying" should be "identifying property sets"
> 1231/1232: "the get and put" should be "get and put"
> 1243/1244: "assign" should be "assigns"
