\section{Conclusions and Future Work}
\label{sec:conclusion}

In this paper we have introduced a semantic framework of (asymmetric) retentive lenses, and designed a DSL for writing retentive transformations between simple algebraic data types.
Retentive lenses enjoy a fine-grained local Hippocraticness, which subsumes the traditional global Hippocraticness.
The potential use of retentiveness has been illustrated in case studies on resugaring and code refactoring.
% In this sense, every state-based well-behaved lens can be lifted to a (weak) retentive lens.
%
In the last part of the paper, we briefly discuss the potential future work on retentive lenses, regarding composability, generalisation, and parametricity.
% composability of retentive lenses, which is also part of our future work.

% \begin{figure}[t]
% % \setlength{\mathindent}{0em}
% \begin{minipage}[t]{0.45\textwidth}
% %
% \begin{code}
% lens12 :: Lens A C
% lens12 = lens1 `comp` lens2

% get lens12 = get lens2 . get lens1

% put lens12 a c =
%   let  b   =  get lens1 a
%        b'  =  put lens2 b c
%   in   put lens1 a b'
% \end{code}
% \end{minipage}%
% %
% \begin{minipage}[t]{0.45\textwidth}
% \begin{code}
% lens_12 :: RetLens AC
% lens_12 = lens1 `comp` lens2

% get lens_12 = get lens2 . get_v lens1

% put lens_12 a c linkAC =
%   let  (b, linkAB)  =  get lens1
%        linkBC       =  (conv linkAB) linkComp linkAC
%        b'           =  put lens2 (b, c, linkBC)
%        linkBC'      =  get_v lens2 b'
%        linkAB'      =  conv (linkBC' linkComp (conv linkAC))
%   in   put lens1 (a, b', linkAB')
% \end{code}
% \end{minipage}
% \caption{Lens Compositions}
% \label{fig:composition}
% \end{figure}


\paragraph{Composability}
Traditional state-based lenses are composable.
% in the sense that given two well-behaved lenses |lens_1 :: Lens A B| and |lens_2 :: Lens B C|, there is a function (combinator) |comp| that accepts |lens_1| and |lens_2| as input while returns us a new well-behaved lens |lens_12　:: A C|.
%
% As \autoref{fig:composition} shows, the definition of |comp| is quite simple in its |get| direction while a bit complex in the |put| direction.
% To understand the behaviour, thinking that in both directions, we need to first find a piece of data of type |B| as a bridge for synchronising data between type |A| and |C|.
For retentive lenses, we can also define a |comp| function, whose behaviour is similar to that in traditional state-based lenses, apart from producing proper intermediate links for the |put| functions.
% These intermediate links are used, to first propagate the parts need to be retained in |A| to |B|,  relates 
%
% Some of these intermediate links are produced by the reverse operation on links: |conv ((pat_l,path_l),(pat_r,path_r)) = ((pat_r,path_r),(pat_l,path_l))|, which swaps the left region and right region of a link.
% (The code can be simplified if we ignore the direction of link compositions.)
% 
% 
However, there are further requirements for composing retentive lenses.
For the moment, we can only say that two retentive lenses |lens_1 :: RetLens A B| and |lens_2 :: RetLens B C| are composable, if |lens_1| and |lens_2| have the same property set on B.
In other words, for any piece of data |b :: B|, |lens_1| and |lens_2| should decompose |b| in the same way.
Here is an example showing non-composable retentive lenses: \\
\begin{center}
\includegraphics[scale=0.35,trim={2cm 10cm 2cm 10cm},clip]{pics/linkComp.pdf}
\end{center}
% \begin{figure}
% \caption{The triangular guarantee}
% \label{fig:swap_and_put}
% \end{figure}
where the first lens connects the region pattern |Neg a _| with |Sub (Num 0) _| (the grey part), while the second lens has a different decomposition strategy and further decomposes |Sub (Num 0) _| into three small region patterns and establishes links for them respectively.
It is hard to determine the result of this kind of link compositions, and we leave this to future work.



As for our DSL, we argue that it is not necessary to provide the user with composition functionality: it is just a different philosophy of design.
Take the scenario of writing a parser for example: we can choose either to use parser combinators (such as \textsc{Parsec}), or to use parser generators (such as \textsc{Happy}).
% Parser combinators provide the user with many small components and a function for composing those small components, finally into the desired parser.
While parser combinators provide the user with many small components that are composable,
parser generators usually provide the user with a high-level syntax for describing the grammar of a language using production rules (associated with semantic actions).
The generated parsers are usually used as a ``black box'', and no one expects to compose them.
% Finally the desired parser is generated as a ``black box'', and no one expects that the parsers generated from a parser generator can be composable.
Thus, since our DSL is designed to be a ``lens generator'', the user will have no difficulty in writing bidirectional transformations even without composability.


\paragraph{Generalisation}
While our theoretical framework and DSL are designed for asymmetric lenses, we believe they can be extended properly to handle symmetric settings.
Many definitions in our framework are general and not limited to symmetric lenses: for example, the notion of regions, links and uniquely identifying property sets.
%
The signature for the |get| and |put| functions might be
\begin{align*}
& |get : (s elem S, v elem V , P_s ~ Q_v) -> (v' : V, P_s ~ Q_v')| \\
& |put : (s elem S, v elem V , P_s ~ Q_v) -> (s' : S, P_s' ~ Q_v)|
\end{align*}
In addition, the region model in this paper is tailored for algebraic data types (trees), with which many functional programming languages are equipped.
For bidirectional transformations in other languages, such as object-oriented languages, we may come up with other models that are more effective.


\paragraph{Parameterised Data Types}
Unlike combinator-based approaches in which many combinators are generic and parameterised, our tiny DSL does not support parametric data types, with generic bidirectional transformations.
To imitate functional dependent types, our DSL collects all the possible source and view types and assigns each a tag,
so that |get| and |put| functions (indexed by source and view types) can pattern match against these type tags and choose a correct branch.
This approach suddenly fails when facing parameterised types, for instance, |List a|, to which it is hard to assign a tag.
We believe that the DSL might be drastically changed, to handle parameterised types and produce generic lenses.

% \begin{code}
% 	data List a = Nil | Cons a (List a)

% 	List a <---> List b
% 	Nil        ~  Nil
% 	Cons x xs  ~  Cons x xs

% 	a      <---> b

% 	get (a) (b) :: List a -> List b
% \end{code}
