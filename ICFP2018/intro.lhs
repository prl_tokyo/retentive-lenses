\section{Introduction}
\label{sec:intro}

Every now and then, we need to write a pair of programs to synchronise data.
Typical examples include: view querying and view updating in relational databases \cite{Bancilhon1981,Dayal:1982:CTU:319732.319740,Gottlob:1988:PUS:49346.50068} for keeping a database and its view in sync;
parsers and printers as front ends of compilers \cite{Matsuda:2007:BTB:1291151.1291162,Rendel:2010:ISD:2088456.1863525,Zhu:2016:PRP:2997364.2997369} for keeping program text and its internal representation in sync;
text file format conversion~\cite{macfarlane2013pandoc} (e.g., between Markdown and HTML).
All these pairs of programs should satisfy some properties in order to make synchronisation work in harmony.

\emph{Bidirectional transformations} \cite{czarnecki2009bidirectional} (BXs for short) provide a sound framework for writing these pairs of programs, where the property of \emph{well-behavedness} \cite{foster2007combinators, Stevens2008} plays a fundamental role.
Within the decade, various bidirectional programming languages and systems have been developed to help the user to write BXs easily~\cite{foster2007combinators,Bohannon:2008:BRL:1328438.1328487,barbosa2010matching,Hofmann:2012:EL:2103656.2103715,Pacheco:2014:BBF:2643135.2643141,ko2016bigul,Hidaka:2010:BGT:1863543.1863573,lutterkort2008augeas}, and well-behavedness has become the minimum, and in most cases the only, standard.

In this paper, we argue that for many applications, well-behavedness is too weak to capture the behaviour of the transformations, and thus a more refined property, which we call \emph{retentiveness}, should be developed.
We will use a small example to demonstrate how the behaviour of well-behaved programs can go beyond the user's expectation.
But before that, let us first recall (asymmetric) \emph{lenses} \cite{foster2007combinators,Stevens2008} (the most popular bidirectional transformation model) and introduce its well-behavedness in Stevens's terminologies~\cite{Stevens2008}.
%Synchronisation is about transforming multiple pieces of data such that consistency is \emph{restored} among them, i.e., a \emph{consistency relation} is satisfied after the transformation.
A lens is used to maintain a functional consistency relation~$R$ between two types $S$~and~$V$,
where $S$~contains more information and is called the \emph{source} type, and $V$~contains less information and is called the \emph{view} type.
Once there are changes in the source and (or) view, two functions |get : S -> V| and |put : S -> V -> S| are invoked to restore the consistency relation.
%; this |get| function, as reasoned by Stevens, should coincide with~|R| extensionally --- that is, |s :: S| and |v :: V| are related by~|R| if and only if |get s = v|.%
%\footnote{Therefore it is only sensible to consider functional consistency relations in the asymmetric setting.}
A bidirectional transformation is well-behaved if it satisfies the following two properties (regarding the restoration behaviour of |put| with respect to |get|):
%Well-behavedness consists of two properties regarding the restoration behaviour of |put| with respect to |get| (and thus the consistency relation~|R|):
% the correctness requires
\begin{align}
|get (put s v)| &= |v|
\tag{Correctness}\label{equ:correctness} \\
|put s (get s)| &= |s|
\tag{Hippocraticness}\label{equ:hippocraticness}
\end{align}
Correctness states that necessary changes must be made so that inconsistent data becomes consistent again,
while hippocraticness says that if two pieces of data are already consistent, the restorer (|put|) must not make any change.
%A pair of |get| and |put| functions, called a \emph{lens}, is \emph{well-behaved} if it satisfies both properties.

Despite being concise and natural, these two properties are not sufficient for precisely characterising the result of an update performed by |put|, and well-behaved lenses may exhibit unintended behaviour, regarding what information is retained in the updated source.
Let us see the example, in which |get| is the first projection function over a tuple of integers and strings. Hence a source and a view are consistent if the first element of the source (tuple) is equal to the view.
\begin{multicols}{2}
\begin{code}
get :: (Int, String) -> Int
get (i, s) = i

put_1 :: (Int, String) -> Int -> (Int, String)
put_1 (i, s) i' = (i', s)
\end{code}
\begin{code}
put_2 :: (Int, String) -> Int -> (Int, String)
put_2 src    i'  | get s == i'  = src
put_2 (i, s) i'  | otherwise    = (i',  "")
\end{code}
\end{multicols}
If the view is modified by the user, it is obvious that replacing the first element of the source with the view is enough --- in other words, we should leave the second element as it is.
% Since it does not contribute to the consistency relation.
This is exactly what |put_1| does.
However, there are many other possibilities regarding the update behaviour.
For example, |put_2| additionally sets the string empty (if the source and view are not consistent) --- which retains nothing from the old source, but is also well-behaved with |get| since the string data contributes nothing to the consistency relation.


% this is also reasonable since it may consider that the old value of the string is ``dirty'' data and no longer useful.


%

The root cause of the above wide range of |put| behaviour is that while lenses are designed to enable retention of source information, the only property guaranteeing it is hippocraticness, which only requires that the \emph{whole} source should be unchanged if the \emph{whole} view is (the first case of |put_2|). In other words, if we have a very small change on the view, we are free to create any source we like (the second case of |put_2|).
This is too ``global'' in most cases, where we want a more ``local'' hippocraticness property saying that if \emph{parts} of the view are unchanged then the \emph{corresponding parts} of the source should be retained as well.
%

\todo{For this consideration, we propose a new \emph{retentiveness} property to capture the local hippocraticness.
Central to our formulation of retentiveness is to decompose sources and views into data fragments, and use \emph{links} to relate those fragments.}
A retentive |put| function accepts links as its additional input, and will produce a new source in a way that it preserves all the data fragments from the old source attached to the links.
For the examples in \todo{XX}, a retentive |put| is guaranteed to preserve the string data in the tuple, by passing it a link connecting the string data.
%
The main contributions of this paper can be summarised as follows.
% \zhenjiang{The following should be rewritten after the contents in the sections are fixed.}
\begin{itemize}
  \item We propose a semantic framework of retentive lenses, in particular for algebraic data types.
  % A retentive lens should be able to decompose a source and its consistent view into two set of properties (respectively) so that links can relate them.
  % The set of properties on source is required to be \emph{uniquely identifying},
  % so that we can specify whatever in the original source should be retained after an update.
  In our framework, Hippocraticness is subsumed by Retentiveness, and any (state-based) well-behaved lens can be lifted to a retentive lens (\autoref{sec:framework}).
  % Later, we show that the set of properties from decomposing a tree into regions cannot be uniquely identifying, and ...
  % Later, we show that how to handle algebraic data types practically.

%
  \item To show that retentive lenses are feasible, we design a tiny but non-trivial domain-specific language, which is able to describe many interesting tree transformations.
  % It is capable to solve the problem we mentioned before and the |put| generated from it can produce all of the desired CSTs shown in \autoref{fig:running_example}.
  We first show a flavour of the language, by giving a solution to the problem in \autoref{fig:running_example}.
  Then the syntax and semantics of the DSL are presented in order.
  %
  We also prove that any program written in our DSL gives rise to a pair of retentive |get| and |put| (\autoref{sec:dsl}).
  % regarding first-order properties.
  % Although our current implementation and proof is not adapted to deal with second-order properties, we show that there is a systematic way to generate and process them, and our implementation in fact obeys them. 
  % (\autoref{sec:proof} and appendixes)
%
  \item To further show that retentiveness is useful, we present case studies in two different areas: resugaring~\cite{pombrio2014resugaring,Pombrio:2015:HRC:2784731.2784755} and code refactoring~\cite{fowler1999refactoring}, showing that retentiveness simplifies the design of tools and provides additional guarantees (\autoref{sec:case_studies}).
%
\end{itemize}

Detailed related work about various alignment strategies, provenance between two pieces of data, and operational-based BXs (\autoref{sec:related_work}).
Conclusions and future work regarding composability and generalisation of the retentive lens framework come later (\autoref{sec:conclusion}).
