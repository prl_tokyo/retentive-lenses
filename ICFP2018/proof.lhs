\subsection{Retentiveness}
\label{subsec:proof}

The |get| and |put| functions specified above satisfy the retentiveness requirement (\autoref{prop:put}) of our definition of retentive lenses (\autoref{def:retentive_lenses}), as we will prove later. 
But they do not form a retentive lens immediately, since they do not generate and process second order properties, they do not satisfy the uniquely identifying requirement (\autoref{prop:getU}) of retentive lenses. 
In fact, our |get| and |put| functions respect \textit{all possible} second order properties by default no matter they are required by the user or not, which is exactly the reason why we leave out second order properties from the interface of |get| and |put| functions.
In this subsection, we will show that |get| and |put| functions in our semantics can be lifted to a version producing and accepting second order properties, and that version of |get| and |put| will form a retentive lens.

%format mprop2 = "\mathit{Prop^2}"
\subsubsection{Second Order Properties}
Before defining the lifted version of |get| and |put|, we need to formalise second order properties in our DSL.
As explained in \autoref{sec:second-order}, they will be properties on the proofs of first order properties (region patterns).
In particular, we will use them to specify either (1) the region proving some pattern is the root of the tree, or (2) the region proving some pattern is the $n$-th child of the region proving another pattern. Thus we define second order properties as a set |mprop2|:
\begin{align*}
|mprop2| &=   |NamedPat| \,\uplus\, (|NamedPat| \times |NamedPat| \times \mathbb{N}) \\
|NamedPat| &= |Pat| \times \mathbb{N}
\end{align*}
where |NamedPat| is the set of patterns paired with a number so that second order properties can refer to a region pattern with its number. We also define $|NamedRegion| = |NamedPat| \times |Path|$.

\subsubsection{Lifted Get and Put}
For a pair of |mgetsv| and |mputsv| functions denoted by our semantics with signatures:
\begin{align*}
\getsv : |S| \rightarrow |V| \times (|Region| \sim |Region|) \quad \quad \quad \quad \putsv : |T| \rightarrow |S|
\end{align*}
where |T| is a subset of $|S| \times |V| \times (|Region| \sim |Region|)$. Based on them, we will construct a |get'| and a |put'| function with the following signatures:
\begin{align*}
|get'| : |S| \rightarrow |V| \times (|SatProp| \sim |SatProp|) \quad \quad \quad \quad |put'| &: |T'| \rightarrow |S|
\end{align*}
where $|SatProp| = |NamedRegion| \uplus |mprop2|$ and |T'| is a subset of $|S| \times |V| \times (|SatProp| \sim |SatProp|)$. 

The construction of |put'| is trivial. |put' s v ls| simply filters out all links of second order properties in |ls| and turns links of |NamedRegion| into links of |Region| by removing all names, and then uses the result of |put| as its result. 

To construct |get'|, its returned view is the same as the returned view of |mgetsv|. For the returned links, besides all links of regions produced by |mgetsv|, |get'| will also generate a set of second order properties completely characterising the relative positions of all regions produced by |mgetsv|. More precisely, (1) |get'| first transforms all links of |Region| produced by |mgetsv| into links of |NamedRegion| by giving unique names to region patterns of every link. (2) Then, for the link connecting the regions at the root of the source and the view, |get'| creates two |mprop2| asserting those two regions are roots, and the pair of those two |mprop2| will be a link. (3) And also, for every pair of source regions $|sr|_1$ and $|sr|_2$, if $|sr|_1$ is the $x$-th child of $|sr|_2$, then we create a $|p_s| \in |mprop2|$ asserting the named region pattern in $|sr|_1$ should be the $x$-th child of the named region pattern in $|sr|_2$. Similarly, for the view region $|vr|_1$ and $|vr|_2$ corresponding to $|sr|_1$ and $|sr|_2$ respectively, we also create $|p_v| \in |mprop2|$ to assert the relative position of $|vr|_1$ and $|vr|_2$, then $(|p_s|, |p_v|)$ will also be a link of second order properties returned by |get'|.

The last piece to construct |get'| and |put'| is defining |T'|, the domain of |put|.
\subsubsection{Retentiveness}  
\begin{theorem}
|get'| and |put'| defined above form a retentive lens.
\end{theorem}

\hrule
\vspace{5em}

In this section, we present the following theorem and the sketch of the proofs. Complete proofs can be found in the appendices.

\begin{theorem}
  \textnormal{Given a source |s| of type |st| and a view |v| of type |vt|, the pair of |get| and |put| functions generated from a well-formed program in our DSL satisfies:}
  \begin{equation}
    |get st vt s = (v,hls)| \ \Rightarrow \ |put st vt s v hls = s|
    \tag{Hippocraticness}
  \end{equation}
  \begin{equation}
    |put st vt s v hls = s'| \ \Rightarrow \ |get_v s' = v|
    \tag{Correctness}
  \end{equation}
  \begin{equation}
    |get (put st vt s v hls) = (_ , hls')| \ \Rightarrow \ |fstR linkComp hls| \subseteq |fstR linkComp hls'|
    \tag{Retentiveness}
  \end{equation}
\end{theorem}
% \todo{Since many functions such as |get| and |put| produce tuples, and in many cases only a part of the tuple is used, we attach subscripts to those functions to denote performing projection over the result tuples after their execution.
% The subscripts |s|, |v|, and |l| mean to obtain the source part, view part, or links part of a tuple respectively.}
Where we treat a list of links (of type |[(Region,Region)]|) to be a relation between source regions and view regions.
We also define |fstR| to be a relation between an element and a tuple containing it at the first position.
Thus the reader should find that the three laws above match the theoretical framework in \autoref{sec:links_and_retentiveness} if we ignore the input type tags for |get| and |put|.


% $$(\forall n. \ (\forall m. \ m < n \rightarrow P \ m) \rightarrow P \ n) \rightarrow P \ n$$

\subsection*{Proof Sketch} \todo{Before proving the above three properties, we need to fix the triple space |T| for the generated lenses, and show that $\ldom (|fstR linkComp (get_l st vt s)|)$ is uniquely identifying. This is done in ...}
%
In addition, the proof of Hippocraticness is not required, as we have shown in \autoref{sec:links_and_retentiveness} that it is a direct result of Retentiveness.
Now let us depict the proof sketch of the remaining two properties.

The proofs of Correctness and Retentiveness can be established simultaneously, by inspecting the results of |get| after |put|.
The key to our proofs is to make inductions on the height of the view:
%
Given a source state |s|, a view state |v| of height |h|, and correspondence links |hls|,
we can assume that Correctness and Retentiveness already hold for all the views |v'| of height less than |h|,
then we prove that the two properties also hold for any view |v| of height |h| using induction hypotheses.
%
The structure of the proofs follows the structure of the generated lenses, and thus can be divided into two cases in general:
\begin{itemize}
  \item When there is no link at the top of the view |v|, we inspect the result of |get| after |putNoLink|.
  \item When there are links at the top of the view |v|, we inspect the result of |get| after |putWithLinks|. This case will be further reduced to the first case, because after handling the links at the top of the view, |putWithLinks| will invoke |putNoLink|.
\end{itemize}
%

\todo{our |put| obeying the inferred second-order properties ...}

We complete the detailed proofs by doing equational reasoning and symbolic computing: which is mainly a process of unfolding function definitions and substituting (local) variables.

% We do equational reasoning by expanding the definitions of generated code of |get| and |put|, and prove that |get_v (put_s st vt s v hls) = v| and |put_l st vt s v hls linkComp get_l (put_s st vt s v hls) = hls|, which are the one-line style of Correctness and Retentiveness.



