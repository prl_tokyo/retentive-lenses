% For double-blind review submission, w/o CCS and ACM Reference (max submission space)
% \documentclass[acmsmall,fleqn,review,anonymous]{acmart}\settopmatter{printfolios=true,printccs=false,printacmref=false}

%% For double-blind review submission, w/ CCS and ACM Reference
% \documentclass[acmsmall,review,anonymous]{acmart}\settopmatter{printfolios=true}

%% For single-blind review submission, w/o CCS and ACM Reference (max submission space)
%\documentclass[acmsmall,review]{acmart}\settopmatter{printfolios=true,printccs=false,printacmref=false}

%% For single-blind review submission, w/ CCS and ACM Reference
% \documentclass[acmsmall,review]{acmart}\settopmatter{printfolios=true}

% For final camera-ready submission, w/ required CCS and ACM Reference
\documentclass[acmsmall,nonacm]{acmart}\settopmatter{}


%include polycode.fmt
\arrayhs

%format emptyset = "\emptyset"

%format ^   = "\ "
%format ^^  = "\;"
%format ^^^ = "\!\;"
%format ^^^^ = "\!"

%format ::= = "\Coloneqq"

%format forall a = "\forall " a
%format exists a = "\exists " a
%format ldot = "."


%format Set_1
%format Pat_i
%format P_s
%format P_s' = " P_{s''} "
%format P_ss' = " P_{ss''} "
%format subset = " \subset "
%format subseteq = " \subseteq "
%format setconj  = " \cap "
%format sigma   = " \sum "
%format elem    = " \in "


%format preyV = "\textit{prey}V"
%format STypes = "S\mkern-3mu\textit{Types}"
%format VTypes = "V\mkern-3.5mu\textit{Types}"
%format S_ExprArithCase0 = "\textit{S\_ExprArithCase0}"
%format V_ExprArithCase0 = "\textit{V\_ExprArithCase0}"
%format prexV = "\textit{prex}V"
%format dollar1 = "\$1"
%format dollar2 = "\$2"
%format dollar3 = "\$3"
%format typeOf = "\textit{typeOf}"

%format pattern_l
%format pattern_r
%format path_l
%format path_r
%format path_t
%format pattern_v
%format path_b
%format pt_1
%format p_1l
%format p_1r
%format pt_2
%format pt_v
%format pt_l
%format T_i
%format pt_r
%format p_2l
%format p_2r
%format vl_1
%format tyX_i
%format tyY_j
%format *** = "*\!\!*\!\!*"

%format e_1
%format e_1' = "e_1''"
%format e_2
%format e_2' = "e_2''"
%format e_3
%format e_b



%format == = "\mathop{\shorteq\kern 1pt\shorteq}"
%format ~ = "\mathrel\sim"
%format <== = "\mathrel\Leftarrow"
%format ==> = "\mathrel\Rightarrow"
%format <--> = "\leftrightarrow"
%format --> = "\rightarrow"
%format <---> = "\leftrightarrow"
%format <===> = "\Longleftrightarrow"
%format <==> = "\Leftrightarrow"
%format (VEC(x)) = "\vv{"x"}"
%format (SEMANTIC(x)) = "\llbracket " x "\rrbracket "
%format (VARS(x)) = "\mkern-\thickmuskip(\vv{" x "})"
%format (SUBST(x)(y)) = "\mkern-\thickmuskip[\vv{" x "}/\vv{" y "}]"


%format (ToRegions(x)) = "\mathsf{ToRegions}\llbracket " x "\rrbracket "
%format (InvVarP(x)) = "\mathsf{InvVarP}\llbracket " x "\rrbracket "
%format (GenInsSubtree(x)) = "\mathsf{GenInsSubtree}\llbracket " x "\rrbracket "

%format (GenPutNoLink2 (x) (y)) = "\mathsf{GenPutNoLink} " ( x ) " \llbracket " y "\rrbracket "
%format (GenGet(x)) = "\mathsf{GenGet}\llbracket " x "\rrbracket "
%format (GenGet2 (x) (y)) = "\mathsf{GenGet} " ( x ) " \llbracket " y "\rrbracket "
%format (GenPut(x)) = "\mathsf{GenPut}\llbracket " x "\rrbracket "
%format (GenRepSubtree(x)) = "\mathsf{GenRepSubtree}\llbracket " x "\rrbracket "
%format (MkLink(x))   = "\mathsf{MkLink}\llbracket " x "\rrbracket "
%format (MkTag(x))   = "\mathsf{MkTag}\llbracket " x "\rrbracket "
%format (Pref(x)) = "\mathsf{Pref}\llbracket " x "\rrbracket"
%format typeOfTT = "\mathsf{typeOf}"
%format (CTypeOf(x)) = "\mathsf{TypeOf}\llbracket " x "\rrbracket"
%format (getVar(x)) = "\mathsf{getVar}\llbracket " x "\rrbracket"
%format (MkPat(x)) = "\mathsf{MkPat}\llbracket " x "\rrbracket "
%format (MkRegPat(x)) = "\mathsf{MkRegPat}\llbracket " x "\rrbracket "
%format (local(x)) ="loc\_ " x " "
%format ST ="\mathsf{ST}"
%format VT ="\mathsf{VT}"
%format MkLinkt = " \mathit{MkLink} "
%format (varsTT(x))  = "\mathsf{vars}\llbracket " x "\rrbracket"

%format itOf    = "\mathit{of} "
%format itCase  = "\mathit{case} "
%format itLet   = "\mathit{let} "
%format itIn    = "\mathit{in} "
%format itWhere = "\mathit{where} "
%format whereTT = "\mathsf{where} "
%format itMkPat = "\mathit{MkPat} "

%format GetPut = "\mathsf{GetPut} "
%format PutGet = "\mathsf{PutGet} "

%format Correctness = "\mathsf{Correctness} "
%format Hippocraticness = "\mathsf{Hippocraticness} "
%format Retentiveness = "\mathsf{Retentiveness} "

%format C_1
%format C_2
%format C_n

%format spat_k
%format spat_k'
%format vpat_k
%format vpat_k'
%format spat_v

%format type_s
%format Type_s
%format Type_v

%format spat_j
%format vpat_j

%format cond_1
%format cond_2
%format cond_3
%format cond_4


%format t_1
%format T_1
%format T_s
%format T_v
%format t_2
%format T_2
%format s_0
%format s_1
%format S_1
%format S_2
%format V_12 = " V_{12} "

%format s_1' = " s_1'' "
%format s_2
%format s_2' = " s_2'' "
%format s_3
%format s_4
%format s_i
%format v_i
%format s_01
%format s_02
%format v_0
%format v_1
%format v_2
%format v_3
%format l_0
%format l_1

%format ls_j'
%format ls_j

%format ls_i'
%format ls_0' = " \mathit{ls}_0'' "
%format ls_1'
%format ls_n'
%format ls_i
%format ls_0
%format ls_1
%format ls_2
%format ls_3
%format ls_n

%format hl_i'
%format hl_0' = " \mathit{hl}_0'' "
%format hl_1'
%format hl_n'
%format hl_i
%format hl_0
%format hl_1
%format hl_2
%format hl_3
%format hl_n


%format vls_0
%format vls_1
%format vls_1' = " \mathit{vls}_1'' "
%format vls_2
%format vls_3
%format vls_4
%format vls_0' = " \mathit{vls}_0'' "
%format vls_n
%format vls_n'
%format vls_i
%format vls_i'

%format hls_0
%format hls_1
%format hls_1' = " \mathit{hls}_1'' "
%format hls_2
%format hls_3
%format hls_0' = " \mathit{hls}_0'' "
%format hls_n
%format hls_n'
%format hls_i
%format hls_i'


%format ys_0
%format ys_1
%format ys_n
%format ys_0' = " \mathit{ys}_0'' "
%format ys_1' = " \mathit{ys}_1'' "
%format ys_n' = " \mathit{ys}_n'' "

%format y_0'  = " y_0'' "
%format y_1'  = " y_1'' "
%format y_i'  = " y_i'' "
%format y_n'  = " y_n'' "

%format vl_0
%format vl_1
%format vl_2
%format vl_3
%format vl_4
%format vl_i
%format vl_i'
%format hl_0
%format hl_1
%format hl_2
%format hl_3
%format hl_4
%format imag_0
%format x_0
%format x_1
%format x_2
%format x_i
%format x_n
%format x_s
%format x_v
%format y_0
%format y_1
%format y_2
%format y_i
%format y_j
%format y_s
%format y_v
%format y_n
%format z_0
%format z_i
%format z_n
%format y_n
%format y_n
%format prefX_0
%format prefX_i
%format prefX_n
%format prefY_0
%format prefY_i
%format prefY_n
%format env_i
%format env_0
%format env_n
%format get_1
%format put_L

%format get_v
%format get_l
%format put_s
%format put_l
%format put_1
%format put_2
%format put_3
%format inj_s
%format inj_l
%format inj_1
%format inj_2

%format p_b
%format p_l
%format p_u
%format p_r
%format vl'_0
%format hl'_0
%format pat_1
%format pat_n
%format pat_l
%format Pat_s
%format Pat_v
%format pat_r

%format linkComp = "{\cdot}"

%format exclude = " \backslash\!\backslash "
%format backslash = "\backslash"

%format lens_1
%format lens_2
%format lens_12 = "lens_{12}"
%format lens_AB = "lens_{AB}"
%format lens_BC = "lens_{BC}"
%format lens_AC = "lens_{AC}"

%format get_AB = "get_{AB}"
%format get_BC = "get_{BC}"
%format get_AC = "get_{AC}"

%format put_AB = "put_{AB}"
%format put_BC = "put_{BC}"
%format put_AC = "put_{AC}"


%format l_ab  = "l_{ab}"
%format l_ab' = "l_{ab}^{''}"
%format l_bc  = "l_{bc}"
%format l_bc' = "l_{bc}^{''}"
%format l_ac  = "l_{ac}"
%format l_ac' = "l_{ac}^{''}"
%format ls_ab  = "ls_{ab}"
%format ls_bc  = "ls_{bc}"
%format ls_bc' = "ls_{bc^{''}}"
%format ls_ac  = "ls_{ac}"
%format ls_ac'  = "ls_{ac^{''}}"

%format ls_b'c  = "ls_{b^{''}c}"
%format ls_ab'  = "ls_{ab^{''}}"
%format ls_a'c  = "ls_{a^{''}c}"
%format ls_a'b  = "ls_{a^{''}b}"
%format ls_a'b'  = "ls_{a^{''}b^{''}}"
%format ls_b'c'  = "ls_{b^{''}c^{''}}"
%format ls_a'c'  = "ls_{a^{''}c^{''}}"

%format ls_c'a  = "ls_{c^{''}a}"
%format ls_c'b  = "ls_{c^{''}b}"
%format ls_c'b' = "ls_{c^{''}b^{''}}"

%format p_AB  = "p_{AB}"
%format p_AB' = "p_{AB}^{''}"
%format p_BC  = "p_{BC}"
%format p_BC' = "p_{BC}^{''}"
%format p_AC  = "p_{AC}"
%format p_AC' = "p_{AC}^{''}"

%format sp_AB  = "sp_{AB}"
%format sp_AB' = "sp_{AB}^{''}"
%format sp_BC  = "sp_{BC}"
%format sp_BC' = "sp_{BC}^{''}"
%format sp_AC  = "sp_{AC}"
%format sp_AC' = "sp_{AC}^{''}"

%format vp_AB  = "vp_{AB}"
%format vp_AB' = "vp_{AB}^{''}"
%format vp_BC  = "vp_{BC}"
%format vp_BC' = "vp_{BC}^{''}"
%format vp_AC  = "vp_{AC}"
%format vp_AC' = "vp_{AC}^{''}"

%format (conv (x)) = x "^{\circ}"

%format HasRegion = "\mathsf{HasRegion}"
%format IsTop = "\mathsf{IsTop}"
%format RelPos = "\mathsf{RelPos}"
%format VertAligned = "\mathsf{VertAligned}"


\usepackage[UKenglish]{isodate}

\makeatletter
\newcommand{\shorteq}{%
  \settowidth{\@@tempdima}{-}%
  \resizebox{\@@tempdima}{\height}{=}%
}
\makeatother

%% Journal information
%% Supplied to authors by publisher for camera-ready submission;
%% use defaults for review submission.
% \acmJournal{PACMPL}
% \acmVolume{1}
% \acmNumber{CONF} % CONF = POPL or ICFP or OOPSLA
% \acmArticle{1}
% \acmYear{2018}
% \acmMonth{1}
% \acmDOI{} % \acmDOI{10.1145/nnnnnnn.nnnnnnn}
% \startPage{1}

%% Copyright information
%% Supplied to authors (based on authors' rights management selection;
%% see authors.acm.org) by publisher for camera-ready submission;
%% use 'none' for review submission.
\setcopyright{none}
%\setcopyright{acmcopyright}
%\setcopyright{acmlicensed}
%\setcopyright{rightsretained}
%\copyrightyear{2018}           %% If different from \acmYear

%% Bibliography style
\bibliographystyle{ACM-Reference-Format}
%% Citation style
%% Note: author/year citations are required for papers published as an
%% issue of PACMPL.
\citestyle{acmauthoryear}   %% For author/year citations



\usepackage{booktabs}   %% For formal tables:
                        %% http://ctan.org/pkg/booktabs
\usepackage{subcaption} %% For complex figures with subfigures/subcaptions
                        %% http://ctan.org/pkg/subcaption

\usepackage{microtype}

\usepackage{multicol}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{stmaryrd}
\usepackage{esvect}
\usepackage{listings}
\usepackage{zi4}
\usepackage[T1]{fontenc}
\usepackage{upquote}
\usepackage{fancybox}
\usepackage{hyperref}
\usepackage{pifont}
% \usepackage[normalem]{ulem}
\usepackage{textcomp}

% \usepackage[perpage]{footmisc}
\usepackage{longtable}


\usepackage{tikz}

\usepackage{xcolor}

\definecolor{scarlet}{RGB}{ 141, 27, 53}
\definecolor{steelblue}{RGB}{ 11, 65, 108}
\hypersetup{
  unicode,
  bookmarks=true,
  bookmarksnumbered=true,
  bookmarksopen=true,
  linktocpage,
  colorlinks=true,
  citecolor=scarlet,
  linkcolor=steelblue,
  urlcolor=steelblue,
  setpagesize=false,
  linktoc=all
}
\urlstyle{same}

\newtheorem{prereq}{Prerequisites}
\definecolor{light-gray}{gray}{0.92}

\newcommand\numberthis{\addtocounter{equation}{1}\tag{\theequation}}

\newcommand\EscapeSans[1]{{-"\text{#1}"-}}

\newcommand*{\draft}[1]{\textcolor{red}{#1}}

\newcommand*{\scName}[1]{\textsc{#1}}
\newcommand*{\qtext}[1]{\text{`{\tt #1}'}}
\newcommand*{\nt}[1]{\mathit{#1}}
\newcommand*{\seman}[1]{[\kern-.16em[ #1 ]\kern-.16em]}
\newcommand*{\fun}[1]{\textsc{\small #1}}
\newcommand*{\iter}[3]{\bigl\langle#3 \bigm|| #1 \in #2\bigr\rangle}

\newcommand{\pfun}[0]{\mathrel{\ooalign{\hfil$\mapstochar\mkern5mu$\hfil\cr$\to$\cr}}}
\newcommand{\powerset}[1]{\mathcal P \left({#1}\right)}
\newcommand{\propsat}[3]{#1;#2 \models #3}
\newcommand{\myset}[1]{\left\{\,#1\,\right\}}
\newcommand{\validLink}[3]{#1 \xleftrightarrow[]{#3} #2}
\newcommand{\strongValidLink}[4]{#1 \xleftrightarrow[#4]{#3} #2}
\newcommand{\srcreg}[0]{\normalfont\textsc{SrcReg}}
\newcommand{\viewreg}[0]{\normalfont\textsc{ViewReg}}


\newcommand{\awa}[2]{\mathrlap{#2}\phantom{#1}} % as wide as

\newcommand{\mywhere}[0]{\quad\text{ \textbf{where} } }
\newcommand{\myspace}[0]{\quad\phantom{\text{ \textbf{where} }} }
\newcommand{\myindent}[0]{\phantom{xxxxxxxxxxx}}
\newcommand{\myindentS}[0]{\phantom{xxxx}}
\newcommand{\myindentSS}[0]{\phantom{xx}}

\newcommand*{\dom}{{\normalfont\textsc{dom}}}
\newcommand*{\ldom}{{\normalfont\textsc{ldom}}}
\newcommand*{\rdom}{{\normalfont\textsc{rdom}}}
\newcommand*{\abs}[1]{\lvert#1\rvert}
\newcommand*{\simp}{\ensuremath{^\sharp}}
\newcommand*{\numcircledmod}[1]{\raisebox{.1pt}{\textcircled{\raisebox{-.6pt} {#1}}}}
\newcommand*{\mcond}[1]{\{\small{\ #1 \ \}}}
\newcommand*{\bfcomp}{\mathop{\,\triangleleft\,}}

\newcommand*{\bb}{\begin{array}{lllll}}
\newcommand*{\ee}{\end{array}}
\newcommand*{\sem}[1]{[ \!\! |\! [  #1 ]\!\! ] \!| } %  semantics
\newcommand*{\q}[1]{\text{`#1'}}
\newcommand*{\key}[1]{\textbf{#1}}
\newcommand*{\reason}[1]{\mbox{\small~~~~\{ #1 \}}}

\newenvironment{centerTab}
  {
  \begingroup
  \setlength{\parskip}{0ex}
  \par
  \begingroup
  \centering
  \begin{tabular}{c}
  }
  {
  \end{tabular}
  \par
  \endgroup
  \endgroup
  \noindent
  }

\AtBeginDocument{
\setlength{\jot}{0ex}
\setlength{\abovedisplayskip}{1.25ex plus 0.25ex minus 0.25ex}
\setlength{\belowdisplayskip}{1.25ex plus 0.25ex minus 0.25ex}

\def\figureautorefname{Fig.}
\def\sectionautorefname{Sect.}
\def\subsectionautorefname{Sect.}
\def\subsubsectionautorefname{Sect.}

\newcommand{\lemmaautorefname}{Lemma}
\newcommand{\propositionautorefname}{Proposition}
\newcommand{\corollaryautorefname}{Corollary}
\newcommand{\definitionautorefname}{Definition}
\newcommand{\exampleautorefname}{Example}
\newcommand{\notationautorefname}{Notation}
}

\allowdisplaybreaks

\lstset{
  % mathescape,
  breaklines=true,
%  xleftmargin = .5\textwidth,
%  xrightmargin = .5\textwidth,
  % numbers = left,
  basicstyle=\footnotesize\ttfamily,
  % tabsize=2, columns = space-flexible, basewidth = 0.5em,
  tabsize=2,
  columns = fixed,
  basewidth = 0.5em,
  emph={Abstract, Concrete, Directives, Actions},
  escapechar=~,
  escapeinside={(*@@}{@@*)},
  % aboveskip=\medskipamount,
  belowskip = 0ex,
  aboveskip = 0ex,
  upquote=true
}

\lstdefinestyle{dimUnnecessaries}{
  morekeywords = {Lit,Num},
  keywordstyle=\color{lightgray},
  escapeinside={(*@@}{@@*)}
}

\DeclareMathSizes{10}{9}{7}{5}
% \DeclareMathSizes{11}{10}{8}{6}

\usepackage{enumitem}
\setlist{leftmargin=*,topsep=0ex}

\usepackage{todonotes}

\begin{document}

\setlength{\mathindent}{\parindent}

%% Title information
\title[Retentive Lenses]{Retentive Lenses}         %% [Short Title] is optional;
                                        %% when present, will be used in
                                        %% header instead of Full Title.
% \titlenote{with title note}             %% \titlenote is optional;
%                                         %% can be repeated if necessary;
                                        %% contents suppressed with 'anonymous'
\begin{anonsuppress}
\titlenote{Draft manuscript (\today)}   %% \titlenote is optional;
\end{anonsuppress}
% \subtitle{Subtitle}                     %% \subtitle is optional
% \subtitlenote{with subtitle note}       %% \subtitlenote is optional;
%                                         %% can be repeated if necessary;
%                                         %% contents suppressed with 'anonymous'


%% Author information
%% Contents and number of authors suppressed with 'anonymous'.
%% Each author should be introduced by \author, followed by
%% \authornote (optional), \orcid (optional), \affiliation, and
%% \email.
%% An author may have multiple affiliations and/or emails; repeat the
%% appropriate command.
%% Many elements are not rendered, but should be provided for metadata
%% extraction tools.

%% Author with single affiliation.
\author{Zirun Zhu}
% \authornote{}          %% \authornote is optional;
                                        %% can be repeated if necessary
% \orcid{}             %% \orcid is optional
\affiliation{
  \position{}
  % \department{Information Systems Architecture Science Research Division}
  \institution{National Institute of Informatics}
  \streetaddress{2-1-2 Hitotsubashi, Chiyoda}
  \city{Tokyo}
  % \state{State2b}
  \postcode{101-8430}
  \country{Japan}
}
\email{zhu@@nii.ac.jp}

\author{Zhixuan Yang}
% \authornote{}          %% \authornote is optional;
                                        %% can be repeated if necessary
% \orcid{}             %% \orcid is optional
\affiliation{
  \position{}
  % \department{Information Systems Architecture Science Research Division}
  \institution{National Institute of Informatics}
  \streetaddress{2-1-2 Hitotsubashi, Chiyoda}
  \city{Tokyo}
  % \state{State2b}
  \postcode{101-8430}
  \country{Japan}
}
\email{yzx@@nii.ac.jp}

\author{Hsiang-Shang Ko}
% \authornote{with author2 note}          %% \authornote is optional;
                                        %% can be repeated if necessary
\orcid{0000-0002-2439-1048}             %% \orcid is optional
\affiliation{
  \position{Assistant Research Fellow}
  % \department{}
  \institution{Institute of Information Science, Academia Sinica}
  \streetaddress{128 Academia Road, Section 2, Nankang}
  \city{Taipei}
  \postcode{11529}
  \country{Taiwan}
}
\email{joshko@@iis.sinica.edu.tw}

%% Author with two affiliations and emails.
\author{Zhenjiang Hu}
% \authornote{with author2 note}          %% \authornote is optional;
                                        %% can be repeated if necessary
\affiliation{
  \position{Professor}
  \institution{Peking University}
  \department{Department of Computer Science and Technology}
  \streetaddress{5 Yiheyuan Road, Haidian}
  \city{Beijing}
  \postcode{1000871}
  \country{China}
}
\email{huzj@@pku.edu.cn}

\begin{abstract}
Based on Foster et al.'s lenses, various bidirectional programming languages and systems have been developed for helping the user to write correct data synchronisers.
The two well-behavedness laws of lenses, namely Correctness and Hippocraticness, are usually adopted as the guarantee of these systems.
While lenses are designed to retain information in the source when the view is modified, well-behavedness says very little about the retaining of information: Hippocraticness only requires that the source be unchanged if the view is not modified, and nothing about information retention is guaranteed when the view is changed.
To address the problem, we propose an extension of the original lenses, called \emph{retentive lenses}, which satisfy a new Retentiveness law guaranteeing that if parts of the view are unchanged, then the corresponding parts of the source are retained as well.
As a concrete example of retentive lenses, we present a domain-specific language for writing tree transformations; we prove that the pair of $\textit{get}$ and $\textit{put}$ functions generated from a program in our DSL forms a retentive lens. We demonstrate the practical use of retentive lenses and the DSL by presenting case studies on code refactoring, Pombrio and Krishnamurthi's resugaring, and XML synchronisation.

\keywords{lenses, bidirectional programming, domain-specific languages}
\end{abstract}

%% 2012 ACM Computing Classification System (CSS) concepts
%% Generate at 'http://dl.acm.org/ccs/ccs.cfm'.
% \begin{CCSXML}
% <ccs2012>
% <concept>
% <concept_id>10011007.10011006.10011050.10011017</concept_id>
% <concept_desc>Software and its engineering~Domain specific languages</concept_desc>
% <concept_significance>500</concept_significance>
% </concept>
% </ccs2012>
% \end{CCSXML}

% \ccsdesc[500]{Software and its engineerisng~Domain specific languages}
% \ccsdesc[500]{Software and its engineering~General programming languages}

\maketitle

\section{Introduction}
\label{sec:introduction}

We often need to write pairs of transformations to synchronise data.
Typical examples include view querying and updating in relational databases~\cite{Bancilhon1981Update} for keeping a database and its view in sync,
text file format conversion~\cite{Macfarlane2013Pandoc} (e.g.~between Markdown and HTML) for keeping their content and common formatting in sync,
and parsers and printers as front ends of compilers~\cite{Rendel2010Invertible} for keeping program text and its abstract representation in sync.
%
Asymmetric \emph{lenses}~\cite{Foster2007Combinators} provide a framework for modelling such pairs of programs and discussing what laws they should satisfy; among such laws, two \emph{well-behavedness} laws (explained below) play a fundamental role.
Based on lenses, various bidirectional programming languages and systems (\autoref{sec:relatedWork}) have been developed for helping the user to write correct synchronisers, and the well-behavedness laws have been adopted as the minimum---and in most cases the only---laws to guarantee.
In this paper, we argue that well-behavedness is not sufficient, and a more refined law, which we call \emph{Retentiveness}, should be developed.

To see this, let us first review the definition of well-behaved lenses, borrowing some of \citeauthor{Stevens2008Bidirectional}{'s} terminologies~\cite{Stevens2008Bidirectional}.
Lenses are used to synchronise two pieces of data respectively of types $S$~and~$V$, where $S$~contains more information and is called the \emph{source} type, and $V$~contains less information and is called the \emph{view} type.
Here, being synchronised means that when one piece of data is changed, the other piece of data should also be changed such that consistency is \emph{restored} among them, i.e.~a \emph{consistency relation}~$R$ defined on $S$~and~$V$ is satisfied.
Since $S$~contains more information than~$V$, we expect that there is a function |get : S -> V| that extracts a consistent view from a source, and this |get| function serves as a consistency restorer in the source-to-view direction: if the source is changed, to restore consistency it suffices to use |get| to recompute a new view.
This |get| function should coincide with~|R| extensionally~\cite{Stevens2008Bidirectional}---that is, |s : S| and |v : V| are related by~|R| if and only if $|get|(s) = |v|$.
(Therefore it is only sensible to consider functional consistency relations in the asymmetric setting.)
Consistency restoration in the other direction is performed by another function $|put| : S \times V \to S$, which produces an updated source that is consistent with the input view and can retain some information of the input source.
Well-behavedness consists of two laws regarding the restoration behaviour of |put| with respect to |get| (i.e.~the consistency relation~|R|):
\begin{align}
|get| |(put| |(s,v))| &= |v|
\tag{Correctness}\label{equ:correctness} \\
|put| |(s, get| |(s))| &= |s|
\tag{Hippocraticness}\label{equ:hippocraticness}
\end{align}
Correctness states that necessary changes must be made by |put| such that the updated source is consistent with the view; Hippocraticness says that if two pieces of data are already consistent, |put| must not make any change.
A pair of |get| and |put| functions, called a \emph{lens}, is \emph{well-behaved} if it satisfies both laws.

Despite being concise and natural, these two properties do not sufficiently characterise the result of an update performed by |put|, and well-behaved lenses may exhibit unintended behaviour regarding what information is retained in the updated source.
Let us illustrate this with a very simple example, in which |get| is a projection function that extracts the first element from a tuple of an integer and a string.
(Hence a source and a view are consistent if the first element of the source tuple is equal to the view.)
\begin{centerTab}
\begin{lstlisting}
get :: (Int, String) -> Int
get (i, s) = i
\end{lstlisting}
\end{centerTab}%
Given this \lstinline{get}\footnote{In this paper, we use Haskell notations to write functions, and concrete examples are always typeset in \lstinline{typewriter} font.}, we can define \lstinline[mathescape]{put$_1$} and \lstinline[mathescape]{put$_2$}, both of which are well-behaved with this \lstinline{get} but have rather different behaviour: \lstinline[mathescape]{put$_1$} simply replaces the integer of the source tuple with the view, while \lstinline[mathescape]{put$_2$} also sets the string empty when the source tuple is not consistent with the view.
\begin{centerTab}
\begin{lstlisting}[mathescape]
put$_1$ :: (Int, String) -> Int -> (Int, String)
put$_1$ (i, s) i' = (i', s)
\end{lstlisting}\\
\begin{lstlisting}[mathescape]
put$_2$ :: (Int, String) -> Int -> (Int, String)
put$_2$ src     i'  || get src == i'  = src
put$_2$ (i, s)  i'  || otherwise      = (i',  "")
\end{lstlisting}
\end{centerTab}%
From another perspective, \lstinline[mathescape]{put$_1$} retains the string from the old source when performing the update, while \lstinline[mathescape]{put$_2$} chooses to discard that string---which is not desired but `perfectly legal', for the string does not contribute to the consistency relation.
%
In fact, unexpected behaviour of this kind of well-behaved lenses could even lead to disaster in practice. For instance, relational databases can be thought of as tables consisting of rows of tuples, and well-behaved lenses used for maintaining a database and its view may erase important data after an update, as long as the data does not contribute to the consistency relation (in most cases this is because the data is simply not in the view). This fact seems fatal, as asymmetric lenses have been considered a satisfactory solution to the longstanding view update problem (stated at the beginning of Foster et~al.'s seminal paper~\cite{Foster2007Combinators}).

The root cause of the information loss (after an update) is that while lenses are designed to retain information, well-behavedness actually says very little about the retaining of information: the only law guaranteeing information retention is Hippocraticness, which merely requires that the \emph{whole} source should be unchanged if the \emph{whole} view is.
%
In other words, if we have a very small change on the view, we are free to create any source we like.
This is too `global' in most cases, and it is desirable to have a law that makes such a guarantee more `locally'.

To have a finer-grained law, we propose \emph{retentive lenses}, an extension of the original lenses, which can guarantee that if parts of the view are unchanged, then the corresponding parts of the source are retained as well.
Compared with the original lenses, the |get| function of a retentive lens is enriched to compute not only the view of the input source but also a set of \emph{links} relating corresponding parts of the source and the view.
If the view is modified, we may also update the set of links to keep track of the correspondence that still exists between the original source and the modified view.
The |put| function of the retentive lens is also enriched to take the links between the original source and the modified view as input, and it satisfies a new law, \emph{Retentiveness}, which guarantees that those parts in the original source having correspondence links to some parts of the modified view are retained at the right places in the updated source.


The main contributions of the paper are as follows:
\begin{itemize}
  \item We develop a formal definition of retentive lenses for tree-shaped data~(\autoref{sec:framework}).
%  \footnote{This paper focuses on the synchronisation between algebraic data types, i.e.~trees, whose structure can be considered more general than tables in relational databases. (A table is a list of tuples, and both lists and tuples can be encoded as trees.)}

  \item We present a domain-specific language (DSL) for writing tree synchronisers and prove that any program written in our DSL gives rise to a retentive lens~(\autoref{sec:retentiveDSL}).

  \item We demonstrate the usefulness of retentive lenses in practice by presenting case studies on code refactoring, resugaring, and XML synchronisation (\autoref{sec:application}), with the help of several view editing operations that also update the links between the view and the original source (\autoref{sec:editOperations}).
\end{itemize}
We will start from a high-level sketch of what retentive lenses do~(\autoref{sec:intuitiveRetentiveness}), and after presenting the technical contents, we will discuss related work~(\autoref{sec:relatedWork}) regarding various alignment strategies for lenses,
%\todo{Currently only list alignment. Add container back if space finally permits.}
provenance and origin between two pieces of data, and operational-based bidirectional transformations, before concluding the paper (\autoref{sec:conclusion}).

\begin{figure}[t]
\setlength{\mathindent}{0em}
\begin{small}
\begin{minipage}[t]{0.45\textwidth}
\begin{center}
\begin{lstlisting}[xleftmargin=0pt]
type Annot  =  String
data Expr   =  Plus  Annot Expr Term
            ||  Minus Annot Expr Term
            ||  FromT Annot Term

data Term   =  Lit   Annot Int
            ||  Neg   Annot Term
            ||  Paren Annot Expr

data Arith  =  Add Arith Arith
            ||  Sub Arith Arith
            ||  Num Int
\end{lstlisting}
\end{center}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
\begin{center}
\begin{lstlisting}
getE :: Expr -> Arith
getE (Plus   _ e  t) = Add (getE e) (getT t)
getE (Minus  _ e  t) = Sub (getE e) (getT t)
getE (FromT  _    t) = getT t

getT :: Term -> Arith
getT (Lit     _ i  ) = Num i
getT (Neg     _ t  ) = Sub (Num 0) (getT t)
getT (Paren   _ e  ) = getE e
\end{lstlisting}
\end{center}
\end{minipage}
\end{small}
\caption{Data types for concrete and abstract syntax of arithmetic expressions and the consistency relations between them as \lstinline{getE} and \lstinline{getT} functions in \scName{Haskell}.}
\label{fig:runningExpDataTypeDef}
\end{figure}
%

\section{A Sketch of Retentiveness}
\label{sec:intuitiveRetentiveness}

We will use the synchronisation of concrete and abstract representations of arithmetic expressions as the running example throughout the paper.
The representations are defined in \autoref{fig:runningExpDataTypeDef} (in \scName{Haskell}).
The concrete representation is either an expression of type \lstinline{Expr}, containing additions and subtractions; or a term of type \lstinline{Term}, including numbers, negated terms, and expressions in parentheses.
Moreover, all the constructors have an annotation field of type \lstinline{Annot} mocking up data that exist solely in the concrete representation like code comments and spaces.
The two concrete types \lstinline{Expr} and \lstinline{Term} coalesce into the abstract representation type \lstinline{Arith}, which does not include annotations, explicit parentheses, and negations---negations are considered \emph{syntactic sugar} and represented in the AST by \lstinline{Sub}.
%The consistency relations between CSTs and ASTs are defined in terms of the |get| functions---\lstinline{e :: Expr} (resp.~\lstinline{t :: Term}) is consistent with \lstinline{a :: Arith} exactly when \lstinline{getE e = a} (resp.~\lstinline{getT t = a}).

As mentioned in \autoref{sec:introduction}, the core idea of Retentiveness is to use links to relate parts of the source and view.
For data of algebraic data types (which we call `trees' or `terms'), a straightforward interpretation of a `part' is a subtree of the data.
But it is too restrictive in most cases, and a more useful interpretation of a `part' is a \emph{region} of a tree, i.e.~a partial subtree.
Partial trees are trees where some subtrees can be missing.
We will describe the content of a partial tree with a pattern that contains wildcards at the positions of missing subtrees.
In \autoref{fig:swapAndPut}, all grey areas are examples of regions; the topmost region in \lstinline{cst} is located at the root of the whole tree, and its content has the pattern \lstinline{Plus "a plus" _ _}\,, which says that the region includes the \lstinline{Plus} node and the annotation \lstinline{"a plus"}, but not the other two subtrees with roots \lstinline{Minus} and \lstinline{Neg} matched by the wildcards.
%This is one particular way of decomposing trees, and will be the one we use in the paper.

\begin{figure}[t]
\centering
\includegraphics[scale=0.65,trim={5.1cm 5.1cm 5.1cm 4.3cm},clip]{pics/swapAndPut.pdf}
\caption{Regions, links, and the triangular guarantee.}
\label{fig:swapAndPut}
\end{figure}

Having broken up source and view trees into regions, we can put in \emph{links} to record the correspondences between source and view regions.
In \autoref{fig:swapAndPut}, for example, the light red dashed lines between the source \lstinline{cst} and the view \lstinline{ast = getE cst} represent two possible links.
The topmost region of pattern \lstinline{Plus "a plus" _ _} in \lstinline{cst} corresponds to the topmost region of pattern \lstinline{Add _ _} in \lstinline{ast}, and the region of pattern \lstinline{Neg "a neg" _} in the right subtree of \lstinline{cst} corresponds to the region of pattern \lstinline{Sub (Num 0) _} in \lstinline{ast}.
The |get| function of a retentive lens will be responsible for producing an initial set of links between a source and its view.

As the view is modified, the links between the source and view should also be modified to reflect the latest correspondences between regions.
For example, in \autoref{fig:swapAndPut}, if we change \lstinline{ast} to \lstinline{ast'} by swapping the two subtrees under \lstinline{Add}, then there should be a new link (among others) recording the fact that the \lstinline{Neg "a neg" _} region and the \lstinline{Sub (Num 0) _} region are still related.
We will describe a way of computing new links from old ones in \autoref{sec:editOperations}.

When it is time to put the modified view back into the source, the links between the source and the modified view are used to guide what regions in the old source should be retained in the new one and at what positions.
In addition to the source and view, the |put| function of a retentive lens also takes a collection of links, and provides what we call the \emph{triangular guarantee}, as illustrated in \autoref{fig:swapAndPut}:
when updating \lstinline{cst} with \lstinline{ast'}, the region \lstinline{Neg "a neg" _} (i.e.~syntactic sugar negation) connected by the red dashed link is guaranteed to be preserved in the result \lstinline{cst'} (as opposed to changing it to a \lstinline{Minus}), and the preserved region will be linked to the same region \lstinline{Sub (Num 0) _} of \lstinline{ast'} if we run \lstinline{getE cst'}.
The Retentiveness law will be a formalisation of the triangular guarantee.

\section{Formal Definitions}
\label{sec:framework}

Here we formalise what we described in \autoref{sec:intuitiveRetentiveness}.
Besides the definition of retentive lenses~(\autoref{sec:retentive-lens-definition}), we will also briefly discuss how retentive lenses compose~(\autoref{sec:lensComp}).

\subsection{Retentive Lenses}
\label{sec:retentive-lens-definition}

We start with some notations.
Relations from set~$A$ to set~$B$ are subsets of $A \times B$, and we denote the type of these relations by $A \sim B$.
Given a relation $r : A \sim B$, define its \emph{converse} $r^\circ : B \sim A$ by $r^\circ = \{\, (b, a) \mid (a, b) \in r \,\}$, its \emph{left domain} by $\ldom(r) = \{\,a \in A \mid \exists b.\ (a, b) \in r \,\}$, and its \emph{right domain} by $\rdom(r) = \ldom(r^\circ)$.
The composition $r \cdot s : A \sim C$ of two relations $r : A \sim B$ and $s : B \sim C$ is defined as usual by $r \cdot s = \{\, (a, c) \mid \exists b.\ (a, b) \in r \mathrel\wedge (b, c) \in s\,\}$.
%
The type of partial functions from~$A$ to~$B$ is denoted by $A \pfun B$.
The \emph{domain} $\dom(f)$ of a function $f : A \pfun B$ is the subset of~$A$ on which $f$~is defined; when $f$~is total, i.e.~$\dom(f) = A$, we write $f : A \to B$.
We will allow functions to be implicitly lifted to relations: a function $f : A \pfun B$ also denotes a relation $f : B \sim A$ such that $(f\,x, x) \in f$ for all $x \in \dom(f)$\footnote{This flipping of domain and codomain (from $A \pfun B$ to $B \sim A$) makes function composition compatible with relation composition: a function composition $|g| \circ |f|$ lifted to a relation is the same as $|g| \cdot |f|$, i.e.~the composition of |g|~and~|f| as relations.}.


We will work within a universal set |Tree| of trees, which is inductively built from all possible finitely branching constructors.
(The semantics of an algebraic data type is then the subset of |Tree| that consists of those trees built with only the constructors of the data type.)
Similarly, the set |Pattern| is inductively built from all possible finitely branching constructors, variables, and a distinguished wildcard element |_|\,.
We will also need a set |Path| of all possible paths for navigating from the root of a tree to one of its subtrees.
The exact representation of paths is not crucial: paths are only required to support some standard operations such as $|sel| : |Tree| \times |Path| \pfun |Tree|$ such that $|sel|(t, p)$ is the subtree of~|t| at the end of path~|p| (starting from the root), or undefined if |p|~does not exist in~|t|; we will mention these operations in the rest of the paper as the need arises.
But, when giving concrete examples, we will use one particular representation: a path is a list of natural numbers indicating which subtree to go into at each node---for instance, starting from the root of \lstinline{cst} in \autoref{fig:swapAndPut}, the empty path \lstinline{[]} points to the root node \lstinline{Plus}, the path \lstinline{[0]} points to \lstinline{"a plus"} (which is the first subtree under the root), and the path \lstinline{[2,0]} points to \lstinline{"a neg"}.

We define a collection of links between two trees as a relation of type $|Region| \sim |Region|$, where $|Region| = |Pattern| \times |Path|$: a region is identified by a path leading to a subtree and a pattern describing the part of the subtree included in the region.
Briefly, a link is a pair of regions, and a collection of links is a relation between regions of two trees.
For brevity we will write |Links| for $|Region| \sim |Region|$.

% \begin{example}
% \label{ex:link-collection-example}
% The diagonal link between \lstinline{cst} and \lstinline{ast'} in \autoref{fig:swapAndPut} is represented as the link collection \lstinline[mathescape]{$\{$((Neg "a neg" _ , [2]) , (Sub (Num 0) _ , [0]))$\}$} (despite being a singleton), which can also be regarded as a relation relating \lstinline{(Neg "a neg" _ , [2])} to \lstinline{(Sub (Num 0) _ , [0])} and vice versa.
% \end{example}

An arbitrary collection of links may not make sense for a given pair of trees though---a region mentioned by some link may not exist in the trees at all.
We should therefore characterise when a collection of links is valid for two trees.
\begin{definition}[Region Containment]
\label{def:sat}
For a tree~|t| and a set of regions $\Phi \subseteq |Region|$, we say that $t \models \Phi$ (read `|t| contains $\Phi$') exactly when
\[ \forall (|pat|, |path|) \in \Phi. \quad |sel|(t, |path|) \text{ matches } |pat|\text. \]
\end{definition}
\begin{definition}[Valid Links]
\label{def:validLinks}
Given $|ls| : |Links|$ and two trees |t|~and~|u|, we say that |ls| is \emph{valid} for |t| and |u|, denoted by $\validLink{|t|}{|u|}{|ls|}$, exactly when
\[ |t| \models \ldom(|ls|) \quad\text{and}\quad |u| \models \rdom(|ls|) \text{.} \]
\end{definition}



% \begin{example}
% The link collection given in \autoref{ex:link-collection-example} is valid for \lstinline{cst} and \lstinline{ast'} because \lstinline[mathescape]{cst $\models \{$(Neg "a neg" _ , [2])$\}$}, \lstinline[mathescape]{ast' $\models \{$(Sub (Num 0) _ , [0])$\}$}.
% \end{example}


% \begin{example}
% The link collection given in \autoref{ex:link-collection-example} is valid (and weakly valid) for \lstinline{cst} and \lstinline{ast'}, and a retentive lens whose |get| function is \lstinline{getE} (in \autoref{fig:runningExpDataTypeDef}), because $\lstinline{cst} \models \{\,(\lstinline{Neg "a neg" _}\, , [2])\,\}$, $\lstinline{ast'} \models \{\,(\lstinline{Sub (Num 0) _} \, , [0])\,\}$, and \lstinline{getE} indeed relates a source region of pattern \lstinline{Neg "a neg" _}\, to a view region of pattern \lstinline{Sub (Num 0) _}\,.
% \end{example}

Now we have all the ingredients for the formal definition of retentive lenses.
\begin{definition}[Retentive Lenses]
\label{def:retLens}
For a set~|S| of source trees and a set~|V| of view trees, a retentive lens between |S|~and~|V| is a pair of functions
\begin{align*}
|get| &: |S| \pfun |V| \times |Links| \\
|put| &: |S| \times |V| \times |Links| \pfun S
\end{align*}
satisfying
\begin{itemize}
\item \emph{Hippocraticness:} if $|get s = (v, ls)|$, then $(|s|, |v|, |ls|) \in \dom(|put|)$ and
\begin{align}
\label{law:get}
|put(s, v, ls) = s| \text{ ;}
\end{align}

\item \emph{Correctness:} if $|put (s, v, ls) = s'|$, then $|s'| \in \dom(|get|)$ and
\begin{align}
\label{law:correct}
|get s' = (v, ls')| \quad \text{ for some |ls'| ;}
\end{align}

\item \emph{Retentiveness:}
\begin{align}
\label{law:retain}
|fst| \cdot |ls| \subseteq |fst| \cdot |ls'|
\end{align}
where $|fst| : A \sim A \times B$ is the first projection function (lifted to a relation).
\end{itemize}
\end{definition}
%\todo{put domain is left unspecified, but retentiveness can put restrictions on it}%
Modulo the handling of links, Hippocraticness and Correctness remain the same as their original forms (in the definition of well-behaved lenses).
%
Retentiveness further states that the input links $|ls|$ must be preserved, except for the location of source regions (i.e.~$\rdom(|snd| \cdot |ls|)$ in the compact relational notation). 
The region patterns (data) and the location of the view region, which are $|fst| \cdot |ls|$ in the relational notation, must be exactly the same.
Retentiveness formalises the triangular guarantee in a compact way, and we can expand it pointwise to see that it indeed specialises to the triangular guarantee.

\begin{proposition}[Triangular Guarantee]
Given a retentive lens, suppose |put(s, v, ls) = s'| and |get s' = (v, ls')|. If $((|spat|, |spath|), (|vpat|, |vpath|)) \in |ls|$, then for some |spath'| we have $|s'| \models |{(spat, spath')}|$ and $|((spat, spath'), (vpat, vpath))| \in |ls'|$.
\end{proposition}


\begin{example}
In \autoref{fig:swapAndPut}, if the \lstinline{put} function takes \lstinline{cst}, \lstinline{ast'}, and links \lstinline[mathescape]{ls${}={}$$\{$((Neg "a neg" _  , [2]) , (Sub (Num 0) _ , [0]))$\}$} as arguments and successfully produces an updated source~\lstinline{s'}, then \lstinline{get s'} will succeed.
Let \lstinline[mathescape]{(v,ls')${}={}$get s'}; we know that we can find a link in \lstinline{ls'} with the path of its source region removed: \lstinline[mathescape]{c${}={}$(Neg "a neg" _ , (Sub (Num 0) _ , [0]))${}\in{}$fst${}\cdot{}$ls'}.
So the view region referred to by \lstinline{c} is indeed the same as the one referred to by the input link, and having \lstinline{c}${}\in{}$\lstinline{fst}${}\cdot{}$\lstinline{ls'} means that the region in~\lstinline{s'} corresponding to the view region will match the pattern \lstinline{Neg "a neg" _}\,.
\end{example}

Finally, we note that retentive lenses are an extension of well-behaved lenses: every well-behaved lens between trees can be directly turned into a retentive lens (albeit in a trivial way).
\begin{example}[Well-behaved Lenses are Retentive Lenses]
Given a well-behaved lens defined by $g : |S| \rightarrow |V|$ and $p : |S| \times |V| \rightarrow |S|$, we define $|get| : |S| \pfun |V| \times |Links|$ and $|put| : |S| \times |V| \times |Links| \pfun |S|$ as follows:
\[ \begin{array}{llcl}
|get| & |s|          & = & |(g s, emptyset)| \\
|put| & |(s, v, ls)| & = & |p (s, v)| \ \text{.}
\end{array} \]
In the definition, $\dom(|put|)$ is restricted to $\big\{\, (|s|, |v|, \emptyset) \,\big\}$.
Hippocraticness and Correctness hold because the underlying $g$ and $p$ are well-behaved.
Retentiveness is also satisfied vacuously since the input link of |put| is empty.
\end{example}

\subsection{Composition of Retentive Lenses}
\label{sec:lensComp}

\begin{figure}[t]
\centering
\includegraphics[scale=0.75,trim={4cm 7.9cm 10cm 3.9cm},clip]{pics/rlensComp.pdf}
\caption{The |put| behaviour of a composite retentive lens, divided into steps \numcircledmod{1} to \numcircledmod{8}. Step \numcircledmod{8} produces consistency links for showing the triangular guarantee.}
\label{fig:rlensComp}
\end{figure}

It is standard to provide a composition operator for composing large lenses from small ones.
Here we discuss this operator for retentive lenses, which basically follows the definition of composition for well-behaved lenses, except that we need to deal with links carefully.
Below we use |lens_AB| to denote a retentive lens that synchronises trees of sets |A| and |B|, |get_AB| and |put_AB| the |get| and |put| functions of the lens, |l_ab| a link between tree |a| (of set |A|) and tree |b| (of set |B|), and |ls_ab| a collection of links between |a| and |b|.

\begin{definition}[Retentive Lens Composition]
\label{def:rlensComp}
Given two retentive lenses |lens_AB| and |lens_BC|, define the |get| and |put| functions of their composition by\\
\begin{minipage}[t]{0.4\textwidth}
\begin{center}
\begin{code}
get_AC a = (c, ls_ab linkComp ls_bc)
  where  (b, ls_ab  ) = get_AB  a
         (c, ls_bc  ) = get_BC  b
\end{code}
\end{center}
\end{minipage}
\begin{minipage}[t]{0.6\textwidth}
\begin{code}
put_AC (a , c' , ls_ac') =   a'
  where    (b, ls_ab)  =  get_AB a
           ls_bc'      =  conv ((conv ls_ac' linkComp ls_ab))
           b'          =  put_BC (b, c', ls_bc')
           ls_b'c'     =  fst (get_BC b')
           ls_ab'      =  ls_ac' linkComp (conv ls_b'c')
           a'          =  put_AB (a, b', ls_ab') {-"."-}
\end{code}
\end{minipage}
%where |get_AB| and |put_AB| (resp.~|get_BC| and |put_BC|) are the corresponding |get| and |put| functions of |lens_AB| (resp.~|lens_BC|).
%and $|ls|^{\circ}$ is the collection of links produced from |ls| by swapping each link's starting point and ending point.
% (Recall that a collection of links is regarded as a relation, so we use the converse symbol for `reversed links'.)
\end{definition}
The |get| behaviour of a composite retentive lens is straightforward; the |put| behaviour, on the other hand, is a little complex and can be best understood with the help of \autoref{fig:rlensComp}.
Let us first recap the composite behaviour of |put| of traditional lenses: in \autoref{fig:rlensComp}, if we need to propagate changes from data |c'| back to data |a| without links, we will first construct the \emph{intermediate} data |b| (by running |get_AB a|), propagate changes from |c'| to |b| and produce |b'|, and finally use |b'| to update |a|.
%
The composition of retentive lenses is similar:
besides the intermediate data |b|, we also need to construct intermediate links |ls_bc'| (\numcircledmod{3} in the figure) for retaining information when updating |b| to |b'|, so that we can further construct intermediate links |ls_ab'| (\numcircledmod{6} in the figure) for retaining information when updating |a| to |a'| using |b'|.

\begin{theorem}
\label{thm:retPrsv}
The composition of two retentive lenses is still a retentive lens.
\end{theorem}
The proof is available in the appendix (\autoref{prf:retPrsv}).


\section{A DSL for Retentive Bidirectional Tree Transformations}
\label{sec:retentiveDSL}

The definition of retentive lenses is somewhat complex, but we can ease the task of constructing retentive lenses with a declarative domain-specific language.
Our DSL is designed to describe consistency relations between algebraic data types, and from each consistency relation defined in the DSL, we can obtain a pair of |get| and |put| functions forming a retentive lens.
Below we will give an overview of the DSL and how retentive lenses are derived from programs in the DSL using the arithmetic expression example (\autoref{sec:dslByExamples}), the syntax (\autoref{sec:syntax}) and semantics (\autoref{sec:DSLSem}) of the DSL, and finally the theorem stating that the generated lenses satisfy the required laws (\autoref{thm:mainthm}).
Due to limited space, we can only provide the proof of the theorem in the appendix~(\autoref{app:proof}), but the essence is given in the last part of \autoref{sec:dslByExamples}.
Also some more programming examples other than syntax tree synchronisation can be found in the appendix (\autoref{sec:programmingExamples}).

\subsection{Overview of the DSL}
\label{sec:dslByExamples}

\begin{figure}[t]
\begin{small}
\begin{minipage}{0.45\textwidth}
\begin{centerTab}
\begin{lstlisting}
Expr <---> Arith
  Plus   _  x  y  ~  Add  x  y
  Minus  _  x  y  ~  Sub  x  y
  FromT  _  t     ~  t
\end{lstlisting}
\end{centerTab}%
\end{minipage}
\begin{minipage}[t]{0.45\textwidth}
\begin{centerTab}
\begin{lstlisting}
Term <---> Arith
  Lit    _  i  ~  Num  i
  Neg    _  r  ~  Sub  (Num 0)  r
  Paren  _  e  ~  e
\end{lstlisting}
\end{centerTab}%
\end{minipage}
\end{small}
\caption{The program in our DSL for synchronising data types defined in \autoref{fig:runningExpDataTypeDef}.}
\label{fig:dsl_example}
\end{figure}


Recall the arithmetic expression example (\autoref{fig:runningExpDataTypeDef}).
In our DSL, we define data types in \scName{Haskell} syntax and describe consistency relations between them that bear some similarity to |get| functions.
For example, the data type definitions for \lstinline{Expr} and \lstinline{Term} written in our DSL remain the same as those in \autoref{fig:runningExpDataTypeDef}, and the consistency relations between them (i.e.~\lstinline{getE} and \lstinline{getT} in \autoref{fig:runningExpDataTypeDef}) are expressed as the ones in \autoref{fig:dsl_example}.
Here we specify two consistency relations similar to \lstinline{getE} and \lstinline{getT}: one between \lstinline{Expr} and \lstinline{Arith}, and the other between \lstinline{Term} and \lstinline{Arith}.
%
Each consistency relation is further defined by a set of \emph{inductive rules}, stating that if the subtrees matched by the same variable appearing on the left-hand side (i.e.~source side) and right-hand side (i.e.~view side) are consistent, then the larger pair of trees constructed from these subtrees are also consistent.
%(For primitive types which do not have constructors such as integers and strings, we consider them consistent exactly when they are equal.)
Take
\begin{centerTab}
\begin{lstlisting}
Plus _ x y  ~  Add x y
\end{lstlisting}
\end{centerTab}%
for example:
it means that if |x_s| is consistent with |x_v|, and |y_s| is consistent with |y_v|, then \lstinline{Plus}~|a x_s y_s| and \lstinline{Add}~|x_v y_v| are consistent for any value~|a|, where |a|~corresponds to the `don't-care' wildcard in \lstinline{Plus _ x y}. So the meaning of \lstinline{Plus _ x y ~ Add x y} can be better understood as the following proof rule:
\[ \frac{|x_s ~ x_v| \quad |y_s ~ y_v|}
        {\text{\lstinline{Plus}\;|a x_s y_s ~ | \lstinline{Add}\;|x_v y_v|}} \]

Each consistency relation is translated to a pair of |get| and |put| functions defined by case analysis generated from the inductive rules.
Detail of the translation will be given in \autoref{sec:DSLSem}, but the idea behind the translation is a fairly simple one which establishes Retentiveness by construction.
For |get|, the rules themselves are already close to function definitions by pattern matching, so what we need to add is only the computation of output links.
For |put|, we use the rules backwards and define a function that turns the regions of an input view into the regions of the new source, reusing regions of the old source wherever required: when there is an input link connected to the current view region, |put| grabs the source region at the other end of the link in the old source; otherwise, |put| creates a new source region as described by the left-hand side of an appropriate rule.

For example, suppose that the |get| and |put| functions generated from the consistency relation \lstinline{Expr <---> Arith} are named \lstinline{getEA} and \lstinline{putEA} respectively.
The inductive rule \lstinline{Plus _ x y ~ Add x y} generates the definition for \lstinline{getEA s} when \lstinline{s} matches \lstinline{Plus _ x y}:
\lstinline{getEA (Plus _ x y)} computes a view recursively in the same way as \lstinline{getE} in \autoref{fig:runningExpDataTypeDef}; furthermore, it produces a new link between the top regions \lstinline{Plus} and \lstinline{Add}, and keeps the links produced by the recursive calls \lstinline{getEA x} and \lstinline{getEA y}.
In the |put| direction, the inductive rule \lstinline{Plus _ x y ~ Add x y} leads to a case \lstinline{putEA s (Add x y) ls}, under which there are two subcases:
if there is any link in \lstinline{ls} that is connected to the \lstinline{Add} region at the top of the view, \lstinline{putEA} grabs the region at the other end of the link in the old source and tries to use it as the top part of the new source;
if such a link does not exist, \lstinline{putEA} uses a \lstinline{Plus} with a default annotation as a substitute for the top part of the new source.
In either case, the subtrees of the new source at the positions marked by \lstinline{x} and \lstinline{y} are computed recursively from the view subtrees \lstinline{x} and \lstinline{y}.

While the core idea is simple, there are cases in which the translated functions do not constitute valid retentive lenses, and the crux of \autoref{thm:mainthm} is finding suitable ways of computation or reasonable conditions to circumvent all such cases (some of which are rather subtle).
The following cases should give a good idea of what is involved in the correctness of the theorem.
\begin{enumerate}[label=\Roman*.,leftmargin=2em]
\item \emph{The translated functions may not be well-defined.}
For example, in the |get| direction, an arbitrary set of rules may assign zero or more than one view to a source, making |get| partial (which, though allowed by the definition, we want to avoid) or ill-defined, and we will impose (fairly standard) restrictions on patterns to preclude such rules.
These restrictions are sufficient to guarantee that exactly one rule is applicable in the |get| direction but not in the |put| direction, in which we need to carefully choose a rule among the applicable ones or risk non-termination (e.g.~producing an infinite number of parentheses by alternating between the \lstinline{Paren} and \lstinline{FromT} rules).
\item \emph{A region grabbed by |put| from the old source may not have the right type.}
For example, if |put| is run on \lstinline{cst}, \lstinline{ast'}, and the link between them in \autoref{fig:swapAndPut}, it has to grab the source region \lstinline{Reg "a neg" _}\,, which has type \lstinline{Term}, and install it as the second argument of \lstinline{Plus}, which has to be of type \lstinline{Expr}.
In this case there is a way out since we can convert a \lstinline{Term} to an \lstinline{Expr} by wrapping the \lstinline{Term} in the \lstinline{FromT} constructor.
We will formulate conditions under which such conversions are needed and can be synthesised automatically.
\item \emph{Hippocraticness may be accidentally invalidated by |put|.}
Suppose that there is another parenthesis constructor \lstinline{Brac} that has the same type as \lstinline{Paren} and for which a similar rule \lstinline{Brac _ e ~ e} is supplied.
Given a source that starts with \lstinline{Brac "" (Paren "" ...)}, |get| will produce two links (among others) relating both the \lstinline{Brac} and \lstinline{Paren} regions with the empty region at the top of the view.
If |put| is immediately invoked on the same source, view, and links, it may choose to process the link attached to the \lstinline{Paren} region first rather than the one attached to the \lstinline{Brac} region, so that the new source starts with \lstinline{Paren "" (Brac "" ...)}, invalidating Hippocraticness.
Therefore |put| has to carefully process the links in the right order for Hippocraticness to hold.
\item \emph{Retentiveness may be invalidated if |put| does not correctly reject invalid input links.}
Unlike |get|, which can easily be made total, |put| is inherently partial since input links may well be invalid and make Retentiveness impossible to hold.
For example, if there is an input link relating a \lstinline{Neg} region and an \lstinline{Add} region, then it is impossible for |put| to produce a result that satisfies Retentiveness since |get| does not produce a link of this form.
Instead, |put| must correctly reject invalid links for Retentiveness to hold.
Apart from checking that input links have the right forms as specified by the rules, there are more subtle cases where the view regions referred to by a set of input links are overlapping---for example, in a view starting with \lstinline{Sub (Num 0) ...} there can be links referring to both the \lstinline{Sub _ _} region and the \lstinline{Sub (Num 0) _} region at the top.
Our |get| cannot produce overlapping view regions, and therefore such input links must be detected and rejected as well.
\end{enumerate}

In the rest of this section we will describe the DSL in more detail.

%With retentive lenses \lstinline{getEA} and \lstinline{putEA}\footnote{There are also \lstinline{getTA} and \lstinline{putTA} generated from the consistency relation \lstinline{Term <---> Arith}, which are mutually recursive with \lstinline{getEA} and \lstinline{putEA} respectively.} generated from \autoref{fig:dsl_example}, we can synchronise the (old) \lstinline{cst} and (modified) \lstinline{ast'} in \autoref{fig:swapAndPut} in different ways by running \lstinline{putEA} with different input links. For instance, supposing that the input links \lstinline{ls'} are obtained by swapping the two subtrees of \lstinline{Add} (using the edit operation |swap| to be introduced in \autoref{sec:editOperations}), \lstinline{putEA cst ast ls'} will produce exactly \lstinline{cst'} in \autoref{fig:swapAndPut}.

% we update \lstinline{ast} (and consistency links \lstinline{ls} between \lstinline{cst} and \lstinline{ast}) by swapping the two subtrees of \lstinline{Add} using the edit operation |swap| introduced in \autoref{sec:editOperations}, we get the updated \lstinline{ast'} and \lstinline{ls'}
% if the user thinks that the two subtrees of \lstinline{Add} are swapped and passes \lstinline{putEA} links that connect not only the roots of the subtrees of \lstinline{Plus} and \lstinline{Add} but also all their inner subtrees, the desired result should be \lstinline[mathescape]!cst$_\texttt{2}$!, which represents `\lstinline{-2 + (0 - 1)}' and retains all annotations, in effect swapping the two subtrees under \lstinline{Plus} and adding two constructors \lstinline{FromT} and \lstinline{Paren} to satisfy the type constraint.

% \begin{figure}
% \centering
% \includegraphics[scale=0.8,trim={5.8cm 7cm 4cm 3cm},clip]{pics/putWithLinks.pdf}
% \caption[\lstinline{cst} is updated in many ways.]{\lstinline{cst} is updated in many ways. \footnotesize{(Red dashed lines are some of the input links for \lstinline[mathescape]!cst$_\texttt{2}$!.)}}
% \label{fig:running_example}
% \end{figure}

\subsection{Syntax}
\label{sec:syntax}

\begin{figure}[t]
\centerline{
\framebox[10cm]{\vbox{\hsize=14cm
\[
\begin{array}{llllllll}
\colorbox{light-gray}{\textsf{Program}} \\
\bb
  |Prog| &\Coloneqq& |TypeDef|^* \ |RelDef|^+ \\
\ee \\[0.5em]
\colorbox{light-gray}{\textsf{Type Definition}} \\
\bb
  |TypeDef|  &\Coloneqq& \texttt{data} \ |Type|\ \texttt{=}\ |Con| \ |Type|^* \ \{ \texttt{||}\ |Con|\ |Type|^* \}^* \\
\ee \\[0.5em]
\colorbox{light-gray}{\textsf{Consistency Relation Definition}} \\
\bb
  |RelDef| &\Coloneqq& |Type|_s \longleftrightarrow |Type|_v\ |Rule|^+
\ee \\[0.5em]
%
\colorbox{light-gray}{\textsf{Inductive Rule}}\\
\bb
   |Rule|  &\Coloneqq& |Pat|_s \ \texttt{\textasciitilde}\ |Pat|_v \\
\ee \\[0.5em]
%
\colorbox{light-gray}{\textsf{Pattern}}\\
\bb
  |Pat| &\Coloneqq& \texttt{\_}  \quad||\quad  |Var|  \quad||\quad  |Con|\; |Pat|  \\
\ee
\end{array}
\]
}}}
\caption[Syntax of the DSL.]{Syntax of the DSL.}
\label{fig:dsl_syntax}
\end{figure}

The syntax of our DSL is summarised in \autoref{fig:dsl_syntax}, where nonterminals are in \textit{italic}; terminals are typeset in \texttt{typewriter} font; $\{ \}$ is for grouping; $^?$, $^*$, and~$^+$ represent zero-or-one occurrence, zero-or-more occurrence, and one-or-more occurrence respectively, and |Type|, |Con|, and |Var| are syntactic categories (whose definitions are omitted) for the names of types, constructors, and variables respectively.
We sometimes additionally attach a subscript $s$ or $v$ to a symbol to mean that the symbol is related to sources or views.
A program consists of two parts: data types definitions and consistency relations between these data types.
We adopt the \scName{Haskell} syntax for data type definitions---a data type is defined by specifying a set of data constructors and their argument types.
%(The definition of |Type|, |Con|, and |Var| are omitted.)
% a type synonym can be defined by giving a new name to existing types.
As for the definitions of consistency relations, each of them starts with |Type_s <---> Type_v|, declaring the source and view types for the relation.
The body of each consistency relation is a list of inductive rules, each of which defined by a pair of source and view patterns |Pat_s ~ Pat_v|, where a pattern can include wildcards, variables, and constructors.
%Finally, two semicolons `;;' are added to finish the definition of a consistency relation. (In this paper, we always omit `;;' when there is no ambiguity.)

\subsubsection{Syntactic Restrictions}
\label{sec:synres}

We impose some syntactic restrictions to guarantee that programs in our DSL indeed give rise to retentive lenses (\autoref{thm:mainthm}).
%Our restrictions on patterns are quite natural, while those on algebraic data types are a little subtle, and the reader may want to skip the latter at the first reading.

On \emph{patterns}, we require (i)~pattern coverage:
for any consistency relation $|S <---> V| = \myset{p_i \sim q_i \mid 1 \leq i \leq n}$ defined in a program, $\myset{p_i}$ should cover all possible cases of type |S|, and  $\myset{q_i}$ should cover all cases of type |V|.
We also require (ii)~source pattern disjointness:
any distinct $p_i$ and $p_j$ should not be matched by the same tree.
Finally, (iii)~a bare variable pattern is not allowed on the source side (e.g.~|x ~ D x|), and (iv) wildcards are not allowed on the view side (e.g.~|C x ~ D _ x|), and (v)~the source side and the view side must use exactly the same set of variables.
These conditions ensure that |get| is total and well-defined (ruling out Case~I in \autoref{sec:dslByExamples}).%

To state the next requirement we need a definition: two data types |S_1| and |S_2| defined in a program are \emph{interchangeable} in data type |S| exactly when (i) there are some data type |V'| and |V| for which consistency relations |S_1 <---> V'|, |S_2 <---> V'| and |S <---> V| are defined in the program, and (ii) |S| may have subterms of type |S_1| and |S_2|, and |V| may have subterms of type |V'|.
%
%The following figure depicts some of the cases,
%\begin{center}
%\includegraphics[scale=0.6,trim={5.5cm 9.2cm 4cm 7.8cm},clip]{pics/interConvertible.pdf}
%\end{center}
%where vertical dotted lines (in black) mean that there are zero or more nodes in between the connected nodes and horizontal dotted lines (in blue) indicate that both |S_1| and |S_2| can be consistent with |V_12| (that is, there are consistency relations |S_1 <---> V_12| and |S_2 <---> V_12|).
%
%  \todo{I hid the picture that was here because I thought it was not very informative and it was drawn so differently from other figures, making it possibly hard to understand. I think the example of \lstinline{Expr <---> Arith} is enough for explanation. But if you think having that picture is better, let's draw it better.}
%
If |S_1| and |S_2| are interchangeable, then Case~II (\autoref{sec:dslByExamples}) may happen:
when doing |put| on |S| and |V| there might be input links dictating that values of type~|S_2| should be retained in a context where values of type~|S_1| are expected, or vice versa.
When this happens, we need two-way conversions between |S_1|~and~|S_2|.
%
% Let us consider a particular case where |S_1| and |S_2| are mutually recursively defined and mapped to the same view type.
%    For instance, \lstinline{Term} and \lstinline{Expr} are interchangeable in \lstinline{Expr}, because the two consistency relations \lstinline{Expr <---> Arith} and \lstinline{Term <---> Arith} in \autoref{fig:dsl_example} share the same view type \lstinline{Arith}, and both \lstinline{Expr} and \lstinline{Term} can be subterms of \lstinline{Expr} (Here in the example $|S_1| = \text{\lstinline{Expr}}$, and $|S_2| = \text{\lstinline{Expr}}$, and $V = |V'| = \text{\lstinline{Arith}}$).
%    Then we may need means to convert between \lstinline{Expr} and \lstinline{Term} when doing \lstinline{put}.
%    As in \lstinline{cst'} in \autoref{fig:swapAndPut}, the second subtree of \lstinline{Plus} have to be of type \lstinline{Expr}, but is created by a link connected to the old source's subtree \lstinline{Neg ...} of type \lstinline{Term}, so we need to wrap \lstinline{Neg ...} into \lstinline{FromT "" (Neg ...)} to make the types match.

We choose a simple way to ensure the existence of conversions:%
for any interchangeable types |S_1| and |S_2| with |S_1 <---> V'| and |S_2 <---> V'| defined, we require that there exists a sequence of data types in the program
\[ S_1 = T_1,\ T_2,\ \cdots,\ T_{n-1},\ T_n = S_2\]
with $n >= 2$ such that for any $1 \leq i < n$, consistency relation |T_i <---> V'| is defined and has a rule |Pat_i ~ x| whose source pattern |Pat_i| contains exactly one variable, and its type in |Pat_i| is $T_{i+1}$ (we also require such a sequence with the roles of |S_1| and |S_2| switched).
With rule |Pat_i ~ x|, we immediately get a function $t_i : T_{i+1} \rightarrow T_{i}$ contructing a |T_i| from a term |v| of $|T|_{i+1}$ by substituting |v| for |x| in |Pat_i| (and filling wildcard positions with default values).
Then we have the needed conversion function:
\begin{equation}\label{equ:inj}
|inj|_{{|S_2|} \rightarrow {|S_1|}@@|V'|} = t_{n-1} \circ \cdots \circ t_2 \circ t_1 
\end{equation}
(and similary $|inj|_{{|S_1|} \rightarrow {|S_2|}@@|V'|}$).
For example, \lstinline{FromT _ t ~ t} gives rise to a function 
\[\text{\lstinline{inj}}_{\text{\lstinline{Term}} \rightarrow \text{\lstinline{Expr}}@@\text{\lstinline{Arith}}}\ x \;=\; \text{\lstinline{FromT ""}}\; x\]
and it can be used to convert \lstinline{Term} to \lstinline{Expr} whenever needed when doing |put| with view type \lstinline{Arith}.

\subsection{Semantics}
\label{sec:DSLSem}

We give the semantics of our DSL in terms of a translation into `pseudo-\scName{Haskell}', where we may replace chunks of \scName{Haskell} code with natural language descriptions to improve readability.
As in \autoref{sec:retentive-lens-definition}, let |Tree| be the set of values of any algebraic data type, and |Pattern| the set of all patterns.
For a pattern $|p| \in |Pattern|$, |Vars p| denotes the set of variables in $p$.
For each $|v| \in |Vars p|$, |TypeOf (p,v)| is (the set of all values of) the type of $|v|$ in pattern |p|, and |path (p, v)| is the path of variable $|v|$ in pattern $|p|$.
We use the following functions (two of which are dependently typed) to manipulate patterns:
\begin{align*}
|isMatch| &: \phantom{(|p| \in {}} |Pattern| \phantom{)} \times |Tree| \rightarrow |Bool| \\
|decompose| &: (|p| \in |Pattern|) \times |Tree| \pfun \big(|Vars p| \rightarrow |Tree|\big) \\
|reconstruct| &: (|p| \in |Pattern|) \times \big(|Vars p| \rightarrow |Tree|\big) \pfun |Tree| \\
|fillWildcards| &: \phantom{(|p| \in {}} |Pattern| \phantom{)} \times |Tree| \pfun |Pattern| \\
|fillWildcardsWD| &: \phantom{(|p| \in {}} |Pattern| \phantom{)} \rightarrow |Pattern| \\
|eraseVars| &: \phantom{(|p| \in {}} |Pattern| \phantom{)} \rightarrow |Pattern| ~\text.
\end{align*}
Given a pattern~|p| and a tree~|t|, |isMatch (p, t)| tests whether |t|~matches~|p|.
If the match succeeds, |decompose (p, t)| returns a function mapping every variable in |p| to its corresponding matched subtree of~|t|.
Conversely, |reconstruct (p, f)| produces a tree matching~|p| by replacing every occurrence of $|v| \in |Vars p|$ in |p| with |f v|, provided that |p|~does not contain any wildcard.
To remove wildcards, we can use |fillWildcards (p, t)| to replace all the wildcards in~|p| with the corresponding subtrees of~|t| (coerced into patterns) when |t|~matches~|p|, or use |fillWildcardsWD| to replace all the wildcards with the default values of their types.
Finally, |eraseVars p| replaces all the variables in~|p| with wildcards.
The definitions of these functions are straightforward and omitted here.%
%\footnote{Non-linear patterns are allowed: multiple occurrences of the same variable in a pattern must have the same type and they must capture the same value.}


\subsubsection{Get Semantics}
\label{sec:getsem}
For a consistency relation $|S| \leftrightarrow |V|$ defined in our DSL with a set of inductive rules $R = \{\, |spat_k| \sim |vpat_k| \mid 1 \leq |k| \leq n\,\}$, its corresponding $|get|_{|SV|}$ function has the following type:
\begin{align*}
|get|_{|SV|} : |S| \rightarrow  |V| \times |Links|
\end{align*}
The idea of computing |get s| is to use a rule $|spat_k| \sim |vpat|_k \in R$ such that~$s$ matches |spat_k|---the restrictions on patterns imply that such a rule uniquely exists for all $s$---to generate the top portion of the view with |vpat_k|, and then recursively generate subtrees for all variables in |spat_k|.
The |get| function also creates links in the recursive procedure: when a rule $|spat_k| \sim |vpat|_k \in R$ is used, it creates a link relating the matched parts/regions in the source and view, and extends the paths in the recursively computed links between the subtrees.
In all, the |get| function defined by $R$ is:
\begin{align*}
&|get|_|SV|~s = (|reconstruct|\;(|vpat_k|, |fst| \circ |vls|),\; l_{\mathit{root}} \cup |links|) \numberthis \label{equ:get} \\
&\mywhere \text{find } k \text{ such that } |spat_k| \sim |vpat|_k \in R \text{ and } |isMatch|(|spat_k|, |s|) \\
&\myspace |vls| = (|get| \circ |decompose (spat_k, s)|) \in |Vars spat_k| \rightarrow |V| \times |Links| \\
&\myspace |spat'| = |eraseVars (fillWildcards (spat_k, s))| \\
&\myspace l_{\mathit{root}} = \myset{((|spat'| , |[]|) , (|eraseVars vpat_k|, [\,])) } \\
&\myspace |links| = \big\{\, ((|spat|, |path (spat_k, v) ++ spath|), (|vpat|, |path (vpat_k, v) ++ vpath|)) \\
&\myspace \myindentS\myindentS \mid |v| \in |Vars vpat_k|, |((spat,spath) , (vpat,vpath))| \in |snd (vls v)| \,\big\} \ \text{.}
\end{align*}
The auxiliary function $|path| : (|p| \in |Pattern|) \times |Vars p| \rightarrow |Path|$ returns the path from the root of a pattern to one of its variables, and |(++)| is path concatenation.
While the recursive call is written as $|get| \circ |decompose (spat_k, s)|$ in the definition above, to be precise, |get| should have different subscripts |TypeOf (spat_k, v)| and |TypeOf (vpat_k, v)| for different $|v| \in |Vars spat_k|$.

\subsubsection{Put Semantics}\label{sec:putsem}
For a consistency relation $|S| \leftrightarrow |V|$ defined in our DSL as $R = \{\, |spat_k| \sim |vpat_k| \mid 1 \leq |k| \leq n\,\}$, its corresponding $|put|_{|SV|}$ function has the following type:
\[|put|_{|SV|} : |Tree| \times |V| \times |Links| \pfun |S| \ \text{.} \]
The source argument of |put| is given the generic type |Tree| since the type of the old source may be different from the type of the result that |put| is supposed to produce.
Given arguments $(|s|, |v|, |ls|)$, |put| is defined by two cases depending on whether the root of the view is within a region referred to by the input links, i.e.~whether there is some $(|_|\, , (|_|\, , |[]|)) \in |ls|$.

\begin{itemize}
\item {
  In the first case where the root of the view is not within any region of the input links, |put| selects a rule $|spat_k| \sim |vpat_k| \in R$ whose |vpat_k| matches |v|---our restriction on view patterns implies that at least one such rule exists for all |v|---and uses |spat_k| to build the top portion of the new source:
  wildcards in |spat_k| are filled with default values and variables in |spat_k| are filled with trees recursively constructed from their corresponding parts of the view.
  \begin{align*}
  &|put|_{|SV|}~|(s, v, ls)| = |reconstruct (spat_k', ss)| \numberthis \label{equ:put1}\\
  &\mywhere \text{find } k \text{ such that } |spat_k| \sim |vpat_k| \in R \text{ and } |isMatch (vpat_k, v)| \\
    &\myspace \phantom{\text{find } k \text{ such }} \text{ and } k \text{ satisfies the extra condition below} \\
  &\myspace |vs| = |decompose (vpat_k, v)|\\
  &\myspace |ss| = \lambda\,(t \in |Vars spat_k|) \rightarrow \\
  &\myspace\myindentS |put|(|s|, |vs t|, |divide (path (vpat_k, t))|, |ls|) \numberthis \label{equ:rec1} \\
  &\myspace |spat|'_k = |fillWildcardsWD spat_k| \\
  &|divide (prefix, ls)| = \myset{(|r|_|s|, |(vpat, vpath)|) \mid (|r|_|s|, (|vpat|, |prefix ++ vpath|) \in |ls|)}\numberthis \label{equ:divide} 
  \end{align*}
  The omitted subscripts of |put| in (\ref{equ:rec1}) are |TypeOf (spat_k, t)| and |TypeOf (vpat_k, t)|.
  Additionally, if there is more than one rule whose view pattern matches |v|, the first rule whose view pattern is \emph{not} a bare variable pattern is preferred for avoiding infinite recursive calls: if |vpat_k = x|, the size of the input of the recursive call in~(\ref{equ:rec1}) does not decrease because |vs t = v| and |path (t, vpat_k) = []|.
  %
    For example, when the view patterns of both \lstinline{Plus _ x y ~ Add x y} and \lstinline{FromT _ t ~ t} match a view tree, the former is preferred.
    This helps to avoid non-termination of |put| as mentioned in Case~I in \autoref{sec:dslByExamples}.
}
\item {
  In the case where the root of the view is an endpoint of some link, |put| uses the source region (pattern) of the link as the top portion of the new source.%
  \begin{align*}
  &|put|_{|SV|}~|(s, v, ls)| = |inj|_{|TypeOf spat_k| \rightarrow |S|@@V} |(reconstruct|(|spat_k'|,|ss|))  \numberthis \label{equ:put2}\\
  &\mywhere l = ((|spat|, |spath|) , (|vpat|, |vpath|)) \in |ls| \\
  &\myspace\myindentSS \text{ such that } |vpath| = |[]|,\,|spath|\text{ is the shortest } \\
  &\myspace \text{find } k \text{ such that } |spat_k| \sim |vpat_k| \in R \text{\ \ and\ \ |spat|} \\
  &\myspace\myindentSS \text{is |eraseVars (fillWildcards (spat_k, t))| for some } t\\
  &\myspace |spat_k'| =  |fillWildcards (spat_k, spat)| \\
  &\myspace |vs| = |decompose (vpat_k, v)|\\
  &\myspace |ss| = \lambda\,(t \in |Vars spat_k|) \rightarrow \\
  &\myspace\myindentS |put|(|s|, |vs t|, |divide (path (vpat_k, t))|, |ls| \setminus \myset{l})) \numberthis \label{equ:rec2}
  \end{align*}
  When there is more than one source region linked to the root of the view, to avoid Case~III in \autoref{sec:dslByExamples}, |put| chooses the source region whose path is the shortest, which ensures that the preserved region patterns in the new source will have the same relative positions as those in the old source, as the following figure shows.
  \begin{center}
  \includegraphics[scale=0.65,trim={6cm 8.4cm 4.5cm 8cm},clip]{pics/multiLinks.pdf}
  \end{center}
    Since the linked source region (pattern) does not necessarily have type |S|, we need to use the function $|inj|_{|TypeOf spat_k| \rightarrow |S|}@@V$ (Equation \ref{equ:inj}) to convert it to type |S|; this function is available due to our requirement on interchangeable data types (see \hyperref[sec:synres]{Syntax Restrictions} in \autoref{sec:syntax}).
}

\end{itemize}

\subsubsection{Domain of |put|}
To avoid Case~IV in \autoref{sec:dslByExamples}, in the actual implementation of |put| there are runtime checks for detecting invalid input links, but these checks are omitted in the above definition of |put| for clarity.
We extract these checks into a separate function |check| below, which also serves as a decision procedure for the domain of |put|.
\begin{align*}
&|check| : |Tree| \times |V| \times |Links| \rightarrow |Bool| \\
&|check (s, v, ls)| =
  \begin{cases}
  |chkWithLink (s, v, ls)|  \quad & \text{if some } |((_, _),(_, []))| \in |ls|\\
  |chkNoLink   (s, v, ls)| & \text{otherwise}
  \end{cases}
\end{align*}
|chkNoLink| corresponds to the first case of |put| (\ref{equ:put1}).
\begin{align*}
&|chkNoLink (s, v, ls)| = |cond|_1 \wedge |cond|_2 \wedge |cond|_3 \\
&\mywhere \text{find } k \text{ such that } |spat_k| \sim |vpat_k| \in R \text{ and } |isMatch (vpat_k, v)| \\
  &\myspace\myindentS \text{ and } k \text{ satisfies the same condition as in (\ref{equ:put1})} \\
&\myspace |vs| = |decompose (vpat_k, v)|\\
&\myspace |vp t| = |path (vpat_k, t)| \\
&\myspace |cond|_1 = |ls ==| \left(\bigcup_{t \in |Vars spat_k|} |addVPrefix (vp t, divide (vp t, ls))|\right) \\
&\myspace |cond|_2 = \bigwedge_{t \in |Vars spat_k|} |check (s, vs t, divide (vp t, ls))| \\
&\myspace |cond|_3 = \text{\textbf{if} } |vpat|_k \text{ is some bare variable pattern `$x$'} \text{ \textbf{then} } \\
&\myspace\qquad\qquad\ |TypeOf (spat_k, x) <---> V|  \text{ has a rule } |spat_j| \sim |vpat_j| \text{ such that}\\
&\myspace\qquad\qquad\ \myindentSS |isMatch (vpat_j, v)| \text{ and } |vpat_j| \text{ is not a bare variable pattern} \\
&\myspace |addVPrefix (prefix, rs)| = \myset{|((a, b), (c, prefix ++ d))| \mid |((a,b), (c,d))| \in |rs|}
\end{align*}
The |divide| function is defined as in \autoref{equ:divide}.
Condition |cond_1| checks that every link in |ls| is processed in one of the recursive calls, i.e.\ the path of every view region of |ls| starts with |path (vpat_k, t)| for some |t|.
(Specifically, if |Vars spat_k| is empty, |ls| in |cond_1| should also be empty meaning that all the links have already been processed.)
|cond_2| summarises the results of |check| for recursive calls.
|cond_3| guarantees the termination of recursion: When |vpat_k| is a bare variable pattern, the recursive call in \autoref{equ:rec1} does not decrease the size of any of its arguments;
|cond_3| makes sure that such non-decreasing recursion will not happen in the next round\footnote{For presentation purposes we only check two rounds here, but in general we should check |N + 1| rounds where |N| is the number data types defined in the program.} for avoiding infinite recursive calls.

For |chkWithLink|, as in the corresponding case of |put| (\autoref{equ:put2}), let $|l = ((spat, spath),| $
$|(vpat, vpath))| \in |ls|$ such that |vpath = []| and |spath| is the shortest when there is more than one such link.
\phantomsection\label{def:chkWithLink}
\begin{align*}
&|chkWithLink (s, v, ls)| = |cond|_1 \wedge |cond|_2 \wedge |cond|_3 \wedge |cond|_4 \\
& \mywhere \\
& \myindentSS |cond|_1 = |isMatch (spat, sel (s, spath))| \wedge |isMatch (vpat, sel (v, vpath))| \\
& \myindentSS |cond|_2 = \exists ! (|spat_k|, |vpat|_k) \in |R|.\ |vpat| = |eraseVars vpat_k| \\
& \myindent \wedge |spat| \text{ is |eraseVars (fillWildcards (spat_k, t))| for some } t\\
& \myindentSS |cond|_3 = |ls| |==| (\myset{l} \cup \bigcup_{t \in |Vars spat_k|} |addVPrefix (path (vpat_k, t),|\\
& \myindentSS\myindent\myindent|divide (path (vpat_k, t), ls {-"\setminus \myset{l}"-}))|) \\
& \myindentSS |cond|_4 = \bigwedge_{t \in |Vars spat_k|}|check|~(s, |vs t|, |divide|~|(path (vpat_k, t))|, |ls| \setminus \myset{l}))
\end{align*}
|cond_1| makes sure that the link |l| is valid (\autoref{def:validLinks}) and |cond_2| further checks that it can be generated from some rule of the consistency relations.
|cond_3| and |cond_4| are for recursive calls: the latter summarises the results for the subtrees and the former guarantees that no link will be missed.
It is |cond_3| that rejects the subtle case of overlapping view regions as described at the end of Case~IV in \autoref{sec:dslByExamples}.%
%\todo{Explain more and refer to the subtle (sub)cases in Case~IV in \autoref{sec:dslByExamples}}

\subsubsection{Main Theorem}
We can now state our main theorem in terms of the definitions of |get| and |put| above.

\begin{theorem}
\label{thm:mainthm}
Let |put'| be |put| with its domain intersected with $|S| \times |V| \times |Links|$. Then |get| and |put'| form a retentive lens as in \autoref{def:retLens}.
\end{theorem}
The proof goes by induction on the size of the arguments to |put| or |get| and can be found in the appendix (\autoref{app:proof}).



\section{Edit Operations and Link Maintenance}
\label{sec:editOperations}
Our |get| function only produces horizontal links between a source and its consistent view, while the input links to a |put| function are the ones between a source and a modified view.
To bridge the gap, in this section, we demonstrate how to update the view while maintaining the links using a set of typical \emph{edit operations} (on views).
% (i) we propose \emph{vertical links} that represent (vertical) correspondences between the original and modified versions of a view and show how to compose vertical links with the horizontal ones to produce the set of input links;
% (ii) we demonstrate how to produce vertical links using a set of typical \emph{edit operations} (on views).
These edit operations will be used in the three case studies in the next section.
% we provide a way to obtain those input links.

% In \autoref{sec:triangularDiagram}, we have discussed vertical correspondence a little and stated the reason for removing it from our framework.
% To demonstrate the feasibility of producing vertical correspondence and composing it with horizontal links to obtain (diagonal) input links, in this subsection, we define vertical correspondence as `vertical links'; a vertical link consists of two paths sharing the same region pattern, i.e.~|(vpath, vpat, vpath')|---meaning that a data fragment |(vpat, vpath)| in a view is not destroyed but probably moved to |vpath'| in the updated view.
% When composing horizontal links with vertical links, we first transform each vertical link |(vpath, vpat, vpath')| to an equivalent representation |((vpat, vpath), (vpat, vpath'))| and then reuse the link composition introduced in \autoref{sec:lensComp}.

% To obtain |put|'s input links,
% A vertical link |(vpath, vpat, vpath')| consists of two paths and a region pattern, meaning that a region |(vpat, vpath)| in a view is not destroyed but preserved at |vpath'| in the updated view.
% When composing horizontal links with vertical links, we first transform each vertical link |(vpath, vpat, vpath')| to an equivalent representation |((vpat, vpath), (vpat, vpath'))| and then reuse the link composition introduced in \autoref{sec:lensComp}.


\begin{figure}[t]
\centering
\includegraphics[scale=0.75,trim={7cm 10.17cm 7cm 2cm},clip]{pics/editOperations.pdf}
\caption{How edit operations |replace|, |copy|, |move|, and |swap| main links.}
\label{fig:editOperations}
\end{figure}


We define four edit operations, |replace|, |copy|, |move|, and |swap|, of which |move| and |swap| are defined in terms of |copy| and |replace|.
The edit operations accept not only an AST but also a set of links, which is updated along with the AST.
%We choose to maintain horizontal links and use vertical links only as an intermediate representation in order to simplify the user interface and `opt for triangular diagrams'.
% \footnote{We let edit operations accept and return horizontal links and use vertical ones only as an intermediate representation, for the simplicity of the user interface and `opting for triangular diagrams'.}
The interface has been designed in a way that the last argument of an edit operation is the pair of the AST and links, so that the user can use \scName{Haskell}'s ordinary function composition to compose a sequence of edits (partially applied to all the other arguments).
The implementation of the four edit operations takes less than 40 lines of \scName{Haskell} code, as our DSL already generates useful auxiliary functions such as fetching a subtree according to a path in some tree.

We briefly explain how the edit operations update links, as illustrated in \autoref{fig:editOperations}:
Replacing a subtree at path |p| will destroy all the links previously connecting to path |p|.
Copying a subtree from path |p| to path |p'| will duplicate the set of links previously connecting to |p| and redirect the duplicated links to connect to |p'|.
Moving a subtree from |p| to |p'| will destroy links connecting to |p'| and redirect the links (previously) connecting to |p| to connect to |p'|.
Swapping subtrees at |p| and |p'| will also swap the links connecting to |p| and |p'|.

% In the next subsection, we will explain that our edit operations are sufficient to write most of the code refactoring operations, and we demonstrate how to use them to write a particular code refactoring called \emph{push-down}.

\section{Case Studies}
\label{sec:application}
We demonstrate how our DSL works for the problems of code refactoring~\cite{Fowler1999Refactoring}, resugaring~\cite{Pombrio2014Resugaring, Pombrio2015Hygienic}, and XML synchronisation~\cite{Pacheco2014BiFluX}, all of which require that we constantly make modifications to ASTs and synchronise them with CSTs.
For all these problems, retentive lenses provide a systematic way for the user to preserve information of interest in the original CST after synchronisation.
The source code for these case studies can be found on the first author's web page: \url{http://www.prg.nii.ac.jp/members/zhu/}.

\subsection{Refactoring}
\label{sec:refactoring}

As we will report below, we have programmed the consistency relations between CSTs and ASTs for a small subset of Java~8~\cite{Gosling2014Java} and tested the generated retentive lens on a particular refactoring.
Even though the case study is small, we believe that our framework is general enough:
We have surveyed the standard set of refactoring operations for Java~8 provided by Eclipse Oxygen (with Java Development Tools) and found that all the 23 refactoring operations can be represented as the combinations of our edit operations defined in \autoref{sec:editOperations}.
A summary can be found in the appendix (\autoref{sec:refactOptAsEditSeq}).


% \subsubsection{Feasibility of Retentiveness for Code Refactoring}

%To see whether Retentiveness helps to retain comments and syntactic sugar for real-world code refactoring,
% For instance, \emph{extract method} that creates a method containing the selected statements and replace those selected statements (in the old place) with a reference to the created method~\cite{EclipseDocumentation}, is the combination of insertion (as for the empty method definition), movement (as for the selected statements), and replacement (as for the reference to the created method).

%Since the implementation of the whole code refactoring tool for Java~8 using retentive lenses requires much engineering work, 
% The paper focuses on the theoretical foundation and language design.
% and there are further research problems not solved.

% In this subsection, we discuss the feasibility of Retentiveness for code refactoring. We introduce the \emph{push-down} code refactoring of Java~8 and use it as the running example throughout the whole subsection: We show how to use our DSL to \hyperref[sec:java8SyncImpl]{implement the transformations} between CSTs and ASTs for a small subset of Java~8, and demonstrate how to use the generated (retentive) lenses to \hyperref[sec:refactorSysRun]{perform code refactoring} on \emph{clean} ASTs for the specific refactoring scenario while retaining comments and syntactic sugar in the updated program text on demand.


\begin{figure}[t]
\centering
\includegraphics[scale=0.6,trim={4cm 4cm 4cm 3cm},clip]{pics/pushDownRefactoring.pdf}
\caption[An example of the \emph{push-down} code refactoring.]{An example of the \emph{push-down} code refactoring. \footnotesize{(Subclasses \lstinline{Bus} and \lstinline{Bicycle} are omitted due to space limitation.)}}
\label{fig:refactoringTextAndAst}
\end{figure}


% \begin{figure}[t]
% \centering
% \includegraphics[scale=0.7,trim={8cm 11.7cm 8cm 3.1cm},clip]{pics/possibleRefactoringResults.pdf}
% \caption[An unsatisfactory result after refactoring]{An unsatisfactory result after refactoring, losing the comment and \emph{while} syntactic sugar.}
% \label{fig:refactoringResults}
% \end{figure}

\subsubsection{The \emph{Push-Down} Code Refactoring}
An example of the \emph{push-down} code refactoring is illustrated in \autoref{fig:refactoringTextAndAst}.
At first, the user designed a \lstinline{Vehicle} class and thought that it should possess a \lstinline{fuel} method for all the vehicles.
The \lstinline{fuel} method has a JavaDoc-style comment and contains a \lstinline{while} loop, which can be seen as syntactic sugar and is converted to a standard \lstinline{for} loop during parsing.
However, when later designing \lstinline{Vehicle}'s subclasses, the user realises that bicycles cannot be fuelled and decides to do the push-down code refactoring, which removes the \lstinline{fuel} method from \lstinline{Vehicle} and pushes the method definition down to subclasses \lstinline{Bus} and \lstinline{Car} but not \lstinline{Bicycle}.
Instead of directly modifying the (program) \lstinline{text}, most refactoring tools choose to parse the program text into its \lstinline{ast}, perform code refactoring on the \lstinline{ast}, and regenerate new (program) \lstinline{text'}.
The bottom-left corner of \autoref{fig:refactoringTextAndAst} shows the desired (program) \lstinline{text'} after refactoring, where we see that the comment associated with \lstinline{fuel} is also pushed down, and the \lstinline{while} sugar is kept.
However, the preservation of the comment and syntactic sugar does not come for free actually, as the \lstinline{ast}---being a concise and compact representation of the program \lstinline{text}---includes neither comments nor the form of the original \lstinline{while} loop.
So if the user implements the |parse| and |print| functions as back-and-forth conversions between CSTs ASTs (or even as a well-behaved lens), they may produce unsatisfactory results in which the comment and the \lstinline{while} syntactic sugar are lost.
% like the one shown in \autoref{fig:refactoringResults}.

\subsubsection{Implementation in Our DSL}
\label{sec:java8SyncImpl}

Following the grammar of Java~8, we define data types for a simplified version of its concrete syntax, which consists of definitions of classes, methods, and variables; arithmetic expressions (including assignment and method invocation); and conditional and loop statements.
For convenience, we also restrict the occurrence of statements and expressions to exactly once in some cases (such as variable declarations).
Then we define the corresponding simplified version of the abstract syntax that follows the one defined by the JDT parser~\cite{OpenJDK}.
This subset of Java~8 has around 80 CST constructs (production rules) and 30 AST constructs; the 70 consistency relations among them generate about 3000 lines of code for the retentive lenses and auxiliary functions (such as the ones for conversions between interchangeable data types and edit operations).

% Now, we define the consistency relations.
%Since the overall structure of the consistency relations is similar to \autoref{fig:dsl_example}, below we only highlight two of them as examples; the reader can refer to the supplementary material for the complete program.
%For the concrete syntax, everything is a class declaration (\lstinline{ClassDecl}), while for the abstract syntax everything is a tree (\lstinline{JCTree}). As a \lstinline{ClassDecl} should correspond to a \lstinline{JCClassDec}, which by definition is yet not a \lstinline{JCTree}, we use the constructors \lstinline{FromJCStmt} and \lstinline{FromJCClassDec} to \draft{inject it into} a \lstinline{JCTree}, emulating the inheritance in Java.%
%\todo{Obscure}\
%This is described by the consistency relation\footnote{We include keywords such as \lstinline{"class"} and \lstinline{"extends"} in the CST patterns to improve readability (even though they are not necessary).}%
%\todo{Just state what it says for the reader}
%\begin{centerTab}
%\begin{lstlisting}
%ClassDecl <---> JCTree
%  NormalClassDeclaration0 _ "class" n "extends" sup body ~
%  FromJCStmt (FromJCClassDec (JCClassDec N n (J (JCIdent sup)) body))
%
%  NormalClassDeclaration1 _ mdf "class" n "extends" sup body ~
%  FromJCStmt (FromJCClassDec (JCClassDec (J mdf) n (J (JCIdent sup)) body))
%  ...
%\end{lstlisting}
%\end{centerTab}%
%Depending on whether a class has a modifier (such as \lstinline{public} and \lstinline{private}) or not, the concrete syntax is divided into two cases\draft{;} while we use a \lstinline{Maybe} type in the abstract syntax representing both cases.
%(To save space, the constructors \lstinline{Just} and \lstinline{Nothing} are shortened to \lstinline{J} and \lstinline{N} respectively.) Similarly, there are two more cases where a class does not extend some superclass and \draft{we omit them} here.
%
%
%Next, we show how a \lstinline{while} loop is represented as a basic \lstinline{for} loop, as the abstract syntax of a language should be as concise as possible\footnote{Although the JDT parser does not do this.}:
%\begin{centerTab}
%\begin{lstlisting}
%Statement <---> JCStatement
%  While "while" "(" exp ")" stmt ~ JCForLoop Nil exp Nil stmt
%\end{lstlisting}
%\end{centerTab}%
%where the four arguments of \lstinline{JCForLoop} in order denote (\draft{the} list of) initialisation statements, the loop condition, (\draft{the} list of) update expressions, and the loop body.
%As for a \lstinline{while} loop, we need to convert its loop condition \lstinline{exp} and loop body \lstinline{stmt} to AST types and put them in the correct places of the \lstinline{for} loop. Initialisation statements and update expressions are left empty since there is none.
%
%\todo[inline]{The above can probably be greatly reduced.}


\subsubsection{Demo}
\label{sec:refactorSysRun}
We can now perform some experiments on \autoref{fig:refactoringTextAndAst}.
\begin{itemize}

\item First we test \lstinline{put cst ast ls}, where \lstinline{(ast, ls) = get cst}.
We get back the same \lstinline{cst}, showing that the generated lenses do satisfy Hippocraticness.

\item As a special case of Correctness, we let \lstinline{cst' = put cst ast []} and check \lstinline{fst (get cst')} |==| \lstinline{ast}.
In \lstinline{cst'}, the \lstinline{while} loop becomes a basic \lstinline{for} loop and all the comments disappear.
This shows that |put| will create a new source solely from the view if links are missing.

\item Then we change \lstinline{ast} to \lstinline{ast'} and the set of links \lstinline{ls} to \lstinline{ls'} using our edit operations, simulating the \emph{push-down} code refactoring for the \lstinline{fuel} method.
To show the effect of Retentiveness more clearly, when building \lstinline{ast'}, the \lstinline{fuel} method in the \lstinline{Car} class is copied from the \lstinline{Vehicle} class, while the \lstinline{fuel} method in the \lstinline{Bus} class is built from scratch (i.e.~replaced with a `new' \lstinline{fuel} method).
Let \lstinline{cst' = put cst ast' ls'}.
In the \lstinline{fuel} method of the \lstinline{Car} class, the \lstinline{while} loop and its associated comments are preserved; but in the \lstinline{fuel} method of the \lstinline{Bus} class, there is only a \lstinline{for} loop without any associated comments.
This is where Retentiveness helps the user to retain information on demand. Finally, we also check that Correctness holds: \lstinline{fst (get cst')} |==| \lstinline{ast'}.

\end{itemize}


\subsection{Resugaring}
We have seen syntactic sugar such as negation and \lstinline{while} loops.
The idea of \emph{resugaring} is to print evaluation sequences in a core language using the constructs of its surface syntax (which contains sugar)~\cite{Pombrio2014Resugaring,Pombrio2015Hygienic}.
To solve the problem,~\citeauthor{Pombrio2014Resugaring}~\cite{Pombrio2014Resugaring} enrich the AST to incorporate fields for holding tags that mark from which syntactic object an AST construct comes.
%
Using retentive lenses, we can also solve the problem while leaving the AST clean---we can write consistency relations between the surface syntax and the abstract syntax and passing the generated |put| function proper links for retaining syntactic sugar, which we have already seen in the arithmetic expression example (where we retain the negation) and in the code refactoring example (where we retain the \lstinline{while} loop).
%
Both \citeauthor{Pombrio2014Resugaring}'s `tag approach' and our `link approach', in actuality, identifies where an AST construct comes from; however, the link approach has an advantage that it leaves ASTs clean and unmodified so that we do not need to patch up the existing compiler to deal with tags.


% In this subsection, we demonstrate that the resugaring problem can be solved while leaving ASTs clean and compact with the help of Retentiveness and our DSL: To retain syntactic sugar, we can write consistency relations between the surface syntax and the abstract syntax and pass the generated \lstinline{put} function proper links.


% For a programming language, usually the constructs of its surface syntax are richer than those of its abstract syntax (core language).





% We still take resugaring for \scName{Tiger} as an example. As shown in \autoref{fig:tigerAmbSyntax}, we use names (i.e.~data constructors) \lstinline{Or} and \lstinline{And} to represent \emph{logical or} and \emph{logical and} expressions in CSTs respectively; they will both be converted to \emph{if-then-else} expressions represented by \lstinline{TCond} (defined in \autoref{fig:tigerUnambSyntax}) in ASTs. Since the code for defining consistency relations for \scName{Tiger} in this DSL is substantially similar to the code for defining parser and printer pairs in \scName{BiYacc}, we only show the core part of the code regarding logical expressions.
% \begin{centerTab}
% \begin{lstlisting}
% Prmtv <---> Arith
%   Or  l r  ~  Cond l (Num 1) r
%   And l r  ~  Cond l r (Num 0)
%   ...
% \end{lstlisting}
% \end{centerTab}%
% With the generated \lstinline{put}, resugaring for the one-step evaluated \lstinline{ast' = Cond 0 1 c} from \lstinline{ast = Cond (Cond 0 10 0) 1 c} (that is parsed from text \lstinline{0 & 10 || c}) is illustrated in the following figure. For this simple case, we can obtain \lstinline{ast'} and links \lstinline{hls} by using the |replace| edit operation to replace \lstinline{Cond 0 10 0} in \lstinline{ast} with (the one-step evaluated result) \lstinline{0}.
% As for \lstinline{cst'}, the syntactic sugar \lstinline{Or} is preserved, for Retentiveness requires $|fst| \cdot |hls| \subseteq |fst| \cdot |hls'|$.\\
% \begin{center}
% \includegraphics[scale=0.65,trim={6cm 7.5cm 6cm 5.15cm},clip]{pics/resugaring.pdf}\\
% \end{center}
% Both \citeauthor{Pombrio2014Resugaring}'s `tag approach' and our `link approach', in actuality, identifies where an AST construct comes from; however, the links approach has an advantage that it leaves ASTs clean so that we do not need to patch up the existing compiler to deal with tags (and thus can use a common AST).


\subsection{XML Synchronisation}
\label{sec:XMLSync}

In this subsection, we present a case study on XML synchronisation, which is pervasive in the real world.
% and different from syntax tree manipulation.
The specific example used here is adapted from \citeauthor{Pacheco2014BiFluX}'s paper~\cite{Pacheco2014BiFluX}, where they use their DSL, \scName{BiFluX}, to synchronise address books.

As for their example, both the source address book and the view address book are grouped by social relationships; however, the source address book (defined by \lstinline{AddrBook}) contains names, emails, and telephone numbers whereas the view (social) address book (defined by \lstinline{SocialBook}) contains names only.
% \begin{small}
% \begin{minipage}[t]{0.5\textwidth}
% \begin{lstlisting}
% data AddrBook =
%   AddrBook (List AddrGroup)
% data AddrGroup =
%   AddrGroup String (List Person)
% data Person =
%   Person (Triple Name Email Tel)
% \end{lstlisting}
% \end{minipage}
% \begin{minipage}[t]{0.5\textwidth}
% \begin{lstlisting}
% data SocialBook  =
%   SocialBook (List SocialGroup)
% data SocialGroup =
%   SocialGroup String (List Name)
% data Triple a b c= Triple a b c
% type Name = String ...
% \end{lstlisting}
% \end{minipage}
% \end{small}

To synchronise \lstinline{AddrBook} and \lstinline{SocialBook}, we write consistency relations in our DSL and the core ones are
\begin{centerTab}
\begin{minipage}[t]{.55\textwidth}
\begin{lstlisting}
AddrGroup  <---> SocialGroup
  AddrGroup grp p  ~  SocialGroup grp p

List Person <---> List Name
  Nil  ~  Nil
  Cons p xs  ~  Cons p xs
\end{lstlisting}
\end{minipage}
\begin{minipage}[t]{.45\textwidth}
\begin{lstlisting}
Person <---> Name
  Person t  ~  t

Triple Name Email Tel <---> Name
  Triple name  _ _  ~  name (*@@.@@*)
\end{lstlisting}
\end{minipage}
\end{centerTab}
\\
The consistency relations will compile to a pair of \lstinline{get} and \lstinline{put}.


As \autoref{fig:XMLSync} shows, the original source is \lstinline{addrBook}
% \begin{small}
% \begin{centerTab}
% \begin{lstlisting}
% AddrBook
%   (Cons (AddrGroup "coworkers" (Cons
%     (Person (Triple "Alice" "alice@@abc.xyz" "000111")) (Cons
%     (Person (Triple "Bob" "bob@@abc.xyz" "222333")) Nil)))
%   (Cons (AddrGroup "friends" (Cons
%     (Person (Triple "Carol" "carol@@abc.xyz" "444555")) Nil)) Nil))
% \end{lstlisting}
% \end{centerTab}%
% \end{small}
and its consistent view is \lstinline{socialBook}, both of which have two relationship groups: \lstinline{coworkers} and \lstinline{friends}.
The source has a record \lstinline{Person (Triple "Alice" "alice@@abc.xyz" "000111")} in the group \lstinline{coworkers}, and we will see how this record changes in the new source after we update the view \lstinline{socialBook} in the following way and propagate the changes back:
we (i) reorder the two groups; (ii) change Alice's group from \lstinline{coworkers} to \lstinline{friends}; (iii) create a new social relationship group \lstinline{family} for family members.
% \begin{centerTab}
% \begin{lstlisting}
% SocialBook
%   (Cons (SocialGroup "coworkers" (Cons
%     "Alice" (Cons
%     "Bob" Nil)))
%   (Cons (SocialGroup "friends" (Cons
%     "Carol" Nil)) Nil))
% \end{lstlisting}
% \end{centerTab}%
% respectively.
% Then the view \lstinline{socialBook} is updated in a way that we (i) reorder the two groups; (ii) change Alice's group (from \lstinline{coworkers} to \lstinline{friends}); (iii) create a new social relationship group \lstinline{family} for family members.
% That is, if we assume that, in the social group \lstinline{friends}, \lstinline{Alice} is inserted after \lstinline{Carol}, the desired updated \lstinline{socialBook'} should be
% \begin{centerTab}
% \begin{lstlisting}
% SocialBook
%   (Cons (SocialGroup "friends" (Cons
%     "Carol" (Cons
%     "Alice" Nil)))
%   (Cons (SocialGroup "coworkers" (Cons
%     "Bob" Nil))
%   (Cons (SocialGroup "family" Nil) Nil))) (*@@.@@*)
% \end{lstlisting}
% \end{centerTab}%

In our case, to produce a new source \lstinline{socialBook'}, we handle the three updates using our basic edit operations (in this case, only |swap|, |move|, and |replace|) which also maintain the links.
Feeding the original source \lstinline{addrBook}, updated view \lstinline{socialBook'} and links \lstinline{hls'} to the (generated) \lstinline{put} function, we obtain the updated \lstinline{addrBook'}.
% \begin{small}
% \begin{centerTab}
% \begin{lstlisting}
% AddrBook (Cons (AddrGroup "friends" (Cons
%         (Person (Triple "Carol" "carol@@abc.xyz" "444555")) (Cons
%         (Person (Triple "Alice" "alice@@abc.xyz" "000111")) Nil)))
%   (Cons (AddrGroup "coworkers" (Cons
%         (Person (Triple "Bob" "bob@@abc.xyz" "222333")) Nil))
%   (Cons (AddrGroup "family" Nil) Nil))) (*@@.@@*)
% \end{lstlisting}
% \end{centerTab}%
% \end{small}
In \autoref{fig:XMLSync}, it is clearly seen that carefully maintained links help us to preserve email addresses and telephone numbers associated with each person during the |put| process; note that well-behavedness does not guarantee the retention of this information, since the input view is not consistent with the input source in this case.



\begin{figure}[t]
\centering
\includegraphics[scale=0.6,trim={4cm 8cm 4cm 3cm},clip]{pics/XMLSync.pdf}
\caption[An example of XML synchronisation.]{An example of XML synchronisation. (Grey areas highlight how the record \lstinline{Alice} is updated.)}
\label{fig:XMLSync}
\end{figure}


As pointed out by \citeauthor{Pacheco2014BiFluX}, examples of this kind motivate extensions to (combinator-based) alignment-aware languages such as \scName{Boomerang}~\cite{Bohannon2008Boomerang} and matching lenses~\cite{Barbosa2010Matching}.
In fact, it is hard for those languages to handle source-view alignment where some view elements are moved out of its original list-like structure (or \emph{chunk}~\cite{Barbosa2010Matching}) and put into a new list-like structure, probably far away---because when using those languages, we usually lift a lens combinator |k| handling a single element to $|k|^*$ dealing with a list of elements, so that the `scope' of the alignment performed by $|k|^*$ is always within that single list (which it currently works on).


\section{Related Work}
\label{sec:relatedWork}

\subsection{Alignment}
\label{sub:alignment}

\emph{Alignment} has been recognised as an important problem when we need to synchronise two lists. Our work is closely related.
% ---if a view element (in a list) is modified (e.g., inserted, deleted, or reordered), which source element should be matched with the (modified) view element and updated correspondingly?

\subsubsection{Alignment for Lists}
The earliest lenses~\cite{Foster2007Combinators} only allow source and view elements to be matched positionally---the $n$-th source element is simply updated using the $n$-th element in the modified view. Later, lenses with more powerful matching strategies are proposed, such as dictionary lenses~\cite{Bohannon2008Boomerang} and their successor matching lenses~\cite{Barbosa2010Matching}.
% In matching lenses, a source is divided into a \emph{resource} consisting of \emph{chunks} of information that can be reordered, and a `rigid complement' storing information outside the chunk structure; the reorderable chunk structure is preserved in the view.
As for matching lenses, when a |put| is invoked, it will first find the correspondence between \emph{chunks} (data structures that are reorderable, such as lists) of the old and new views using some predefined strategies; based on the correspondence, the chunks in the source are aligned to match the chunks in the new view. Then element-wise updates are performed on the aligned chunks. Matching lenses are designed to be practically easy to use, so they are equipped with a few fixed matching strategies (such as greedy align) from which the user can choose. However, whether the information is retained or not, still depends on the lens applied after matching. As a result, the more complex the applied lens is, the more difficult to reason about the information retained in the new source.
Moreover, it suffers a disadvantage that the alignment is only between a single source list and a single view list, as already discussed in the last paragraph of \autoref{sec:XMLSync}.
% look up elements in all the lists.
\scName{BiFluX}~\cite{Pacheco2014BiFluX} overcomes the disadvantage by providing the functionality that allows the user to write alignment strategies manually; in this way, when we see several lists at once, we are free to search for elements and match them in all the lists. But this alignment still has the limitation that each source element and each view element can only be matched at most once---after that they are classified as either \emph{matched pair}, \emph{unmatched source element}, or \emph{unmatched view element}. Assuming that an element in the view has been copied several times, there is no way to align all the copies with the same source element. (However, it is possible to reuse an element several times for the handling of unmatched elements.)

By contrast, retentive lenses are designed to abstract out matching strategies (alignment) and are more like taking the result of matching as an additional input. This matching is not a one-layer matching but rather, a global one that produces (possibly all the) links between a source's and a view's unchanged parts. The information contained in the linked parts is preserved independently of any further applied lenses.



 \subsubsection{Alignment for Containers}
 To generalise list alignment, a more general notion of data structures called \emph{containers}~\cite{Abbott2005Containers} is used~\cite{Hofmann2012Edit}.
 In the container framework, a data structure is decomposed into a \emph{shape} and its \emph{content}; the shape encodes a set of positions, and the content is a mapping from those positions to the elements in the data structure.
 The existing approaches to container alignment take advantage of this decomposition and treat shapes and contents separately.
 For example, if the shape of a view container changes, \citeauthor{Hofmann2012Edit}'s approach will update the source shape by a fixed strategy that makes insertions or deletions at the rear positions of the (source) containers.
 By contrast, \citeauthor{Pacheco2012Delta}'s method permits more flexible shape changes, and they call it \emph{shape alignment}~\cite{Pacheco2012Delta}.
 In our setting, both the consistency on data and the consistency on shapes are specified by the same set of consistency declarations.
 In the |put| direction, both the data and shape of a new source is determined by (computed from) the data and shape of a view, so there is no need to have separated data and shape alignments.

 Container-based approaches have the same situation (as list alignment) that the retention of information is dependent on the basic lens applied after alignment. Moreover, the container-based approaches face another serious problem:
 they always translate a change on data in the view to another change on data in the source, without affecting the shape of a container. This is wrong in some cases, especially when the decomposition into shape and data is inadequate.
 For example, let the source be \lstinline{Neg (Lit 100)} and the view \lstinline{Sub (Num 0) (Num 100)}. If we modify the view by changing the integer \lstinline{0}~to~\lstinline{1} (so that the view becomes \lstinline{Sub (Num 1) (Num 100)}), the container-based approach would not produce a correct source \lstinline{Minus ...}, as this data change in the view must not result in a shape change in the source.
 In general, the essence of container-based approaches is the decomposition into shape and data such that they can be processed independently (at least to some extent), but when it comes to scenarios where such decomposition is unnatural (like the example above), container-based approaches hardly help.

\subsection{Provenance and Origin}
\label{sec:provenance}

Our idea of links is inspired by research on provenance~\cite{Cheney2009Provenance} in database communities and origin tracking~\cite{vanDeursen1993Origin} in the rewriting communities.

\citeauthor{Cheney2009Provenance} classify provenance into three kinds, \emph{why}, \emph{how}, and \emph{where}: \emph{why-provenance} is the information about which data in the view is from which rows in the source; \emph{how-provenance} additionally counts the number of times a row is used (in the source); \emph{where-provenance} in addition records the column where a piece of data is from. In our setting, we require that two pieces of data linked by vertical correspondence be equal (under a specific pattern), and hence the vertical correspondence resembles where-provenance.
However, the above-mentioned provenance is not powerful enough as they are mostly restricted to relational data, namely rows of tuples---in functional programming, the algebraic data types are more complex. For this need, \emph{dependency provenance}~\cite{Cheney2011Provenance} is proposed; it tells the user on which parts of a source the computation of a part of a view depends. In this sense, our consistency links are closer to dependency provenance.

The idea of inferring consistency links can be found in the work on origin tracking for term rewriting systems~\cite{vanDeursen1993Origin}, in which the origin relations between rewritten terms can be calculated by analysing the rewrite rules statically.
However, it was developed solely for building trace between intermediate terms rather than using trace information to update a tree further. Based on origin tracking, \citeauthor{deJonge2012Algorithm} implemented an algorithm for code refactoring systems, which `preserves formatting for terms that are not changed in the (AST) transformation, although they may have changes in their subterms'~\cite{deJonge2012Algorithm}.
This description shows that the algorithm also decomposes large terms into smaller ones resembling our regions.
In terms of the formatting aspect, we think that retentiveness can in effect be the same as their theorem if
% However, they only tailored the theorem for their specific printing algorithm but did not generalise the theorem to other scenarios.
we include vertical correspondence (representing view updates) in the theory, rather than dealing with it implicitly and externally as in \autoref{sec:editOperations}.

%This was something we thought about (for quite some time), but eventually, we opted for the current, simpler theory. The rationale is that the original state-based lens framework (which we extended) does not really have the notion of view update built in. A view given to a |put| function is not necessarily modified from the view got from the source---it can be constructed in any way, and |put| does not have to consider how the view is constructed. Coincidentally, the paper about (symmetric) delta-based lenses~\cite{Diskin2011Symmetric} also introduces square diagrams and later switches to triangular diagrams.

%We retain this separation of concern in our framework, in particular separating the jobs of retentive lenses and third-party tools that operate in a view update setting: third-party tools are responsible for producing vertical correspondence between the consistent view and a modified view (not between sources and views), and when it is time to run |put|, the vertical correspondence are composed (as shown \autoref{fig:swapAndPut}) with the consistency links produced by |get| to compute the input links between the source and the modified view. We will present a concrete example regarding vertical correspondence and edit operations in \autoref{sec:editOperations}.


% \note{deleted. no space}
% Similarly, \citeauthor{Martins2014Generating} developed a system for attribute grammars which define transformations between tree structures (in particular CSTs and ASTs)~\cite{Martins2014Generating}. Their bidirectional system also uses links to trace the correspondence between source nodes and view nodes, which is later used by |put| to solve the syntactic sugar problem.
% The differences between their system and ours are twofold: One is that in their system, links are implicitly used in the |put| direction. The advantage (of implicit link usage) is that, for the user, links become transparent and are automatically maintained when a view is updated; the disadvantage is that, as a result, newly created nodes on an AST can never have `links back' to the old CST, even if they might be the copies of some old nodes.
% The second difference is the granularity of links; in their system, a link seems to connect the whole subtrees between a CST and an AST instead of between smaller regions. As a result, if a leaf of an AST is modified, all the nodes belonging to the spine from the leaf to the root will lose their links.

The use of consistency links can also be found in \citeauthor{Wang2011Incremental}'s work, where the authors extend state-based lenses and use links for tracing data in a view to its origin in a source~\cite{Wang2011Incremental}.
When a sub-term in the view is edited locally, they use links to identify a sub-term in the source that `contains' the edited sub-term in the view. When updating the old source, it is sufficient to only perform state-based |put| on the identified sub-term (in the source) so that the update becomes an incremental one. Since lenses generated by our DSL also create consistency links (albeit for a different purpose), they can be naturally incrementalised using the same technique.

\subsection{Operation-based BX}
\label{sec:operationBX}

Our work is closely relevant to the operation-based approaches to BX, in particular, the delta-based BX model~\cite{Diskin2011Asymmetric,Diskin2011Symmetric} and edit lenses~\cite{Hofmann2012Edit}. The (asymmetric) delta-based BX model regards the differences between a view state |v| and |v'| as \emph{deltas}, which are abstractly represented as arrows (from the old view to the new view).
The main law of the framework can be described as `given a source state |s| and a view delta $|det|_|v|$, $|det|_|v|$ should be translated to a source delta $|det|_|s|$ between |s| and $s'$ satisfying |get s' = v'|'.
%
As the law only guarantees the existence of a source delta $|det|_|s|$ that updates the old source to a correct state, it is yet not sufficient to derive Retentiveness in their model, for there are infinite numbers of translated delta $|det|_|s|$ which can take the old source to a correct state, of which only a few are `retentive'.
To illustrate, \citeauthor{Diskin2011Asymmetric} tend to represent deltas as edit operations such as \emph{create}, \emph{delete}, and \emph{change}; representing deltas in this way will only tell the user what must be changed in the new source, while it requires additional work to reason about what is retained.
However, it is possible to exhibit Retentiveness if we represent deltas in some other proper form. Compared to \citeauthor{Diskin2011Asymmetric}'s work, \citeauthor{Hofmann2012Edit} give concrete definitions and implementations for propagating edit operations (in a symmetric setting).

\section{Conclusion}
\label{sec:conclusion}

In this paper, we showed that well-behavedness is not sufficient for retaining information after an update and it may cause problems in many real-world applications.
To address the issue, we illustrated how to use links to preserve desired data fragments of the original source, and developed a semantic framework of (asymmetric) retentive lenses.
Then we presented a small DSL tailored for describing consistency relations between syntax trees; we showed its syntax, semantics, and proved that the pair of |get| and |put| functions generated from any program in the DSL form a retentive lens.
We provide four edit operations which can update a view together with the links between the view and the original source, and demonstrated the practical use of retentive lenses for code refactoring, resugaring, and XML synchronisation;
we discussed related work about alignment, origin tracking, and operation-based BX.
Some further discussions can be found in the appendix (\autoref{sec:discussions}).

% At the end of the paper, we will briefly discuss Strong Retentiveness (that subsumes Hippocraticness).


% Acknowledgments
\begin{acks}                            %% acks environment is optional
                                        %% contents suppressed with 'anonymous'
  %% Commands \grantsponsor{<sponsorID>}{<name>}{<url>} and
  %% \grantnum[<url>]{<sponsorID>}{<number>} should be used to
  %% acknowledge financial support and will be used by metadata
  %% extraction tools.
  % This material is based upon work supported by the
  % \grantsponsor{GS100000001}{National Science
  %   Foundation}{http://dx.doi.org/10.13039/100000001} under Grant
  % No.~\grantnum{GS100000001}{nnnnnnn} and Grant
  % No.~\grantnum{GS100000001}{mmmmmmm}.  Any opinions, findings, and
  % conclusions or recommendations expressed in this material are those
  % of the author and do not necessarily reflect the views of the
  % National Science Foundation.
  We thank Jeremy Gibbons, Meng Wang for useful discussions and comments.
  Yongzhe Zhang helped us to create a nice figure in the introduction of the last submission, although the figure was later revised.
  This work is partially supported by the \grantsponsor{GS501100001691}{Japan Society for the Promotion of Science}{https://doi.org/10.13039/501100001691} (JSPS) Grant-in-Aid for Scientific Research (S)~No.~\grantnum{GS501100001691}{17H06099}.
\end{acks}

\newpage

% changing fontsize of the biblio might be illegal
\renewcommand*{\bibfont}{\small}
\bibliography{retentive}

\newpage
\appendix

%include appendix.lhs


% \begin{appendices}

% \end{appendices}

\end{document}
