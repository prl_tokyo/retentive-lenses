import BX
import BXDef
import Data.Dynamic

zero = Zero
one = Succ Zero
two = Succ (Succ Zero)

src1 = Node one (Node zero Tip Tip) (Node two Tip Tip)

-- Node TTrue (Node FFalse Tip Tip) (Node TTrue Tip Tip)
getSrc1 = topGetBinTNatBinTBoool src1

-- produce exactly (Node 1 (Node 0 Tip) (Node 2 Tip))
putSrc1Identity =
  fromDynamic (topPut BinTNatTag BinTNatTag BinTBooolTag (toDyn src1)
                (toDyn $ fst getSrc1) (snd getSrc1)) :: Maybe (BinT Nat)

-- produce something like (Node 1 (Node 0 Tip) (Node 1 Tip))
putSrc1NoLink =
  fromDynamic (topPut BinTNatTag BinTNatTag BinTBooolTag (toDyn src1)
                (toDyn $ fst getSrc1) ([])) :: Maybe (BinT Nat)
