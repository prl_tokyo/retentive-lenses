AddrBook <---> SocialBook
    AddrBook xs  ~  SocialBook xs
;

List AddrGroup  <---> List SocialGroup
    Nil        ~  Nil
    Cons x xs  ~  Cons x xs
;

AddrGroup  <---> SocialGroup
    AddrGroup grp p  ~  SocialGroup grp p
;

List Person <---> List Name
	Nil  ~  Nil
	Cons p xs  ~  Cons p xs
;


Person <---> Name
	Person t  ~  t
;

Triple Name Email Tel <---> Name
	Triple name  _ _  ~  name
;