import BX
import BXDef
import Data.Dynamic
import EditOperations
import Data.List (sort)

cst = AddrBook (Cons coworkers (Cons friends Nil))

coworkers = AddrGroup "coworkers" (
  Cons (Person (Triple "Alice" "alice@nii.ac.jp" "000111"))
    (Cons (Person (Triple "Bob" "bob@nii.ac.jp" "222333"))
      Nil))

friends = AddrGroup "friends" (
  Cons (Person (Triple "Carol" "carol@abc.xyz" "444555"))
  Nil)

(ast, consisLinks) = topGetAddrBookSocialBook cst


editAST ast hls =
  let coworkerGrpLoc = [0,0]
      frdGrpLoc = [0,1,0]

      -- reorder the two groups
      res1 = swapNode (coworkerGrpLoc, frdGrpLoc) (ast, hls)

      -- change alice’s group
      aliceNewConsLoc = [0,0,1,1]
      -- step 1 change Nil to Cons _ Nil
      resTemp = replaceNode (aliceNewConsLoc, toDyn (Cons "" Nil)) res1
      aliceLoc = [0,1,0,1,0]
      -- since Nil ---> Cons _ Nil. add a [0] to the old path
      aliceNewLoc = aliceNewConsLoc ++ [0]
      -- step 2 move alice
      res2 = moveNode (toDyn "", aliceLoc, aliceNewLoc) resTemp

      -- delete the node before zhenjiang in the list == move zhenjiang to the previous location.
      zhenjiangOldConsLoc = [0,1,0,1,1]
      zhenjiangNewConsLoc = [0,1,0,1]
      res3 = moveNode (toDyn (Nil :: List String), zhenjiangOldConsLoc, zhenjiangNewConsLoc) res2

      -- add a new group 'family'
      family = SocialGroup "family" Nil
      res4 = replaceNode ([0,1,1], toDyn (Cons family Nil)) res3

  in  res4

(newAST, newHls) = editAST ast consisLinks

putWithDiagLink = fromDyn (topPut AddrBookTag AddrBookTag SocialBookTag
            (toDyn cst) (toDyn $ newAST) newHls) AddrBookNull

