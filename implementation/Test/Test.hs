module Test where

import Main
import Def
import TypeInf
import ParserInterface
import GenProgram
import GenBX.GenPutWithLinks
import PrintDataTypes
import GenHelper.GenFetchable
import GenVCorrHelper.GenInsertable
import GenVCorrHelper.GenDefVal
import GenVCorrHelper.GenNodeAndPath
import GenHelper.GenTypeable
import GenHelper.GenSelSPat
import GenHelper.GenRegPat
import GenHelper.GenRepSubtree
import GenHelper.GenInsSubtree
import GenHelper.GenInterConv

import Data.Maybe
import Data.Map hiding (map, take, null, filter)
import Text.Parsec
import Text.Show.Pretty
import qualified Language.Haskell.TH as TH

import System.Directory
import Text.PrettyPrint

import System.Environment
import Control.Monad
import System.IO.Unsafe
import Debug.Trace

data TCase =
    TExp
  | TMirror
  | TMap
  | TSpine
  | TSJava
  | TAddrBook
  deriving (Show, Eq)

testAll :: IO ()
testAll = do
  sequence_ $ map test [TExp, TMirror, TMap, TSpine, TSJava, TAddrBook]

test :: TCase -> IO ()
test tcase = do
  let (dir, dFile, crFile) = case tcase of
        TExp    -> ("Test/expression/", "expType.txt", "expProgram.txt")
        TMirror -> ("Test/mirror/", "mirrorType.txt", "mirrorProgram.txt")
        TMap    -> ("Test/map/", "mapType.txt", "mapProgram.txt")
        TSpine  -> ("Test/spine/", "spineType.txt", "spineProgram.txt")
        TSJava  -> ("Test/JavaSubset/", "SimplifiedJavaType.txt"
                   ,"SimplifiedJavaProgram.txt")
        TAddrBook -> ("Test/addrBook/", "addrBookType.txt", "addrBookProgram.txt")
  (tyDecs, InterConv intcvs_, crs_) <- parseAll (dir ++ dFile) (dir ++ crFile)
  let crs = map (\g@(Group (sTy, vTy) _) -> applyTyAnno sTy vTy tyDecs g) crs_
      prog       = foldr1 newlineSS $ map TH.pprint (genProgram crs)
      regPatEnv  = genRegPatEnv crs
      intcvsAuto = genInterConvAutos tyDecs crs
      intcvs     = diffBy (\(InjDecl (f1,t1) _ _) (InjDecl (f2,t2) _ _) ->
                              f1 == f2 && t1 == t2) intcvsAuto intcvs_ ++ intcvs_

  -- traceM (ppShow $ instantiateAllParTyReps tyDecs crs)
  writeFile (dir ++ "BXDef.hs") $ foldr1 newlineSS
    [bxDefModHead
    ,prtAbsDTs tyDecs
    ,TH.pprint $ genTyTagDef crs tyDecs
    ,genTyTagClass
    ,genTyTagInstances tyDecs crs

    ,genFetchableClass
    ,genFetchableInstance tyDecs
    ,genFetchDispatches crs

    ,genInsertableClass
    ,genInsertableInstance crs tyDecs

    ,genDefValClass
    ,genDefValInstance crs tyDecs

    ,genNodeAndPathClass
    ,genNodeAndPathInstance crs tyDecs

    ,genAllInjDef intcvs
    ,genSelSPatDef crs
    ,genAskDynTyTagDef crs tyDecs

    ,spliceDef
    ,genRepSubtreeDef regPatEnv crs
    ,genInsSubtreeDef regPatEnv crs
    ,genRegPatDef crs
    ]

  writeFile (dir ++ "BX.hs") $ foldr1 newlineSS
    [bxModHead
    ,prog
    ,topPut
    ,""
    ]

  editDefs <- readFile ("VerticalCorr/EditOperations.hs")
  writeFile (dir ++ "EditOperations.hs") editDefs

-- delete elements from l1 satisfying judge => preserving elements if judge does not hold
diffBy :: Eq a => (a -> a -> Bool) -> [a] -> [a] -> [a]
diffBy judge l1 [] = l1
diffBy judge l1 (l:l2) = diffBy judge (filter (not . (judge l)) l1) l2
