{-
Test the push down code refactoring.

Useful library functions (contained in BXDef module):

-- ask the type-tag of a dynamic value.
askDynTyTag :: Dynamic -> TyTag

(there is a function  toDyn : a -> Dynamic  which wraps the given input into Dynamic
from the module Data.Dynamic)

-- return a default value according to the given type-tag
defVal :: TyTag -> a

-- compose consistency links and vertical correspondence:
compHVs :: [RLink] -> [VCorr] -> [RLink]

-- enumerate all the nodes with their pahts wrapped in Dynamic
-- nodeAndPath :: a -> [(Dynamic, Path)]

-}

import BX
import BXDef
import Data.Dynamic
import Data.Data
import Data.Maybe
import Data.List
import EditOperations

-- public class Vehicle {
--   /** fuelling
--   */
--   public void fuel (int vol) {
--     while (vol < 10){
--       oil = oil + 1;
--       vol = vol - 1;
--     }
--   }
-- }

-- public class Car extends Vehicle {

-- }

-- public class Bus extends Vehicle {

-- }

-- public class Bicycle extends Vehicle {

-- }


cst :: CompilationUnit
cst = CompilationUnit $
  Cons vehicle (Cons car (Cons bus (Cons bicycle Nil)))

vehicle = FromClassDeclaration (NormalClassDeclaration3 "VehicleClass" "public" "class" "Vehicle" vehicleBody)

vehicleBody :: ClassBody
vehicleBody = ClassBody1 "{" (Cons (FromClassMemberDeclaration memberDec) Nil) "}"

memberDec :: ClassMemberDeclaration
memberDec = MethodDeclaration1 "fuelling it!" "public"
  (MethodHeader (LLeft UInteger) (MethodDeclarator1 "fuel" "(" methArg ")")) methBody

methArg :: Prod UType String
methArg = Prod UInteger "vol"

methBody :: MethodBody
methBody = MethodBody0 (Cons (FromStatement whileLoop) Nil)

whileLoop :: Statement
whileLoop = While "while" "(" whileCond ")" (FromBlockStatement whileStmtBlock)

whileCond = AssignExp . FromRelExp $ RelExp0
  (FromAdditiveExp (FromPostfixExp (ExpN "vol")))
  "<"
  (FromPrimary (PrimaryLit (UIntegerLit 10)))

whileStmtBlock = Cons blockStmt1 blockStmt2

blockStmt1 = FromStatement (ExpressionStatement (AssignExp assign1) ";")
blockStmt2 = Cons (FromStatement (ExpressionStatement (AssignExp assign2) ";")) Nil

assign1 = Assignment "oil" "=" oilAddExp
assign2 = Assignment "vol" "=" volSubExp

oilAddExp = AssignExp . FromRelExp . FromAdditiveExp $
  AdditiveExp0 (FromPostfixExp (ExpN "oil")) "+" (FromPrimary (PrimaryLit (UIntegerLit 1)))
volSubExp = AssignExp . FromRelExp . FromAdditiveExp $
  AdditiveExp0 (FromPostfixExp (ExpN "vol")) "-" (FromPrimary (PrimaryLit (UIntegerLit 1)))

-- arrExpr = AssignExp (FromRelExp (FromAdditiveExp (FromPostfixExp (ExpN "arr"))))

-- empty classes car, bus, and bicycle
car = FromClassDeclaration (NormalClassDeclaration1 "CarClass" "public" "class" "Car" "extends" "Vehicle" nilBody)

bus = FromClassDeclaration (NormalClassDeclaration1 "BusClass" "public" "class" "Bus" "extends" "Vehicle" nilBody)

bicycle = FromClassDeclaration (NormalClassDeclaration1 "BicycleClass" "public" "class" "Bicycle" "extends" "Vehicle" nilBody)

nilBody :: ClassBody
nilBody = ClassBody0 "{" "}"


getcst = topGetCompilationUnitJCCompilationUnit cst
putIdLinks = fromDyn (topPut CompilationUnitTag CompilationUnitTag JCCompilationUnitTag
  (toDyn cst) (toDyn $ fst v) (snd v)) CompilationUnitNull
  where v = topGetCompilationUnitJCCompilationUnit cst
testPutIdLinks = putIdLinks == cst

putIdNoLink = fromDyn (topPut CompilationUnitTag CompilationUnitTag JCCompilationUnitTag
  (toDyn cst) (toDyn $ fst v) []) CompilationUnitNull
  where v = topGetCompilationUnitJCCompilationUnit cst
testPutIdNoLink = fst getcst == fst (topGetCompilationUnitJCCompilationUnit (putIdNoLink))


ast = fst $ getcst
consisLinks = snd $ getcst

-- ast' is created by pushing the fuel method declaration into Car and Bus but not Bicycle
ast' = JCCompilationUnit
  (Cons (FromJCStatement (FromJCClassDecl (JCClassDecl (JJust "public") "Vehicle" NNothing Nil)))
  (Cons (FromJCStatement (FromJCClassDecl (JCClassDecl (JJust "public") "Car" (JJust (JCIdent "Vehicle")) (Cons fuelDecl Nil))))
  (Cons (FromJCStatement (FromJCClassDecl (JCClassDecl (JJust "public") "Bus" (JJust (JCIdent "Vehicle")) (Cons fuelDecl Nil))))
  (Cons (FromJCStatement (FromJCClassDecl (JCClassDecl (JJust "public") "Bicycle" (JJust (JCIdent "Vehicle")) Nil))) Nil))))

fuelDecl = FromJCMethodDecl (JCMethodDecl (JJust "public") "fuel" (JCPrimitiveTypeTree UInteger) (JJust (JCVariableDecl NNothing "vol" (JCPrimitiveTypeTree UInteger) NNothing)) (Cons (JCForLoop Nil (JCBinary LESSTHAN (JCIdent "vol") (FromJCLiteral (UIntegerLit 10)) "<") Nil (FromJCBlock (Cons (FromJCExpressionStatement (JCAssign (JCIdent "oil") (JCBinary PLUS (JCIdent "oil") (FromJCLiteral (UIntegerLit 1)) "+"))) (Cons (FromJCExpressionStatement (JCAssign (JCIdent "vol") (JCBinary MINUS (JCIdent "vol") (FromJCLiteral (UIntegerLit 1)) "-"))) Nil)))) Nil))

-- now we create the diagonal links by hand:
-- we link the fuel method in the Car class so that the EnhancedFor is preserved
-- as comparison, we do not link the fuel method in the Bus class so that a default BasicFor will be created
diagLinks = shiftPaths (deleteBusLinks (deleteCarLinks consisLinks))

-- we delete the links connected to the body of the Vehicle class, since the method disappear
deleteCarLinks = filter (\((sr,sp),(vr,vp)) -> length vp <= 3 || take 3 vp /= [0,1,0])


-- the fuel method is Pushed Down into the Car class, so that the method should have links to the old source cst
-- we can view this modification as "shift" the fuel method from Vehicle class to the Car class
-- so that the new links can be obtained by shift the prefix of the old links
-- before that, we delete the links of the bus class
deleteBusLinks = filter (\((sr,sp),(vr,vp)) -> length vp <= 4 || take 4 vp /= [0,1,1,0])

shiftPaths = map shiftPath
-- shift the path [0,0,0,0...] in view-region to [0,1,0,0,0...]
-- create diagonal links. Region = (RegPat, Path)
shiftPath :: (Region, Region) -> (Region,Region)
shiftPath ((sr,sp),(vr, 0:0:0:0:3:vp)) = ((sr,sp),(vr, 0:1:0:0:0:3:vp))
shiftPath a = a


-- now we cant test put cst ast' with diagonal links
-- the result will show:
-- (1) a Vehicle class with empty body
-- (2) a Car class with annotations and While loop preserved (search in the printed result for (MethodDeclaration1 "fuelling it!") and (While))
-- (3) a Bus class without annotations and a Basic for loop is created (search in the printed result for (MethodDeclaration1 "DEF_VAL") and (BasicFor))
-- (4) a Bicycle class with empty body
-- putDiagLink' = putStrLn . ppShow $ putDiagLink

-- result of (put cst ast' links)
putDiagLink = fromDyn (topPut CompilationUnitTag CompilationUnitTag JCCompilationUnitTag
  (toDyn cst) (toDyn $ ast') diagLinks) CompilationUnitNull

-- True  (Correctness)
testPutDiagLink = fst (topGetCompilationUnitJCCompilationUnit putDiagLink) == ast'

-- False, of course
testPutDiagLink2 = topGetCompilationUnitJCCompilationUnit putDiagLink == getcst

-- Retentiveness
testRetentiveness = map (\((a,b),(c,d)) -> (a,(c,d))) diagLinks `subList`
  (map (\((a,b),(c,d)) -> (a,(c,d))) . snd . topGetCompilationUnitJCCompilationUnit $ putDiagLink)



--------------------------
-- after code refactoring


-- public class Vehicle {

-- }

-- public class Car extends Vehicle {
--   /** fuelling
--   */
--   public void fuel (int vol) {
--     while (vol < 10 ){
--       oil = oil + 1;
--       vol = vol - 1;
--     }
--   }
-- }

-- public class Bus extends Vehicle {
--   /** fuelling
--   */
--   public void fuel (int vol) {
--     for ( ; vol < 10 ; ){
--       oil = oil + 1;
--       vol = vol - 1;
--     }
--   }
-- }


-- public class Bicycle extends Vehicle {

-- }




-- Test with edit operations

pushDownEdits :: JCCompilationUnit -> [HLink] -> (JCCompilationUnit, [HLink])
pushDownEdits ast hls =
  let fuelOldLoc = head $ locateMethod (JCTreeTag, isFuelMethodAST) ast -- [0,0,0,0,3,0]
      -- the loccation of Cons. We neee to first replace the parent Nil node with a Cons node.
      carFuelConsLoc = [0,1,0,0,0,3]

      res1 = replaceNode (carFuelConsLoc, toDyn (Cons JCTreeNull Nil)) (ast, hls)

      carFuelLoc = [0,1,0,0,0,3,0] -- the first child of the newly created Cons node
      res2 = copyNode (fuelOldLoc, carFuelLoc)  res1
      -- finished copying the fule method into the Car (sub)class

      busFuelConsLoc = [0,1,1,0,0,0,3]
      res3 = replaceNode (busFuelConsLoc, toDyn (Cons fuelDecl Nil)) res2
      -- finished (newly) inserting the fule method into the Bus (sub)class

      res4 = replaceNode (init fuelOldLoc, toDyn (Nil :: List JCTree)) res3 -- init fuelOldLoc is the Cons node.
      -- finished removing the fule method from the Vehicle class

  in  res4


(newAST, newHls) = pushDownEdits ast consisLinks

-- result of (put cst ast' links)
putDiagLink' = fromDyn (topPut CompilationUnitTag CompilationUnitTag JCCompilationUnitTag
            (toDyn cst) (toDyn $ newAST) newHls) CompilationUnitNull

-- True  (Correctness)
testPutDiagLink' = fst (topGetCompilationUnitJCCompilationUnit putDiagLink') == newAST

-- False, of course
testPutDiagLink2' = fst (topGetCompilationUnitJCCompilationUnit putDiagLink') == fst getcst

-- Retentiveness
testRetentiveness' = map (\((a,b),(c,d)) -> (a,(c,d))) newHls `subList`
  (map (\((a,b),(c,d)) -> (a,(c,d))) . snd . topGetCompilationUnitJCCompilationUnit $ putDiagLink')


------------ auxiliary functions


locateMethod :: (Typeable a, Insertable a, Fetchable a, Data a, NodeAndPath a, Typeable b, HasDefVal b) =>
  (TyTag, (b -> Bool)) -> a -> [Path]
locateMethod (ty, p) t =
  let xs = nodeAndPath t
  in  concatMap (\(dyn,path) -> if ty == askDynTyTag dyn
                                then if p (fromDyn dyn (defVal ty))
                                     then [path]
                                     else []
                                else []) xs


--
isFuelMethodCST :: ClassMemberDeclaration -> Bool
isFuelMethodCST memberDecl = case memberDecl of
  (MethodDeclaration0 _ header _) -> checkHd header
  (MethodDeclaration1 _ _ header _) -> checkHd header
  _ -> False
  where checkHd (MethodHeader _ (MethodDeclarator0 id _ _))   = id == "fuel"
        checkHd (MethodHeader _ (MethodDeclarator1 id _ _ _)) = id == "fuel"

--
isFuelMethodAST :: JCTree -> Bool
isFuelMethodAST jctree = case jctree of
  (FromJCMethodDecl decl) -> isFuelMethodAST' decl
  _ -> False

isFuelMethodAST' :: JCMethodDecl -> Bool
isFuelMethodAST' decl = case decl of
  JCMethodDecl _ name _ _ _ -> name == "fuel"
  _ -> False


-- a little dirty.
subList :: (Ord a) => [a] -> [a] -> Bool
subList xs ys = sort xs `isSubsequenceOf` sort ys
