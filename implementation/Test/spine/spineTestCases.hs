import BX
import BXDef
import Data.Dynamic

src1 :: RTree Integer
src1 = RNode 1
         (RNode 11 Nil `Cons` (RNode 22 Nil `Cons` Nil))


src2 :: RTree Integer
src2 = RNode 1
         (  (RNode 11
              (RNode 111 Nil `Cons` Nil)) `Cons`
            ((RNode 22 Nil) `Cons`
            ((RNode 33 Nil) `Cons` Nil)))

-- Cons 1 (Cons 11 (Cons 111 Nil))
getSrc2 = topGetRTreeIntegerListInteger src2


-- produce exactly
-- RNode 1 (Cons (RNode 11 (Cons (RNode 111 Nil) Nil)) (Cons (RNode 22 Nil) (Cons (RNode 33 Nil) Nil)))
putSrc2Identity =
  fromDynamic (topPut RTreeIntegerTag RTreeIntegerTag ListIntegerTag (toDyn src2)
                (toDyn $ fst getSrc2) (snd getSrc2)) :: Maybe (RTree Integer)

-- produce  RNode 1 (Cons (RNode 11 (Cons (RNode 111 Nil) ListNull)) ListNull)
-- the subtrees not in the spine got lost as expected
putSrc2NoLink =
  fromDynamic (topPut RTreeIntegerTag RTreeIntegerTag ListIntegerTag (toDyn src2)
                (toDyn $ fst getSrc2) ([])) :: Maybe (RTree Integer)
