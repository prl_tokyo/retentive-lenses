import BX
import BXDef
import Data.Dynamic

src1 = Node 1 (Node 0 (Node 100 Tip Tip) (Node 200 Tip Tip)) (Node 2 Tip Tip)

-- produce Node 1 (Node 2 Tip Tip) (Node 0 (Node 200 Tip Tip) (Node 100 Tip Tip)
getSrc1 = topGetBinTIntegerBinTInteger src1

-- produce exactly Node 1 (Node 0 (Node 100 Tip Tip) (Node 200 Tip Tip)) (Node 2 Tip Tip)
putSrc1Identity =
  fromDynamic (topPut BinTIntegerTag BinTIntegerTag BinTIntegerTag (toDyn src1)
                (toDyn $ fst getSrc1) (snd getSrc1)) :: Maybe (BinT Integer)

-- produce the same result. this is a bijection
putSrc1NoLink =
  fromDynamic (topPut BinTIntegerTag BinTIntegerTag BinTIntegerTag (toDyn src1)
                (toDyn $ fst getSrc1) ([])) :: Maybe (BinT Integer)
