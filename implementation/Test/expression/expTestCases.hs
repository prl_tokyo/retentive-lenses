import BX
import BXDef
import Data.Dynamic
import EditOperations
import Data.List (sort)

expr = Plus "comm+" eaddE2 eaddT2
eaddE2 = Minus "comm-" (FromT "" (FromF "" (Lit "comm0" 0))) (FromF "" (Lit "comm1" 1))
eaddT2 = FromF "" (Neg "neg" (Lit "comm2" 2))
-- expr = 0 "comm0" - "comm-" 1 "comm1"    + "comm+"  -2 "comm2"

-- consistent ast
ast2  = Add (Sub (Num 0) (Num 1)) (Sub (Num 0) (Num 2))

-- consistency links
consisLinks = snd $ topGetExprArith expr

-- identity put
testIdPut = flip fromDyn ExprNull $
 topPut ExprTag ExprTag ArithTag (toDyn expr) (toDyn . fst $ topGetExprArith expr) (snd $ topGetExprArith expr)


-- swap left and right subtree (of the root node only) of ast2
ast2' = Add (Sub (Num 0) (Num 2)) (Sub (Num 0) (Num 1))

-- the diagonal links after swapping subtrees of Add
os2LinksSwapLR =
  [ ( ( ExprArithS0 , [] ) , ( ExprArithV0 , [] ) )
  , ( ( ExprArithS1 , [ 1 ] ) , ( ExprArithV1 , [ 1 ] ) )
  , ( ( ExprArithS2 , [ 1 , 1 ] ) , ( Void , [ 1 , 0 ] ))
  , ( ( TermArithS2 , [ 1 , 1 , 1 ] ) , ( Void , [ 1 , 0 ] ))
  , ( ( FactorArithS1 , [ 1 , 1 , 1 , 1 ] ), ( FactorArithV1 , [ 1 , 0 ] ))
  , ( ( IntegerR , [ 1 , 1 , 1 , 1 , 1 ] ), ( IntegerR , [ 1 , 0 , 0 ] ))
  , ( ( TermArithS2 , [ 1 , 2 ] ), ( Void , [ 1 , 1 ] ))
  , ( ( FactorArithS1 , [ 1 , 2 , 1 ] ), ( FactorArithV1 , [ 1 , 1 ] ))
  , ( ( IntegerR , [ 1 , 2 , 1 , 1 ] ), ( IntegerR , [ 1 , 1 , 0 ] ))

  , ( ( TermArithS2 , [ 2 ] ) , ( Void , [ 0 ] ) )
  , ( ( FactorArithS0 , [ 2 , 1 ] ), ( FactorArithV0 , [ 0 ] ))
  , ( ( FactorArithS1 , [ 2 , 1 , 1 ] ), ( FactorArithV1 , [ 0 , 1 ] ))
  , ( ( IntegerR , [ 2 , 1 , 1 , 1 ] ), ( IntegerR , [ 0 , 1 , 0 ] ))
  ]



-- put with links meaning that the left and right subtrees of Sub (in the ast, root) changes
-- generate plus "comm+" (FromT "XT" (FromF "" (Neg "neg" (Lit "comm2" 2)))) 
--                       (FromF "DEF_VAL" (Paren "DEF_VAL" 
--                         (Minus "comm-" (FromT "" (FromF "" (Lit "comm0" 0)))
--                                        (FromF "" (Lit "comm1" 1)))))

testSwapAndPut = flip fromDyn ExprNull $
 topPut ExprTag ExprTag ArithTag (toDyn expr) (toDyn ast2') os2LinksSwapLR

-- put without links. the result has only default values without comments
-- Plus "DEF_VAL" (Minus "DEF_VAL" (FromT "DEF_VAL" (FromF "DEF_VAL" (Lit "DEF_VAL" 0))) (FromF "DEF_VAL" (Lit "DEF_VAL" 1))) (FromF "DEF_VAL" (Neg "DEF_VAL" (Lit "DEF_VAL" 2)))
testSwapAndPutWithoutLink = flip fromDyn ExprNull $
 topPut ExprTag ExprTag ArithTag (toDyn expr) (toDyn ast2') []



-----------------
-- use edit operations

(newAST, newHls) = swapNode ([0], [1]) (ast2, consisLinks)

-- This test returns True. It shows that the swap function correctly handle vertical correspondence,
-- which is further composed with the consistency links to get the input diagonal links
testDiagLinkAfterSwap = sort newHls == sort os2LinksSwapLR

testSwapUsingEdit = flip fromDyn ExprNull $
 topPut ExprTag ExprTag ArithTag (toDyn expr) (toDyn newAST) (newHls)

testSwapAndPut'' = testSwapAndPut == testSwapUsingEdit
