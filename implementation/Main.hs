-- CURRENTLY THIS MODULE DOES NOT WORK. USE Test/Test.hs

module Main where

import Def
import ParserInterface
import TypeInf
import GenProgram
import PrintDataTypes
import GenHelper.GenFetchable
import GenHelper.GenTypeable
import GenHelper.GenRegPat

import Data.Maybe
import Data.Map hiding (map, take, null)
import Text.Parsec
import Text.Show.Pretty
import qualified Language.Haskell.TH as TH

import System.Directory
import Text.PrettyPrint

import System.Environment
import Control.Monad
import System.IO.Unsafe
import Debug.Trace



main :: IO ()
main = putStrLn "Hello World"

-- main :: IO ()
-- main = do
--   args <- getArgs
--   if length args < 2
--     then error $ "not enough input arguments." +^+
--                  "please input data-type-def-file and program-file"
--     else do
--   let [typeDefFile, progFile] = args
--   typeDefStr <- readFile typeDefFile
--   progStr <- readFile progFile
--   case parse pAllDatatypesAndInterConvs "TypeDefinitionFile" typeDefStr of
--     Left  err -> pPrint err
--     Right (allTypes, intercvs) -> do
--       putStrLn (ppShow intercvs)
--       case parse pProgram "dataFile" progStr of
--         Left  err -> pPrint err
--         Right groups -> do
--           putStrLn (ppShow groups)
--           let env  = buildCon2FieldsEnv allTypes
--               groups' = map (\g@(Group (sTy, vTy) _) -> applyTyAnno sTy vTy env g) groups
--               prog    = foldr1 newlineSS $ map TH.pprint (genProgram groups')
--               tagTyDefs = TH.pprint $ genTypeTagDef (getTypes allTypes)
--           -- putStrLn prog
--           writeFile "Test/BXDef.hs" $ foldr1 newlineSS
--             [bxDefModHead
--             ,prtAbsDTs allTypes, tagTyDefs
--             ,genTagTypeableClass
--             ,genTagTypeableInstances allTypes
--
--             -- ,genSPatTagTagTypeableInstances groups'
--
--             ,genFetchableClass
--             ,genFetchableInstance allTypes
--
--             -- ,genSPatTagDef groups'
--             -- ,genVPatTagDef groups'
--             ]
--
--           writeFile "Test/Dekita.hs" $ foldr1 newlineSS
--             [bxModHead
--             ,prog
--             ]

-- head for BXDef Module
bxDefModHead :: String
bxDefModHead = foldr1 newlineS
  ["{-# LANGUAGE DeriveDataTypeable, TypeSynonymInstances, FlexibleInstances, MultiParamTypeClasses, ViewPatterns #-}"
  ,""
  ,"module BXDef where"
  ,""
  ,"import Prelude hiding (putChar)"
  ,"import Data.Data"
  ,"import Data.Generics hiding (GT)"
  ,"import Data.Dynamic"
  ,"import Data.List (partition, sortBy, nub)"
  ,"import Data.Map hiding (map, foldl, foldr, filter, null, drop, partition, take)"
  ,"import Data.Maybe (catMaybes, isJust, fromJust)"
  ,""
  ,"type Region = (RegPat, Path)"
  ,"type RLink = (Region, Region)"
  ,"type HLink = RLink"
  ,""
  ,"type VLink = (Path,RegPat,Path)"
  ,"type VCorr = VLink"
  ,""
  ,"type OSDyn = Dynamic"
  ,"type SDyn  = Dynamic"
  ,"type VDyn  = Dynamic"
  ,""
  ,"type OSTyTag = TyTag"
  ,"type STyTag  = TyTag"
  ,"type VTyTag  = TyTag"
  ,"type SupTyTag = TyTag"
  ,"type SubTyTag = TyTag"
  ,""
  ,"type Path = [Integer]"
  ,"type Env = [RLink]"
  ,"type RLinks = [RLink]"
  ,""
  ,pathFuns
  ,""
  ,"apFst f (x,y) = (f x , y)"
  ,""
  ]
--------

--
spliceDef :: String
spliceDef = foldr1 newlineS
  ["splice :: OSDyn -> RLink -> SDyn -> SDyn"
  ,"splice osDyn imag s0 = insSubtree sReg (fetch' (askDynTyTag osDyn) osDyn imag) s0"
  ,"  where"
  ,"    ((sReg,_),(Void,[])) = imag"]


-- functions handling paths and steps.
pathFuns = foldr1 newlineS
  ["-- functions handling paths and steps."
  ,""
  ,"-- compose a HLink and a VLink"
  ,"compHV :: RLink -> VCorr -> Maybe RLink"
  ,"compHV ((hRegL,hPathL),(hRegR,hPathR)) (vPathU,vReg,vPathB)"
  ,"  | hPathR == vPathU && hRegR == vReg = Just ((hRegL,hPathL),(hRegR,vPathB))"
  ,"compHV _ _ = Nothing"
  ,""
  ,"-- compose [VCorr] and [RLink]"
  ,"compHVs :: [RLink] -> [VCorr] -> [RLink]"
  ,"compHVs hls vls = nub $ catMaybes [hl `compHV` vl | hl <- hls, vl <- vls]"
  ,""
  ,"filterEnv :: Path -> [RLink] -> [RLink]"
  ,"filterEnv s = filter (\\ (_ , (_,p)) -> isPrefix s p)"
  ,""
  ,"filterVLinkS :: Path -> [VCorr] -> [VCorr]"
  ,"filterVLinkS p0 = filter (\\ (p, _, _) -> isPrefix p0 p)"
  ,""
  ,"filterVLinkE :: Path -> [VCorr] -> [VCorr]"
  ,"filterVLinkE p0 = filter (\\ (_, _, p) -> isPrefix p0 p)"
  ,""
  ,"isPrefix :: Path -> Path -> Bool"
  ,"isPrefix [] _ = True"
  ,"isPrefix (s1:ss1) (s2:ss2) | s1 /= s2 = False"
  ,"isPrefix (s1:ss1) (s2:ss2) | s1 == s2 = isPrefix ss1 ss2"
  ,"isPrefix _ _ = False"
  ,""
  ,"-- delete the given prefix path from all links"
  ,"delPathH :: (Path,Path) -> RLink -> RLink"
  ,"delPathH (sPre,vPre) ((sReg,sp),(vReg,vp)) ="
  ,"  ((sReg,delPrefix sPre sp) , (vReg,delPrefix vPre vp))"
  ,""
  ,"delPrefix :: Path -> Path -> Path"
  ,"delPrefix [] p = p"
  ,"delPrefix s1 s2 = if isPrefix s1 s2 then drop (length s1) s2"
  ,"  else error $ \"the first input path is not a prefix of the second \\n\" ++"
  ,"               \"input path1: \" ++ show s1 ++ \"\\n\" ++ \"input path2: \" ++ show s2"
  ,""
  ,"addPathV :: (Path,Path) -> VLink -> VLink"
  ,"addPathV (sPre1,sPre2) (sp1,sPat,sp2) = (sPre1 ++ sp1,sPat,sPre2 ++ sp2)"
  ,""
  ,"addPathH :: (Path,Path) -> RLink -> RLink"
  ,"addPathH (sPre,vPre) ((sReg,sp),(vReg,vp)) = ((sReg,sPre ++ sp) , (vReg,vPre ++ vp))"
  ,""
  ,"hasTopLink :: [RLink] -> Bool"
  ,"hasTopLink = ([] `elem`) . map (snd . snd)"
  ,""
  ,"-- get links with empty view paths (link at the top)"
  ,"-- return a Maybe real-link, a list of imaginary links (may be empty list), and remainings"
  ,"-- to do so, we first sort the HLinks in the following order:"
  ,"-- (3) : the links whose view-paths are empty come first"
  ,"-- (2) : among (3), whose view-patterns is Void come first."
  ,"-- (1) : among (2), whose source-paths are smaller come first."
  ,"getTopLinks :: [RLink] -> (Maybe RLink, [RLink], [RLink])"
  ,"getTopLinks hls ="
  ,"  let (candidates3,rem3) = partition (\\ (_,(_,vpath)) -> null vpath) hls"
  ,"      -- rem2 should be either empty or singleton"
  ,"      (candidates2,rem2) = partition (\\ (_,(vRegion,_)) -> vRegion == Void) candidates3 "
  ,"      candidates1 = sortBy cmpSPathRL candidates2"
  ,"  in  case rem2 of"
  ,"        []  -> (Nothing, candidates1, rem3)"
  ,"        [r] -> (Just r, candidates1, rem3)"
  ,""
  ,"cmpSPathRL :: RLink -> RLink -> Ordering"
  ,"cmpSPathRL ((_,sp1),_) ((_,sp2),_) = if sp1 < sp2 then LT"
  ,"  else if sp1 == sp2 then EQ else GT"
  ,""
  ,"-- build vertical correspondence between an AST and itself, according to the given consitency links"
  ,"-- do not use nub. nub will remove usefull links having the void region."
  ,"buildIdVCorr :: [RLink] -> [VCorr]"
  ,"buildIdVCorr consisHLS = [(path2,pat2,path2) | ((pat1,path1) , (pat2,path2)) <- consisHLS]"
  ]

-- head for BX Module
bxModHead :: String
bxModHead = foldr1 newlineS
  ["{-# LANGUAGE ViewPatterns #-}"
  ,"module BX where"
  ,""
  ,"import BXDef"
  ,"import Prelude hiding (putChar)"
  ,"import Data.Maybe (isJust, isNothing, fromJust)"
  ,"import Data.List (partition, sortBy, sort)"
  ,"import Data.Map hiding (map, foldl, foldr, filter, null, drop, partition, take)"
  ,"import Data.Dynamic"
  ,"import Text.Show.Pretty (pPrint, ppShow)"
  ]
