{-# LANGUAGE RecordWildCards #-}

module GenBX.GenPutNoLink where

import qualified Def as D
import ParserInterface
import TypeInf
import Data.Map as Map (Map, lookup)
import THAuxFuns
import GenHelper.GenRegPat

import Language.Haskell.TH

-- generate functions for walking down the view and recursively calling the put on subtrees.
-- since both the case put-handling-links and put-not-handling-links will call this case,
-- we give it a name as a new function to make the execution process clearer.
putNoLinkSig :: Dec
putNoLinkSig =
  let sig = SigD (mkName "putNoLink")
              (var2T "OSTyTag" `mkArrTy` var2T "STyTag"  `mkArrTy` var2T "VTyTag" `mkArrTy`
                con2T "RegPat" `mkArrTy` var2T "OSDyn" `mkArrTy` var2T "VDyn"
                  `mkArrTy` var2T "[RLink]" `mkArrTy` var2T "SDyn")
  in  sig

genPutNoLinkDef :: [D.Group] -> [Dec]
genPutNoLinkDef grps =
  let sRegPatEnv = genRegPatEnv grps
  in  map (\ D.Group{..} -> FunD (mkName "putNoLink")
                                 (map (genPutNoLinkCase sRegPatEnv gTy) gRules)) grps



-- generate a function for walking down the view and recursively calling the put on subtrees.
genPutNoLinkCase :: Map (D.Pattern,String,String) D.RegPatRep ->
                    (D.DataTypeRep,D.DataTypeRep) -> D.Rule -> Clause
genPutNoLinkCase sRegPatEnv (sTy,vTy) D.Rule{..} =
  Clause [var2P "osTy", con2P (D.printTyTag sTy), con2P (D.printTyTag vTy), var2P sRegPat
         ,var2P "osDyn", viewPV, var2P "env"] body decs
  where
    sRegPat = D.fromJ "error in genPutNoLinkCase. 1" $
                Map.lookup (normPattern lhs, D.printTyAsName sTy, D.printTyAsName vTy) sRegPatEnv
    viewPV = ViewP (ParensE $ var2E ("fromDynamic :: Dynamic -> Maybe (" ++ D.printTypeRep vTy ++ ")"))
                   patV
    patV = ConP (mkName "Just") [dslPat2HsPat' rhs]
    body = NormalB (if null (extractNonlinear bases)
             then newSrc
             else CondE (var2E "nonLinearCheck == True")
                        newSrc
                        (var2E $ "error \"ERROR. In the put direction, in the view, " ++
                                 "subtrees bound to nonlinear patterns differ.\""))

    newSrc = var2E "toDyn" `AppE`
             SigE (ParensE $ dslPat2HsExp (addPreToVar "res_" (useRHSNames bases lhs)))
                  (var2T $ D.printTypeRepWithParen sTy)
    decs = concatMap (genPutEachSubTree (lhs,rhs)) bases ++
           (if null (extractNonlinear bases)
                then []
                else [addNonlinear (lhs,rhs) bases])

    -- given a list of variables, generate a list containing variable names to vertical links
    -- e.g.:  [x ~ a, y ~ b, z ~ c] gives [xVl, yVl, zVl]
    subVLs :: [((D.Var,D.DataTypeRep),(D.Var,D.DataTypeRep))] -> Exp
    subVLs = foldr (\((x,_),(_,_)) (ListE vls) -> ListE (var2E (x ++ "vl'"):vls)) (ListE [])

-- mix the name of varaibles in lhs with their corresponding variable's names in rhs and their UID.
mixNames :: [(D.Pattern,D.Pattern)] -> D.Pattern -> D.Pattern
mixNames bases (D.ConP con ty subs) = D.ConP con ty (map (mixNames bases) subs)
mixNames bases (D.VarP suid svar ty) =
  let (vvar,vuid) = findCorVar svar bases
  in  D.VarP suid (svar ++ suid ++ vvar ++ vuid) ty
mixNames _ other = other

-- replace the varialbe names in an LHS with corresponding variable names in its RHS
useRHSNames :: [(D.Pattern,D.Pattern)] -> D.Pattern -> D.Pattern
useRHSNames bases (D.ConP con ty subs) = D.ConP con ty (map (useRHSNames bases) subs)
useRHSNames bases (D.VarP suid svar ty) =
  let (vvar,vuid) = findCorVar svar bases
  in  D.VarP vuid (vvar ++ vuid) ty
useRHSNames _ other = other

-- find the corresponding variable
findCorVar :: D.Var -> [(D.Pattern,D.Pattern)] -> (D.Var,D.UID)
findCorVar svar' ((D.VarP _ svar _ , D.VarP vuid vvar _):bb) | svar' == svar = (vvar,vuid)
findCorVar svar' (_:bb) = findCorVar svar' bb
findCorVar _ [] = error "FUCK"

-- group-view-type -> (lhs,rhs) of a declaration -> (a-var-in-lhs,a-var-in-rhs) -> [Dec]
genPutEachSubTree :: (D.Pattern,D.Pattern) -> (D.Pattern,D.Pattern) -> [Dec]
genPutEachSubTree (lhs,rhs) (D.VarP uidX x tyXRep , D.VarP uidY y tyYRep) =
  let svUIDCom = x ++ uidX ++ y ++ uidY
      -- for non-linear patterns, we also need uidY to generate unique name
      preXDec = simpleValD (var2P ("preS_" ++ y ++ uidY))
                  (listInt2Exp $ findVarPath (uidX,x) lhs)
      preYDec = simpleValD (var2P ("preV_" ++ y ++ uidY))
                  (listInt2Exp $ findVarPath (uidY,y) rhs)
      envXDec = simpleValD (var2P ("env_" ++ y ++ uidY)) genSubEnv

      putResTyDec = SigD (mkName ("res_" ++ y ++ uidY))
                      (var2T . D.wrapParen $ D.printTypeRep tyXRep)
      putResDec = simpleValD (var2P ("res_" ++ y ++ uidY))
                    (var2E "fromJust" `AppE` (foldl1 AppE [var2E "fromDynamic", resE1]))

      resE1 = foldl1 AppE
                [var2E "put", var2E "osTy"
                ,con2E . D.printTyTag . D.fromJ "cannot find the type of source variable. 0xa0." $
                  findVarType x lhs
                ,con2E . D.printTyTag . D.fromJ "cannot find the type of view variable. 0xa1." $
                  findVarType y rhs
                ,var2E "osDyn", var2E "toDyn" `AppE` var2E (y ++ uidY)
                ,var2E ("env_" ++ y ++ uidY)]

  in [preXDec,preYDec,envXDec,putResTyDec,putResDec]
  where
    genSubEnv :: Exp
    genSubEnv = foldl1 AppE
      [var2E "map"
      ,var2E "delPathH" `AppE` TupE [var2E "[]", var2E ("preV_" ++ y ++ uidY)]
      ,var2E "filterEnv" `AppE` var2E ("preV_" ++ y ++ uidY) `AppE` var2E "env"]


addNonlinear :: (D.Pattern,D.Pattern) -> [(D.Pattern,D.Pattern)] -> Dec
addNonlinear (lhs,rhs) bases =
  simpleValD
    (var2P "nonLinearCheck")
    (foldr1 (\s ss -> InfixE (Just s) (var2E "&&") (Just ss)) $ map genCheckDoc patsList)
  where
    patsList = extractNonlinear bases
    genCheckDoc :: [(D.Pattern,D.Pattern)] -> Exp
    genCheckDoc patsList =
      let resNames = map (\(D.VarP uidX x _ , D.VarP uidY y _) -> "res_" ++ y ++ uidY) patsList
      in  var2E "all" `AppE` (InfixE Nothing (var2E "==") (Just . var2E $ head resNames)) `AppE`
          ListE (map var2E $ tail resNames)

extractNonlinear :: [(D.Pattern,D.Pattern)] -> [[(D.Pattern,D.Pattern)]]
extractNonlinear bases =
  let svars = map (\(D.VarP _ v _) -> v) (fst $ unzip bases)
  in  filter (\a -> length a > 1) . map (preserveNonLinear bases) $ svars
  -- length > 1 means the variable appear more than once, i.e. a real non-linear pattern
  where
    preserveNonLinear [] _ = []
    preserveNonLinear ((D.VarP uidX x tyXRep , D.VarP uidY y tyYRep):bs) svar =
      if x == svar
        then (D.VarP uidX x tyXRep , D.VarP uidY y tyYRep) : preserveNonLinear bs svar
        else preserveNonLinear bs svar

--
genClausePutNoLink :: (D.DataTypeRep, D.DataTypeRep) -> D.Rule -> Clause
genClausePutNoLink  (sTy,vTy) D.Rule{..} =
  Clause [con2P sTyTag , var2P "sPat", originS , env, var2P "originalV"] body []
  where
    (originS , env) = (var2P "os", var2P "env")
    (sTyTag,vTyTag) = (D.printTyAsName sTy ++ "Tag",D.printTyAsName vTy ++ "Tag")
    patV = dslPat2HsPat' rhs
    body = GuardedB [(guard, e)]

    -- possible cases of the guard
    guard = NormalG (con2E "True")
    e = foldl1 AppE $ map var2E ["put" ++ D.printTyAsName vTy ++ "Rec"
                                ,sTyTag,"sPat","os","env", "originalV"]


---------- put for primitive types
primPutNoLink :: [Dec]
primPutNoLink = concatMap genP D.primTypes
  where
    genP ty =
      let resTy = TupleT 2 `AppT` var2T "Dynamic" `AppT` var2T "SSLinks"
      in  [FunD (mkName "putNoLink") [mkPrimPutNoLinkRec ty] ]


mkPrimPutNoLinkRec :: String -> Clause
mkPrimPutNoLinkRec ty = simpleClause
  [WildP, con2P (ty ++ "Tag"), con2P (ty ++ "Tag"), WildP, WildP, var2P "dynV", WildP]
  (var2E "dynV")


mkPrimPutNoLink :: String -> Clause
mkPrimPutNoLink ty = simpleClause
  [con2P (ty ++ "Tag"), var2P "sPat", var2P "os", var2P "env", var2P "v"]
  (foldl1 AppE [var2E ("put" ++ ty ++ "Rec"), var2E (ty ++ "Tag")
               ,var2E "sPat", var2E "os", var2E "env", var2E "v"])


mkPrimLinkSS :: String -> Exp
mkPrimLinkSS s = TupE [con2E "[]" , con2E (mkPrimPatReg s) , con2E "[]"]
