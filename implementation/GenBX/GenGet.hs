{-# LANGUAGE RecordWildCards #-}

module GenBX.GenGet where

import qualified Def as D
import TypeInf
import THAuxFuns
import GenHelper.GenRegPat
import Data.Map as Map hiding (map, foldr)
import Language.Haskell.TH
import Control.Monad.State
import Control.Arrow ((***))
import Data.Maybe (maybe)

import Debug.Trace
import Text.Show.Pretty

genGroupGet :: Map (D.Pattern,String,String) D.RegPatRep -> D.Group -> [Dec]
genGroupGet sRegPatEnv D.Group{..}  =
  let funName = "get" ++ D.printTyAsName (fst gTy) ++ D.printTyAsName (snd gTy)
      hsSTy = var2T (D.printTypeRep (fst gTy)) -- source type in haskell type rep
      hsVTy = var2T (D.printTypeRep (snd gTy))
      sig = SigD (mkName funName)
              (hsSTy `mkArrTy` (TupleT 2 `AppT` hsVTy `AppT` var2T "[RLink]"))
  in  [sig, FunD (mkName funName) (map (genClauseGet sRegPatEnv gTy) gRules)]


genClauseGet :: Map (D.Pattern,String,String) D.RegPatRep -> (D.DataTypeRep,D.DataTypeRep) -> D.Rule -> Clause
genClauseGet sRegPatEnv (sTy,vTy) D.Rule{..} = Clause [pat] body decs
  where
    pat  = dslPat2HsPat' lhs
    body = NormalB (TupE [dslPat2HsExp' rhs, lsE])
    decs = concatMap (genGetEachSubTree (lhs,rhs)) bases ++ [mkL0, hlUnion]

    lsE = simpleInfixE (var2E "l0") (var2E ":") (var2E "ls'")
    hlUnion = simpleValD (var2P "ls'")
              (var2E "concat" `AppE` subHLs bases)

    subHLs :: [(D.Pattern,D.Pattern)] -> Exp
    subHLs = foldr (\(D.VarP uidX x _ , D.VarP uidY y _) (ListE vls) ->
                        ListE (var2E (y ++ uidY ++ "ls'"):vls))
                   (ListE [])

    mkL0 = simpleValD (var2P "l0") (mkHLink sRegPatEnv (sTy,vTy) (lhs,rhs))

genGetEachSubTree :: (D.Pattern,D.Pattern) -> (D.Pattern,D.Pattern) -> [Dec]
genGetEachSubTree (sPat,vPat) (D.VarP uidX x tyX , D.VarP uidY y tyY) =
  let svUIDCom = x ++ uidX ++ y ++ uidY -- combination of varialbe name and uid
      getResDec = simpleValD
                    (TupP [VarP (mkName (y ++ uidY)), VarP (mkName (y ++ uidY ++ "ls")) ])
                    (VarE (mkName $ "get" ++ D.printTyAsName tyX ++ D.printTyAsName tyY)
                                  `AppE` (VarE (mkName (x ++ uidX))))

      preXDec = simpleValD (var2P ("preS_" ++ y ++ uidY)) (listInt2Exp $ findVarPath (uidX,x) sPat)
      preYDec = simpleValD (var2P ("preV_" ++ y ++ uidY)) (listInt2Exp $ findVarPath (uidY,y) vPat)

      xHL'Dec = simpleValD (var2P (y ++ uidY ++ "ls'"))
                (foldl1 AppE [var2E "map"
                             ,var2E "addPathH" `AppE` TupE [var2E ("preS_" ++ y ++ uidY)
                                                           ,var2E ("preV_" ++ y ++ uidY)]
                             ,var2E (y ++ uidY ++ "ls")])
  in [getResDec,preXDec,preYDec,xHL'Dec]




-- source-pat -> view-pat -> HLink
mkHLink :: Map (D.Pattern,String,String) D.RegPatRep -> (D.DataTypeRep,D.DataTypeRep) ->
           (D.Pattern, D.Pattern) -> Exp

-- imaginary link
mkHLink env (sTy,vTy) (lhs, D.VarP _ _ _) =
  let sRegPat = D.fromJ "error in MkLink. 2" $
                  Map.lookup (normPattern lhs, D.printTyAsName sTy, D.printTyAsName vTy) env
      t1 = TupE [con2E sRegPat, con2E "[]"]
      t2 = TupE [con2E "Void", con2E "[]"]
  in  TupE [t1, t2]


-- normal link
mkHLink env (sTy,vTy) (lhs, rhs) =
  let sRegPat = D.fromJ "error in MkLink. 1" $
                  Map.lookup (normPattern lhs, D.printTyAsName sTy, D.printTyAsName vTy) env
      vRegPat = D.fromJ "error in MkLink. 2" $
                  Map.lookup (normPattern rhs, D.printTyAsName sTy, D.printTyAsName vTy) env
      t1 = TupE [con2E sRegPat, con2E "[]"]
      t2 = TupE [con2E vRegPat, con2E "[]"]
  in  TupE [t1, t2]


-- no link
-- mkHLink env (sTy, vTy) = var2E "[]" -- or should we report an error ?


----------- get for primitive types
primGets :: [Dec]
primGets = map mkPrimGet D.primTypes

mkPrimGet :: String -> Dec
mkPrimGet ty = FunD (mkName' ty) [clause1]
  where mkName' n = mkName ("get" ++ n ++ n)
        clause1 = simpleClause [var2P "s"] (TupE [var2E "s", ListE [mkPrimLinkH ty]])

mkPrimLinkH :: String -> Exp
mkPrimLinkH ty = TupE [TupE [con2E (mkPrimPatReg ty), con2E "[]"]
                      ,TupE [con2E (mkPrimPatReg ty), con2E "[]"]]
------

-- top-level interface for "get". post-generate second-order properties after invoking the real get
topGet :: D.Group -> [Dec]
topGet D.Group{..}  =
  let funName = "topGet" ++ sTy ++ vTy
      hsSTy = var2T (D.printTypeRep (fst gTy)) -- source type in haskell type rep
      hsVTy = var2T (D.printTypeRep (snd gTy))
      (sTy, vTy) = (D.printTyAsName *** D.printTyAsName) gTy
      sig = SigD (mkName funName)
              (hsSTy `mkArrTy` (TupleT 2 `AppT` hsVTy `AppT` var2T "[RLink]"))
      body = NormalB (LetE [dec] (var2E "(view, rlinks)"))
      dec  = ValD (TupP [var2P "view", var2P "rlinks"]) decBody []
      decBody = NormalB $ var2E $ "get" ++ sTy  ++ vTy ++ " s"
  in  [sig, FunD (mkName funName) [Clause [var2P "s"] body []]]
