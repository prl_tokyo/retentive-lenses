{-# LANGUAGE RecordWildCards #-}

module GenBX.GenPutWithLinks where

import qualified Def as D
import ParserInterface
import TypeInf
import THAuxFuns
import GenBX.GenPutNoLink
import Language.Haskell.TH

import Data.Map as Map hiding (map, foldl, foldr, filter, null, drop, partition)
import Data.List (nub)
import Control.Monad.State
import Control.Arrow

-- putWithLinks :: OSTyTag -> STyTag -> VTyTag -> OSDyn -> VDyn -> Env -> SDyn
genPutWithLinksSig :: Dec
genPutWithLinksSig =
  let sig = SigD (mkName "putWithLinks")
              (var2T "OSTyTag" `mkArrTy` var2T "STyTag" `mkArrTy` var2T "VTyTag"
                `mkArrTy` var2T "OSDyn" `mkArrTy` var2T "VDyn" `mkArrTy`
                  var2T "[RLink]" `mkArrTy` var2T "SDyn")
  in  sig

genPutWithLinksDec :: [Dec]
genPutWithLinksDec =
  [FunD (mkName "putWithLinks") [genClausePutLink]]


genClausePutLink :: Clause
genClausePutLink =
  Clause (map var2P [ "osTy", "sTy", "vTy", "osDyn", "vDyn", "env"]) body decs
  where
    body = NormalB (var2E "s4")
    decs = [getLinksD, anaRealD, spliceD, mkInjD]
    getLinksD   = simpleValD (TupP [var2P "maybeL", var2P "imags", var2P "env'"])
                    (var2E "getTopLinks" `AppE` var2E "env")

    -- case analysis on real link
    anaRealD = ValD (var2P "s2")
                    (NormalB $ CaseE (var2E "maybeL") [hasReal, noReal]) []
    hasReal = Match (ConP (mkName "Just") [var2P "l"]) (NormalB hasRealBody) []
    hasRealBody = LetE [expandLD, fetchD, putNoLinkD] repSubE
    expandLD = simpleValD (TupP [TupP [var2P "sReg",var2P "sPath"]
                               ,TupP [WildP,var2P "[]"]])  (var2E "l")
    fetchD = simpleValD (var2P "s0") (var2E "fetch'" `AppE` var2E "osTy" `AppE` var2E "osDyn" `AppE` var2E "l")


    putNoLinkD = simpleValD (var2P "s1") putRecD2
    putRecD2 = var2E "putNoLink" `AppE` var2E "osTy" `AppE`
                 (ParensE $ var2E "askDynTyTag" `AppE` var2E "s0")
                 `AppE` var2E "vTy" `AppE` var2E "sReg" `AppE` var2E "osDyn" `AppE`
                 var2E "vDyn" `AppE` var2E "env'"

    repSubE = TupE [foldl1 AppE $ map var2E ["repSubtree","sReg","s0","s1"]]

    noReal  = Match (ConP (mkName "Nothing") []) (NormalB noRealBody2) []
    noRealBody2 = var2E "putNoLink" `AppE` var2E "osTy" `AppE` var2E "sTy" `AppE`
                    var2E "vTy" `AppE` selPat `AppE` var2E "osDyn" `AppE`
                    var2E "vDyn" `AppE` var2E "env'"

    selPat = ParensE $ var2E "selSPat" `AppE` var2E "sTy" `AppE` var2E "vTy"
            `AppE` var2E "vDyn"

    spliceD = simpleValD (var2P "s3")
                (foldl1 AppE (map var2E ["foldr", "(splice osDyn)", "s2", "imags"]))

    mkInjD = simpleValD (var2P "s4")
             (foldl1 AppE [var2E "mkInj", var2E "(askDynTyTag s3)", var2E "sTy", var2E "s3"])
