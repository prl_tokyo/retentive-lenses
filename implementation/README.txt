This folder contains the implementation of the DSL and the test cases in the paper.

There are five examples: expression, mirror, map, spine, and JavaSubset.
For each example, we include both the consistency relations written in our DSL and the corresponding generated lenses.

To load the test cases, switch the working directory to some testcase's folder (such as JavaSubset);
use `ghci` to load the corresponding xxxTestcases module (such as `ghci SimplifiedJavaTestcases.hs`) and run the functions defined in the test module.


If you want to generate source code on your own, use the Test module instead of the Main module to generate lenses from DSL program. But before that, please first run `make parsers` to generate necessary parser files.

We have not sorted out the code and the documentations.
