{-
Generate a default values from a type tag

class HasDefVal a where
  defVal :: TyTag -> a
-}

module GenVCorrHelper.GenDefVal where

import Def as D
import THAuxFuns
import TypeInf

import Data.Map (elems)
import Language.Haskell.TH as TH
import qualified Language.Haskell.TH.Syntax as TH (Pat (..))


genDefValClass = pprint genDefValClassDec
genDefValInstance crs decs = pprint $ genDefValInstanceDecs crs decs


genDefValClassDec :: Dec
genDefValClassDec = ClassD [] (mkName "HasDefVal") [PlainTV (mkName "a")] [] decs
  where  decs = [SigD (mkName "defVal")
                (foldr1 mkArrTy [var2T "TyTag", var2T "a"])]


genDefValInstanceDecs :: [D.Group] -> [D.DataTypeDec] -> [Dec]
genDefValInstanceDecs crs tyDecs =
  let instantiatedParTyReps = concat . elems $ instantiateAllParTyReps tyDecs crs
      simpleTyReps = concatMap (\(D.DataTypeDec t _) ->
                      if isParType t then [] else [t]) tyDecs
      allTyReps = instantiatedParTyReps ++ simpleTyReps ++ map (\t -> TyCon t []) D.primTypes

  in  [let className = var2T "HasDefVal" `AppT` var2T (D.printTypeRepWithParen tyRep)
           insDecs = [FunD (mkName "defVal")
                       [simpleClause [var2P (D.printTyTag tyRep)] (mkDefVal tyRep)]]
      in   InstanceD Nothing [] className insDecs | tyRep <- allTyReps]
