{-
Generate class and instances like:

class AllNodePath a where
  allNodePath :: a -> [(Dynamic, Path)]

instance AllNodePath Arith where
  allNodePath t@(Add t0 t1) =
    [(toDyn t, [])] ++
      map (\(node,path) -> (node, 0:path)) (allNodePath t0) ++
      map (\(node,path) -> (node, 1:path)) (allNodePath t1)
  allNodePath t@(Num t0) =
    [(toDyn t, [])] ++ map (\(node,path) -> (node, 0:path)) (allNodePath t0)

instance AllNodePath Integer where
  allNodePath i = [(toDyn i, [])]

-}
module GenVCorrHelper.GenNodeAndPath where

import Def as D
import THAuxFuns
import TypeInf

import Language.Haskell.TH as TH
import qualified Language.Haskell.TH.Syntax as TH (Pat (..))

genNodeAndPathClass = pprint genNodeAndPathClassDec
genNodeAndPathInstance crs decs = pprint $ genNodeAndPathInstanceDecs crs decs


genNodeAndPathClassDec :: Dec
genNodeAndPathClassDec = ClassD [] (mkName "NodeAndPath") [PlainTV (mkName "a")] [] decs
  where  decs = [SigD (mkName "nodeAndPath")
                (foldr1 mkArrTy [var2T "a", var2T "[(Dynamic, Path)]"])]


genNodeAndPathInstanceDecs :: [Group] -> [DataTypeDec] -> [Dec]
genNodeAndPathInstanceDecs crs tyDecs =
  let insTyDecs = instantiateAllParTyDecs crs tyDecs
  in  primIns ++ map genNodeAndPathInstanceDec insTyDecs
  where
    -- primitive types' instances
    primIns = map mkIns primTypes

    mkIns ty = InstanceD Nothing [] (mkFTy ty)
                 [FunD (mkName "nodeAndPath") [mkcl2 ty]]
    mkFTy ty = var2T "NodeAndPath" `AppT`  var2T ty
    mkcl2 ty = simpleClause [var2P "i"] (var2E "[(toDyn i, [])]")


genNodeAndPathInstanceDec :: DataTypeDec -> Dec
genNodeAndPathInstanceDec (DataTypeDec tyRep defs) =
  InstanceD Nothing [] ty [FunD (mkName "nodeAndPath") (map genClause defs)]
  where
    ty = var2T "NodeAndPath" `AppT`  var2T (printTypeRepWithParen tyRep)


genClause :: DataTypeDef -> Clause
genClause def = simpleClause [genLHSPat def] (genRHSExp def)


-- Add t1 t2 ... tn
genLHSPat :: DataTypeDef -> Pat
genLHSPat (DataTypeDef con fields) =
  AsP (mkName "t") $
    TH.ConP (mkName con) [var2P ("t" ++ show i) | (fd, i) <- zip fields [0..]]

genRHSExp :: DataTypeDef -> Exp
genRHSExp (DataTypeDef con fields) =
  foldl1 (\e es -> InfixE (Just e) (var2E "++") (Just es)) (self : process fields)
  where
    self = var2E "[(toDyn t, [])]"
    process fields =
      let serial = map snd $ zip fields [0..]
      in  [var2E ("map (\\(n,p) -> (n," ++ show i ++
                  ":p)) (nodeAndPath t" ++ show i ++ ")") | i <- serial]
