{-
For each user-defined datatype, generate a type class Insertable containing
a function ins that do the following thing:

Given a tree-to-be-updated and a path, insert (i.e. replace) a (sub)tree of a
given type at the path; return the new tree.

class Insertable a where
  ins :: (a, Path) -> (TyTag, Dynamic) -> a

instance Insertable Expr where
  ins (src, [p]) (tysubT,subT) = case (src, p) of
    (Add theHole t1, 0) -> Add (fromDyn subT ExprNull) t1
    (Add t0 theHole, 1) -> Add t0 (fromDyn subT ExprNull)
    -- Sub ...
  ins (src, p:ps) (tysubT,subT) = case (src, p) of
    (Add theHole t1, 0) -> Add (ins (theHole,ps) (tySubT, subT)) t1
    (Add t0 theHole, 1) -> Add t0 (ins (theHole,ps) (tySubT, subT))
    -- Sub ...


-}

module GenVCorrHelper.GenInsertable where

import Def as D
import THAuxFuns
import TypeInf

import Language.Haskell.TH as TH
import qualified Language.Haskell.TH.Syntax as TH (Pat (..))

-- import Data.List
import Data.Map hiding (foldr,map,null,filter)
import Data.Maybe (catMaybes)
import Data.List (nub)
import qualified Data.Map as Map
import Control.Monad.State

genInsertableClass = pprint genInsertableClassDec
genInsertableInstance crs decs = pprint $ genInsertableInstanceDecs crs decs

-- a class equipped with a function from data to its type
genInsertableClassDec :: Dec
genInsertableClassDec = ClassD [] (mkName "Insertable") [PlainTV (mkName "a")] [] decs
  where  decs = [SigD (mkName "ins")
                (foldr1 mkArrTy [var2T "(a, Path)", var2T "(TyTag, Dynamic)", var2T "a"])]


genInsertableInstanceDecs :: [Group] -> [DataTypeDec] -> [Dec]
genInsertableInstanceDecs crs tyDecs =
  let insTyDecs = instantiateAllParTyDecs crs tyDecs
  in  primIns ++ map genInsertableInstanceDec insTyDecs
  where
    -- primitive types' instances
    primIns = map mkIns primTypes

    mkIns ty = InstanceD Nothing [] (mkFTy ty)
                 [FunD (mkName "ins") [mkcl2 ty]]
    mkFTy ty = var2T "Insertable" `AppT`  var2T ty
    mkcl2 ty = simpleClause [TH.WildP,TH.WildP] (var2E "error" `AppE` LitE
                  (StringL $ "invalid path for insertion. Primitive types such as Integer is reached"))

-- replace parameterised data types with instantiated concrete types.
-- eg: a consistency relation List Int <---> List Char will produce from definition
-- (List a) both (List Int) and (List Char)


genInsertableInstanceDec :: DataTypeDec -> Dec
genInsertableInstanceDec (DataTypeDec tyRep defs) =
  InstanceD Nothing [] insTy [FunD (mkName "ins") [hereClause, goOnClause]]
  where
    insTy = var2T "Insertable" `AppT`  var2T (printTypeRepWithParen tyRep)

    goOnClause = simpleClause
                  [var2P "(src, p:ps)", var2P "(tySubT, subT)"] goOnCase
    goOnCase = if null goOnmatches
        then var2E "error $ \"path too long.\""
        else CaseE (TupE [var2E "src", var2E "p"]) goOnmatches
    goOnmatches = concatMap genGoOnMatches defs

    genGoOnMatches :: DataTypeDef -> [Match]
    genGoOnMatches def@(DataTypeDef con fields) =
      let rhsPat = genGoOnMatchesRHSPats def
          lhsPatPos = genMatchLHSPats def
      in  [Match (TupP [var2P lpat, var2P (show pos)]) (NormalB (var2E rpat)) []
            | ((lpat,pos),rpat) <- zip lhsPatPos rhsPat]

    hereClause = simpleClause
                  [(var2P "(src, [p])"), var2P "(tySubT, subT)"] hereCase
    hereCase = if null hereMatches
        then var2E "error $ \"path too long.\""
        else CaseE (TupE [var2E "src", var2E "p"]) hereMatches
    hereMatches = concatMap genHereMatches defs

    genHereMatches :: DataTypeDef -> [Match]
    genHereMatches def@(DataTypeDef con fields) =
      let rhsPat = genHereMatchRHSPats def
          lhsPatPos = genMatchLHSPats def
      in  [Match (TupP [var2P lpat, var2P (show pos)]) (NormalB (var2E rpat)) []
            | ((lpat,pos),rpat) <- zip lhsPatPos rhsPat]


-- get type variable lists
getTyVar :: D.DataTypeRep -> [String]
getTyVar (TyVar v)    = [v]
getTyVar (TyCon _ ts) = concatMap getTyVar ts

isParameterisedType :: D.DataTypeRep -> Bool
isParameterisedType (TyVar _) = True
isParameterisedType (TyCon _ []) = False
isParameterisedType (TyCon _ ts) = or $ map isParameterisedType ts


-- for each constructor, generate several cases
-- C1 (String,Char) (Maybe Int) Bool :: T1    gives   (C1 theHole t1 t2, 0)  ;
-- (C1 t0 theHole t1, 1)  ;  (C1 t0 t1 theHole, 2)
genMatchLHSPats :: DataTypeDef -> [(String, Int)]
genMatchLHSPats (DataTypeDef con fields) =
  map (\(_,i) -> (con +^+ mkInsPat 0 i (length fields), i)) (zip fields [0..])
  where
    mkInsPat :: Int -> Int -> Int -> String
    mkInsPat _ _ 0 = ""
    mkInsPat m 0 n = "theHole" +^+ mkInsPat (m+1) (-1) (n-1)
    mkInsPat m i n = "t" ++ show m +^+ mkInsPat (m+1) (i-1) (n-1)


-- for each constructor, generate several cases
-- C1 (String,Char) (Maybe Int) Bool :: T1   gives  (C1 (fromDyn subT T1Null) t1)  ;
-- (C1 t0 (fromDyn subT T1Null) t1)  ;  (C1 t0 t1 (fromDyn subT T1Null))
genHereMatchRHSPats :: DataTypeDef -> [String]
genHereMatchRHSPats (DataTypeDef con fields) =
  map (\(f,i) -> (con +^+ mkInsPat f 0 i (length fields))) (zip fields [0..])
  where
    mkInsPat :: DataTypeRep -> Int -> Int -> Int -> String
    mkInsPat _ _ _ 0 = ""
    mkInsPat tyRep m 0 n = (wrapParen "fromDyn subT theHole") +^+
                            mkInsPat tyRep (m+1) (-1) (n-1)
    mkInsPat tyRep m i n = "t" ++ show m +^+ mkInsPat tyRep (m+1) (i-1) (n-1)

--
genGoOnMatchesRHSPats :: DataTypeDef -> [String]
genGoOnMatchesRHSPats (DataTypeDef con fields) =
  map (\(_,i) -> (con +^+ mkInsPat 0 i (length fields))) (zip fields [0..])
  where
    mkInsPat :: Int -> Int -> Int -> String
    mkInsPat _ _ 0 = ""
    mkInsPat m 0 n = (wrapParen "ins (theHole,ps) (tySubT,subT)") +^+ mkInsPat (m+1) (-1) (n-1)
    mkInsPat m i n = "t" ++ show m +^+ mkInsPat (m+1) (i-1) (n-1)
