{-# LANGUAGE RecordWildCards #-}

module TypeInf where

import Def as D
import Data.Map as Map hiding (map, filter, null)
import Data.Maybe (catMaybes)
import Data.List (nub, sort, groupBy)
import Control.Monad.State
import Control.Monad.Reader
import Data.Generics

import Text.Show.Pretty
import Debug.Trace

-----------------------------------------------

applyTyAnno :: Data a => DataTypeRep -> DataTypeRep -> [DataTypeDec] -> a -> a
applyTyAnno grpSTy grpVTy tyDecs =
  everywhere' (mkT (addTypeAnno' grpSTy grpVTy tyDecs))


addTypeAnno' :: DataTypeRep ->     -- (group) source type
                DataTypeRep ->     -- (group) view type
                [DataTypeDec] ->    -- Type declarations
                Rule ->            -- data to be processed
                Rule
addTypeAnno' sTy@(TyCon sTyName _) vTy@(TyCon vTyName _) tyDecs Rule{..} =
  let substL = produceSubst sTy tyDecs
      tyDecL = applyTyVarSubsts' sTyName substL tyDecs

      lhs'   = if isVarPat lhs
                  then handleVarP lhs sTy
                  else addTypeAnno tyDecL (lhs, sTy)

      substR = produceSubst vTy tyDecs
      tyDecR = applyTyVarSubsts' vTyName substR tyDecs

      rhs'   = if isVarPat rhs
                  then handleVarP rhs vTy
                  else addTypeAnno tyDecR (rhs, vTy)
  in  Rule lhs' rhs' (updateBasis (lhs',rhs') bases)


-- substitutions of type variables to concrete data types
-- the first argument is the concrete data type appeared in consistency relations (e.g. source type)
-- the second argument is the data type declarations user defined before
produceSubst :: DataTypeRep -> [DataTypeDec] -> [(String,DataTypeRep)]
produceSubst (TyCon tyName []) _ = []
produceSubst (TyCon tyName ts) decs =
  let vars = findTyVars tyName decs
  in  zip vars ts

findTyVars :: String -> [DataTypeDec] -> [String]
findTyVars tyName [] = []
findTyVars tyName (DataTypeDec (TyCon tyName' vars) _ : decs) =
  if tyName == tyName'
    then map (\(TyVar v) -> v) vars
    else findTyVars tyName decs

-- instantiate parameterised type variables to concrete types and update the DataTypeDec
-- the first argument is a list of (Parameterised-TyVar, Concrete-Types) pair
applyTyVarSubsts :: [(String, DataTypeRep)] -> DataTypeDec -> DataTypeDec
applyTyVarSubsts [] dec = dec
applyTyVarSubsts (subst:substs) dec@(DataTypeDec (TyCon tyName _) _) =
  applyTyVarSubsts substs $ everywhere' (mkT (applyOneSubst tyName subst)) dec
  where
    applyOneSubst :: String -> (String, DataTypeRep) -> DataTypeRep -> DataTypeRep
    applyOneSubst _ (v, new) tv@(TyVar v') | v == v' = new
    applyOneSubst _ _ tyRep = tyRep


-- the substitution for one data type, replace type variables with concrete data types
-- e.g. user may define : data List a = ... ; data Tree a = ...
-- we only want to replace "a" with "Int" in the data type "List" but not "Tree"
applyTyVarSubsts' :: String -> [(String, DataTypeRep)] -> [DataTypeDec] -> [DataTypeDec]
applyTyVarSubsts' _ _ [] = []
applyTyVarSubsts' tyName substs (dec@(DataTypeDec (TyCon tyName' vars) _) : decs) =
  if tyName == tyName'
    then applyTyVarSubsts substs dec : decs
    else dec : applyTyVarSubsts' tyName substs decs


applyTyVarSubstsAndSelect :: String -> [(String, DataTypeRep)] -> [DataTypeDec] -> DataTypeDec
applyTyVarSubstsAndSelect tyName substs [] = error $
  "Impossible. Type variable substitutions failed.\n" ++
  "Type variable name: " ++ tyName ++ "\n" ++
  "Subst: " ++ show substs ++ "\n" ++
  "check if there are typos"
applyTyVarSubstsAndSelect tyName substs (dec@(DataTypeDec (TyCon tyName' vars) _) : decs) =
  if tyName == tyName'
    then applyTyVarSubsts substs dec
    else applyTyVarSubstsAndSelect tyName substs decs



updateBasis :: (Pattern,Pattern) -> [(Pattern,Pattern)] -> [(Pattern,Pattern)]
updateBasis (lhs,rhs) bases = map go bases
  where
    go (VarP uidL vL _ , VarP uidR vR _) =
      let tyL = fromJ ("error. Pattern:\n" ++ ppShow lhs ++ "\nmight be undefined in the data types")
                  (findVarType vL lhs)
          tyR = fromJ ("error. Pattern:\n" ++ ppShow rhs ++ "\nmight be undefined in the data types")
                  (findVarType vR rhs)
      in  (VarP uidL vL tyL, VarP uidR vR tyR)


-- non borderline cases
addTypeAnno :: [DataTypeDec] ->
               (Pattern, DataTypeRep) ->  -- data to be processed
               Pattern
addTypeAnno tyDecs (ConP consName _ pats , ty@(TyCon tyName _)) =
  -- produce and apply type varaible substitutions and build new env
  let subst   = produceSubst ty tyDecs
      tyDecs' = applyTyVarSubsts' tyName subst tyDecs
      env'    = buildCon2FieldsEnv tyDecs'
  -- lookup pats' types in the new env and recursively invoking addTypeAnno
      subTys' = case Map.lookup consName env' of
                  Nothing -> error $ "Cannot lookup a pattern's type. \n" ++
                             "Pattern's constructor name: " ++ show consName ++ "\n"
                  Just a -> fst a
      pats'   = map (addTypeAnno tyDecs') (zip pats subTys')
  in  ConP consName ty pats'

addTypeAnno tyDecs (VarP uid var _ , ty) = VarP uid var ty

addTypeAnno tyDecs (WildP _ , ty) = WildP ty

addTypeAnno _ (litP, _) = litP



-- the case when the view pattern is merely a variable and whose type should be
-- referred to the group view type
handleVarP :: Pattern     ->   -- data to be processed
              DataTypeRep ->   -- the type of the view (type of the group of actions)
              Pattern

handleVarP (VarP uid uv' _) vt = VarP uid uv' vt
handleVarP pat _  = pat



------------------------ lookup
-- accept variable name version
findVarType :: Var -> Pattern -> Maybe DataTypeRep
findVarType var p@(ConP _ ty subpats) =
  case catMaybes (map (findVarType var) subpats) of
    [ty] -> Just ty
    t:ts  -> case all (== t) ts of
    -- handle non-linear patterns
      True  -> Just t
      False -> error $ "variable [" ++ (init var) ++
                 "] occurres more than once in the pattern:\n" ++ show p ++ "\n" ++
                 "with different types"
    []   -> Nothing
findVarType var (VarP _ var' ty) =
  if var /= var' then Nothing else Just ty
findVarType _ _ = Nothing



-- use UID to search a variable's path, use Var(name) to do error reporting
findVarPath :: (UID,Var) -> Pattern -> [Int]
findVarPath (uid,v) p = case findVarPath' (uid,v) p [] of
  Nothing   -> error $ "variable " ++ v ++ " does not exist in pattern:\n" ++ ppShow p
  Just path -> path

findVarPath' :: (UID,Var) -> Pattern -> [Int] -> Maybe [Int]
findVarPath' (uid,v) (ConP _ _ subPats) p =
  let subPaths = zipWith (\a b -> a ++ [b]) (repeat p) [0,1..]
      subs     = zip subPats subPaths
  in  case catMaybes (map (\(pat,path) -> findVarPath' (uid,v) pat path) subs) of
        [p'] -> Just p'
        _:_  -> error $ "variable [" ++ (init v) ++ "] occurred more than once in a pattern."
                         ++ " In function findVarPath'"
        []   -> Nothing

findVarPath' (uid,v) (VarP uid' _ _) p = if uid /= uid' then Nothing else Just p
findVarPath' _ _ _ = Nothing


-- find a source-pattern's variable path, while the var pattern is guaranteed to be linear
-- useful for inter-convertible data types.
findLinearVarPath :: Var -> Pattern -> [Int]
findLinearVarPath v p = case findLinearVarPath' v p [] of
  Nothing   -> error $ "variable " ++ v ++ " does not exist in pattern:\n" ++ ppShow p
  Just path -> path

findLinearVarPath' :: Var -> Pattern -> [Int] -> Maybe [Int]
findLinearVarPath' var (ConP _ _ subPats) p =
  let subPaths = zipWith (\a b -> a ++ [b]) (repeat p) [0,1..]
      subs     = zip subPats subPaths
  in  case catMaybes (map (\(pat,path) -> findLinearVarPath' var pat path) subs) of
        [p'] -> Just p'
        _:_  -> error $ "variable [" ++ (init var) ++ "] occurred more than once in a pattern."
                         ++ " In function findLinearVarPath'"
        []   -> Nothing

findLinearVarPath' var (VarP _ var' _) p = if var /= var' then Nothing else Just p
findLinearVarPath' _ _ _ = Nothing


-------------------

-- type declarations with all the parameterised data types instantiated.
-- eg: remove  data List a = Nil | Cons (List a) and add  data List Int = Nil | Cons (List Int)
instantiateAllParTyDecs :: [Group] -> [DataTypeDec] -> [DataTypeDec]
instantiateAllParTyDecs crs tyDecs =
  -- instantiated parameterised types
  let insParTyReps = concat . elems $ instantiateAllParTyReps tyDecs crs
      notParTyDecs = filter (\(D.DataTypeDec t _) -> not (isParType t)) tyDecs
      parTyDecs = filter (\(D.DataTypeDec t _) -> isParType t) tyDecs
  in  [let substs = produceSubst insParTyRep parTyDecs
        in applyTyVarSubstsAndSelect c substs parTyDecs
      | insParTyRep@(TyCon c _) <- insParTyReps ] ++ notParTyDecs


-- instantiate all parameterised types with concrete types found from consistency relations
-- genrate a map from a type constructor to all the concrete instances. For example,
-- List ---> List Int, List (List Int), List Char
instantiateAllParTyReps :: [D.DataTypeDec] -> [D.Group] -> Map String [D.DataTypeRep]
instantiateAllParTyReps tyDecs crs =
  let parTyDecs = filter (\(D.DataTypeDec t _) -> isParType t) tyDecs
      userInstantiated = nub . sort . filter isParType $ D.getTypes crs
      res = nub .sort $ alreadyInstantiatedTypes tyDecs ++
            concatMap (\t -> evalState (instantiateParTypes t parTyDecs) []) userInstantiated
  in  unions . map (\ls@(D.TyCon c _:l) -> Map.singleton c ls) $
      groupBy (\(D.TyCon c1 _) (D.TyCon c2 _) -> c1 == c2) res



-- instantiate all the data type definitions with one given concrete type
instantiateParTypes :: D.DataTypeRep -> [D.DataTypeDec] -> State [D.DataTypeRep] [D.DataTypeRep]
instantiateParTypes concTy@(D.TyCon c _) parTyDecs = do
  let substs = produceSubst concTy parTyDecs
      (D.DataTypeDec tyRep' tyDefs') = applyTyVarSubstsAndSelect c substs parTyDecs
  modify (nub . sort . (:) tyRep' )
  done <- get
  if (extractParTypeFromTyDefs tyDefs') `isPrefix` done
    then return done
    else liftM concat . sequence $
            [instantiateParTypes subTyRep parTyDecs |
              D.DataTypeDef tcon subTyReps <- tyDefs',
              subTyRep <- subTyReps, isParType subTyRep]

-- extract instantiated parameterised types from the RHS of a data type definitions
extractParTypeFromTyDefs :: [D.DataTypeDef] -> [D.DataTypeRep]
extractParTypeFromTyDefs tyDefs = nub $ sort
  [tRep | D.DataTypeDef tcon subTyRep <- tyDefs
        , tRep@(D.TyCon c ts) <- subTyRep, not (null ts)]


-- instantiated parameterised types come from two parts: one is like Maybe Int in
-- the definition data T = C (Maybe Int).
-- the other is the real instantiation: data List a = Cons a (List a) and when we use (List Int)
alreadyInstantiatedTypes :: [D.DataTypeDec] -> [D.DataTypeRep]
alreadyInstantiatedTypes tyDecs = [tyRep |
  D.DataTypeDec _ tyDefs <- tyDecs, D.DataTypeDef _ tyReps <- tyDefs
  , tyRep <- tyReps, isAlreadyInstantiated tyRep]

isNotInstantiated :: DataTypeDec -> Bool
isNotInstantiated (DataTypeDec (TyCon _ pars) _) = any check pars
  where check (TyCon _ []) = False
        check (TyCon _ pars) = any check pars
        check (TyVar _ ) = True
isNotInstantiated _ = error "impossible. 0xC0"

isParType (D.TyCon _ []) = False
isParType (D.TyCon _ (_:_)) = True
isParType _ = False

isAlreadyInstantiated (D.TyCon _ []) = False
isAlreadyInstantiated (D.TyCon _ tvs) = and $ map isAlreadyInstantiated' tvs
isAlreadyInstantiated (D.TyVar _) = False

isAlreadyInstantiated' (D.TyCon _ []) = True
isAlreadyInstantiated' (D.TyCon _ tvs) = and $ map isAlreadyInstantiated' tvs
isAlreadyInstantiated' (D.TyVar _) = False

dropParametricTys :: [DataTypeDec] -> [DataTypeDec]
dropParametricTys = filter (not . isNotInstantiated)

isPrefix :: Eq a => [a] -> [a] -> Bool
isPrefix [] _ = True
isPrefix (s1:ss1) (s2:ss2) | s1 /= s2 = False
isPrefix (s1:ss1) (s2:ss2) | s1 == s2 = isPrefix ss1 ss2
isPrefix _ _ = False
