{
{-# LANGUAGE DeriveDataTypeable, ViewPatterns #-}
module Parsers.TyDefParser (tyDefParser, tyDefLexer) where


import Data.Char
import Data.Data
import Data.Map as Map (Map, insert, empty)
import Data.Typeable
import Def

}

%name tyDefParser
%tokentype { Token }
%error { tyParseError }

%monad { Maybe } { (>>=) } { return }

%token
  int             { TKInt    $$ }
  bool            { TKBool   $$ }
  string          { TKStr    $$ }
  char            { TKChar   $$ }
  var             { TKVar  $$ }
  cons            { TKCons $$ }
  '='             { TKEq }
  '|'             { TKOr }
  '('             { TKLParen }
  ')'             { TKRParen }
  'data'          { TKData }
  'type'          { TKType }
  '#InterConvertible' { TKInterConv }
  'instance'      { TKInstance }
  'InterConv'     { TKCInterConv }
  'where'         { TKWhere }
  'inj'           { TKInj }

%%

Defs :: { (Map DataTypeRep DataTypeRep, DataTypeDecs, InterConv) }
  : SynonymMap TypeDefs InterConv { ($1, $2, $3) }

SynonymMap :: { Map DataTypeRep DataTypeRep }
  : {- empty -}           { Map.empty }
  | TySynonym SynonymMap  { $1 $2 }

TySynonym :: { (Map DataTypeRep DataTypeRep -> Map DataTypeRep DataTypeRep) }
  : 'type' cons TyVars '=' cons TyFields  { Map.insert (TyCon $2 $3) (TyCon $5 $6) }

TypeDefs :: { DataTypeDecs }
  : TypeDef                  { [$1] }
  | TypeDef TypeDefs         { $1 : $2 }

TypeDef  :: { DataTypeDec }
  : 'data' cons TyVars TyLines  {DataTypeDec (TyCon $2 $3) $4}

TyVars :: { [DataTypeRep] }
  : {- empty -}  { [] }
  | var TyVars   { (TyVar $1) : $2}

TyLines :: { [DataTypeDef] }
  : TyLine         { [$1] }
  | TyLine TyLines { $1 : $2 }

TyLine :: { DataTypeDef }
  : '=' cons TyFields {DataTypeDef $2 $3}
  | '|' cons TyFields {DataTypeDef $2 $3}

TyFields :: { [DataTypeRep] }
  : {- empty -}    { [] }
  | TyField TyFields { $1 : $2}


TyField :: { DataTypeRep }
  : cons    { TyCon $1 [] }
  | var     { TyVar $1 }
  | '(' cons TyFields ')' { TyCon $2 $3 }


InterConv :: { InterConv }
  : {- -}                                { InterConv [] }
  | '#InterConvertible' InstanceDecls    { InterConv $2 }

InstanceDecls :: { [InjDecl] }
  : InstanceDecl                { [$1] }
  | InstanceDecl InstanceDecls  { $1 : $2 }


--   (sup, sub) / variable in lhs / the function to do the injection
InstanceDecl :: { InjDecl }
  : 'instance' 'InterConv' Sup Sub 'where' 'inj' var '=' ConsPat { InjDecl ($3,$4) $7 $9 }

Sup :: { DataTypeRep }
  :  cons                 { TyCon $1 [] }
  | '(' cons TyFields ')' { TyCon $2 $3 }

Sub :: { DataTypeRep }
  :  cons                 { TyCon $1 [] }
  | '(' cons TyFields ')' { TyCon $2 $3 }



ConsPat :: { Pattern }
  : cons          {ConP $1 (mkSimpleTyRep "UNDEFINED") []}
  | cons ConsArgs {ConP $1 (mkSimpleTyRep "UNDEFINED") $2}

ConsArgs :: { [Pattern] }
   : ConsArg          { [$1] }
   | ConsArg ConsArgs { $1 : $2}

ConsArg :: { Pattern }
  : Constants         { $1 }
  | var               { VarP "NO_UID" $1 (mkSimpleTyRep "UNDEFINED") }
  | cons              { (ConP $1 (mkSimpleTyRep "UNDEFINED") [])}
  | '(' cons ConsArgs ')'    { ConP $2 (mkSimpleTyRep "UNDEFINED") $3 }

Constants :: { Pattern }
  : int      {LitP (LitInt $1) }
  | bool     {LitP (LitBool $1) }
  | string   {LitP (LitString $1) }
  | char     {LitP (LitChar $1) }


{
tyParseError :: [Token] -> a
tyParseError a = error $ "Parse error. First 100 unconsumed tokens:\n" ++ show (take 100 a)


data Token = TKInt  Integer
           | TKBool Bool
           | TKStr  String
           | TKChar Char
           | TKVar  String
           | TKCons String
           | TKData
           | TKType
           | TKEq
           | TKOr
           | TKLParen
           | TKRParen
           | TKInterConv  -- keyword #InterConvertible
           | TKCInterConv -- class InterConv
           | TKInstance
           | TKIntConv
           | TKWhere
           | TKInj
  deriving (Show, Eq, Ord)


-- simple non-total tyDefLexer. does not support the case when variable names contain "instance"
tyDefLexer :: String -> [Token]
tyDefLexer [] = []
tyDefLexer ('-':'-':cs) = lexComment0 cs
tyDefLexer ('=':cs) = TKEq : tyDefLexer cs
tyDefLexer ('|':cs) = TKOr : tyDefLexer cs
tyDefLexer ('(':cs) = TKLParen : tyDefLexer cs
tyDefLexer (')':cs) = TKRParen : tyDefLexer cs
tyDefLexer ('d':'a':'t':'a':cs) | (isSpace . head $ cs) = TKData : tyDefLexer cs
tyDefLexer ('t':'y':'p':'e':cs) | (isSpace . head $ cs) = TKType : tyDefLexer cs
tyDefLexer ('#':'I':'n':'t':'e':'r':'C':'o':'n':'v':'e':'r':'t':'i':'b':'l':'e':cs) =
  TKInterConv : lexInterConv cs
tyDefLexer (c:cs)
      | isSpace c = tyDefLexer cs
      | isLower c = lexVar  (c:cs) tyDefLexer
      | isUpper c = lexCons (c:cs) tyDefLexer
tyDefLexer cs = error $ "non exhaustive patterns in tyDefLexer." ++
  "First untokenised 100 chars:\n" ++ take 100 cs

lexComment0 :: String -> [Token]
lexComment0 [] = []
lexComment0 (c:cs) | isNewline c = tyDefLexer cs
lexComment0 (c:cs) | otherwise = lexComment0 cs

findInstance :: String -> (String, String)
findInstance ('i':'n':'s':'t':'a':'n':'c':'e' : cs) | (isSpace . head $ cs) =
  ([], "instance" ++ cs)
findInstance (c:cs) =
  let (c1, cs1) = findInstance cs
  in  (c:c1, cs1)
findInstance [] = ([],[])

lexInterConv :: String -> [Token]
lexInterConv [] = []
lexInterConv ('-':'-':cs) = lexComment1 cs
lexInterConv ('(':cs) = TKLParen : lexInterConv cs
lexInterConv (')':cs) = TKRParen : lexInterConv cs
lexInterConv ('=':cs) =
  let (before,after) = findInstance cs
  in  TKEq : (lexInterConv before ++ lexInterConv after)
lexInterConv ('i':'n':'s':'t':'a':'n':'c':'e':cs) | (isSpace . head $ cs) = TKInstance : lexInterConv cs
lexInterConv ('I':'n':'t':'e':'r':'C':'o':'n':'v':cs) | (isSpace . head $ cs) = TKCInterConv : lexInterConv cs
lexInterConv ('w':'h':'e':'r':'e':cs) | (isSpace . head $ cs) = TKWhere : lexInterConv cs
lexInterConv ('i':'n':'j':cs) | (isSpace . head $ cs) = TKInj : lexInterConv cs
lexInterConv (c:cs)
      | isSpace c = lexInterConv cs
      | otherwise = lexData (c:cs)


lexComment1 :: String -> [Token]
lexComment1 [] = []
lexComment1 (c:cs) | isNewline c = lexInterConv cs
lexComment1 (c:cs) | otherwise = lexComment1 cs

isNewline :: Char -> Bool
isNewline '\r' = True
isNewline '\n' = True
isNewline _ = False

lexData (c:cs) | isUpper c  = lexCons (c:cs) lexInterConv
lexData (c:cs) | isLower c  = lexVar  (c:cs) lexInterConv
lexData (c:cs) | isNumber c = lexNum  (c:cs) lexInterConv
lexData ('"':cs)  = lexStr  cs lexInterConv
lexData ('\'':cs) = lexChar cs lexInterConv
lexData ('T':'r':'u':'e':cs)     = TKBool True : lexInterConv cs
lexData ('F':'a':'l':'s':'e':cs) = TKBool False : lexInterConv cs
lexData otherwise = error $ "lex error in the right-hand side of interconvertible declarations. unconsumed tokens: \n" ++ otherwise


lexStr :: String -> (String -> [Token]) -> [Token]
lexStr cs nextLexer =
  let (str, rest) = span (\c -> c /= '"') cs
  in  TKStr str : nextLexer (tail rest)


lexChar :: String -> (String -> [Token]) -> [Token]
lexChar (c:'\'':cs) nextLexer = TKChar c : nextLexer cs
lexChar _ _ = error "unsupported characters. (e.g.: escape)"


lexNum :: String -> (String -> [Token]) -> [Token]
lexNum cs nextLexer =
  let (i,rest) = span isNumber cs
  in  TKInt (read i :: Integer) : nextLexer rest

lexVar :: String -> (String -> [Token]) -> [Token]
lexVar cs nextLexer =
  let (var,rest) = span isAlphaNum cs
  in  TKVar var : nextLexer rest

lexCons :: String -> (String -> [Token]) -> [Token]
lexCons cs nextLexer =
  let (cons,rest) = span isAlphaNum cs
  in  TKCons cons : nextLexer rest

----------
mkSimpleTyRep :: String -> DataTypeRep
mkSimpleTyRep str = TyCon str []

}
