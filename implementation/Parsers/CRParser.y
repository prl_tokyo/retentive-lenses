{
{-# LANGUAGE DeriveDataTypeable, ViewPatterns #-}
module Parsers.CRParser (crParser, crLexer) where

import Data.Typeable
import Def

import Data.Char
import Data.Data
import Control.Monad.Reader
import Control.Monad.Trans.Maybe
import Data.Map as Map (lookup)

}

%name crParser
%tokentype { Token }
%error { crParseError }

%monad { MaybeT (Reader (ArityMap, ArityMap)) } { (>>=) } { return }

%token
  int             { TKInt    $$ }
  bool            { TKBool   $$ }
  string          { TKStr    $$ }
  char            { TKChar   $$ }
  var             { TKVar  $$ }
  cons            { TKCons $$ }
  '<--->'         { TKCRArrowTy }
  '~'             { TKCRArrowR  }
  ';'            { TKSemi }
  '_'             { TKWild }
  '('             { TKLParen }
  ')'             { TKRParen }


%%

Program :: { [Group] }
  : CRs      { $1 }

CRs :: { [Group] }
  : CR      { [$1] }
  | CR CRs  { $1 : $2 }

CR :: { Group }
  : Type '<--->' Type  InductRules ';' { Group ($1,$3) $4 }

Type :: {DataTypeRep}
 : cons TyFields {% do
   (tyArityMap, _) <- lift ask
   let argLen = length $2
       defLen = case (Map.lookup $1 tyArityMap) of
                  Nothing -> -1 -- -1 simply means this parse tree needs to fail
                  Just t  -> t
   if defLen == argLen
       then MaybeT . return $ Just (TyCon $1 $2)
       else MaybeT . return $ Nothing }

TyFields :: { [DataTypeRep] }
  : {- empty -}    { [] }
  | TyField TyFields { $1 : $2 }


TyField :: { DataTypeRep }
  : cons    { TyCon $1 [] }
  | var     { TyVar $1 }
  | '(' cons TyFields ')' { TyCon $2 $3 }


InductRules  :: { [Rule] }
  : InducRule              { [$1] }
  | InducRule InductRules  { $1 : $2 }


InducRule :: { Rule }
  : LHSPat '~' RHSPat     {Rule $1 $3 []}

LHSPat :: { Pattern }
  : ConstructorPat { $1 }

ConstructorPat :: { Pattern }
  : cons          { ConP $1 (mkSimpleTyRep "UNDEFINED") [] }
  | cons ConsArgs {% do
    (_ , consArityMap) <- lift ask
    let argLen = length $2
        defLen = case (Map.lookup $1 consArityMap) of
                   Nothing -> error $ "Data constructor " ++ $1 ++ " undefined."
                   Just t  -> t
    if defLen == argLen
        then MaybeT . return $ Just (ConP $1 (mkSimpleTyRep "UNDEFINED") $2)
        else MaybeT . return $ Nothing
  }

ConsArgs :: { [Pattern] }
  : ConsArg          { [$1] }
  | ConsArg ConsArgs { $1 : $2}

ConsArg :: { Pattern }
  : Constants         { $1 }
  | var               { VarP "NO_UID" $1 (mkSimpleTyRep "UNDEFINED") }
  | '_'               { WildP (mkSimpleTyRep "UNDEFINED") }
  | cons              { ConP $1 (mkSimpleTyRep "UNDEFINED") [] }
  | '(' cons ConsArgs ')'    { ConP $2 (mkSimpleTyRep "UNDEFINED") $3 }


RHSPat :: { Pattern }
  : Constants           { $1 }
  | ConstructorPat      { $1 }
  | var                 { VarP "NO_UID" $1 (mkSimpleTyRep "UNDEFINED") }


Constants :: { Pattern }
  : int      { LitP (LitInt $1) }
  | bool     { LitP (LitBool $1) }
  | string   { LitP (LitString $1) }
  | char     { LitP (LitChar $1) }

{

crParseError :: [Token] -> a
crParseError a = error $ "Parse error. Unconsumed 100 tokens: \n" ++ show (take 100 a)

data Token =
             TKInt  Integer
           | TKBool Bool
           | TKStr  String
           | TKChar Char
           | TKVar  String
           | TKCons String

           | TKCRArrowTy
           | TKSemi
           | TKCRArrowR
           | TKWild
           | TKLParen
           | TKRParen
  deriving (Show, Eq, Ord)


crLexer :: String -> [Token]
crLexer [] = []
crLexer ('-':'-':cs) = lexComment2 cs
crLexer ('_':cs)  = TKWild : crLexer cs
crLexer ('(':cs)  = TKLParen : crLexer cs
crLexer (')':cs)  = TKRParen : crLexer cs
crLexer ('~':cs)  = TKCRArrowR : crLexer cs
crLexer (';':cs)  = TKSemi : crLexer cs
crLexer ('<':'-':'-':'-':'>':cs) = TKCRArrowTy : crLexer cs
crLexer ('"':cs)  = lexStr  cs
crLexer ('\'':cs) = lexChar cs

crLexer (c:cs)
      | isSpace c = crLexer cs
      | otherwise = lexData (c:cs)


lexData ('T':'r':'u':'e':cs)     = TKBool True : crLexer cs
lexData ('F':'a':'l':'s':'e':cs) = TKBool False : crLexer cs
lexData (c:cs) | isUpper c = lexCons (c:cs)
lexData (c:cs) | isLower c = lexVar  (c:cs)
lexData (c:cs) | isNumber c = lexNum  (c:cs)
lexData cs = error $ "Unrecognised data. First untokenised 100 chars: \n" ++ take 100 cs


lexStr cs  =
  let (str,rest) = span (\c -> c /= '"') cs
  in  TKStr str : crLexer (tail rest)


lexChar (c:'\'':cs) = TKChar c : crLexer cs
lexChar _ = error "unsupported characters. (e.g.: escape)"

lexNum cs =
  let (i,rest) = span isNumber cs
  in  TKInt (read i :: Integer) : crLexer rest

lexVar cs =
  let (var,rest) = span isAlphaNum cs
  in  TKVar var : crLexer rest

lexCons cs =
  let (cons,rest) = span isAlphaNum cs
  in  TKCons cons : crLexer rest

lexComment2 :: String -> [Token]
lexComment2 [] = []
lexComment2 (c:cs) | isNewline c = crLexer cs
lexComment2 (c:cs) | otherwise = lexComment2 cs

isNewline :: Char -> Bool
isNewline '\r' = True
isNewline '\n' = True
isNewline _ = False

--------------
mkSimpleTyRep :: String -> DataTypeRep
mkSimpleTyRep str = TyCon str []

}
