{-# LANGUAGE RecordWildCards #-}

module GenProgram where

import qualified Def as D
import TypeInf
import THAuxFuns
import GenBX.GenGet
import GenBX.GenPutNoLink
import GenBX.GenPutWithLinks
import GenHelper.GenRegPat
import Language.Haskell.TH
import Language.Haskell.TH.Syntax

import Data.Generics hiding (GT)
import Data.Data
import Data.Map as Map hiding (map, foldr)
import Data.List (sortBy, sort, nub)
import Data.Typeable

import Debug.Trace

genProgram :: [D.Group] -> [Dec]
genProgram gs =
  let sRegPatEnv = genRegPatEnv gs
  in
      concatMap topGet gs ++
      concatMap (genGroupGet sRegPatEnv) gs ++
      primGets ++
      genPut (sortByVT gs) [] ++

      [putNoLinkSig] ++
      primPutNoLink ++
      genPutNoLinkDef (sortByVT gs) ++

      [genPutWithLinksSig] ++
      genPutWithLinksDec

-----------------------------


-- top-level interface for "put".
topPut :: String
topPut = foldr1 D.newlineSS
  ["topPut :: OSTyTag -> STyTag -> VTyTag -> OSDyn -> VDyn -> [RLink] -> SDyn"
  ,"topPut osTy sTy vTy osDyn vDyn hls = put osTy sTy vTy osDyn vDyn hls"
  ]

-- we want to generate a function "put" accepting source-type and view-type.
-- But it is not possible in Haskell because in this way the return type of
-- the function will depend on the input type. So we use tags instead of types as arguments.

-- The user may write consistency declarations that associate many source types
-- to a view type: SourceType1 ~ ViewType1 ;  SourceType2 ~ ViewType1 ; SourceType3 ~ ViewType1.
-- 1) We generate two branches for a case declared (for any type).
-- 2) A link may be connected to a source of any type that is declared above.
-- So all the branches generated from the same view type need to share the same
-- function name, because we must make sure that any branch of the put function
-- can be invoked (tried) by any case declared
genPut :: [D.Group] -> [String] -> [Dec]
genPut [] _ = []
genPut (D.Group{..}:ds) invalid =
  let funName = "put"
      sTy = var2T (D.printTypeRep (fst gTy))
      vTy = var2T (D.printTypeRep (snd gTy))
      sig = SigD (mkName funName)
              (var2T "OSTyTag" `mkArrTy` var2T "STyTag" `mkArrTy` var2T "VTyTag"
                `mkArrTy` var2T "OSDyn" `mkArrTy` var2T "VDyn" `mkArrTy`
                  var2T "[RLink]" `mkArrTy` var2T "SDyn")

      putNoLink = FunD (mkName funName) [Clause params nbody []]
      params   = map var2P ["osTy", "sTy", "vTy", "osDyn", "vDyn", "env"]
      nbody  = GuardedB [(nguard , nbody2)]
      nguard = NormalG (var2E "not" `AppE` (var2E "hasTopLink" `AppE` var2E "env"))
      nbody2 =  foldl1 AppE (map var2E ["putNoLink", "osTy", "sTy", "vTy", selPat, "osDyn", "vDyn", "env" ])
      selPat = "(selSPat sTy vTy vDyn)"

      putWithLinks = FunD (mkName funName) [Clause params lbody []]
      lbody  = GuardedB [(NormalG (var2E "hasTopLink" `AppE` var2E "env"), lbody2)]
      lbody2 =  foldl1 AppE (map var2E ["putWithLinks", "osTy", "sTy", "vTy", "osDyn", "vDyn", "env" ])
  in  [sig, putNoLink, putWithLinks]




-- sort the declarations group according to the ordering of the view type.
sortByVT :: [D.Group] -> [D.Group]
sortByVT = sortBy vtOrd
  where vtOrd (D.Group (_,ty1) _) (D.Group (_,ty2) _)
          | ty1 < ty2  = LT
          | ty1 == ty2 = EQ
          | ty1 > ty2 = GT
