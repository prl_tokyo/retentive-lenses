module ParserInterface where

import Def
import THAuxFuns (extractVars, extractVarAndUID, addPosToVar, addPreToVar)

import Parsers.TyDefParser      as TyParser
import Parsers.TyDefParserData
import Parsers.CRParser         as CRParser
import Parsers.CRParserData

import Data.Map as Map (Map, singleton, union, unions, lookup, toList)
import Data.Maybe (fromMaybe, catMaybes)
import Data.List (sort, nub)
import Control.Monad
import Control.Monad.State
import Control.Monad.Reader
import Control.Monad.Trans.Maybe
import Data.Generics
import Text.Show.Pretty

import Text.Parsec hiding (State)
import qualified Text.Parsec.Token as TPT

import Debug.Trace


forest_lookup :: Ord k => k -> Map k a -> a
forest_lookup f i = case Map.lookup f i of
  Nothing -> error "HEHE"
  Just a -> a

toSingleton :: a -> [a]
toSingleton x = [x]


-- type-def fileName -> consistency-relations fileName -> return...
parseAll :: String -> String -> IO (DataTypeDecs, InterConv, [Group])
parseAll dFile crFile = do
  defStr <- readFile dFile
  crStr  <- readFile crFile
  let tyToks = map toSingleton (tyDefLexer defStr)
      crToks = map toSingleton (crLexer crStr)

  case tyDefParser tyToks of
    TyParser.ParseEOF f  -> do
      error $ "Premature end of input when parsing type declarations :\n" ++
                 unlines (map show $ Map.toList f)

    TyParser.ParseError ts f -> do
      error $ "Error when parsing type declarations. " ++ "Number of results: " ++
                 show (length ts) ++ "\nDetails:" ++ ppShow ts

    TyParser.ParseOK r f -> do
      let mTyDefs = catMaybes (TyParser.decode (flip forest_lookup f) r
                     :: [Maybe (SynonymMap, DataTypeDecs, InterConv)])
          tyDefs = if length mTyDefs == 1
                    then head mTyDefs
                    else error ("Internal error: more than one parse results "
                                ++ "generated for type declarations.")

      let (synonymMap, decs, intercv) = tyDefs
          oTyArityMap = buildTyArityMap decs
          tyArityMap = oTyArityMap `union` buildSynonymArityMap synonymMap oTyArityMap
          consArityMap = buildConsArityMap decs

      -- parse consistency relations
      case crParser crToks of
        CRParser.ParseEOF f  -> do
          error $ "Premature end of input when parsing consistency relations :\n" ++
                     unlines (map show $ Map.toList f)

        CRParser.ParseError ts f -> do
          error $ "Error when parsing consistency relations. " ++
                     "Number of results: " ++ show (length ts) ++
                     "\nDetails:" ++ ppShow ts

        CRParser.ParseOK r f -> do

          let mCRs_ = (CRParser.decode (flip forest_lookup f) r
                         :: [MaybeT (Reader (ArityMap,ArityMap)) [Group]])

              mCRs1 = map (\res -> runReader (runMaybeT res) (tyArityMap,consArityMap)) mCRs_
              mCRs = trace ("Number of parse trees: " ++ show (length mCRs1)) $ catMaybes mCRs1
              -- mCRs = catMaybes $ map (\res -> runReaderT res (tyArityMap,consArityMap)) mCRs_
              crs = if length mCRs == 1
                        then head mCRs
                        else error ("Internal error: more than one parse results "
                                    ++ "generated for consistency relations. " ++
                                    "Number of results: " ++ show (length mCRs))

          let grps     = crs
              decs'    = replaceAllSynonyms synonymMap decs
              intercv' = replaceAllSynonyms synonymMap intercv
              grps'    = addBases (addUID (replaceAllSynonyms synonymMap grps))
          return (decs', intercv', grps')


replaceAllSynonyms :: Data a => SynonymMap -> a -> a
replaceAllSynonyms table = everywhere (mkT (replaceOneSynonym table))

-- replace type synonym by its definition
-- note that the replacement should be run several times to got the final type
-- e.g.: Type A = B; Type B = C. Then A should be evantually represetned by C but not B
replaceOneSynonym :: SynonymMap -> DataTypeRep -> DataTypeRep
replaceOneSynonym table old =
  case Map.lookup old table of
    Nothing -> old
    Just new -> case new of
      t@(TyCon con [])  -> replaceOneSynonym table t
      TyCon con tys -> let tys' = map (replaceOneSynonym table) tys
                       in  replaceOneSynonym table (TyCon con tys')
      t@(TyVar _) -> replaceOneSynonym table t


-- build arity environment for type constructors
buildTyArityMap :: DataTypeDecs -> ArityMap
buildTyArityMap decs = unions (map b decs) `union` unions (map c primTypes)
  where
    b :: DataTypeDec -> ArityMap
    b (DataTypeDec (TyCon ty ts) _) = Map.singleton ty (length ts)

    c ty = Map.singleton ty 0 -- Integer, String, Char, and Bool have zero arity

-- do not forget to build arity environment for type synonyms
buildSynonymArityMap :: SynonymMap -> ArityMap -> ArityMap
buildSynonymArityMap synMap oArityMap = unions $ do -- list monad
  (TyCon syn ts , _) <- Map.toList synMap
  -- let ori' = searchFinal ori synMap
  -- case Map.lookup (printTyCon ori') oArityMap of
  --   Nothing -> error $ "Type constructor " ++ printTyCon ori' ++ " undefined."
  --   Just arity -> return (Map.singleton syn arity) -- list monad
  return (Map.singleton syn (length ts))

-- -- type A = B, type B = C, type C = ... search for the final
-- searchFinal :: DataTypeRep -> SynonymMap -> DataTypeRep
-- searchFinal ty m =
--   case Map.lookup ty m of
--     Nothing -> ty
--     Just ty' -> searchFinal ty' m


-- build arity environment for data constructors
buildConsArityMap :: DataTypeDecs -> ArityMap
buildConsArityMap decs = unions (map b decs)
  where
    b  (DataTypeDec (TyCon _ _) defs) = unions (map bb defs)
    bb (DataTypeDef con fs) = Map.singleton con (length fs)

--
addBases :: [Group] -> [Group]
addBases = map (\(Group gTy rs) -> Group gTy (map addBasesEach rs))

addBasesEach :: Rule -> Rule
addBasesEach (Rule lhs rhs []) =
  if checkVars lhs rhs
    then let lhs' = addPosToVar "l" lhs
             rhs' = addPosToVar "r" rhs
             bases = mkBases (extractVarAndUID lhs') (extractVarAndUID rhs')
         in  Rule lhs' rhs' bases
    else error $ "invalid variable usage.\nLeft-hand side:\n" ++ show lhs ++
                 "\nRight-hand side:\n" ++ show rhs

-- check if variable usage is linear or simple non-linear
checkVars :: Pattern -> Pattern -> Bool
checkVars lhs rhs =
  let lhsVars = extractVars lhs
      rhsVars = extractVars rhs
  -- in  sort lhsVars == sort rhsVars &&
  in  length lhsVars <= length rhsVars &&
      nub lhsVars == lhsVars -- currently forbid non-linear vars in LHS
      -- && nub rhsVars == rhsVars


addUID :: [Group] -> [Group]
addUID grps = map goInto1 grps
  where
    goInto1 :: Group -> Group
    goInto1 (Group gTy gRules) = Group gTy (map goInto2 gRules)
    goInto2 :: Rule -> Rule
    goInto2 (Rule lhs rhs bases) =
      Rule (evalState (makeUID lhs) 0) (evalState (makeUID rhs) 0) bases

-- for each varaible in a pattern, generates its Unique-ID
makeUID :: Pattern -> State Int Pattern
makeUID (ConP con ty pats) = do
  pats' <- mapM makeUID pats
  return $ ConP con ty pats'
makeUID (VarP _ v ty) = do
  i <- get
  put (i + 1)
  return $ VarP (show i) v ty
makeUID (LitP  lit) = return $ LitP lit
makeUID (WildP ty)  = return $ WildP ty

mkBases :: [(Var,UID)] -> [(Var,UID)] -> [(Pattern,Pattern)]
mkBases ss vs = map (mkBase ss) vs

mkBase :: [(Var,UID)] -> (Var,UID) -> (Pattern,Pattern)
mkBase ss (vvar,vuid) = head
  [(VarP suid svar (mkTyRep "NO_TYPE" [])
   ,VarP vuid vvar (mkTyRep "NO_TYPE" [])) | (svar,suid) <- ss, init svar == init vvar]
  -- we have added postfix "l" to the variables on the LHS and "r" to the variables
  -- on the RHS. So we use init to ignore "l" and "r" when comparing their original names
