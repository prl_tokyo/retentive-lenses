{-
Definitions of edit operations. Put it in the same directory as the generated BX and BXDef module.

Useful library functions (contained in BXDef module):

-- ask the type-tag of a dynamic value.
askDynTyTag :: Dynamic -> TyTag

(there is a function  toDyn : a -> Dynamic  which wraps the given input into Dynamic
from the module Data.Dynamic)

-- return a default value according to the given type-tag
defVal :: TyTag -> a

-- compose consistency links and vertical correspondence:
compHVs :: [RLink] -> [VCorr] -> [RLink]

-}
module EditOperations where

import BX
import BXDef
import Data.Dynamic

import Data.List


-- Three core operations. Although swapNode are written in terms of replaceNode and copyNode,
-- however, there are many operations on vertical correspondence. So, we finally make it a core op.

-- (location, new-subtree-wrapped-in-Dynamic) ->
-- (tree-to-be-updated, vertical-correspondence) -> (new-tree, new-vcorr)
replaceNode :: (Typeable a, Insertable a, Fetchable a) =>
  (Path, Dynamic) -> (a, [HLink]) -> (a, [HLink])
replaceNode (p, dynSubT) (t, hls) =
  let tytag = askDynTyTag dynSubT
      t' = ins (t, p) (tytag, dynSubT)
      vcorr = buildIdVCorr hls
      vcorr' = delCorrHasPrefix p vcorr
  in  (t', hls `compHVs` vcorr')

-- (from-path, to-path) -> (tree, vertical-corr) -> (new-tree, newly-created-vertical-corr)
copyNode :: (Typeable a, Insertable a, Fetchable a) =>
  (Path, Path) -> (a, [HLink]) -> (a, [HLink])
copyNode (p1, p2) (t, hls) =
  let dynSub1 = fetch p1 t
      tytag = askDynTyTag dynSub1
      vcorr = buildIdVCorr hls
      newVCorr = establishNewCorr (p1,p2) vcorr
      newhls' = hls `compHVs` newVCorr

      (t', hls') = replaceNode (p2, dynSub1) (t, hls)
      -- delete invalid links originally connected to p2
  in  (t', sort (hls' ++ newhls')) -- newhls' and hls' should be disjoint

-- (path1, path2) -> (tree, vcorr) -> (new-tree, new-vcorr)
swapNode :: (Typeable a, Insertable a, Fetchable a, HasDefVal a) =>
  (Path, Path) -> (a, [HLink]) -> (a, [HLink])
swapNode (p1, p2) (t, hls) =
  let (t1, hls1) = copyNode (p1, p2) (t, hls)
      newhls1 = hls1 \\ hls
      (t2, hls2) = copyNode (p2, p1) (t, hls)
      newhls2 = hls2 \\ hls

      (dynSub1, dynSub2) = (fetch p1 t, fetch p2 t)
      (tytag1, tytag2) = (askDynTyTag dynSub1, askDynTyTag dynSub2)
      (t', hls')   = replaceNode (p1, dynSub2) (t, hls)
      (t'', hls'') = replaceNode (p2, dynSub1) (t',hls')

  in  (t'', sort (newhls1 ++ newhls2 ++ hls''))

-- (default-value-for-deleted-node, from-location, to-location) ->
-- (tree, vcorr) -> (new-tree, new-vcorr)
moveNode :: (Typeable a, Insertable a, Fetchable a, HasDefVal a) =>
  (Dynamic, Path, Path) -> (a, [HLink]) -> (a, [HLink])
moveNode (dynDefVal, p1, p2) (t, hls) =
  let (t', hls') = copyNode (p1, p2) (t, hls)
  in  replaceNode (p1, dynDefVal) (t', hls')  -- use replaceNode to destroy links


-- functions that create new or destroy existing vertical correspondence
-- When using edit sequences to transform ASTs, there is no need to consider regions (vpat below).
-- just do not touch any regions and change paths of links only

-- (prefix-of-vcorr-for-modification, new-prefix-of-vcorr-for-modification) ->
-- vcorr -> newly-created-vcorr
establishNewCorr :: (Path, Path) -> [VCorr] -> [VCorr]
establishNewCorr (oldPrefix, newPrefix) = concatMap
    (\(path1, vpat ,path2) -> case stripPrefix oldPrefix path1 of
        Nothing -> []
        Just postFix -> [(path1, vpat, newPrefix ++ postFix)])

--  (prefix-of-vcorr-for-deletion) -> vcorr -> new-vcorr
-- S means starting point
delCorrHasPrefix :: Path -> [VCorr] -> [VCorr]
delCorrHasPrefix prefix =
  filter (\(path1, vpat ,path2) -> not . isPrefix prefix $ path2)
