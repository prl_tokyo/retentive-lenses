{-
generate interconvertible data types injections for converting between them.

For example, if we have data types

data Expr =   ... | FromT Term
data Term =   ... | FromF Factor
data Factor = ... | Paren Expr

then we can use FromT to inject a value of type Term into a value of type Expr.
The value (Term) became the first subtree of an Expr, so we need to add a path [0].

class IntConv sub sup where
  inj :: sub -> sup

these declarations are written by users:

instance IntConv Expr Term where
  inj a = FromT "TT" a
instance IntConv Term Factor where
  inj a = FromF "FF" a
instance IntConv Factor Expr where
  inj a = Paren "EE" a
...

and we need to make these declarations transitive and generate:

instance IntConv Expr Factor  where
  inj a = FromT "TT" (FromF "TT" a)
...

-- also generate mkInj functions, because we need to handle data wrapped in Dynamic,
mkInj :: SubTyTag -> SupTyTag -> Dynamic -> Dynamic

mkInj ExprTag TermTag (s0, vls) | isJust (fromDynamic s0 :: Maybe Term) = toDyn s'
  where s' = inj $ fromJust (fromDynamic s0 :: Maybe Term) :: Expr
...

mkInj a b c | a == b = c

-}

module GenHelper.GenInterConv where

import Def as D
import Language.Haskell.TH hiding (report)
import THAuxFuns
import TypeInf (findVarPath, findLinearVarPath, produceSubst, applyTyVarSubsts', dropParametricTys)
import Data.List (nub, nubBy, union)
import Data.Maybe (catMaybes, isJust, fromJust)
import Control.Arrow
import Control.Monad.State
import Control.Monad.Writer

import Text.Show.Pretty
import Debug.Trace

report = flip trace

genAllInjDef :: [InjDecl] -> String
genAllInjDef decls =
  pprint $ genIntConvClassDec : map genIntConvInsDec decls ++
  [mkInjSig] ++ map genMkInjDecs decls ++ [idInjDec]

genIntConvClassDec :: Dec
genIntConvClassDec = ClassD [] (mkName "IntConv") tyVars [] [SigD (mkName "inj") sigTy]
  where sigTy = var2T "sub" `mkArrTy` var2T "sup"
        tyVars = [PlainTV (mkName "sub"), PlainTV (mkName "sup")]

genIntConvInsDec :: InjDecl -> Dec
genIntConvInsDec (InjDecl (sup,sub) var rhs) = InstanceD Nothing [] ty [dec]
  where
    ty  = var2T "IntConv" `AppT` var2T (printTypeRepWithParen sup) `AppT`
            var2T (printTypeRepWithParen sub)
    dec = FunD (mkName "inj") [simpleClause [var2P var] e]
    e   =  dslPat2HsExp rhs

mkInjSig :: Dec
mkInjSig =  SigD (mkName "mkInj") funTy
 where funTy = foldr1 mkArrTy [var2T "SubTyTag", var2T "SupTyTag"
                              ,var2T "Dynamic" ,var2T "Dynamic"]


-- we need to handle data wrapped in Dynamics. So we wrap the function inj
-- into mkInj to handle Dynamics.
genMkInjDecs :: InjDecl -> Dec
genMkInjDecs (InjDecl tyRepPair var rhs) =
  FunD (mkName "mkInj") [genMkInjDec tyRepPair]

genMkInjDec :: (DataTypeRep,DataTypeRep) -> Clause
genMkInjDec (sub,sup) = Clause pat (GuardedB [(guard,e)]) [w]
  where
    pat = [con2P (printTyTag sub)
          ,con2P (printTyTag sup), (var2P "s0")]
    guard = NormalG $ var2E "isJust" `AppE`
              (SigE (var2E "fromDynamic" `AppE` var2E "s0")
                    (con2T "Maybe" `AppT` con2T (printTypeRepWithParen sub)))
    e = var2E "toDyn" `AppE` var2E"s'"
    w = simpleValD (var2P "s'") (SigE
          (var2E "inj" `AppE`
            (var2E "fromJust" `AppE`
              (SigE (var2E "fromDynamic" `AppE` var2E "s0")
                    (con2T "Maybe" `AppT` con2T (printTypeRepWithParen sub)))))
          (con2T (printTypeRepWithParen sup)))

idInjDec :: Dec
idInjDec = FunD (mkName "mkInj") [Clause [var2P "a" , var2P "b", var2P "c"]
             (GuardedB [(NormalG $ simpleInfixE (var2E "a") (var2E "==") (var2E "b")
                       ,var2E "c")]) [] ]


-- insert a datatype rep (d) into a focus of another datatype rep indicated by path (p)
insHole :: (Pattern, Path) -> Pattern -> Pattern
insHole (d, []) _  = error "impossible"
insHole (d, p) (D.ConP con ty fields) =
  D.ConP con ty (insHole2 (d, p) fields)

-- insert to place
insHole2 :: (Pattern, Path) -> [Pattern] -> [Pattern]
insHole2 (d, [p]) ds =
  let (before, this, after) = (take p ds , ds !! p , drop (p + 1) ds)
  in  before ++ [d] ++ after
insHole2 (d, p:ps) ds =
  let (before, this, after) = (take p ds , ds !! p , drop (p + 1) ds)
  in  before ++ [insHole (d,ps) this] ++ after


--------------------------------------------------------------
{- Update. Try to automatically generate interconvertible data types from program
given

data Expr = ... | FromT Term
data Term = ... | Paren Expr

and

Expr <---> Arith
FromT t ~ t

Term <---> Arith
Paren e ~ t

we know that Expr and Term are inter convertible

In general, if (1) Ty1 and Ty2 share the same view type, and
(2) Ty1 and Ty2 are used to define some type (this type also includes either Ty1 and Ty2)
e.g.:  (Ty1 = ... Ty2 ...) or  (Ty2 = ... Ty1 ...) or (Ty3 = ... Ty2 ... (... Ty1 ...))
then they should be interconvertible
-}


-- extract inductive rules that are should be used to generate inter-convertible instances
--                (view-type) ---> list of (source-type, conv-rule)
type EquivTuple = (D.DataTypeRep, [(D.DataTypeRep, D.Rule)])

extractEquivTriples :: [D.Group] -> [EquivTuple]
extractEquivTriples grps =
  let vtys = gatherViewTypes grps
  in  filter (\(_, lst) -> not (null lst))
        (map (\ vty -> (vty,searchEquivSrc vty grps)) vtys)


-- view-type -> all groups -> selected (source-type,rule)
searchEquivSrc :: DataTypeRep -> [D.Group] -> [(D.DataTypeRep,D.Rule)]
searchEquivSrc tgtViewTy grps =
  let hit = filter (\(D.Group (sTy, vTy) _ ) -> vTy == tgtViewTy) grps
  in  -- trace (ppShow hit) $
      if length hit > 1
    then [(sTy,r) | (D.Group (sTy,_) rules) <- hit
                , Just r <- findConvRule (sTy,tgtViewTy) rules]

    else []

-- find the rule like FromT t ~ t
findConvRule :: (D.DataTypeRep,D.DataTypeRep) -> [D.Rule] -> [Maybe D.Rule]
findConvRule (sTy,vTy) rs =
  case filter (\(D.Rule lhs rhs _) -> D.isVarPat rhs) rs of
    [r] -> [Just r]
    []  -> [Nothing]

gatherViewTypes :: [D.Group] -> [D.DataTypeRep]
gatherViewTypes grps = nub $ map (\(D.Group (_, vTy) _) -> vTy) grps
---------------------------------------------

---------------------------------------------

genInterConvAutos :: [D.DataTypeDec] -> [D.Group] -> [D.InjDecl]
genInterConvAutos tyDecs grps =
  let eqvs = extractEquivTriples grps
      (injDecls,errMsgs) = (concat *** concat) . unzip $
                            map (\eqv -> genInterConvAuto eqv tyDecs) eqvs
  in  if null errMsgs
        then injDecls
        else report injDecls ("Warnings about interconvertible data types:\n" ++
                             foldr newlineS "" errMsgs)


-- firstly check if source data types are used together to define some other type
-- e.g.:  (Ty1 = ... Ty2 ...) or  (Ty2 = ... Ty1 ...) or (Ty3 = ... Ty2 ... (... Ty1 ...))
-- if not, filter it out
-- if so, for a pair (ty1, ty2), also make another pair (ty2, ty1) since they are inter-conv.
-- and generate its (interconv instance, wrappers). wrappers are for working with Dynamics
genInterConvAuto :: EquivTuple -> [D.DataTypeDec] -> ([D.InjDecl],[String]) -- [String] are errMsgs
genInterConvAuto (vTy, sTyRulePairs) tyDecs =
  let instantiatedDecs = dropParametricTys . foldr1 union $
                          (map (\(sTy,r) -> instantiateTyDef sTy tyDecs) sTyRulePairs)
      tyPairs = mkTyPairs (map fst sTyRulePairs)
      (pairsToGen,errMsgs1) = (concat *** concat) . unzip $ map checkAndMake tyPairs

      checkAndMake p =
        let (flag,err) = checkDefTopology p instantiatedDecs
        in  if flag then ([p, swap p],err) else ([],err)
      swap p = (snd p, fst p)

      (mInjdecls, states) = unzip $ map (\p -> runState (genOne p sTyRulePairs) ([],[])) pairsToGen
      errMsgs2 = concatMap snd states
      injdecls = catMaybes mInjdecls
  in  (injdecls, nub (errMsgs1 ++ errMsgs2))

-- instantiate a list of type declarations, by instanating type variables with concrete types
instantiateTyDef :: DataTypeRep -> [D.DataTypeDec] -> [D.DataTypeDec]
instantiateTyDef sTy@(TyCon sTyName _) tyDecs =
  let subst = produceSubst sTy tyDecs
  in  dropParametricTys $ applyTyVarSubsts' sTyName subst tyDecs

-- [a,b,c] ---> [(a,b),(a,c),(b,c)]
mkTyPairs :: [D.DataTypeRep] -> [(D.DataTypeRep,D.DataTypeRep)]
mkTyPairs [] = []
mkTyPairs (t:ts) = map (\t2 -> (t,t2)) ts ++ mkTyPairs ts


--------------------------------------------------------
-- check for all datatype decs
-- check if ty1 is used to define ty2, or if ty2 is used to define ty1,
-- or ty1 and ty2 are used to define ty3.
checkDefTopology :: (D.DataTypeRep,D.DataTypeRep) -> [D.DataTypeDec] ->
                    (Bool,[String]) -- [String] are error messages
checkDefTopology (ty1,ty2) allDecs =
  let res = map (\start -> evalState (searchAllCases (ty1,ty2) start allDecs) []) allDecs
      flags = map fst res
      errMsgs = concatMap snd res
  in (or flags, errMsgs)


searchAllCases :: (D.DataTypeRep,D.DataTypeRep) -> D.DataTypeDec -> [D.DataTypeDec] ->
                  State [D.DataTypeRep] (Bool,[String])
searchAllCases (ty1,ty2) start@(D.DataTypeDec ty defs) allDecs | ty1 == ty =
  search ty2 start allDecs   -- if ty2 is used to define ty1
searchAllCases (ty1,ty2) start@(D.DataTypeDec ty defs) allDecs | ty2 == ty =
  search ty1 start allDecs   -- if ty1 is used to define ty2
searchAllCases (ty1,ty2) start@(D.DataTypeDec ty defs) allDecs | otherwise = do
  -- if ty1 and ty2 are used to define some other ty
  (flag1, msg1) <- search ty1 start allDecs
  (flag2, msg2) <- search ty2 start allDecs
  case (flag1, flag2) of
    (True,True)   -> return (True, [])
    (False,False) -> return (False, [])
    (True,False)  -> return (False, msg2)
    (False,True)  -> return (False, msg1)


search :: D.DataTypeRep -> D.DataTypeDec -> [D.DataTypeDec] ->
          State [D.DataTypeRep] (Bool,[String])
search goal (D.DataTypeDec ty defs) allDecs | goal == ty = return (True, [])
search goal (D.DataTypeDec ty defs) allDecs | otherwise  = do
  discovered <- get
  let tys = concatMap collectTypes defs
      newTys = filter skipPrimTypes
               (filter (`notElem` discovered) tys)
  if goal `elem` newTys
    then return (True, [])
    else if null newTys
       then return (False, [])
       else do
        put (newTys ++ discovered)
        let (newStarts,errMsgs) = runWriter $ mapM (pickStart (goal,ty) allDecs) newTys
        (res, errMsgs') <- liftM unzip $ mapM (\s -> search goal s allDecs) (catMaybes newStarts)
        return (or res, nub (errMsgs ++ concat errMsgs'))

collectTypes :: D.DataTypeDef -> [D.DataTypeRep]
collectTypes (DataTypeDef _ tys) = tys

pickStart :: (D.DataTypeRep,D.DataTypeRep) -> [D.DataTypeDec] ->
             D.DataTypeRep -> Writer [String] (Maybe D.DataTypeDec)
pickStart (ft,tt) decs ty =
  case filter (\(DataTypeDec ty' _ ) -> ty' == ty) decs of
    [t] -> return $ Just t
    []  -> tell [errMsg] >> return Nothing
    _   -> error "0xB1.looks impossible." -- error (ppShow a)
  where errMsg = "The convertion from " ++ D.printTypeRep ft ++ " to " ++
                 D.printTypeRep tt ++ " is not available."

skipPrimTypes :: D.DataTypeRep -> Bool
skipPrimTypes ty = D.printTypeRep ty `notElem` D.primTypes
--------------------------------------------------------

-- generate Interconvertible declarations for one pair. shall be iterated many times
--      (toTy,fromTy) -> useful (ty,conv-rule) pairs -> State Discovered result
-- the result is InjDecl to be passed to the genAllInjsDef function in module GenInterConv.
genOne :: (D.DataTypeRep,D.DataTypeRep) -> [(D.DataTypeRep,D.Rule)] ->
          State ([D.DataTypeRep],[String]) (Maybe D.InjDecl) -- [String] are error msgs
genOne (t,f) sTyRulePairs = do
  case filter (\(sTy,_) -> sTy == t) sTyRulePairs of
    []  -> modify (\(dis,msg) -> (dis,errMsg:msg)) >> return Nothing
    [x] -> do
      (discovered, errMsgs) <- get
      let D.Rule lhs (D.VarP _ vv0 _) bases = snd x
          -- lhs should contain only one variable. find the type of the variable.
          lhsVarTy = case [t | (D.VarP _ sv t , D.VarP _ vv _) <- bases, vv == vv0] of
                      [t] -> t
                      _   -> error "0xB3. seems impossible."

          lhsVar = case [sv | (D.VarP _ sv t , D.VarP _ vv _) <- bases, vv == vv0] of
                    [sv] -> sv
                    _   -> error "0xB4. seems impossible."
      put (lhsVarTy : discovered , errMsgs)

      if f == lhsVarTy
        then return $ Just (InjDecl (f,t) lhsVar lhs) -- definition of InjDecl is (fromTy,toTy)
        else if lhsVarTy `notElem` discovered
          then do
            mRes <- genOne (lhsVarTy,f) sTyRulePairs
            case mRes of
              Just (InjDecl _ var' pat')  -> do
                let path = findLinearVarPath lhsVar lhs
                return $ Just (InjDecl (f,t) var' (insHole (pat', path) lhs))
              Nothing -> return Nothing
          else error "impossible. no way to converte data types that should be interconvertible."

    _   -> error "0xB2. seems impossible."
  where errMsg = "The convertion from " ++ D.printTypeRep f ++ " to " ++
                 D.printTypeRep t ++ " is not available."
