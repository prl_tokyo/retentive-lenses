{-# LANGUAGE RecordWildCards #-}

module GenHelper.GenSelSPat where

import qualified Def as D
import THAuxFuns
import GenHelper.GenRegPat
import Data.Map as Map (Map, lookup)
import Language.Haskell.TH

{-
select a function selecting source-pattern-tag depending on
source-type-tag, view-type-tag, and view

generate code like
selSPat :: TyTag -> TyTag -> Dynamic -> SPatTag
selSPat ExprTag ArighTag ((fromDynamic v :: Dynamic -> Maybe Arith) -> Add x y) = S_ExprArithCase0
selSPat ExprTag ArighTag ((fromDynamic v :: Dynamic -> Maybe Arith) -> Sub x y) = S_ExprArithCase1
selSPat ExprTag ArighTag ((fromDynamic v :: Dynamic -> Maybe Arith) -> a)       = S_ExprArithCase2

selSPat TermTag ArighTag ((fromDynamic v :: Dynamic -> Maybe Arith) -> Mul x y) = S_TermArithCase0
...
-}

genSelSPatDef = pprint . genSelSPat



genSelSPat :: [D.Group] -> [Dec]
genSelSPat grps =
  let regPatEnv = genRegPatEnv grps
      sigD = SigD (mkName "selSPat")
             (foldr1 mkArrTy (map con2T ["TyTag", "TyTag", "Dynamic", "RegPat"]))
      funDs = map (\ D.Group{..} -> FunD (mkName "selSPat")
                                    (map (genSelSPatClause regPatEnv gTy) gRules)) grps
      panic = FunD (mkName "selSPat")
              [Clause [var2P "x", var2P "y", var2P "z"]
              (NormalB (var2E ("error $ \"panic. source tag: \" ++ show x ++ "  ++
                        "\"\\nview tag \" ++ show y ++ \"\\nnot found!\\n\" ++ " ++
                        "show z"))) []]
  in  (sigD : funDs) ++ genSelSPatPrim ++ [panic]


genSelSPatClause :: Map (D.Pattern,String,String) D.RegPatRep ->
                    (D.DataTypeRep, D.DataTypeRep) -> D.Rule -> Clause
genSelSPatClause env (sTy, vTy) D.Rule{..}=
  Clause [con2P sTyTag , con2P vTyTag , viewPV] body []
  where
    -- sTags = elems sTagEnv ++ map mkPrimSPatTag  primTypes
    (sTyTag,vTyTag) = (D.printTyAsName sTy ++ "Tag" , D.printTyAsName vTy ++ "Tag")
    viewPV = ViewP (ParensE $ var2E ("fromDynamic :: Dynamic -> Maybe " ++
                                     D.printTypeRepWithParen vTy))
                   (ConP (mkName "Just") [dslPat2HsPat' rhs])

    sRegPat = D.fromJ "error in lookup region pattern tags.in genSelSPatClause" $
              Map.lookup (normPattern lhs, D.printTyAsName sTy, D.printTyAsName vTy) env
    body = NormalB (var2E sRegPat)

genSelSPatPrim :: [Dec]
genSelSPatPrim = map f D.primTypes
  where f ty =
          let tyRep = D.mkTyRep ty []
              tyTag = ty ++ "Tag"
              clause = Clause [con2P tyTag , con2P tyTag , viewPV] body []
              viewPV = ViewP (ParensE $ var2E ("fromDynamic :: Dynamic -> Maybe " ++ D.printTypeRep tyRep))
                             (ConP (mkName "Just") [var2P "prim"])

              body = NormalB (var2E patTag)
              patTag = ty ++ "R"
          in  FunD (mkName "selSPat") [clause]
