{-
For each user-defined datatype, generate a type class Fetchable containing
a function fetch that do the following thing:

Given a path and a tree, fetch a subtree in the tree according to the path.
Crucially, we do not know the type of the subtree, if the type information
is not encoded into the path.

fetch :: Path -> s -> Dynamic

-}

module GenHelper.GenFetchable where

import Def as D
import THAuxFuns
import TypeInf

import Language.Haskell.TH as TH
import qualified Language.Haskell.TH.Syntax as TH (Pat (..))

-- import Data.List
import Data.Map hiding (foldr,map,null)
import qualified Data.Map as Map
import Control.Monad.State

genFetchableClass = pprint genFetchableClassDec
genFetchableInstance = pprint . genFetchableInstanceDecs

-- a class equipped with a function from data to its type
genFetchableClassDec :: Dec
genFetchableClassDec = ClassD [] (mkName "Fetchable") [PlainTV (mkName "a")] [] decs
  where  decs = [SigD (mkName "fetch")
                (foldr1 mkArrTy [var2T "Path", var2T "a", var2T "Dynamic"])]


genFetchableInstanceDecs :: [DataTypeDec] -> [Dec]
genFetchableInstanceDecs decs = primIns ++ map genFetchableInstanceDec decs
  where
    -- primitive types' instances
    primIns = map mkIns primTypes

    mkIns ty = InstanceD Nothing [] (mkFTy ty)
                 [FunD (mkName "fetch") [cl1 , mkcl2 ty]]
    mkFTy ty = var2T "Fetchable" `AppT`  var2T ty
    cl1 = simpleClause [var2P "[]", var2P "src"] (var2E "toDyn" `AppE` var2E "src")
    mkcl2 ty = simpleClause [TH.WildP,TH.WildP] (var2E "error" `AppE` LitE
                  (StringL $ "invalid path for fetching a " ++ ty))


genFetchableInstanceDec :: DataTypeDec -> Dec
genFetchableInstanceDec (DataTypeDec tyRep defs) =
  InstanceD Nothing [] fetchTy [FunD (mkName "fetch") [hereClause, goOnClause]]
  where
    fetchTy = if not (isParameterisedType tyRep)
      then var2T "Fetchable" `AppT`  var2T (printTypeRepWithParen tyRep)
      else let tyVars = getTyVar tyRep
               tyVarClass = wrapParen . addComma $ concatMap (\t -> ["Fetchable" +^+ t, "Typeable" +^+ t]) tyVars
           in  var2T $ tyVarClass +^+ "=>" +^+ "Fetchable" +^+ (printTypeRepWithParen tyRep)

    hereClause = simpleClause [var2P "[]", var2P "src"] (var2E "toDyn" `AppE` var2E "src")
    goOnClause = simpleClause
              [InfixP (var2P "p") (mkName ":") (var2P "ps") , var2P "src"] case2
    matches = concatMap genMatch defs
    case2 = if null matches
        then var2E "error $ \"path too long.\""
        else CaseE (TupE [var2E "src", var2E "p"]) matches

    genMatch :: DataTypeDef -> [Match]
    genMatch def@(DataTypeDef con fields) =
      let triple = genFetchPatForEachCon def
      in  map genMatch2 triple

    genMatch2 :: (String, String, Int) -> Match
    genMatch2 (pat, var, pos) = Match p b []
      where p = TupP [var2P pat, var2P (show pos)]
            b = NormalB (foldl1 AppE [var2E "fetch", var2E "ps", var2E var])


genFetchDispatches :: [Group] -> String
genFetchDispatches = pprint . genFetchDispatchesDecs

genFetchDispatchesDecs :: [Group] -> [Dec]
genFetchDispatchesDecs grps =
  [SigD (mkName "fetch'") (foldr1 mkArrTy $ map var2T ["OSTyTag", "OSDyn", "RLink", "SDyn"])
  ,FunD (mkName "fetch'") (map genFetchDispatch (getTypes grps))
  ,FunD (mkName "fetch'") (map genFetchDispatchPrim primTypes)]


-- get type variable lists
getTyVar :: D.DataTypeRep -> [String]
getTyVar (TyVar v)    = [v]
getTyVar (TyCon _ ts) = concatMap getTyVar ts


isParameterisedType :: D.DataTypeRep -> Bool
isParameterisedType (TyVar _) = True
isParameterisedType (TyCon _ []) = False
isParameterisedType (TyCon _ ts) = or $ map isParameterisedType ts



-- fetch' :: OSTyTag -> OSDynamic -> HLink -> SDynamic
-- fetch' ExprTag (flip fromDyn ExprNull  -> os) l =
--  let ((sReg,sPath), (_,[])) = l
--  in fetch sPath os
genFetchDispatch :: DataTypeRep -> Clause
genFetchDispatch ty = Clause pats (NormalB body) []
  where
    pats = [var2P (D.printTyTag ty), viewOS, var2P "l"]
    body = LetE [d1] (var2E "fetch sPath os")
    d1 = simpleValD (TupP [TupP [var2P "sReg", var2P "sPath"]
                    ,TupP [TH.WildP, var2P "[]"]])
                    (var2E "l")
    viewOS = ViewP (ParensE sig) (TH.ConP (mkName "Just") [var2P "os"])
    sig    = SigE (var2E "fromDynamic")
                  (con2T "Dynamic" `mkArrTy`
                    (var2T "Maybe" `AppT` con2T (printTypeRepWithParen ty)))


genFetchDispatchPrim :: String -> Clause
genFetchDispatchPrim ty = Clause pats (NormalB body) []
 where
   pats = [var2P (ty ++ "Tag"), viewOS, var2P "l"]
   body = LetE [d1] (var2E "fetch sPath os")
   d1 = simpleValD (TupP [TupP [var2P "sReg", var2P "sPath"]
                   ,TupP [TH.WildP, var2P "[]"]])
                   (var2E "l")
   viewOS = ViewP (ParensE sig) (TH.ConP (mkName "Just") [var2P "os"])
   sig = SigE (var2E "fromDynamic")
              (con2T "Dynamic" `mkArrTy` var2T "Maybe" `AppT` con2T ty)


-- for each constructor, generate several cases
-- C1 (String,Char) (Maybe Int) Bool    gives
-- (C1 t0 _ _ , t0, 0)    ;   (C1 _ t1 _ , t1, 1)    ;   (C _ _ t2 , t2, 2)
genFetchPatForEachCon :: DataTypeDef -> [(String, String, Int)]
genFetchPatForEachCon (DataTypeDef con fields) =
  map (\(_,i) -> (con +^+ mkFetchPat i i (length fields), "t" ++ show i, i)) (zip fields [0..])

mkFetchPat :: Int -> Int -> Int -> String
mkFetchPat _  _ 0 = ""
mkFetchPat i0 0 n = "t" ++ show i0 +^+ mkFetchPat i0 (-1) (n - 1)
mkFetchPat i0 i n = "_" +^+ mkFetchPat i0 (i - 1) (n - 1)
