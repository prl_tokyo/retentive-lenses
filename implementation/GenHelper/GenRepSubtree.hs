{-
For each user-defined datatype, generate a type class Replaceable containing
a function repSubtree that do the following thing:

given s1 and s2 described by a pattern Add (Num 0) _ (ET y)
let Add (Num 0) a1 (ET y1) = s1
    Add (Num 0) a2 (ET y2) = s2

generate Add (Num 0) a1 (ET y2)

that is, replace all the subtrees in s1 with subtrees in s2 marked by variables.
leave other subtrees in s1 unchanged. (we need to handle Dynamic types)

generated code example:
repSubtree S_ExprArithCase0 (flip fromDyn ExprNull -> Plus fromWild0 la lb)
                            (flip fromDyn ExprNull -> Plus _ ra rb) =
                            toDyn (Plus fromWild0 ra rb)

repSubtree S_ExprArithCase1 (flip fromDyn ExprNull -> Minus fromWild0 la lb)
                            (flip fromDyn ExprNull -> Minus _ ra rb) =
                            toDyn (Minus fromWild0 ra rb)

repSubtree S_ExprArithCase2 (flip fromDyn ExprNull -> ET fromWild0 lt)
                            (flip fromDyn ExprNull -> ET _ rt) =
                            toDyn (ET fromWild0 rt)
...
repSubtree S_TermArithCase1
...
repSubtree S_Integer
repSubtree S_String
...

-}

{-# LANGUAGE RecordWildCards #-}

module GenHelper.GenRepSubtree where

import qualified Def as D
import THAuxFuns

import GenHelper.GenRegPat
import Language.Haskell.TH

import Data.Maybe (catMaybes)
import Data.Map hiding (foldr,map)
import qualified Data.Map as Map
import Control.Monad.State


genRepSubtreeDef :: Map (D.Pattern,String,String) D.RegPatRep -> [D.Group] -> String
genRepSubtreeDef sRegPatEnv = pprint . genRepSubtree sRegPatEnv

genRepSubtree :: Map (D.Pattern,String,String) D.RegPatRep -> [D.Group] -> [Dec]
genRepSubtree sRegPatEnv grps =
  let clauses = catMaybes $ concat $ evalState (mapM genC grps) []
  in  [SigD (mkName "repSubtree")
        (foldr1 mkArrTy (map con2T ["RegPat", "Dynamic", "Dynamic", "Dynamic"]))
      ,FunD (mkName "repSubtree") clauses] ++
      genPrimRepSubtree
  where
    genC :: D.Group -> State [(D.DataTypeRep,D.DataTypeRep,D.Pattern)] [Maybe Clause]
    genC (D.Group (sTy,vTy) rs) = mapM (genC2 (sTy,vTy)) rs

    genC2 :: (D.DataTypeRep,D.DataTypeRep) -> D.Rule ->
             State [(D.DataTypeRep,D.DataTypeRep,D.Pattern)] (Maybe Clause)
    genC2 (sTy,vTy) D.Rule{..} = do
      -- to avoid generate duplicated cases, we normalise the lhs pattern and
      -- see if we have already generated a case for it
      let lhs' = normPattern lhs
      done <- get
      if (sTy,vTy,lhs') `elem` done
        then return Nothing
        else do
          let patTag = D.fromJ "cannot find pattern. in genRepSubtree" $
                         Map.lookup (lhs', D.printTyAsName sTy, D.printTyAsName vTy) sRegPatEnv
              vExp = ParensE $ var2E ("fromDynamic :: Dynamic -> Maybe " ++
                                        D.printTypeRepWithParen sTy)

              body = NormalB (var2E (D.printTyAsName sTy ++ "Tag"))

              s1Pat = ConP (mkName "Just")
                           [dslPat2HsPat' $ evalState (renameWildP (addPreToVar "l" lhs)) 0]
              -- exp in view-pattern
              s1PatV = ViewP vExp s1Pat

              s2Pat = ConP (mkName "Just")
                           [dslPat2HsPat' $ addPreToVar "r" lhs]
              s2PatV = ViewP vExp s2Pat
              resExp = var2E "toDyn" `AppE`
                         (SigE (dslPat2HsExp' $ evalState (renameWildP (addPreToVar "r" lhs)) 0)
                               (var2T $ D.printTypeRepWithParen sTy))
          put ((sTy,vTy,lhs') : done)
          return $ Just (Clause [var2P patTag, s1PatV, s2PatV] (NormalB resExp) [])


genPrimRepSubtree :: [Dec]
genPrimRepSubtree = map go D.primTypes
  where go ty = FunD (mkName "repSubtree")
                  [(Clause [var2P (ty ++ "R"), var2P "dynS1", var2P "dynS2"]
                          (NormalB (var2E "dynS1")) [])]
