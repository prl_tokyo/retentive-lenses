{-

For each user-defined datatype, generate a tag representing the type.
Types in Haskell are not first class citizen, so we need some representation
(tags) to denote types.

data TyTag =
    ExprTag
  | TermTag
  | FactorTag
  | ArithTag


Also generate a type class TypeTag and instances for each data type
containing functions "tyTag" for mapping data to its typeTag.

class TypeTag a where
 tyTag :: a -> TyTag

instance TypeTag Arith where
  tyTag (Add _ _) = ArithTag
  tyTag (Sub _ _) = ...

-}

module GenHelper.GenTypeable where

import qualified Def as D
import THAuxFuns
import GenHelper.GenRegPat
import GenVCorrHelper.GenInsertable
import TypeInf

import Language.Haskell.TH
import Data.List (nub, sort, groupBy)
import Data.Map hiding (foldr, map, foldl, filter, null)
import qualified Data.Map as Map

import Control.Monad.State
import Debug.Trace
import Text.Show.Pretty

-------------------------
-- generate datatype declaration of TypeTags from D.[DataTypeDec]
-- genTyTagDef' :: [D.DataTypeDec] -> [D.Group] -> Dec
-- genTyTagDef' tyDecs crs = genTyTagDef

-- generate datatype declaration of TypeTags from [DataTypeRep]
-- each user-defined datatype and primitive type will be assigned one tag.
genTyTagDef :: [D.Group] -> [D.DataTypeDec] -> Dec
genTyTagDef crs tyDecs =
  let instantiatedParTyReps = concat . elems $ instantiateAllParTyReps tyDecs crs
      simpleTyReps = concatMap (\(D.DataTypeDec t _) ->
                      if isParType t then [] else [t]) tyDecs
      tags = [D.printTyAsName tyRep ++ "Tag" |
                tyRep <- instantiatedParTyReps ++ simpleTyReps] ++ primTags
      primTags = map mkPrimTyTag D.primTypes
  in  simpleDataD "TyTag"
        (map (\tg -> NormalC (mkName tg) []) tags)
        [var2T "Eq", var2T "Show"]


---------------------------


-- from here, generate type class and instances
genTyTagClass = pprint genTyTagClassDec
genTyTagInstances tyDecs crs = pprint $ genTyTagInstanceDecs tyDecs crs


-- a class equipped with a function from data to its type
genTyTagClassDec :: Dec
genTyTagClassDec = ClassD [] (mkName "TypeTag") [PlainTV (mkName "a")] [] decs
  where  decs = [SigD (mkName "tyTag") (var2T "a" `mkArrTy` var2T "TyTag")]

-- only generate instances for concrete data types appeared in concsistency relations
-- and, do not generate redundant ones
genTyTagInstanceDecs :: [D.DataTypeDec] -> [D.Group] -> [Dec]
genTyTagInstanceDecs tyDecs crs =
  let insTyDecs = instantiateAllParTyDecs crs tyDecs
  in  map genPrimTyTagInstance D.primTypes ++
      [let tyTagDecs = tyTagNullCase tyRep : map (genDef tyRep) tyDefs
           tyTag = var2T "TypeTag" `AppT` var2T (D.printTypeRepWithParen tyRep)
       in  InstanceD Nothing [] tyTag tyTagDecs
        | D.DataTypeDec tyRep tyDefs <- insTyDecs]




genDef :: D.DataTypeRep -> D.DataTypeDef -> Dec
genDef tyRep (D.DataTypeDef con tys) = FunD (mkName "tyTag")
         [simpleClause [genTyTagPat con tys]
           (ConE (mkName (D.printTyAsName tyRep ++ "Tag")))]

genTyTagPat :: String -> [a] -> Pat
genTyTagPat con tys = ConP (mkName con) (map (const WildP) tys)

tyTagNullCase :: D.DataTypeRep -> Dec
tyTagNullCase tyRep = FunD (mkName "tyTag")
                        [simpleClause [var2P . D.addNullCase $ tyRep]
                        (var2E . mkPrimTyTag . D.printTyAsName $ tyRep)]

addIfNotExist :: Eq a => a -> [a] -> [a]
addIfNotExist a as = if a `elem` as then as else a : as

genPrimTyTagInstance :: String -> Dec
genPrimTyTagInstance ty = InstanceD Nothing [] (var2T "TypeTag" `AppT`  var2T  ty)
  [FunD (mkName "tyTag") [simpleClause [WildP]  (var2E (mkPrimTyTag ty))]]



-------------
-- tags for primitive types
mkPrimTyTag :: String -> String
mkPrimTyTag s = s ++ "Tag"



genAskDynTyTagDef :: [D.Group] -> [D.DataTypeDec] -> String
genAskDynTyTagDef crs tyDecs = pprint $ genAskDynTyTag crs tyDecs

genAskDynTyTag :: [D.Group] -> [D.DataTypeDec] -> [Dec]
genAskDynTyTag crs tyDecs =
  let tyReps = map (\(D.DataTypeDec tRep _) -> tRep) $
                instantiateAllParTyDecs crs tyDecs
  in  [sig, FunD (mkName "askDynTyTag")
              (map genAskDynTyTag2 tyReps ++
               map (\t -> genAskDynTyTag2 $ D.mkTyRep t []) D.primTypes)]
  where sig = SigD (mkName "askDynTyTag") (var2T "Dynamic" `mkArrTy` var2T "TyTag")

genAskDynTyTag2 :: D.DataTypeRep -> Clause
genAskDynTyTag2 sTy = Clause [viewPV] body []
  where
    viewPV = ViewP (ParensE $ var2E ("fromDynamic :: Dynamic -> Maybe " ++
                              D.printTypeRepWithParen sTy))
               (ConP (mkName "Just") [WildP])
    body = NormalB (var2E (D.printTyAsName sTy ++ "Tag"))
