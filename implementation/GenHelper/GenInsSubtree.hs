{-# LANGUAGE RecordWildCards #-}
module GenHelper.GenInsSubtree where

{-
insert a subtree into the ONLY hole in a given tree
generate code like:

insSubtree :: SPatTag -> Dynamic -> Dynamic -> Dynamic
insSubtree (S_ExprArithCase2)
          ((fromDynamic :: Dynamic -> Maybe Expr) -> Just (ET fromWild0 theHole))
          s0 = ET fromWild0 s1
 where s1 = mkInj (tyTag theHole) (askDynTyTag s0) s0
-}

import qualified Def as D
import THAuxFuns
import GenBX.GenPutNoLink
import GenHelper.GenRegPat
import GenHelper.GenInterConv
import Language.Haskell.TH
import TypeInf (findVarPath, findLinearVarPath)

import Control.Monad.State
import Data.Map as Map hiding (map, foldl, foldr, filter, null, drop, partition, take)
import Debug.Trace
import Text.Show.Pretty

genInsSubtreeDef :: Map (D.Pattern,String,String) D.RegPatRep -> [D.Group] -> String
genInsSubtreeDef sRegPatEnv grps =
  let defs = genInsSubtreeDec sRegPatEnv grps
  in  if null defs
        then "insSubtree = undefined -- no need to define it since it will not be invoked."
        else pprint genInsSubtreeSig ++ "\n" ++ pprint defs


genInsSubtreeSig :: Dec
genInsSubtreeSig = SigD (mkName "insSubtree")
  (var2T "RegPat" `mkArrTy` dynT  `mkArrTy` dynT `mkArrTy` dynT)
  where dynT = var2T "Dynamic"

genInsSubtreeDec :: Map (D.Pattern,String,String) D.RegPatRep -> [D.Group] -> [Dec]
genInsSubtreeDec sRegPatEnv grps =
  let eqvs = extractEquivTriples grps
      triples = [(st,vt,r) | (vt,sTyRulePairs) <- eqvs, (st,r) <- sTyRulePairs]
  in  map (genInsSubtreeGrpDec sRegPatEnv) triples

genInsSubtreeGrpDec :: Map (D.Pattern,String,String) D.RegPatRep ->
                       (D.DataTypeRep,D.DataTypeRep,D.Rule) -> Dec
genInsSubtreeGrpDec sRegPatEnv (sTy, vTy, D.Rule lhs rhs@(D.VarP _ vv0 _) bases) =
  FunD (mkName "insSubtree") [clause]
  where
    clause = Clause [var2P regPat, viewPS, var2P "s0"]
               (NormalB (var2E "toDyn" `AppE` dslPat2HsExp (repHoleName "s1" (invlhs, path))))
               [mkInj]
    regPat = D.fromJ "cannot find pattern. in genRepSubtree" $
               Map.lookup (normPattern lhs, D.printTyAsName sTy, D.printTyAsName vTy) sRegPatEnv

    invlhs = evalState (D.pat2RegPat lhs) 0
    viewPS = ViewP (ParensE (var2E ("fromDynamic :: Dynamic -> Maybe (" ++
                                     D.printTypeRep sTy ++ ")")))
                   (ConP (mkName "Just") [dslPat2HsPat $ repHoleName "theHole" (invlhs, path)])
    mkInj = simpleValD (var2P "s1")
              (var2E "fromDyn" `AppE` mkInj2 `AppE` var2E nullCon)
    mkInj2 = foldl1 AppE [var2E "mkInj"
             ,var2E "tyTag" `AppE` var2E "theHole"
             ,var2E "askDynTyTag" `AppE` var2E "s0"
             ,var2E "s0"]
    nullCon = pprint (mkDefVal lhsVarTy)

    lhsVar = case [sv | ((D.VarP _ sv t),(D.VarP _ vv _)) <- bases, vv == vv0] of
              [sv] -> sv
              _   -> error "0xA4. seems impossible."
    path = findLinearVarPath lhsVar lhs
    lhsVarTy = case [t | ((D.VarP _ sv t),(D.VarP _ vv _)) <- bases, vv == vv0] of
                [t] -> t
                _   -> error "0xA3. seems impossible."


-- replace the ONLY hole (varialbe pattern, not wild carsd) in a LHS pattern
-- with a new variable name the search for the hole is guided by Step
repHoleName :: String -> (D.Pattern, D.Path) -> D.Pattern
repHoleName newName (D.ConP con rep fields , []) = D.ConP con rep fields
repHoleName newName (D.ConP con rep fields , p)  =
  D.ConP con rep (repHoleName2 newName (fields,p))
repHoleName newName (D.VarP uid v rep , []) = D.VarP uid v rep
repHoleName _ _ = error "impossible case in repHoleName."

repHoleName2 :: String -> ([D.Pattern], D.Path) -> [D.Pattern]
repHoleName2 newName (ds, [p]) =
  let (before, this, after) = (take p ds , ds !! p , drop (p + 1) ds)
  in  case this of
        D.WildP rep  -> before ++ [D.VarP "" newName rep] ++ after
repHoleName2 newName (ds, p:ps) =
  let (before, this , after) = (take p ds , ds !! p , drop (p + 1) ds)
  in  before ++ [repHoleName newName (this,ps)] ++ after
