{-
we need to generate datatypes for representing all the left-hand paterns of
consistency declarations.

Expr <---> Arith
Plus _ x y ~ Add x y

data SRegPat =
    PlusRP0
  | MinusRP1
  | FromTermRP2
  | ...

We also need to do so for the right-hand sides because we need the run-time
representation of the pattern for view-side of the HLinks.

data VRegPat =
    AddRP0
  | SubRP1
  | ...

For simplicity, the two data types are mereged into one in this implementation
-}


module GenHelper.GenRegPat where

import qualified Def as D
import THAuxFuns

import Language.Haskell.TH as TH

import Data.Map hiding (foldr, foldl, map)
import qualified Data.Map as Map
import Text.Show.Pretty
import Control.Monad.State
import Data.List (nub, sort)
import Data.Maybe (catMaybes)

import Debug.Trace

-- generate datatype declaration of Regions from [D.DataTypeRep]
genRegPatDef :: [D.Group] -> String
genRegPatDef grps =
  let regPatEnv = genRegPatEnv grps
      sRegs     = elems regPatEnv
      sRegsPlus = sRegs ++ map mkPrimPatReg D.primTypes

      sDec = DataD [] (mkName "RegPat") [] Nothing
              (map (\tg -> NormalC (mkName tg) []) sRegsPlus ++
  -- add additional unit pattern for imaginary nodes.
                    [NormalC (mkName "Void") []])
              [DerivClause Nothing
                [var2T "Eq", var2T "Show", var2T "Ord", var2T "Read"]]

  in  pprint sDec



-- tags for prim types. only string representations
mkPrimPatReg :: String -> String
mkPrimPatReg ty = ty ++ "R"


-------------

-- generate a map from pattern to its hash, which is the runtime representation
-- for both the pattern and its region pattern (pat: Plus _ x y. regpat: Plus a _ _)
genRegPatEnv :: [D.Group] -> Map (D.Pattern,String,String) D.RegPatRep
genRegPatEnv grps = unions $ map (\g -> evalState (genRegPatGroup g) 0) grps
  where
    genRegPatGroup :: D.Group -> State Int (Map (D.Pattern,String,String) D.RegPatRep)
    genRegPatGroup (D.Group (sTy,vTy) rs) = do
      ms <- mapM (genRegPat2 (sTy,vTy)) rs
      return $ unions ms

    genRegPat2 :: (D.DataTypeRep, D.DataTypeRep) -> D.Rule ->
                  State Int (Map (D.Pattern,String,String) D.RegPatRep)
    genRegPat2 (sTy,vTy) (D.Rule lhs rhs _) = do
      let lhs' = normPattern lhs
          rhs' = normPattern rhs
          sTyStr = D.printTyAsName sTy
          vTyStr = D.printTyAsName vTy
      n <- get
      put (n + 1)
      return $ if D.isVarPat lhs' then singleton (lhs',sTyStr,vTyStr) "Void"
                  else singleton (lhs',sTyStr,vTyStr) (sTyStr ++ vTyStr ++ "S" ++ show n)
               `union`
               -- here. singleton (lhs',sTyStr) "Void" is not an error. we only need ONE "void"
               if D.isVarPat rhs' then singleton (lhs',sTyStr,vTyStr) "Void"
                  else singleton (rhs',sTyStr,vTyStr) (sTyStr ++ vTyStr ++ "V" ++ show n)



-- normalise a pattern by renaming its variables
-- pattern "a" and pattern "z" are the same
-- pattern "Add x y" and pattern "Add y x" are the same
-- pattern "Add x x" and pattern "Add y y" are the same
-- build a map from a variable's old name to its new name and perform simultaneous substitution
-- apply the substitution, and generate variables UID afterwards
normPattern :: D.Pattern -> D.Pattern
normPattern pat =
  let vars = nub . sort . extractVars $ pat
      env = Map.fromList $ zipWith (\v i -> (v, "v" ++ show i)) vars [0..]
  in  applySubsts env pat


applySubsts :: Map String String -> D.Pattern -> D.Pattern
applySubsts env (D.ConP con ty pats) =
  D.ConP con ty (map (applySubsts env) pats)
applySubsts env (D.VarP uid v ty) = D.VarP uid (D.fromJ "impossible. 9x00" (Map.lookup v env)) ty
applySubsts _ other = other


-- Plus _ a b ~ Add x y <= a ~ x, b ~ y ; ... ; FromTerm _ a ~ t <= a ~ t ;
-- will give [(PlusR String Unit Unit, AddR Unit Unit),
-- (FromTermR String Unit, UnitR)]
-- where the "String" should be replaced by the type of the hole.
